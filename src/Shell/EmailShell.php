<?php
namespace App\Shell;

use Cake\Console\Shell;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


class EmailShell extends Shell
{
    public function main(){
        
        $this->out("Training Notification");
        $this->trainingPendingNotification();        
        $this->out("----------------");
        $this->out('Consultation Follow Up');
        $this->consultationEmailFollowUp();
        $this->out("----------------");
        $this->out('Bookings Follow Up');
        $this->bookingsEmailFollowUp();
        $this->changeBookingStatus();
        $this->deleteAssessments();
        
    }
    
    private function changeBookingStatus(){
        
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails = $ProgramsTable->find("all",['conditions' => [
            'Programs.status in ' => ['Pending', 'In Progress']
        ]])->contain(['Lessons']);
        $x = 0;
        foreach($programDetails as $prog):
            
            $up = "Yes";
            foreach($prog->lessons as $lesson):
                
                if($lesson->status == "Pending" || $lesson->status == "Scheduled"):
                    $up = "No";
                endif;
                
            endforeach;
            
            if($up == "Yes"):
                
                $progDetails = $ProgramsTable->get($prog->id);
                $progDetails->status = "Completed";
                $ProgramsTable->save($progDetails);
                $x++;
                echo $prog->id . "<br/>";
            endif;
            
        endforeach;
    }
    
    private function deleteAssessments () {
        $status = ['In Progress', 'Pending'];
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentTypesDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.created <= ' => date("Y/m/d H:i:s", strtotime('-14 days')),
            'StudentAssessmentTypes.status in ' => $status
        ]])->contain(['Students.Clients']);
        
        foreach ($studentAssessmentTypesDetails as $details):
            $StudentAssessmentTypesTable->delete($details);
        endforeach;
    }
    
    private function consultationEmailFollowUp () {
        
        $emailInfo = $this->emailInfo();
        $consultationTbl = TableRegistry::get('Consultations');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status' => 'Pending Consultation',
                        'Consultations.consultation_session_date <= ' => date("Y/m/d H:i:s", strtotime('-24 hours'))
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students']);
         
            foreach ($consultations as $consultation):
                foreach ($consultation->leads as $lead):
                    if ($lead->tutor->status == 'Active'):
                $message = '

                    Hi ' . $lead->tutor->first_name .',

                    Records indicate you had a consultation booked recently for ' . $lead->client->first_name . " " . $lead->client->last_name . ' Please complete your consultation report via your dashboard as soon as possible. 

                    Alternatively, please phone the office on 1300 4200 79 if there are any difficulties.

                    ' . $emailInfo[3] . '

                    ';
                if (empty($lead->notif_consultation)):
                  
                    $this->out($lead->tutor->email);
                    $email = new Email();
                    $email->from([$emailInfo[5] => 'Tutor2You'])
                    ->to($lead->tutor->email)
//                    ->to('tammy@tutor2you.com.au')
                    ->send($message);

                    $updateConsultation = $consultationTbl->get($consultation->id);
                    $updateConsultation->notif_consultation = date("Y-m-d H:i:s");
                    $consultationTbl->save($updateConsultation);
                
                
                else:
                    
                    $dateNotif = date("Y-m-d H:i:s", strtotime($lead->notif_consultation));
                    $currentDate = date("Y-m-d H:i:s"); 
                    $seconds = strtotime($currentDate) - strtotime($dateNotif);
                    $hours = $seconds / 60 /  60;
                    
                    if ($hours >= 24):
//                      
                       $this->out($lead->tutor->email);
                       $email = new Email();
                       $email->from([$emailInfo[5] => 'Tutor2You'])
                       ->to($lead->tutor->email)
//                       ->to('tammy@tutor2you.com.au')
                       ->send($message);  
                       
                    $updateConsultation = $consultationTbl->get($consultation->id);
                    $updateConsultation->notif_consultation = date("Y-m-d H:i:s");
                    $consultationTbl->save($updateConsultation);
                     
                    endif; 
                    
                endif;
                    endif;
                endforeach;
            endforeach; 
    }
    
    private function bookingsEmailFollowUp() {
        
        $emailInfo = $this->emailInfo();
        $LeadsTable = TableRegistry::get('Leads');
        $leads = $LeadsTable->find('all', ['conditions' => [
                        'Leads.status' => 'New',
                        'Leads.created <= ' => date("Y-m-d H:i:s", strtotime('-24 hours') )
               
            ]])->distinct(['Leads.consultation_id'])
                ->contain(['Clients', 'Tutors', 'Consultations']);
      
            foreach($leads as $lead):
//             $leadID = $lead->id;
                if ($lead->consultation->status == 'New' && $lead->tutor->status == 'Active'):
                $message = '

                Hi ' . $lead->tutor->first_name .',
                
                It looks like you were allocated a new client ' . $lead->client->first_name . " " . $lead->client->last_name . ' and a consultation date has not been booked in. Please book your consultation with the client and
                allocate any relevant assessments as soon as possible. 
                
                Alternatively, please phone the office on 1300 4200 79 if there are any difficulties.
              
                 ' . $emailInfo[3] . '

                  '; 
                if (empty($lead->notif_bookings)):
                      
                    $this->out($lead->tutor->email);
                    $email = new Email();
                    $email->from([$emailInfo[5] => 'Tutor2You'])
//                    ->to('tammy@tutor2you.com.au')
                    ->to($lead->tutor->email)
                    ->send($message);

                    $updateLead = $LeadsTable->get($lead->id);
                    $updateLead->notif_bookings = date("Y-m-d H:i:s");
                    $LeadsTable->save($updateLead);
//                    debug($updateLead); exit;
                    
                else:
               
                    $dateNotif = date("Y-m-d H:i:s", strtotime($lead->notif_bookings));
                    $currentDate = date("Y-m-d H:i:s");

                    $seconds = strtotime($currentDate) - strtotime($dateNotif);
                    $hours = $seconds / 60 /  60;
                      
                     if ($hours >= 24):
                       
                       $this->out($lead->tutor->email);
                       $email = new Email();
                       $email->from([$emailInfo[5] => 'Tutor2You'])
                       //->to('tammy@tutor2you.com.au')
                       ->to($lead->tutor->email)    
                       ->send($message);  
                       
                       $updateLead = $LeadsTable->get($lead->id);
                       $updateLead->notif_bookings = date("Y-m-d H:i:s");
                       $LeadsTable->save($updateLead);
                       //debug($updateLead); exit;
                      
                       endif; 
                        
                    endif;
                endif;
             endforeach; 
             
             $this->out('Email Success');
        
    }
    
    private function trainingPendingNotification(){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $emailInfo = $this->emailInfo();
        $info = array('New','Pending Contract Review & Expectations','Pending Platform Training');
        $message = "";
        $moduleUsersDetail_3= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status in ' => $info,
            'Tutors.status' => "Active",
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-3 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
            
            
        $subject = 'Tutor2you Training Followup';
            
        foreach($moduleUsersDetail_3 as $day3):
            
            $updateInfoMD = $moduleUsersTable->get($day3->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day3->tutor->first_name . " " . $day3->tutor->last_name . ',<br/><br/>
                    You have a "' . $day3->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/><br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
        
            $this->out($day3->tutor->email);
            $email = new Email();
            $email->from([$emailInfo[5] => 'Tutor2You'])
                ->emailFormat('html')
                ->to($day3->tutor->email)
                ->subject($subject)
                ->send($message);
            
        endforeach;
            
            
        $moduleUsersDetail_10= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status' => 'Pending Tutor Training',
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-30 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function 
            ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
        
        
            
        foreach($moduleUsersDetail_10 as $day10):
            
            $updateInfoMD = $moduleUsersTable->get($day10->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day10->tutor->first_name . " " . $day10->tutor->last_name . ',<br/><br/>
                    You have a "' . $day10->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/><br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
        
            $this->out($day10->tutor->email);        
            $email = new Email();
            $email->from([$emailInfo[5] => 'Tutor2You'])
                ->emailFormat('html')
                ->to($day10->tutor->email)
                ->subject($subject)
                ->send($message);
            
        endforeach;
        
        
//            $message = 'SERVER TEST MESSAGE ';
        
//        $email = new Email();
//        $email->from([$emailInfo[5] => 'Tutor2You'])
//            ->emailFormat('html')
//            ->to('deloso.sesniel@yahoo.com')
//            ->subject($subject)
//            ->send($message);
        $this->out('Email Success!!!');
    }

    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                Tutor2You | Primary and Secondary | One-on-One Tutoring
                Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'joshua@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }
}
