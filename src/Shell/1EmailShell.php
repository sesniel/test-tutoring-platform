<?php
namespace App\Shell;

use Cake\Console\Shell;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


class EmailShell extends Shell
{
    public function main(){
        
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $emailInfo = $this->emailInfo();
        $info = array('New','Pending Contract Review & Expectations','Pending Platform Training');
        $message = "";
        $moduleUsersDetail_3= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status in ' => $info,
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-3 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
            
            
        $subject = 'Tutor2you Training Followup';
            
        foreach($moduleUsersDetail_3 as $day3):
            
            $updateInfoMD = $moduleUsersTable->get($day3->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day3->tutor->first_name . " " . $day3->tutor->last_name . ',<br/><br/>
                    You have a "' . $day3->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/><br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
        
            $this->out($day3->tutor->email);
            $email = new Email();
            $email->from([$emailInfo[5] => 'Tutor2You'])
                ->emailFormat('html')
                ->to($day3->tutor->email)
                ->subject($subject)
                ->send($message);
            
        endforeach;
            
            
        $moduleUsersDetail_10= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status' => 'Pending Tutor Training',
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-30 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function 
            ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
        
        
            
        foreach($moduleUsersDetail_10 as $day10):
            
            $updateInfoMD = $moduleUsersTable->get($day10->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day10->tutor->first_name . " " . $day10->tutor->last_name . ',<br/><br/>
                    You have a "' . $day10->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/><br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
        
            $this->out($day10->tutor->email);        
            $email = new Email();
            $email->from([$emailInfo[5] => 'Tutor2You'])
                ->emailFormat('html')
                ->to($day10->tutor->email)
                ->subject($subject)
                ->send($message);
            
        endforeach;
        
        
//            $message = 'SERVER TEST MESSAGE ';
        
//        $email = new Email();
//        $email->from([$emailInfo[5] => 'Tutor2You'])
//            ->emailFormat('html')
//            ->to('deloso.sesniel@yahoo.com')
//            ->subject($subject)
//            ->send($message);
        $this->out('Email Success!!!');
    }

    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                Tutor2You | Primary and Secondary | One-on-One Tutoring
                Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'joshua@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }
}