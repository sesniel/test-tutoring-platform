<?php
namespace App\Shell;

use Cake\Console\Shell;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


class EmailShell extends Shell
{
    public function main(){
        
//        $this->overdueSession();
        
    }
    
    public function overdueSession(){
        
        $emailInfo = $this->emailInfo();
        $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.status' => "Active"
        ]])->contain(['BookingPrograms.ProgramBookingsLessons', 'BookingPrograms.Clients']);
        
//        debug($tutorDetails->toArray());exit;
        $date = date("Y-m-d");
        $display = array();
        foreach($tutorDetails as $tutor):   
        $body = "";
            if(!empty($tutor->booking_programs)):
               foreach($tutor->booking_programs as $bookings):
                if(!empty($bookings->program_bookings_lessons)):
                    $status = "";
                    foreach($bookings->program_bookings_lessons as $lessons):
                        $status = $lessons->status . " " . "<b>". date("d/m/Y", strtotime($lessons->date)) ."</b>". " " . date("h:i A", strtotime($lessons->time));
                        if($status != "" ):
                            $display[$tutor->id]['tutor_name'] = $tutor->first_name;
                            $display[$tutor->id]['email'] = $tutor->email;
                            $display[$tutor->id]['client_name'] = $bookings->client->first_name . " " . $bookings->client->last_name;
                            $body .= $status. "<br>";
                        endif;
                    endforeach;
                endif;
               endforeach;
            endif;
            if($body != ""):
                $display[$tutor->id]['date'][] = $body . "</div>";
            endif;
        endforeach; 
        
        foreach($display as $key => $dis):
            foreach($dis['date'] as $date):
            $subject = $dis['tutor_name'] . " - Overdue session Report Follow Up";
            $message = '
                
                Hi ' . $dis['tutor_name']. ',
                
                Please be advised that platform records indicate that there are one or more of your session reports that appear to be overdue. Please review the list below, and either submit your report or reschedule the lesson in order to avoid further follow ups and to avoid any payroll delays.<br><br>
               
                Client: '. $dis['client_name'] .'<br><br>
                
                '. $date .'<br><br>
                
                '. $emailInfo[3];
                
          $emailNotified = $TutorsTable->get($key);
          if(!empty($emailNotified->email_overdue)):
        
        $email = new Email();
        $email->from(['admin@tutor2you.com.au' => 'Tutor2You'])
            ->emailFormat('html')
            ->to($dis['email'])
            ->bcc('tammy@tutor2you.com.au')
            ->subject($subject)
            ->send($message);
          $emailNotified->email_overdue = date("Y-m-d H:i:s");
          $TutorsTable->save($emailNotified);
          endif;

            endforeach;
        endforeach;
       
        
    }
    
    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                Tutor2You | Primary and Secondary | One-on-One Tutoring
                Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'joshua@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }
}
