<style>
      
    .form-group input[type="checkbox"] {
        display: none;
    }

    .form-group input[type="checkbox"] + .btn-group > label span {
        width: 20px;
    }

    .form-group input[type="checkbox"] + .btn-group > label span:first-child {
        display: none;
    }
    .form-group input[type="checkbox"] + .btn-group > label span:last-child {
        display: inline-block;   
    }

    .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
        display: inline-block;
    }
    .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
        display: none;   
    }
    
    .text-info{
        font-size: 10px !important;
        width: 75px;
    }
    
    .text-info1{
        font-size: 10px !important;
        width: 30 px;
    }
    
    .text-info-long{
        font-size: 10px !important;
        width: 150px;
    }
</style>
<div class="container">
    <?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: -70px')) ?>
    
    
        <?php
        echo $this->Form->create(null, [
            'url' => ['action' => 'indexMap'],
            'method' => 'post',
            'target' => 'main'
        ]);
    ?>
    View Type:<br/> 
    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-primary text-info">
            <input type="checkbox" id='Tutor' name="users[Tutor]" value='' autocomplete="off" > Tutor
        </label>
        <label class="btn btn-primary text-info">
            <input type="checkbox" id='Client' name="users[Client]"  autocomplete="off"> Client
        </label>
        <label class="btn btn-primary text-info">
            <input type="checkbox" id='Applicant' name="users[Applicant]"  autocomplete="off"> Applicant
        </label>
    </div>
    
    
    <div><br/>&nbsp;</div>
    
    
    
    <div class="collapse" id="collapseClient">
      <div class="well">
          <h4>Client Detail View</h4>
          Select Client: <br/>
          <table class='table'>
              <tr>
                  <td>
                    <?Php                         
                      echo $this->Html->image("icon_client.png",
                                      ['border' => '0', 'class' => 'img-responsive center-block']);
                    ?>
                    &nbsp;
                  </td>
                  <td>
                    <select class="form-control client_id" id="client" name="client" >
                        <option value="0" disabled="">Select Client</option>
                    <?php foreach($clientDetails as $client): ?>
                              <option value="<?= $client->id ?>"><?= $client->first_name . " " . $client->last_name ?></option>
                    <?php endforeach; ?>
                    </select>
                      
                    <input id="pac-input" name="address[details]" class="pull-left form-control map-input" type="text" placeholder="Enter a location">
                  </td>
              </tr>
              <tr>
                  <td colspan="2">
                    <div class="form-group pull-left">
                        <input type="checkbox" name="address[checkbox]" id="address" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="address" style='background-color: #0b34ff; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="address" class="btn btn-default text-info-long">
                                Search for address
                            </label>
                        </div>
                    </div>
                  </td>
              </tr>
          </table>
      </div>
    </div>
    
    <div class="collapse" id="collapseTutor">
      <div class="well container">
          <h4>Tutor Detail View</h4>
                <div class="col-xs-12 col-sm-12">
                    <div class="form-group">
                        <input type="checkbox" name="tutor[Available]" id="tutor_available" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="tutor_available" style='background-color: #3f7a06; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="tutor_available" class="btn btn-default text-info-long">
                                Available
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="tutor[NotAvailable]" id="tutor_notavailable" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="tutor_notavailable" style='background-color: #ff0000; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="tutor_notavailable" class="btn btn-default text-info-long">
                                Not Available
                            </label>
                        </div>
                    </div>
                    <div><hr/><h5>Availability</h5></div>
                    <div class="form-group">
                        <input type="checkbox" name="hour[five]" id="hour_five" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="hour_five" style='background-color: #3f7a06; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="hour_five" class="btn btn-default text-info-long">
                                5 hour
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="hour[four]" id="hour_four" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="hour_four" style='background-color: #92ff29; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="hour_four" class="btn btn-default text-info-long">
                                4 hour
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="hour[three]" id="hour_three" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="hour_three" style='background-color: #d8da22; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="hour_three" class="btn btn-default text-info-long">
                                3 hour
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="hour[two]" id="hour_two" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="hour_two" style='background-color: #e89f55; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="hour_two" class="btn btn-default text-info-long">
                                2 hour
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="hour[one]" id="hour_one" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="hour_one" style='background-color: #e38383; border-color: #ccc;' class="btn btn-info text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="hour_one" class="btn btn-default text-info-long">
                                1 hour
                            </label>
                        </div>
                    </div>
                </div>
      </div>
    </div>
    
    <div class="collapse" id="collapseApplicant">
      <div class="well container">
          <h4>Applicant Detail View</h4>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <input type="checkbox" name="applicant[New]" id="applicant_new" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="applicant_new" style='background-color: #676767; border-color: #ccc;' class="btn btn-success text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="applicant_new" class="btn btn-default text-info-long">
                                New
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="applicant[Reviewed]" id="applicant_reviewed" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="applicant_reviewed" style='background-color: #3cc9fd; border-color: #ccc;' class="btn btn-success text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="applicant_reviewed" class="btn btn-default text-info-long">
                                Reviewed
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="applicant[Accepted]" id="applicant_accepted" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="applicant_accepted"  style='background-color: #ff8000; border-color: #ccc;' class="btn btn-success text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="applicant_accepted" class="btn btn-default text-info-long">
                                Accepted
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="applicant[Abandoned]" id="applicant_abandoned" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="applicant_abandoned"  style='background-color: #ff0000; border-color: #ccc;' class="btn btn-success text-info1">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="applicant_abandoned" class="btn btn-default text-info-long">
                                Abandoned
                            </label>
                        </div>
                    </div>
                </div>
      </div>
    </div>
    
    <div>
        <br/>&nbsp;
        <input class='btn btn-success pull-right' type="submit" value="Generate" />
    </div>
    
    <?php echo $this->Form->end(); ?>
    
</div>

<script>
    

      function initMap() {
          
        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            return;
          }

        });
      }
    
    $(document).ready(function () {
        
        $(".map-input").hide();
        $("#address").click(function(event) {
            if ($(this).is(":checked")){
                $("#client").hide();
                $(".map-input").show();
            }else{
                $("#client").show();
                $(".map-input").hide();
            }
        });
        
        
        $('#Tutor').change(function(){
            if (this.checked) {
                $('#collapseTutor').fadeIn('slow');
            }
            else {
                $('#collapseTutor').fadeOut('slow');
            }                   
        });
        $('#Client').change(function(){
            if (this.checked) {
                $('#collapseClient').fadeIn('slow');
            }
            else {
                $('#collapseClient').fadeOut('slow');
            }                   
        });
        $('#Applicant').change(function(){
            if (this.checked) {
                $('#collapseApplicant').fadeIn('slow');
            }
            else {
                $('#collapseApplicant').fadeOut('slow');
            }                   
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDudMe2fMw8WDRKUH_wWJkfolMKgKg7gVI&callback=initMap&libraries=places" async defer></script>
    