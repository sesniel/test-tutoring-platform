
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
        z-index: 9999;
      }
      /* Optional: Makes the sample page fill the window.
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      } */
    </style>
    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
    <div id="map">    </div>

    <script>
        
        
      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

        
        
        var user = '<?php echo $userArray ?>';
        var customLabel = {
        Tutor: {
          image: '/webroot/img/icon_green.png'
        },
        Client: {
          image: '/webroot/img/icon_client.png'
        },
        Applicant_New: {
          image: '/webroot/img/icon_grey.png'
        },
        Applicant_Review: {
          image: '/webroot/img/icon_lightblue.png'
        },
        Applicant_Accepted: {
          image: '/webroot/img/icon_orange.png'
        },
        Applicant_Abandoned: {
          image: '/webroot/img/icon_red.png'
        },
        Tutor_Available: {
          image: '/webroot/img/icon_green.png'
        },
        Tutor_NotAvailable: {
          image: '/webroot/img/icon_red.png'
        },
        Tutor_One: {
          image: '/webroot/img/icon_lightred.png'
        },
        Tutor_Two: {
          image: '/webroot/img/icon_lightorange.png'
        },
        Tutor_Three: {
          image: '/webroot/img/icon_yellow.png'
        },
        Tutor_Four: {
          image: '/webroot/img/icon_lightgreen.png'
        },
        Tutor_Five: {
          image: '/webroot/img/icon_green.png'
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-27.469771, 153.025124),
          zoom: 10
        });
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
//          downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function(data) {
          downloadUrl('/admin/maps/site-xml/xml/'+user, function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                icon: icon.image
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
    
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDudMe2fMw8WDRKUH_wWJkfolMKgKg7gVI&callback=initMap">
    </script>
    