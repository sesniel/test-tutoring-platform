<?Php 
//debug($userDetails->toArray());exit;
$xmlString = '<?xml version="1.0" encoding="UTF-8"?><markers>'; 

    foreach($userDetails as $user): 

        $xmlString .= '<marker id="'.$user->id.'" name="'. $user->first_name .' '. $user->last_name . '" address="'.$user->address.'" lat="'.$user->lat.'" lng="'.$user->lng.'" type="'. $user->type . '" />';

    endforeach; 
    $xmlString .= '</markers>';
//    debug($userDetails->toArray());
//    echo $xmlString;
    
$dom = new DOMDocument;
$dom->preserveWhiteSpace = FALSE;
$dom->loadXML($xmlString);

//Save XML as a file
$dom->save(WWW_ROOT . 'map'.DS.'personnel.xml');