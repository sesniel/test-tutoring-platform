<style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
   #map {
    height: 100%;
  }
  /* Optional: Makes the sample page fill the window. */
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
  #description {
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
  }

  #infowindow-content .title {
    font-weight: bold;
  }

  #infowindow-content {
    display: none;
  }

  #map #infowindow-content {
    display: inline;
  }

  .pac-card {
    margin: 10px 10px 0 0;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    background-color: #fff;
    font-family: Roboto;
  }

  #pac-container {
    padding-bottom: 12px;
    margin-right: 12px;
  }

  .pac-controls {
    display: inline-block;
    padding: 5px 11px;
  }

  .pac-controls label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
  }

  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 400px;
  }

  #pac-input:focus {
    border-color: #4d90fe;
  }

  #title {
    color: #fff;
    background-color: #4d90fe;
    font-size: 25px;
    font-weight: 500;
    padding: 6px 12px;
  }
  #target {
    width: 345px;
  }
</style>
<input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map">    </div>
<script
  src="https://code.jquery.com/jquery-3.2.1.js"
  integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
  crossorigin="anonymous"></script>
<script>
    <?php $urlGetDetails = $userArray ."/". $getClient ."/". $getHour ."/".$getTutor."/".$getApplicant; ?>
     var user = '<?php echo $urlGetDetails ?>';
    console.log(user);
    var customLabel = {
    Tutor: {
      image: '/webroot/img/icon_green.png'
    },
    Client: {
      image: '/webroot/img/icon_client.png'
    },
    Applicant_New: {
      image: '/webroot/img/icon_grey.png'
    },
    Applicant_Review: {
      image: '/webroot/img/icon_lightblue.png'
    },
    Applicant_Accepted: {
      image: '/webroot/img/icon_orange.png'
    },
    Applicant_Abandoned: {
      image: '/webroot/img/icon_red.png'
    },
    Tutor_Available: {
      image: '/webroot/img/icon_green.png'
    },
    Tutor_NotAvailable: {
      image: '/webroot/img/icon_red.png'
    },
    Tutor_One: {
      image: '/webroot/img/icon_lightred.png'
    },
    Tutor_Two: {
      image: '/webroot/img/icon_lightorange.png'
    },
    Tutor_Three: {
      image: '/webroot/img/icon_yellow.png'
    },
    Tutor_Four: {
      image: '/webroot/img/icon_lightgreen.png'
    },
    Tutor_Five: {
      image: '/webroot/img/icon_green.png'
    }
  };

    function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(-27.469771, 153.025124),
      zoom: 10
    });

    var infoWindow = new google.maps.InfoWindow;

    // Change this depending on the name of your PHP or XML file
    // downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function(data) {
    downloadUrl('/admin/maps/site-xml/xml/'+user, function(data) {
                
        var distance;
        var duration;
        var olng = '<?php echo $originLng; //"153.0310787"; ?>';
        var olat = '<?php echo $originLat; //"-27.3728931"; ?>';
        
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName('marker');
        Array.prototype.forEach.call(markers, function(markerElem) {
        var name = markerElem.getAttribute('name');
        var address = markerElem.getAttribute('address');
        var subjects = markerElem.getAttribute('subjects');
        var gpa = markerElem.getAttribute('gpa');
        var opatar = markerElem.getAttribute('opatar');
        var type = markerElem.getAttribute('type'); 
        var link = markerElem.getAttribute('link');
        var url = document.createElement('a');
        var LatDesti = markerElem.getAttribute('lat');
        var LngDesti = markerElem.getAttribute('lng');
        url.href = link;
        url.target = "_blank";
        url.textContent = "View Profile"; 
        var textH2 = document.createElement('h2');
        textH2.textContent = name;        
        var point = new google.maps.LatLng(
            parseFloat(LatDesti),
            parseFloat(LngDesti));
        var infowincontent = document.createElement('div');
        infowincontent.appendChild(textH2);
        
        infowincontent.appendChild(url);
        infowincontent.appendChild(document.createElement('br'));
        var text = document.createElement('text');
        text.textContent = address;

        infowincontent.appendChild(text);            

        if(subjects != ""){
            var textSubject = document.createElement('text');
            var strong = document.createElement('strong');
            strong.textContent = "Subjects: ";
            textSubject.textContent = subjects;

            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(strong);
            infowincontent.appendChild(textSubject);

        }

        if(gpa != ""){
            var textGpa = document.createElement('text');
            var textOpt = document.createElement('text');
            var strong1 = document.createElement('strong');
            var strong2 = document.createElement('strong');
            strong1.textContent = "GPA: ";
            strong2.textContent = "OP/ATAR: ";
            textGpa.textContent = gpa;
            textOpt.textContent = opatar;

            // console.log("SESNIEL: " + strong);
            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(strong1);
            infowincontent.appendChild(textGpa);
            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(strong2);
            infowincontent.appendChild(textOpt);

        }        
                               
        var leads = markerElem.getElementsByTagName('leads');
        infowincontent.appendChild(document.createElement('br'));
        for (var i = 0; i < leads.length; i++) {   
            var studentName = leads[i].getAttribute('student_name');
            var studentSubj = leads[i].getAttribute('student_subj');

            var strongStud = document.createElement('strong');
            var strongSubj = document.createElement('strong');
            strongStud.textContent = "Student: ";
            strongSubj.textContent = "Subject: ";

            var textStud = document.createElement('text');
            var textSubj = document.createElement('text');
            textStud.textContent = studentName;
            textSubj.textContent = studentSubj;

            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(strongStud);
            infowincontent.appendChild(textStud);

            infowincontent.appendChild(document.createElement('br'));
            infowincontent.appendChild(strongSubj);
            infowincontent.appendChild(textSubj);
            infowincontent.appendChild(document.createElement('br'));

        }   
        
        if(olng != 0 && olat != 0){                
            $.ajax({
                url: '/admin/maps/destination/json/'+olat+'/'+olng+'/'+LatDesti+'/'+LngDesti,
                type: "POST",
                success:function(data){ 

                    distance = "Distance: " + data['details']['rows'][0]['elements'][0]['distance']['text'];
                    duration = "Duration: " + data['details']['rows'][0]['elements'][0]['duration']['text'];
                    var textDistance = document.createElement('text');
                    var textDuration = document.createElement('text');
                    textDistance.textContent = distance;
                    textDuration.textContent = duration;
                    infowincontent.appendChild(document.createElement('hr'));
                    infowincontent.appendChild(document.createElement('br'));
                    infowincontent.appendChild(textDistance);
                    infowincontent.appendChild(document.createElement('br'));
                    infowincontent.appendChild(textDuration);

                }
            });   
        }
        
        var icon = customLabel[type] || {}; 
        var marker = new google.maps.Marker({
          map: map,
          position: point,
          icon: icon.image
        });
        marker.addListener('click', function() {
          infoWindow.setContent(infowincontent);
          infoWindow.open(map, marker);
        });
        });
    });



    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

    }
    
    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
              request.onreadystatechange = doNothing;
              callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

  function doNothing() {}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDudMe2fMw8WDRKUH_wWJkfolMKgKg7gVI&callback=initMap&libraries=places" async defer></script>
    