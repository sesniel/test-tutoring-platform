<style>
    .standard{
        width:160px;
    }
    
    .standard1{
        width:160px;
    }
    
    .form-control{
        height:30px !important;
    }
    
    .center-block {
      display: block;
      text-align: center;
      margin-left: auto;
      margin-right: auto;
    }
    
    .inner {
        width: 50%;
        text-align: center;
        margin: 0 auto;
      }
      
    
</style>
<?php
    $consultationStatus = array('New','Pending Consultation','Completed Consultation','Discontinued Consultation','Abandoned', 'Booked'); asort($consultationStatus);
?>
<div class="row"> 
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Consultation Details</strong></h2>
            </div>
            <hr class="customHr">
            <div class="row">
                <div class="col-sm-6 col-md-6"></div>
                <div class="col-sm-6 col-md-6 pull-right">
                    <?php
                    echo $this->Form->create(null, [
                        'url' => ['action' => 'selectstatus'],
                        'method' => 'post'
                    ]);
                    ?>
                    Status:
                    <select name="bookingStatus" class="form-control" onchange="this.form.submit()" >
                        <option selected disabled >Select Consultation</option>
                        <option value="All" <?php $sel = ($status == "%%" ? "selected" : ""); echo $sel; ?> >All</option>
                        <?php foreach($consultationStatus as $cStat): ?>
                            <option <?php $sel = ($status == $cStat ? "selected" : ""); ?> value="<?= $cStat ?>" <?= $sel ?> ><?php echo ucfirst($cStat) ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo $this->Form->end(); ?>
                </div> 
            </div>
            <?Php 
                if(!empty($consDetails)): 
                
                    echo $this->Form->create(null, [
                        'url' => ['action' => 'updateConsultationInfo'],
                        'method' => 'post'
                    ]);
            ?>
            <input type="hidden" value="<?= $status ?>" name="bookingStatus" />
            <?Php if(count($consDetails) == 100): ?>
            <div class="row ">
                
                <div class="paginator col-lg-12" style="width:100%">
                    <div class="inner">
                    <ul class="uwrapper pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    </div>
                    
                </div>
            </div>
            <?php endif; ?>
            
            <div <?Php if(count($consDetails) == 100): ?> style="position:relative; top:-40px;" <?php endif; ?> class="widget-container table-responsive">
                
                <div style="padding-right: 50px; padding-bottom: 20px;">
                    <input type="submit" value="Update Consultation" class="btn btn-xs btn-primary pull-right" />
                </div>
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped responsive display table-bordered">
                                    <?php foreach($consDetails as $key => $consult): ?>
                                    <thead>
                                        <tr class="bg-info">
                                            <th>Id</th>
                                            <th>Consultation Date</th>
                                            <th>Consultation Time</th>
                                            <th>Payroll Date</th>
                                            <th>Status</th>
                                            <th>Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?= $consult->id ?></td>
                                            <td>
                                                <div class="input-group">
                                                    <input class="form-control consultationDate" id="lessonDate<?= $consult->id ?>"
                                                        type="text" name="lessonDates[<?= $consult->id ?>]" value="<?php if(date("d/m/Y", strtotime($consult->consultation_session_date)) != "01/01/1970" ): echo date("d/m/Y", strtotime($consult->consultation_session_date)); endif; ?>" ><span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input value="<?php echo date('h:i A', strtotime($consult->consultation_session_date)); ?>" class="form-control lessonTime ui-timepicker-input" id="lessonTime" type="text" name="lessonTime[<?= $consult->id ?>]" autocomplete="off">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <input class="form-control payrollDate" id="payrollDate<?= $consult->id ?>"
                                                        type="text" name="payrollDate[<?= $consult->id ?>]" value="<?php if(date("d/m/Y", strtotime($consult->payroll_date)) != "01/01/1970" ): echo date("d/m/Y", strtotime($consult->payroll_date)); endif; ?>" ><span class="input-group-addon">
                                                        <i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </td>
                                            <td>
                                                <select name="status[<?= $consult->id ?>]" class="form-control">
                                                    <option selected disabled > </option>
                                                <?php foreach($consultationStatus as $cStat): ?>
                                                    <option <?php $sel = ($consult->status == $cStat ? "selected" : ""); ?> value="<?= $cStat ?>" <?= $sel ?> ><?php echo ucfirst($cStat) ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </td>
                                            <td>
                                                <?php echo date("Y/m/d", strtotime($consult->created)); ?>
                                            </td>
                                        </tr>
                                        <?php if(!empty($consult->leads)): ?>
                                        <tr class="bg-warning">
                                            <th colspan="6">Lead Informations</th>
                                        </tr>
                                        <tr>
                                            <th>Id</th>
                                            <th>Status</th>
                                            <th>Student</th>
                                            <th>Client</th>
                                            <th colspan="2">Tutor</th>
                                        </tr>
                                        <?php foreach($consult->leads as $leads): ?>
                                            <tr>
                                                <td><?= $leads->id ?></td>
                                                <td><?= $leads->status ?></td>
                                                <td><?= $leads->student->name ?></td>
                                                <td><?= $leads->client->first_name . " " . $leads->client->last_name ?></td>
                                                <td colspan="2"><?php if($leads->tutor): echo $leads->tutor->first_name . " " . $leads->tutor->last_name; endif; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                        <tr>
                                            <th colspan="6"><br/><br/></th>
                                        </tr>
                                    </tbody>
                                    <?php endforeach; ?>
                                </table>
                            <?Php if(count($consDetails) == 100): ?>
                            <div class="row ">
                                <div class="paginator col-lg-12" style="width:100%">
                                    <div class="inner">
                                    <ul class="uwrapper pagination">
                                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next(__('next') . ' >') ?>
                                    </ul>
                                    <?php // echo $this->Paginator->counter() ?>
                                    </div>

                                </div>
                            </div>
                            <?php endif; ?>
                            <div style="padding-right: 50px;">
                                <input type="submit" value="Update Consultation" class="btn btn-xs btn-primary pull-right" />
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                echo $this->Form->end();
                endif; 
            ?>
        </div>
    </div>
</div>
<?php //debug($clientDetails->toArray()); ?>
<script>
    $(document).ready(function () {
       
        $('.consultationDate').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy',
            maxDate: "7m"
        });
        
        $('.payrollDate').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy',
            maxDate: "7m"
        });

        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        });
    });
</script>


<?php

//debug($consDetails);
