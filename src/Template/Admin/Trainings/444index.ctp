<?php $programStatus = array('Completed Training', 'All', 'My Active Applicants', 'New Applicants', 'Active Applicants', 'Applicants to be Finalized'); asort($programStatus); 
      $appStatus = array('My New Applicants', 'My Reviewed Applicants','New', 'Reviewed', 'Accepted', 'Abandoned'); 
?>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            
            <div class="widget-title">
                <h2><strong>List of Tutors</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-9 col-md-9">
                                <p>These are the list of tutors that are currently on the system, you can search a tutor
                                    by typing any keywords in the input box at the top right corner of the table.</p>
                                <hr>
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                                Status: 
                                <select name="status" class="form-control" onchange="this.form.submit()" >
                                    <?php  foreach($programStatus as $stat):  ?>
                                        <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?php if($status == "First"): echo "1"; else: echo $stat; endif; ?>"><?= $stat ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date Registered</th>
                                        <th>Training</th>
                                        <th>Created By</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tutors as $tutor): ?>
                                        <tr>
                                            <td>
                                            <?php
                                            echo $this->Html->link(
                                                $tutor->first_name . ' ' . $tutor->last_name,
                                                array('controller' => 'tutors', 'action' => 'tutorView',$tutor->token),
                                                ['class' => '']
                                            );
                                            ?></td>
                                            <td><?= date_format($tutor->created, 'Y/m/d') ?></td>                                         
                                            <td> 
                                                <?php
                                                    if(!empty($tutor->module_user)):
                                                        echo $tutor->module_user->status;
                                                    else:
                                                        echo "None";
                                                    endif;
                                                ?>
                                            </td>
                                            <td><?= $tutor->owner->first_name ?> <?= $tutor->owner->last_name ?></td>     
                                            <td>
                                                <div class="btn-group">
                                                <?php
//                                                    if(!empty($tutor->module_user)):

                                                        echo $this->Html->link(" View Training ",
                                                            array('controller' => 'trainings', 'action' => 'view', $tutor->id),
                                                            ['class' => 'btn btn-sm btn-info']
                                                        );
                                                        
                                                        if($tutor->module_user->status == "Completed Orientation"):
                                                            echo $this->Html->link(" Activate Contract Review & Expectations ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Contract Review & Expectations"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        elseif($tutor->module_user->status == "Completed Contract Review & Expectations"):
                                                            echo $this->Html->link(" Activate Platform Training ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Platform Training"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        elseif($tutor->module_user->status == "Completed Platform Training"):
                                                            echo $this->Html->link(" Activate Tutor Training ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Tutor Training"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        endif;
                                                                                                            
//                                                    else:
//                                                        
//                                                        echo $this->Html->link(
//                                                            "Activate Training",
//                                                            array('controller' => 'tutors', 'action' => 'activate-training', $tutor->token),
//                                                            ['class' => 'btn btn-sm btn-primary']
//                                                        );
//                                                        
//                                                                                                          
//                                                    endif;
                                                ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Recruitment Page -->
            <div class="widget-title">
                <h2><strong>List of Applicants</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-sm-9 col-md-9">
                                <p>These are the list of applicants who submitted an online form.</p>
                                
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                            Status: 
                                <select name="applicationStatus" class="form-control" onchange="this.form.submit()" >
                                    <?php foreach ($appStatus as $status): ?>
                                   <option <?php $sel = ($applicationStatus === $status ? "selected" : ""); ?> <?= $sel ?> value="<?= $status ?>"><?= $status ?></option>
                                   <?php endforeach; ?>
                                </select> 
                               
                           <!--    <input type="text" name="blue_card_expiry"
                                value="<?php foreach ($applicantDetails as $applicant):
                              foreach ($applicant->applications as $application):
                              echo $application->status;
                                 endforeach;
                              endforeach; ?>"
                         
                          
                          class="form-control" required="required"
                          data-toggle="tooltip" title="Blue Card Expiry Date">  -->
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Suburb</th>
                                        <th>OP/ATAR</th>
                                        <th>Date Registered</th>
                                        <th>Application Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        
                                        <?php foreach ($userDetails as $applications): ?>
                                           
                                        <tr>
                                            <td><?= $applications->first_name . " " . $applications->last_name; ?></td>
                                            <td><?= $applications->city ?></td>
                                            <td><?= $applications->suburb ?></td>
                                            <td><?php foreach ($applications->application_answers as $answers) {
                                             $ans = $answers->details;   
                                             $json = json_decode($ans, true);
                                             echo $json['HighSchoolResultOP_ATAR'];
                                            }
                                                      
                                            ?></td>
                                            <td><?= date_format($applications->created, 'Y/m/d') ?></td>
                                            <td><?php foreach ($applications->applications as $appli) {
                                                echo $appli->status;
                                            }?></td>
                                            <td><?php 
                                            echo $this->Html->link(" View Profile ",
                                                            array('controller' => 'trainings', 'action' => 'applicantView', $applications->token),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
                                             ?>
                                            <a href="<?= $this->request->webroot . 'web/resume/' . $applications->resume?>" class="btn btn-sm btn-warning" target="_blank">Resume</a>    
                                           </td>
                                        </tr>
                                            
                                        <?php endforeach; ?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
<?= $this->Html->script('admin/training.js'); ?>
<?php //debug($tutors->toArray()); ?>