<?php $programStatus = array('Completed Training', 'All', 'My Active Applicants', 'New Applicants', 'Active Applicants', 'Applicants to be Finalized'); asort($programStatus); 
      $appStatus = array('My New Applicants', 'My Reviewed Applicants', 'New', 'Reviewed', 'Accepted', 'Abandoned'); 
?>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
         <!-- Recruitment Page -->
            <div class="widget-title">
                <h2><strong>List of Applicants</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-sm-9 col-md-9">
                                <p>These are the list of applicants who submitted an online form.</p>
                                
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'recruitment'],
                                        'method' => 'post'
                                    ]);
                                ?>
                            Status: 
                                <select name="applicationStatus" class="form-control" onchange="this.form.submit()" >
                                    <?php foreach ($appStatus as $status): ?>
                                   <option <?php $sel = ($applicationStatus === $status ? "selected" : ""); ?> <?= $sel ?> value="<?= $status ?>"><?= $status ?></option>
                                   <?php endforeach; ?>
                                </select> 
                               
                           <!--    <input type="text" name="blue_card_expiry"
                                value="<?php foreach ($applicantDetails as $applicant):
                              foreach ($applicant->applications as $application):
                              echo $application->status;
                                 endforeach;
                              endforeach; ?>"
                         
                          
                          class="form-control" required="required"
                          data-toggle="tooltip" title="Blue Card Expiry Date">  -->
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Suburb</th>
                                        <th>OP/ATAR</th>
                                        <th>Date Registered</th>
                                        <th>Application Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($applicationDetails as $applications): ?>
                                        <?php if (!empty($applications->user)): ?>
                                        <tr>
                                            <td><?= $applications->user->first_name . " " . $applications->user->last_name; ?></td>
                                            <td><?= $applications->user->city ?></td>
                                            <td><?= $applications->user->suburb ?></td>
                                            <td><?php if (!empty($applications->application_answer->details)) {
                                                    $ans = $applications->application_answer->details;
                                                      $json = json_decode($ans, true);
                                                      echo $json['HighSchoolResultOP_ATAR'];
                                                    } 
                                                      
                                            ?></td>
                                            <td><?= date_format($applications->user->created, 'Y/m/d') ?></td>
                                            <td><?= $applications->status ?></td>
                                            <td><?php 
                                            echo $this->Html->link(" View Profile ",
                                                            array('controller' => 'trainings', 'action' => 'applicantView', $applications->user->token),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
                                             ?>
                                                
                                            <?php if ($this->request->webroot . 'web/resume/' . $applications->resume ): ?>
                                                <?php if ($applications->resume): ?>
                                            <a href="<?= $this->request->webroot . 'web/resume/' . $applications->resume?>" class="btn btn-sm btn-warning" target="_blank">Resume</a>    
                                                <?php endif; ?>
                                            <?php endif; ?>
                                           </td>
                                        </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
<?= $this->Html->script('admin/training.js'); ?>
<?php //debug($tutors->toArray()); ?>