<?php
$programStatus = array('All', 'Cancel', 'Done', 'In Progress', 'Pending', 'Tutor', 'Active');
$tutorStatus = array('Active', 'Discontinue');
$applicationStatus = ['New', 'Reviewed', 'Accepted', 'Abandoned'];
$statusDetails = ['New', 'Completed Orientation', 'Pending Contract Review & Expectations', 'Completed Contract Review & Expectations', 'Pending Platform Training',
    'Completed Platform Training', 'Pending Tutor Training', 'Completed Training'];
?>

<?php foreach ($applicantDetails as $applicant) : ?>
<div class="container">
    <div id="tutor-view" class="widget-box" role="dialog">
        
        <div class="widget-title">
                <h2><strong>Applicant Profile</strong></h2>
            </div>
       
        <div class="modal-body row">
            <?= $this->Form->create(null, ['url' => ['action' => 'applicantUpdate', 'method' =>'post']]); ?>
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-7">
                        <h4>Contact Info:</h4>
                        <hr>
                    </div>
                    <div class="col-md-3 customMtop top-buffer">

                    </div>
                    <div class="col-md-1">
                        <?php foreach($applicant->applications as $answers): ?>
                        <a href="<?= $this->request->webroot . 'web/resume/' . $answers->resume?>" class="btn btn-sm btn-success top-buffer" target="_blank">Resume</a>    
                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-1">
                        <button type="button" id="edit-tutor-btn"
                                class="btn btn-primary btn-xs pull-right top-buffer">Edit Contact
                        </button>
                        
                        <button type="submit" id="update-tutor-btn"
                                class="btn btn-success btn-xs hidden pull-right top-buffer">
                            Update
                        </button>
                    </div>
        
                 <!-- Tutor Status END --> 
                </div>
                <?= $this->Form->hidden('token', ['value' => $applicant->token]) ?>
                <div class="row center-indent tutor-info">
                    <hr>
                    <div class="col-md-3">
                        <label for="fname">First Name:</label>
                        <input type="text" id="fname" name="first_name"
                               value="<?= $applicant->first_name ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip" title="First Name">
                    </div>
                    <div class="col-md-3">
                        <label for="lname">Last Name:</label>
                        <input type="text" id="lname" name="last_name"
                               value="<?= $applicant->last_name ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip" title="Last Name">
                    </div>
                    <div class="col-md-3">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email"
                               name="email" value="<?= $applicant->email ?>" title="Email"
                               required>
                    </div>
                    <div class="col-md-3">
                        <label for="mobile">Mobile:</label>
                        <input type="text" class="form-control" id="mobile" name="mobile"
                               value="<?= $applicant->mobile ?>"
                               title="Mobile" maxlength="10" required>
                    </div>
                </div>
                <div class="row center-indent tutor-info">
                    <div class="col-md-3">
                        <label for="phone">Phone:</label>
                        <input type="text" id="phone" name="phone"
                               value="<?= $applicant->phone ?>"
                               class="form-control"
                               data-toggle="tooltip" title="First Name">
                    </div>
                    <div class="col-md-3">
                        <label for="suburb">Suburb:</label>
                        <input type="text" id="suburb" name="suburb"
                               value="<?= $applicant->suburb ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip"
                               title="Suburb">
                    </div>
                    <div class="col-md-3">
                        <label for="street_number">Street#:</label>
                        <input type="text" id="street_number" name="street_number"
                               value="<?= $applicant->street_number ?>" class="form-control"
                               required="required" data-toggle="tooltip"
                               title="Street Number">
                    </div>
                    <div class="col-md-3">
                        <label for="street_name">Street Name:</label>
                        <input type="text" id="street_name" name="street_name"
                               value="<?= $applicant->street_name ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip" title="Street Name">
                    </div>
                </div>
               
            <div class="row center-indent tutor-info">
               <div class="col-md-3">
                   <label for="card_number">Working With Children (Card number):</label>
                   <input type="text" name="blue_card_number"
                          value="<?= $applicant->blue_card_number ?>"
                          class="form-control"
                          data-toggle="tooltip" title="Blue Card Number">
               </div>
               <div class="col-md-3">
                   <label for="card_number">Working With Children (Expiry date):</label>
                   <input type="text" name="blue_card_expiry"
                          value="<?php if(!empty($applicant->blue_card_expiry)) { echo date("Y-m-d", strtotime($applicant->blue_card_expiry));}
                          ?>"
                          class="form-control"
                          data-toggle="tooltip" title="Blue Card Expiry Date">
               </div>
                
                <div class="col-md-3">
                        <label for="rate_name">Contact Owner:</label>
                        <select name="owner_id" id="ownder" class="form-control">
                        <?php foreach($adminSelect as $adm): ?>
                            <option <?php $sel = ($applicant->owner_id === $adm->id ? "selected" : ""); ?> <?= $sel ?> value="<?= $adm->id ?>"><?= $adm->first_name ?> <?= $adm->last_name ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
               
                <div class="col-md-3">
                        <label for="status_name">Application Status:</label>
                       <select name="status" class="form-control" onchange="form.submit()">
                            <?php foreach($applicationStatus as $stat): ?>
                           <?php foreach ($applicant->applications as $application):
                                  //echo $application->status; ?>
                          <option <?php $sel = ($application->status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <?php  endforeach; ?>
                              <?php endforeach; ?>
                       </select>
                </div> 
         <!--  <div class="col-md-3">
                    <label for="status_name">Application Status:</label>
                   <input type="text" name="blue_card_expiry"
                          value="<?php //foreach ($applicant->applications as $application):
                            //  echo $application->status;
                           //endforeach;  ?>"
                          
                          
                          class="form-control" required="required"
                          data-toggle="tooltip" title="Blue Card Expiry Date">
            </div> -->
                
            </div>
            <div class="row center-indent tutor-info">
               <div class="col-md-3">
                        <label for="training">Training Status:</label>
                        <select name="training" id="training" class="form-control">
                        <?php if (!isset($applicant->module_user)): ?>
                            <option selected="selected" disabled="">- - Not available - -</option>
                        <?php endif; ?>
                        <?php foreach($statusDetails as $stats): ?>
                            <?php if (isset($applicant->module_user)): ?>
                            <option <?php $sel = ($applicant->module_user->status === $stats ? "selected" : ""); ?> <?= $sel ?> value="<?= $stats ?>"><?= $stats ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </select>
                </div> 
           
           
            </div>
            <div class="row center-indent tutor-info">
                    <div class="col-md-6">
                        <label for="comments">General comments:</label>
                        <textarea name="comments"  title="comments"class="form-control" id="exampleTextarea" rows="3"><?php echo $applicant->comments; ?></textarea><br> 
                    </div>
            </div>
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h4 class="icon">Application Details</h4>
                    <hr>
                </div>
            </div>
            <?php foreach ($applicant->application_answers as $answers): ?>
            <div class="row center-indent applicant-answer acad">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">High School Result (OP/ATAR):</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['HighSchoolResultOP_ATAR'];
                                       ?></i></p></span>
               </div>
                <div class="col-md-4">
                    <span class="col-md-12"><label for="card_number">Are you currently studying, if so what?</label></span><br>
                    <span class="col-md-12 bg-success"><p class="text-justify"><i><?php $ans = $answers->details;
                            $json = json_decode($ans, true);
                            echo $json['Are_you_currently_studying_if_so_what'];
                            ?></i></p></span>
                </div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number">Where are you studying?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Where_are_you_studying'];
                    ?></i></p></span>
               </div>
            </div>
           <div class="row center-indent applicant-answer acad">
               <div class="col-md-2"></div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number">GPA:</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['GPA'];
                          ?></i></p></span>
               </div> 
               <div class="col-md-4">
                   <span class="col-md-12"> <label for="card_number">Do you have a drivers license and access to your own car?</label></span><br>
                   <span class="col-md-12 bg-success"><p><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Do_you_have_a_drivers_license_and_access_to_your_own_car'];
                          ?></i></p></span>
               </div>
               <div class="col-md-3">
                   <span class="col-md-12"><label for="card_number">How many hours a week would you like to tutor?</label></span><br>
                   <span class="col-md-12 bg-success"><p><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['How_many_hours_a_week_would_you_like_to_tutor'];
                          ?></i></p></span>
               </div>
            </div> 
            <div class="row center-indent applicant-answer acad">
                <div class="col-md-2"></div>
                
                <div class="col-md-3">
                   
                   <span class="col-md-12"><label class="text-justify">What Subjects do you feel you are able to tutor and to what level?</label></span><br>
                   <span class="col-md-12 bg-success"><p class="text-justify"><i><?php
                          $subj = array();
                          foreach ($applicant->tutor_subjects as $subjects) {
                          $subj[] = $subjects->subject->name;
                          }
                          echo implode(", ", $subj);
                          ?></i></p></span>
                   
                   
                </div>
                <div class="col-md-4">
                   <span class="col-md-12"><label for="card_number" class="text-justify">Explain why you think you would make an excellent tutor for these subjects.</label> </span><br>
                   <span class="col-md-12 bg-success"><p class="text-justify"><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects'];
                                       ?></i></p></span>
                </div> 
            </div>
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h4 class="icon1">Personal Background & Ability</h4>
                    <hr>
                </div>
            </div>
            <div class="row center-indent applicant-answer acad1">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Describe how you would use your personal experience to assist your students, and why you believe it would help them.</label></span><br>
                    <span class="col-md-12 bg-success"><p class="text-justify"><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Describe_how_you_would_use_your_personal_experience_to_assist_your_students_and_why_you_believe_it_would_help_them'];
                                       ?></i></p></span>
                </div> 
                
            
            </div>
            
            <div class="row center-indent applicant-answer acad1">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <span class="col-md-12"> <label for="card_number" class="text-justify">Given sessions are often 1-2 hours long, how long would you be comfortable travelling in order to get to a session? (Note: hourly rates vary from $27-35/hr, travel time not included)</label></span><br>
                        <span class="col-md-12 bg-success"><p class="text-justify"><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Given_sessions_are_often_1_to_2_hours_long_how_long_would_you_be_comfortable_travelling_in_order_to_get_to_a_session'];
                                       ?></i></p></span>
                </div> 
            </div>
            <div class="row center-indent applicant-answer acad1">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Lastly please describe why you would like to become an Tutor2You Tutor and why do you think you would represent our company well with clients.</label></span><br>
                    <span class="col-md-12 bg-success"><p class="text-justify"><i><?php $ans = $answers->details;
                                       $json = json_decode($ans, true);
                                       echo $json['Lastly_please_describe_why_you_would_like_to_become_an_Tutor2You_Tutor_and_why_do_you_think_you_would_represent_our_company_well_with_clients'];
                                       ?></i></p></span>
                </div>     
            </div>
                
            <?php endforeach; ?>    
            <?= $this->Form->end(); ?>
            
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h4 class="icon1">Phone Interview Notes</h4>
                    <hr>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <label>Notes section <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></label><br><br>
                        <textarea name="notes"  title="comments"class="form-control" id="applicant-info" rows="6"><?php if (isset($answers->interview_notes)) {echo $answers->interview_notes;}  ?></textarea><br> 
                        </div>
                        <div class="col-md-12">
                    <button type="button" id="edit-applicant-btn"
                                class="btn btn-primary btn-xs top-buffer pull-right">Edit notes
                        </button>
                        
                        <button type="submit" id="update-applicant-btn"
                                class="btn btn-success btn-xs hidden top-buffer pull-right">
                            Update notes
                        </button>

                </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <label class="">Background</label>
                            <p><span class="glyphicon glyphicon-ok"></span> Previous jobs</p>
                            <p><span class="glyphicon glyphicon-ok"></span> Availability</p>
                            <p><span class="glyphicon glyphicon-ok"></span> How many hours can commit to for whole semester:</p>
                            <p><span class="glyphicon glyphicon-ok"></span> What’s your day to day look like? Can you respond to calls and messages within hours:</p>
                            
                        </div>
                        <div class="col-md-6 ">
                            <label class="">Communication Skills</label>
                            <p><span class="glyphicon glyphicon-ok"></span> Describe an example of when your listening skills proved crucial to an outcome: </p>
                            <p><span class="glyphicon glyphicon-ok"></span> Describe a time you had to explain something complex at work:</p>
                            <p><span class="glyphicon glyphicon-ok"></span> How and when have you varied your communication approach according to the person you were addressing:</p>
                        </div>
                    </div>
               
                </div>
            </div> 
               
           
            
        </div>
    </div>
    </div> 
</div>
<?php endforeach; ?>

 <script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#doesnt-exist':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('.');
            break;
        case '#applicant-updated':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Applicant information updated.');
            break;  
        case '#accepted':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Application status changed to "Accepted"');
            break;  
        case '#reviewed':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Application status changed to "Reviewed"');
            break; 
        case '#rejected':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Application status changed to "Abandoned"');
            break;
    }

});
 

</script>


<?= $this->Html->script('admin/applicant-view.js'); ?>