<?php $programStatus = array('Completed Training', 'All', 'My Active Applicants', 'New Applicants', 'Active Applicants', 'Applicants to be Finalized'); asort($programStatus); 
      $appStatus = array('My New Applicants', 'My Reviewed Applicants', 'New', 'Reviewed', 'Accepted', 'Abandoned'); 
?>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            
            <div class="widget-title">
                <h2><strong>List of Tutors</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-9 col-md-9">
                                <p>These are the list of tutors that are currently on the system, you can search a tutor
                                    by typing any keywords in the input box at the top right corner of the table.</p>
                                <hr>
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                                Status: 
                                <select name="status" class="form-control" onchange="this.form.submit()" >
                                    <?php  foreach($programStatus as $stat):  ?>
                                        <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?php if($status == "First"): echo "1"; else: echo $stat; endif; ?>"><?= $stat ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date Registered</th>
                                        <th>Training</th>
                                        <th>Created By</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tutors as $tutor): ?>
                                        <tr>
                                            <td>
                                            <?php if($tutor->type == "Applicant"):
                                               echo $this->Html->link(
                                                $tutor->first_name . ' ' . $tutor->last_name,
                                                array('controller' => 'trainings', 'action' => 'applicantView',$tutor->token),
                                                ['class' => '', 'target' => '_blank']
                                            ); 
                                            else:
                                                echo $this->Html->link(
                                                $tutor->first_name . ' ' . $tutor->last_name,
                                                array('controller' => 'tutors', 'action' => 'tutorView',$tutor->token),
                                                ['class' => '', 'target' => '_blank']
                                            );
                                            endif;
                                            
                                            ?></td>
                                            <td><?= date_format($tutor->created, 'Y/m/d') ?></td>                                         
                                            <td> 
                                                <?php
                                                    if(!empty($tutor->module_user)):
                                                        echo $tutor->module_user->status . " " . $tutor->module_user->position ;
                                                    else:
                                                        echo "None";
                                                    endif;
                                                ?>
                                            </td>
                                            <td><?= $tutor->owner->first_name ?> <?= $tutor->owner->last_name ?></td>     
                                            <td>
                                                <div class="btn-group">
                                                <?php
//                                                    if(!empty($tutor->module_user)):

                                                        echo $this->Html->link(" View Training ",
                                                            array('controller' => 'trainings', 'action' => 'view', $tutor->id),
                                                            ['class' => 'btn btn-sm btn-info']
                                                        );
                                                        
                                                        if($tutor->module_user->status == "Completed Orientation"):
                                                            echo $this->Html->link(" Activate Contract Review & Expectations ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Contract Review & Expectations"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        elseif($tutor->module_user->status == "Completed Contract Review & Expectations"):
                                                            echo $this->Html->link(" Activate Platform Training ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Platform Training"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        elseif($tutor->module_user->status == "Completed Platform Training"):
                                                            echo $this->Html->link(" Activate Tutor Training ",
                                                                array('action' => 'activate', $tutor->module_user->id, "Pending Tutor Training"),
                                                                ['class' => 'btn btn-sm btn-success']
                                                            );
                                                        endif;
                                                                                                            
//                                                    else:
//                                                        
//                                                        echo $this->Html->link(
//                                                            "Activate Training",
//                                                            array('controller' => 'tutors', 'action' => 'activate-training', $tutor->token),
//                                                            ['class' => 'btn btn-sm btn-primary']
//                                                        );
//                                                        
//                                                                                                          
//                                                    endif;
                                                ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php  endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Recruitment Page -->
           
            
        </div>
        
    </div>
</div>
<script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#wwc-number-empty':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Working With Children Number is empty!');
            break;
        
    }

});
</script>
<?= $this->Html->script('admin/training.js'); ?>
<?php //debug($tutors->toArray()); ?>