<?php $moduleStat = ($moduleDetails->status == "Pending") ? 4 : $moduleDetails->status ; ?>
<style>
    .answerInfo{
        padding-left: 60px;
        padding-bottom:20px;
    }
    .idealAnswer{
        padding-left: 60px;
        font-style: italic;
    }
</style>
<div class="widget-box">
<div class="widget-title">
    <div class="row">
        <div class="col-sm-6 col-md-6">
            <strong><h2>Training for <?= $moduleDetails->tutor->first_name ?> <?= $moduleDetails->tutor->last_name ?></h2></strong>
        </div>
        <div class="col-sm-6 col-md-6">
        <?php 
//                          debug($moduleDetails);                          
            if($moduleDetails->status == "Completed Orientation"):
                echo $this->Html->link(" Activate Contract Review & Expectations ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Contract Review & Expectations"),
                    ['class' => 'btn btn-primary pull-right']
                );
            elseif($moduleDetails->status == "Completed Contract Review & Expectations"):
                echo $this->Html->link(" Activate Platform Training ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Platform Training"),
                    ['class' => 'btn btn-primary pull-right']
                );
            elseif($moduleDetails->status == "Completed Platform Training"):
                echo $this->Html->link(" Activate Tutor Training ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Tutor Training"),
                    ['class' => 'btn btn-primary pull-right']
                );
            endif;
        ?>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                

                    <?php for($y = 1; $y <= 10; $y++): ?>
                    
                    <?php // if($moduleStat == "Done" || (!empty($moduleDetails) && $moduleStat >= $y)): ?>
                    <div <?php $stat = (!empty($moduleDetails) && $moduleStat == $y) ? "active" : $info = ( $moduleStat == "Done" && $y == 8 ) ? "active" : "" ; ?> class="tab-pane fade <?= $stat ?> in" id="module<?= $y ?>"> 
                        
                        <?php 
                            $access = ($moduleStat != $y || $moduleDetails->status == "Pending" || $moduleStat == "Done") ? "readonly" : "" ;
                                
                            $x = 0;
                            foreach($moduleDetails->module_user_questions as $userQuestion): 
                                if($userQuestion->module_id == $y):
                                    if($x == 0): $x++;
                        ?>
                                        <div class="row">

                                            <div class="col-md-12">
                                                <h3>Module <?= $y ?> Overview: <?= $userQuestion->module->overview ?></h3>     
                                                <h4>Download PDF: <a href='//<?= $_SERVER['SERVER_NAME'] . '/webroot/web/viewer.html?file=training/' . $userQuestion->module->pdf ?>'
                               target='_blank' class='btn btn-xs btn-success'>View Module</a></h4>                           
                                            </div>
                                            <div class="col-md-12">
                                            </div>

                                        </div>
                                    <?php endif; ?>

                             
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="panel-title"><?= $x ?>. <?= $userQuestion->questions ?></h3>                                        
                                    </div>
                                    <?php if($userQuestion->type == "statement"): ?>
                                    <div class="col-md-12 idealAnswer">
                                        Ideal Answer: <?= $userQuestion->answer ?>
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-md-12 answerInfo">
                                        <strong>Answer: <?= $userQuestion->user_answer ?></strong>
                                    </div>
                                </div>
                        
                                    
                        <?php     $x++;       
                                endif; 
                            endforeach; 
                            
                            
                        ?>
                        
                    </div>
                    <?php // endif; ?>
                    
                    
                    <?php endfor; ?>


        <?php 
//                          debug($moduleDetails);                          
            if($moduleDetails->status == "Completed Orientation"):
                echo $this->Html->link(" Activate Contract Review & Expectations ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Contract Review & Expectations"),
                    ['class' => 'btn btn-primary pull-right']
                );
            elseif($moduleDetails->status == "Completed Contract Review & Expectations"):
                echo $this->Html->link(" Activate Platform Training ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Platform Training"),
                    ['class' => 'btn btn-primary pull-right']
                );
            elseif($moduleDetails->status == "Completed Platform Training"):
                echo $this->Html->link(" Activate Tutor Training ",
                    array('action' => 'activate', $moduleDetails->id, "Pending Tutor Training"),
                    ['class' => 'btn btn-primary pull-right']
                );
            endif;
        ?>
                        
            </div>
        </div>

    </div>
</div>
</div>

<?php 
//echo $moduleDetails->status . "sesniel";
//        debug($moduleDetails);
//        debug($moduleUserQuestionDetails);
?>