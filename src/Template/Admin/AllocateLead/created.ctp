<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Client Created</strong></h2>
        </div>
        <hr class="customHr">
        <div class="widget-container table-responsive">
            <?php foreach ($client as $clientDetail): ?>
                <div class="row diffMarg center-table">
                    <div class="row">
                        <div class="col-md-11">
                            <h4>Student &amp; Tutor Details</h4>
                            <p>These are prefilled based on
                                <strong><?= $clientDetail->first_name . ' ' . $clientDetail->last_name ?></strong>
                                details. Select a student and tutor combination and nominate one or more subjects that
                                the
                                tutor is responsible for tutoring. Click <u>'Add Lead'</u> to specify a tutor for a
                                second/other student or
                                another tutor for the same student tutoring different subjects.</p>
                        </div>
                        <div class="col-md-1">
                            <div class="btn-group pull-right">
                                <?php
                                echo $this->Html->link(
                                    "Skip Allocation",
                                    array('controller' => 'dashboard', 'action' => 'index'),
                                    ['class' => 'btn btn-xs btn-warning pull-right']
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <?= $this->Form->create(null, ['url' => ['action' => 'saveLeads']]); ?>
                    <div class="row col-md-12 top-buffer container">
                        <?= $this->Form->hidden('client_token', ['value' => $clientDetail->token]); ?>
                        <table id="add-lead-table" class="table table-hover">
                            <thead>
                            <tr>
                                <td>Student</td>
                                <td>Tutor</td>
                                <td>Subject</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="subjectRow1" class="clonedInput">
                                <td>
                                    <select name='students[]' class="form-control student" required>
                                        <option selected disabled>- - Select Student - -</option>
                                        <?php foreach ($clientDetail->students as $student) : ?>
                                            <option
                                                value='<?= $student->id ?>'><?= $student->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name='tutors[]' class="form-control" required>
                                        <option selected disabled>-- Please select a Tutor --</option>
                                        <?php if ($this->request->session()->read("type") == "Admin") {
                                            foreach ($tutors as $tutor) : ?>
                                                <option
                                                    value='<?= $tutor->token ?>'><?= $tutor->first_name . ' ' . $tutor->last_name ?></option>
                                            <?php endforeach;
                                        } else {
                                            foreach ($tutors as $tutor) : if ($this->request->session()->read("aimhigh_tutor_id") == $tutor->tutor_id) { ?>
                                                <option
                                                    value='<?= $tutor->tutor_id ?>'><?= $tutor->tutor_fname . ' ' . $tutor->tutor_lname ?></option>
                                            <?php } endforeach;
                                        } ?>
                                    </select>
                                </td>
                                <td class="studentSubj">
                                    <select id="samSub" class="form-control lead-subjects"
                                            multiple>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="hidden">
                            <?php foreach ($clientDetail->students as $key => $std) : ?>
                                <select id="<?= $std->id ?>" name="subjects<?= $std->id ?>[]" class="form-control"
                                        multiple>
                                    <?php foreach ($std->student_subjects as $stdSubj) : ?>
                                        <option
                                            value='<?= $stdSubj->subject->id ?>'><?= $stdSubj->subject->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endforeach; ?>
                            <select id="samSubCop" class="form-control lead-subjects"
                                    multiple>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="form-group col-md-6 text-center">
                            <div class="btn-group">
                                <button id="btn-remove" class="btn btn-default" type="button">Delete Lead
                                </button>
                                <button id="btn-add" class="btn btn-default" type="button">Add Lead</button>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-center">
                            <button type="submit" id="cl-submit-lead-btn"
                                    class="btn btn-default">Save Lead/s
                            </button>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/allocate-tutor-created.js'); ?>