<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Lookup Client</strong></h2>
        </div>
        <hr class="customHr">
        <div class="widget-container table-responsive">
            <div class="row diffMarg center-table">
                <div class="row">
                    <div class="col-md-12">
                    <p><strong>Note: </strong>If you wish to Skip the consultation process you may check the skip allocation checkbox</p>
                    </div>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'saveLeads']]); ?>
                <div class="row col-md-12 top-buffer">
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-5 top-buffer">
                            <p class="text-right"><strong>Please Select or Type Client name to populate
                                    Students: </strong></p>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-8 text-left">
                                    <input id="clientNameSInput" list="clientName"
                                           placeholder="Client Name" class="form-control"
                                           required>
                                    <datalist id='clientName'>
                                        <?php foreach ($clients as $clist) : ?>
                                            <option
                                                data-value='<?= $clist->token ?>'
                                                value='<?= $clist->first_name . ' ' . $clist->last_name ?>'/>
                                        <?php endforeach; ?>
                                    </datalist>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="search-client-btn" class="btn btn-info">
                                        Populate
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            
                        </div>
                    </div>
                </div>
                <input type="hidden" id="clientToken" name="client_token" value=""/>
                <div class="row col-md-12">
                    <hr>
                    <h4>Student &amp; Tutor Details</h4>
                    <p>These are prefilled based on
                        <strong></strong>
                        details. Select a student / tutor combination and nominate one or more subjects that the
                        tutor is responsible for tutoring. Click <u>'Add Lead'</u> to specify a tutor for a
                        second/other student or
                        another tutor for the same student tutoring different subjects.</p>
                    <hr>
                </div>
                <div class="row col-md-12 top-buffer container">
                    <table id="add-lead-table" class="table table-hover">
                        <thead>
                        <tr>
                            <td>Student</td>
                            <td>Tutor</td>
                            <td>Subject</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="subjectRow1" class="clonedInput">
                            <td>
                                <select name='students[]' class="form-control students" required>
                                    <option selected disabled>- - Select Student - -</option>
                                </select>
                            </td>
                            <td>
                                <select name='tutors[]' class="form-control tutors" required>
                                    <option selected disabled>-- Please select a Tutor --</option>
                                    <?php foreach ($tutors as $tutor): ?>
                                        <option
                                            value='<?= $tutor->token ?>'><?= $tutor->first_name . ' ' . $tutor->last_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td class="studentSubj">
                                <select id="samSub" class="form-control lead-subjects"
                                        multiple>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="hidden">
                    <select class="form-control subjects" multiple></select>
                    <select id="decoy" class="form-control" multiple></select>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-6 text-center">
                        <div class="btn-group">
                            <button id="btn-remove" class="btn btn-danger" type="button">Delete Student
                            </button>
                            <button id="btn-add" class="btn btn-primary" type="button">Add Student</button>
                        </div>
                    </div>
                    <div class="form-group col-md-6 text-center">
                        <button type="submit" id="save-allocation-btn"
                                class="btn btn-success">Allocate Tutor & Create Consultation
                        </button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/allocate-tutor.js'); ?>
