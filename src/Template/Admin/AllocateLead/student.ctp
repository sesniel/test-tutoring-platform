<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Allocate Student</strong></h2>
        </div>
        <hr class="customHr">
        <div class="widget-container table-responsive">
            <?php foreach ($student as $studentDetail): ?>
                <div class="row diffMarg center-table">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Student &amp; Tutor Details</h4>
                            <p>Please fill up the Tutor and the subjects details below.</p>
                        </div>
                    </div>
                    <hr>
                    <?= $this->Form->create(null, ['url' => ['action' => 'saveLeads']]); ?>
                    <div class="row col-md-12 top-buffer container">
                        <?= $this->Form->hidden('client_token', ['value' => $studentDetail->client->token]); ?>
                        <div class="row center-indent">
                        <table id="add-lead-table" class="table table-hover">
                            <thead>
                            <tr>
                                <td>Student</td>
                                <td>Tutor</td>
                                <td>Subject</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="subjectRow1" class="clonedInput noDesign">
                                <td>
                                    <?= $this->Form->hidden('students[]', ['value' => $studentDetail->id]) ?>
                                    <input type="text" class="form-control students" value='<?= $studentDetail->name ?>' required disabled>
                                </td>
                                <td>
                                    <select name='tutors[]' class="form-control" required>
                                        <option selected disabled>-- Please select a Tutor --</option>
                                        <?php foreach ($tutors as $tutor) : ?>
                                            <option
                                                value='<?= $tutor->token ?>'><?= $tutor->first_name . ' ' . $tutor->last_name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td class="studentSubj">
                                    <select id="primarySelected" name="subjectRow1[]"
                                            class="form-control"
                                            multiple>
                                        <?php foreach ($studentDetail->student_subjects as $subjects) : ?>
                                            <option
                                                value='<?= $subjects->subject->id ?>'><?= $subjects->subject->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="hidden">
                            <select id="subjectList" class="form-control" multiple>
                                <?php foreach ($studentDetail->student_subjects as $subjects) : ?>
                                    <option
                                        value='<?= $subjects->subject->id ?>'><?= $subjects->subject->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="form-group col-md-6 text-center">
                            <div class="btn-group">
                                <button id="btn-remove" class="btn btn-danger" type="button">Delete Lead
                                </button>
                                <button id="btn-add" class="btn btn-primary" type="button">Add Lead</button>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-center">
                            <button type="submit" id="save-allocation-btn"
                                    class="btn btn-default">Save Lead/s
                            </button>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    </div>
</div>
<?= $this->Html->script('admin/allocate-tutor-student.js'); ?>