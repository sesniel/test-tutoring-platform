<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Re-allocate Tutor</strong></h2>
        </div>
        <hr class="customHr">
        
      
        <div class="widget-container table-responsive">
            <div class="row diffMarg center-table">
                 
                <div class="row center-content">
                    <!--
                    <div class="col-md-12">
                    <p><strong>Note: </strong>If you wish to Skip the consultation process you may check the skip allocation checkbox</p>
                    </div>
                </div>
                
                <div class="row col-md-12 top-buffer">
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-5 top-buffer">
                            <p class="text-right"><strong>Please Select or Type Client name to populate
                                    Students: </strong></p>
                        </div>
                       
                    <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-8 text-left">
                                    <input id="clientNameSInput" list="clientName"
                                           placeholder="Client Name" class="form-control"
                                           required>
                                    <datalist id='clientName'>
                                        <?php foreach ($clients as $clist) : ?>
                                            <option
                                                data-value='<?= $clist->token ?>'
                                                value='<?= $clist->first_name . ' ' . $clist->last_name ?>'/>
                                        <?php endforeach; ?>
                                    </datalist>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="search-client-btn" class="btn btn-info">
                                        Populate
                                    </button>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                            
                        </div>
                    </div>
                </div>
                -->
                <?= $this->Form->create(null, ['url' => ['action' => 'saveNewLeadsReallocate', 'method' => 'post']]); ?>
                
                <input type="hidden" id="clientToken" name="client_token" value="<?= $leadDetails->client->token ?>"/>
                <input type="hidden" id="tutorId" name="tutor_id" value="<?= $leadDetails->tutor->id ?>"/>
                <input type="hidden" id="leadId" name="lead_id" value="<?= $leadDetails->id ?>"/>
                <input type="hidden" id="consultationId" name="consultation_id" value="<?= $leadDetails->consultation->id ?>"/>
               
                <div class="row center-content">
                    <div class="col-md-3">
                        <h4>Consultation Date and Time:</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group"><input class="form-control conDate tip" id="conDate" type="text" name="conDate" data-toggle="tooltip" value="" required <?php //if($consultation->status == 'Completed Consultation') echo 'readonly'; ?>><span class="input-group-addon conDateIcon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group"><input class="form-control" id="conTime" type="text" name="conTime" value="" required <?php //if($consultation->status == 'Completed Consultation') echo 'readonly'; ?>><span class="input-group-addon conTimeIcon"><i class="glyphicon glyphicon-time"></i></span></div>
                    </div>
                    
                </div>
                <br>
                <div class="row col-md-12">
                    <div class="col-md-3">
                        <h4>Student &amp; Tutor Details</h4>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer container">
                    <table id="add-lead-table" class="table table-hover">
                        <thead>
                        <tr>
                            <td>Student</td>
                            <td>Tutor</td>
                            <td>Subject</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="subjectRow1" class="clonedInput">
                            <td>
                                <select name='students[]' class="form-control students" required>
                                    <?php foreach($clients as $client): ?>
                                        <?php foreach($client->students as $student): ?>
                                        <option value="<?= $student->id ?>"><?= $student->name; ?></option>
                                   <!--  <option selected
                                        value="<?= $studentId ?>"><?= $studentName ?></option> -->
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                    
                                </select>
                            </td>
                            <td>
                                <select name='tutors[]' class="form-control tutors" required>
                                    <option selected disabled>-- Please select a Tutor --</option>
                                    <?php foreach ($tutors as $tutor): ?>
                                        <option
                                            value='<?= $tutor->token ?>'><?= $tutor->first_name . ' ' . $tutor->last_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td class="studentSubj">
                                <select id="samSub" class="form-control lead-subjects" name="subjectRow1[]"
                                        multiple>
                                    <?php foreach($subjects as $subject): ?>
                                        <?php foreach($subject->student_subjects as $subj):  ?>
                                    <option value="<?= $subj->subject->id ?>"><?= $subj->subject->name ?></option>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                    
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="hidden">
                    <select class="form-control subjects" multiple></select>
                    <select id="decoy" class="form-control" multiple></select>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-6 text-center">
                        <div class="btn-group">
                            <button id="btn-remove" class="btn btn-danger" type="button">Delete Lead
                            </button>
                            <button id="btn-add" class="btn btn-primary" type="button">Add Lead</button>
                        </div>
                    </div>
                    <div class="form-group col-md-6 text-center">
                        <button type="submit" id="save-allocation-btn"
                                class="btn btn-success">Save Lead/s
                        </button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('admin/reallocate-tutor.js'); ?>
<?= $this->Html->script('admin/editConsultation.js'); ?>
