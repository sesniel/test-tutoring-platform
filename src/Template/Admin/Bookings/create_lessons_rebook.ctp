<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Create Booking Program</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <?= $this->Form->create(null, ['url' => ['action' => 'generateLessonRebook', $this->request->params['pass'][0]]]); ?>
                <?php if($lead_id){ echo $this->Form->hidden('lead_id', ['value' => $lead_id]);} ?>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12 top-buffer">
                        <p> Please fill up the program table. Note: Type is populated by the assessments completed by the students </p>
                    </div>
                    <div class="col-md-12">
                        <table class="tbll table table-bordered">
                            <thead>
                            <tr>
                                <td>Date</td>
                                <td>Time</td>
                                <td>Length(hours)</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="leadRow1" class="clonedInput">
                                <td>
                                    <div class="input-group"><input class="form-control ldate" id="ldate"
                                                                    type="text" name="lessonDates[]" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span></div>
                                </td>
                                <td>
                                    <div class="input-group"><input class="form-control lessonTime" id="lessonTime"
                                                                    type="text" name="lessonTime[]" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-time"></i></span></div>
                                </td>
                                <td>
                                    <input type="text" name="length[]" class="form-control" maxlength="5" required/>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6" class="text-center">
                                    <div class="btn-group">
                                        <button type="button" id="btn-remove" class="btn btn-danger btn-sm">Remove Row
                                        </button>
                                        <button type="button" id="addRowBtn" class="btn btn-success btn-sm">Add Row
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-12 text-center">
                        <button id="cpBtn" class="btn btn-default">Create Booking</button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php // debug($programDetails); ?>
    


