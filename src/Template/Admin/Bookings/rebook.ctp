<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Rebooking</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row center-content">
                    <div class="col-md-12">
                        <table id="bookingsTable"
                                class="table table-striped responsive display table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Students</th>
                                        <th>Status</th>
                                        <th>Remaining Lessons</th>
                                        <th>Lesson's Last Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($programDetails as $key => $program): 
                                            
                                            if(count($program->rebook_lessons) <= 2):
                                    ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?php echo ucfirst($program->client->first_name) . " " . ucfirst($program->client->last_name); ?></td>
                                            <td><?php echo ucfirst($program->tutor->first_name) . " " . ucfirst($program->tutor->last_name); ?></td>
                                            <td>
                                                <?php 
                                                if($program->id <= 183):
                                                    echo ucfirst($program->program_students[0]->student->name);
                                                else:
                                                    foreach($program->program_students as $key => $student):
                                                        if((count($program->program_students) - 1) == $key):
                                                            echo ucfirst($student->student->name);
                                                        else:    
                                                            echo ucfirst($student->student->name) . ", ";
                                                        endif;
                                                    endforeach; 
                                                endif;
                                                ?>
                                                
                                            </td>
                                            <td><?php  echo $program->status; ?>
                                            </td>
                                            <td><?= count($program->rebook_lessons) ?></td>
                                            <td><?php 
                                                if(isset($program->rebook_lessons[0]->date)):
                                                    echo date("Y-m-d", strtotime($program->rebook_lessons[0]->date));
                                                endif;
                                            ?></td>
                                            <td>
                                                <?php
                                                echo $this->Html->link(
                                                    "Details",
                                                    array('controller' => 'bookings', 'action' => 'details',$program->id),
                                                    ['class' => 'btn btn-xs btn-warning', 'target'=>'_blank']
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Discontinued",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Discontinued"),
                                                    ['class' => 'btn btn-xs btn-danger', 'onclick' => "return confirm('Are you sure you want to discontinue this booking?')"]
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Rebooked",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Rebooked"),
                                                    ['class' => 'btn btn-xs btn-primary', 'target'=>'_blank']
                                                );
                                                ?>
                                            </td>
                                        </tr>
                                    
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function () {

    $('#bookingsTable').dataTable({"autoWidth": false,"order": [1, 'asc']});
                

});
</script>