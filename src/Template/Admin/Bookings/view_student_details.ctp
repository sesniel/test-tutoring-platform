<div id="studentDetailsDia" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Students:</h4>
            </div>
            <div class="modal-body row">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">List of students under the same consultation.</p>
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="center-content">
                        <table id="studentTbl" class="responsive display table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Year</th>
                                    <th>Subject/s</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $btn = "";
                                    $studentInfo = "";
                                foreach ($leads as $lead): ?>
                                    <tr>
                                        <td><?= $lead->student->name ?></td>
                                        <td><?= $lead->student->year_level->name ?></td>
                                        <td> 
                                            <?php
                                                $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                            echo implode(", ", $subj); ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <?php
                                                    $studentInfo .= "<input type='hidden' name='studentInfo' value='$lead->student_id' />";
                                                    if($btn == ""):
                                                        $btn = $this->Html->link("Create Booking", ['controller' => 'bookings', 'action' => 'autoGenProgram', $lead->id], ['escape' => false, 'class' => "btn btn-xs btn-success pull-right"]);
                                                    endif;
                                                    echo $this->Form->button('Re-allocate', ['type' => 'button', 'class' => 'btn btn-sm btn-warning reallocateBtn', 'value' => $lead->id . '/' . $lead->student_id]);
                                                    echo $this->Form->button('Abandon', ['type' => 'button', 'class' => 'btn btn-sm btn-danger abandonBtn', 'value' => $lead->id]);
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                    <tr>
                                        <td colspan="4">
                                            <?php
                                                echo $btn;
                                            ?>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        
       $('.reallocateBtn').click( function () {
            var values = $(this).val(),
                arr = values.split('/'),
                lead_id = arr[0],
                student_id = arr[1],
                reallocateCont = $("#reallocate-container");

            $.post('/admin/bookings/reallocate', {"lead_id": lead_id,"student_id": student_id }, function (data) {
                reallocateCont.html(data);
                $('#reallocate-dia').modal('show');
            });
        });
       
        $('.abandonBtn').click( function () {
            var lead_id = $(this).val(),
                abandonCont = $("#abandon-container");

            $.post('/admin/bookings/abandon', {"lead_id": lead_id}, function (data) {
                abandonCont.html(data);
                $('#abandonDia').modal('show');
            });
        });
        
    });
</script>