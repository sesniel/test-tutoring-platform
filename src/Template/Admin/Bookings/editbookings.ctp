<?php $bookingStatus = array("Pending", "In Progress", "Completed", "Cancelled"); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Booking</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <?= $this->Form->create(null, ['url' => ['action' => 'updateProgram', $programDetails->id], 'id' => 'checkSubmit']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <h3><strong>Client:</strong> <?= $programDetails->client->first_name ?> <?= $programDetails->client->last_name ?></h3>
                    </div>
                    <div class="col-md-6">
                        <h3><strong>Tutor:</strong> <?= $programDetails->tutor->first_name ?> <?= $programDetails->tutor->last_name ?></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h5><strong>Status:</strong> </h5>
                        <select name='status' id='status' class='form-control'>
                        <?php 
                            foreach($bookingStatus as $stats): 
                        ?>
                            <option <?php if($programDetails->status == $stats): echo "selected"; endif; ?> 
                                value='<?= $stats ?>' ><?= $stats ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <h5><strong>Rebook:</strong> </h5>
                        <select name='rebook_status' class='form-control'>
                            <option selected disabled value='' ></option>
                            <option <?php if($programDetails->rebook_status == "Rebooked"): echo "selected"; endif; ?> 
                                value='Rebooked' >Rebooked</option>
                            <option <?php if($programDetails->rebook_status == "Discontinued"): echo "selected"; endif; ?> 
                                value='Discontinued' >Discontinued</option>
                        </select>
                        <br/>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h5><strong>Invoice Number:</strong> </h5>
                        <input type="text" value='<?= $programDetails->invoice_number ?>' class='form-control' name='number' />
                    </div>
                    <div class="col-md-4">
                        <h5><strong>Invoice Amount:</strong> </h5>
                        <input type="text" value='<?= $programDetails->invoice_amount ?>' class='form-control' name='amount' />
                    </div> 
                    <div class="col-md-4">
                        <h5><strong>Rate:</strong> </h5>
                            <select name="rate_id" id="rate_name" class="form-control">
                            <?php 
                                foreach($rateDetails as $rate): 
                                    
                                    if($programDetails->rate->id == $rate->id):
                                        $sel = "selected";
                                    else:
                                        $sel = "";
                                    endif;
                                    
                            ?>
                                <option <?= $sel ?> value="<?= $rate->id ?>"><?= $rate->name ?></option>
                            <?php endforeach; ?>
                            </select>
                        <?php // debug($programDetails->toArray()) ?>
                    </div> 
                    <div class="col-md-12">
                        <h5><strong>Booking Note:</strong> </h5>
                        <textarea class="form-control" rows="3" name="note" ><?= htmlspecialchars($programDetails->note) ?></textarea>
                    </div>              
                    <div class="col-md-12">
                        <div class="form-group text-center">
                            <br/><br/>
                            <button id="cpBtn" class="btn btn-default">Update Booking Information</button>
                        </div>
                    </div>
                </div>
            </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        
        $('#checkSubmit').submit(function( event ) {
            
            var status = $('#status').val();
            if(status == "Cancelled"){
                if (confirm("Before cancelling a booking, please ensure that you have updated any required invoices or issued any credit notes as necessary and that tutors have submitted all pending session reports for this booking. Once cancelled they'll be unable to submit session reports.")){
                    return true; // Allow the form to submit
                }else{
                    return false; // Stop the form submitting
                }
            }
            
            
        });
        
    });
</script>

<?php // debug($programDetails); ?>
    


