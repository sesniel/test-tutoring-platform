<?php
function getYearLevelInfo($yearLevelId){
    if($yearLevelId == 1):
        return "F";
    else:
        return $yearLevelId - 1;
    endif;
}

$programStatus = array('All', 'Cancel', 'Done', 'In Progress', 'Pending', 'Tutor');
$bookingStatus = array('All', 'New','Pending Consultation','Completed Consultation', 'Booked', 'Abandoned');

?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Uninvoiced Bookings</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <?php //debug($leads->toArray()); ?> 
                <div class="row">
                    <div class="col-md-12"></div>
                    <div class="col-md-6">
                        <h4>Bookings Information</h4>
                    </div>
                    <div class="col-md-6 statusPos">
                        <?php
                        echo $this->Form->create(null, [
                            'url' => ['action' => 'uninvoice_view'],
                            'method' => 'post'
                            ]);
                            ?>
                            
                            <label>Status: </label>
                            <select name="status" class="form-control" onchange="this.form.submit()" >
                                <?php 
                                foreach($programStatus as $stat): 
                                    ?>
                                <option <?php $sel = ($status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?php if($stat == "Tutor"):  echo "Scheduled";  elseif($stat == "Done"):  echo "Completed";  else: echo $stat; endif;  ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <table id="bookingsTable"
                                class="table table-striped responsive display table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Amount</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($programDetails as $program): ?>
                                    <?php if (empty($program->invoice_number)): ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?= $program->invoice_number ?></td>
                                            <td><?= $program->invoice_amount ?></td>
                                            <td><?php
                                                if(!empty($program->client)):
                                                    echo $program->client->first_name . " " . $program->client->last_name;
                                                endif;
                                             ?></td>
                                            <td><?= $program->tutor->first_name ?> <?= $program->tutor->last_name ?></td>
                                            <td>    
                                                <?php 
                                                    if($program->status == "Tutor"):
                                                        echo "Scheduled";
                                                    elseif($program->status == "Done"):
                                                        echo "Completed";
                                                    else:
                                                        echo $program->status;
                                                    endif; 
                                                ?>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <?php if($program->status == "Pending"): ?>
                                                        <?= 
                                                        $this->Html->link(
                                                            "Create Lesson",
                                                            ['action' => 'createLessons', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary"]); 
                                                            ?>
                                                        <?php else: ?>
                                                            <?= 
                                                            $this->Html->link(
                                                                "View Details",
                                                                ['action' => 'details', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-warning"]); 
                                                                ?>
                                                            <?php endif; ?>
                                                            <?= 
                                                            $this->Html->link(
                                                                "Delete",
                                                                ['action' => 'deleteProgram', $program->id, 'prefix' => 'admin'], ['escape' => false, 
                                                                    'class' => "btn btn-xs btn-danger", 
                                                                    'onclick' => "return confirm('Are you sure you want to delete this booking?')"]); 
                                                                ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                        <?php endif; ?>
                                                <?php endforeach; ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consultation-container"></div>
<div id="student-details"></div>
<div id="reallocate-container"></div>
<div id="abandon-container"></div>
<script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#consultation-updated':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Consultation updated successfully.');
            break;
    }

});
        $('#consultationsTbl').on('submit', '.changeStatusBtn', function () {
        var token = $(this).val();

        $('#consultation-updated').find('#consultation-updated').val($(this).val());

    }); 
</script>
<?= $this->Html->script('admin/bookings3.js'); ?>