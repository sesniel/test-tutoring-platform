<style>
    .standard{
        width:160px;
    }
    .standard1{
        width:160px;
    }
    .form-control{
        height:30px !important;
    }
</style>
<?php
    $bookingStatus = array('Schedule','Completed','Cancel','Pending'); asort($bookingStatus);
    $programStatus = array('Schedule','Completed','Cancel','Pending'); asort($programStatus);
?>
<div class="row"> 
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Lesson Details</strong></h2>
            </div>
            <hr class="customHr">
            <div class="row">
                <div class="col-sm-6 col-md-6"></div>
                <div class="col-sm-6 col-md-6 pull-right">
                    <?php
                    echo $this->Form->create(null, [
                        'url' => ['action' => 'lessons'],
                        'method' => 'post'
                    ]);
                    ?>
                    Client:
                    <select name="id" class="form-control" onchange="this.form.submit()" >
                        <option selected disabled >Select Client</option>
                        <?php  foreach ($client as $det): ?>
                        <option <?php $sel = ($selectedClient == $det->id ? "selected" : ""); ?> value="<?= $det->id ?>" <?= $sel ?> ><?php echo ucfirst($det->first_name) . " " . ucfirst($det->last_name) ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo $this->Form->end(); ?>
                </div> 
            </div>
            <?Php 
                if(!empty($clientDetails)): 
                
                    echo $this->Form->create(null, [
                        'url' => ['action' => 'updateLessonDetails',$selectedClient],
                        'method' => 'post'
                    ]);
            ?>
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="questionsTbl"
                                       class="table table-striped responsive display table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <td colspan="5" ><div class='pull-left' >Client: <?= $clientDetails->first_name . " " . $clientDetails->last_name ?></div></td>
                                            <td colspan="4"><div class='pull-right' ><input type="submit" value="Update Lesson" class="btn btn-xs btn-primary" /></div></td>
                                        </tr>
                                        <tr>
                                            <?php
                                                $displayStudent="";
                                                $countStudents = count($clientDetails->students) - 1;
                                                foreach($clientDetails->students as $key => $stdnt):
                                                    if($countStudents == $key):
                                                        $displayStudent .= ucwords($stdnt->name);
                                                    else:    
                                                        $displayStudent .= ucwords($stdnt->name) . ", ";
                                                    endif;
                                                endforeach;
                                            ?>
                                            <td colspan="9"><div class='pull-left'>Student: <?= $displayStudent ?></div></td>
                                        </tr>
                                        <?php
                                        $display = ""; 
                                            if(!empty($clientDetails->programs)):
                                                foreach($clientDetails->programs as $program):
                                                    $progStats = $program->status ;
                                                    $display .= '<tr><td colspan="8">&nbsp;</td></tr>';
                                                    $display .= '<tr>';
                                                    $display .= '<td><div class="pull-left" >Booking Id: '.$program->id.'</div></td>';
                                                    $display .= '<td colspan="3"><div class="pull-left" >Tutor: '.$program->tutor->first_name . ' ' . $program->tutor->last_name .'</div></td>';
                                                    $display .= '<td colspan="4"><div class="pull-left" >Status: '.$progStats.'</div></td>';
                                                    $display .= '</tr><thead><tbody>';
                                                    $display .= '<tr>
                                                                    <th>Id</th>
                                                                    <th class="standard">Lesson Date</th>
                                                                    <th class="standard">Lesson Time</th>
                                                                    <th class="standard">Length</th>
                                                                    <th class="standard">Payroll Date</th>
                                                                    <th class="standard1">Status</th>
                                                                    <th>Created Date</th>
                                                                    <th>Last Modified</th>
                                                                </tr>';
                                                    
                                                    if(!empty($program->bookings)):
                                                        foreach($program->bookings as $bookings):
                                                        
                                                            
                                                            
                                                            $pDate = ""; 
                                                            if($bookings->payroll_date != "" ):
                                                                
                                                                $pDate = date('d/m/Y', strtotime($bookings->payroll_date));
                                                            
                                                            endif;
                                                            
                                                            $slctedBStats = $bookings->status;
                                                            
                                                            
                                                            $bStatsDisplay = '<select name="programStatus['.$bookings->id.']" class="form-control" >';
                                                            foreach ($bookingStatus as $bstat):                                                                
                                                                $sel = ($slctedBStats === $bstat ? "selected" : "");     
                                                                $bstatV = $bstat;
                                                                
                                                                $bStatsDisplay .= '<option value="'.$bstatV.'" '.$sel.' >'.$bstat.'</option>';
                                                            endforeach;
                                                            $bStatsDisplay .= '</select>';
                                                            
                                                            $display .= '<tr>';
                                                            $display .= '<td>'.$bookings->id.'</td>';
                                                            $display .= '<td><div class="input-group"><input class="form-control lessonDate" id="lessonDate'.$bookings->id.'"
                                                                                    type="text" name="lessonDates['.$bookings->id.']" value="'.date('d/m/Y', strtotime($bookings->date)).'" required><span class="input-group-addon">
                                                                                    <i class="glyphicon glyphicon-calendar"></i></span></div>
                                                                                    </td>';
                                                            $display .= '<td><div class="input-group"><input class="form-control lessonTime" id="lessonTime'.$bookings->id.'"
                                                                                                        type="text" name="lessonTime['.$bookings->id.']" value="'.date('h:i A', strtotime($bookings->time)).'" required><span
                                                                                class="input-group-addon"><i
                                                                                    class="glyphicon glyphicon-time"></i></span></div></td>';
                                                            $display .= '<td><input type="text" name="length['.$bookings->id.']" class="form-control" value="'.$bookings->length.'" maxlength="5" required/></td>';
                                                            $display .= '<td><div class="input-group"><input class="form-control payrollDate" id="payrollDate'.$bookings->id.'"
                                                                                    type="text" name="payrollDates['.$bookings->id.']" value="'.$pDate.'"><span
                                                                                    class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                                                                                    </td>';
                                                            $display .= '<td>'.$bStatsDisplay.'</td>';
                                                            $display .= '<td>'. date('d/m/Y', strtotime($bookings->created)) .'</td>';
                                                            $display .= '<td>'. date('d/m/Y', strtotime($bookings->modified)) .'</td>';
                                                            $display .= '</tr>';
                                                            
                                                            
                                                        endforeach;
                                                    endif;
                                                endforeach;
                                                
                                            endif;
                                            
                                                    $display .= '<tr><td colspan="8">&nbsp;</td></tr>';
                                                    
                                            echo $display;
                                        ?>
                                        <tr>
                                            <td colspan="9"><div class='pull-right' ><input type="submit" value="Update Lesson" class="btn btn-xs btn-primary" /></div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                echo $this->Form->end();
                endif; 
            ?>
        </div>
    </div>
</div>
<?php //debug($clientDetails->toArray()); ?>
<script>
    $(document).ready(function () {
       
        $('.lessonDate').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy',
            maxDate: "7m"
        });
        
        $('.payrollDate').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy',
            maxDate: "7m"
        });

        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        });
    });
</script>

