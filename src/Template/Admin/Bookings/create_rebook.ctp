<script>
$(function () {

    $('#studentSelect').multiSelect();

});
</script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Create Rebooking Lesson</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row">
                    <div class="col-md-12">
                        <p>Please finish up the form to proceed with lesson planning.</p>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-6">
                               <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'createRebook', $projectDetails->id],
                                        'method' => 'post'
                                    ]);
                                ?>
                                <div class="row form-group">
                                    <input type="hidden" name="program_id" value="<?= $projectDetails->id ?>" />
                                    <div class="col-md-3">
                                        <h5 for="tutor" class="text-right"><strong>Tutors:</strong></h5>
                                    </div>
                                    <div class="col-md-9">
                                        <select class="form-control tutor_id" id="tutor" name="tutor_id"
                                                onchange="this.form.submit()">
                                            <option selected disabled=""></option>
                                            <?php foreach ($tutorDetails as $tutor): ?>
                                                <option <?php if (!empty($selectedTutor) && $tutor->id == $selectedTutor->id): echo "selected"; endif; ?>
                                                    value="<?= $tutor->id ?>"><?= $tutor->first_name . " " . $tutor->last_name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                   
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <h5 for="client" class="text-right"><strong>Clients:</strong></h5>
                                    </div>
                                    <div class="col-md-9">
                                        <?php // echo $selectedClient->id . " --> "; debug($leadsDetails->toArray()); ?>
                                        <?php if (!empty($leadsDetails)): ?>
                                            <select class="form-control client_id" id="client" name="client_id"
                                                    onchange="this.form.submit()">
                                                <option selected disabled=""></option>
                                                <?php foreach ($leadsDetails as $client): ?>
                                                    <option <?php if ($projectDetails->client_id == $client->client_id): echo "selected"; endif; ?>
                                                        value="<?= $client->client->id ?>"><?= $client->client->first_name . " " . $client->client->last_name ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php 
                                    echo $this->Form->end();                                     
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'programRebook',$projectDetails->id],
                                        'method' => 'post'
                                    ]);
                                ?>
                                        
                                <?php if (!empty($selectedClient)): ?>
                                <div class="row form-group">
                                <div class="col-md-3">
                                    <input type="hidden" name="program_id" value="<?= $projectDetails->id ?>" />
                                    <h5 for="student" class="text-right"><strong>Rate:</strong></h5>
                                </div>
                                    <div class="col-md-9">
                                        <select name="rate_id" id="rate_name" class="form-control">
                                        <?php foreach($rateDetails as $rate): ?>
                                            <option <?php $sel = ($projectDetails->rate_id === $rate->id ? "selected" : ""); ?> <?= $sel ?> value="<?= $rate->id ?>"><?= $rate->name ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                <div class="col-md-3">
                                    <h5 for="student" class="text-right"><strong>Students:</strong></h5>
                                </div>
                                <div class="col-md-9">
                                    <input type='hidden' id="populate_tutor_id" name='tutor_id' class="tutor_id" value='<?= $selectedTutor->id ?>'/>
                                    <input type='hidden' id="populate_client_id" name='client_id' class="client_id" value='<?= $selectedClient->id ?>'/>

                                    <select class="form-control students" id="studentSelect" name="studentInfo[]" multiple>
                                        <?php foreach ($selectedClient->students as $student): ?>
                                            <option <?php //if ($selectedStudent == $student->id): 
                                                echo "selected"; 
                                            //endif; ?>
                                                value="<?= $student->id ?>"><?= $student->name ?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <div class="hidden">
                                        <select class="form-control students" id="studentSelect" multiple>
                                            <?php foreach ($selectedClient->students as $student): ?>
                                                <option <?php if ($selectedStudent == $student->id): echo "selected"; endif; ?>
                                                    value="<?= $student->id ?>"><?= $student->name ?> </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <select  class="form-control" multiple>
                                            <?php foreach ($studentDetail as $subjects) : ?>
                                                <option
                                                    value=''><?= $subjects ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="ppt" class="btn btn-success pull-right">
                                            Generate Booking
                                        </button>
                                        <hr>
                                    </div>
                                </div>
                                <?php endif; ?>
                                
                                
                            
                        </div>
                            <!-- -->
                            <div class="col-md-6">
                               
                                <div class="row form-group">
                                    <div class="col-md-3">
                                         <h5 for="tutor" class="text-right"><strong>Invoice Number:</strong></h5>
                                          
                                    </div>
                                    <div class="col-md-9">
                                       <input type="text" value='' class='form-control' name='number' /> 
                                    </div>
                                   
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                         <h5 for="tutor" class="text-right"><strong>Invoice Amount:</strong></h5>
                                          
                                    </div>
                                    <div class="col-md-9">
                                       <input type="text" value='' class='form-control' name='amount' /> 
                                    </div>
                                </div>
                               <?php
                                    echo $this->Form->end();
                                ?> 
                        </div>
                    </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
//debug($projectDetails->toArray());
    $studentDetail = array("math", "english", "science");
?>
<!--<div class="row">
    <div class='col-md-12'>
        
        <select id="studentSelect" name="subjectRow1[]"
                class="form-control"
                multiple>
            <?php foreach ($studentDetail as $subjects) : ?>
                <option
                    value='<?= $subjects ?>'><?= $subjects ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="hidden">
        <select id="subjectList" class="form-control" multiple>
            <?php foreach ($studentDetail as $subjects) : ?>
                <option
                    value=''><?= $subjects ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>       -->
