<?php
function getYearLevelInfo($yearLevelId){
    if($yearLevelId == 1):
        return "F";
    else:
        return $yearLevelId - 1;
    endif;
}

$programStatus = array('All', 'Active', 'Pending', 'In Progress', 'Completed', 'Cancelled');
$bookingStatus = array('All','Active', 'New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation');

?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Consultations</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row">
                    <div class="col-md-6">
                        <h4>Consultation Information</h4>
                    </div>
                    <div class="col-md-6 statusPos">
                        <?php
                        echo $this->Form->create(null, [
                            'url' => ['action' => 'consultations'],
                            'method' => 'post'
                            ]);
                            ?>
                           
                            <label>Status: </label>
                            <select name="booking" class="form-control" onchange="this.form.submit()" >
                                <?php foreach($bookingStatus as $stat): ?>
                                <option <?php $sel = ($Bookingstatus == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div><?php //debug($leads->toArray()); ?>
                <div class="row center-content">
                    <div class="col-md-12">
                        <table id="consultationsTbl"
                               class="table table-striped responsive display table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Client</th>
                                    <th>Tutor</th>
                                    <th>Date of Consultation</th>
                                    <th>Payroll Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($consultations as $key => $con): ?>
                                    <?php if(!empty($con->leads[0]->tutor)): ?>
                                    <tr>
                                        <td><?php echo $con->id; ?></td>
                                        <td><?php
                                            if(!empty($con->leads)):
                                                echo $con->leads[0]->client->first_name . ' ' . $con->leads[0]->client->last_name;
                                            endif;
                                         ?></td>
                                        <td><?php
                                                if(!empty($con->leads[0]->tutor)):
                                                    echo $con->leads[0]->tutor->first_name . ' ' . $con->leads[0]->tutor->last_name;
                                                endif;
                                             ?></td>
                                        <td><?php
                                        
                                            if($con->consultation_session_date != ""):
                                                echo date('Y/m/d', strtotime($con->consultation_session_date));
                                            endif;
                                        
                                         ?></td>
                                        <td><?php
                                        
                                            if($con->payroll_date != ""):
                                                echo date('Y/m/d', strtotime($con->payroll_date));
                                            endif;
                                           
                                            ?></td>
                                        <td><?= $con->status ?></td>
                                        <td>   
                                            <div class="btn-group">
                                                <?php
                                                echo $this->Html->link("View/Edit", array('controller' => 'dashboard', 'action' => 'editConsultation', $con->id), ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']);
                                                //echo $this->Form->button('Notes', ['type' => 'button', 'class' => 'btn btn-sm btn-info consultationView', 'value' => $con->id]);
                                                //echo $this->Form->button('Book/Reallocate/Abandon', ['type' => 'button', 'class' => 'btn btn-sm btn-warning studentDetail', 'value' => $con->id]);
                                                
                                                if($con->status != "Abandoned"):
                                                    echo $this->Form->button('Discontinue', ['type' => 'button', 'class' => 'btn btn-sm btn-danger consultationView', 'value' => $con->id]);
                                                    //echo $this->Html->link("Discontinue", array('controller' => 'bookings', 'action' => 'changeStatus', $con->id,'Abandoned'), ['class' => 'btn btn-sm btn-info discontinue', /* 'onclick' => "return confirm('Are you sure you want to Abandoned this consultation?')" */]);
                                                endif;
                                                
                                                echo $this->Html->link("Book",    array('controller' => 'bookings', 'action' => 'changeStatus', $con->id,'Booked'), ['class' => 'btn btn-sm btn-warning']);
                                                
                                                
                                                ?>  
                                            </div>
                                        </td>
                                    </tr>                                
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>
                                
<div id="consultation-container"></div>
<div id="student-details"></div>
<div id="reallocate-container"></div>
<div id="abandon-container"></div>
<script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#consultation-updated':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Consultation updated successfully.');
            break;
        case '#discontinued':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Client has been discontinued.');
            break;
    }

});
        $('#consultationsTbl').on('submit', '.changeStatusBtn', function () {
        var token = $(this).val();

        $('#consultation-updated').find('#consultation-updated').val($(this).val());

    }); 
</script>
<?= $this->Html->script('admin/bookings3.js'); ?>
