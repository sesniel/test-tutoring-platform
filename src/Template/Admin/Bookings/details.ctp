<style>
.hidden_studentInfo {
     display:none;
}
.forum-content {
    width:500px;
    height:auto;
    padding:5px 10px;
}
.comments-space {
    width:90%;
    min-height:50px;
    height:auto;
    border-radius:5px;
    margin-bottom: 5px;
    text-align: justify;
}
.remaining-content span {
    display:none;
}
</style>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Lesson Details for <?= $programDetails->client->first_name . " " . $programDetails->client->last_name ?> </strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive" style="padding-top: 0px;">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <?php 
                                    echo $this->Html->link("Edit Booking Information",
                                    array('action' => 'editbookings', $programDetails->id),
                                    ['class' => 'btn btn-xs btn-warning pull-left']
                                    );
                                ?>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <h5><strong>Tutor:</strong> <?= $programDetails->tutor->first_name ?> <?= $programDetails->tutor->last_name ?></h5>
                                    <h5><strong>Program ID:</strong> <?= $programDetails->id ?></h5>
                                    <hr>
                                </div>
                                <div class="col-md-2">
                                    <h5><strong>Invoice Number:</strong> <?= $programDetails->invoice_number ?></h5>
                                    <h5><strong>Invoice Amount:</strong>
                                        <?php 
                                            if($programDetails->invoice_amount != ""):
                                             echo " $" .  money_format('%i', $programDetails->invoice_amount);
                                            endif;
                                        ?> <?php //echo $programDetails->invoice_amount ?></h5>
                                    <hr>
                                </div>
                                <div class="col-md-7">
                                    <h5><strong>Note:</strong> <?php echo htmlspecialchars($programDetails->note) ?></h5>
                                    <?php if(strlen(htmlspecialchars($programDetails->note)) < 107): ?>
                                    <h5 class="hideme">- - -</h5>
                                    <?php endif; ?>
                                    <hr>
                                </div>
                            </div>
                            
                            <div class="row center-content">
                                <div class="col-md-12">
                                    
                                    <div class="btn-group pull-right" id="progBook">
                                    
                                        <?php
                                        echo $this->Html->link(
                                            "Add New Lesson",
                                            array('action' => 'addNewLessons',$programDetails->id),
                                            ['class' => 'btn btn-xs btn-primary pull-right']
                                        );
                                        ?>
                                        <br/>
                                    </div>
                                    <table id="bookingTable1"
                                    class="table table-striped responsive display table-hover table-bordered">
                                    <thead>
                                       
                                        <tr>
                                            <th>Status</th>
                                            <th>Date</th>
                                            <th style="width:100px">Time</th>
                                            <th>Length</th>
                                            <th>Rate</th>
                                            <th style="width:350px">Student Information</th>
                                            <th>Payroll Date</th>
                                            <th>Note</th>
                                            <th>Variation Request</th>
                                            <th style="width:100px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php foreach ($lessonDetails as $key => $lesson): ?>
                                        
                                            <tr id="note">
                                                <td><?= $lesson->status ?></td>
                                                <td><?php echo date('Y/m/d', strtotime($lesson->date))  ?></td>
                                                <td><?php echo date('h:i A', strtotime($lesson->time))  ?></td>
                                                <td><?= $lesson->length ?></td>
                                                <td><?php
                                                    if(!empty($lesson->rate)):
                                                        echo $lesson->rate->name;
                                                    else:
                                                        
                                                        if(!empty($programDetails->rate)):
                                                            echo $programDetails->rate->name;
                                                        else:
                                                            if(!empty($programDetails->tutor->rate)):
                                                                echo $programDetails->tutor->rate->name;
                                                            else:
                                                                echo "No Selected Rate";
                                                            endif;
                                                        endif;
                                                        
                                                    endif;
//                                                        debug($lesson);
                                                    
                                                     ?></td>
                                                <td>
                                                    
                                                    <button class="button btn-xs btn-primary" onclick="$('#myContent<?= $key ?>').slideToggle();">
                                                        View Lesson Details
                                                    </button>
                                                    <div id="myContent<?= $key ?>" class='hidden_studentInfo'>
                                                        <br/>
                                                        <table class="table">
                                                            <tr>
                                                                <th>Student</th>
                                                                <th>Outcome</th>
                                                                <th>Type</th>
                                                                <th>Topic</th>
                                                            </tr>

                                                    <?php foreach($lesson->lesson_students as $studentInfo): ?>
                                                            <tr>
                                                                <td><?= $studentInfo->program_student->student->name ?></td>
                                                                <td>
                                                                    <?php //debug($studentInfo);
                                                                        if($studentInfo->assessment_type_id >= 1):
                                                                            echo $studentInfo->assessment_type->type;
                                                                        else:    
                                                                            echo "General";
                                                                        endif;                                                                        
                                                                    ?>
                                                                    
                                                                </td>
                                                                <td>
                                                                    <?php if($studentInfo->assessment_type_id >= 1): ?>
                                                                        <?= $studentInfo->major_outcome->name ?>
                                                                    <?php endif; ?>
                                                                    
                                                                </td>
                                                                <td>
                                                                    <?php // debug($studentInfo->minor_outcome->code); ?>
                                                                    <?php if(isset($studentInfo->minor_outcome)): ?>
                                                                        <?= $studentInfo->minor_outcome->code ?>
                                                                    <?php endif; ?>
                                                                    
                                                                </td>
                                                            </tr>
                                                            
                                                    <?php endforeach; ?>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if(!empty($lesson->payroll_date) || $lesson->payroll_date != ""):
                                                            echo date('Y/m/d', strtotime($lesson->payroll_date)); 
                                                        endif;
                                                    ?>
                                                </td>
                                                <td  style="width:250px;"><?php if (!empty($lesson->note)): echo '<p class="comments-space" >' . htmlspecialchars($lesson->note) . '</p>'; endif; ?></td>
                                                <td  style="width:250px;"><?php echo '<p>' . htmlspecialchars($lesson->request) . '</p>'; ?></td>
                                                <td>
                                                 <div class="btn-group">
                                                     <?php echo $this->Html->link(
                                                         "Edit",
                                                         array('action' => 'edit', $lesson->id),
                                                         ['class' => 'btn btn-xs btn-warning']
                                                         );
                                                         ?>
                                                         <?php
                                                         echo $this->Html->link(
                                                             "Delete",
                                                             array('action' => 'delete', $lesson->id),
                                                             ['class' => 'btn btn-xs btn-danger']
                                                             );
                                                             ?>
                                                         </div>

                                                </td>
                                            </tr>
                                                    <?php endforeach; ?>
                                                <script>
                                                    
                                                    var showChar = 60;
                                                    var ellipsestext = "...";
                                                    var moretext = "See More";
                                                    var lesstext = "See Less";
                                                    $('.comments-space').each(function () {
                                                        var content = $(this).html();
                                                        if (content.length > showChar) {
                                                            var show_content = content.substr(0, showChar);
                                                            var hide_content = content.substr(showChar, content.length - showChar);
                                                            var html = show_content + '<span class="moreelipses">' + ellipsestext + '</span><span class="remaining-content"><span>' + hide_content + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                                                            $(this).html(html);
                                                        }
                                                    });

                                                    $(".morelink").click(function () {
                                                        if ($(this).hasClass("less")) {
                                                            $(this).removeClass("less");
                                                            $(this).html(moretext);
                                                        } else {
                                                            $(this).addClass("less");
                                                            $(this).html(lesstext);
                                                        }
                                                        $(this).parent().prev().toggle();
                                                        $(this).prev().toggle();
                                                        return false;
                                    });
                                      
                                                        
                                                      
                                                      
                                                    
                                                </script>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="lead-view-modal" class="modal fade" role="dialog"></div>
<div id="modal-container"></div>
<div id="editSubjectsContainer"></div>
<script>
    $(document).ready(function () {

//        $('#progBook').dataTable({"autoWidth": false});
        
        
        $('#progBook').on('click', '.programBooking',function () {
            var id = $(this).val(),
                modalContainer = $("#modal-container");

//        $.post('/admin/bookings/add-new-lessons', {"id": id}, function (data) {
//            modalContainer.html(data);
//            $('#lead-view').modal('show');
//        });
        $.post('/admin/bookings/add-new-lessons', {"id": id}, function (data) {
            modalContainer.html(data);
            $('#lead-view').modal('show');
        });

        });
        
        

    });
    
    function toggler(divId) {
        $("#" + divId).toggle();
    }
</script>
<?php // debug($programDetails->toArray()); ?>
