<?php $display = ""; $grandTotal = ""; //debug($tutorDetails->toArray()); ?>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<div id="dialog-confirm"></div>
<style>
    .form-control{
        height: 22px;
    }
    .textDate{
        height: 35px !important;
    }
    .selectDate{
        position:relative;
        top:20px;
    }
    .headerDisplay{
        position:relative;
        top:-15px;
    }
    .input-group-addon {
        font-size: 8px;
    }
</style>
<div class="widget-box container">
<div class="widget-box">
<div class="widget-title">
    <div class="col-md-3">
        <h2 class="headerDisplay">
            <strong>Session Report</strong>
        </h2>
    </div>
    <div class="col-md-5"></div>
    <div class="col-md-4">
        <div class="row ">
        <?= $this->Form->create(null, ['url' => ['action' => 'tutor'], 'method' => 'post']); ?>
        <div class="col-md-6 ">
            <div class="input-group selectDate">
                <?php 
                    if($date):
                        $displayDate = date('d/m/Y', strtotime($date));
                    else:
                        $displayDate = "";
                    endif; 
                ?>
                <input class="form-control accessDate textDate" value="<?= $displayDate ?>" type="text" id="payroll_date" name="date" required>
                <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
            </div>
        </div>
        <div class="col-md-6">
            <input type="submit" class="btn btn-success selectDate" value="Submit" />
        </div>
        <?= $this->Form->end(); ?>
        </div>
    </div>    
</div>
</div>
<div class="row">
<div class="col-sm-12 col-md-12">

    <?php foreach($tutorDetails as $tutor): 
        
        $x = 0;
        $button = $this->Html->link(
                                    "Send Email",
                                    array('action' => 'sendEmail', $tutor->id,$date),
                                    ['class' => 'btn btn-xs btn-warning']
                                );
        $notif = "";
        if($tutor->email_notified != ""):
            $notif = "Last notified: " . date('Y/m/d', strtotime($tutor->email_notified)) . " " . date('h:i A', strtotime($tutor->email_notified));
        endif;
        $header = '<div class="row">
                        <div class="col-sm-12 col-md-12">
                        <h3 style="text-align: left"><strong>'.$tutor->first_name. " " . $tutor->last_name.'</strong>'.$button. " <label style='font-size: 10px;'>" . $notif .'<label></h3>
                        </div>
                    </div><hr class="customHr"><br/>';
        
        if(!empty($tutor->booking_programs)):
           
            foreach($tutor->booking_programs as $booking):
                 $header1 = "";
                  $y = 0;
                if(!empty($booking->program_bookings)):
                    $stat = $booking->status;
                    $header1 .= '<table id="bookingStudents"
                       class="table table-striped responsive display table-hover table-bordered">
                    <thead>
                    <tr>
                        <th><div class="pull-left">Client: '.$booking->client->first_name . ' ' . $booking->client->last_name.', ' . $stat .'</div></th>
                    </tr>
                    </thead>';
                    $header1 .= "<tbody>";
                    $header1 .= '<tr><th>';
                    $header1 .= '<table class="table table-striped responsive display table-hover table-bordered">';
                    
                    $header1 .= '<tr>';
                    $header1 .= '<th>Status</th>';
                    $header1 .= '<th>Date</th>';
                    $header1 .= '<th>Time</th>';
                    $header1 .= '<th>Length</th>';
                    $header1 .= '</tr>';
                    
                    $total = 0;
                    foreach($booking->program_bookings as $pbook):
                        
                        $stats = $pbook->status;
                        
                        if(date('Ymd', strtotime($date)) >= date('Ymd', strtotime($pbook->date))):
                            $x++; $y++;
                            $header1 .= '<tr>';
                            $header1 .= '<td>'.$stats.'</td>';
                            $header1 .= '<td>'.date('Y/m/d', strtotime($pbook->date)).'</td>';
                            $header1 .= '<td>'.date('h:i A', strtotime($pbook->time)).'</td>';
                            $header1 .= '<td>'.$pbook->length.'</td>';
                            $header1 .= '</tr>';
                            
                            $total += $pbook->length;
                            $grandTotal += $pbook->length;
                        endif;
                       
                    endforeach;
                    
                    $header1 .= '<tr>';
                    $header1 .= '<th></th>';
                    $header1 .= '<th></th>';
                    $header1 .= '<th></th>';
                    $header1 .= '<th>Total: ' . $total .  '</th>';
                    $header1 .= '</tr>';
                    $header1 .= '</table>';
                    $header1 .= '</th></tr>';
                    $header1 .= '</tbody>
                                </table>';
                    
                    
                    if($y > 0):
                        $header .= $header1;
                    endif;
                endif;
                
            endforeach;
            
        endif;
        
        
        if($x > 0):
            $display .= $header;
        endif;
        
          
        endforeach; 
            
            
            echo $display;
        
        ?>
   
        
    <div class='col-md-12'>
        <div class='col-md-6'> </div>
        <div class='col-md-6'>
            <table class="pull-right table display table-hover table-bordered">
         <tr><p class="pull-right"><strong>Grand Total: </strong><?= $grandTotal; ?></p> </tr>
        </div>
     </table> 
    </div>
  
</div>
        
</div>
</div>
</div>
<script>
$(document).ready(function () {
    
    $('.accessDate').datepicker({
//        minDate: 0,
        dateFormat: 'dd/mm/yy',
        maxDate: 0
    });

    
});
</script>