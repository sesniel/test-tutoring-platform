<div class="modal fade" id="abandonDia" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Abandon Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" id="abandonLeadID" name='lead_id' value="<?= $lead_id ?>">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>