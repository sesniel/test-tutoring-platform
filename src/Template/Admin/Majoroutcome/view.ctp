<style>
    .my-width{
        width: 300px;
    }
    .my-width-choice{
        width: 250px;
        padding: 20px;
        font-size: x-large;
    }
    .borderless table {
        border-top-style: none;
        border-left-style: none;
        border-right-style: none;
        border-bottom-style: none;
    }
    
    .borderless td, .borderless th {
        border: none;
    }
    
    .pager {display:none;}
    .preload { 
        width:50px;
        height: 50px;
        position: fixed;
        top: 53%;
        left: 48%;}
    
    .btn-sm {
        padding: 10px 5px;
        font-size: 16px;
        line-height: 1.5;
        border-radius: 3px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="widget-title "><br/><br/><br/>
                <h1 class="text-center"><?= $question->description ?></h1>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
                        
            <?php
                if($question->image != ""):
                $newstring = substr($question->image, -3);
                    if($majorOutcomes->subject_id == 1):
                        echo $this->Html->image("Math/" .substr($question->image,0, -3). strtolower($newstring),
                            ['border' => '0', 'class' => 'img-responsive center-block']);
                    elseif($majorOutcomes->subject_id == 2):
                        echo $this->Html->image("English/" .substr($question->image,0, -3). strtolower($newstring),
                            ['border' => '0', 'class' => 'img-responsive center-block']);
                    endif;
                endif;

            ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 text-center">
            <h5 class="text-center "> Choices </h5>
        </div>    
        <div class="col-md-12 text-center"> 
            
                <?php
                    $questionsChoices = json_decode($question->choices, true);
                    foreach($questionsChoices as $choice):
                ?>
            
        <button id="singlebutton" name="singlebutton" class="btn btn-primary my-width"><?= $choice ?></button> 

                <?php endforeach; ?>
        
        
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <br/><br/><br/>
            <?= $this->Html->link(__('Edit Question'), ['action' => 'edit',$question->id], ['class' => 'btn btn-info']) ?>
            <br/><br/><br/>
        </div>
    </div>
</div>
