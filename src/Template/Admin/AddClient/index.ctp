<?php ($dataInfo); // debug($dataInfo); exit;
    $fname = "";  $lname = ""; $email=""; $phone = ""; 
    $mobile = ""; $unit_number = ""; $street_number = "";
    $street_name = ""; $postcode = ""; $suburb = ""; 
    $city = ""; $state = ""; 
    
    
    if(!empty($dataInfo)):
        $fname = $dataInfo['fname'];
        $lname = $dataInfo['lname'];
        $email = $dataInfo['email'];
        $phone = $dataInfo['phone'];
        $mobile = $dataInfo['mobile'];
        $unit_number = $dataInfo['unit-number'];
        $street_number = $dataInfo['street-number'];
        $street_name = $dataInfo['street-name'];
        $postcode = $dataInfo['postcode'];
        $suburb = $dataInfo['suburb'];
        $city = $dataInfo['city'];
        $state = $dataInfo['state'];
        
        
    endif;
?>

<h1><?= $check ?></h1>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
           
            <div class="widget-title">
                <h2><strong>Add New Client</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <?= $this->Form->create(null, ['url' => ['action' => 'index']]);  ?>
               
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Contact Detail:</h4>
                        <p>Please complete client contact details below. Upon submission the client will receive an
                            invitation email with their email and system generated password.</p>
                        <hr>
                    </div>
                </div>
                 
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-6">
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="fname">First Name:</label>
                                <input type="text" class="form-control" id="fname"
                                       name="fname"
                                       title="First Name"
                                       value="<?= $fname ?>" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="lname">Last Name:</label>
                                <input type="text" class="form-control" id="lname"
                                       name="lname" title="Last Name" 
                                       value="<?= $lname ?>"required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="email">Email:</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       title="Email"
                                       value="<?= $email ?>"required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <label for="phone">Phone:</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       title="Phone" maxlength="10" value="<?= $phone ?>">
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="mobile">Mobile:</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       title="Mobile" maxlength="10"
                                       value="<?= $mobile ?>" required>
                            </div>
                        </div>
                        <!-- <div class="row top-buffer">
                            <div class="col-md-12">
                                <label for="availability">Availability:</label>
                                <select multiple="multiple" id="availability" name="availability[]" required></select>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-md-6">
                        <div class="row top-buffer">
                            <div class="col-md-3">
                                <label for="unit-number">Unit #:</label>
                                <input type="text" class="form-control" id="unit-number"
                                       name="unit-number"
                                       title="First Name" maxlength="5" value="<?= $unit_number ?>">
                            </div>
                            <div class="col-md-3">
                                <span class="require-mark">&#42; </span><label for="street-number">Street #:</label>
                                <input type="text" class="form-control" id="street-number"
                                       name="street-number"
                                       title="Street Number" maxlength="5"
                                       value="<?= $street_number ?>" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="street-name">Street Name:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="street-name"
                                       title="Street Name" 
                                       value="<?= $street_name ?>" required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="postcode">Postcode:</label>
                                <input type="text" class="form-control" id="postcode" name="postcode"
                                       title="Postcode" maxlength="4" 
                                       value="<?= $postcode ?>" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="suburb">Suburb:</label>
                                <input type="text" class="form-control" id="suburb" name="suburb"
                                       title="Suburb" 
                                       value="<?= $suburb ?>"required>
                            </div>
                         
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="city" class="required">City:</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       title="City" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="state">State:</label>
                                <select id="state" name='state' class="form-control"
                                        value="<?= $state ?>" required>
                                    <option selected='selected' disabled>Please select state</option>
                                    <?php foreach ($states as $state) : ?>
                                        <option
                                            value='<?= $state->id ?>'><?= $state->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Students:</h4>
                        <p>Enter student details, select which subjects the student needs tutoring for
                            (<u><strong>Note:</strong>
                                this will be used to help allocate tutors later, and is independent of the
                                assessments</u>)</p>
                        <hr>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-6">
                        <div id="entry1" class="form-inline clonedInput container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 id="reference" name="reference" class="heading-reference">Student Detail #1</h5>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label for="student"> First Name:</label>
                                    <input type="text" class="form-control student-name" id="student"
                                           name="student[]"
                                           title="Student First Name" required>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label for="year-level">Year Level:</label>
                                    <select id="year-level" name='year-level[]'
                                            class="form-control std-year-lvl" required>
                                        <option selected='selected' disabled>- Please select year level -</option>
                                        <?php foreach ($yrLvls as $yr) : ?>
                                            <option
                                                value='<?= $yr->id ?>'><?= $yr->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label for="student">Subjects:</label>
                                    <div class="subjects">
                                        <select multiple="multiple" id="available-subjects" name="subjects1[]"
                                                class="student-subjecs" required>
                                            <?php foreach ($subjs as $subj) : ?>
                                                <option
                                                    value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-12 top-buffer">
                            <div class="form-inline">
                                <div class="form-group col-md-6 text-center">
                                    <input type="button" id="btnDel" class="btn btn-danger" value="Remove Student">
                                </div>
                                <div class="form-group col-md-6 text-center">
                                    <input type="button" id="btnAdd" class="btn btn-primary"
                                           value="Add Extra Student">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hidden">
                        <select id="sub-cop" multiple>
                            <?php foreach ($subjs as $subj) : ?>
                                <option
                                    value='<?= $subj->id ?>'><?= $subj->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-12 text-center">
                        <button type="submit" id="tutor-confirm-session-btn" class="btn btn-success">Create Client</button>
                    </div>
                </div>
                
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div><!-- End .row-->

<!-- Remove Detail -->
<div id="remove-section" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Remove Student</h4>
            </div>
            <div class="modal-body row">
                <p class="text-center">Are you sure you want to remove this section?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id='confirm-section-remove' class="btn btn-default" data-dismiss="modal">
                    Confirm
                </button>
            </div>
        </div>
    </div>
</div>

 <script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#email-exists':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Email address already in use. Please use different email address.');
            break;
    }

});
        $('#clientsTbl').on('submit', '.changeStatusBtn', function () {
        var token = $(this).val();

        $('#email-exists').find('#emailExists').val($(this).val());

    }); 

</script>

<?= $this->Html->script('admin/add-new-clients.js'); ?>
