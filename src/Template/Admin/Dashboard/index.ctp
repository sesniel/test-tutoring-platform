

<?php //$programStatus = array("All", "New", 'Pending Consultation', 'Completed Consultation'); 

$programStatus = array('All','Active', 'New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation');
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-container table-responsive">
                <div class="row">
                    <div class="col-md-12 top-buffer">
                        <div class="col-md-9"></div>
                       <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <?php
//                                echo $this->Html->link(
//                                    "Create Booking",
//                                    ['controller' => 'bookings', 'action' => 'create', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-success", 'target' => '_blank']);
                                echo $this->Html->link(
                                        "Create client",
                                        ['controller' => 'addClient', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary pull-right", 'target' => '_blank']);

                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row center-content">
                        <div class="col-md-6">
                            <div class="widget-title">
                                    <h2><strong>Unbooked Consultations</strong></h2>
                            </div>
                            <table class="table table-striped responsive display table-hover table-bordered consultationsTbl">
                                <hr>
                                <thead>
                                <tr>
                                    <th style="display:none;">Id</th>
                                    <th>Tutor</th>
                                    <th>Client</th>
                                    <th>Created date</th>
                                    
                                   
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($consultations as $key=>$con): if($con->abandoned_leads): ?>
                                        <?php if (($con->status) == 'New'): $count = count($con->leads) - 1;?>
                                                <?php $conDate = date("Y/m/d", strtotime($con->created));
                                                       $currentDate = date("Y/m/d");
                                                       
                                                       if ($conDate != $currentDate):
                                                ?>
                                        <tr>
                                            <td style="display:none;"><?= $con->id ?></td>
                                            <td><?= $con->leads[$count]->tutor->first_name.' '.$con->leads[$count]->tutor->last_name ?></td>
                                            <td><?= $con->leads[$count]->client->first_name.' '.$con->leads[$count]->client->last_name ?></td>
                                            <td><?= date('Y/m/d', strtotime($con->created)) ?></td>
                                            
                                            
                                            
                                        </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-striped responsive display table-hover table-bordered consultationsOverdueTbl">
                                <div class="widget-title">
                                    <h2><strong>Overdue Consultation Reports</strong></h2>
                            </div>
                                <hr>
                                <thead>
                                <tr>
                                    <th style="display:none;">Id</th>
                                    <th>Tutor</th>
                                    <th>Client</th>
                                    <th>Consultation date</th>
                                    
                                   
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($consultations as $key=>$con): if($con->abandoned_leads): ?>
                                        <?php if (($con->status) == 'Pending Consultation'):  $count = count($con->leads) - 1; ?>
                                        <tr>
                                            <td style="display:none;"><?= $con->id ?></td>
                                            <td><?= $con->leads[$count]->tutor->first_name.' '.$con->leads[$count]->tutor->last_name ?></td>
                                            <td><?= $con->leads[$count]->client->first_name.' '.$con->leads[$count]->client->last_name ?></td>
                                            <td><?= date('Y/m/d', strtotime($con->consultation_session_date)) ?></td>
                                            
                                            
                                            
                                        </tr>
                                        <?php endif; ?>
                                    <?php endif; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div><br>
                    <div class="widget-container table-responsive">
                <div class="row center-content">
                    <div class="widget-title">
                        <h2><strong>Recently Completed Consultation</strong></h2>
                    </div>
                    <hr>
                   <!-- <div class="col-md-6 statusPos">
                        <?php
                        echo $this->Form->create(null, [
                            'url' => ['action' => 'consultations'],
                            'method' => 'post'
                            ]);
                            ?>
                           
                            <label>Status: </label>
                            <select name="booking" class="form-control" onchange="this.form.submit()" >
                                <?php foreach($bookingStatus as $stat): ?>
                                <option <?php $sel = ($Bookingstatus == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div> -->
                </div><?php //debug($leads->toArray()); ?>
                <div class="row center-content">
                    <div class="col-md-12">
                        <table id="consultationsTbl"
                               class="table table-striped responsive display table-hover table-bordered completedConsultationTbl">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Client</th>
                                    <th>Tutor</th>
                                    <th>Date of Consultation</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($completedConsultations as $key => $con): ?>
                                    <?php if(!empty($con->leads[0]->tutor)): ?>
                                    <?php //if(($con->status) == 'Completed Consultation'): ?>
                                    
                                    <tr>
                                        <td><?php echo $con->id; ?></td>
                                        <td><?php
                                            if(!empty($con->leads)):
                                                echo $con->leads[0]->client->first_name . ' ' . $con->leads[0]->client->last_name;
                                            endif;
                                         ?></td>
                                        <td><?php
                                                if(!empty($con->leads[0]->tutor)):
                                                    echo $con->leads[0]->tutor->first_name . ' ' . $con->leads[0]->tutor->last_name;
                                                endif;
                                             ?></td>
                                        <td><?php
                                        
                                            if($con->consultation_session_date != ""):
                                                echo date('Y/m/d', strtotime($con->consultation_session_date));
                                            endif;
                                        
                                         ?></td>
                                        <td><?= $con->status ?></td>
                                        <td>   
                                            <div class="btn-group">
                                                
                                                <?php
                                                echo $this->Html->link("View/Edit", array('controller' => 'dashboard', 'action' => 'editConsultation', $con->id), ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']);
                                                //echo $this->Form->button('Notes', ['type' => 'button', 'class' => 'btn btn-sm btn-info consultationView', 'value' => $con->id]);
                                                //echo $this->Form->button('Book/Reallocate/Abandon', ['type' => 'button', 'class' => 'btn btn-sm btn-warning studentDetail', 'value' => $con->id]);
                                                
                                                if($con->status != "Abandoned"):
                                                    echo $this->Form->button('Discontinue', ['controller' => 'bookings','type' => 'button', 'class' => 'btn btn-sm btn-danger consultationView', 'value' => $con->id ]);
                                                    //echo $this->Html->link("Discontinue", array('controller' => 'bookings', 'action' => 'changeStatus', $con->id,'Abandoned'), ['class' => 'btn btn-sm btn-info discontinue', /* 'onclick' => "return confirm('Are you sure you want to Abandoned this consultation?')" */]);
                                                endif;
                                                
                                                echo $this->Html->link("Book", array('controller' => 'bookings', 'action' => 'changeStatus', $con->id,'Booked'), ['class' => 'btn btn-sm btn-warning']);
                                                
                                                
                                                ?>  
                                            </div>
                                        </td>
                                    </tr>  
                                        <?php endif; ?>
                                    <?php //endif; ?>
                                  <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                
            </div>
                    <div class="row center-content top-buffer">
                        
                        <div class="col-md-12">
                            <div class="widget-title">
                                    <h2><strong>Urgent Rebookings</strong></h2>
                            </div>
                            <div class="col-md-6 top-buffer">
                                <span><h5 class="top-buffer"><strong>Total number of Rebookings:</strong> <?php echo $rebookCount; ?></h5><hr></span>
                             </div>  
                            <div class="col-md-6 top-buffer">
                                <span><h5 class="top-buffer"><strong>Total number of Uninvoiced Bookings:</strong> <?php echo $uninvoicedCount; ?></h5><hr></span>
                                
                            </div>
                            <?php if (!empty($programDetails)): ?>
                            <table id="bookingsTable" class="table table-striped responsive display table-hover table-bordered consultationsTbl">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Students</th>
                                        <th>Status</th>
                                        <th>Remaining Lessons</th>
                                        <th>Lesson's Last Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($programDetails as $key => $program): 
                                            
                                            if(count($program->rebook_lessons) === 0):
                                    ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?php echo ucfirst($program->client->first_name) . " " . ucfirst($program->client->last_name); ?></td>
                                            <td><?php echo ucfirst($program->tutor->first_name) . " " . ucfirst($program->tutor->last_name); ?></td>
                                            <td>
                                                <?php 
                                                if($program->id <= 183):
                                                    echo ucfirst($program->program_students[0]->student->name);
                                                else:
                                                    foreach($program->program_students as $key => $student):
                                                        if((count($program->program_students) - 1) == $key):
                                                            echo ucfirst($student->student->name);
                                                        else:    
                                                            echo ucfirst($student->student->name) . ", ";
                                                        endif;
                                                    endforeach; 
                                                endif;
                                                ?>
                                                
                                            </td>
                                            <td><?php 
                                                if($program->status == "Done"):
                                                    echo "Completed";
                                                elseif($program->status == "Tutor"):
                                                    echo "Pending";
                                                else:
                                                   echo $program->status;                                                    
                                                endif;
                                             ?>
                                            </td>
                                            <td><?= count($program->rebook_lessons) ?></td>
                                            <td><?php 
                                                if(isset($program->rebook_lessons[0]->date)):
                                                    echo date("Y-m-d", strtotime($program->rebook_lessons[0]->date));
                                                endif;
                                            ?></td>
                                            <td>
                                                <?php
                                                echo $this->Html->link(
                                                    "Details",
                                                    array('controller' => 'bookings', 'action' => 'details',$program->id),
                                                    ['class' => 'btn btn-xs btn-warning', 'target'=>'_blank']
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Discontinued",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Discontinued"),
                                                    ['class' => 'btn btn-xs btn-danger', 'onclick' => "return confirm('Are you sure you want to discontinue this booking?')"]
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Rebooked",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Rebooked"),
                                                    ['class' => 'btn btn-xs btn-primary', 'target'=>'_blank']
                                                );
                                                ?>
                                            </td>
                                        </tr>
                                    
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- Completed Consultations -->
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consultation-container"></div>
<div id="consultation-container-details"></div>
<div id="student-details"></div>
<div id="reallocate-container"></div>
<div id="abandon-container"></div>
<!-- Re-allocate Lead -->
<div class="modal fade" id="reallocate-lead" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Re-allocate Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon and you will be redirected to the allocation page.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" class="abandonLeadID" name='' value="">
                    <input type="hidden" id="reallocateStdID" name='student_id' value="">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#consultation-updated':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Consultation updated successfully.');
            break;
        case '#discontinued':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Info');
            $('#response-dialog .modal-body p').text('Client has been discontinued.');
            break;
    }

});
        $('#consultationsTbl').on('submit', '.changeStatusBtn', function () {
        var token = $(this).val();

        $('#consultation-updated').find('#consultation-updated').val($(this).val());

    }); 
</script>

<?= $this->Html->script('admin/dashboard6.js'); ?>