

<?php 
    $bookingStatus = array("New", "Pending Consultation", "Abandoned", "Completed Consultation", "Booked", "Discontinued Consultation"); 
//    sort($bookingStatus);
//    debug($leads);exit;
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Consultation</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <?php
                            if($type == "New"):
                                echo $this->Form->create(null, ['url' => ['controller' => 'dashboard' ,'action' => 'newConAndPayDate']]);
                            else:
                                echo $this->Form->create(null, ['url' => ['controller' => 'dashboard' ,'action' => 'updateConAndPayDate']]);
                            endif; 
                        ?>
                      <!--  <div class="row center-content">
                            <div class="col-md-3">
                                <h4>Consultation Date and Time:</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control conDate tip" id="conDate" type="text" name="conDate" data-toggle="tooltip" title="All dates should be in dd/mm/yyyy format" value="<?php if($consultation->consultation_session_date != ""): echo date("d/m/Y" , strtotime($consultation->consultation_session_date)); endif;  // date_format($consultation->consultation_session_date, 'm/d/Y'); ?>" required <?php //if($consultation->status == 'Completed Consultation') echo 'readonly'; ?>><span class="input-group-addon conDateIcon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control" id="conTime" type="text" name="conTime" value="<?php if($consultation->consultation_session_date != ""): echo date('G:ia' , strtotime($consultation->consultation_session_date)); endif;  //ate_format($consultation->consultation_session_date, 'G:ia'); ?>" required <?php //if($consultation->status == 'Completed Consultation') echo 'readonly'; ?>><span class="input-group-addon conTimeIcon"><i class="glyphicon glyphicon-time"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
                            </div>
                            <?= $this->Form->hidden('consultation_id', ['value' => $consultation->id]) ?>
                        </div>
                      -->
                        <div class="row center-content">
                            <div class="col-md-3">
                                <h4>Payroll Date:</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control payDate tip" data-toggle="tooltip" title="All dates should be in dd/mm/yyyy format" id="payDate" type="text" name="payDate" value="<?php if($consultation->payroll_date): echo date("m/d/Y", strtotime($consultation->payroll_date)); endif; ?>"><span id="payCalIcon" class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                                </div>
                            
                            <?= $this->Form->hidden('consultation_id', ['value' => $consultation->id]) ?>
                        </div>
                        <div class="row center-content">
                            <div class="col-md-3">
                                <h4>Status:</h4>
                            </div>
                            <div class="col-md-3">
                                <select name="status" class="form-control" >
                                        <?php foreach($bookingStatus as $stat): ?>
                                        <option <?php $sel = ($consultation->status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            </div>
                        </div>
                        
                    </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="studentDetailsTable" class="responsive display table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong>Clients</strong></td>
                                            <td><strong>Student</strong></td>
                                            <td><strong>Year</strong></td>
                                            <td><strong>Lead Status</strong></td>
                                            <td><strong>Subjects</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($leads as $key=>$lead): ?>
                                        <tr>
                                            <td><?= $lead->client->first_name ?></td>
                                            <td><?= $lead->student->name ?></td>
                                            <td><?= $lead->student->year_level->name ?></td>
                                            <td><?= $lead->status ?></td>
                                            <td><?php $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                                echo implode(", ", $subj); ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="width: 100%;">
                                    
                                    <?php
                                        if($type == "New"):
                                            echo 'Save New Consultation';
                                        else:
                                            echo 'Update Consultation';
                                        endif; 
                                    ?>
                                </button>
                            </div>
                        </div>
                        <?= $this->Form->end(); ?>
                <br>
                    <?= $this->Form->create(null, ['url' => ['action' => 'leadAnswers']]); ?>
                    <?php if (!empty($lquest)):?>
                    <div class="row">
                        <div class="col-md-3">
                           <div class="widget-title">
                            <h2><strong>Students Notes: </strong></h2>
                            
                            </div>
                        </div>
                        <?php if(!empty($answers)): ?>
                        <div class="col-md-12">
                            <button type="button" id="edit-notes" class="btn btn-sm btn-success pull-right" >Edit</button>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="update-notes" class="btn btn-sm hidden btn-success pull-right" >Update</button>
                        </div>
                        <?php endif; ?>
                        
                        <div class="col-md-12">
                           
                            <?php foreach ($leadQuestions as $key => $question): ?>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <p><?= ($key+1).'. '.$question->question ?></p>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <?php if ($question->answer) { ?>
                                <textarea name="<?= $question->id ?>" class="form-control notes-info" cols="3" rows="3" disabled><?= $question->answer ?></textarea>
                            <?php } else { ?>
                                <textarea name="<?= $question->id ?>" class="form-control notes-info" cols="3" rows="3" required></textarea>
                            <?php } ?>
                        </div>
                    </div>
                        
                            <?php endforeach; ?>
                        </div>
                     </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4 top-buffer">
                        <?php if (empty($answers)): ?>
                        <button type="submit" class="btn btn-success" style="width: 100%;">Submit</button>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                
               <?php endif; ?>
                
                
                <?= $this->Form->end(); ?>  
                </div>
            </div>
            
        </div>
    </div>
</div>
<?= $this->Html->script('admin/editConsultation.js'); ?>

<script>
$(document).ready(function(){
    $('.tip').tooltip();
    
    
});


<?php // debug($consultation->toArray()); ?>
