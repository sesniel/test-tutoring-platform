<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Recent Allocations</strong></h2>
            </div>
            
                            <div class="row">
                                <div class="row col-md-12 center-content">
                                    <div class="col-md-10">
                                        <h4>Students Detail:</h4>
                                        <hr>
                                    </div>
                                    
                                </div>
                                <div class="row center-indent">
                                    <div id="studentsTbl" class="col-md-12">
                                        <table class="table table-striped responsive display table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <td>Client</td>
                                                <td>Student</td>
                                                <td>Suburb</td>
                                                <td>Year</td>
                                                <td>Subject</td>
                                                <td>Tutor</td>
                                                <td>Created Date</td>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($notAlign as $allocation): ?>
                                                <tr>
                                                   
                                                    
                                                    <td><?= $allocation->client->first_name . " " . $allocation->client->last_name ?></td>
                                                    <td><?= $allocation->student->name ?></td>
                                                    <td><?= $allocation->client->suburb ?></td>
                                                    <td><?= $allocation->student->year_level->name?></td>
                                                    <td>
                                                        <?php $subj = array();
                                                            foreach ($allocation->lead_subjects as $subjects) {
                                                                $subj[] = $subjects->subject->name;
                                                            }
                                                            echo implode(", ", $subj); ?>
                                                        
                                                        
                                                    </td>
                                                    <td>
                                                        <?php if(!empty($allocation->tutor)): ?>
                                                            <?= $allocation->tutor->first_name . " " . $allocation->tutor->last_name ?>
                                                            <?php endif; ?>
                                                        
                                                        
                                                    </td>
                                                    <td><?= date('Y/m/d', strtotime($allocation->created)); ?></td>
                                                   
                                                  
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row col-md-12 center-content">
                                
                            </div>
                            <div class="row center-indent">
                                <div class="col-md-12">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row center-content">
                                    <div class="col-md-12">
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div id="password-reset" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Password Reset</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'passwordReset']]); ?>
                <div class="modal-body">
                    <div class="row center-content">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center">Please confirm if you want to reset tutor password?</p>
                            </div>
                        </div>
                        <?= $this->Form->hidden('token', ['value' => $cli->token]) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
<?php if (isset($stdDetails)) { ?>
    <div id="editStudentDia" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Student Details</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'studentUpdate']]); ?>
                <div class="modal-body">
                    <?= $this->Form->hidden('student_id', ['value' => $stdDetails]) ?>
                    <?php foreach ($client as $cli2) : foreach ($cli2->students as $student) : if ($student->id == $stdDetails) : ?>
                    <div class="row center-content">
                        <div class="col-md-12">
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                    <label for="student">Student First Name:</label>
                                    <input type='text' value='<?= $student->name ?>' name="student_name"
                                    class='form-control student-name'
                                    required>
                                </div>
                                <div class="col-md-6">
                                    <label for="year-level">Year Level:</label>
                                    <select id="year-level" name='year_level'
                                    class="form-control" required>
                                    <option value='<?= $student->year_level->id ?>'
                                        selected='selected'><?= $student->year_level->name ?></option>
                                        <?php foreach ($yrlvls as $yrlvl) : ?>
                                            <option
                                            value='<?= $yrlvl->id ?>'><?= $yrlvl->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <label for="student">Subjects:</label>
                                <select id="student-subjects" name='subjects[]'
                                    class="form-control" multiple required>
                                    <?php foreach ($student->student_subjects as $selectedSubj) : ?>
                                        <option
                                        value='<?= $selectedSubj->subject->id ?>'
                                        selected><?= $selectedSubj->subject->name ?></option>
                                    <?php endforeach;
                                    foreach ($subjs as $subj) : ?>
                                    <option
                                    value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php endif; endforeach; endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#editStudentDia').modal('show');
            $('#year-level option').each(function () {
                $(this).siblings("[value='" + this.value + "']").remove();
            });
            $('#student-subjects option').each(function () {
                $(this).siblings("[value='" + this.value + "']").remove();
            });
            $('#student-subjects').multiSelect();
            $('.ms-container').css("width","100%");
        });
    </script>
<?php } ?>
<?php if (isset($addStudent)) { ?>
    <div id="addStudentDia" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Students:</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'addStudents']]); ?>
                <?= $this->Form->hidden('client_token', ['value' => $cli->token]) ?>
                <div class="modal-body">
                    <div class="row">
                        <div id="entry1" class="clonedInput col-md-12">
                            <div class="row col-md-12">
                                <p id="reference"><strong>Student Detail:</strong></p>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label>Student First
                                    Name:</label>
                                    <input type='text' name='addStdName[]' class='form-control student-name'
                                    required>
                                </div>
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label>Year
                                    Level:</label>
                                    <select name='addYrLvlId[]' class="form-control" required>
                                        <option selected="selected" disabled></option>
                                        <?php foreach ($yrlvls as $yrlvl) : ?>
                                            <option
                                            value='<?= $yrlvl->id ?>'><?= $yrlvl->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <span class="require-mark">&#42; </span><label>Subjects:</label>
                                <div class="remove-cont">
                                    <select id="addSubj" name='addSubjId1[]' class="form-control new-student-subjects" multiple
                                    required>
                                    <?php foreach ($subjs as $subj) : ?>
                                        <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-12 top-buffer text-center">
                            <div class="btn-group">
                                <button type="button" id="remove-btn" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove
                                </button>
                                <button type="button" id="add-btn" class="btn btn-xs btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Add Student/s</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="hidden">
        <select id="sub-cop" class="form-control" multiple required>
            <?php foreach ($subjs as $subj) : ?>
                <option
                value='<?= $subj->id ?>'><?= $subj->name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <script>
        $(document).ready(function () {
            $('#addStudentDia').modal('show');
            $('.new-student-subjects').multiSelect();
            $('.ms-container').css("width","100%");
        });
    </script>
<?php } ?>
<?php if (isset($leadInfo)) { ?>
<div id="editSubjDia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lead Subjects:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'leadSubjUpdate']]); ?>
            <div class="modal-body">
                <?= $this->Form->hidden('lead_id', ['value' => $leadInfo->id]) ?>
                <div class="row top-buffer">
                    <div class="col-md-12">
                        <label for="student">Student Reg. Subj/s:</label>
                        <p class="text-center">
                        <?php $subj = array();
                            foreach ($leadInfo->student->student_subjects as $subjects) {
                                $subj[] = $subjects->subject->name;
                            }
                            echo implode(", ", $subj); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <div class="col-md-12 top-buffer">
                            <label for="student">Subjects:</label>
                            <select id="student-subjects" name='subjects[]' class="form-control" multiple required>
                                <?php foreach ($leadInfo->lead_subjects as $selectedSubj) : ?>
                                    <option value='<?= $selectedSubj->subject_id ?>' selected><?= $selectedSubj->subject->name ?></option>
                                <?php endforeach;
                                foreach ($regSubjects as $subj) : ?>
                                    <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#editSubjDia').modal('show');
        $('#year-level option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Available Subjects'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Lead Subjects'>",
            afterInit: function(ms){
                var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        $('.ms-container').css("width","100%");
    });
</script>
<?php } ?>
<?= $this->Html->script('admin/view-client2.js'); ?>