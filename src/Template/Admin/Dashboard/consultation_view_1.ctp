<?php $conId = $con->id;
//foreach ($consultations as $key => $con):
//    $id = $con->id;
//endforeach;
?>

<div id="consultation" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Discontinue</h4>
            </div>
            
           <?= $this->Form->create(null, ['url' => ['controller' => 'allocateLead','action' => 'reallocateTutor']]); ?>
            
            <?= $this->Form->hidden('client_id', ['value' => $leadClientId]); ?>
            <div class="modal-body row center-table">
                <div class="col-md-12">
                    <p class="center-content">Would you like to discontinue this client or reallocate to another tutor?</p>
                </div>
            </div>
            <div class="modal-footer center-content">
                <button type="button" class="btn btn-default" id="discontinue" data-toggle="modal" data-target="discontinue">Discontinue</button>
                <?php //if (!$question->answer) { ?>
                    <button type="submit" class="btn btn-success">Re-allocate</button>
                <?php // } ?>                
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>

<div id="discontinue-client" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Discontinue</h4>
            </div>
           
           <?= $this->Form->create(null, ['url' => ['action' => 'discontinue']]); ?>
            <?= $this->Form->hidden('consultation_id', ['value' => $conId]); ?>
            <div class="modal-body row center-table">
                <div class="col-md-12">
                    <p class="center-content">Are you sure you want to discontinue?</p>
                </div>
            </div>
            <div class="modal-footer center-content">
                    <button type="button" class="btn btn-default" id="discontinue" data-dismiss="modal" data-target="discontinue">No</button>
                    <button type="submit" class="btn btn-success">Yes</button>
            </div>
            <?= $this->Form->end(); ?>    
            
        </div>
        
    </div>
</div>



<script>
$(document).ready(function () {
    $('#discontinue').click(function() {
        $('#discontinue-client').modal('show');
    });
    
});
</script>
