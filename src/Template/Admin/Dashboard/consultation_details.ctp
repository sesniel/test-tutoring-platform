<div id="consultation-details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Consultation:</h4>
            </div>
            <div class="modal-body row center-table">
                <div class="row col-md-12 center-content">
                    <div class="col-md-12">
                        <h5><strong>Student Notes:</strong></h5>
                    </div>
                </div>
                <?php foreach ($leadQuestions as $key => $question): ?>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <p><?= ($key+1).'. '.$question->question ?></p>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <textarea name="<?= $question->id ?>" class="form-control" cols="3" rows="3" disabled><?= $question->answer ?></textarea>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>          
            </div>       
        </div>
    </div>
</div>