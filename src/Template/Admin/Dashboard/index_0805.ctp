<?php //$programStatus = array("All", "New", 'Pending Consultation', 'Completed Consultation'); 
$programStatus = array('All','Active', 'New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation');
?>
<div class="widget-box">
<div class="row">
    <div class="col-sm-6">
        <div class="widget-title">
            <h2><strong>Calendar</strong></h2>
        </div>
        <hr class="customHr">

    <?= $this->Html->css('calendar-stylesheet.css'); ?>
        <style>
            body { display: felx; -webkit-box-orient: vertical; -webkit-box-direction: normal; -ms-flex-direction: column; flex-direction: column; }
            .wrapper { display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -ms-flex-direction: calendar-row; flex-direction: calendar-row; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; -ms-flex-line-pack: center; align-content: center; }
            body > p { font-family: "Satellite", "Roboto", sans-serif; font-size: 20px; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; -ms-flex-line-pack: center; align-content: center; -webkit-box-align: center; -ms-flex-align: center; align-items: center; margin: 20px 40px; text-align: justify; }
        </style>
        <div class="wrapper">
            <div id="calendarContainer"></div>
            <div id="eventsContainer" style="margin-left: 8px;"></div>
        </div>
        

    <?= $this->Html->script('library.js'); ?>
        <script>
            // my best way of organizing data, maybe that can be the outcome of joining multiple tables in the database and then parsing them in such a manner, easier for the person to push use a date and the events of it

            // my best way of organizing data, maybe that can be the outcome of joining multiple tables in the database and then parsing them in such a manner, easier for the person to push use a date and the events of it
            data = { years: [<?php echo json_encode($jsonInfo) ?> ] };
            
//            data = { years:[ {  
//         "int":"2017",
//         "months":[ {  
//               "int":"08",
//               "days":[ {  
//                     "int":"10",
//                     "events":[ {  
//                           "name":"Lachlan  Whyborn",
//                           "text":"Comment Comment Comment"
//                        }
//                     ]
//                  }
//               ]
//            }
//         ]
//      }
//   ]
//};
//           
//                 
//            data = { years: [   {
//                int: 2017,
//                months: [ {
//                    int: 8,
//                    days: [ {
//                        int: 10,
//                        events: [ { 
//                            name: "Joshua Sesniel Deloso",
//                            text: "Weirdo was born1"
//                                  },{ 
//                            name: "Joshua Sesniel Deloso",
//                            text: "Weirdo was born2"
//                                  },{ 
//                            name: "Joshua Sesniel Deloso",
//                            text: "Weirdo was born3"
//                                  }
//                               ]
//                        },
//                        {
//                        int: 11,
//                        events: [
//                            {
//                            name: "Joshua Sesniel Deloso",
//                            text: "Weirdo was born - 11"
//                            }
//                        ]
//                        },
//                        {
//                        int: 12,
//                        events: [
//                            {
//                            name: "Joshua Sesniel Deloso",
//                            text: "Weirdo was born - 12"
//                            }
//                        ]
//                        }
//                    ]
//                    }
//                ]
//                }
//            ]
//            };
            
            var arr = Object.keys(data).map(function(k) { return data[k] });
            console.log(data);
            
            
            calendar = new Calendar("calendarContainer", "small", [ "Sunday", "3" ], [ "#ec8b43", "#76b2aa", "#ffffff", "#ffecb3" ], data); // calendar container, size, array of first day of the week, and the number of letters for the week days ( max of 3 min of 1 ), colors... i would suggest using materialpalette.com
            organizer = new Organizer("eventsContainer", calendar); // organizer container and the calendar object to associate it to

            currentDay = calendar.date.getDate(); // used this in order to make anyday today depending on the current today


            showEvents(); // list events of today, when calendar loads

            callback = function () {
            showEvents();
            }

            organizer.setOnClickListener('day-slider', callback, callback); // first callback is for the back slider, second is for the next slider
            organizer.setOnClickListener('days-blocks', callback, null); // only needed callback is the first, no back or next, last parameter is ignored
            organizer.setOnClickListener('month-slider', callback, callback); // first callback is for the back slider, second is for the next slider
            organizer.setOnClickListener('year-slider', callback, callback); // first callback is for the back slider, second is for the next slider

            // a way to show the events of a date after retrieving the data from the database
            function showEvents() {
            theYear = -1, theMonth = -1, theDay = -1;

            for (i = 0; i < data.years.length; i++) {
                if (calendar.date.getFullYear() == data.years[i].int) {
                theYear = i;
                break;
                }
            }

            if (theYear == -1) return;

            for (i = 0; i < data.years[theYear].months.length; i++) {
                if ((calendar.date.getMonth() + 1) == data.years[theYear].months[i].int) {
                theMonth = i;
                break;
                }
            }

            if (theMonth == -1) return;

            for (i = 0; i < data.years[theYear].months[theMonth].days.length; i++) {
                if (calendar.date.getDate() == data.years[theYear].months[theMonth].days[i].int) {
                theDay = i;
                break;
                }
            }

            if (theDay == -1) return;

            theEvents = data.years[theYear].months[theMonth].days[theDay].events; 

            organizer.list(theEvents);
            }
        </script>
    </div>
    
    <div class="col-sm-6 col-md-6">
        <div class="widget-title">
            <h2><strong>Dashboard</strong></h2>
        </div>
        <hr class="customHr">
        <div class="widget-container table-responsive">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-9">
                        <p>Welcome to your dashboard!</p>
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <div class="btn-group pull-right">
                            <?php
                            echo $this->Html->link(
                                "Add New Client",
                                ['controller' => 'addClient', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-success", 'target' => '_blank']);
                            echo $this->Html->link(
                                    "Allocate Student/s",
                                    ['controller' => 'allocateLead', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary pull-right", 'target' => '_blank']);

                            ?>
                        </div>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">    
                            <li class="active"><a href="#unallocatedStudents" data-toggle="tab"><h5>New Students</h5></a></li>
                            <li><a href="#notAligned" data-toggle="tab"><h5>Recent Allocations</h5></a></li>
                            <li><a href="#consultations" data-toggle="tab"><h5>Pending Consultations</h5></a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade" id="consultations">
                                <div class="row col-md-12 top-buffer">
                                    <div class="col-md-9">
                                        <p class="customMtop">Pending Consultations are shown in this table.</p>
                                    </div>
                                    <div class="col-md-3">
                                        <?php
                                        echo $this->Form->create(null, [
                                            'url' => ['action' => 'index'],
                                            'method' => 'post'
                                        ]);
                                        ?>
                                        Status: 
                                        <select name="status" class="form-control" onchange="this.form.submit()" >
                                            <?php foreach ($programStatus as $stat): ?>
                                                <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                    <div class="row center-content">
                                        <table id="consultationsTbl"
                                               class="table table-striped responsive display table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Client</th>
                                                <th>Tutor</th>
                                                <th>Date of Consultation</th>
                                                <th>Payroll Date</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($consultations as $key=>$con): if($con->abandoned_leads): ?>
                                                    <tr>
                                                        <td><?= $con->leads[0]->client->first_name.' '.$con->leads[0]->client->last_name ?></td>
                                                        <td><?= $con->leads[0]->tutor->first_name.' '.$con->leads[0]->tutor->last_name ?></td>
                                                        <td><?php if($con->consultation_session_date != ""): echo date('Y/m/d', strtotime($con->consultation_session_date)); endif; ?></td>
                                                        <td><?php if($con->payroll_date != ""): echo date('Y/m/d', strtotime($con->payroll_date)); endif;  ?></td>
                                                        <td><?= $con->status ?></td>
                                                        <td>
                                                        <?php if ($con->status == 'Completed Consultation') { ?>
                                                            <div class="btn-group">
                                                            <?php 
                                                                 echo $this->Form->button('Notes',['type' => 'button', 'class' => 'btn btn-sm btn-info consultationView', 'value' => $con->id]);
                                                                 echo $this->Form->button('Book/Reallocate/Abandon', ['type' => 'button', 'class' => 'btn btn-sm btn-warning studentDetail', 'value' => $con->id]);
                                                                 echo $this->Html->link("Edit/View", array('controller' => 'dashboard', 'action' => 'editConsultation', $con->id), ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']);
                                                            ?>  
                                                            </div>
                                                        <?php } else{ ?>  
                                                            <div class="btn-group">
                                                            <?php 
                                                                  echo $this->Form->button('Reallocate/Abandon', ['type' => 'button', 'class' => 'btn btn-sm btn-danger studentDetail', 'value' => $con->id]);
                                                                  echo $this->Html->link("Complete", ['action' => 'updateconsultation', $con->id], ['class' => "btn btn-xs btn-success"]);
                                                                  echo $this->Html->link("Edit/View", array('controller' => 'dashboard', 'action' => 'editConsultation', $con->id), ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']);
                                                            ?>
                                                            </div>
                                                        <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php endif; endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="notAligned">
                                <div class="row col-md-12 top-buffer">
                                    <div class="row center-content">
                                        <p class="customMtop">These are students with no alignment lesson date.</p>
                                    </div>
                                    <div class="row center-content">
                                        <table id="notAlignedTable"
                                               class="table table-striped responsive display table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Client</th>
                                                <th>Student</th>
                                                <th>Suburb</th>
                                                <th>Year</th>
                                                <th>Subject</th>
                                                <th>Tutor</th>
                                                <th>Created Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($notAligned as $info):  ?>
                                                <tr>
                                                    <td><?= $info->client->first_name . " " . $info->client->last_name ?></td>
                                                    <td><?= $info->student->name ?></td>
                                                    <td><?= $info->client->suburb ?></td>
                                                    <td><?= $info->student->year_level->name ?></td>
                                                    <td style="width:300px;"><?php $subj = array();
                                                        foreach ($info->lead_subjects as $subjects) {
                                                            $subj[] = $subjects->subject->name;
                                                        }
                                                        echo implode(", ", $subj); ?></td>
                                                    <td>
                                                        <?php if(!empty($info->tutor)): ?>
                                                        <?= $info->tutor->first_name . " " . $info->tutor->last_name ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?= date('Y/m/d', strtotime($info->created)); ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php 
                                                                echo $this->Form->button('Re-allocate',['type' => 'button', 'class' => 'btn btn-sm btn-warning reallocateLead', 'value' => $info->id.'/'.$info->student_id]);
                                                                echo $this->Form->button('Abandon',['type' => 'button', 'class' => 'btn btn-sm btn-danger abandonLead', 'value' => $info->id]);
                                                                echo $this->Html->link("Edit/View", array('controller' => 'dashboard', 'action' => 'editConsultation', $con->id), ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']);
                                                            ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active in" id="unallocatedStudents">
                                <div class="row col-md-12 top-buffer">
                                    <div class="row center-content">
                                        <p class="customMtop">These are the Students who do not have Tutors
                                            allocated on them..</p>
                                    </div>
                                    <div class="row center-content">
                                        <table id="unallocatedStudentsTbl"
                                               class="table table-striped responsive display table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Student</th>
                                                <th>Year Level</th>
                                                <th>Subjects</th>
                                                <th>Suburb</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php foreach ($noTutor as $noTut): ?>

                                                <tr>
                                                    <td><?= $noTut->name ?></td>
                                                    <td><?= $noTut->year_level->name ?></td>
                                                    <td style="width:300px;"><?php $subjs = array();
                                                        foreach ($noTut->student_subjects as $stdSubj):
                                                            $subjs[] = $stdSubj->subject->name;
                                                        endforeach;
                                                        echo implode(", ", $subjs); ?></td>
                                                    <td><?= $noTut->client->suburb ?></td>
                                                    <td>
                                                        <?= $this->Html->link(
                                                            "Allocate",
                                                            ['controller' => 'allocateLead', 'action' => 'student', $noTut->id], ['escape' => false, 'class' => "btn btn-xs btn-warning", 'target' => '_blank']); ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
</div>


<div id="consultation-container"></div>
<div id="student-details"></div>
<div id="reallocate-container"></div>
<div id="abandon-container"></div>
<!-- Re-allocate Lead -->
<div class="modal fade" id="reallocate-lead" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Re-allocate Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon and you will be redirected to the allocation page.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" class="abandonLeadID" name='lead_id' value="">
                    <input type="hidden" id="reallocateStdID" name='student_id' value="">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- Abandon Lead -->
<div class="modal fade" id="abandon-lead" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Abandon Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" class="abandonLeadID" name='lead_id' value="">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/dashboard6.js'); ?>