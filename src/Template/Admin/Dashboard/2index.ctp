<?php //$programStatus = array("All", "New", 'Pending Consultation', 'Completed Consultation'); 
$programStatus = array('All','Active', 'New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation');
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Dashboard</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-9">
                            <p>Welcome to your dashboard!</p>
                            <hr>
                        </div>
                        <div class="col-md-3">
                            <div class="btn-group pull-right">
                                <?php
                                echo $this->Html->link(
                                    "Create Booking",
                                    ['controller' => 'bookings', 'action' => 'create', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-success", 'target' => '_blank']);
                                echo $this->Html->link(
                                        "Create client",
                                        ['controller' => 'addClient', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary pull-right", 'target' => '_blank']);

                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row center-content">
                        <div class="col-md-6">
                            <table class="table table-striped responsive display table-hover table-bordered consultationsTbl">
                                <h5><strong>Unbooked Consultations</strong></h5>
                                <hr>
                                <thead>
                                <tr>
                                    <th>Tutor</th>
                                    <th>Client</th>
                                    <th>Created date</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($consultations as $key=>$con): if($con->abandoned_leads): ?>
                                        <?php if (($con->status) == 'New'): ?>
                                        <tr>
                                            <td><?= $con->leads[0]->tutor->first_name.' '.$con->leads[0]->tutor->last_name ?></td>
                                            <td><?= $con->leads[0]->client->first_name.' '.$con->leads[0]->client->last_name ?></td>
                                            <td><?= date('Y/m/d', strtotime($con->created)) ?></td>
                                            
                                            
                                        </tr>
                                        <?php endif; ?>
                                    <?php endif; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-striped responsive display table-hover table-bordered consultationsTbl">
                                <h5><strong>Overdue Consultations</strong></h5>
                                <hr>
                                <thead>
                                <tr>
                                    <th>Tutor</th>
                                    <th>Client</th>
                                    <th>Created date</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($consultations as $key=>$con): if($con->abandoned_leads): ?>
                                        <?php if (($con->status) == 'Pending Consultation'): ?>
                                        <tr>
                                            <td><?= $con->leads[0]->tutor->first_name.' '.$con->leads[0]->tutor->last_name ?></td>
                                            <td><?= $con->leads[0]->client->first_name.' '.$con->leads[0]->client->last_name ?></td>
                                            <td><?= date('Y/m/d', strtotime($con->created)) ?></td>
                                            
                                            
                                        </tr>
                                        <?php endif; ?>
                                    <?php endif; endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                      </div>
                    <div class="row center-content">
                        
                        <div class="col-md-12">
                            <table id="bookingsTable"
                                class="table table-striped responsive display table-hover table-bordered consultationsTbl">
                                <h5><strong>Bookings</strong></h5>
                                <hr>
                                
                                <div class="col-md-12">fjgfjgfk</div>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Students</th>
                                        <th>Status</th>
                                        <th>Remaining Lessons</th>
                                        <th>Lesson's Last Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach ($programDetails as $key => $program): 
                                            
                                            if(count($program->rebook_lessons) <= 1):
                                    ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?php echo ucfirst($program->client->first_name) . " " . ucfirst($program->client->last_name); ?></td>
                                            <td><?php echo ucfirst($program->tutor->first_name) . " " . ucfirst($program->tutor->last_name); ?></td>
                                            <td>
                                                <?php 
                                                if($program->id <= 183):
                                                    echo ucfirst($program->program_students[0]->student->name);
                                                else:
                                                    foreach($program->program_students as $key => $student):
                                                        if((count($program->program_students) - 1) == $key):
                                                            echo ucfirst($student->student->name);
                                                        else:    
                                                            echo ucfirst($student->student->name) . ", ";
                                                        endif;
                                                    endforeach; 
                                                endif;
                                                ?>
                                                
                                            </td>
                                            <td><?php 
                                                if($program->status == "Done"):
                                                    echo "Completed";
                                                elseif($program->status == "Tutor"):
                                                    echo "Pending";
                                                else:
                                                   echo $program->status;                                                    
                                                endif;
                                             ?>
                                            </td>
                                            <td><?= count($program->rebook_lessons) ?></td>
                                            <td><?php 
                                                if(isset($program->rebook_lessons[0]->date)):
                                                    echo date("Y-m-d", strtotime($program->rebook_lessons[0]->date));
                                                endif;
                                            ?></td>
                                            <td>
                                                <?php
                                                echo $this->Html->link(
                                                    "Details",
                                                    array('controller' => 'bookings', 'action' => 'details',$program->id),
                                                    ['class' => 'btn btn-xs btn-warning', 'target'=>'_blank']
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Discontinued",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Discontinued"),
                                                    ['class' => 'btn btn-xs btn-danger', 'onclick' => "return confirm('Are you sure you want to discontinue this booking?')"]
                                                );
                                                ?>
                                                <?php
                                                echo $this->Html->link(
                                                    "Rebooked",
                                                    array('controller' => 'bookings', 'action' => 'changeRebookStatus',$program->id,"Rebooked"),
                                                    ['class' => 'btn btn-xs btn-primary', 'target'=>'_blank']
                                                );
                                                ?>
                                            </td>
                                        </tr>
                                    
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="consultation-container"></div>
<div id="student-details"></div>
<div id="reallocate-container"></div>
<div id="abandon-container"></div>
<!-- Re-allocate Lead -->
<div class="modal fade" id="reallocate-lead" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Re-allocate Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon and you will be redirected to the allocation page.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" class="abandonLeadID" name='lead_id' value="">
                    <input type="hidden" id="reallocateStdID" name='student_id' value="">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- Abandon Lead -->
<div class="modal fade" id="abandon-lead" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Abandon Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <div class="modal-body">
                <div class="form-group">
                    <p><strong>Warning:</strong> If you click <u>Confirm</u> the status of the lead will changed
                        from New to Abandon.</p>
                    <label class="control-label" for="reason">Reason for abandoning:</label>
                    <input type="hidden" class="abandonLeadID" name='lead_id' value="">
                        <textarea id="reason" name="reason" class="form-control" rows="3"
                                  required="required"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/dashboard6.js'); ?>