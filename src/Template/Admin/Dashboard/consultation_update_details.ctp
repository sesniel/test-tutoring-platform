<?php $leadId = $consultationDetails->leads[0]->id;
      $clientID = $consultationDetails->leads[0]->client->id;
      $studentID = $consultationDetails->leads[0]->student->id;
?>
<div id="consultation" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> 
                    <?php 
                    if($header == "Unbooked"):
                        echo "Unbooked Consultations";
                    elseif($header == "Overdue"):
                        echo "Overdue Consultation Reports";
                    endif; ?>
                </h4>
            </div>  
            <?= $this->Form->create(null, ['url' => ['controller' => 'allocateLead','action' => 'reallocateTutor']]); ?>
            <?= $this->Form->hidden('client_id', ['value' => $clientID]); ?>
            <?= $this->Form->hidden('lead_id', ['value' => $leadId]); ?>
            <?= $this->Form->hidden('student_id', ['value' => $studentID]); ?>
            <div class="modal-body row center-table">
                <div class="col-md-12">
                    <table class='table'>
                        <tr>
                            <td>Tutor</td>
                            <td>Client</td>
                            <td>Student</td>
                            <td>Tutor Phone</td>
                            <td>Tutor Email</td>
                            <td>Created</td>
                        </tr>
                        <tr>
                            <?Php $count = count($consultationDetails->leads) - 1; ?>
                            <td>
                                <?= $consultationDetails->leads[$count]->tutor->first_name ?> 
                                <?= $consultationDetails->leads[$count]->tutor->last_name ?>
                                
                                
                            </td>
                            <td>
                                <?= $consultationDetails->leads[$count]->client->first_name ?> 
                                <?= $consultationDetails->leads[$count]->client->last_name ?>
                            </td>
                            <td> 
                                <?php $student = array();
                                foreach ($consultationDetails->leads as $leads) {
                                $student[] = $leads->student->name;
                                }
                                echo implode(", ", $student);
                                ?> 
                            </td>
                            <td>
                                <?Php if($consultationDetails->leads[$count]->tutor->phone != "" && $consultationDetails->leads[$count]->tutor->mobile != ""): ?>
                                    <?= $consultationDetails->leads[$count]->tutor->phone ?>, <?= $consultationDetails->leads[$count]->tutor->mobile ?> 
                                <?php elseif($consultationDetails->leads[$count]->tutor->phone != ""): ?>
                                    <?= $consultationDetails->leads[$count]->tutor->phone ?>
                                <?php elseif($consultationDetails->leads[$count]->tutor->mobile != ""): ?>
                                    <?= $consultationDetails->leads[$count]->tutor->mobile ?> 
                                <?php endif; ?>
                            </td>
                            <td>
                                <?= $consultationDetails->leads[$count]->tutor->email ?> 
                            </td>
                            <td>
                                <?php
                                    echo date("d/m/Y", strtotime($consultationDetails->created));
                                ?> 
                            </td>
                        </tr>
                    </table>
                    
                    
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#abandon-lead">Abandon Lead</button>
                    <button type="submit" class="btn btn-primary" >Reallocate</button>
                </div>
            </div>
        </div>  
         <?= $this->Form->end(); ?>
    </div>
</div>

<div class="modal fade" id="abandon-lead" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Abandon Lead</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'abandonLead']]); ?>
            <input type="hidden" class="abandonLeadID" name="lead_id" value="<?= $leadId ?>">
            <div class="modal-body row center-table">
                <div class="col-md-12">
                    <p class="center-content">Please confirm if you want to abandon this client.</p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>

