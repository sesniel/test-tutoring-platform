<?php
$programStatus = array('All', 'Active', 'Pending', 'In Progress', 'Completed', 'Cancelled');
?>

<?php foreach ($client as $cli) : ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Client Profile</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->Form->create(null, ['url' => ['action' => 'clientUpdate']]); ?>
                                    <div id="client-info" class="row">
                                        <div class="row col-md-12 center-content">
                                            <div class="col-md-11">
                                                <h4>Contact Info:</h4>
                                                <hr>
                                            </div>
        
                                            <table class="pull-right" style="position:relative; right: 45px;">
                                                <tr>
                                                    <td>                                
                                                        <button type="button" id="edit-client-btn" class="btn btn-primary btn-xs pull-right top-buffer">Edit Contact</button>
                                                        <button type="submit" id="update-client-btn"class="btn btn-success btn-xs hidden pull-right top-buffer">Update</button>
                                                    </td>
                                                    <td>
                                                        <?php 
                                                            $image = $this->Html->image("gmap.ico", ['border' => '0', 'class' => 'img-responsive', 'style' => 'width: 25px; height: 25px;']); 

                                                            echo $this->Html->link($image,
                                                            ['controller' => 'maps', 'action' => 'index',$cli->id, 'prefix' => 'admin'], ['escape' => false, 'target' => '_blank']); ?>
                                                    </td>
                                                </tr>
                                            </table>
                                    </div>
                                    <?= $this->Form->hidden('token', ['value' => $cli->token]) ?>
                                    <div class="row center-indent">
                                        <hr>
                                        <div class="col-md-4">
                                            <label for="fname">First Name:</label>
                                            <input type="text" id="fname" name="first_name"
                                            value="<?= $cli->first_name ?>"
                                            class="form-control" required="required"
                                            data-toggle="tooltip" title="First Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="lname">Last Name:</label>
                                            <input type="text" id="lname" name="last_name"
                                            value="<?= $cli->last_name ?>"
                                            class="form-control" required="required"
                                            data-toggle="tooltip" title="Last Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="email">Email:</label>
                                            <input type="text" class="form-control" id="email"
                                            name="email" value="<?= $cli->email ?>" title="Email"
                                            required>
                                        </div>
                                    </div>
                                    <div class="row center-indent">
                                        <div class="col-md-4">
                                            <label for="mobile">Mobile:</label>
                                            <input type="text" class="form-control" id="mobile" name="mobile"
                                            value="<?= $cli->mobile ?>"
                                            title="Mobile" maxlength="10" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="phone">Phone:</label>
                                            <input type="text" id="phone" name="phone"
                                            value="<?= $cli->phone ?>"
                                            class="form-control" data-toggle="tooltip" title="First Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="suburb">Suburb:</label>
                                            <input type="text" id="suburb" name="suburb"
                                            value="<?= $cli->suburb ?>"
                                            class="form-control" required="required"
                                            data-toggle="tooltip"
                                            title="Suburb">
                                        </div>
                                    </div>
                                    <div class="row center-indent">
                                        <div class="col-md-4">
                                            <label for="street_number">Street#:</label>
                                            <input type="text" id="street_number" name="street_number"
                                            value="<?= $cli->street_number ?>" class="form-control"
                                            required="required" data-toggle="tooltip"
                                            title="Street Number">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="street_name">Street Name:</label>
                                            <input type="text" id="street_name" name="street_name"
                                            value="<?= $cli->street_name ?>"
                                            class="form-control" required="required"
                                            data-toggle="tooltip" title="Street Name">
                                        </div>
                                        <div class="col-md-4">
                                            <label for="status">Status</label>
                                            <select name='status' id="status" class="form-control" required>
                                                <option <?php if($cli->status == "Active"): echo "selected"; endif; ?> value="Active">Active</option>
                                                <option <?php if($cli->status == "Discontinue"): echo "selected"; endif; ?>  value="Discontinue">Discontinue</option>
                                            </select>
                                            <?php 
                                                $statConsult = 0;
                                                foreach($statusActiveConsultations as $checkConsultation):
                                                    if(!empty($checkConsultation->consultation)):
                                                        $statConsult = 1;
                                                    endif;
                                                endforeach;
                                                if(!empty($statusActiveBookings->toArray()) || $statConsult === 1): 
                                            ?>
                                                <script>
                                                    $(document).ready(function () {
                                                        // this function is triggered as soon as something changes in the form
                                                        $("select[name='status']").change(function () {
                                                            selectDialog('Unable to discontinue this client as they currently have an active booking or consultation. Please resolve this and try again.', $(this).val());
                                                        });

                                                        function selectDialog(text,value) {
                                                            if(value == "Discontinue"){
                                                                $("#status").val('Active');
                                                                return $('<div></div>').append(text)
                                                                    .dialog({
                                                                    resizable: true,
                                                                    modal: true,
                                                                    buttons: {
                                                                        "Exit": function () {
                                                                            $(this).dialog("close");
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                </script>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-4" id="comments-info">
                                            <div class="top-buffer">
                                                <label for="comments">General comments:</label>
                                            <textarea name="comments"  title="comments"class="form-control" id="exampleTextarea" rows="3"><?php echo $cli->comments; ?></textarea><br> 
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <?php //debug($statusActiveBookings->toArray()); ?>
                                            <button type="button" class="btn btn-danger customMtop pull-right"
                                            data-toggle="modal" data-target="#password-reset">Reset Password
                                            </button>
                                        </div>
                                    </div>
                            </div>
                            <?= $this->Form->end(); ?>
                            <div class="row">
                                <div class="row col-md-12 center-content">
                                    <div class="col-md-8">
                                        <h4>Students Detail:</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-1">
                                        <?= $this->Form->create(null, ['url' => ['action' => 'viewClient', $cli->token]]); ?>
                                        <button id="addStudentBtn" type="submit" class="btn btn-primary btn-xs pull-right top-buffer" name="addStudent" value="1">Add Student</button>
                                        <?= $this->Form->end(); ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $this->Html->link("Allocate Tutor & Create New Consultation", array('controller' => 'allocateLead', 'action' => 'allocateTutor', $cli->id), ['class' => 'btn btn-xs btn-success top-buffer', 'target' => '_blank', 'type' => 'button']); ?>
                                    </div>
                                </div>
                                <div class="row center-indent">
                                    <div id="studentsTbl" class="col-md-12">
                                        <table class="table table-striped responsive display table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <td>Student</td>
                                                <td>Year</td>
                                                <td>Subjects</td>
                                                <td>Action</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($cli->students as $student): ?>
                                                <tr>
                                                    <td><?= $student->name ?></td>
                                                    <td><?= $student->year_level->name ?></td>
                                                    <td>
                                                        <?php $subj = array();
                                                            foreach ($student->student_subjects as $subjects) {
                                                                $subj[] = $subjects->subject->name;
                                                            }
                                                            echo implode(", ", $subj); ?>
                                                    </td>
                                                    <td>
                                                        <?= $this->Form->create(null, ['url' => ['action' => 'viewClient', $cli->token]]); ?>
                                                        <div class="btn-group">
                                                        <?= $this->Form->button('Edit',['type' => 'submit', 'class' => 'btn btn-sm btn-info editStudentBtn', 'name' => 'editStudent','value' => $student->id]); ?>
                                                            <?php //echo $this->Html->link("Allocate Tutor", array('controller' => 'allocateLead', 'action' => 'student', $student->id), ['class' => 'btn btn-xs btn-success', 'target' => '_blank', 'type' => 'button']); ?>
                                                        </div>
                                                        <?= $this->Form->end(); ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row col-md-12 center-content">
                                <div class="col-md-12">
                                    <h4>Tutors:</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row center-indent">
                                <div class="col-md-12">
                                    <table id="leadTbl"
                                    class="table table-striped responsive display table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Student</td>
                                            <td>Year</td>
                                            <td>Subject</td>
                                            <td>Tutor</td>
                                            <td>Status</td>
                                            <td>Actions</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($leads as $lead): ?>
                                            <tr>
                                                <td><?= $lead->student->name ?></td>
                                                <td><?= $lead->student->year_level->name ?></td>
                                                <td><?php $subj = array();
                                                    foreach ($lead->lead_subjects as $subjects) {
                                                        $subj[] = $subjects->subject->name;
                                                    }
                                                    echo implode(", ", $subj); ?></td>
                                                    <td><?= $lead->tutor->first_name . " " . $lead->tutor->last_name ?></td>
                                                    <td><?= $lead->status ?></td>
                                                    <td>
                                                    <?= $this->Form->create(null, ['url' => ['action' => 'viewClient', $cli->token]]); ?>
                                                        <?= $this->Form->button('Edit Subjects', ['type' => 'submit', 'name' => 'editLeadSubjs', 'value' => $lead->id, 'class' => 'btn btn-xs btn-primary']); ?>
                                                    <?= $this->Form->end(); ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row center-content">
                                    <div class="col-md-12">
                                        <h4>Assessments:</h4>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row center-content">
                                    <div class="col-md-12">
                                        <ul class="nav nav-tabs">  
                                            <?php foreach ($cli->students as $key => $tabName):  ?>
                                                <li <?php if($key < 1){ echo "class='active'";} ?>><a href="<?= '#'.$tabName->id.''.$tabName->name ?>" data-toggle="tab"><h5><?= $tabName->name ?></h5></a></li>
                                            <?php endforeach; ?>  
                                        </ul>
                                    </div>
                                </div>
                                <div class="row center-content top-buffer">
                                    <div class="col-md-12 top-buffer">
                                        <div class="tab-content top-buffer">
                                            <?php foreach ($cli->students as $key => $tabContent):  ?>
                                                <div <?php if($key < 1){ echo "class='tab-pane fade active in'";}else{echo "class='tab-pane fade'";} ?> id="<?= $tabContent->id.''.$tabContent->name ?>">
                                                    <div class="row col-md-12 center-content">
                                                        <table class="table table-striped responsive display table-hover table-bordered top-buffer">
                                                            <thead>
                                                                <tr>
                                                                    <td>Assessment</td>
                                                                    <td>Completed</td>
                                                                    <td>Assigned Tutor</td>
                                                                    <td>Status</td>
                                                                    <td>Action</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($stdAssTypes as $assmnt): if ($assmnt->student->id == $tabContent->id) : ?>
                                                                    <tr>
                                                                        <td><?= $assmnt->assessment_type->type ?></td>
                                                                        <td><?= $assmnt->status == "Done" ? "Yes" : "No" ?></td>
                                                                        <td><?= $assmnt->tutor->first_name . ' ' . $assmnt->tutor->last_name ?></td>
                                                                        <td><?= $assmnt->status?></td>
                                                                        <td>
                                                                            <?php if($assmnt->status == "Done") {
                                                                                if ($assmnt->assessment_type_id > 2) {
                                                                                    echo $this->Html->link(
                                                                                        "View",
                                                                                        array('controller' => 'report', 'action' => 'printReport', $assmnt->id, $cli->token),
                                                                                        ['class' => 'btn btn-info btn-sm', 'target' => '_blank', 'type' => 'button']
                                                                                        );
                                                                                } else {
                                                                                    echo $this->Html->link(
                                                                                        "View",
                                                                                        array('controller' => 'report', 'action' => 'printReportLearning', $assmnt->id, $cli->token),
                                                                                        ['class' => 'btn btn-info btn-sm', 'target' => '_blank', 'type' => 'button']
                                                                                        );
                                                                                }
                                                                            } else {
                                                                                echo " - - - ";
                                                                            } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php endif; endforeach; ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <!-- Bookings Information START -->  
                       
                      <div class="row">
                    <div class="col-md-12"><hr/></div>
                    <div class="col-md-6">
                        <h4>Bookings Information:</h4>
                    </div>
                    <div class="col-md-6 statusPos">
                        <?php
                            echo $this->Form->create(null, [
                            'url' => ['action' => 'viewClient', $cli->token],
                            'method' => 'post'
                            ]);
                            ?>
                            
                            <label>Status: </label>
                         <select name="status" class="form-control" onchange="this.form.submit()" >
                                <?php 
                                foreach($programStatus as $stat): 
                                    ?>
                                <option <?php $sel = ($status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?php if($stat == "Tutor"):  echo "Scheduled";  elseif($stat == "Done"):  echo "Completed";  else: echo $stat; endif;  ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                     <div class="row center-content">
                    <div class="col-md-12">
                        <table id="bookingsTable"
                                class="table table-striped responsive display table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Amount</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                                
                                                foreach ($programDetails as $program): 
//                                                  echo $program->client->first_name;
                                            ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?= $program->invoice_number ?></td>
                                            <td><?= $program->invoice_amount ?></td>
                                            <td><?php
                                                if(!empty($program->client)):
                                                    echo $program->client->first_name . " " . $program->client->last_name;
                                                endif;
                                             ?></td>
                                            <td><?= $program->tutor->first_name ?> <?= $program->tutor->last_name ?></td>
                                            <td>    
                                                <?php 
                                                    if($program->status == "Tutor"):
                                                        echo "Scheduled";
                                                    elseif($program->status == "Done"):
                                                        echo "Completed";
                                                    else:
                                                        echo $program->status;
                                                    endif; 
                                                ?>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <?php if($program->status == "Pending"): ?>
                                                        <?= 
                                                        $this->Html->link(
                                                            "Create Lesson",
                                                            ['action' => 'createLessons', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary"]); 
                                                            ?>
                                                        <?php else: ?>
                                                            <?= 
                                                            $this->Html->link(
                                                                "View Details",
                                                                ['action' => 'details', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-warning"]); 
                                                                ?>
                                                            <?php endif; ?>
                                                            <?php 
//                                                             echo $this->Html->link(
//                                                                "Delete",
//                                                                ['controller' => 'bookings', 'action' => 'deleteProgram', $program->id, 'prefix' => 'admin'], ['escape' => false, 
//                                                                    'class' => "btn btn-xs btn-danger", 
//                                                                    'onclick' => "return confirm('Are you sure you want to Delete this booking?')"]); 
                                                                ?>
                                                            </div>
                                                        </td>
                                                    </tr>   
                                                    
                                                <?php endforeach; ?>
                                </tbody>
                            </table>
                    </div>
                </div>
                       <!-- Bookings Information END -->
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div id="password-reset" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Password Reset</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'passwordReset']]); ?>
                <div class="modal-body">
                    <div class="row center-content">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center">Please confirm if you want to reset tutor password?</p>
                            </div>
                        </div>
                        <?= $this->Form->hidden('token', ['value' => $cli->token]) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
<?php if (isset($stdDetails)) { ?>
    <div id="editStudentDia" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Student Details</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'studentUpdate']]); ?>
                <div class="modal-body">
                    <?= $this->Form->hidden('student_id', ['value' => $stdDetails]) ?>
                    <?php foreach ($client as $cli2) : foreach ($cli2->students as $student) : if ($student->id == $stdDetails) : ?>
                    <div class="row center-content">
                        <div class="col-md-12">
                            <div class="row top-buffer">
                                <div class="col-md-6">
                                    <label for="student">Student First Name:</label>
                                    <input type='text' value='<?= $student->name ?>' name="student_name"
                                    class='form-control student-name'
                                    required>
                                </div>
                                <div class="col-md-6">
                                    <label for="year-level">Year Level:</label>
                                    <select id="year-level" name='year_level'
                                    class="form-control" required>
                                    <option value='<?= $student->year_level->id ?>'
                                        selected='selected'><?= $student->year_level->name ?></option>
                                        <?php foreach ($yrlvls as $yrlvl) : ?>
                                            <option
                                            value='<?= $yrlvl->id ?>'><?= $yrlvl->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <label for="student">Subjects:</label>
                                <select id="student-subjects" name='subjects[]'
                                    class="form-control" multiple required>
                                    <?php foreach ($student->student_subjects as $selectedSubj) : ?>
                                        <option
                                        value='<?= $selectedSubj->subject->id ?>'
                                        selected><?= $selectedSubj->subject->name ?></option>
                                    <?php endforeach;
                                    foreach ($subjs as $subj) : ?>
                                    <option
                                    value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php endif; endforeach; endforeach; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#editStudentDia').modal('show');
            $('#year-level option').each(function () {
                $(this).siblings("[value='" + this.value + "']").remove();
            });
            $('#student-subjects option').each(function () {
                $(this).siblings("[value='" + this.value + "']").remove();
            });
            $('#student-subjects').multiSelect();
            $('.ms-container').css("width","100%");
        });
    </script>
<?php } ?>
<?php if (isset($addStudent)) { ?>
    <div id="addStudentDia" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Students:</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'addStudents']]); ?>
                <?= $this->Form->hidden('client_token', ['value' => $cli->token]) ?>
                <div class="modal-body">
                    <div class="row">
                        <div id="entry1" class="clonedInput col-md-12">
                            <div class="row col-md-12">
                                <p id="reference"><strong>Student Detail:</strong></p>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label>Student First
                                    Name:</label>
                                    <input type='text' name='addStdName[]' class='form-control student-name'
                                    required>
                                </div>
                                <div class="col-md-6">
                                    <span class="require-mark">&#42; </span><label>Year
                                    Level:</label>
                                    <select name='addYrLvlId[]' class="form-control" required>
                                        <option selected="selected" disabled></option>
                                        <?php foreach ($yrlvls as $yrlvl) : ?>
                                            <option
                                            value='<?= $yrlvl->id ?>'><?= $yrlvl->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 top-buffer">
                                <span class="require-mark">&#42; </span><label>Subjects:</label>
                                <div class="remove-cont">
                                    <select id="addSubj" name='addSubjId1[]' class="form-control new-student-subjects" multiple
                                    required>
                                    <?php foreach ($subjs as $subj) : ?>
                                        <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-12 top-buffer text-center">
                            <div class="btn-group">
                                <button type="button" id="remove-btn" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle" aria-hidden="true"></i> Remove
                                </button>
                                <button type="button" id="add-btn" class="btn btn-xs btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Add Student/s</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <div class="hidden">
        <select id="sub-cop" class="form-control" multiple required>
            <?php foreach ($subjs as $subj) : ?>
                <option
                value='<?= $subj->id ?>'><?= $subj->name ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <script>
        $(document).ready(function () {
            $('#addStudentDia').modal('show');
            $('.new-student-subjects').multiSelect();
            $('.ms-container').css("width","100%");
        });
    </script>
<?php } ?>
<?php if (isset($leadInfo)) { ?>
<div id="editSubjDia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lead Subjects:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'leadSubjUpdate']]); ?>
            <div class="modal-body">
                <?= $this->Form->hidden('lead_id', ['value' => $leadInfo->id]) ?>
                <div class="row top-buffer">
                    <div class="col-md-12">
                        <label for="student">Student Reg. Subj/s:</label>
                        <p class="text-center">
                        <?php $subj = array();
                            foreach ($leadInfo->student->student_subjects as $subjects) {
                                $subj[] = $subjects->subject->name;
                            }
                            echo implode(", ", $subj); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <div class="col-md-12 top-buffer">
                            <label for="student">Subjects:</label>
                            <select id="student-subjects" name='subjects[]' class="form-control" multiple required>
                                <?php foreach ($leadInfo->lead_subjects as $selectedSubj) : ?>
                                    <option value='<?= $selectedSubj->subject_id ?>' selected><?= $selectedSubj->subject->name ?></option>
                                <?php endforeach;
                                foreach ($regSubjects as $subj) : ?>
                                    <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#editSubjDia').modal('show');
        $('#year-level option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Available Subjects'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Lead Subjects'>",
            afterInit: function(ms){
                var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        $('.ms-container').css("width","100%");
    });
</script>
<?php } ?>

<?= $this->Html->script('admin/view-client2.js'); ?>