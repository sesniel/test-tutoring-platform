<?php $activeStats = array('Pending','Scheduled','Completed','Cancelled'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Booking Program</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <?= $this->Form->create(null, ['url' => ['controller' => 'Tutors', 'action' => 'updateLesson', $this->request->params['pass'][0]]]); ?>

                <div class="row col-md-12 top-buffer"></div>
                    <div class="col-md-12">
                        <table class="tbll table table-bordered">
<!--                            <tr><th colspan='5' class='bg-success'>General Information</th></tr>
                            <tr>                                    
                                <td>Type</td>
                                <td>Outcome</td>
                                <td>Topic</td>
                                <td>Note</td>
                                <td>Reason</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                        if(!empty($lessonDetails->assessment_type)):
                                            echo $lessonDetails->assessment_type->type;
                                        else:
                                            echo "General";
                                        endif;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(!empty($lessonDetails->major_outcome)):
                                            echo $lessonDetails->major_outcome->name;
                                        endif;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        if(!empty($lessonDetails->minor_outcome)):
                                            echo $lessonDetails->minor_outcome->code;
                                        endif;
                                    ?>
                                </td>
                                <td style="width:250px;"><?php echo '<p>' . htmlspecialchars($lessonDetails->note) . '</p>'; ?></td>
                                <td style="width:250px;"><?php echo '<p>' . htmlspecialchars($lessonDetails->reason) . '</p>'; ?></td>
                            </tr>-->
                            <tr><th colspan='6' class='bg-danger' >Update Information</th></tr>
                            <tr>
                                <td>Date</td>
                                <td>Time</td>
                                <td>Length(hours)</td>
                                <td>Rate</td>
                                <td>Status</td>
                                <td>Payrol Date</td>
                            </tr>
                            <tr id="leadRow1" class="clonedInput">
                                <td>
                                    <div class="input-group"><input class="form-control edate specifyWidth"
                                                                    value="<?php echo date('d/m/Y', strtotime($lessonDetails->date))  ?>" type="text" name="lessonDates" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span></div>
                                </td>
                                <td>
                                    <div class="input-group"><input class="form-control lessonTime specifyWidth" id="lessonTime"
                                                                    value="<?php echo date('H:i A', strtotime($lessonDetails->time))  ?>" type="text" name="lessonTime" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-time"></i></span></div>
                                </td>
                                <td>
                                    <input type="text" name="length" class="form-control" value="<?= $lessonDetails->length ?>" maxlength="5" required/>
                                </td>
                                <td>
                                    <?php //debug($rateDetails->toArray()) ?>
                                    <select class='form-control specifyWidth' name='rate_id' >
                                        <option selected disabled="">Pay Rate</option>
                                        <?php 
                                            foreach($rateDetails as $rate): 
                                                if($lessonDetails->rate_id > 0):
                                                    $active = ($rate->id == $lessonDetails->rate_id) ? " selected " : "";
                                                else:
                                                    $active = ($rate->id == $lessonDetails->program->tutor->rate->id) ? " selected " : "";
                                                endif;
                                                echo "<option $active value='".$rate->id."' >".$rate->name."</option>";
                                        ?>
                                        
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <select class='form-control specifyWidth' name='status' >
                                    <?php
                                        foreach($activeStats as $stat):
                                            
                                            $active = ($stat == $lessonDetails->status) ? " selected " : "";
                                            
                                            
                                            echo "<option $active value='".$stat."' >".$stat."</option>";
                                            
                                        endforeach;
                                    ?>
                                    </select>
                                </td>
                                <td>
                                    <?php 
                                        if(!empty($lessonDetails->payroll_date)):
                                            $passPay = date('d/m/Y', strtotime($lessonDetails->payroll_date)); 
                                        else:
                                            $passPay = "";
                                        endif;
                                    ?>
                                    <div class="input-group"><input class="form-control pdate specifyWidth" 
                                                                    value="<?= $passPay ?>" type="text" name="payrolldate"><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span></div>  
                                </td>
                            </tr>
                            
                                    </table>
                        </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="form-group col-md-12 text-center">
                            <button id="cpBtn" class="btn btn-default">Update Lesson</button>
                        </div>
                    </div>
                    </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {

    $('.edate').datepicker({
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });
    
    $('.pdate').datepicker({
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });
    
});
</script>

<?php // debug($lessonDetails); ?>
    


