<?php $programStatus = array('Completed Training', 'All', 'My Active Applicants', 'New Applicants', 'Active Applicants', 'Applicants to be Finalized'); asort($programStatus); 
      $appStatus = array('My New Applicants', 'My Reviewed Applicants', 'New', 'Reviewed', 'Accepted', 'Abandoned'); 
?>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
         <!-- Recruitment Page -->
            <div class="widget-title">
                <h2><strong>New Tutor Bank Details To Be Processed</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-sm-9 col-md-9">
                                <p>This is a list of recently hired tutors whom we're still awaiting their response to the Employment Information Request email. Once received enter the details into Xero and update the Tutors profile accordingly.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Tutor Name</th>
                                        <th>Date created</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($tutorDetails as $tutor): ?>
                                            <?php if($tutor->module_user->position == 7 && $tutor->bank_details == "No"): ?>
                                        <tr>
                                            <td><?= $tutor->first_name . " " . $tutor->last_name; ?></td>
                                            <td><?= date("Y/m/d", strtotime($tutor->created)); ?></td>
                                            <td><?php 
                                            echo $this->Html->link(" View Profile ",
                                                            array('controller' => 'tutors', 'action' => 'tutorView', $tutor->token),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
                                             ?></td>
                                        </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
<?= $this->Html->script('admin/training.js'); ?>
<?php //debug($tutors->toArray()); ?>