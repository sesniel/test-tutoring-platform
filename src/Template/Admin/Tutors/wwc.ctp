<?php $programStatus = array('Completed Training', 'All', 'My Active Applicants', 'New Applicants', 'Active Applicants', 'Applicants to be Finalized'); asort($programStatus); 
      $appStatus = array('My New Applicants', 'My Reviewed Applicants', 'New', 'Reviewed', 'Accepted', 'Abandoned'); 
?>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
         <!-- Recruitment Page -->
            <div class="widget-title">
                <h2><strong>Working With Children Checks to Be Finalised</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-sm-9 col-md-9">
                                <p>This is a list of those tutors recently appointed whom we're waiting for their final working with children approvals (this should take no longer than 2 weeks).</p>
                           </div><br>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'recruitment'],
                                        'method' => 'post'
                                    ]);
                                ?>
                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><br>
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date created</th>
                                        <th>Working With Children Number</th>
                                        <th>Working With Children Expiry</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($tutorDetails as $tutor): ?>
                                            <?php if(empty($tutor->blue_card_number && $tutor->blue_card_expiry)): ?>
                                        <tr>
                                            <td><?= $tutor->first_name . " " . $tutor->last_name; ?></td>
                                            <td><?= date("Y/m/d", strtotime($tutor->created)); ?></td>
                                            <td><?= $tutor->blue_card_number; ?></td>
                                            <td><?php if(!empty($tutor->blue_card_expiry)) { echo date("Y/m/d", strtotime($tutor->blue_card_expiry));}?></td>
                                            <td><?php 
                                            echo $this->Html->link(" View Profile ",
                                                            array('controller' => 'tutors', 'action' => 'tutorView', $tutor->token),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
                                             ?></td>
                                        </tr>
                                            <?php endif; ?>
                                      <?php endforeach; ?>      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
              
            </div>
            <!-- Expiry -->
            <div class="widget-title">
                <h2><strong>Expiring Working With Children Checks</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                           <div class="col-sm-9 col-md-9">
                                <p>This is a list of those tutors who's blue cards expire in the next couple of months and must be renewed if continuing.</p>
                           </div><br>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'recruitment'],
                                        'method' => 'post'
                                    ]);
                                ?>
                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered trainingTable">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date created</th>
                                        <th>Working With Children Number</th>
                                        <th>Working With Children Expiry</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($tutorDetails as $tutor): ?>
                                            <?php if(!empty($tutor->blue_card_expiry)): ?>
                                            <?php if(date("Y/m/d", strtotime("+60 days")) >= date("Y/m/d", strtotime($tutor->blue_card_expiry))): ?>
                                        <tr>
                                            <td><?= $tutor->first_name . " " . $tutor->last_name; ?></td>
                                            <td><?= date("Y/m/d", strtotime($tutor->created)); ?></td>
                                            <td><?= $tutor->blue_card_number; ?></td>
                                            <td><?php if(!empty($tutor->blue_card_expiry)) { echo date("Y/m/d", strtotime($tutor->blue_card_expiry));}?></td>
                                            <td><?php 
                                            echo $this->Html->link(" View Profile ",
                                                            array('controller' => 'tutors', 'action' => 'tutorView', $tutor->token),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
                                             ?></td>
                                        </tr>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
              
            </div>
        </div>
        
    </div>
</div>
<?= $this->Html->script('admin/training.js'); ?>
<?php //debug($tutors->toArray()); ?>