<?php
$programStatus = array('All', 'Active', 'Pending', 'In Progress', 'Completed', 'Cancelled');
$tutorStatus = array('Active', 'Discontinue');
$availabilityStatus = ['Yes', 'No'];
$hours = ['1', '2', '3', '4', '5+'];
$bankDetails = ['Yes', 'No'];
?>

<style>
    
.label {
    font-size: 20px;
    
}

.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.modal-md {
    max-width: 500px;
}
.trash { 
    color:rgb(209, 91, 71);
    font-size: 20px;
}
.view { 
    color:rgb(248, 148, 6); 
    font-size: 20px;
}
.panel-body { 
    padding:0px;
}
.panel-footer .pagination { 
    margin: 0;
}
.panel .glyphicon,.list-group-item .glyphicon { 
    margin-right:5px;
}
.panel-body .radio, .checkbox { 
    display:inline-block;margin:0px;
}
.panel-body input[type=checkbox]:checked + label { 
    text-decoration: line-through;
    color: rgb(128, 144, 160);
}
.list-group-item:hover, a.list-group-item:focus {
    text-decoration: none;
    background-color: rgb(245, 245, 245);
}
.list-group { 
    margin-bottom:0px;
}
</style>
<?php foreach ($tutor as $tutor) : ?>
    <div id="tutor-view" class="widget-box" role="dialog">
        
        <div class="widget-title">
                <h2><strong>Tutor Profile</strong></h2>
            </div>
       
        <div class="modal-body row">
            <?= $this->Form->create(null, ['url' => ['action' => 'tutorUpdate']]); ?>
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-8">
                        <h4>Contact Info:</h4>
                        <hr>
                    </div>
                    <div class="col-md-3 customMtop top-buffer">
<!--                        <p class="pull-left "><strong>Full Platform Access</strong></p>
                        <p class="col-md-1 pull-left" id="fullAccess">
                            <?= $this->Form->hidden('id', ['value' => $tutor->id]); ?>
                            <?= ($tutor->access) ?
                                $this->Form->checkbox('Study Skills', ['name' => 'access', 'onChange' => 'this.form.submit()', 'id' => 'full']) :
                                $this->Form->checkbox('Study Skills', ['name' => 'access', 'onChange' => 'this.form.submit()', 'id' => 'full']);
                            ?>
                            
                         </p> -->
                    </div>
        
                    <table class="pull-right" style="position:relative; right: 45px;">
                        <tr>
                            <td>
                                <button type="button" id="edit-tutor-btn"
                                        class="btn btn-primary btn-xs pull-right top-buffer">Edit Contact
                                </button>

                                <button type="submit" id="update-tutor-btn"
                                        class="btn btn-success btn-xs hidden pull-right top-buffer">
                                    Update
                                </button> 
                            </td>
                            <td>
                                <?php 
                                    $image = $this->Html->image("gmap.ico", ['border' => '0', 'class' => 'img-responsive', 'style' => 'width: 25px; height: 25px;']); 

                                    echo $this->Html->link($image,
                                    ['controller' => 'maps', 'action' => 'index',$tutor->id, 'prefix' => 'admin'], ['escape' => false, 'target' => '_blank']); ?>
                            </td>
                        </tr>
                    </table>
                 <!-- Tutor Status END --> 
                </div>
                <?= $this->Form->hidden('token', ['value' => $tutor->token]) ?>
                <div class="row center-indent tutor-info">
                    <hr>
                    <div class="col-md-3">
                        <label for="fname">First Name:</label>
                        <input type="text" id="fname" name="first_name"
                               value="<?= $tutor->first_name ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip" title="First Name">
                    </div>
                    <div class="col-md-3">
                        <label for="lname">Last Name:</label>
                        <input type="text" id="lname" name="last_name"
                               value="<?= $tutor->last_name ?>"
                               class="form-control" required="required"
                               data-toggle="tooltip" title="Last Name">
                    </div>
                    <div class="col-md-3">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control" id="email"
                               name="email" value="<?= $tutor->email ?>" title="Email"
                               required>
                    </div>
                    <div class="col-md-3">
                        <label for="mobile">Mobile:</label>
                        <input type="text" class="form-control" id="mobile" name="mobile"
                               value="<?= $tutor->mobile ?>"
                               title="Mobile" maxlength="10">
                    </div>
                </div>
                <div class="row center-indent tutor-info">
                    <div class="col-md-3">
                        <label for="phone">Phone:</label>
                        <input type="text" id="phone" name="phone"
                               value="<?= $tutor->phone ?>"
                               class="form-control"
                               data-toggle="tooltip" title="First Name">
                    </div>
                    
                    <div class="col-md-3">
                        <label for="street_number">Street#:</label>
                        <input type="text" id="street_number" name="street_number"
                               value="<?= $tutor->street_number ?>" class="form-control"
                               data-toggle="tooltip"
                               title="Street Number">
                    </div>
                    <div class="col-md-3">
                        <label for="street_name">Street Name:</label>
                        <input type="text" id="street_name" name="street_name"
                               value="<?= $tutor->street_name ?>"
                               class="form-control"
                               data-toggle="tooltip" title="Street Name">
                    </div>
                    <div class="col-md-3">
                        <label for="suburb">Suburb:</label>
                        <input type="text" id="suburb" name="suburb"
                               value="<?= $tutor->suburb ?>"
                               class="form-control"
                               data-toggle="tooltip"
                               title="Suburb">
                    </div>
                </div>
                <div class="row center-indent tutor-info">
                    <div class="col-md-3">
                        <label for="suburb">Postcode:</label>
                        <input type="text" id="postcode" name="postcode"
                               value="<?= $tutor->postcode ?>"
                               class="form-control"
                               data-toggle="tooltip"
                               title="Postcode">
                    </div>
                    <div class="col-md-3">
                        <label for="suburb">State:</label>
                        <select id="state" name='state' class="form-control"
                                        value="<?= $state ?>" required>
                                    <?php foreach ($states as $state) : 
                                        if($tutor->state_id == $state->id):?>
                                        <option
                                            value='<?= $state->id ?>'><?= $state->name ?></option>
                                    <?php endif; endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3 text-center">
                        <button type="button" class="btn btn-danger customMtop"
                                data-toggle="modal" data-target="#password-reset">Reset Password
                        </button>
                    </div>
                    
                    <div class="col-md-3 text-center"></div>
                    <div class="col-md-3 text-center"></div>
                   
                </div>
               
            </div>
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-8">
                        <h4>General:</h4>
                        <hr>
                    </div>
                    <div class="col-md-3 customMtop top-buffer"></div>
                </div>
                <div class="row center-indent tutor-info">
                    <hr>
                    <div class="col-md-3 statusPos">
                        <?php
                            echo $this->Form->create(null, [
                            'url' => ['action' => 'tutorView', $tutor->token ],
                            'method' => 'post'
                            ]);
                            ?>
                            
                            <label>Status: </label>
                            <select name="status" class="form-control" onchange="this.form.submit()" >
                                <?php 
                                foreach($programStatus as $stat): 
                                    ?>
                                <option <?php $sel = ($status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?php echo $stat; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="col-md-3">
                        <label for="rate_name">Pay Rates:</label>
                        <select name="rate_id" id="rate_name" class="form-control">
                        <?php foreach($rateDetails as $rate): ?>
                            <option <?php $sel = ($tutor->rate_id === $rate->id ? "selected" : ""); ?> <?= $sel ?> value="<?= $rate->id ?>"><?= $rate->name ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="rate_name">Regional Manager:</label>
                        <select name="owner_id" id="owner" class="form-control">
                        <?php foreach($adminSelect as $adm): ?>
                            <option <?php $sel = ($tutor->owner_id === $adm->id ? "selected" : ""); ?> <?= $sel ?> value="<?= $adm->id ?>"><?= $adm->first_name ?> <?= $adm->last_name ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3 customMtop">
                        <p class="pull-left "><strong>Full Platform Access</strong></p>
                        <p class="col-md-1 pull-left">
                            <?= $this->Form->hidden('id', ['value' => $tutor->id]); ?>
                            <?= ($tutor->access) ?
                                $this->Form->checkbox('Study Skills', ['name' => 'access', 'checked', 'id' => 'full']) :
                                $this->Form->checkbox('Study Skills', ['name' => 'access', 'id' => 'full']);
                            ?>
                            
                         </p> 
                    </div>
                </div>
                <div class="row center-indent tutor-info">
                    <div class="col-md-3">
                        <label for="training">Training Status:</label>
                        <select name="training" id="training" class="form-control">
                        <?php foreach($statusDetails as $stats): ?>
                            <?php if(!empty($tutor->module_user)): ?>
                            <option <?php $sel = ($tutor->module_user->status === $stats ? "selected" : ""); ?> <?= $sel ?> value="<?= $stats ?>"><?= $stats ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="card_number">Working With Children Number:</label>
                        <input type="text" id="blue_card_number" name="blue_card_number"
                               value="<?= $tutor->blue_card_number ?>" class="form-control"
                               data-toggle="tooltip"
                               title="Card Number">
                   
                    </div>
                    <div class="col-md-3">
                        <label for="expiry_date">Working With Children Expiry Date:</label>
                        <input type="text" id="blue_card_number" name="blue_card_expiry"
                               value="<?php if(!empty($tutor->blue_card_expiry)) { echo date("d/m/Y", strtotime($tutor->blue_card_expiry));} ?>" class="form-control"
                               data-toggle="tooltip"
                               title="Card Number">
                    </div>
                    
                </div>
                <div class="row center-indent tutor-info">
                    <div class="col-md-6">
                        <label for="comments">General comments:</label>
                        <textarea name="comments"  title="comments"class="form-control" id="exampleTextarea" rows="3"><?php echo $tutor->comments; ?></textarea><br> 
                    </div>
                    <div class="col-md-6">
                        <label for="biography">Biography:</label>
                        <textarea name="biography"  title="comments"class="form-control" id="exampleTextarea" rows="3"><?php echo $tutor->biography; ?></textarea><br> 
                    </div>
                    <div class="col-md-3 text-center"></div>
                    <div class="col-md-3 text-center"></div>
                   
                </div>
               
            </div>
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-8">
                        <h4>Availability:</h4>
                        <hr>
                    </div>
                    <div class="col-md-3 customMtop top-buffer"></div>
                </div>
                <div class="row center-indent tutor-info">
                    <hr>
                    <div class="col-md-3">
                        <label for="rate_name">Available for new clients:</label>
                       <select name="availability" id="training" class="form-control">
                          
                        <?php foreach($availabilityStatus as $stats): ?>
                           <option <?php $sel = ($tutor->availability === $stats ? "selected" : ""); ?> <?= $sel ?> value="<?= $stats ?>"><?= $stats ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="rate_name">How many more hours would you like?</label>
                        <select name="hours" id="training" class="form-control" name="hours">
                        <?php foreach ($hours as $hour): ?>
                            
                            <option <?php $sel = ($tutor->hours === $hour ? "selected" : ""); ?> <?= $sel ?> value="<?= $hour ?>"><?= $hour ?></option>
                            
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-6 center-content">
                        <label for="subjects">List of Subjects:</label>
                        <div class="subjects">
                            <select multiple="multiple" id="available-subjects" name="subjects[]"
                                    class="form-control">
                                
                                <?php foreach($tutorSubjects as $subj): ?>
                                <option selected value="<?= $subj->subject->id ?>"><?= $subj->subject->name; ?></option>
                                <?php endforeach; ?>
                                <?php foreach($subjectsList as $subjList): ?>
                                <option value="<?= $subjList->id ?>" ><?= $subjList->name?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <?= $this->Form->end(); ?>
          <!--  <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-12">
                        <h4>General comments:</h4>
                        <hr>
                        <div class="col-md-6">
                            <textarea name="ans_3"  title="Answer_3"class="form-control" id="exampleTextarea" rows="3"></textarea><br>
                        </div>
                    </div>
                    
                </div>
            </div> -->
            
            
             <?php if (isset($tutor->application_answer )): ?>
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-12">
                        <h4 class="applicant-info">Tutor Application Information  <i class="glyphicon glyphicon-triangle-bottom details" ></i></h4>
                        <hr>
                    </div>
                </div>
            </div>
           
            
            <div class="row app-info" >
                <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h5 id="academic-background">Academic Background <i class="glyphicon glyphicon-triangle-top" ></i></h5>
                    <hr>
                </div>
                </div>
            <div class="row center-indent acad-background">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">High School Result (OP/ATAR):</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['HighSchoolResultOP_ATAR'];
                    ?></i></p></span>
                </div>
                <div class="col-md-4">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Are you currently studying, if so what?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Are_you_currently_studying_if_so_what'];
                    ?></i></p></span>
                </div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Where are you studying?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Where_are_you_studying'];
                    ?></i></p></span>
                </div>
            </div>  
            <div class="row center-indent acad-background">
                <div class="col-md-2">

                </div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">GPA:</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['GPA'];
                    ?></i></p></span>
                </div>
                <div class="col-md-4">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Do you have a drivers license and access to your own car?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Do_you_have_a_drivers_license_and_access_to_your_own_car'];
                    ?></i></p></span>
                </div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">How many hours a week would you like to tutor?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['How_many_hours_a_week_would_you_like_to_tutor'];
                    ?></i></p></span>
                </div>
                
            </div>
            <div class="row center-indent acad-background">
                <div class="col-md-2"></div>
                <div class="col-md-3">
                    <span class="col-md-12"><label for="card_number" class="text-justify">What Subjects do you feel you are able to tutor and to what level?</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $subj = array();
                                                                   foreach ($tutor->tutor_subjects as $subjects) {
                                                                       $subj[] = $subjects->subject->name;
                                                                   }
                                                                   echo implode(", ", $subj);
                    ?></i></p></span>
                </div>
                <div class="col-md-4">
                    <span class="col-md-12"><label for="card_number" class="text-justify">Explain why you think you would make an excellent tutor for these subjects.</label></span><br>
                    <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects'];
                    ?></i></p></span>
                </div>
            </div>
            
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h5 id="personal-background">Personal Background & Ability <i class="glyphicon glyphicon-triangle-top" ></i></h5>
                    <hr>
                </div>
            </div>
            <div class="row center-indent personal-background">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <span class="col-md-12"><label for="card_number" class="text-justify">Describe how you would use your personal experience to assist your students, and why you believe it would help them.</label></span><br>
                        <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects'];
                    ?></i></p></span>
                    </div>
                    
                </div>
            </div>
            <div class="row center-indent personal-background">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <span class="col-md-12"><label for="card_number" class="text-justify">Given sessions are often 1-2 hours long, how long would you be comfortable travelling in order to get to a session? (Note: hourly rates vary from $27-35/hr, travel time not included)</label></span><br>
                        <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects'];
                    ?></i></p></span>
                    </div>
                    
                </div>
            </div>
            <div class="row center-indent personal-background">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-10">
                        <span class="col-md-12"><label for="card_number" class="text-justify">Lastly please describe why you would like to become an Tutor2You Tutor and why do you think you would represent our company well with clients.</label></span><br>
                        <span class="col-md-12 bg-success"><p><i><?php $ans = $tutor->application_answer->details;
                                                                   $json = json_decode($ans, true);
                                                                   echo $json['Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects'];
                    ?></i></p></span>
                    </div>
                    
                </div>
            </div>
            </div>
                <?php endif; ?>
            <?php endforeach; ?>
            
            <div class="row">
                <div class="row col-md-12 center-content">
                    <div class="col-md-12">
                        <h4>Clients:</h4>
                        <hr>
                    </div>
                </div>
                <div class="row center-indent">
                    <div class="col-md-12">
                        <table id="leadTbl"
                               class="table table-striped responsive display table-hover table-bordered">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Client</td>
                                <td>Student</td>
                                <td>Year</td>
                                <td>Subjects</td>
                                <td>Status</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($leads as $lead): ?>
                                <tr>
                                    <td><?= $lead->id ?></td>
                                    <td><?= $lead->client->first_name . ' ' . $lead->client->last_name ?></td>
                                    <td><?= $lead->student->name ?></td>
                                    <td><?= $lead->student->year_level->name ?></td>
                                    <td><?php $subj = array();
                                        foreach ($lead->lead_subjects as $subjects) {
                                            $subj[] = $subjects->subject->name;
                                        }
                                        echo implode(", ", $subj); ?></td>
                                    <td><?= $lead->status ?></td>
                                    <td>
                                        <?= $this->Form->create(null, ['url' => ['action' => 'tutorView', $tutor->token]]); ?>
                                        <div class="btn-group">
                                        <?= $this->Html->link(
                                                "View",
                                                array('controller' => 'clients', 'action' => 'viewClient', $lead->client->token),
                                                array('class' => 'btn btn-sm btn-info','target' => '_blank', 'escape' => false)
                                            ); 
                                        ?>
                                        <?= $this->Form->button('Edit Subjects', ['type' => 'submit', 'name' => 'editLeadSubjs', 'value' => $lead->id, 'class' => 'btn btn-xs btn-primary']); ?>
                                        </div>
                                        <?= $this->Form->end(); ?>
                                    </td>
                                    
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </div>
                        <!-- Bookings Information START -->
                        
                    <div class="row">
                    <div class="col-md-12"><hr/></div>
                    <div class="col-md-8">
                        <h4>Bookings Information:</h4>
                    </div>
                    <div class="col-md-4 statusPos">
                        <?php
                            echo $this->Form->create(null, [
                            'url' => ['action' => 'tutorView', $tutor->token ],
                            'method' => 'post'
                            ]);
                            ?>
                            
                            <label>Status: </label>
                            <select name="status" class="form-control" onchange="this.form.submit()" >
                                <?php 
                                foreach($programStatus as $stat): 
                                    ?>
                                <option <?php $sel = ($status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?php echo $stat; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    </div>
                     <div class="row center-content">
                    <div class="col-md-12">
                        <table id="bookingsTable"
                                class="table table-striped responsive display table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Amount</th>
                                        <th>Client</th>
                                        <th>Tutor</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                                foreach ($programDetails as $program):
                                            ?>
                                        <tr>
                                            <td><?= $program->id ?></td>
                                            <td><?= $program->invoice_number ?></td>
                                            <td><?= $program->invoice_amount ?></td>
                                            <td><?php
                                                if(!empty($program->client)):
                                                    echo $program->client->first_name . " " . $program->client->last_name;
                                                endif;
                                             ?></td>
                                            <td><?= $program->tutor->first_name ?> <?= $program->tutor->last_name ?></td>
                                            <td>    
                                                <?php 
                                                        echo $program->status;
                                                ?>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <?php if($program->status == "Pending"): ?>
                                                        <?= 
                                                        $this->Html->link(
                                                            "Create Lesson",
                                                            ['action' => 'createLessons', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary"]); 
                                                            ?>
                                                        <?php else: ?>
                                                            <?= 
                                                            $this->Html->link(
                                                                "View Details",
                                                                ['action' => 'details', 'controller' => 'bookings', $program->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-warning"]); 
                                                                ?>
                                                            <?php endif; ?>
                                                            </div>
                                                        </td>
                                                    </tr>   
                                                <?php endforeach;  ?>
                                </tbody>
                            </table>
                    </div>
                </div>
                     
                <!-- HR Documents -->
                <?php foreach($documentDetails as $document): ?>
               <?php if (!empty($document)): ?>
                <div class="row">
                    <div class="col-md-12"><hr/></div>
                    <div class="col-md-6">
                        <h4>HR Documents:</h4>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?> 
                
                
                <div class="row">
                     <?php foreach($documentDetails as $document): ?>
                <?php if(!empty($document)):?>
                    <div class="col-md-7">
                        <div class="panel panel-primary">
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-list"></span>
                            <div class="pull-right action-buttons">

                            </div>
                        </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <?php foreach($documentDetails as $document):?>
                        <li class="list-group-item files">
                            <div class="checkbox">
                                <?= $document->reference; ?>
                            </div>
                            <div class="pull-right action-buttons">
                                <?php echo $this->Html->link(
                                    "Delete",
                                    ['action' => 'deleteDocument', $document->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-danger"]); 
                                ?>
                                <a href="<?= $this->request->webroot . 'web/hrdocs/' . $document->filename?>" class="btn btn-sm btn-warning" target="_blank">View</a>    
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                
            </div>
                
                    </div>
                    <?php endif; ?>
            <?php endforeach; ?>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Upload Files</button>
                    </div>
                    <!--<?=  $this->Form->create(null, ['url' => ['action' => 'upload'], 'type' => 'file']); ?>
                    <div class="col-md-3">
                        <label>Select file to upload (Note: Please rename the title of the document)</label>
                            <input id="input-b1" name="hrdocs" type="file" class="file upload">
                            <input id="upload" type="submit" value="Upload file" class="btn btn-primary btn-xs top-buffer"></input>
                    </div>
                    <?= $this->Form->end(); ?>
                    -->
                </div>
                    </div>

                </div>
            
        </div>
                
    <!-- START: Modal for Uploading Files -->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Enter reference filename.</h4>
            </div>
            
           <?= $this->Form->create(null, ['url' => ['action' => 'upload'], 'type' => 'file']); ?>
            <?= $this->Form->hidden('tutor_id', ['value' => $tutor->id]) ?>
            
            <div class="modal-body row center-table">
                <div class="col-md-12">
                    <input type="text" class="form-control" name="reference" placeholder="(e.g. Superannuation)"title="Email" required>
                </div>
            </div>
            <div class="modal-footer center-content">
                <div class="col-md-3 top-buffer">
                    <input type="file" name="hrdocs" required></input>
                </div>
                <div class="col-md-3 pull-right">
                    <button type="submit" name="hrdocs" class="btn btn-default">Upload File</button>
                </div>
                
                
            </div>
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>
    
    <!-- END: Modal for Uploading Files -->
    </div>
    <div id="password-reset" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Password Reset</h4>
                </div>
                <?= $this->Form->create(null, ['url' => ['action' => 'passwordReset']]); ?>
                <div class="modal-body">
                    <div class="row center-content">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center">Please confirm if you want to reset tutor password?</p>
                            </div>
                        </div>
                        <?= $this->Form->hidden('token', ['value' => $tutor->token]) ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>

<?php if (isset($leadInfo)) { ?>
<div id="editSubjDia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lead Subjects:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'leadSubjUpdate']]); ?>
            <div class="modal-body">
                <?= $this->Form->hidden('lead_id', ['value' => $leadInfo->id]) ?>
                <div class="row top-buffer">
                    <div class="col-md-12">
                        <label for="student">Student Reg. Subj/s:</label>
                        <p class="text-center">
                        <?php $subj = array();
                            foreach ($leadInfo->student->student_subjects as $subjects) {
                                $subj[] = $subjects->subject->name;
                            }
                            echo implode(", ", $subj); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <div class="col-md-12 top-buffer">
                            <label for="student">Subjects:</label>
                            <select id="student-subjects" name='subjects[]' class="form-control" multiple required>
                                <?php foreach ($leadInfo->lead_subjects as $selectedSubj) : ?>
                                    <option value='<?= $selectedSubj->subject_id ?>' selected><?= $selectedSubj->subject->name ?></option>
                                <?php endforeach;
                                foreach ($regSubjects as $subj) : ?>
                                    <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        
        
        $('#editSubjDia').modal('show');
        $('#year-level option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Available Subjects'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Tutor Subjects'>",
            afterInit: function(ms){
                var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        $('.ms-container').css("width","100%");
    
    
    });

</script>
<?php }
    //debug($statusDetails);
?>
<?= $this->Html->script('admin/tutors-view.js'); ?>