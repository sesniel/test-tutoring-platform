<?php $programStatus = array('All', 'Active', 'Discontinue'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Tutors</strong><?php echo $this->Html->link(
                                    "Create Tutor",
                                    ['controller' => 'addTutor', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-success pull-right", 'target' => '_blank']); ?></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <p>These are the list of tutors that are currently on the system, you can search a tutor
                                    by typing any keywords in the input box at the top right corner of the table.</p>
                                
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                                Status: 
                                <select name="status" class="form-control" onchange="this.form.submit()" >
                                    <?php 
                                        foreach($programStatus as $stat): 
                                    ?>
                                        <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tutorsTbl"
                                       class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date Registered</th>
                                        <th>Rates</th>
                                        <th>Suburb</th>
                                        <th>Status</th>
                                        <th>Email address</th>
                                         <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tutors as $tutor): ?>
                                        <tr>
                                            <td><?= $tutor->first_name . ' ' . $tutor->last_name ?></td>
                                            <td><?= date_format($tutor->created, 'Y/m/d') ?></td>
                                            <td><?php if (!empty($tutor->rate->name)){
                                                echo $tutor->rate->name;
                                            }
                                                ?></td>
                                            <td><?= $tutor->suburb ?></td>
                                            <td><?= $tutor->status ?></td>                                            
                                            <td><?= $tutor->email ?></td>
                                            
                                         <!--   <td align="cneter">
                                                <?= $this->Form->create(null, ['url' => ['action' => 'fullAccess']]); ?>
                                                <?= $this->Form->hidden('id', ['value' => $tutor->id]); ?>
                                                <?= ($tutor->access) ?
                                                    $this->Form->checkbox('Study Skills', ['name' => 'access', 'checked', 'onChange' => 'this.form.submit()']) :
                                                    $this->Form->checkbox('Study Skills', ['name' => 'access', 'onChange' => 'this.form.submit()']);
                                                ?>
                                                <?= $this->Form->end(); ?>
                                            </td> -->
                                            <td>
                                                <div class="btn-group">
                                                <?php
                                                    if(!empty($tutor->module_user)):

                                                        echo $this->Html->link(
                                                            " View Training ",
                                                            array('controller' => 'trainings', 'action' => 'view', $tutor->id),
                                                            ['class' => 'btn btn-sm btn-primary']
                                                        );
                                                                                                            
                                                    else:
                                                        
                                                        
                                                        echo $this->Html->link(
                                                            "Activate Training",
                                                            array('action' => 'activate-training', $tutor->token),
                                                            ['class' => 'btn btn-sm btn-primary']
                                                        );
                                                        
                                                    endif;
                                                ?>
                                                    <?php
                                                        echo $this->Html->link(
                                                            "View",
                                                            array('action' => 'tutorView', $tutor->token, $tutor->id),
                                                            ['class' => 'btn btn-sm btn-info', 'target' => '_blank']
                                                        );
//                                                      echo $this->Form->button('View', ['type' => 'button',
//                                                        'class' => 'btn btn-sm btn-info viewTutor', 'value' => $tutor->token]); ?>
                                                    <?=
                                                    $this->Form->button('Status', ['type' => 'button',
                                                        'class' => 'btn btn-sm btn-warning changeStatusBtn', 'data-toggle' => 'modal',
                                                        'data-target' => '#change-status', 'value' => $tutor->token]);
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-container"></div>
<div id="change-status" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Change of Status</h4>
            </div>
            <?php echo $this->Form->create(null, ['url' => ['action' => 'changeStatus']]);
            echo $this->Form->hidden('token', ['id' => 'changeStatus']) ?>
            <div class="modal-body">
                <div class="row center-content">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">Are you sure you want to change the status?</p>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <select name='status' class="form-control" required>
                                <option selected disabled> Please select Status</option>
                                <option value="Active">Active</option>
                                <option value="Discontinue">Discontinue</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?Php // debug($tutors->toArray()); ?>
<?= $this->Html->script('admin/tutors1.js'); ?>