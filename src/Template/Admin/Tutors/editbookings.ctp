<?php $bookingStatus = array("Pending", "In Progress", "Completed", "Cancel"); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Booking</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <?= $this->Form->create(null, ['url' => ['action' => 'updateProgram', $programDetails->id]]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <h3><strong>Client:</strong> <?= $programDetails->client->first_name ?> <?= $programDetails->client->last_name ?></h3>
                    </div>
                    <div class="col-md-6">
                        <h3><strong>Tutor:</strong> <?= $programDetails->tutor->first_name ?> <?= $programDetails->tutor->last_name ?></h3>
                    </div>
                    <div class="col-md-3">
                        <h5><strong>Invoice Number:</strong> </h5>
                        <input type="text" value='<?= $programDetails->invoice_number ?>' class='form-control' name='number' />
                    </div>
                    <div class="col-md-3">
                        <h5><strong>Invoice Amount:</strong> </h5>
                        <input type="text" value='<?= $programDetails->invoice_amount ?>' class='form-control' name='amount' />
                    </div> 
                    <div class="col-md-3">
                        <h5><strong>Rate:</strong> </h5>
                            <select name="rate_id" id="rate_name" class="form-control">
                            <?php 
                                foreach($rateDetails as $rate): 
                                    
                                    if($programDetails->rate->id == $rate->id):
                                        $sel = "selected";
                                    else:
                                        $sel = "";
                                    endif;
                                    
//                                    if(!empty($programDetails->rate)):
//                                    else:
//                                        if(!empty($programDetails->tutor->rate)):
//                                            if($programDetails->rate->id == $rate->id):
//                                                $sel = "selected";
//                                            endif;
//                                        else:
//                                            $sel = "";
//                                        endif;
//                                    endif;
                            ?>
                                <option <?= $sel ?> value="<?= $rate->id ?>"><?= $rate->name ?></option>
                            <?php endforeach; ?>
                            </select>
                        <?php // debug($programDetails->toArray()) ?>
                    </div>
                    <div class="col-md-3">
                        <h5><strong>Status:</strong> </h5>
                        <select name='status' class='form-control'>
                        <?php 
                            foreach($bookingStatus as $stats): 
                                if($stats == "Completed"):
                                    $statD = "Done";
                                else:
                                    $statD = $stats;
                                endif;
                        ?>
                            <option <?php if($programDetails->status == $statD): echo "selected"; elseif($programDetails->status == "Tutor" && $statD == "Pending"): echo "selected"; endif; ?> 
                                value='<?= $statD ?>' ><?= $stats ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div> 
                    <div class="col-md-12">
                        <h5><strong>Booking Note:</strong> </h5>
                        <textarea class="form-control" rows="3" name="note" ><?= htmlspecialchars($programDetails->note) ?></textarea>
                    </div>              
                    <div class="col-md-12">
                        <div class="form-group text-center">
                            <br/><br/>
                            <button id="cpBtn" class="btn btn-default">Update Booking Information</button>
                        </div>
                    </div>
                </div>
            </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php // debug($programDetails); ?>
    


