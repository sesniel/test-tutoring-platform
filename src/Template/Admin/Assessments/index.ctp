<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" rel="stylesheet"
      type="text/css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Assessments</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Students assessments and there results.</p>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#assessments" data-toggle="tab"><h5>Assessments</h5></a>
                                    </li>
                                    <li><a href="#results" data-toggle="tab"><h5>Results</h5></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="assessments">
                                        <div class="row col-md-12 top-buffer">
                                            <div class="row center-content">
                                                <p class="customMtop">Students and their assessments set by tutors are shown in this table.</p>
                                            </div>
                                            <div class="row center-content">
                                                <table id="assessmentsTable"
                                                       class="table table table-striped table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Student</th>
                                                        <th>Suburb</th>
                                                        <th>Assessments Completed</th>
                                                        <th>Tutor</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $b = 0;
                                                    foreach ($lAlign as $stdwass): $b++; ?>
                                                        <tr>
                                                            <td><?= $stdwass->student->name ?></td>
                                                            <td><?= $stdwass->client->suburb ?></td>
                                                            <td>
                                                                <?php
                                                                $completed = 0;
                                                                foreach ($stdwass->student->student_assessment_types as $assessmentDetails):
                                                                    if ($assessmentDetails->status == "Done"):
                                                                        $completed++;
                                                                    endif;
                                                                endforeach;

                                                                echo $completed . "/" . count($stdwass->student->student_assessment_types);
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($stdwass->tutor)): ?>
                                                                    <?= $stdwass->tutor->first_name . ' ' . $stdwass->tutor->last_name ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td>
                                                                <?php if(!empty($stdwass->tutor)): ?>
                                                                <?=
                                                                $this->Html->link('<span class="text">View</span>',
                                                                    ['controller' => 'assessments', 'action' => 'edit_student', $stdwass->student->id, $stdwass->tutor->token, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-sm btn-info reportVerifySA$b", 'target' => '_blank'])
                                                                ?>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                        <script type="text/javascript">

                                                            var $modalDialog = $('<div/>', {
                                                                'class': 'reportVerifySA<?php echo $b; ?>',
                                                                'id': 'reportVerifySA<?php echo $b; ?>'
                                                            })
                                                                .appendTo('body')
                                                                .dialog({
                                                                    resizable: false,
                                                                    autoOpen: false,
                                                                    height: $(window).height(),
                                                                    width: $(window).width(),
                                                                    draggable: true,
                                                                    show: 'fold',
                                                                    buttons: {
                                                                        "Exit": function () {
                                                                            $modalDialog.dialog("close");
                                                                        }
                                                                    },
                                                                    modal: true
                                                                });

                                                            $(function () {
                                                                $('a.reportVerifySA<?php echo $b; ?>').on('click', function (e) {
                                                                    e.preventDefault();
                                                                    // TODO: Undo comments, below
                                                                    var url = $('a.reportVerifySA<?php echo $b; ?>:first').attr('href');
                                                                    $modalDialog.load(url);
                                                                    $modalDialog.dialog("open");
                                                                });
                                                            });

                                                        </script>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="results">
                                        <div class="row col-md-12 top-buffer">
                                            <div class="row center-content">
                                                <p class="customMtop">These are Student Assessments that are not yet
                                                    completed for more than two days.</p>
                                            </div>
                                            <div class="row center-content">
                                                <table id="resultsTbl"
                                                       class="responsive display table table-hover table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Student</th>
                                                        <th>Assessment</th>
                                                        <th>Created Date</th>
                                                        <th>Finished Date</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($assmntResults as $assmnt): ?>
                                                        <tr>
                                                            <td><?= $assmnt->student->name ?></td>
                                                            <td><?= $assmnt->assessment_type->type ?></td>
                                                            <td><?= date('d/m/Y', strtotime($assmnt->created)); ?></td>
                                                            <td><?= date('d/m/Y', strtotime($assmnt->done_date)); ?></td>
                                                            <td>
                                                                <?php if ($assmnt->assessment_type_id == 1 || $assmnt->assessment_type_id == 2):

                                                                    echo $this->Html->link(
                                                                        'View',
                                                                        array('controller' => 'report', 'action' => 'print_report_learning', $assmnt->id),
                                                                        array('target' => '_blank', 'class' => 'btn btn-info btn-sm')
                                                                    );

                                                                else:

                                                                    echo $this->Html->link(
                                                                        'View',
                                                                        array('controller' => 'report', 'action' => 'printReport', $assmnt->id),
                                                                        array('target' => '_blank', 'class' => 'btn btn-info btn-sm')
                                                                    );

                                                                endif; ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/assessments.js'); ?>