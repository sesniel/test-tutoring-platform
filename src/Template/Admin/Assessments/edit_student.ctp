<div class="well">
    <?= $this->Form->create(null, ['url' => ['action' => 'allocate']]); ?>
        <div class="modal-body row center-table">
            <div class="row col-md-12">
                <div class="col-md-12">
                    <h4>Student Assessment</h4>
                </div>
            </div>
            <div class="row col-md-12 center-block">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Contact Name:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->first_name . " " . $studentDetails->client->last_name ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Email:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->email ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Phone:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->phone ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Mobile:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->mobile ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Address:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->street_number . " " . $studentDetails->client->street_name  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Suburb:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->suburb  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Postcode:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->postcode  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>City:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->city  ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <hr/>
                    <h4>Pending Assessments:</h4>
                    <p><b>Warning:</b><em> <u>Cancelled Assessment</u> will be deleted.</em></p>
                </div>
                <div class="col-md-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <td>Assessment</td>
                            <td>Completed</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($studentAssessmentDetails as $studDetails):  ?>
                                <tr>
                                    <td><?= $studDetails->assessment_type->type ?></td>
                                    <td><?= $studDetails->status ?></td>
                                    <td>
                                        <?php
                                        if ($studDetails->status == "Done") {
                                            echo $this->Html->link(
                                                "View",
                                                array('controller' => '../client', 'action' => 'printReport', $studDetails->id),
                                                ['class' => 'btn btn-default btn-sm', 'target' => '_blank', 'type' => 'button']
                                            );
                                        } else {
                                            echo $this->Html->link(
                                                "Cancel Assessment",
                                                array('action' => 'cancelAssessment', $studDetails->id),
                                                ['class' => 'btn btn-sm btn-default']
                                            );
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <hr/>
                    <h4>Add Assessment:</h4>
                    <p class="p-notes">Select which assessments you would like the student to
                        complete
                        (hold ctrl to select more than 1)</p>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Student</td>
                            <td>Year</td>
                            <td>Subject</td>
                            <td>Assessments</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?= $studentDetails->name ?>
                                <input type="hidden" name="student_id" value="<?= $studentDetails->id ?>" />
                            </td>
                            <td>
                                <?Php
                                    if($studentDetails->year_level_id == "1"):
                                        echo "F";
                                    else:
                                        echo ($studentDetails->year_level_id - 1);
                                    endif;                                        
                                ?>
                            </td>
                            <td>
                                <?php foreach($studentSubjects as $subject): ?>
                                    <?= $subject ?><br/>
                                <?php endforeach; ?>
                            </td>
                            <td class="col-md-3">
                                <select name="assessments[]" class="form-control lead-subjects" multiple="" row="10" required="">
                                    <?php                                    
                                        $assessmentTypeDetailsInfo = $assessmentTypeDetails->toArray();
                                        foreach($studentAssessmentDetails as $studentAssesstype):
                                            foreach($assessmentTypeDetailsInfo as $key => $studentTypes):

                                                if($studentAssesstype->assessment_type_id == $studentTypes->id):
                                                    unset($assessmentTypeDetailsInfo[$key]);
                                                endif;

                                            endforeach;

                                        endforeach;
                                    ?>
                                    
                                    <?php foreach($assessmentTypeDetailsInfo as $assessDetails): ?>
                                        <option value="<?= $assessDetails->id ?>" ><?= $assessDetails->type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td><?= $this->Form->hidden('tutor_token', ['value' => $token]) ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                    <div class="modal-footer">
        <button type="submit" class="btn btn-default">Confirm Assessments</button>
    </div>
<?= $this->Form->end(); ?>         
</div>
<?php 
    
    //debug($studentAssessmentDetails->toArray());
    
?>