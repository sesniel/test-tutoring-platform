<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Minor Outcomes</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
            <div class="row">
                <div class="col-md-12"><p>Here you can edit and view Minor and Major outcomes.</p><hr></div>
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="minorOutcomeTbl" class="table table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Major Outcome</th>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($minorOutcomes as $minorOutcome): ?>
                                    <tr>
                                        <td><?= $minorOutcome->id ?></td>
                                        <td><?= $minorOutcome->major_outcome->name ?></td>
                                        <td><?= $minorOutcome->code ?></td>
                                        <td><?= $minorOutcome->description ?></td>
                                        <td>
                                            <?= $this->Form->button('Edit', ['type' => 'button', 'value' => $minorOutcome->id, 'class' => 'btn btn-xs btn-info editMinorBtn']); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="edit-minor-cont"></div>
<?= $this->Html->script('admin/minor_outcome.js'); ?>