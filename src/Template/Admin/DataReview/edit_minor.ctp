<div id="edit-minor-outcome" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= $this->Form->create(null, ['url' => ['action' => 'updateMinorOutcome']]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Minor and Major Outcome</h4>
                <?= $this->Form->hidden('min-id', ['value' => $minorOutcomes->id]) ?>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Minor Outcome:</h4>
                        <hr>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="min-majid">Major Outcome ID:</label>
                                <input type="text" class="form-control" id="min-majid" name="min-majid" value="<?= $minorOutcomes->major_outcome_id ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="min-code">Code:</label>
                                <input type="text" class="form-control" id="min-code" name="min-code" value="<?= $minorOutcomes->code ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="min-desc">Description:</label>
                                <textarea id="min-desc" name="min-desc" class="form-control" cols="3" rows="4"><?= $minorOutcomes->description ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 top-buffer">
                        <h4>Major Outcome:</h4>
                        <hr>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="maj-catid">Category ID:</label>
                                <input type="text" class="form-control" id="maj-catid" name="maj-catid" value="<?= $minorOutcomes->major_outcome->category_id ?>" required>
                            </div>
                            <div class="col-md-4">
                                <label for="maj-subid">Subject ID:</label>
                                <input type="text" class="form-control" id="maj-subid" name="maj-subid" value="<?= $minorOutcomes->major_outcome->subject_id ?>" required>
                            </div>
                            <div class="col-md-4">
                                <label for="maj-yrid">Year Level ID:</label>
                                <input type="text" class="form-control" id="maj-yrid" name="maj-yrid" value="<?= $minorOutcomes->major_outcome->year_level_id ?>" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 top-buffer">
                                <label for="maj-name">Name:</label>
                                <input type="text" class="form-control" id="maj-name" name="maj-name" value="<?= $minorOutcomes->major_outcome->name ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="maj-desc">Description:</label>
                        <textarea class="form-control" id="maj-desc" name="maj-desc" cols="3" rows="4"><?= $minorOutcomes->major_outcome->description ?></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>