<style>
    .text-size{
        font-size: 12px;
    }
</style>
<div class="widget-box container">
    <div class="widget-title">
        <div class="row">
        <div class="col-md-6">
            <h2 class="headerDisplay">
                <strong>KPI Report</strong>
            </h2>
                <br/><i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
        </div> 
        <div class="col-md-6">
            
        </div> 
        </div>
        <br/>&nbsp;
        <div class="row">            
            <div class="col-md-6">
                <h5 class="text-center">Tutoring Fulfilled In Given Date Range</h5>
            <hr>
                <div class="row" style="padding-right: 25px; padding-left: 25px;">
                    <div class="col-md-12 well">
                        <?= $this->Form->create(null, ['url' => ['action' => 'index'], 'method' => 'post']); ?>
                                            <input value="<?= $lessonNotCDDetails['date'] ?>" type="hidden" name="date" required>
                            <table class="table">
                                <tr>
                                    <td>
                                        Start Date
                                    </td>
                                    <td>                                
                                        End Date
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group selectDate">
                                            <input class="form-control accessDate textDate" value="<?= $lessonDetails['start_date'] ?>" type="text" id="start_date" name="start_date" required>
                                            <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                                        </div>
                                    </td>
                                    <td>                                
                                        <div class="input-group selectDate">
                                            <input class="form-control accessDate textDate" value="<?= $lessonDetails['end_date'] ?>" type="text" id="end_date" name="end_date" required>
                                            <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                                        </div>
                                    </td>
                                    <td>                                
                                        <input type="submit" class="btn btn-success selectDate" value="Submit" />
                                    </td>
                                </tr>
                            </table>            
                        <?= $this->Form->end(); ?>

                        <?php if(isset($lessonDetails->sumlength)): ?>

                            <table class="table">
                                <tr>
                                    <td>
                                        <p class="pull-left">Date range total hours from <?= $lessonDetails->start_date ?> to <?= $lessonDetails->end_date ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p class="pull-left">Total lesson hours: <?= $lessonDetails->sumlength ?>
                                    </td>
                                </tr>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5 class="text-center">Total Scheduled Unfulfilled Lessons As At Date</h5>
            <hr>
                <div class="row" style="padding-right: 25px; padding-left: 25px;">
                    <div class="col-md-12 well">
                        <?= $this->Form->create(null, ['url' => ['action' => 'index'], 'method' => 'post']); ?>
                            <table class="table">
                                <tr>
                                    <td>
                                        Date
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="input-group selectDate">
                                            <input class="form-control accessDate textDate" value="<?= $lessonNotCDDetails['date'] ?>" type="text" id="date" name="date" required>
                                            <input value="<?= $lessonDetails['start_date'] ?>" type="hidden" id="start_date" name="start_date" required>
                                            <input value="<?= $lessonDetails['end_date'] ?>" type="hidden" id="end_date" name="end_date" required>
                                            <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                                        </div>
                                    </td>
                                    <td>                                
                                        <input type="submit" class="btn btn-success selectDate" value="Submit" />
                                    </td>
                                </tr>
                            </table>            
                        <?= $this->Form->end(); ?>

                        <?php if(isset($lessonNotCDDetails->sumlength)): ?>

                            <table class="table">
                                <tr>
                                    <td>
                                        <p class="pull-left">Total Scheduled Lessons Booked: <?= $lessonNotCDDetails->sumlength ?>
                                    </td>
                                </tr>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
            
            
            
        <div class="row">
            <div class="col-sm-12 text-size">
                <h3></h3>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    
    $('.accessDate').datepicker({
//        minDate: 0,
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });

    
});
</script>
