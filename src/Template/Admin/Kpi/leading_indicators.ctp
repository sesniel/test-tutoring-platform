<?php

//New Consultations Trend
    $sixthMonthConsultation = 0;
    $fifthMonthConsultation = 0;
    $fourthMonthConsultation = 0;
    $thirdMonthConsultation = 0;
    $secondMonthConsultation = 0;
    $firstMonthConsultation = 0;
    $consultationMonth = array();
    for($i = 6; $i >= 0; $i--) {
        $consultationMonth[] =  date('F', strtotime("-$i month"));
    }
        foreach($consultationDetails as $consultationTrends):
            if(date("F", strtotime($consultationTrends->created)) != date("F") && $consultationTrends->status == "New"):
                if(date("F", strtotime($consultationTrends->created)) == $consultationMonth[0]):
//                    echo $month[0] . "<br>";
                    $sixthMonthConsultation += count($consultationMonth[0]);
                elseif(date("F", strtotime($consultationTrends->created)) == $consultationMonth[1]):
                   // echo $month[1] . "<br>";
                    $fifthMonthConsultation += count($consultationMonth[1]);
                elseif(date("F", strtotime($consultationTrends->created)) == $consultationMonth[2]):
                    //echo $month[2] . "<br>";
                    $fourthMonthConsultation += count($consultationMonth[2]);
                elseif(date("F", strtotime($consultationTrends->created)) == $consultationMonth[3]):
                    //echo $month[3] . "<br>";
                    $thirdMonthConsultation += count($consultationMonth[3]);
                elseif(date("F", strtotime($consultationTrends->created)) == $consultationMonth[4]):
                    //echo $month[4] . "<br>";
                    $secondMonthConsultation += count($consultationMonth[4]);
                elseif(date("F", strtotime($consultationTrends->created)) == $consultationMonth[5]):
                    //echo $month[5] . "<br>";
                    $firstMonthConsultation += count($consultationMonth[5]);
                endif;

            endif;
        endforeach;
        
//Booking Conversion Rate
        $bookingMonth = array();
        for($i = 6; $i >= 0; $i--) {
            $bookingMonth[] =  date('F', strtotime("-$i month"));
        }

       $sixthMonthBooked = 0;
       $fifthMonthBooked = 0;
       $fourthMonthBooked = 0;
       $thirdMonthBooked = 0;
       $secondMonthBooked = 0;
       $firstMonthBooked = 0;
       
       $sixthMonthDiscontinued = 0;
       $fifthMonthDiscontinued = 0;
       $fourthMonthDiscontinued = 0;
       $thirdMonthDiscontinued = 0;
       $secondMonthDiscontinued = 0;
       $firstMonthDiscontinued = 0;

       foreach($consultationDetails as $bookingConversion):
           if(date("F", strtotime($bookingConversion->created)) != date("F") && $bookingConversion->status == "Booked"):
                if(date("F", strtotime($bookingConversion->created)) == $bookingMonth[0]):
                    $sixthMonthBooked += count($bookingMonth[0]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[1]):
                    $fifthMonthBooked += count($bookingMonth[1]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[2]):
                    $fourthMonthBooked += count($bookingMonth[2]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[3]):
                    $thirdMonthBooked += count($bookingMonth[3]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[4]):
                    $secondMonthBooked += count($bookingMonth[4]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[5]):
                     $firstMonthBooked += count($bookingMonth[5]);
                endif;
           elseif($bookingConversion->status == "Discontinued Consultation"):
                if(date("F", strtotime($bookingConversion->created)) == $bookingMonth[0]):
                    $sixthMonthDiscontinued += count($bookingMonth[0]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[1]):
                    $fifthMonthDiscontinued+= count($bookingMonth[1]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[2]):
                    $fourthMonthDiscontinued += count($bookingMonth[2]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[3]):
                    $thirdMonthDiscontinued += count($bookingMonth[3]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[4]):
                    $secondMonthDiscontinued += count($bookingMonth[4]);
                elseif(date("F", strtotime($bookingConversion->created)) == $bookingMonth[5]):
                     $firstMonthDiscontinued += count($bookingMonth[5]);
                endif;
          endif;
       endforeach;
       
       $sumOfBooked = $sixthMonthBooked + $fifthMonthBooked + $fourthMonthBooked + $thirdMonthBooked + $secondMonthBooked + $firstMonthBooked;
       $sumOfDiscontinued = $sixthMonthDiscontinued + $fifthMonthDiscontinued + $fourthMonthDiscontinued + $thirdMonthDiscontinued + $secondMonthDiscontinued + $firstMonthDiscontinued;
       
       $sixthbookedDiscontinued = $sixthMonthBooked + $sixthMonthDiscontinued;
       $fifthbookedDiscontinued = $fifthMonthBooked + $fifthMonthBooked;
       $fourthbookedDiscontinued = $fourthMonthBooked + $fourthMonthDiscontinued;
       $thirdbookedDiscontinued = $thirdMonthBooked + $thirdMonthDiscontinued;
       $secondbookedDiscontinued = $secondMonthBooked + $secondMonthDiscontinued;
       $firstbookedDiscontinued = $firstMonthBooked + $firstMonthDiscontinued;
       
       if(($sixthMonthBooked && $sixthbookedDiscontinued) == 0):
           $sixthAverageBooking = null;
       else:
           $sixthAverageBooking = ($sixthMonthBooked / $sixthbookedDiscontinued) * 100;
       endif;
       
       if(($fifthMonthBooked && $fifthbookedDiscontinued) == 0):
           $fifthAverageBooking = null;
       else:
           $fifthAverageBooking = ($fifthMonthBooked / $fifthbookedDiscontinued) * 100;
       endif;
       
       if(($fourthMonthBooked && $fourthbookedDiscontinued) == 0):
           $fourthAverageBooking = null;
       else:
           $fourthAverageBooking = ($fourthMonthBooked / $fourthbookedDiscontinued) * 100;
       endif;
       
       if(($thirdMonthBooked && $thirdbookedDiscontinued) == 0):
           $thirdAverageBooking = null;
       else:
           $thirdAverageBooking = ($thirdMonthBooked / $thirdbookedDiscontinued) * 100;
       endif;
       
       if(($secondMonthBooked && $secondbookedDiscontinued) == 0):
           $secondAverageBooking = null;
       else:
           $secondAverageBooking = ($secondMonthBooked / $secondbookedDiscontinued) * 100;
       endif;
       
       if(($firstMonthBooked && $firstbookedDiscontinued) == 0):
           $firstAverageBooking = null;
       else: 
           $firstAverageBooking = ($firstMonthBooked / $firstbookedDiscontinued) * 100;
       endif;

        
//New Consultation Month to Date
       
    $consultationMTDcount = 0;
        foreach($consultationDetails as $consultationsMTD):
            if(date("F", strtotime($consultationsMTD->created)) == date("F") && $consultationsMTD->status == "New"):
                $countMTD = count($consultationsMTD);
                $consultationMTDcount += count($countMTD);
            endif;
        endforeach;  
        
//New Booking Month To Date
        
    $bookingMTDcount = 0;
        foreach($programDetails as $bookingsMTD):
            if(date("F", strtotime($bookingsMTD->created)) == date("F")):
               $countMTD = count($bookingsMTD);
                $bookingMTDcount += count($countMTD);
            endif;
        endforeach;
        
//New Bookings Trend
        $sixthMonthProgram = 0;
        $fifthMonthProgram = 0;
        $fourthMonthProgram = 0;
        $thirdMonthProgram = 0;
        $secondMonthProgram = 0;
        $firstMonthProgram = 0;
        $programMonth = array();
        for($i = 6; $i >= 0;$i--){
            $programMonth[] = date("F", strtotime("-$i month"));
        }
        foreach($programDetails as $bookings):
            if(date("F", strtotime($bookings->created)) != date("F") && $bookings->status == "Pending"):
                if(date("F", strtotime($bookings->created)) == $programMonth[0]):
                    $sixthMonthProgram += count($programMonth[0]);
                elseif(date("F", strtotime($bookings->created)) == $programMonth[1]):
                    $fifthMonthProgram += count($programMonth[1]);
                elseif(date("F", strtotime($bookings->created)) == $programMonth[2]):
                    $fourthMonthProgram += count($programMonth[2]);
                elseif(date("F", strtotime($bookings->created)) == $programMonth[3]):
                    $thirdMonthProgram += count($programMonth[3]);
                elseif(date("F", strtotime($bookings->created)) == $programMonth[4]):
                    $secondMonthProgram += count($programMonth[4]);
                elseif(date("F", strtotime($bookings->created)) == $programMonth[5]):
                    $firstMonthProgram += count($programMonth[5]);
                endif;
            endif;
        endforeach;
        
?>
<style>
.productbox1 {
    background-color:#FCF5F3;
    padding:10px;
    margin-bottom:10px;
    min-height: 100px;
    min-width: 450px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}

.productbox2 {
    background-color:#66b3ff;
    padding:10px;
    margin-bottom:10px;
    min-height: 101px;
    min-width: 150px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}

.producttitle {
    font-weight:bold;
	padding:5px 0 5px 0;
}

.productprice {
	border-top:1px solid #dadada;
	padding-top:5px;
}

.pricetext {
	font-weight:bold;
	font-size:1.4em;
}
.iconSize {
    font-size: 60px;
    margin-right: 25px;
    margin-top: 10px;
    color: #fff;
    
}
.fontText {
    font-family: Helvetica Rounded, Arial, sans-serif;
    font-size: 40px;
    margin: auto;
    
}
h5 {
    
    margin: auto;
}

</style>
<script type="text/javascript">
$(function () {
    
    
    //Booking Conversion Rate
    Highcharts.chart('bookingConversionRate', {
    xAxis: {
        
        categories: [<?php for ($i = 6; $i >= 0; $i--) { ?>
                        '<?php $month =  date(' F', strtotime("-$i month"));
                               echo $month;
                        ?>',
                    <?php } ?>]
    },
    yAxis: {
       title: {
           enabled: false
       }
    },
    title: {
      text:   'Booking Conversion Rate'
    },
    
    subtitle: {
        text: 'Booking Conversion Rate for the Past 6 Calendar Months'
    },
    
    credits: {
        enabled: false
    },
    
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                
                
            }
        }
    },
            
    tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:.2f}%</b>'
        },

    series: [{
            name: 'Booking Conversion Rate',
            data: [<?php echo round($sixthAverageBooking); ?>, <?php echo round($fifthAverageBooking); ?>, <?php echo round($fourthAverageBooking); ?>,
            <?php echo round($thirdAverageBooking); ?>, <?php echo round($secondAverageBooking); ?>, <?php echo round($firstAverageBooking); ?>]
        }]
});
   
    
    //New Consultations Trend        
    Highcharts.chart('consultationsTrend', {
    xAxis: {
        
        categories: [<?php for ($i = 6; $i >= 0; $i--) { ?>
                        '<?php $month =  date(' F', strtotime("-$i month"));
                               echo $month;
                        ?>',
                    <?php } ?>]
    },
    yAxis: {
       title: {
           enabled: false
       }
    },
    title: {
      text:   'New Consultations Trend'
    },
    subtitle: {
        text: 'Consultations Trend for the Past 6 Calendar Months'
    },
    
    credits: {
        enabled: false
    },
    
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
            }
        }
    },

    series: [{
            name: 'Consultations',
            data: [<?= $sixthMonthConsultation ?>, <?= $fifthMonthConsultation ?>, <?= $fourthMonthConsultation ?>,
            <?= $thirdMonthConsultation ?>, <?= $secondMonthConsultation ?>, <?= $firstMonthConsultation ?>]
        }]
});

//New Bookings Trend
    Highcharts.chart('bookingsTrend', {
    xAxis: {
        
        categories: [<?php for ($i = 6; $i >= 0; $i--) { ?>
                        '<?php $month =  date(' F', strtotime("-$i month"));
                               echo $month;
                        ?>',
                    <?php } ?>]
    },
    yAxis: {
       title: {
           enabled: false
       }
    },
    title: {
      text:   'New Bookings Trend'
    },
            
    subtitle: {
        text: 'Bookings Trend for the Past 6 Calendar Months'
    },
    
    credits: {
        enabled: false
    },
    
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
            }
        }
    },

    series: [{
            name: 'Consultations',
            data: [<?= $sixthMonthProgram ?>, <?= $fifthMonthProgram ?>, <?= $fourthMonthProgram ?>,
            <?= $thirdMonthProgram ?>, <?= $secondMonthProgram ?>, <?= $firstMonthProgram ?>]
        }]
});
});

</script> 
    
<div class="row">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Leading Indicators</strong></h2>  
            <hr class="customHr">
        </div>
        <div class="widget-container">
               <div class="row center-content">
                    <div class="col-sm-6 col-md-6 top-buffer pull-left">
                        <div id="bookingConversionRate" style="width: 500px; height: 250px; margin: 0 auto"></div>
                    </div>
                    <div class="col-sm-6 col-md-6 top-buffer pull-left">
                        <div id="consultationsTrend" style="width: 500px; height: 250px; margin: 0 auto"></div>
                    </div> 
                </div><br>
                <div class="row center-content top-buffer">
                    <div class="col-sm-6 col-md-6 top-buffer">
                        <div id="bookingsTrend" style="width: 500px; height: 250px; margin: 0 auto"></div>
                    </div>
                    <div class="col-sm-6 col-md-6 center-content pull-right">
                        <div class="col-md-12 top-buffer">
                            
                        </div>
                        <div class="col-md-12 top-buffer">
                            <div class="col-md-6 column productbox1 text-center">
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <div class="producttitle"><br>
                                            <span><h5 class="top-buffer"><strong>Number of Consultation Month To Date</strong></h5></span>                                                 
                                        </div>                       
                                    </div>
                                    <div class="col-md-6 text-center">
                                        <div class="producttitle"><br>
                                            <span><h5 class="top-buffer fontText"><strong><?php echo $consultationMTDcount; ?></strong> </h5></span>                                                 
                                        </div>                       
                                    </div>
                                                          
                                </div>
                            </div>
                            <div class="productbox2 center-content">
                                <span><i class="glyphicon glyphicon-stats iconSize pull-right"></i></span> 
                            </div>
                            <div class="col-md-6 column productbox1 text-center">
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <div class="producttitle"><br>
                                            <span><h5 class="top-buffer pull-right"><strong>Number of Bookings Month to Date</strong></h5></span>                                                 
                                        </div>                       
                                    </div>
                                    <div class="producttitle"><br>
                                            <span><h5 class="top-buffer fontText"><strong><?php echo $bookingMTDcount; ?></strong> </h5></span>                                                 
                                        </div>                     
                                </div>
                            </div> 
                            <div class="productbox2 center-content">
                                <span><i class="glyphicon glyphicon-stats iconSize pull-right"></i></span> 
                            </div>
                        </div>
                        
                    </div>
                     
                </div>
            </div>               
     </div>
</div>


<?php // debug($studentAssessmentDetails); ?>
