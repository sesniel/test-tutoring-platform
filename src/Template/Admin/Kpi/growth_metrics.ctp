<?php

//Tutored Hours Per Month
    $tutorHoursPerMonth = array();
    for($x=6;$x>=0;$x--):
          $tutorHoursPerMonth[] = 0;
    endfor;
    $countHours = 0;   
    foreach($lessonDetails as $lessons):
      $count = $lessons->length;
      $countHours += $count;
      if(date("F", strtotime($lessons->created)) != date("F") && $lessons->status == "Completed"):
      $months = array();
          for($i=6; $i>=0;$i--):
              $months[] = date("F", strtotime("-$i months"));
          endfor;
          if(date("F", strtotime($lessons->created)) == $months[0]):
              $count = $lessons->length;
              $tutorHoursPerMonth[0] += $count;
          elseif(date("F", strtotime($lessons->created)) == $months[1] ):
              $count = $lessons->length;
              $tutorHoursPerMonth[1] += $count;
          elseif(date("F", strtotime($lessons->created)) == $months[2] ):
              $count = $lessons->length;
              $tutorHoursPerMonth[2] += $count;
          elseif(date("F", strtotime($lessons->created)) == $months[3] ):
              $count = $lessons->length;
              $tutorHoursPerMonth[3] += $count;
          elseif(date("F", strtotime($lessons->created)) == $months[4] ):
              $count = $lessons->length;
              $tutorHoursPerMonth[4] += $count;
          elseif(date("F", strtotime($lessons->created)) == $months[5] ):
              $count = $lessons->length;
              $tutorHoursPerMonth[5] += $count;
          endif;
      endif;
    endforeach; 
    
//Bookings for the Current Month
    $hoursForThisMonth = 0;
        foreach($lessonDetails as $lessons):
            if(date("F", strtotime($lessons->date)) == date("F") && $lessons->status != "Cancelled"):
                $count = $lessons->length;
                $hoursForThisMonth += $count;
            endif;
        endforeach;
        
    $hoursForNextMonth = 0;
        foreach($lessonDetails as $lessons):
            if(date("F", strtotime($lessons->date)) == date("F", strtotime("1 month"))):
                $count = $lessons->length;
                $hoursForNextMonth += $count;
            endif;
        endforeach;

        
?>
<style>
.productbox1 {
    background-color:#FCF5F3;
    padding:10px;
    margin-top: 60px;
    margin-bottom:10px;
    margin-left: 100px;
    min-height: 150px;
    min-width: 450px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}
.productbox2 {
    background-color:#66b3ff;
    padding:10px;
    margin-bottom:10px;
    min-height: 101px;
    min-width: 150px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}
.producttitle {
    font-weight:bold;
	padding:5px 0 5px 0;
}

.productprice {
	border-top:1px solid #dadada;
	padding-top:5px;
}

.pricetext {
	font-weight:bold;
	font-size:1.4em;
}
.iconSize {
    font-size: 50px;
}

h5 {
    margin-left: 10px;
}

p.hours {
    text-align: center;
    margin-top: 10px;
    font-size: 30px;
    
}
</style>
<script type="text/javascript">
$(function () {
    
    
    //Tutored Hours Per Month
    Highcharts.chart('tutoredHoursPerMonth', {
    xAxis: {
        
        categories: [<?php for ($i = 6; $i >= 0; $i--) { ?>
                        '<?php $month =  date(' F', strtotime("-$i month"));
                               echo $month;
                        ?>',
                    <?php } ?>]
    },
    yAxis: {
       title: {
           enabled: false
       }
    },
    title: {
      text:   'Tutored Hours Per Month'
    },
    
    subtitle: {
        text: 'Tutored Hours Per Month for the Past 6 Calendar Months'
    },
    
    credits: {
        enabled: false
    },
    
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                
                
            }
        }
    },
            
    tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><b>{point.y:.2f} hours</b>'
        },

    series: [{
            name: 'Tutored Hours per Month',
            data: [<?= $tutorHoursPerMonth[0] ?>,<?= $tutorHoursPerMonth[1] ?>,
                    <?= $tutorHoursPerMonth[2] ?>, <?= $tutorHoursPerMonth[3] ?>,
                            <?= $tutorHoursPerMonth[4] ?>, <?= $tutorHoursPerMonth[5] ?>, 

    
                   ]
        }]
});
   

});

</script> 
    
<div class="row">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Growth Metrics</strong></h2>  
            <hr class="customHr">
        </div>
        <div class="widget-container">
            <div class="row center-content">
                    <div class="col-sm-6 col-md-6 top-buffer pull-left">
                        <div id="tutoredHoursPerMonth" style="width: 650px; height: 400px; margin: 0 auto"></div>
                    </div>
                    <br><br>
                    <div class="col-md-4 column productbox1 top-buffer center-block pull-10" style="margin-top: 10px;">
                        <div class="producttitle">
                            <span><h5 class="top-buffer text-center"><strong>Booked hours for this calendar month</strong> </h5></span><br>                                                 
                                <span><p class="hours"><?= $hoursForThisMonth ?></p></span>
                        </div>
                        
                    </div>
                    
                    <div class="col-md-4 column productbox1 top-buffer center-content">
                        <div class="producttitle">
                            <span><h5 class="top-buffer text-center"><strong>Booked hours next calendar month</strong> </h5></span><br>
                            <span><p class="hours"><?= $hoursForNextMonth ?></p></span>
                        </div>
                    </div> 
                    
                </div><br>
                <div class="row center-content top-buffer">
                    <div class="col-sm-6 col-md-6 center-content pull-right">
                        <div class="col-md-12 top-buffer">
                            
                        </div>
                        
                        
                    </div>
                     
                </div>
        </div>            
     </div>
</div>


<?php // debug($studentAssessmentDetails); ?>
