<?php
    
//Average Lesson Length
    $totalLengthLessons = 0;
    $totalLessons = 0;
    foreach($lessonDetails as $lessons):
        if($lessons->status == "Completed"):
        $length = $lessons->length;
        $lessonCount = count($lessons);
        $totalLessons += $lessonCount;
        $totalLengthLessons += $length;
        endif;
    endforeach;
    $averageLessons = ($totalLengthLessons / $totalLessons);
    
//Average Lessons Per Booking
    $totalBookings = 0;
    $totalLessons = 0;
    $averageLessonsPerBooking = 0;
    foreach($programDetails as $bookings):
        if($bookings->status == "Completed"):
        $bookingsCount = count($bookings);
        $totalBookings += $bookingsCount;
        foreach($bookings->lessons as $lessons):
//                echo $lessons->id . "<br>";
            $lessonsCount = count($lessons);
            $totalLessons += $lessonsCount;
        endforeach;
        endif;
    endforeach;
    $averageLessonsPerBooking = $totalLessons / $totalBookings;
    
//Average Booking Value (Per month)
    $months = array();
    for($x = 1; $x<=12;$x++) {
        $months[$x] = date('F', mktime(0, 0, 0, $x, 1));
    }
    //Total number of bookings per month
    $jan = 0; $feb = 0; $mar = 0; $apr = 0; $may = 0;
    $jun = 0; $jul = 0; $aug = 0; $sep = 0; $oct = 0;
    $nov = 0; $dec = 0;

    //Total number of bookings per month == completed
    $completedJan = 0; $completedFeb = 0; $completedMar = 0; $completedApr = 0;
    $completedMay = 0; $completedJun = 0; $completedJul = 0; $completedAug = 0;
    $completedSep = 0; $completedOct = 0; $completedNov = 0; $completedDec = 0;
    
    foreach($programDetails as $bookings):
        $count = count($bookings);
        $totalBookings += $count;
        $months = array();
        for($x = 1; $x<=12;$x++):
            $months[$x] = date('F', mktime(0, 0, 0, $x, 1));
            if(date("F", strtotime($bookings->created)) == $months[$x]):
                if($x == 1):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedJan += $completed;
                    endif;
                    $jan += $count;
                elseif($x == 2):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedFeb += $completed;
                    endif;
                    $feb += $count;
                elseif($x == 3):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedMar += $completed;
                    endif;
                    $mar += $count;
                elseif($x == 4):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedApr += $completed;
                    endif;
                    $apr += $count;
                elseif($x == 5):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedMay += $completed;
                    endif;
                    $may += $count;
                elseif($x == 6):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedJun += $completed;
                    endif;
                    $jun += $count;
                elseif($x == 7):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedJul += $completed;
                    endif;
                    $jul += $count;
                elseif($x == 8):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedAug += $completed;
                    endif;
                    $aug += $count;
                elseif($x == 9):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedSep += $completed;
                    endif;
                    $sep += $count;
                elseif($x == 10):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedOct += $completed;
                    endif;
                    $oct += $count;
                elseif($x == 11):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedNov += $completed;
                    endif;
                    $nov += $count;
                elseif($x == 12):
                    $count = count($months[$x]);
                    if($bookings->status == "Completed"):
                       $completed = count($months[$x]);
                       $completedDec += $completed;
                    endif;
                    $dec += $count;
               endif;
            endif;
        endfor;

    endforeach;

    if(($completedJan && $jan) == 0):
        $averageJan = null;
    else:
        $averageJan = $completedJan / $jan;
    endif;

    if(($completedFeb && $feb) == 0):
        $averageFeb = null;
    else:
        $averageFeb = $completedFeb / $feb; 
    endif;

    if(($completedMar && $mar) == 0):
        $averageMar = null;
    else:
        $averageMar = $completedMar / $mar;  
    endif;

    if(($completedApr && $apr) == 0):
        $averageApr = null;
    else:
        $averageApr = $completedApr / $apr; 
    endif;

    if(($completedMay && $may) == 0):
        $averageMay = null;
    else:
       $averageMay = $completedMay /$may;
    endif;

    if(($completedJun && $jun) == 0):
        $averageJun = null;
    else:
       $averageJun = $completedJun / $jun;
    endif;

    if(($completedJul && $jul) == 0):
        $averageJul = null;
    else:
       $averageJul = $completedJul / $jul; 
    endif;

    if(($completedAug && $aug) == 0):
        $averageAug = null;
    else:
       $averageAug = $completedAug / $aug;
    endif;

    if(($completedSep && $sep) == 0):
        $averageSep = null;
    else:
       $averageSep = $completedSep / $sep; 
    endif;

    if(($completedOct && $oct) == 0):
        $averageOct = null;
    else:
       $averageOct = $completedOct / $oct;
    endif;

    if(($completedNov && $nov) == 0):
        $averageNov = null;
    else:
       $averageNov = $completedNov / $nov;
    endif;

    if(($completedDec && $dec) == 0):
        $averageDec = null;
    else:
       $averageDec = $completedDec / $dec;
endif;

//Average Lessons Per Client

  //Sum of All Lessons Per Client
$sumOfLessons = array();

$total = 0;

//Sum of All Lessons Status == "Completed" & "Pending"
$totalCom = 0;

$totalPercentage = 0;
$totalClient = 0;
foreach($clientDetails as $key => $client):
    $countClient = count($client);
    $totalClient += $countClient;
    if(!empty($client->programs)):
        foreach($client->programs as $bookings):
            foreach($bookings->lessons as $lessons):
               $count = count($lessons);
                if($lessons->status == "Completed" && "Pending"):
                    $countCom = count($lessons);
                    $totalCom += $countCom;
                endif;
                if(!isset($sumOfLessons[$key])):
                   $sumOfLessons[$key] = "";
                else:
                    $sumOfLessons[$key] += $count;
                endif;
           endforeach; 
        endforeach;
    endif;    
        if(!isset($sumOfLessons[$key])):
            $sumOfLessons[$key] = "";
        else:
            $sumOfLessons[$key] += $count;   
        endif;
    $total += $sumOfLessons[$key];
    if(($totalCom && $total) == 0):
        $averageLessonPerClient = "";
    else:
        $averageLessonPerClient = $totalCom / $totalClient;
    endif;
endforeach;

//Average Lifetime Customer Value
    $totalCompletedBookings = 0;
    $totalBookings = 0; 
    foreach($clientDetails as $customers):
        foreach($customers->programs as $bookings):
            $count = count($bookings);
            $totalBookings += $count;
            if($bookings->status == "Completed"):
                $count = count($bookings);
                $totalCompletedBookings += $count;
            endif;
        endforeach;
    endforeach;
    if(($totalCompletedBookings && $totalBookings) == 0):
        $averageCustomerValue = null;
    else:
        $averageCustomerValue = $totalCompletedBookings / $totalBookings;
    endif;
   
?>
<style>
.calendar {
    background-color:#FCF5F3;
    padding:10px;
    margin: 10px;
    min-height: 500px;
    min-width: 500px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}

.productbox1 {
    background-color:#FCF5F3;
    padding:10px;
    margin-top: 80px;
    margin-right: 10px;
    margin-bottom: 5px;
    margin-left: 30px;
    min-height: 152px;
    min-width: 50px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}
.productbox2 {
    background-color:#FCF5F3;
    padding:10px;
    margin-top: 40px;
    margin-left: 80px;
    margin-bottom:10px;
    min-height: 150px;
    min-width: 150px;
    -webkit-box-shadow: 0 8px 6px -6px  #999;
    -moz-box-shadow: 0 8px 6px -6px  #999;
    box-shadow: 0 8px 6px -6px #999;
}
.producttitle {
    font-weight:bold;
	padding:5px 0 5px 0;
}

.productprice {
	border-top:1px solid #dadada;
	padding-top:5px;
}

.pricetext {
	font-weight:bold;
	font-size:1.4em;
}
.iconSize {
    font-size: 60px;
    margin-top: 30px;
    margin-right: 15px;
    color: #2E2E2E;
}
.average-month {
    font-family: "Lato", sans-serif;
    font-size: 18px;
    color: #2E2E2E;
    font-weight: bold;
    margin-top: -20px;
   
}
.average-title {
    font-family: "Lato", sans-serif;
    color: #2E2E2E;
    font-weight: bold;
    font-size: 20px;
}
.average {
    color: #2E2E2E;
    margin-bottom: -10px;
    font-size: 18px;
    
}
p.text {
    font-family: "Lato", sans-serif;
    font-size: 60px;
    margin-top: 50px;
    margin-left: 30px;
/*    font-weight: bold;*/
    color: #2E2E2E;
}

p.title {
    margin-top: 20px;
    margin-left: 30px;
    color: black;
}
.month {
    margin-bottom: 100px;
}
.grid {
    margin-top: 50px;
    margin-bottom: -50px;
}
</style>
<div class="row">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Key Performance Indicators</strong></h2>  
            <hr class="customHr">
        </div>
        <div class="widget-container">
            <div class="row">
                <div class="col-md-12">
                    <p>The data below shows the average lesson length, average lessons per booking, average booking value, average lessons per client and
                    average lifetime customer value.
                    </p>
                </div>
                <hr class="customHr">
            </div>
            <div class="row center-content">
                
                <div class="col-md-3 calendar">
                    <div class="row">
                        <div class="col-md-12 text-center average-title">
                            <p>Average Booking Value Per Month</p>
                        </div>
                    </div>
                    <div class="grid-calendar">
                        <div class="row calendar-week-header month text-center">
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">January</p></span><span><p class="average"><?php if($averageJan != 0) { echo round((float)$averageJan * 100) . '%';} else { echo "-";} ?></p></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">February</p></span><span><p class="average"><?php if($averageFeb != 0) { echo round((float)$averageFeb * 100) . '%';} else { echo "-";} ?></p></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">March</p></span><span><p class="average"><?php if($averageMar != 0) { echo round((float)$averageMar * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            
                        </div>
                    </div>
                    <div class="grid-calendar ">
                        <div class="row calendar-week-header month text-center">
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">April</p></span><span><p class="average"><?php if($averageApr != 0) { echo round((float)$averageApr * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">May</p></span><span><p class="average"><?php if($averageMay != 0) { echo round((float)$averageMay * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">June</p></span><span><p class="average"><?php if($averageJun != 0) { echo round((float)$averageJun * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            
                        </div>
                    </div>
                    <div class="grid-calendar">
                        <div class="row calendar-week-header month text-center">
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">July</p></span><span><p class="average"><?php if($averageJul != 0) { echo round((float)$averageJul * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">August</p></span><span><p class="average"><?php if($averageAug != 0) { echo round((float)$averageAug * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">September</p></span><span><p class="average"><?php if($averageSep != 0) { echo round((float)$averageSep * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            
                        </div>
                    </div>
                    <div class="grid-calendar">
                        <div class="row calendar-week-header month text-center">
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">October</p></span><span><p class="average"><?php if($averageOct != 0) { echo round((float)$averageOct * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">November</p></span><span><p class="average"><?php if($averageNov != 0) { echo round((float)$averageNov * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            <div class="col-xs-4 grid-cell grid"><div><div><span><p class="average-month">December</p></span><span><p class="average"><?php if($averageDec != 0) { echo round((float)$averageDec * 100) . '%';} else { echo "-";} ?></span></div></div></div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-3 productbox1">
                    <i class="glyphicon glyphicon-book iconSize pull-right"></i>
                    <span><p class="text"><?php echo round($averageLessons, 2) ?></p></span>
                    <span><p class="title">Average Lesson Length</p></span>
                </div>
                <div class="col-md-3 productbox1">
                    <i class="glyphicon glyphicon-book iconSize pull-right"></i>
                    <span><p class="text"><?php echo round($averageLessonsPerBooking); ?></p></span>
                    <span><p class="title">Average Lessons Per Booking</p></span>
                </div>
                <div class="col-md-3 productbox1">
                    <i class="glyphicon glyphicon-user iconSize pull-right"></i>
                    <span><p class="text"><?php echo round((float) $averageLessonPerClient) ;?></p></span>
                    <span><p class="title">Average Lesson Per Client</p></span>
                </div>
                <div class="col-md-3 productbox1">
                    <i class="glyphicon glyphicon-user iconSize pull-right"></i>
                    <span><p class="text"><?php echo round($averageCustomerValue, 2) * 100 ?></p></span>
                    <span><p class="title">Average Lifetime Customer Value</p></span>
                </div>
                
            </div><br>
            
        </div>            
     </div>
</div>


<?php // debug($studentAssessmentDetails); ?>
