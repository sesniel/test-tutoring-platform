<?php $programStatus = array('All', 'Active', 'Discontinue'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Students</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9">
                                <p>These are the list of students that are currently on the system, you can search
                                    a client by typing any keywords in the input box at the top right corner of the
                                    table.</p>
                                <hr>
                            </div>
                            <div class="col-sm-3 col-md-3" style="position: relative;top:-25px;">
                                <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                                 
                                
                                <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="clientsTbl"
                                       class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Student</th>
                                        <th>Client</th>
                                        <th>Status</th>
                                        <th>Subjects</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($students as $student): ?>
                                        <tr>
                                            <td><?= $student->name ?></td>
                                            <td><?= $student->client->first_name . " " . $student->client->last_name?></td>
                                            <td><?= $student->client->status ?></td>
                                            <td><?php 
                                                $count = count($student->student_subjects) - 1;
                                                foreach($student->student_subjects as $key => $subjects):
                                                    
                                                    if($count > $key):
                                                        echo $subjects->subject->name . ", ";
                                                    else:
                                                        echo $subjects->subject->name;
                                                    endif;
                                                    
                                                    
                                                    
                                                endforeach;
                                             ?></td>
                                           
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-container"></div>
<div id="change-status" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Change of Status</h4>
            </div>
            <?php echo $this->Form->create(null, ['url' => ['action' => 'changeStatus']]);
            echo $this->Form->hidden('token', ['id' => 'changeStatus']) ?>
            <div class="modal-body">
                <div class="row center-content">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">Are you sure you want to change the status?</p>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <select name='status' class="form-control" required>
                                <option selected disabled> Please select Status </option>
                                <option value="Active">Active</option>
                                <option value="Discontinue">Discontinue</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/clients1.js'); ?>

