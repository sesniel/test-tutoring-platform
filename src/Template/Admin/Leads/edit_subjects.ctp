<div id="editSubjDia" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lead Subjects:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'leadSubjUpdate']]); ?>
            <div class="modal-body">
                <?= $this->Form->hidden('lead_id', ['value' => $leadInfo->id]) ?>
                <div class="row top-buffer">
                    <div class="col-md-12">
                        <label for="student">Student Reg. Subj/s:</label>
                        <p class="text-center">
                        <?php $subj = array();
                            foreach ($leadInfo->student->student_subjects as $subjects) {
                                $subj[] = $subjects->subject->name;
                            }
                            echo implode(", ", $subj); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="row center-content">
                    <div class="col-md-12">
                        <div class="col-md-12 top-buffer">
                            <label for="student">Subjects:</label>
                            <select id="student-subjects" name='subjects[]' class="form-control" multiple required>
                                <?php foreach ($leadInfo->lead_subjects as $selectedSubj) : ?>
                                    <option value='<?= $selectedSubj->subject_id ?>' selected><?= $selectedSubj->subject->name ?></option>
                                <?php endforeach;
                                foreach ($regSubjects as $subj) : ?>
                                    <option value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Confirm</button>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#year-level option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects option').each(function () {
            $(this).siblings("[value='" + this.value + "']").remove();
        });
        $('#student-subjects').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Available Subjects'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Lead Subjects'>",
            afterInit: function(ms){
                var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                    if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                    }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                    if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                    }
                });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });
        $('.ms-container').css("width","100%");
        
    });
</script>