<?php $programStatus = array('All', 'Booked', 'Abandoned'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Allocations</strong><?php
                                    echo $this->Html->link(
                                        "Add Leads",
                                        ['controller' => 'allocateLead', 'action' => 'index', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-success pull-right", 'target' => '_blank']);
                                        ?></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <p>This table displays all student-tutor allocations.</p>
                                    <hr>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'index'],
                                        'method' => 'post'
                                    ]);
                                ?>
                                Status: 
                                <select name="status" class="form-control" onchange="this.form.submit()" >
                                    <?php 
                                        foreach($programStatus as $stat): 
                                    ?>
                                        <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo $this->Form->end(); ?>
                                </div>
                            </div>
                            <div class="row center-content">
                                <div class="col-md-12">
                                    <table id="leadsTable"
                                           class="table table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Status</th>
                                            <th>Client</th>
                                            <th>Student</th>
                                            <th>Suburb</th>
                                            <th>Year</th>
                                            <th>Subject</th>
                                            <th>Tutor</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($leads as $lead): ?>
                                            <tr>
                                                <td><?= $lead->id ?></td>
                                                <td><?= $lead->status ?></td> 
                                                <td><?= $lead->client->first_name . " " . $lead->client->last_name ?></td>
                                                <td><?= $lead->student->name ?></td>
                                                <td><?= $lead->client->suburb ?></td>
                                                <td>
                                                    <?php if(!empty($lead->student->year_level)): ?>
                                                    <?= $lead->student->year_level->name ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php $subj = array();
                                                    foreach ($lead->lead_subjects as $subjects) {
                                                        $subj[] = $subjects->subject->name;
                                                    }
                                                    echo implode(", ", $subj); ?></td>
                                                <td>
                                                    <?php if(!empty($lead->tutor)): ?>
                                                    <?= $lead->tutor->first_name . " " . $lead->tutor->last_name ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                    <?= $this->Form->button('View', ['type' => 'button', 'value' => $lead->id, 'class' => 'btn btn-xs btn-info viewLead']); ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-container"></div>
<div id="editSubjectsContainer"></div>
<script>
    
 $(document).ready(function () {

    
    var hash = window.location.hash;

    switch (hash) {
        case '#status-change':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Updated');
            $('#response-dialog .modal-body p').text('Status successfully updated.');
            break;
    }

});
    $('#clientsTbl').on('submit', '.changeStatusBtn', function () {
        var token = $(this).val();

        $('#change-status').find('#changeStatus').val($(this).val());

    });    
    
</script>
<?= $this->Html->script('admin/leads230317.js'); ?>