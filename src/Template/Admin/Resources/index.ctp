<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Resources</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg center-table">
                    <div class="row top-buffer">
                        <div class="col-md-12">
                            <h4>Online Memberships &amp; Helpful Links</h4>
                            <p>Below is a list of links that include free and subscription sites that we are members of.
                                Where required, login details are provided (If you find other good online resources that
                                you believe tutors would benefit from access too, please contact admin so that we can
                                look into purchasing access)</p>
                            <hr>
                        </div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <li><a href="https://www.superteacherworksheets.com/" target="_blank"><i class="glyphicon glyphicon-thumbs-up"></i>Super Teacher Work Sheets</a> - (username: aimhight / password:
                                    aimhightutor1) – This contains a really wide range of printable worksheets for
                                    primary school.
                                </li>
                                <li><a href="https://www.worksheetworks.com/" target="_blank"><i class="glyphicon glyphicon-thumbs-up"></i>worksheetworks.com</a> - This site enables you to create a variety of
                                    worksheets up to grade 8 across Maths, English, Geography and Problem Solving.
                                </li>
                                <li><a href="http://www.wolframalpha.com/" target="_blank"><i class="glyphicon glyphicon-thumbs-up"></i>Wolfram Alpha</a> - Great computational and knowledge database.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row top-buffer">
                        <div class="col-md-12">
                            <h4>Resources</h4>
                            <p>
                                You can group the resources table based on the year level just select the year level and
                                hit search. You can also search for the subject, name, learning category etc. or enter
                                any
                                keywords in the search input at the top right corner of the table and it will populate
                                the table.
                            </p>
                            <hr>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <table id="resTbl" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Year</th>
                                    <th>Subject</th>
                                    <th>Name</th>
                                    <th>Learning Category</th>
                                    <th>Minor Outcome</th>
                                    <th>Major Outcome</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    
                                    <th>Year</th>
                                    <th>Subject</th>
                                    <th>Name</th>
                                    <th>Learning Category</th>
                                    <th>Minor Outcome</th>
                                    <th>Major Outcome</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($resources as $resource) : if($resource->minor_outcome): ?>
                                    <tr>
                                        <td><?= $resource->id ?></td>
                                        <td><?= $resource->minor_outcome->major_outcome->year_level->name ?></td>
                                        <td><?php if (($resource->minor_outcome->major_outcome->subject->name) == 'Math'):
                                                echo $resource->minor_outcome->major_outcome->subject->name . "s";
                                        else: 
                                            echo $resource->minor_outcome->major_outcome->subject->name;
                                                  endif; ?></td>
                                        <td><?= $resource->name ?></td>
                                        <td><?= $resource->minor_outcome->major_outcome->category->name ?></td>
                                        <td><?= $resource->minor_outcome->code ?></td>
                                        <td><?= $resource->minor_outcome->major_outcome->name ?></td>
                                        <td>
                                            <a href='<?= $this->request->webroot . 'web/resources/'.$resource->minor_outcome->major_outcome->year_level->name.'/'. $resource->minor_outcome->major_outcome->subject->name . '/' . $resource->pdf ?>' target='_blank' class='btn btn-sm btn-info'>View</a>
                                        </td>
                                    </tr>
                                <?php endif; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('tutor/resources1.js'); ?>