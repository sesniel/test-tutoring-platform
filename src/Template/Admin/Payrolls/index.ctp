<style>
    .form-control{
        height: 22px;
    }
    .textDate{
        height: 35px !important;
    }
    .selectDate{
        position:relative;
        top:20px;
    }
    .headerDisplay{
        position:relative;
        top:-15px;
    }
    .input-group-addon {
        font-size: 8px;
    }
</style>
<?php //debug($tutorsInfo->toArray());exit;
    $dateCondition = date('Ymd', strtotime($dateRestrict));    
?>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<div id="dialog-confirm"></div>
<div class="widget-box">
        <div class="widget-title">
            <div class="col-md-6">
                <h2 class="headerDisplay">
                    <strong>Payroll Process</strong>
                </h2>
            </div>
            <div class="col-md-2 text-center">
                  <br> <i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
            </div>
            <div class="col-md-4">
                
                <div class="row ">
                <?= $this->Form->create(null, ['url' => ['action' => 'index'], 'method' => 'post']); ?>
                <div class="col-md-6 ">
                    <div class="input-group selectDate">
                        <?php 
                            if(!empty($dateRestrict)):
                                $displayDate = date('d/m/Y', strtotime($dateRestrict));
                            else:
                                $displayDate = "";
                            endif; 
                        ?>
                        <input class="form-control accessDate textDate" value="<?= $displayDate ?>" type="text" id="payroll_date" name="date" required>
                        <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success selectDate" value="Submit" />
                </div>
                <?= $this->Form->end(); ?>
                </div>
            </div>    
        </div>
        <?php 
            if(!empty($tutorsInfo)):
                
            $inputList = "";
            $totalLengthValue = 0;  
            $totalBasic = 0;
            $totalIntermediate = 0;
            $totalSenior = 0;
            $totalLengthRateValue = 0;  
            foreach($tutorsInfo as $rootKey => $tutor): 
            $accessLeads= array();
            $totalSubLengthValue = 0;  
            $totalSubLengthRateValue = 0;  
            $bodyDisplay = "";
            $bodyConsult = "";
            
            $totalSubLengthValue1 = 0;
            $totalSubLengthValue2 = 0;
            $totalSubLengthValue3 = 0;
            
            $totalSubLengthValueRate1 = 0;
            $totalSubLengthValueRate2 = 0;
            $totalSubLengthValueRate3 = 0;
            
        ?>
        
        <?php if(!empty($tutor['programs']) || !empty($tutor['leads'])): //!empty($tutor['programs']) && ?>

        <?php $header = '<div class="row">
                            <div class="col-lg-12">
                            <table class="table">

                                <tr class="success">
                                    <td colspan="7"><p style="font-size:20px;" class="pull-left" > ' . $tutor->first_name . ' ' . $tutor->last_name . '</p></td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th>Client</th>
                                    <th>Student</th>
                                    <th>Length</th>
                                    <th>Rate Type</th>
                                    <th>Scheduled Date</th>
                                    <th>Time</th>
                                </tr>
                                ';
                
                if(!empty($tutor['leads'])): 
                    foreach($tutor['leads'] as $key => $lead):
                                        
                        if(!empty($lead->consultation)): 
                            $accessLeads[$tutor['id']][$lead['consultation_id']]['consultation']    = $lead['consultation'];
                            $accessLeads[$tutor['id']][$lead['consultation_id']]['client']          = $lead['client'];                            
                        endif;
                                                
                    endforeach; 
                endif; 
                
                    if(!empty($tutor['programs'])): 
                        
                    foreach($tutor['programs'] as $program): 
                    
                        foreach($program['payroll_lessons'] as $lesson):  
                            if(date('Ymd', strtotime($lesson->date)) <= date('Ymd', strtotime($dateRestrict))):

                                $totalLengthValue += $lesson->length; 
//                                $totalSubLengthValue += $lesson->length; 
                                
                                $StudentCollection = "";
                                if(!empty($lesson->lesson_students)):
                                    $x = 1;
                                    foreach($lesson->lesson_students as $sKey => $studentInfo):
                                        $StudentCollection .= $studentInfo->program_student->student->name ;
                                        if(count($lesson->lesson_students) > $x):
                                            $StudentCollection .= ", ";
                                        endif;
                                        $x++;
                                    endforeach;
                                endif;
                                
                                if(!empty($lesson->rate)):
                                    $rateId = $lesson->rate_id;
                                    $rate = $lesson->rate->amount;
                                    $rateName = $lesson->rate->name;
                                elseif(!empty($program->rate)):
                                    $rateId = $program->rate_id;
                                    $rate = $program->rate->amount;
                                    $rateName = $program->rate->name;
                                else:
                                    $rateId = $tutor->rate->id;
                                    $rate = $tutor->rate->amount;
                                    $rateName = $tutor->rate->name;
                                endif;
                                
                                $amountTotal = $rate * $lesson->length;
//                                $totalSubLengthRateValue += $amountTotal;
                                
                                if($rateName == "Intermediate"):
                                    $totalSubLengthValueRate2 += $amountTotal;
                                    $totalSubLengthValue2 += $lesson->length;
                                elseif($rateName == "Senior"):
                                    $totalSubLengthValueRate3 += $amountTotal;
                                    $totalSubLengthValue3 += $lesson->length;
                                else:
                                    $totalSubLengthValueRate1 += $amountTotal;
                                    $totalSubLengthValue1 += $lesson->length;
                                endif;
                                
                                $inputList .= "<input type='hidden' name='lesson_id[".$lesson->id."]' value=".$lesson->length." />";
                                $inputList .= "<input type='hidden' name='rate_id[".$lesson->id."]' value=".$rateId." />";
                                
                   
                                $bodyDisplay .= ' 
                                    <tr id="leadRow1" class="clonedInput">
                                        <td>Tutoring</td>
                                        <td>'. $program['client']['first_name'] . " " . $program['client']['last_name'] .'</td>
                                        <td>'. $StudentCollection .'</td>
                                        <td>'. $lesson->length .'</td>
                                        <td>'. $rateName .'</td>
                                        <td>'. date('d/m/Y', strtotime($lesson->date)) .'</td>
                                        <td>'. date('h:i a', strtotime($lesson->time)) .'</td>
                                    </tr>
                                    ';

                        endif;
                        endforeach; 
                    
                    endforeach; 
                
                endif; 
                

        endif; 
        
        if(!empty($accessLeads)): 
            
            foreach($accessLeads as $key => $consult): 
            
                foreach($consult as $key2 => $const):
            
                        $consultValue = checkStudentNumber($const['client']['students']);
//                        echo $consultValue;exit;
//            debug($const);exit;
        
                    $dateConditionCompare = date('Ymd', strtotime($const['consultation']['consultation_session_date']));  
                    
                    if(($const['consultation']['status'] == "Completed Consultation" || 
                            $const['consultation']['status'] == "Booked" || 
                            $const['consultation']['status'] == "Discontinued Consultation")
                        && $const['consultation']['payroll_date'] == "" && $dateConditionCompare <= $dateCondition):
                        
                        $consultValue = checkStudentNumber($const['client']['students']);
                        
                        $totalLengthValue += $consultValue;
                        
                        if($const['consultation']['consultation_session_date'] != ""):
                            $showDate = date('d/m/Y', strtotime($const['consultation']['consultation_session_date']));
                        else:
                            $showDate = " - ";
                        endif;
                        $inputList .= "<input type='hidden' name='consultation_id[".$const['consultation']['id']."]' value=".$const['consultation']['id']." />";

                        $amountTotal = $consultValue * $tutor->rate->amount;
//                        $totalSubLengthRateValue += $amountTotal;
//                        $totalSubLengthValueRate1 += $amountTotal;

                        if($tutor->rate->name == "Intermediate"):
                            $totalSubLengthValueRate2 += $amountTotal;
                            $totalSubLengthValue2 += $consultValue;
                        elseif($tutor->rate->name == "Senior"):
                            $totalSubLengthValueRate3 += $amountTotal;
                            $totalSubLengthValue3 += $consultValue;
                        else:
                            $totalSubLengthValueRate1 += $amountTotal;
                            $totalSubLengthValue1 += $consultValue;
                        endif;
                        
                        $stdnameCol = "";
                        foreach($const['client']['students'] as $key => $student):
                            if(count($const['client']['students']) <= 1):
                                $stdnameCol .= $student['name'];
                            else:
                                if(count($const['client']['students']) == ($key + 1)):
                                    $stdnameCol .= $student['name'];
                                else:
                                    $stdnameCol .= $student['name'] . ', ';    
                                endif;
                                    
                            endif;
                        endforeach;
                        
                        $bodyConsult .= ' 
                                    <tr id="leadRow1" class="clonedInput">
                                        <td>Consultation --> '.$const['consultation']['status'].'</td>
                                        <td>'.$const['client']['first_name']." ".$const['client']['last_name'].'</td>
                                        <td>'.$stdnameCol.'</td>
                                        <td>'.$consultValue.'</td>
                                        <td>'. $tutor->rate->name .'</td>
                                        <td>' . $showDate . '</td>
                                        <td>-</td>
                                    </tr>
                                    ';

                    endif;
                    
                endforeach;
            
                
            endforeach;
            
        endif;
        
        $totalSubLengthValue = $totalSubLengthValue1 + $totalSubLengthValue2 + $totalSubLengthValue3;
        $totalSubLengthRateValue = number_format($totalSubLengthValueRate1, 2) + number_format($totalSubLengthValueRate2, 2) + number_format($totalSubLengthValueRate3, 2);
        
        $footer = '<tr class="info"><td colspan="7"><br/></td></tr>
                    <tr class="info">
                        <td colspan="3"></th>
                        <th>Basic</th>
                        <th>Intermediate</th>
                        <th>Senior</th>
                        <th>Total</th>
                    </tr>
                    <tr class="info">
                        <td colspan="3"><div class="pull-right">Hours</div></td>
                        <td>'. $totalSubLengthValue1 .'</td>
                        <td>'. $totalSubLengthValue2 .'</td>
                        <td>'. $totalSubLengthValue3 .'</td>
                        <th>'. $totalSubLengthValue .'</th>
                    </tr>
                    <tr class="info">
                        <td colspan="3"><div class="pull-right">Computation</div></td>
                        <td>$ '. number_format($totalSubLengthValueRate1, 2, '.', ',') .'</td>
                        <td>$ '. number_format($totalSubLengthValueRate2, 2, '.', ',') .'</td>
                        <td>$ '. number_format($totalSubLengthValueRate3, 2, '.', ',') .'</td>
                        <th>$ '. number_format($totalSubLengthRateValue, 2, '.', ',') .'</th>
                    </tr>

                    </table>
                    <br/>&nbsp;
                    </div>
                </div>';
        
        $totalBasic += $totalSubLengthValue1;
        $totalIntermediate += $totalSubLengthValue2;
        $totalSenior += $totalSubLengthValue3;
        
        $totalLengthRateValue += $totalSubLengthRateValue;
        
        if($bodyDisplay != "" || $bodyConsult != ""):
            echo $header;
            echo $bodyConsult;
            echo $bodyDisplay;
            echo $footer;
        endif;
        
        endforeach; 
        
        ?>
        
        <div class="row">
            <div class="col-lg-12">
                <?php
                    echo '<table class="table">
                <tr class="info">
                    <td>
                        <p style="font-size:15px;" class="pull-right">
                        Basic: <strong>'. $totalBasic .'  Hours</strong>
                        </p>
                    </td>
                    <td>
                        <p style="font-size:15px;" class="pull-right">
                        Intermediate: <strong>'. $totalIntermediate .'  Hours</strong>
                        </p>
                    </td>
                     <td>
                        <p style="font-size:15px;" class="pull-right">
                       Senior: <strong>'. $totalSenior .'  Hours</strong>
                        </p>
                    </td>
                    <td>
                        <p style="font-size:15px; position:relative; left:-30px;" class="pull-right">
                        Total Length: <strong>'. $totalLengthValue .'  Hours</strong>
                        </p>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="7">
                        <p style="font-size:15px; position:relative; left:-30px;" class="pull-right" >
                        Total Rate Amount: <strong> $ '. number_format($totalLengthRateValue, 2, '.', ',') .' </strong>
                        </p>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="7"> 
                        ' . $this->Form->create(null, ['url' => ['action' => 'updatePayrollDate'], 'method' => 'post',
                            'onsubmit' => 'return confirm("Are you sure you want to save the following payroll date against these lessons?")']) . ' 
                        <div class="input-group pull-right">
                            <span class="input-group-addon" style="font-size: 12px">Payroll Date:</span>
                            <input class="form-control accessDate textDate" name="date" type="text" id="payroll_dateInfo" required>
                            <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                        </div>
                        <br/><br/>
                        <input type="submit" onclick="myFunction()" class="btn btn-success pull-right" value="Save payroll date" />
                        ' . $inputList .' 
                        ' . $this->Form->end() .' 
                    </td>
                </tr>
                </table>';
                ?>
            </div>
        </div>
    
<?php else: ?>
    <div class="well" style="height: 500px;" ></div>
<?php endif; ?>
</div>

<script>
$(document).ready(function () {
    
    $('.accessDate').datepicker({
//        minDate: 0,
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });

    
});
</script>

<?php
    function checkStudentNumber($student){
        
        if(count($student) >= 3):
            $pay = 1;
        elseif(count($student) == 2):
            $pay = .75;
        else:
            $pay = .5;
        endif;
        
        return $pay;
        
    }
?>
