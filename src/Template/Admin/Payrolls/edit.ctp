<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Booking Program</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <?= $this->Form->create(null, ['url' => ['action' => 'updateLesson', $lessonDetails->id]]); ?>
                <div class="row">
                    <div class="col-md-12 top-buffer">
                        <p> Please fill up the program table. Note: Type is populated by the assessments completed by the students </p>
                    </div>
                    <div class="col-md-12 top-buffer">
                        <h4>
                        Tutor Name: <?= $lessonDetails->program->tutor->first_name ?> <?= $lessonDetails->program->tutor->last_name ?>
                        </h4>
                    </div>
                    <div class="col-md-12 top-buffer">
                        <h4>
                        Student Name: <?= $lessonDetails->program->student->name ?>
                        </h4>
                    </div>
                    <div class="col-md-12">
                        <table class="tbll table table-bordered">
                            <thead>
                            <tr>
                                <td>Date</td>
                                <td>Time</td>
                                <td>Length(hours)</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="leadRow1" class="clonedInput">
                                <td>
                                    <div class="input-group"><input class="form-control ldate" id="ldate"
                                                                    type="text" name="lessonDate" value="<?php echo date('d/m/Y', strtotime($lessonDetails->date)); ?>" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span></div>
                                </td>
                                <td>
                                    <div class="input-group"><input class="form-control lessonTime" id="lessonTime"
                                                                    type="text" name="lessonTime" value="<?php echo date('h:i a', strtotime($lessonDetails->time)); ?>" required><span
                                            class="input-group-addon"><i
                                                class="glyphicon glyphicon-time"></i></span></div>
                                </td>
                                <td>
                                    <input type="text" name="length" class="form-control" maxlength="5" value="<?php echo $lessonDetails->length; ?>" required/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-12 text-center">
                        <button id="cpBtn" class="btn btn-default">Update Lesson</button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
    
</div>
<?php // debug($lessonDetails); ?>    


