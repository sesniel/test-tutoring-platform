<style>
    .form-control{
        height: 22px;
    }
    .textDate{
        height: 35px !important;
    }
    .selectDate{
        position:relative;
        top:20px;
    }
    .headerDisplay{
        position:relative;
        top:-15px;
    }
    .input-group-addon {
        font-size: 8px;
    }
</style>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
<div id="dialog-confirm"></div>
<div class="widget-box">
        <div class="widget-title">
            <div class="col-md-3">
                <h2 class="headerDisplay">
                    <strong>Payroll Process</strong>
                </h2>
            </div>
            <div class="col-md-5"></div>
            <div class="col-md-4">
                <div class="row ">
                <?= $this->Form->create(null, ['url' => ['action' => 'index'], 'method' => 'post']); ?>
                <div class="col-md-6 ">
                    <div class="input-group selectDate">
                        <?php 
                            if(!empty($dateRestrict)):
                                $displayDate = date('d/m/Y', strtotime($dateRestrict));
                            else:
                                $displayDate = "";
                            endif; 
                        ?>
                        <input class="form-control accessDate textDate" value="<?= $displayDate ?>" type="text" id="payroll_date" name="date" required>
                        <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success selectDate" value="Submit" />
                </div>
                <?= $this->Form->end(); ?>
                </div>
            </div>    
        </div>
        <?php 
            if(!empty($tutorsInfo)):
                
            $inputList = "";
            $totalLengthValue = 0;  
            foreach($tutorsInfo as $rootKey => $tutor): 
            $accessLeads= array();
            $totalSubLengthValue = 0;  
            $bodyDisplay = "";
            $bodyConsult = "";
        ?>
        
        <?php if(!empty($tutor['programs']) && !empty($tutor['leads'])): ?>

        <?php $header = '<div class="row">
                            <div class="col-lg-12">
                            <table class="table">

                                <tr class="success">
                                    <td colspan="6"><p style="font-size:20px;" class="pull-left" > ' . $tutor->first_name . ' ' . $tutor->last_name . '</p></td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Name</th>
                                    <th>Length</th>
                                    <th>Scheduled Date</th>
                                    <th>Time</th>
                                </tr>
                                ';
                
                if(!empty($tutor['leads'])): 
                    foreach($tutor['leads'] as $key => $lead):
                        
                        if(!empty($lead->consultation)): 
                            $accessLeads[$tutor['id']][$lead['consultation_id']]['consultation']    = $lead['consultation'];
                            $accessLeads[$tutor['id']][$lead['consultation_id']]['client']          = $lead['client'];
                        endif;
                                                
                    endforeach; 
                endif; 
                    
                    if(!empty($tutor['programs'])): 
                        
                    foreach($tutor['programs'] as $program): 
                    
                        foreach($program['payroll_lessons'] as $lesson):  
                            if(date('Ymd', strtotime($lesson->date)) <= date('Ymd', strtotime($dateRestrict))):

                                $totalLengthValue += $lesson->length; 
                                $totalSubLengthValue += $lesson->length; 
                                $inputList .= "<input type='hidden' name='lesson_id[".$lesson->id."]' value=".$lesson->length." />";

                                $StudentCollection = "";
                                if(!empty($lesson->lesson_students)):
                                    $x = 1;
                                    foreach($lesson->lesson_students as $sKey => $studentInfo):
                                        $StudentCollection .= $studentInfo->program_student->student->name ;
                                        if(count($lesson->lesson_students) > $x):
                                            $StudentCollection .= ", ";
                                        endif;
                                        $x++;
                                    endforeach;
                                endif;

                                $bodyDisplay .= ' 
                                    <tr id="leadRow1" class="clonedInput">
                                        <td>Tutoring</td>
                                        <td>'. $lesson->status .'</td>
                                        <td>'. $StudentCollection .'</td>
                                        <td>'. $lesson->length .'</td>
                                        <td>'. date('d/m/Y', strtotime($lesson->date)) .'</td>
                                        <td>'. date('h:i a', strtotime($lesson->time)) .'</td>
                                    </tr>
                                    ';

                        endif;
                        endforeach; 
                    
                    endforeach; 
                
                endif; 
                

        endif; 
        
        if(!empty($accessLeads)):
            
            foreach($accessLeads as $key => $consult):
            
                foreach($consult as $key2 => $const):
            
                    if($const['consultation']['status'] == "Completed Consultation" && $const['consultation']['payroll_date'] == ""):
                        $totalSubLengthValue += .5;
                        $totalLengthValue += .5;
                        if($const['consultation']['consultation_session_date'] != ""):
                            $showDate = date('d/m/Y', strtotime($const['consultation']['consultation_session_date']));
                        else:
                            $showDate = " - ";
                        endif;
                        
                        $bodyConsult .= ' 
                                    <tr id="leadRow1" class="clonedInput">
                                        <td>Consultation</td>
                                        <td>'.$const['consultation']['status'].'</td>
                                        <td>'.$const['client']['first_name']." ".$const['client']['last_name'].'</td>
                                        <td>0.5</td>
                                        <td>' . $showDate . '</td>
                                        <td>-</td>
                                    </tr>
                                    ';

                    endif;
                    
                endforeach;
            
                
            endforeach;
            
        endif;
        
        $footer = ' <tr class="info">
                        <td colspan="6">
                            <p style="font-size:15px; position:relative; left:-30px;" class="pull-right" >
                            Total Sub Length: <strong>'. $totalSubLengthValue .'  Hours</strong>
                            </p>
                        </td>
                    </tr>

                    </table>
                    <br/>&nbsp;
                    </div>
                </div>';
        
        
        if($bodyDisplay != ""):
            echo $header;
            echo $bodyConsult;
            echo $bodyDisplay;
            echo $footer;
        endif;
        
        endforeach; 
        
        ?>
        
        <div class="row">
            <div class="col-lg-12">
                <?php
                    echo '<table class="table">
                <tr class="info">
                    <td colspan="6">
                        <p style="font-size:15px; position:relative; left:-30px;" class="pull-right" >
                        Total Length: <strong>'. $totalLengthValue .'  Hours</strong>
                        </p>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="6"> 
                        ' . $this->Form->create(null, ['url' => ['action' => 'updatePayrollDate'], 'method' => 'post',
                            'onsubmit' => 'return confirm("Are you sure you want to save the following payroll date against these lessons?")']) . ' 
                        ' . $inputList .' 
                        <div class="input-group pull-right">
                            <span class="input-group-addon" style="font-size: 12px">Payroll Date:</span>
                            <input class="form-control accessDate textDate" name="date" type="text" id="payroll_dateInfo" required>
                            <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                        </div>
                        <br/><br/>
                        <input type="submit" onclick="myFunction()" class="btn btn-success pull-right" value="Save payroll date" />
                        ' . $this->Form->end() .' 
                    </td>
                </tr>
                </table>';
                ?>
            </div>
        </div>
    
<?php else: ?>
    <div class="well" style="height: 500px;" ></div>
<?php endif; ?>
</div>

<script>
$(document).ready(function () {
    
    $('.accessDate').datepicker({
//        minDate: 0,
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });

    
});




</script>
