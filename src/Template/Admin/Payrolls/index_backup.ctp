<style>
    .form-control{
        height: 22px;
    }
    .textDate{
        height: 35px !important;
    }
    .selectDate{
        position:relative;
        top:20px;
    }
    .headerDisplay{
        position:relative;
        top:-15px;
    }
    .input-group-addon {
        font-size: 8px;
    }
</style>
<div class="widget-box">
        <div class="widget-title">
            <div class="col-md-3">
                <h2 class="headerDisplay">
                    <strong>Payroll Process</strong>
                </h2>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-3">
                <div class="input-group pull-right selectDate">
                    <input class="form-control accessDate textDate" type="text" id="payroll_date" onchange="sendForm" required>
                    <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                </div>
            </div>    
        </div>
        <?php 
            foreach($tutorsInfo as $tutor): 
            $totalLengthValue = 0;  
        ?>
        
        <?php if(!empty($tutor['programs']) && !empty($tutor['leads'])): ?>
        
        <div class="row">
            <div class="col-lg-12">
            <table class="table">
                
                <tr class="success">
                    <td colspan="8"><p style="font-size:20px;" class="pull-left" ><?= $tutor->first_name ?> <?= $tutor->last_name ?></p></td>
                </tr>
                <tr>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Length</th>
                    <th>Scheduled Date</th>
                    <th>Time</th>
                    <th>Payroll Date</th>
                    <th></th>
                </tr>
                
                <?php if(!empty($tutor['leads'])): ?>
                
                <?Php 
                    foreach($tutor['lead_payroll'] as $key => $lead): 
                       $totalLengthValue += 0.5; 
                ?>
                    <tr>
                        <td>Consultation</td>
                        <td><?= $lead->status ?></td>
                        <td><?= $tutor['leads'][$key]['client']['first_name'] . " " . $tutor['leads'][$key]['client']['last_name'] ?></td>
                        <td>0.5</td>
                        <td><?php echo date('d/m/Y', strtotime($lead->consultation_session_date)) ?></td>
                        <td> - </td>
                        <td>
                            <?php
                                if($lead->payroll_date == ""):
                                    echo '<div id="result_lead'.$lead->id.'"></div>';
                                endif;
                            ?>
                        </td>
                        <td style="width:200px">
                            <?php if($lead->payroll_date == ""): ?>
                            <?= $this->Form->create(null, ['id' => 'sendajaxlead' . $lead->id, 'method' => 'post']); ?>
                                
                                <div class="input-group">
                                    <input class="form-control accessDate" type="text" id="payroll_date_lead<?= $lead->id ?>" onchange="sendFormLead<?= $lead->id ?>()" required>
                                    <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                                
                                    <?php 
//                                        echo $this->Html->link(
//                                            "<i class='fa fa-pencil-square-o fa-2x' aria-hidden='true'></i>",
//                                            array('action' => 'leadedit',$lead->id),
//                                            array('escape' => false, 'style' => 'padding-left: 10px;')
//                                        );
                                    ?>
                                </div>
                            
                                <input type="hidden" id="id_lead<?= $lead->id ?>" value="<?= $lead->id ?>" />
                                
                                <script type="text/javascript">
                                    
                                   function sendFormLead<?= $lead->id ?>(){

                                        var payroll = $("#payroll_date_lead<?= $lead->id ?>").val();
                                        var id_info = $("#id_lead<?= $lead->id ?>").val();

                                        $.ajax({
                                        type: "POST",
                                        url: "payrolls/ajaxSavePayrollDateLead",
                                        data: {
                                            payroll_date : payroll,
                                            id: id_info
                                        },
                                        cache: false,
                                        success:  function(data){
                                            $("#result_lead<?= $lead->id ?>").html(data);
//                                            console.log(data);
                                        }
                                      });

                                    }
                                </script>
                                
                                
                                
                                
                                
                            <?= $this->Form->end(); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                
                <?php endif; ?>
                    
                    
                
                <?php if(!empty($tutor['programs'])): ?>
                
                <?Php foreach($tutor['programs'] as $program): ?>
                <?Php 
                    foreach($program['payroll_lessons'] as $lesson): 
                        $totalLengthValue += $lesson->length; 
                ?>
                    <tr id="leadRow1" class="clonedInput">
                        <td>Tutoring</td>
                        <td><?= $lesson->status ?></td>
                        <td><?= $program->student->name ?></td>
                        <td><?= $lesson->length ?></td>
                        <td><?php echo date('d/m/Y', strtotime($lesson->date)) ?></td>
                        <td><?php echo date('h:i a', strtotime($lesson->time)) ?></td>
                        <td>
                            <?php
                                if($lesson->payroll_date == ""):
                                    echo '<div id="result'.$lesson->id.'"></div>';
                                endif;
                            ?>
                        </td>
                        <td style="width:200px">
                            <?php if($lesson->payroll_date == ""): ?>
                            <?= $this->Form->create(null, ['url' => ['action' => 'ajaxSavePayrollDate'], 'id' => 'sendajax' . $lesson->id, 'method' => 'post']); ?>
                                
                                <div class="input-group">
                                    <input class="form-control accessDate" type="text" id="payroll_date<?= $lesson->id ?>" onchange="sendForm<?= $lesson->id ?>()" required>
                                    <span class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </span>
                                    
                                    <?php                                     
//                                        echo $this->Html->link(
//                                            "<i class='fa fa-pencil-square-o fa-2x' aria-hidden='true'></i>",
//                                            array('action' => 'edit',$lesson->id),
//                                            array('escape' => false, 'style' => 'padding-left: 10px;')
//                                        );
                                    ?>
                                </div>
                                
                                <input type="hidden" id="id<?= $lesson->id ?>" value="<?= $lesson->id ?>" />
                                
                                <script type="text/javascript">
                                    
                                   function sendForm<?= $lesson->id ?>(){

                                        var payroll = $("#payroll_date<?= $lesson->id ?>").val();
                                        var id_info = $("#id<?= $lesson->id ?>").val();

                                        $.ajax({
                                        type: "POST",
                                        url: "payrolls/ajaxSavePayrollDateLesson",
                                        data: {
                                            payroll_date : payroll,
                                            id: id_info
                                        },
                                        cache: false,
                                        success:  function(data){
                                            $("#result<?= $lesson->id ?>").html(data); 
                                        }
                                      });

                                    }
                                </script>
                                
                                
                                
                                
                                
                            <?= $this->Form->end(); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php endforeach; ?>
                
                <?php endif; ?>
                <tr class="info">
                    <td colspan="8">
                        <p style="font-size:15px; position:relative; left:-30px;" class="pull-right" >
                        Total Length: <strong><?= $totalLengthValue ?> Hours</strong>
                        </p>
                    </td>
                </tr>

            </table>
            <br/>&nbsp;
            </div>
        </div>
        
        <?php endif; ?>
        
        
        
        <?php endforeach; ?>
        
        <div class="row">
            <div class="col-lg-12">
                
                <?php // pr($tutorsInfo->toArray()) ?>
            </div>
        </div>
        
</div>

<script>
$(document).ready(function () {


    $('.accessDate').datepicker({
//        minDate: 0,
        dateFormat: 'dd/mm/yy',
        maxDate: "7m"
    });

    
});




</script>
<?php // debug($tutorsInfo->toArray()); ?>