<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Questions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Minor Outcomes'), ['controller' => 'MinorOutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Minor Outcome'), ['controller' => 'MinorOutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assessments'), ['controller' => 'Assessments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assessment'), ['controller' => 'Assessments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Questions'), ['controller' => 'UserQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Question'), ['controller' => 'UserQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="questions form large-9 medium-8 columns content">
    <?= $this->Form->create($question) ?>
    <fieldset>
        <legend><?= __('Add Question') ?></legend>
        <?php
            echo $this->Form->input('minor_outcome_id', ['options' => $minorOutcomes]);
            echo $this->Form->input('description');
            echo $this->Form->input('image');
            echo $this->Form->input('answer');
            echo $this->Form->input('choices');
            echo $this->Form->input('mark');
            echo $this->Form->input('type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
