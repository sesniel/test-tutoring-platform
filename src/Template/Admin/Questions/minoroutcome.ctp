<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Question'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Minor Outcomes'), ['controller' => 'MinorOutcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Minor Outcome'), ['controller' => 'MinorOutcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Assessments'), ['controller' => 'Assessments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Assessment'), ['controller' => 'Assessments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Questions'), ['controller' => 'UserQuestions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Question'), ['controller' => 'UserQuestions', 'action' => 'add']) ?></li>
    </ul>
</nav> -->
<div class="row"> <!-- New Leads -->
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong><?= __('Questions') ?></strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="questionsTbl"
                                       class="table table-striped responsive display table-hover table-bordered">
                                   
                                    <thead>
                                      
                                    <tr> 
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?=$this->Paginator->sort('question')?></th>
                                        <th><?= $this->Paginator->sort('choices') ?></th>
                                        <th><?=$this->Paginator->sort('answer')?></th>
                                        
                                        <th class="actions"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         
                                        <?php foreach ($questions as $question) : ?>
                                        
                                        <tr>
                                            <td><?= $question->id ?></td>
                                            <td><?= $question->description ?></td>
                                            <td><?= $question->choices ?></td>
                                            <td><?= $question->answer ?> </td> 
                                          
                                            <td>
                                              <?= $this->Html->link(__('Edit'), ['action' => 'edit', $question->id], ['class' => 'btn btn-success']) ?>
                                          
                                            </td> 
                                            
                                    
                                      <?php endforeach; ?>
                                    
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="paginator col-lg-12 col-centered">
                                        <ul class="pagination">
                                            
                                        </ul>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
