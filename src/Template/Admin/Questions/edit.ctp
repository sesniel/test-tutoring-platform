<?= $this->Form->create($question) ?>
<div class="content well">
    
    <div class="row">
        <div class="col-lg-12"><legend>Question Display</legend></div>
        <div class="col-md-12">
            <h2 class="text-center"><?php echo $question->description; ?></h2>
        </div>
        <div class="col-md-12">
            <?php
            if ($question->image != ""):
                $imageDetails = $question->minor_outcome->major_outcome->subject->name . "/" . $question->image;
                echo $this->Html->image($imageDetails,
                    ['border' => '0', 'class' => 'img-responsive center-block']);
            endif;

            ?>
            <br/>
        </div>
        <div class="col-md-12 text-center">
            <div class="btn-group" data-toggle="buttons">
                <?php
                $questions = json_decode($question->choices, true);
                $x = 1;
                foreach ($questions as $question1):
                    ?>

                    <label class="btn btn-md btn-primary">
                        <input type="radio" id="answer<?= $x ?>" name="answer"
                               autocomplete="off"
                               value="<?= $question1 ?>"
                            >
                        <?= $question1 ?>
                    </label>

                    <?php $x++; endforeach; ?>

            </div>
        </div>
        <div class="col-md-12 text-center"><hr/></div>
        
    </div>
    
            <?php // debug($question); ?>
    
    
    <div class="row">
        <div class="col-lg-12"><legend><?= __('Update Question') ?></legend></div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Type</label>
                <?php echo $this->Form->input('type', [
                                                        'label' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Question</label>
                <?php echo $this->Form->input('description', [
                                                        'label' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Answer</label>
                <?php echo $this->Form->input('answer', [
                                                        'label' => false,
                                                        'required' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Choices (["1","2","3","4"])</label>
                <?php echo $this->Form->input('choices', [
                                                        'label' => false,
                                                        'required' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Mark (["2","4","6","8"])</label>
                <?php echo $this->Form->input('mark', [
                                                        'label' => false,
                                                        'required' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="active">Image</label>
                <?php echo $this->Form->input('image', [
                                                        'label' => false,
                                                        'required' => false,
                                                        'class' => 'form-control'
                                            ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">            
            <div>
                <?= $this->Form->button(__('Update Question'), ['class' => 'btn btn-info pull-right']) ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>


<?php // debug($question) ?>