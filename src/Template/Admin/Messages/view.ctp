<style>
    table, td, th{
        width: 200px;
    }
    .btn-xs{
        display: inline-block; 
        border: 5px solid #fff;
    }
</style>
<div class="widget-box">
    <div class="widget-title">
        <div class='row'>
            <div class="col-md-6">
                <h2 class="headerDisplay">
                    <strong>Messages Details</strong>
                </h2>
            </div>
            <div class='col-md-6'>
                <br/>&nbsp;
                <?php
    //                echo $this->Html->link(
    //                        "Create message",
    //                        ['action' => 'create', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary pull-right", 'target' => '_blank']);

                ?>
            </div>
        </div>
    </div>
    <div class="widget-container">
        <div class='row'>
            <div class="col-md-4">
                <table>
                    <tr>
                        <th scope="row">Created By</th>
                        <td><?= $message->user->first_name . " " . $message->user->last_name ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?php echo date('d/m/Y', strtotime($message->created)); ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8"></div>
            <div class="col-md-12"><hr/></div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Subject</h4>
                        <?= $this->Text->autoParagraph(h($message->subject)); ?>
                    </div>
                    <div class="col-md-12">
                        <h4>Message</h4>
                        <?= $this->Text->autoParagraph(h($message->message)); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                
            </div>
        </div>
    </div>
</div>

