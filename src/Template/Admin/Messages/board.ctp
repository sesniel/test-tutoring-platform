<div class="widget-box">
    <div class="widget-title">
        <div class="col-md-6">
            <h2 class="headerDisplay">
                <strong>Messages View</strong>
            </h2>
        </div>
        <div class='col-md-6'>
            <br/>&nbsp;
            <?php
                echo $this->Html->link(
                        "Create message",
                        ['action' => 'create', 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-primary pull-right", 'target' => '_blank']);

            ?>
        </div>
    </div>
    <div class="widget-container">
        <div class='row'>
        <div class="col-md-12 content">
            <table class='table'>
                <thead>
                    <tr>
                        <td colspan='6' class='pull-left'>
                            
                            <?php
                                echo $this->Form->create(null, [
                                    'url' => ['action' => 'board'],
                                    'method' => 'post'
                                ]);
                            ?>
                                <select name='count' onchange="this.form.submit()" class='form-control input-sm'>
                                    <option <?php if($count == 10): echo "selected"; endif; ?> value='10'>10</option>
                                    <option <?php if($count == 20): echo "selected"; endif; ?> value='20'>20</option>
                                    <option <?php if($count == 50): echo "selected"; endif; ?> value='50'>50</option>
                                    <option <?php if($count == 100): echo "selected"; endif; ?> value='100'>100</option>
                                </select>
                            <?php echo $this->Form->end(); ?>
                            
                        </td>
                    </tr>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('subject') ?></th>
                        <th scope="col">Notification</th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($messages as $message): ?>
                    <tr>
                        <td><?= $this->Number->format($message->id) ?></td>
                        <td><?php echo $message->user->first_name . " " . $message->user->last_name; ?></td>
                        <td>
                            <?php 
                                if(strlen($message->subject) <= 50):
                                    echo $message->subject;
                                else:
                                    echo substr($message->subject, 0, 50) . "...";
                                endif;
                            ?>
                        </td>
                        <td>
                            <?php
                                if(count($message->message_tags) > 1):
                                    echo '<i class="fa fa-check" aria-hidden="true"></i>';
                                endif;
                            ?>
                        </td>
                        <td><?php echo date("d/m/Y", strtotime($message->created)); ?></td>
                        <td>
                            <?php
                                echo $this->Html->link(
                                        "View",
                                        ['action' => 'view', $message->id, 'prefix' => 'admin'], ['escape' => false, 'class' => "btn btn-xs btn-info"]);

                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator pull-right">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
        </div>    
    </div>
</div>
<?php //debug($messages); ?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Message'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>-->

