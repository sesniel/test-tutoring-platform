<div class="widget-box">
    <div class="widget-title">
        <div class="col-md-12">
            <h2 class="headerDisplay">
                <strong>Create Message</strong>
            </h2>
        </div>
    </div>
    <div class="widget-container">
        <div class="row">
        <div class="col-md-12">
        <?= $this->Form->create(null, ['url' => ['action' => 'saveMessage'], 'method' => 'post']); ?>
            <div class="form-group">
                <input class="form-control" name="title" type="text" placeholder="Subject"/>
            </div>
            <textarea name="content" class="form-control" data-provide="markdown" placholder="Message" rows="12"></textarea>

            <button type="submit" class="btn">Submit</button>
        <?= $this->Form->end(); ?>
        </div>
        </div>
    </div>
</div>




