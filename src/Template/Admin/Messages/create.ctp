<style>
      
    .form-group input[type="checkbox"] {
        display: none;
    }

    .form-group input[type="checkbox"] + .btn-group > label span {
        width: 20px;
    }

    .form-group input[type="checkbox"] + .btn-group > label span:first-child {
        display: none;
    }
    .form-group input[type="checkbox"] + .btn-group > label span:last-child {
        display: inline-block;   
    }

    .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
        display: inline-block;
    }
    .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
        display: none;   
    }
    
    .text-info{
        font-size: 10px !important;
        width: 75px;
    }
    
    .text-info1{
        font-size: 10px !important;
        width: 30 px;
    }
    
    .text-info2{
        font-size: 10px !important;
        width: 30 px;
    }
    
    .text-info-long{
        font-size: 10px !important;
        width: 150px;
    }
</style>
<script>
    
</script>
<script>
$(function () {

    $('#tutor_available').change(function(){
        if (this.checked) {
            $('#tutorInfo').fadeIn('slow');
        }
        else {
            $('#tutorInfo').fadeOut('slow');
        }                   
    });

    $('#tutor_email_all').change(function(){
        if (this.checked) {
            $('#tutorEmail').fadeIn('slow');
        }
        else {
            $('#tutorEmail').fadeOut('slow');
        }                   
    });
        

    $('#studentSelect').multiSelect({
      selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search for tutor'>",
      selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Search for selected tutor'>",
      afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on('keydown', function(e){
          if (e.which === 40){
            that.$selectableUl.focus();
            return false;
          }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on('keydown', function(e){
          if (e.which == 40){
            that.$selectionUl.focus();
            return false;
          }
        });
      },
      afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
      }
    });

});
</script>
<div class="widget-box">
    <div class="widget-title">
        <div class="col-md-12">
            <h2 class="headerDisplay">
                <strong>Create Message</strong>
            </h2>
        </div>
    </div>
    <div class="widget-container">
        <?= $this->Form->create(null, ['url' => ['action' => 'saveMessage'], 'method' => 'post']); ?>
        <div class="row">
            <div class='col-md-8'>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input class="form-control" name="subject" type="text" maxlength="150" placeholder="Subject" required="" />
                        </div>
                    </div>            
                    <div class="col-md-12">
                        <textarea name="message" class="form-control" data-provide="markdown" placholder="Message" rows="12" required="" ></textarea>
                    </div>
                </div>
            </div>
            <div class='col-md-4'>
                <div class="row">
                    
                <div class='col-md-12'>
                    
                    <div class="form-group">
                        <input type="checkbox" name="tutor_email_all" id="tutor_email_all" autocomplete="off" />
                        <div class="[ btn-group ]">
                            <label for="tutor_email_all" style='background-color: #3f7a06; border-color: #ccc;' class="btn btn-info text-info2">
                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                <span> </span>
                            </label>
                            <label for="tutor_email_all" class="btn btn-default text-info-long">
                                Email Announcement
                            </label>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="tutorEmail">
                    <div class='col-md-12'>

                        <div class="form-group">
                            <input type="checkbox" name="tutor" id="tutor_available" autocomplete="off" />
                            <div class="[ btn-group ]">
                                <label for="tutor_available" style='background-color: #3f7a06; border-color: #ccc;' class="btn btn-info text-info1">
                                    <span class="[ glyphicon glyphicon-ok ]"></span>
                                    <span> </span>
                                </label>
                                <label for="Select Recipients" class="btn btn-default text-info-long">
                                    Select Recipients
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="tutorInfo">
                    <div class="col-md-12">
                        <strong>Select Tutor</strong>
                    </div>
                    <div class="col-md-12">
                        <select class="form-control students" id="studentSelect" name="tutorInfo[]" multiple>
                            <?php foreach ($tutorDetails as $tutor): ?>
                                <option value='<?= $tutor->id ?>' ><?= $tutor->first_name . " " . $tutor->last_name ?> </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12 hidden">
                    <div class="form-group">
                      <input type="text" value="Joshua, Sesniel" data-role="tagsinput" class="form-control" />
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='col-md-12'>         
                <br/>&nbsp;<br/>&nbsp;
                <button type="submit" class="btn pull-right">Submit</button>
            </div>            
        </div>
        <?= $this->Form->end(); ?>
    </div>
</div>


