<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Add New Tutor</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <?= $this->Form->create(null, ['url' => ['action' => 'createTutor']]); ?>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Contact Detail:</h4>
                        <p>Please complete client contact details below. Upon submission the tutor will receive an
                            invitation email with their email and system generated password.</p>
                        <hr>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-6">
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="lname">Rate:</label>
                                    <select class='form-control specifyWidth' required name='rate_id' >
                                        <option selected disabled="">Pay Rate</option>
                                        <?php 
                                            foreach($rateDetails as $rate): 
                                                echo "<option value='".$rate->id."' >".$rate->name."</option>";
                                            endforeach; 
                                        ?>
                                    </select>
                            </div>
                            <div class="col-md-6">&nbsp;</div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="fname">First Name:</label>
                                <input type="text" class="form-control" id="fname"
                                       name="fname"
                                       title="First Name" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="lname">Last Name:</label>
                                <input type="text" class="form-control" id="lname"
                                       name="lname" title="Last Name" required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="email">Email:</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       title="Email" required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <label for="phone">Phone:</label>
                                <input type="text" class="form-control" id="phone" name="phone"
                                       title="Phone" maxlength="10">
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="mobile">Mobile:</label>
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       title="Mobile" maxlength="10" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- <div class="row top-buffer">
                            <div class="col-md-3">
                                <label for="unit-number">Unit #:</label>
                                <input type="text" class="form-control" id="unit-number"
                                       name="unit-number"
                                       title="First Name" maxlength="5">
                            </div>
                            <div class="col-md-3">
                                <span class="require-mark">&#42; </span><label for="street-number">Street #:</label>
                                <input type="text" class="form-control" id="street-number"
                                       name="street-number"
                                       title="Street Number" maxlength="5" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="street-name">Street Name:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="street-name"
                                       title="Street Name" required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="postcode">Postcode:</label>
                                <input type="text" class="form-control" id="postcode" name="postcode"
                                       title="Postcode" maxlength="4" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="suburb">Suburb:</label>
                                <input type="text" class="form-control" id="suburb" name="suburb"
                                       title="Suburb" required>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="city" class="required">City:</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       title="City" required>
                            </div>
                            <div class="col-md-6">
                                <span class="require-mark">&#42; </span><label for="state">State:</label>
                                <select id="state" name='state' class="form-control" required>
                                    <option selected='selected' disabled>Please select state</option>
                                    //foreach ($states as $state) : 
                                        <option
                                            value=' //$state->id '> //$state->name </option>
                                    //endforeach;
                                </select>
                            </div>
                        </div>
                        <div class="row top-buffer">
                            <div class="col-md-12">
                                <label for="tbio">Biography:</label>
                                            <textarea class="form-control" rows="1" id="tbio" name="tbio"
                                            ></textarea>
                            </div>
                        </div> -->
                        <div class="row top-buffer">
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="availability">Subjetcs:</label>
                                <select multiple="multiple" id="subjects" name="subjects[]" required>
                                    <?php foreach ($subjs as $subj) : ?>
                                        <option
                                            value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="row top-buffer">
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="email">Availability:</label>
                                <select multiple="multiple" id="availability" name="availability[]" required>
                                </select>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="form-group col-md-12 text-center top-buffer">
                        <button id="tutor-confirm-session-btn" class="btn btn-success">Create Tutor</button>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div><!-- End .row-->

<!-- Remove Detail -->
<div id="remove-section" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Remove Student</h4>
            </div>
            <div class="modal-body row">
                <p class="text-center">Are you sure you want to remove this section?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id='confirm-section-remove' class="btn btn-default" data-dismiss="modal">
                    Confirm
                </button>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/add-tutor.js'); ?>