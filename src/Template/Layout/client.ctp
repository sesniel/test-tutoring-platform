<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title><?php echo $this->name; ?></title>
    <?= $this->Html->css(['bootstrap.min','font-awesome','multi-select','style','dataTables.bootstrap']) ?>
    <?= $this->Html->script('jquery-3.1.1.min.js'); ?>    
</head>
<body>
<div <?php if ($this->name == "Profile" ) {echo "id='profile-container'"; }?> class="container">
    <div class="row navbar-fixed-top" id="top-navigation">
        <header class="col-sm-12 col-md-10">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: 4px')) ?>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">                        
                        <li <?php if ($this->name == "Dashboard") {
                            echo "class='active'";
                        } ?>>
                            <?Php
                            echo $this->Html->link(
                                'Assessments',
                                array('controller' => 'dashboard', 'action' => 'index','prefix' => 'client', '_full' => true),
                                array('escape' => false)  // important
                            );
                            ?>
                        </li>
                         <li <?php if ($this->name == "Bookings") {
                            echo "class='active'";
                        } ?>>
                            <?Php
                            echo $this->Html->link(
                                'Bookings',
                                array('controller' => 'bookings', 'action' => 'table','prefix' => 'client', '_full' => true),
                                array('escape' => false)  // important
                            );
                            ?>
                        </li>
                       <!--  <li <?php if ($this->name == "Calendar"): echo "class='active'"; endif; ?> >
                                    <?php
                                    echo $this->Html->link(
                                        'Calendar',
                                        array('controller' => 'calendar', 'action' => 'lessons', 'prefix' => "client", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                        </li> -->
                        <li <?php if ($this->name == "StudySkills") {
                            echo "class='active'";
                        } ?>>
                            <?Php
                            echo $this->Html->link(
                                'Study Skills',
                                array('controller' => 'studySkills', 'action' => 'index','prefix' => 'client', '_full' => true),
                                array('escape' => false)  // important
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Report") {
                            echo "class='active'";
                        } ?>>
                            <?Php
                            echo $this->Html->link(
                                'Results',
                                array('controller' => 'report', 'action' => 'index','prefix' => 'client', '_full' => true),
                                array('escape' => false)  // important
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Feedback" && $this->request->params['action'] != "students"): echo "class='active'"; endif; ?> >
                            <a href="" class="btn1-floating btn1-large red" data-toggle="modal"
                               data-target="#report-support">Feedback</a>
                        </li>
                        <li>
                            <?php
                            echo $this->Html->link(
                                "Profile",
                                array('controller' => '../profile', 'action' => 'client'),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="col-xs-12 col-sm-12 col-md-2 nav-user">
            <ul class="nav navbar-nav pull-right">
                <li>
                    <?php
                    echo $this->Html->link(
                        "<i class='glyphicon glyphicon-log-out'></i>Logout",
                        array('controller' => '../users', 'action' => 'logout'),
                        array('escape' => false)
                    );
                    ?>
                </li>
            </ul>
        </div>
    </div>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>
</div>

<!-- Feedback -->
<div class="modal fade" id="report-support" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php echo $this->Form->create(null, ['url' => ['controller' => '../support', 'action' => 'support']]);?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-flag"></i> Report / Support</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="token" value="<?= $this->request->session()->read("token") ?>"/>
                        <input type="hidden" name="page" value="<?= 'Client - '.$this->name.'/'.$this->request->params['action'] ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input id="report-subject" type="text" name="subject" class="form-control" placeholder="Subject" required="">
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="report-message" class="form-control" name="message" rows="4" placeholder="Message..." required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<!-- Response Dialog -->
<div id="response-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <p>Copyright © 2017 • <a href="#">Tutor2You</a> | <a href="#">Tutoring Platform</a> • All Rights
        Reserved &reg;</p>
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.quicksearch.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('custom.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<?= $this->Html->script('general.js'); ?>
</body>
</html>