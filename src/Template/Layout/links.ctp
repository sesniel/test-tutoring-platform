<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title><?= $this->name ?></title>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('multi-select.css') ?>
    <?= $this->Html->css('jquery.timepicker.css') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->Html->script('jquery-3.1.1.min.js'); ?>
    <?= $this->Html->css('dataTables.bootstrap.css') ?>
    
</head>
<body>
    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.quicksearch.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>
<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('custom.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<?= $this->Html->script('general.js'); ?>
</body>
</html>