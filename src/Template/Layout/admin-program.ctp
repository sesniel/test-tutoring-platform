<!DOCTYPE html>
<html class="no-js" lang="en"> 
<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="icon.png">
    <title><?= $this->name ?> | Tutoring Platform</title>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('multi-select.css') ?>
    <?= $this->Html->css('dataTables.bootstrap.css') ?>
    <?= $this->Html->css('jquery-ui.css'); ?>
    <?= $this->Html->script('jquery-1.12.1.js'); ?>
</head>
<body>
<div <?php if ($this->name == "Settings") {echo "id='tutor-profile-container'"; }
elseif($this->name == "Lessons" && $this->request->params['action'] == "create" ){echo "id='adminCreateLesson'";}
if ($this->name == "Resources" && $this->request->params['action'] == "index") {echo "style='width:100%'";}
        else{ echo "style='padding-right: 30px; padding-left:30px;'"; } ?> class="container1">

    <div class="row navbar-fixed-top top-notch" id="top-navigation">
        <header class="col-sm-12 col-md-10">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: 4px')) ?>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php if ($this->name == "Dashboard") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Dashboard',
                                array('controller' => 'dashboard', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Leads") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Allocations',
                                array('controller' => 'leads', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                       <li <?php if ($this->name == "Clients") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Clients
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                           
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Active Clients',
                                        array('controller' => 'clients', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false) 
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Clients without bookings',
                                        array('controller' => 'clients', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false) 
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Tutor
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                           
                                <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Active Tutors',
                                    array('controller' => 'tutors', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Tutor Training',
                                        array('controller' => 'trainings', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Recruitment',
                                    array('controller' => 'tutors', 'action' => 'recruitment', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                 <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'WWC',
                                    array('controller' => 'tutors', 'action' => 'wwc', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                 <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Tutor Onboarding',
                                    array('controller' => 'tutors', 'action' => 'tutorOnboarding', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Admin
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'General',
                                        array('controller' => 'trainings', 'action' => 'general', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Process Payroll',
                                        array('controller' => 'payrolls', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                     <?php
                                    echo $this->Html->link(
                                        'Payroll History',
                                        array('controller' => 'payrolls', 'action' => 'report', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Overdue Reports',
                                        array('controller' => 'bookings', 'action' => 'tutor', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Consultation Reports',
                                        array('controller' => 'consultations', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li class="dropdown-submenu">
                                    <?php
                                    echo $this->Html->link(
                                        'KPI',
                                        array('target' => '#','_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'KPI Report',
                                                array('controller' => 'kpi', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Leading Indicators',
                                                array('controller' => 'kpi', 'action' => 'leadingIndicators', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Growth Metrics',
                                                array('controller' => 'kpi', 'action' => 'growthMetrics', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Key Performance Indicators',
                                                array('controller' => 'kpi', 'action' => 'keyPerformanceIndicators', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>


                                    </ul>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Map',
                                        array('controller' => 'maps', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Message Board',
                                        array('controller' => 'messages', 'action' => 'board', 'prefix' => "admin", '_full' => true)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "bookings" || $this->name == "payrolls") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Bookings
                                <b class="caret"></b></a>
                                
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Create Booking',
                                        array('controller' => 'bookings', 'action' => 'create', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Rebookings',
                                        array('controller' => 'bookings', 'action' => 'rebook', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                               
                                <li> </li>
                                
<!--                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Payroll View',
                                        array('controller' => 'payrolls', 'action' => 'view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>-->
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'View Bookings',
                                        array('controller' => 'bookings', 'action' => 'view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Consultations',
                                        array('controller' => 'bookings', 'action' => 'consultations', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Client Bookings',
                                        array('controller' => 'bookings', 'action' => 'lessons', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Uninvoiced Bookings',
                                        array('controller' => 'bookings', 'action' => 'uninvoice_view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li></li>
<!--                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Calendar View',
                                        array('controller' => 'lessons', 'action' => 'calendar', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Table View',
                                        array('controller' => 'lessons', 'action' => 'table', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>-->
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Resources
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                             
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Study Skills',
                                        array('controller' => 'trainings', 'action' => 'studyskills', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Resources',
                                        array('controller' => 'resources', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Year Level Overviews and Curriculum',
                                        array('controller' => 'resources', 'action' => 'yearCumRes', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "DataReview" ) {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Data
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Minor Outcome',
                                        array('controller' => 'dataReview', 'action' => 'minor-outcome', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Questions',
                                        array('controller' => 'questions', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Feedback" && $this->request->params['action'] != "students"): echo "class='active'"; endif; ?> >
                            <a href="" class="btn1-floating btn1-large red" data-toggle="modal"
                               data-target="#report-support">
                                Feedback
                            </a>
                        </li>
                        <li>
                            <?php
                            echo $this->Html->link(
                                "Profile",
                                array('controller' => '../profile', 'action' => 'admin'),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="col-xs-12 col-sm-12 col-md-2 nav-user">
            <ul class="nav navbar-nav pull-right">
                <li>
                    <?php
                    echo $this->Html->link(
                        "<i class='glyphicon glyphicon-log-out'></i>Logout",
                        array('controller' => '../users', 'action' => 'logout'),
                        array('escape' => false)
                    );
                    ?>
                </li>
            </ul>
        </div>
    </div>


    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

</div>

<!-- Feedback -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php
    echo $this->Form->create(null, [
        'url' => ['controller' => '../users', 'action' => 'feedback']
    ]);
    ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feedback</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Page: </label>
                        http://<?php echo $_SERVER['SERVER_NAME'] . "/" . $this->name . "/" . $this->request->params['action'] ?>
                        <input type="hidden" name="server_name"
                               value="http://<?php echo $_SERVER['SERVER_NAME'] . "/" . $this->name . "/" . $this->request->params['action'] ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" name="subject" value="" class="form-control admin-addnewleads-input"
                               placeholder="Subject" required="">
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea class="form-control" name="feedback" rows="11"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Feedback</button>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<!-- Response Dialog -->
<div id="response-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <p>Copyright © 2017 • <a href="#">Tutor2You</a> | <a href="#">Tutoring Platform</a> • All Rights
        Reserved &reg;</p>
</footer>


<?= $this->Html->css('jquery.timepicker.css') ?>
<?= $this->Html->css('jquery-ui.css') ?>

<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>
<?= $this->Html->script('admin/admin.js'); ?>
<?= $this->Html->script('tutor/create-program.js'); ?> 

<?= $this->Html->script('jquery.min.js'); ?>
<?= $this->Html->script('jquery.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<script type="text/javascript">
    var trackcmp_email = '';
    var trackcmp = document.createElement("script");
    trackcmp.async = true;
    trackcmp.type = 'text/javascript';
    trackcmp.src = '//trackcmp.net/visit?actid=223206443&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
    var trackcmp_s = document.getElementsByTagName("script");
    if (trackcmp_s.length) {
            trackcmp_s[0].parentNode.appendChild(trackcmp);
    } else {
            var trackcmp_h = document.getElementsByTagName("head");
            trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
    }
</script>
</body>
</html>