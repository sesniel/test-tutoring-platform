<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title><?php echo $this->name; ?></title>
    
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('multi-select.css') ?>
    <?= $this->Html->script('jquery-3.1.1.min.js'); ?>
    <?= $this->Html->css('dataTables.bootstrap.css') ?>
    <?= $this->Html->css('lity.css') ?>

    <style>
        body{
            padding-top: 0px !important;
        }
    </style>
</head>
<body>
<div class="container">

    <?= $this->fetch('content') ?>
    
</div>

<footer class="footer">
    <p>Copyright © 2017 • <a href="#">Tutor2You</a> | <a href="#">Tutoring Platform</a> • All Rights
        Reserved &reg;</p>
</footer>

<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.quicksearch.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('custom.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<?= $this->Html->script('lity.js'); ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

</body>
</html>