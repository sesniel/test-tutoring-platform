<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title><?= $this->name ?></title>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('multi-select.css') ?>
    <?= $this->Html->css('jquery.timepicker.css') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->Html->script('jquery-3.1.1.min.js'); ?>
    <?= $this->Html->css('dataTables.bootstrap.css') ?>
    
</head>
<style>
   
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    min-width: 220px;
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
    
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
} 
</style>
<body>
<div <?php if ($this->name == "Profile" ) {echo "id='profile-container'"; }
elseif($this->name == "Lessons" && $this->request->params['action'] == "create" ){echo "id='adminCreateLesson'"; }
if ($this->name == "Resources" && $this->request->params['action'] == "index") {echo "style='width:100%'";} ?> class="container">

    <div class="row navbar-fixed-top" id="top-navigation">
        <header class="col-sm-12 col-md-10">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: 4px')) ?>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li <?php if ($this->name == "Dashboard") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Dashboard',
                                array('controller' => 'dashboard', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Leads") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Allocations',
                                array('controller' => 'leads', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Clients") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Clients
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                           
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Active Clients',
                                        array('controller' => 'clients', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                array('escape' => false) 
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Clients without bookings',
                                        array('controller' => 'clients', 'action' => 'without_bookings', 'prefix' => "admin", '_full' => true),
                                array('escape' => false) 
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        
<!--                        <li <?php //if ($this->name == "Tutors"): echo "class='active'"; endif; ?> >
                            <?Php
//                            echo $this->Html->link(
//                                'Tutors',
//                                array('controller' => 'tutors', 'action' => 'index', 'prefix' => "admin", '_full' => true),
//                                array('escape' => false)
                           // );
                            ?>
                        </li>-->
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Tutor
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                           
                                <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Active Tutors',
                                    array('controller' => 'tutors', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Tutor Training',
                                        array('controller' => 'trainings', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Recruitment',
                                    array('controller' => 'trainings', 'action' => 'recruitment', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                 <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'WWC',
                                    array('controller' => 'tutors', 'action' => 'wwc', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                                 <li>
                                   <?Php
                                    echo $this->Html->link(
                                    'Tutor Onboarding',
                                    array('controller' => 'tutors', 'action' => 'tutorOnboarding', 'prefix' => "admin", '_full' => true),
                                    array('escape' => false)
                                    );
                                    ?>
                                 </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Admin
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu multi-level" role="menu">
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'General',
                                        array('controller' => 'trainings', 'action' => 'general', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Process Payroll',
                                        array('controller' => 'payrolls', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                     <?php
                                    echo $this->Html->link(
                                        'Payroll History',
                                        array('controller' => 'payrolls', 'action' => 'report', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Overdue Reports',
                                        array('controller' => 'bookings', 'action' => 'tutor', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Consultation Reports',
                                        array('controller' => 'consultations', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li class="dropdown-submenu">
                                    <?php
                                    echo $this->Html->link(
                                        'KPI',
                                        array('target' => '#','_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'KPI Report',
                                                array('controller' => 'kpi', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Leading Indicators',
                                                array('controller' => 'kpi', 'action' => 'leadingIndicators', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Growth Metrics',
                                                array('controller' => 'kpi', 'action' => 'growthMetrics', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>
                                        <li>
                                            <?php
                                            echo $this->Html->link(
                                                'Key Performance Indicators',
                                                array('controller' => 'kpi', 'action' => 'keyPerformanceIndicators', 'prefix' => "admin", '_full' => true),
                                                array('escape' => false)
                                            );
                                            ?>
                                        </li>


                                    </ul>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Map',
                                        array('controller' => 'maps', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false, 'target' => '_blank')
                                    );
                                    ?>
                                </li>
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Message Board',
                                        array('controller' => 'messages', 'action' => 'board', 'prefix' => "admin", '_full' => true)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "bookings" || $this->name == "payrolls") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Bookings
                                <b class="caret"></b></a>
                                
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Create Booking',
                                        array('controller' => 'bookings', 'action' => 'create', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Rebookings',
                                        array('controller' => 'bookings', 'action' => 'rebook', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                               
                                <li> </li>
                                
<!--                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Payroll View',
                                        array('controller' => 'payrolls', 'action' => 'view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>-->
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'View Bookings',
                                        array('controller' => 'bookings', 'action' => 'view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Consultations',
                                        array('controller' => 'bookings', 'action' => 'consultations', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Client Bookings',
                                        array('controller' => 'bookings', 'action' => 'lessons', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Uninvoiced Bookings',
                                        array('controller' => 'bookings', 'action' => 'uninvoice_view', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                
                                <li></li>
<!--                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Calendar View',
                                        array('controller' => 'lessons', 'action' => 'calendar', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Table View',
                                        array('controller' => 'lessons', 'action' => 'table', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>-->
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Resources
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Study Skills',
                                        array('controller' => 'trainings', 'action' => 'studyskills', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Resources',
                                        array('controller' => 'resources', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Year Level Overviews and Curriculum',
                                        array('controller' => 'resources', 'action' => 'yearCumRes', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "DataReview" ) {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Data
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Minor Outcome',
                                        array('controller' => 'dataReview', 'action' => 'minor-outcome', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Questions',
                                        array('controller' => 'questions', 'action' => 'index', 'prefix' => "admin", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Feedback" && $this->request->params['action'] != "students"): echo "class='active'"; endif; ?> >
                            <a href="" class="btn1-floating btn1-large red" data-toggle="modal"
                               data-target="#report-support">
                                Feedback
                            </a>
                        </li>
                        <li>
                            <?php
                            echo $this->Html->link(
                                "Profile",
                                array('controller' => '../profile', 'action' => 'admin'),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="col-xs-12 col-sm-12 col-md-2 nav-user">
            <ul class="nav navbar-nav pull-right">
                        <li>
                            <?php
                            echo $this->Html->link(
                                "<i class='glyphicon glyphicon-log-out'></i>Logout",
                                array('controller' => '../users', 'action' => 'logout'),
                                array('escape' => false)
                            );
                            ?>
                        </li>
            </ul>
        </div>
    </div>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

</div>

<div class="modal fade" id="report-support" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php echo $this->Form->create(null, ['url' => ['controller' => '../support', 'action' => 'support']]);?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-flag"></i> Report / Support</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="token" value="<?= $this->request->session()->read("token") ?>"/>
                        <input type="hidden" name="page" value="<?= 'Admin - '.$this->name.'/'.$this->request->params['action'] ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input id="report-subject" type="text" name="subject" class="form-control" placeholder="Subject" required="">
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="report-message" class="form-control" name="message" rows="4" placeholder="Message..." required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="repsup-btn" type="submit" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<!-- Response Dialog -->
<div id="response-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <p>Copyright © 2017 • <a href="#">Tutor2You</a> | <a href="#">Tutoring Platform</a> • All Rights
        Reserved &reg;</p>
</footer>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.quicksearch.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>
<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('custom.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<?= $this->Html->script('general.js'); ?>
<script type="text/javascript">
    var trackcmp_email = '';
    var trackcmp = document.createElement("script");
    trackcmp.async = true;
    trackcmp.type = 'text/javascript';
    trackcmp.src = '//trackcmp.net/visit?actid=223206443&e='+encodeURIComponent(trackcmp_email)+'&r='+encodeURIComponent(document.referrer)+'&u='+encodeURIComponent(window.location.href);
    var trackcmp_s = document.getElementsByTagName("script");
    if (trackcmp_s.length) {
            trackcmp_s[0].parentNode.appendChild(trackcmp);
    } else {
            var trackcmp_h = document.getElementsByTagName("head");
            trackcmp_h.length && trackcmp_h[0].appendChild(trackcmp);
    }
</script>
</body>
</html>