<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="description" content="Tutoring Platform"/>
    <meta name="keywords" content="Online Assessment, Test Prep, Tutors, Review Center"/>
    <meta name="author" content="Tutor2You"/>
    <link rel="shortcut icon" href="/img/icon.png">
    <title><?php echo $this->name; ?></title>    
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('multi-select.css') ?>
    <?= $this->Html->css('jquery.timepicker.css') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->Html->script('jquery-3.1.1.min.js'); ?>
    <?= $this->Html->css('dataTables.bootstrap.css') ?>    
<style>
    .morecontent span {
    display: none;
}
.morelink {
    display: block;
}
</style>
</head>
<body>
<div <?php if ($this->name == "Profile" ) {echo "id='profile-container'"; } elseif ($this->name == "Resources" && $this->request->params['action'] == "index") {echo "style='width:100%';";} ?> class="container">
    <div class="row navbar-fixed-top" id="top-navigation">
        <header class="col-sm-12 col-md-10">
            <div class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: 4px')) ?>
                    </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php if($this->request->session()->read("access")){ ?>
                        <li <?php if ($this->name == "Dashboard") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Dashboard',
                                array('controller' => 'dashboard', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Clients"): echo "class='active'"; endif; ?> >
                            <?Php
                            echo $this->Html->link(
                                'Clients',
                                array('controller' => 'clients', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                        <li <?php if ($this->name == "Bookings"): echo "class='active'"; endif; ?> >
                                    <?php
                                    echo $this->Html->link(
                                        'Bookings',
                                        array('controller' => 'bookings', 'action' => 'table', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                        </li>
                        <li>
                            <?php
                            echo $this->Html->link(
                                'Message Board',
                                array('controller' => 'messages', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                       <!--  <li <?php if ($this->name == "Calendar"): echo "class='active'"; endif; ?> >
                                    <?php
                                    echo $this->Html->link(
                                        'Calendar',
                                        array('controller' => 'calendar', 'action' => 'lessons', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                        </li> -->
                        <li <?php if ($this->name == "Trainings") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Admin
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'General',
                                        array('controller' => 'trainings', 'action' => 'general', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Training',
                                        array('controller' => 'trainings', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Study Skills',
                                        array('controller' => 'trainings', 'action' => 'studyskills', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
                        <li <?php if ($this->name == "Resources") {
                            echo "class='active dropdown'";
                        } else {
                            echo "class='dropdown'";
                        } ?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Resources
                                <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Resources',
                                        array('controller' => 'resources', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                                <li>
                                    <?php
                                    echo $this->Html->link(
                                        'Year Level Overviews and Curriculum',
                                        array('controller' => 'resources', 'action' => 'yearCumRes', 'prefix' => "tutor", '_full' => true),
                                        array('escape' => false)
                                    );
                                    ?>
                                </li>
                            </ul>
                        </li>
<!--                        <li <?php if ($this->name == "SessionReport") {
                            echo "class='active'";
                        } ?> >
                            <?php
                            echo $this->Html->link(
                                'Session Report',
                                array('controller' => 'sessionReport', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                array('escape' => false)
                            );
                            ?>
                        </li>-->
                        <?php } if (!$this->request->session()->read("access")){ ?>
                        <li>
                            <?php echo $this->Html->link(
                                'Training',
                                array('controller' => 'trainings', 'action' => 'index', 'prefix' => "tutor", '_full' => true),
                                array('escape' => false)
                                );
                                ?>
                            </li>   
                            <?php } ?>                     
                        <li <?php if ($this->name == "Feedback" && $this->request->params['action'] != "students"): echo "class='active'"; endif; ?> >
                            <a href="" class="btn1-floating btn1-large red" data-toggle="modal"
                               data-target="#report-support">
                                Feedback
                            </a>
                        </li>
                        <li>
                            <?php
                            echo $this->Html->link(
                                "Profile",
                                array('controller' => '../profile', 'action' => 'tutor'),
                                array('escape' => false)
                            );
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="col-xs-12 col-sm-12 col-md-2 nav-user">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown">
                    <li>
                        <?php
                        echo $this->Html->link(
                            "<i class='glyphicon glyphicon-log-out'></i>Logout",
                            array('controller' => '../users', 'action' => 'logout'),
                            array('escape' => false)
                        );
                        ?>
                    </li>
                </li>
            </ul>
        </div>
    </div>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

</div>

<!-- Feedback -->
<div class="modal fade" id="report-support" tabindex="-1" role="dialog" aria-labelledby="reportLabel">
    <?php echo $this->Form->create(null, ['url' => ['controller' => '../support', 'action' => 'support']]);?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><i class="glyphicon glyphicon-flag"></i> Report / Support</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="token" value="<?= $this->request->session()->read("token") ?>"/>
                        <input type="hidden" name="page" value="<?= 'Tutor - '.$this->name.'/'.$this->request->params['action'] ?>"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input id="report-subject" type="text" name="subject" class="form-control" placeholder="Subject" required="">
                        <br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="report-message" class="form-control" name="message" rows="4" placeholder="Message..." required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<!-- Response Dialog -->
<div id="response-dialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <p>Copyright © 2017 • <a href="#">Tutor2You</a> | <a href="#">Tutoring Platform</a> • All Rights
        Reserved &reg;</p>
</footer>

<script>
    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?= $this->Html->script('bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.quicksearch.js'); ?>
<?= $this->Html->script('jquery.multi-select.js'); ?>

<?= $this->Html->script('jquery.dataTables.min.js'); ?>
<?= $this->Html->script('dataTables.bootstrap.min.js'); ?>
<?= $this->Html->script('jquery.timepicker.js'); ?>
<?= $this->Html->script('custom.js'); ?>
<?= $this->Html->script('jquery-ui.min.js'); ?>
<?= $this->Html->script('general.js'); ?>
</body>
</html>