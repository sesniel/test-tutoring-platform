<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Clients</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Clients</h4>
                                <p>These are the list of your Clients, you can search a student by typing any keywords in the input box at the top right corner of the table.</p>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="clientsTbl"
                                       class="table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Suburb</th>
                                        <th>Date Registered</th>
                                        <th>Availability</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clients as $client): ?>
                                        <tr>
                                            <td><?= $client->client->first_name.' '.$client->client->last_name ?></td>
                                            <td><?= $client->client->suburb ?></td>
                                            <td><?= date_format($client->client->created, 'Y/m/d') ?></td>
                                            <td><?php $avail = array();
                                                foreach ($client->client->availabilities as $availability) {
                                                    $avail[] = $availability->day->name;
                                                }
                                                echo implode(", ", $avail); ?></td>
                                            <td>
                                            <div class="btn-group">
                                                <?=
                                                $this->Form->button('Details', ['type' => 'button',
                                                    'class' => 'btn btn-sm btn-info viewClient', 'value' => $client->client->token]); ?>
                                            </div>
                                                
                                                    
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Students</h4>
                                <p>These are the list of your Students, you can search a student by typing any keywords in the input box at the top right corner of the table.</p>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="studentsTbl"
                                       class="table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Students</th>
                                        <th>Clients</th>
                                        <th>Year Level</th>
                                        <th>Subjects</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($leads as $lead): ?>
                                        <tr>
                                            <td><?= $lead->student->name ?></td>
                                            <td><?= $lead->client->first_name.' '.$lead->client->last_name ?></td>
                                            <td><?= $lead->student->year_level->name ?></td>
                                            <td>
                                                <?php $subj = array();
                                                    foreach ($lead->lead_subjects as $subjects) {
                                                        $subj[] = $subjects->subject->name;
                                                    }
                                                    echo implode(", ", $subj); ?>
                                            </td>
                                            <td><?= $lead->status ?></td>
                                            <td>
                                            <div class="btn-group">
                                                    <?=
                                                $this->Form->button('Assessments', ['type' => 'button',
                                                    'class' => 'btn btn-sm btn-warning assessment', 'value' => $lead->student_id]); ?>
                                            </div>
                                                
                                                    
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="client-view-dialogues"></div>
<div id="assessment-view"></div>
<?= $this->Html->script('tutor/clients.js'); ?>