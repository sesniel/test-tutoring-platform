<div id="assessment-details" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Assessments:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['controller' => 'assessment' ,'action' => 'allocate']]); ?>
            <div class="modal-body row center-table">
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Assessments:</h4>
                        <p><b>Warning:</b><em> <u>Cancelled Assessment</u> will be deleted.</em></p>
                    </div>
                    <div class="col-md-12">
                        <table id="assessmentTbl" class="responsive display table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <td>Assessment</td>
                                    <td>Completed</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($studentAssessmentDetails as $studDetails):  ?>
                                    <tr>
                                        <td><?= $studDetails->assessment_type->type ?></td>
                                        <td><?= $studDetails->status ?></td>
                                        <td>
                                            <?php
                                            if ($studDetails->status == "Done") {
                                                if ($studDetails->assessment_type_id > 2){
                                                    echo $this->Html->link(
                                                    "View",
                                                    array('controller' => 'report', 'action' => 'printReport', $studDetails->id),
                                                    ['class' => 'btn btn-info btn-sm', 'target' => '_blank', 'type' => 'button']
                                                    );
                                                } else {
                                                    echo $this->Html->link(
                                                    "View",
                                                    array('controller' => 'report', 'action' => 'printReportLearning', $studDetails->id),
                                                    ['class' => 'btn btn-info btn-sm', 'target' => '_blank', 'type' => 'button']
                                                    );
                                                }
                                            } else {
                                                echo $this->Html->link(
                                                    "Cancel Assessment",
                                                    array('controller' => 'assessment','action' => 'cancelAssessment', $studDetails->id),
                                                    ['class' => 'btn btn-sm btn-danger']
                                                    );
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <hr/>
                        <h4>Add Assessment:</h4>
                        <p class="p-notes">Select which assessments you would like the student to
                            complete
                            (hold ctrl to select more than 1)</p>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Student</th>
                                        <th>Year</th>
                                        <th>Subject</th>
                                        <th>Assessments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?= $studentDetails->name ?>
                                            <input type="hidden" name="student_id" value="<?= $studentDetails->id ?>" />
                                        </td>
                                        <td>
                                            <?Php
                                            if($studentDetails->year_level_id == "1"):
                                                echo "F";
                                            else:
                                                echo ($studentDetails->year_level_id - 1);
                                            endif;                                        
                                            ?>
                                        </td>
                                        <td>
                                            <?php foreach($studentSubjects as $subject): ?>
                                                <?= $subject ?><br/>
                                            <?php endforeach; ?>
                                        </td>
                                        <td class="col-md-3">
                                            <select id="assessmentsList" name="assessments[]" class="form-control lead-subjects" multiple="" row="10" required="">
                                                <?php                                    
                                                $assessmentTypeDetailsInfo = $assessmentTypeDetails->toArray();
                                                foreach($studentAssessmentDetails as $studentAssesstype):
                                                    foreach($assessmentTypeDetailsInfo as $key => $studentTypes):

                                                        if($studentAssesstype->assessment_type_id == $studentTypes->id && $studentAssesstype->status != 'Done'):
                                                            unset($assessmentTypeDetailsInfo[$key]);
                                                        endif;

                                                        endforeach;

                                                        endforeach;
                                                        ?>

                                                        <?php foreach($assessmentTypeDetailsInfo as $assessDetails): ?>
                                                            <option value="<?= $assessDetails->id ?>" ><?= $assessDetails->type ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <button type="submit" class="btn btn-success pull-right">Add Assessments</button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        <?= $this->Form->end(); ?>   
                    </div>
                </div>
            </div>      
        </div>
        <?= $this->Html->script('tutor/assessment_view.js'); ?>