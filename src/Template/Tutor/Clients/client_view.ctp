<div id="client-view" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Client Profile</h4>
        </div>
        <div class="modal-body row">
            <?php foreach ($client as $cli) : ?>
                <div id="client-info" class="row">
                    <div class="row col-md-12 center-content">
                        <div class="col-md-12">
                            <h4>Contact Info:</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row center-view">
                        <div class="col-md-6">
                            <div class="col-md-5"><h5><strong>Name:</strong></h5></div>
                            <div class="col-md-7"><h5><?= $cli->first_name . ' ' . $cli->last_name ?></h5></div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-3"><h5><strong>Email:</strong></h5></div>
                            <div class="col-md-9"><h5><?= $cli->email ?></h5></div>
                        </div>
                    </div>
                    <div class="row center-view">
                        <div class="col-md-6">
                            <div class="col-md-5"><h5><strong>Mobile:</strong></h5></div>
                            <div class="col-md-7"><h5><?= ($cli->mobile)? $cli->mobile: '- - -'; ?></h5></div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-3"><h5><strong>Phone:</strong></h5></div>
                            <div class="col-md-9"><h5><?= ($cli->phone)? $cli->phone: '- - -'; ?></h5></div>
                        </div>
                    </div>
                    <div class="row center-view">
                        <div class="col-md-12">
                            <div class="col-md-2"><h5><strong>Address:</strong></h5></div>
                            <div class="col-md-10"><h5><?= $cli->street_number.' '.$cli->street_name.', '.$cli->suburb ?></h5></div>
                        </div>
                    </div>
                </div>
                <div id="student-details" class="row">
                    <?= $this->Form->create(null, ['url' => ['action' => 'studentUpdate']]); ?>
                    <div class="row col-md-12 center-content">
                        <div class="col-md-10">
                            <h4>Students Details:</h4>
                            <hr>
                        </div>
                        <div class="col-md-2">
                            <button type="button" id="edit-students-details"
                                    class="btn btn-primary btn-xs pull-right top-buffer">Edit
                                Student/s
                            </button>
                            <button type="submit" id="update-students-details"
                                    class="btn btn-xs btn-success hidden pull-right">Update
                                Student/s Details
                            </button>
                        </div>
                    </div>
                    <div class="row center-indent">
                        <?php $ctr = 1;
                        foreach ($students as $lead) : ?>
                            <div class="col-md-6 current-students">
                                <div class="col-md-12 top-buffer">
                                    <h5><strong>Student Detail <?= "#" . $ctr ?></strong></h5>
                                </div>
                                <div class="col-md-12 top-buffer">
                                    <div class="col-md-6">
                                        <label for="student">Student First Name:</label>
                                        <?= $this->Form->hidden('student_id[]', ['value' => $lead->student->id]) ?>
                                        <input type='text' value='<?= $lead->student->name ?>' name="student_name[]"
                                               class='form-control student-name'
                                               required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="year-level">Year Level:</label>
                                        <select id="year-level" name='year_level[]'
                                                class="form-control std-year-lvl" required>
                                            <option value='<?= $lead->student->year_level->id ?>'
                                                    selected='selected'><?= $lead->student->year_level->name ?></option>
                                            <?php foreach ($yrlvls as $yrlvl) : ?>
                                                <option
                                                    value='<?= $yrlvl->id ?>'><?= $yrlvl->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 top-buffer">
                                    <label for="student">Subjects:</label>
                                    <select name='subjects<?= $ctr ?>[]'
                                            class="form-control student-subjects" disabled
                                            multiple required>
                                        <?php foreach ($lead->student->student_subjects as $selectedSubj) : ?>
                                            <option
                                                value='<?= $selectedSubj->subject->id ?>'
                                                selected><?= $selectedSubj->subject->name ?></option>
                                        <?php endforeach;
                                        foreach ($subjs as $subj) : ?>
                                            <option
                                                value='<?= $subj->id ?>'><?= $subj->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <?php $ctr++;
                        endforeach;
                        echo $this->Form->end(); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
<?= $this->Html->script('tutor/clients-view.js'); ?>