<?php $colorWheelCollection = array('#004d80', '#d33c44',  '#cc33ff','#75b1a9', '#ec8b43', '#499867', '#ff531a', '#3a430a', '#b300b3', '#ff3333', '#476b6b'); 
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Lessons</strong>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row">
                    <div class="col-md-12">
                        <div id='lessons'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        
        $('#lessons').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            defaultDate: '<?= date('Y-m-d') ?>',
            navLinks: true, // can click day/week names to navigate viewsAIzaSyB-mpFCptgGr5mnYkZjhFGoQZU3CZfOTMI
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            events: [
            <?php foreach ($lessons as $key => $les): $sample = $les->date.' '.$les->time; $color = $colorWheelCollection[array_rand($colorWheelCollection)]; ?>
                <?php foreach ($les->lesson_students as $student): ?>
                {   
                    title: '<?= $student->program_student->student->name ?> = <?= ($student->assessment_type == null)? 'General': $student->assessment_type->type; ?>',
                    start: '<?= date('Y-m-d H:i:s', strtotime($sample)) ?>',
                    backgroundColor: '<?= $color ?>'
                },
                <?php endforeach; ?>
            <?php endforeach; ?>
            ]
        });
        
    });
</script>