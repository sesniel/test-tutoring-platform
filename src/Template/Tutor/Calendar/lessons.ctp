<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.css' />
<script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js'></script>

<style>
    #calendar{
        padding: 15px;
    }
</style>

<div class="widget-box">
<div class="row">
    <div class="col-sm-6">
        <div id='calendar'></div>
    </div>
    <div class="col-sm-6"></div>
</div>
</div>



<script>
    
$(document).ready(function() {
    
  $('#calendar').fullCalendar({
        events: [
            {
                title  : 'event1',
                start  : '2017-08-13 08:00:00',
                end    : '2017-08-13 09:00:00',
                allDay : false ,
                url: 'http://google.com/'
            },
            {
                title  : 'event2',
                start  : '2017-08-13 14:00:00',
                end    : '2017-08-13 16:30:00',
                allDay : false ,
                url: 'http://google.com/'
            },
            {
                title  : 'event3',
                start  : '2017-08-14 15:00:00',
                end    : '2017-08-14 17:00:00',
                allDay : false, 
                url: 'http://google.com/'
            }
        ],
        theme: true,
        editable: false,
        defaultView: 'agendaWeek',
        header: {
          left: 'prev,next today',
          center: 'title'
        },
        eventClick: function(event) {
            if (event.url) {
                window.open(event.url, "_blank");
                return false;
            }
        }
  })
});

</script>