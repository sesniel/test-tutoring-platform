
<div class="widget-box">
    <div class="widget-title">
        <div class="col-md-6">
            <h2 class="headerDisplay">
                <strong>Messages View</strong>
            </h2>
        </div>
        <div class='col-md-6'>
        </div>
    </div>
    <div class="widget-container">
        <div class='row'>
        <div class="col-md-12 content">
            <?php if(!empty($messageTagDetails->toArray())): ?>
            <table class="table table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Created By</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Created</th>
                    <th>Read</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($messageTagDetails as $messageTag): ?>
                    <tr>
                        <td><?= $messageTag->message->user->first_name . " " . $messageTag->message->user->last_name ?></td>
                        <td><?php echo $messageTag->message->subject; ?></td>
                        <td><span class="more"><?php echo $messageTag->message->message; ?></span></td>
                        <td><?php echo date("d/m/Y", strtotime($messageTag->created)); ?></td>
                        <td><?php echo date("d/m/Y", strtotime($messageTag->modified)); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?Php else: ?>
                No message available.
            <?php endif; ?>
<?php // debug($messageTagDetails->toArray()); ?>
            
        </div>
        </div>    
    </div>
</div>
    
