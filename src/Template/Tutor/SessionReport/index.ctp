<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Report Details:</strong></h2>
        </div>
        <hr class="customHr">
        <div class="widget-container table-responsive add-new-client-content">
            <?= $this->Form->create(null, ['url' => ['action' => 'sessionReport']]); ?>
            <div class="row diffMarg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Please fill up the form below: <strong>Note: The report will be sent to the Client as well.</strong></p>
                            <hr>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>My Details:</h4>
                                    <p>If you wish to update your details you may change it in the profile menu.</p>
                                    <hr>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="form-group col-md-11 center-table">
                                    <label for="full_name">Full Name:</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name"
                                    value="<?= $details->first_name . ' ' . $details->last_name ?>"
                                    readonly="readonly">
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="form-group col-md-11 center-table">
                                    <label for="tutor-email">Email:</label>
                                    <input type="text" class="form-control" id="tutor-email" name="tutor-email"
                                    value="<?= $details->email; ?>"
                                    readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Client Details:</h4>
                                    <p>Please enter the full name of the adult responsible for the signing off
                                        session.</p>
                                    <hr>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="form-group col-md-11 center-table">
                                    <label for="client_fullname">Full Name:</label>
                                    <input id="client_fullname" list="clientFullname" name="client" class="form-control"
                                           required>
                                    <datalist id='clientFullname'>
                                        <?php foreach ($leads as $lead) : ?>
                                            <option
                                                data-value='<?= $lead->client->email ?>'
                                                value='<?= $lead->client->first_name . ' ' . $lead->client->last_name ?>'/>
                                        <?php endforeach; ?>
                                    </datalist>
                                    <div id="client-data"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Session Details:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="row top-buffer">
                                <div class="form-group col-md-11 center-table">
                                    <label for="session-length">Session Length:</label>
                                    <input type="text" class="form-control" id="session-length" name="session-length"
                                           maxlength="7" required>
                                </div>
                            </div>
                            <div class="row top-buffer">
                                <div class="form-group col-md-11 center-table">
                                    <label for="session-date">Session Date:</label>
                                    <div class='input-group'>
                                        <input id='session-date' class='form-control' type='text' name='session-date'
                                               required>
                                        <span class='input-group-addon dateIcon'>
                                            <i class='glyphicon  glyphicon-calendar'></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="row">
                                <div class="form-group col-md-11 center-table">
                                    <p>Please describe what you covered in the session any worksheets, subject
                                        assignments etc. <strong>(character limit: 1,970)</strong></p>
                                    <label for="session-note">Session Notes:</label>
                                    <textarea class="form-control" rows="2" id="session-note" name="session-note" maxlength="1970"
                                              required
                                    ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-success center-block">Submit Session</button>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
<?= $this->Html->script('tutor/session-report.js'); ?>

