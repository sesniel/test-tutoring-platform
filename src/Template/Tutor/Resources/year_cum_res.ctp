<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Student Worksheets</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg center-table">
                    <div class="row top-buffer">
                        <div class="col-md-12">
                            <p>These are the worksheets grouped by year level, overview and Naplan.</p>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 1</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Overview</strong></h5>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/1/Overview/Curriculum.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i>
                                                Curriculum</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/1/Overview/English Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> English
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/1/Overview/History Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> History
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/1/Overview/Maths Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Maths
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/1/Overview/Science Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Science
                                                Portfolio</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 2</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Overview</strong></h5>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/2/Overview/Curriculum.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i>
                                                Curriculum</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/2/Overview/English Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> English
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/2/Overview/History Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> History
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/2/Overview/Maths Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Maths
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/2/Overview/Science Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Science
                                                Portfolio</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 3</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Overview</strong></h5>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/3/Overview/Curriculum.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i>
                                                Curriculum</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/3/Overview/English Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> English
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/3/Overview/History Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> History
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/3/Overview/Maths Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Maths
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/3/Overview/Science Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Science
                                                Portfolio</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 4</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Overview</strong></h5>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/4/Overview/Curriculum.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i>
                                                Curriculum</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/4/Overview/English Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> English
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/4/Overview/History Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> History
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/4/Overview/Science Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Science
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/4/Overview/Maths Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Maths
                                                Portfolio</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 5</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <div class="panel-group" id="y5">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           data-parent="#y5" href="#naplan"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Naplan</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="naplan" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Keeping animals in cages persuasive stimulus A.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Keeping animals in cages persuasive stimulus A</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Literacy Persuasive writing task Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Literacy Persuasive writing task Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Mobile phones at school persuasive stimulus B.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Mobile phones at school persuasive stimulus B</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Naplan Writing marking guide for teachers.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Naplan Writing marking guide for teachers</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Persuasive Writing Marking Guide.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Persuasive Writing Marking Guide</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 3 set 10.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 3 set 10</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 Language Conventions.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 Language Conventions</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 Literacy Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 Literacy Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 Literacy Preparation.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 Literacy Preparation</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 Literacy Questions.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 Literacy Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 marking info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 marking info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 1.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 2.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 3.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 4.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 5.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 6.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 6</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 7.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 7</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 8.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 8</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 9.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 9</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Naplan/Year 5 set 10.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 5 set 10</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           data-parent="#y5" href="#overview"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Overview</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="overview" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Overview/Curriculum.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Curriculum</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Overview/English Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                English
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Overview/History Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                History
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Overview/Maths Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i> Maths
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/5/Overview/Science Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Science
                                                                Portfolio</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 6</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Overview</strong></h5>
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/6/Overview/Curriculum.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i>
                                                Curriculum</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/6/Overview/English Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> English
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/6/Overview/History Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> History
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/6/Overview/Maths Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Maths
                                                Portfolio</a>
                                        </li>
                                        <li>
                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/6/Overview/Science Portfolio.pdf' ?>"
                                               target='_blank'><i class="glyphicon glyphicon-chevron-right"></i> Science
                                                Portfolio</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 7</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <div class="panel-group" id="y7">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           data-parent="#y7" href="#naplan7"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Naplan</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="naplan7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Keeping animals in cages persuasive stimulus A.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Keeping animals in cages persuasive stimulus A</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Literacy Persuasive writing task Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Literacy Persuasive writing task Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Mobile phones at school persuasive stimulus B.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Mobile phones at school persuasive stimulus B</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Naplan Writing marking guide for teachers.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Naplan Writing marking guide for teachers</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Persuasive Writing Marking Guide.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Persuasive Writing Marking Guide</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 Language Conventions.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 Language Conventions</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 Literacy Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 Literacy Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 Literacy Preparation.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 Literacy Preparation</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 Literacy Questions.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 Literacy Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 marking info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 marking info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 1.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 2.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 3.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 4.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 5.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 7</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 6.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 6</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 7.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 7</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 8.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 8</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 9.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 9</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Naplan/Year 7 set 10.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 7 set 10</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           data-parent="#y7" href="#overview7"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Overview</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="overview7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Overview/Curriculum.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Curriculum</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Overview/English Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                English
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Overview/History Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                History
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Overview/Maths Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i> Maths
                                                                Portfolio</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/7/Overview/Science Portfolio.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Science
                                                                Portfolio</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 8</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Coming soon...</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 9</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <div class="panel-group" id="y9">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           data-parent="#y9" href="#naplan9"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Naplan</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="naplan9" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Keeping animals in cages persuasive stimulus A.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Keeping animals in cages persuasive stimulus A</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Literacy Persuasive writing task Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Literacy Persuasive writing task Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Mobile phones at school persuasive stimulus B.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Mobile phones at school persuasive stimulus B</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Naplan Writing marking guide for teachers.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Naplan Writing marking guide for teachers</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Persuasive Writing Marking Guide.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Persuasive Writing Marking Guide</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 Literacy Info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 Literacy Info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 Literacy Preparation.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 Literacy Preparation</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 Literacy Questions.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 Literacy Questions</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 marking info.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 marking info</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 1.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 2.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 3.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 4.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 5.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 6.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 6</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 7.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 7</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 8.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 8</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 9.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 9</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/9/Naplan/Year 9 set 10.pdf' ?>"
                                                               target='_blank'><i
                                                                    class="glyphicon glyphicon-chevron-right"></i>
                                                                Year 9 set 10</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 10</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Coming soon...</strong></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 11</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Coming soon...</strong></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Year 12</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Coming soon...</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-3 top-buffer">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="text-center">Misc. Resources</h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <h5><strong>Coming soon...</strong></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('tutor/resources.js'); ?>