<div class="well">
    
    <?= $this->Form->create(null, ['url' => ['action' => 'allocate']]); ?>
              
        <div class="modal-body row center-table">
            <div class="row col-md-12">
                <div class="modal-header">
                    <h4 class="modal-title">Lead Details</h4>
                    <input type="hidden" name='lead_id' value='<?= $leadId ?>' />
                </div>
            </div>
            <div class="row col-md-12 center-block">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Contact Name:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->first_name . " " . $studentDetails->client->last_name ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Email:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->email ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Phone:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->phone ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <h5><strong>Mobile:</strong></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><?= $studentDetails->client->mobile ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Address:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->street_number . " " . $studentDetails->client->street_name  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Suburb:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->suburb  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>Postcode:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->postcode  ?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <h5><strong>City:</strong></h5>
                            </div>
                            <div class="col-md-8">
                                <h5><?= $studentDetails->client->city  ?></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-md-12 top-buffer">
                <div class="col-md-12">
                    <h4>Allocate Student Assessments:</h4>
                    <p class="p-notes">Select which assessments you would like the student to
                        complete
                        (hold ctrl to select more than 1)</p>
                </div>
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <td>Student</td>
                            <td>Year</td>
                            <td>Subject</td>
                            <td>Assessments</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?= $studentDetails->name ?>
                                <input type="hidden" name="student_id" value="<?= $studentDetails->id ?>" />
                            </td>
                            <td>
                                <?Php
                                    if($studentDetails->year_level_id == "1"):
                                        echo "F";
                                    else:
                                        echo ($studentDetails->year_level_id - 1);
                                    endif;                                        
                                ?>
                            </td>
                            <td>
                                <?php foreach($studentSubjects as $subject): ?>
                                    <?= $subject ?><br/>
                                <?php endforeach; ?>
                            </td>
                            <td class="col-md-3">
                                <select name="assessments[]" class="form-control lead-subjects" multiple="" row="10" required="">
                                    <?php foreach($assessmentTypeDetails as $assessDetails): ?>
                                        <option value="<?= $assessDetails->id ?>" ><?= $assessDetails->type ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
        <button type="submit" class="btn btn-default pull-right">Confirm Assessments</button>
            </div>
        </div>
    </div>
<?= $this->Form->end(); ?>         
</div>
<?php // debug($studentDetails->toArray()); ?>