<div id="client-view" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Student/s Info</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12 ">
                    <div class="center-content">
                        <table id="studentTbl" class="responsive display table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Year</th>
                                    <th>Subject/s</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($leads as $lead): ?>
                                    <tr>
                                        <td><?= $lead->student->name ?></td>
                                        <td><?= $lead->student->year_level->name ?></td>
                                        <td> 
                                            <?php
                                                $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                            echo implode(", ", $subj); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>