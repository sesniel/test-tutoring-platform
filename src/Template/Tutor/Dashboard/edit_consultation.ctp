<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Edit Consultation</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <?= $this->Form->create(null, ['url' => ['controller' => 'dashboard' ,'action' => 'updateConsultation']]); ?>
                        <div class="row center-content">
                            <div class="col-md-3">
                                <h4>Consultation Date and Time:</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control" id="conDate" type="text" name="conDate" value="<?= date_format($consultation->consultation_session_date, 'm/d/Y'); ?>" required><span class="input-group-addon conDateIcon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control" id="conTime" type="text" name="conTime" value="<?= date_format($consultation->consultation_session_date, 'G:ia'); ?>" required><span class="input-group-addon conTimeIcon"><i class="glyphicon glyphicon-time"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
                            </div>
                            <?= $this->Form->hidden('consultation_id', ['value' => $consultation->id]) ?>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="studentDetailsTable" class="responsive display table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong>Student</strong></td>
                                            <td><strong>Year</strong></td>
                                            <td><strong>Subjects</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($leads as $key=>$lead): ?>
                                        <tr>
                                            <td><?= $lead->student->name ?></td>
                                            <td><?= $lead->student->year_level->name ?></td>
                                            <td><?php $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                                echo implode(", ", $subj); ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="width: 100%;">Update Consultation</button>
                            </div>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('tutor/book-consultation1.js'); ?>