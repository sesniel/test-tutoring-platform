<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Book Consultation</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <?php
//                            echo $this->Form->create(null, ['url' => ['controller' => 'assessment' ,'action' => 'bookConsultation'] 
//                            ],['escape' => false, 'class' => "form-control formSubmit"]);
                            ?>
                        <form method="post" accept-charset="utf-8" action="/tutor/assessment/book-consultation" id="formSub">
                        <div class="row center-content">
                            <div class="col-md-3">
                                <h4>Consultation Date and Time:</h4>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control" id="conDate" type="text" name="conDate" required><span class="input-group-addon conDateIcon"><i class="glyphicon glyphicon-calendar"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group"><input class="form-control" id="conTime" type="text" name="conTime" required><span class="input-group-addon conTimeIcon"><i class="glyphicon glyphicon-time"></i></span></div>
                            </div>
                            <div class="col-md-3">
                                <i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
                            </div>
                            <?= $this->Form->hidden('consultation_id', ['value' => $consultation_id]) ?>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td><strong>Student</strong></td>
                                            <td><strong>Year</strong></td>
                                            <td><strong>Subjects</strong></td>
                                            <td><strong>Assessments</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($leads as $key=>$lead): ?>
                                        <?= $this->Form->hidden('lead_id[]', ['value' => $lead->id]) ?>
                                        <?= $this->Form->hidden('student_id[]', ['value' => $lead->student_id]) ?>
                                        <tr>
                                            <td><?= $lead->student->name ?></td>
                                            <td><?= $lead->student->year_level->name ?></td>
                                            <td style="width: 350px;"><?php $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                                echo implode(", ", $subj); ?>
                                            </td>
                                            <td>
                                                <select id="assessments<?= $key ?>" name='assessments<?= $key ?>[]' class="form-control assessmentSubject" multiple>
                                                    <?php foreach ($assessments as $assmnt) : ?>
                                                        <option id="opt"
                                                        value='<?= $assmnt->id ?>'><?= $assmnt->type ?>
                                                        </option> 
                                                    <?php endforeach; ?>
                                                    
                                                </select>
                                                <script>
                                                    $(document).ready(function () {
                                                    $('#assessments<?= $key ?>').multiSelect();
                                                    $('.ms-container').css("width","100%");
                                                    
                                                    });
                                                
                                                </script>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" id="book" class="btn btn-success" data-target="#myModal" data-toggle="modal" style="width: 100%;">Book Consultation</button>
                               
                                     <!--   
                                    <div id="myModal11111" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Save without assessments being allocated</h4>
                                                    Are you sure you want to book in the consultation without allocating any assessments?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <input type="submit" class="btn btn-primary" name="Save changes" />
                                                </div>
                                        </div>                                        
                                       
                                       </div>
                                    </div>
                                    -->
                            </div>
                        </div>
                        
                        <div id="modal-container"></div>
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><i class="glyphicon glyphicon-info-sign"></i> Save without assessments being allocated </h4>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row center-content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p class="text-center">Are you sure you want to book in the consultation without allocating any assessments?</p>
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                            <button type="submit" class="btn btn-success" id="book1">Yes</button>
                                        </div>
                                        <?= $this->Form->end(); ?>
                                    </div>
                                </div>
                            </div> 
                    </div>
            </div>
        </div>
</div>
     
<script>
    
    $(document).ready( function () {
        $('#book').click('submit',function(event){
        
            if($('.assessmentSubject option[value]:selected').text() === '')
                event.preventDefault();
            else  $('#book').removeAttr('data-target');
        });
      
     });
     

</script>   
<?= $this->Html->script('tutor/book-consultation1.js'); ?>