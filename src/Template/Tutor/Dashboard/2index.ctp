<?php $consultationStatus = array( 'Pending Consultation', 'Completed Consultation'); ?>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.css' />
<script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js'></script>

<style>
    #calendar{
        padding: 15px;
    }
</style>

<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Dashboard</strong></h2>
        </div>
        <hr class="customHr">
        <div class="row">
            
            <?php //if(!empty($leads->consultations)): ?>
            <div class="col-sm-6">
                
                <div id='calendar'></div>
            </div>
            <?php //else: ?>
           <!-- <div class="col-sm-12">
                
                <div id='calendar'></div>
            </div> -->
            <?php //endif; ?>
            
           
            <div class="col-sm-6">

            <div class="widget-container table-responsive">
               
                <div class="row">
                    <div class="row">
                        <?php if(!empty($leads)): ?>
                        <?php foreach ($leads as $lead):
                            if ($lead->consultation->status == 'New'):
                                echo $lead->client->first_name . " ";
                            endif;
                            
                        endforeach; ?>
                        
                        <div class="col-md-12">
                        <h4>New Clients</h4>
                        
                        <p>Click details to view the student/s details or book consultation to allocate assessments and schedule the consultation date and time.</p>
                        <hr>
                    </div>
                        <div class="col-md-12">
                            
                            <table id="newStdTbl"
                            class="responsive display table table-hover table-bordered consultationTable">
                            <thead>
                                <tr>
                                    <th>Client</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                
                                <?php foreach ($leads as $newLeads): ?>
                                 <?php //if ($newLeads->consultation->status == 'New'): ?>
                                    
                                    
                                <tr>

                                    <td class="text-center" id="leads"><?= $newLeads->client->first_name . ' ' . $newLeads->client->last_name ?></td>
                                    <td><?= $newLeads->client->street_number . ' ' . $newLeads->client->street_name . ', ' . $newLeads->client->suburb ?></td>
                                    <td><?= $newLeads->client->email ?></td>
                                    <td><?php
                                        if ($newLeads->client->mobile) :
                                            echo  $newLeads->client->mobile.' ';
                                            if ($newLeads->client->phone) :
                                                echo '| '.$newLeads->client->phone;
                                            endif;
                                        elseif ($newLeads->client->phone):
                                            echo  $newLeads->client->phone;
                                        else:
                                            echo " - - - ";
                                        endif;
                                    ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php
                                            echo $this->Form->button(
                                                    'Details', ['type' => 'button',
                                                'class' => 'btn btn-sm btn-info viewClient', 'value' => $newLeads->consultation_id]);
                                            echo $this->Html->link(
                                                    "Book Consultation", array('controller' => 'dashboard', 'action' => 'bookConsultation', $newLeads->consultation_id), ['class' => 'btn btn-sm btn-success', 'target' => '_blank']
                                            );
                                            ?>
                                        </div>
                                    </td>
                                    </tr>


                            <?php if ($hours >= 24): ?>
                                    <div id="myModal" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Reminder <span class="glyphicon glyphicon-info-sign"></span></h4>
                                                </div>

                                                <div class="modal-body">
                                                    <p>It looks like you were allocated a new client, <strong> <?php echo $newLeads->client->first_name . " " . $newLeads->client->last_name; ?> </strong> and a consultation date hasn't been booked in. Please book your consultation with the client and allocate any relevant assessments as soon as possible. <br><br>
                                                    Alternatively, please phone the office on 1300 4200 79 if there are any difficulties.
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <script>
                               $(document).ready(function(){
                                 $('#myModal').modal('show');

                                });
                            </script>
                                   <?php endif; ?>
                                   <?php //endif; ?> 
                               
                                <?php endforeach; ?>
                                       


                                </tbody>
                            </table>
                                
                            <?php endif; ?>
                        </div>
                        
                    </div>
                </div>
               
                <div class="row">
                    
                    
                    <?php if(!empty($consultations)): ?>
                    <div class="col-md-9">
                        <h4>Consultations</h4>
                        <p>Click <u>"Details"</u> to <u><em>see</em></u> Student/s details and their assessments.</p>
                        <p>Please print the <strong><a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Consultation Run Sheet.pdf' ?>" target="_blank">Consultation Run Sheet</a></strong> and take to your initial session along with the student's results reports.</p>
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <?php
                        echo $this->Form->create(null, [
                            'url' => ['action' => 'index'],
                            'method' => 'post'
                        ]);
                        ?>
                        Status: 
                        <select name="status" class="form-control" onchange="this.form.submit()" >
                            <?php
                            foreach ($consultationStatus as $stat):
                                ?>
                                <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="row center-content">
                        <div class="col-md-12">
                            <table class="responsive display table table-hover table-bordered consultationTable">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Consultation Date</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($consultations as $key => $consultation): 
                                        ?>
                                            
                                        <tr>
                                            <td><?= $consultation->leads[0]->client->first_name . ' ' . $consultation->leads[0]->client->last_name ?></td>
                                            <td><?php echo date('Y/m/d h:i A', strtotime($consultation->consultation_session_date)); ?></td>
                                            <td><?= $consultation->status ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <?php

                                                    echo $this->Form->button(
                                                            'Details', ['type' => 'button', 'class' => 'btn btn-sm btn-info consultationDetail', 'value' => $consultation->id]);
                                                    if($consultation->status == "Pending Consultation"){
                                                    echo $this->Html->link(
                                                            "Edit", array('controller' => 'dashboard', 'action' => 'editConsultation', $consultation->id), ['class' => 'btn btn-sm btn-success', 'target' => '_blank']);
                                                    }
                                                    echo $this->Form->button(
                                                            'Submit Report', ['type' => 'button', 'class' => 'btn btn-sm btn-warning consultationView', 'value' => $consultation->id]);
                                                    ?>
                                                </div>

                                                <!-- Consultation Notification -->

                            <?php 

                                foreach ($consultation->leads as $key => $lead): 


                                $conDate = date("Y/m/d H:i:s", strtotime($consultation->consultation_session_date));
                                $currentDate = date("Y/m/d H:i:s");

                                $seconds1 = strtotime($currentDate) - strtotime($conDate);
                                $hours1 = $seconds1 / 60 /  60;

                                    if ($hours1 >= 24): ?>
                                    <div id="myModal1" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title pull-left">Reminder <span class="glyphicon glyphicon-info-sign"></span></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Reminder: Records indicate you had a consultation booked recently for <strong> <?php echo $lead->client->first_name . " " . $lead->client->last_name . ".";  ?> </strong> Please complete your consultation report via your dashboard as soon as possible.
                                                    </p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                            <script>
                                $(document).ready(function(){
                                   $('#myModal1').modal('show');
                                });
                            </script>

                            <?php endforeach; ?>
                                
                             <?php endforeach; ?>
                                            </td>
                                        </tr>
                                        <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <?php if(!empty($programDetails)): ?>
                    <div class="col-md-9">
                        <h4>Overdue Reports</h4>
                        <hr>
                    </div>
                    <div class="col-md-3">
                        <?php
                        echo $this->Form->create(null, [
                            'url' => ['action' => 'index'],
                            'method' => 'post'
                        ]);
                        ?>
                      <!--  Status: 
                        <select name="status" class="form-control" onchange="this.form.submit()" >
                            <?php
                            foreach ($consultationStatus as $stat):
                                ?>
                                <option <?php $sel = ($status === $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                            <?php endforeach; ?>
                        </select> -->
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="row center-content">
                        <div class="col-md-12">
                            <table class="responsive display table table-hover table-bordered consultationTable">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Length</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($programDetails as $key => $program): ?>
                                        <?php foreach ($program->lessons as $lessons):?>
                                        <tr>
                                            <td><?= $program->client->first_name . ' ' . $program->client->last_name ?></td>
                                            <td><?php echo date('Y/m/d', strtotime($lessons->date)); ?></td>
                                            <td><?php echo date('h:i A', strtotime($lessons->time));?> </td>
                                            <td><?= $lessons->length ?></td>
                                            <td><?php
                                                if($lessons->status == "Tutor"):
                                                    echo "Pending";
                                                elseif($lessons->status == "Done"):
                                                    echo "Completed";
                                                else:
                                                    echo $lessons->status;
                                                endif;
                                            ?></td>
                                            <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModalReports">Add session report</button>
                                            </td>
                                        </tr>
                                <?php endforeach; ?>
                             <?php endforeach; ?>
                                </tbody>
                            </table>
                            
                            
                       <!--     <input type="hidden" name="lessonId" value="<?= $lessons->id ?>" /> -->
                            <div class="row center-indent">
                                <div id="myModalReports" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title text-center">Add New Report</h4>
                                                        Please add your lesson notes in the field below, please ensure you cover major topics such as any specific issues e.g. Homework status from last week, homework for next week. These notes will be visible to both parents, tutors and admin.<br><br>
                                                        <strong><i class="glyphicon glyphicon-info-sign"></i>   NOTE: DO NOT SUBMIT SESSION REPORT IF LESSON WAS CANCELLED </strong>
                                                    </div>
                                                    <?php echo $key . " " . $lessons->id; ?>
                                                    <!-- -->
                                                    <div class="modal-body">  
                                                     <div class="row">   
                                                        <div class="col-md-4">
                                                            <h5><strong>Date</strong></h5>
                                                             <input type="hidden" name="lesson_id[]" value="<?= $lessons->id ?>" />
                                                                <div class="input-group"><input class="form-control lessonDate1" id="lessonDate1<?= $key ?>" type="text" value="<?php echo date('d/m/Y', strtotime($lessons->date))  ?>" name="lessonDate" required="">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Time</strong></h5>
                                                                <div class="input-group"><input class="form-control lessonTime" id="lessonTime<?= $key ?>"
                                                                type="text" name="lessonTime" value="<?php echo date('h:i A', strtotime($lessons->time))  ?>" required=""><span
                                                                class="input-group-addon"><i
                                                                class="glyphicon glyphicon-time"></i></span></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Length</strong></h5>
                                                            <h5><?= $lessons->length ?></h5>
                                                        </div> 
                                                     </div>
                                                    </div> 

                                                    <!-- -->
                                                    <div class="modal-body">
                                                        <textarea rows="5" class="form-control" name="note" placeholder="Report here" required="" ></textarea>

                                                    </div>
                                                    <div class='modal-body'>
                                                        <input type="checkbox" id="bookingVariation" class="enable_textarea pull-right" name="box" style='position:relative; top:-15px; '> <span class='pull-right' style='position:relative; top:-15px; left:-10px;' >Request Booking Variation </span>

                                                            <textarea rows="5" class="form-control show_textarea" name="reason" placeholder="Place your reason here."  ></textarea>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input type="submit" class="btn btn-primary" name="submitted"/>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                                
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                    <?php endif; ?>    
               
                </div>
            </div>

        </div>
        </div>
    </div>
</div>
        
        <div id="client-view-dialogues"></div>
	<div id="consulation-edit-details"></div>
        <div id="assessment-details-dialogue"></div>
        <div id="consultation-dialogue"></div>
        
        
        <script>
                        
        $(document).ready(function(){
            
            $('#calendar').fullCalendar({
                    events: [
                        
                        <?Php 
                            foreach($calendarInfo as $calendar): 
                                $minute = $calendar->length * 60;
                                $hours = "+" . $minute . " minutes";
                            ?>
        
                        {
                            title  : '<?= $calendar->program->client->first_name . " " . $calendar->program->client->last_name ?>',
                            start  : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($calendar->time)) ?>',
                            end    : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($hours, strtotime($calendar->time))); ?>',
                            allDay : false ,
                            url: '/tutor/bookings/view/' + <?php echo $calendar->program->id; ?>
                        },
                                
                        <?php endforeach; ?>
                        
                        
                    ],
                    minTime: "08:00:00",
                    maxTime: "21:00:00",
                    theme: true,
                    editable: false,
                    defaultView: 'agendaWeek',
                    header: {
                      left: 'prev,next today',
                      center: 'title'
                    },
                    eventClick: function(event) {
                        if (event.url) {
                            window.open(event.url, "_blank");
                            return false;
                        }
                    }
            });

        
            var hash = window.location.hash;

            switch (hash) {
                case '#follow':
                    $('#response-dialog').modal('show');
                    $('#response-dialog .modal-title').text('Info');
                    $('#response-dialog .modal-body p').text('Records indicate you had a consultation booked recently for <?php //echo $newLeads->client->first_name; ?>. Please complete your consultation report via your dashboard as soon as possible.');
                    break;
            }
        });
        $(document).ready(function () {
                
                $('#formDelete').submit(function(e){
                        e.preventDefault();
                        $('#dialog').dialog('open');
                    });
                    $('#dialog').dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                            "Confirm": function() {
                                $('#formDelete').submit();  
                            },
                            "Cancel": function() {
                                $(this).dialog("close");
                            }
                    }
                }); 

//                $('#bookingTable').dataTable({"autoWidth": false});
//                $('#lessonTable').dataTable({"autoWidth": false});
//                $('#newLeadsTable').dataTable({"autoWidth": false});
//                $('[data-toggle="popover"]').popover(); 
                
                $('input.enable_textarea').change(function(){
                    if ($(this).is(':checked')){ 
                        $(this).parent().children('textarea.show_textarea').attr('required', "true").show();    
                        alert("Reminder: Variation requests are for when the length of sessions are not in accordance with the booking, do you wish to proceed?");
                    }
                    else{ 
                        $(this).parent().children('textarea.show_textarea').removeAttr('required').hide(); 
                    }
                }).change();                

            });
            
            $("#textUpdate").keydown(function(){
                var value = $(this).val();
                if (value && value.length > 0) {
                    // Exist text in your input
                    $("#updateButton").show();
                } else {
                    $("#updateButton").hide();
                }
            });
            
            function toggler(divId) {
                $("#" + divId).toggle();
            }
            
            function variation(){
                confirm("Either OK or Cancel.");
            }
        </script>
<?= $this->Html->script('tutor/dashboard3.js'); ?>
<?= $this->Html->script('jquery.cookie.js'); ?>