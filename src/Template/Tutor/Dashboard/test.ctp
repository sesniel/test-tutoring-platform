<?php $consultationStatus = array( 'Pending Consultation', 'Completed Consultation'); ?>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.css' />
<script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js'></script>

<style>
    #calendar{
        padding: 15px;
    }
     .lessonDate1{z-index:1151 !important;}
</style>
<div class="col-md-4">
                                                            <h5><strong>Time</strong></h5>
                                                                <div class="input-group"><input class="form-control lessonTime"
                                                                type="text" name="lessonTime" value="" required=""><span
                                                                class="input-group-addon"><i
                                                                class="glyphicon glyphicon-time"></i></span></div>
                                                        </div>
<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Add session report</button>
<button class="lessonTime">Calendar</button>
<?php foreach ($programDetails as $key => $programs): ?>
<?php foreach ($programs->lessons as $lesson): ?>
                                                    <div id="myModal" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Add New Report</h4>
                                                        Please add your lesson notes in the field below, please ensure you cover major topics such as any specific issues e.g. Homework status from last week, homework for next week. These notes will be visible to both parents, tutors and admin.<br><br>
                                                        <strong><i class="glyphicon glyphicon-info-sign"></i>   NOTE: DO NOT SUBMIT SESSION REPORT IF LESSON WAS CANCELLED </strong>
                                                    </div>
                                                    
                                                    <!-- -->
                                                    <div class="modal-body">  
                                                     <div class="row">   
                                                        <div class="col-md-4">
                                                            <h5><strong>Date</strong></h5>
                                                             <input type="hidden" name="lesson_id[]" value="<?= $lesson->id ?>" />
                                                                <div class="input-group"><input class="form-control lessonDate1" id="lessonDate1" type="text" value="<?php echo date('d/m/Y', strtotime($lesson->date))  ?>" name="lessonDate" required="">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Time</strong></h5>
                                                                <div class="input-group"><input class="form-control lessonTime" id="lessonTime"
                                                                type="text" name="lessonTime" value="<?php echo date('h:i A', strtotime($lesson->time))  ?>" required=""><span
                                                                class="input-group-addon"><i
                                                                class="glyphicon glyphicon-time"></i></span></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Length</strong></h5>
                                                            <h5><?= $lesson->length ?></h5>
                                                        </div> 
                                                     </div>
                                                    </div>

                                                    <!-- -->
                                                    <div class="modal-body">
                                                        <textarea rows="5" class="form-control" name="note" placeholder="Report here" required="" ></textarea>

                                                    </div>
                                                    <div class='modal-body'>
                                                        <input type="checkbox" id="bookingVariation" class="enable_textarea pull-right" name="box" style='position:relative; top:-15px; '> <span class='pull-right' style='position:relative; top:-15px; left:-10px;' >Request Booking Variation </span>

                                                            <textarea rows="5" class="form-control show_textarea" name="reason" placeholder="Place your reason here."  ></textarea>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input type="submit" class="btn btn-primary" name="submitted"/>
                                                    </div>
                                                </div>
                                            </div>
                                                    </div>

<?php endforeach; ?>
<?php endforeach; ?>
        <script>
        $(document).ready(function(){
            $('.lessonDate1').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy'
            
        });
        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        })
        });
        
        </script>
        <div id="client-view-dialogues"></div>
	<div id="consulation-edit-details"></div>
        <div id="assessment-details-dialogue"></div>
        <div id="consultation-dialogue"></div>
        
        
        <script>
                        
        $(document).ready(function(){
            
            $('#calendar').fullCalendar({
                    events: [
                        
                        <?Php 
                            foreach($calendarInfo as $calendar): 
                                $minute = $calendar->length * 60;
                                $hours = "+" . $minute . " minutes";
                            ?>
        
                        {
                            title  : '<?= $calendar->program->client->first_name . " " . $calendar->program->client->last_name ?>',
                            start  : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($calendar->time)) ?>',
                            end    : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($hours, strtotime($calendar->time))); ?>',
                            allDay : false ,
                            url: '/tutor/bookings/view/' + <?php echo $calendar->program->id; ?>
                        },
                                
                        <?php endforeach; ?>
                        
                        
                    ],
                    minTime: "08:00:00",
                    maxTime: "21:00:00",
                    theme: true,
                    editable: false,
                    defaultView: 'agendaWeek',
                    header: {
                      left: 'prev,next today',
                      center: 'title'
                    },
                    eventClick: function(event) {
                        if (event.url) {
                            window.open(event.url, "_blank");
                            return false;
                        }
                    }
            });

        
            var hash = window.location.hash;

            switch (hash) {
                case '#follow':
                    $('#response-dialog').modal('show');
                    $('#response-dialog .modal-title').text('Info');
                    $('#response-dialog .modal-body p').text('Records indicate you had a consultation booked recently for <?php //echo $newLeads->client->first_name; ?>. Please complete your consultation report via your dashboard as soon as possible.');
                    break;
            }
        });
        
        $(document).ready(function(){
//            $('#myModal').modal('show');
             $('#myModal1').modal('show');
             
        });
        
        
        $(document).ready(function () {
                
                $('#formDelete').submit(function(e){
                        e.preventDefault();
                        $('#dialog').dialog('open');
                    });
                    $('#dialog').dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                            "Confirm": function() {
                                $('#formDelete').submit();  
                            },
                            "Cancel": function() {
                                $(this).dialog("close");
                            }
                    }
                }); 

//                $('#bookingTable').dataTable({"autoWidth": false});
//                $('#lessonTable').dataTable({"autoWidth": false});
//                $('#newLeadsTable').dataTable({"autoWidth": false});
//                $('[data-toggle="popover"]').popover(); 
                
                $('input.enable_textarea').change(function(){
                    if ($(this).is(':checked')){ 
                        $(this).parent().children('textarea.show_textarea').attr('required', "true").show();    
                        alert("Reminder: Variation requests are for when the length of sessions are not in accordance with the booking, do you wish to proceed?");
                    }
                    else{ 
                        $(this).parent().children('textarea.show_textarea').removeAttr('required').hide(); 
                    }
                }).change();                

            });
            
            

            
        });
            
            $("#textUpdate").keydown(function(){
                var value = $(this).val();
                if (value && value.length > 0) {
                    // Exist text in your input
                    $("#updateButton").show();
                } else {
                    $("#updateButton").hide();
                }
            });
            
            function toggler(divId) {
                $("#" + divId).toggle();
            }
            
            function variation(){
                confirm("Either OK or Cancel.");
            }
        </script>
<?= $this->Html->script('tutor/dashboard3.js'); ?>
<?= $this->Html->script('jquery.cookie.js'); ?>