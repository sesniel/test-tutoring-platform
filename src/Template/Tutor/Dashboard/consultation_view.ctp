<div id="consultation" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Consultation:</h4>
            </div>
            <?= $this->Form->create(null, ['url' => ['action' => 'leadAnswers']]); ?>
            <div class="modal-body row center-table">
               <!-- <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Initial Consultation:</h4>
                        <p>Please print the <strong><a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Consultation Run Sheet.pdf' ?>" target="_blank">Consultation Run Sheet</a></strong> and take to your initial session along with the student's results reports.</p>
                        <hr>
                    </div>
                </div>  -->
                <div class="row col-md-12 center-content">
                    <div class="col-md-6">
                        <h5><strong>Student Notes:</strong></h5>
                    </div>
                    
                    <?php if(!empty($leadQuestions)): ?>
                    <?php if (!empty($answer)): ?>
                    <div class="col-md-6 top-buffer">
                            <button type="button" id="edit-notes" class="btn btn-sm btn-success pull-right" >Edit</button>
                    </div>
                    <div class="col-md-6 top-buffer">
                            <button type="submit" id="update-notes" class="btn btn-sm hidden btn-success pull-right" >Update</button>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
                <?php foreach ($leadQuestions as $key => $question): ?>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <p><?= ($key+1).'. '.$question->question ?></p>
                        </div>
                    </div>
                    <div class="row col-md-12 top-buffer">
                        <div class="col-md-12">
                            <?php if ($question->answer) { ?>
                                <textarea name="<?= $question->id ?>" class="form-control" cols="3" rows="3" disabled><?= $question->answer ?></textarea>
                            <?php } else { ?>
                                <textarea name="<?= $question->id ?>" class="form-control" cols="3" rows="3" required></textarea>
                            <?php } ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               <?php if (!$question->answer) { ?>
                    <button type="submit" class="btn btn-success">Submit</button>
                <?php } ?>             
            </div>
            <?= $this->Form->end(); ?>         
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {
        $('#edit-notes').click(function() {
            $(this).addClass('hidden');
            $('#update-notes').removeClass('hidden');
            $('textarea').prop('disabled', false);
        });
    });
</script>    