<?php $consultationStatus = array( 'Pending Consultation', 'Completed Consultation'); ?>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.css' />
<script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.6.0/fullcalendar.min.js'></script>

<style>
    #calendar{
        padding: 15px;
    }
    
    .lessonDate1{z-index:1151 !important;}
    .lessonTime{z-index:1151 !important;}
</style>

<div class="col-sm-12 col-md-12">
    <div class="widget-box">
        <div class="widget-title">
            <h2><strong>Dashboard</strong></h2>
        </div>
        <hr class="customHr">
        
        <div class="row">
            <div class="col-sm-6">
                <div id='calendar'></div>
            </div>
            <div class="col-sm-6">
                <div class="widget-container table-responsive">
                <?php if(!empty($messageTagDetails->toArray())): ?>
                    <div class='row'>
                    <div class="col-md-12 content">
                        <table class="table table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Created By</th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Created</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($messageTagDetails as $messageTag): ?>
                                <tr>
                                    <td><?= $messageTag->message->user->first_name . " " . $messageTag->message->user->last_name ?></td>
                                    <td><?php echo $messageTag->message->subject; ?></td>
                                    <td><span class="more"><?php echo $messageTag->message->message; ?></span></td>
                                    <td><?php echo date("d/m/Y", strtotime($messageTag->created)); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    </div>  

                <?php endif; ?>
            <?php // debug($messageTagDetails->toArray()); ?>

                    <div class="row" style="overflow-x: hidden;" >
                        <?php if(!empty($leads)):?>
                                <?php foreach($leads as $lead):?>
                                    <?php if ($lead->consultation->status == 'New'): ?>
                        <div class="row">
                                <div class="col-md-12">
                                        <h4>New Clients</h4>
                                        <p>Click details to view the student/s details or book consultation to allocate assessments and schedule the consultation date and time.</p>
                                        <hr>
                                        <table id="newStdTbl" class="responsive display table table-hover table-bordered consultationTable">
                                            <thead>
                                                <tr>
                                                    <th>Client</th>
                                                    <th>Address</th>
                                                    <th>Email</th>
                                                    <th>Contact</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($leads as $newLeads): ?>
                                                <?php if($newLeads->consultation->status == 'New'): ?>
                                                <tr>
                                                    <td class="text-center" id="leads"><?= $newLeads->client->first_name . ' ' . $newLeads->client->last_name ?></td>
                                                    <td><?= $newLeads->client->street_number . ' ' . $newLeads->client->street_name . ', ' . $newLeads->client->suburb ?></td>
                                                    <td><?= $newLeads->client->email ?></td>
                                                    <td><?php
                                                        if ($newLeads->client->mobile) :
                                                            echo  $newLeads->client->mobile.' ';
                                                            if ($newLeads->client->phone) :
                                                                echo '| '.$newLeads->client->phone;
                                                            endif;
                                                        elseif ($newLeads->client->phone):
                                                            echo  $newLeads->client->phone;
                                                        else:
                                                            echo " - - - ";
                                                        endif;
                                                    ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php
                                                            echo $this->Form->button(
                                                                    'Details', ['type' => 'button',
                                                                'class' => 'btn btn-sm btn-info viewClient', 'value' => $newLeads->consultation_id]);
                                                            echo $this->Html->link(
                                                                    "Book Consultation", array('controller' => 'dashboard', 'action' => 'bookConsultation', $newLeads->consultation_id), ['class' => 'btn btn-sm btn-success', 'target' => '_blank']
                                                            );
                                                            ?>
                                                        </div>
                                                    </td>
                                                    <?php endif; ?>
                                            <?php endforeach; ?>
                                                </tr> 
                                            </tbody>
                                        </table>
                                        <?php if($hours >= 24): ?>
                                        <div id="myModal" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h4 class="modal-title">Reminder <span class="glyphicon glyphicon-info-sign"></span></h4>
                                                    </div>

                                                    <div class="modal-body">
                                                        <p>It looks like you were allocated a new client, <strong> <?php echo $newLeads->client->first_name . " " . $newLeads->client->last_name; ?> </strong> and a consultation date hasn't been booked in. Please book your consultation with the client and allocate any relevant assessments as soon as possible. <br><br>
                                                        Alternatively, please phone the office on 1300 4200 79 if there are any difficulties.
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                    
                                    <?php foreach($consultations as $consult): ?>
                                        <?php if(!empty($consult)): ?>
                                    <div class="col-md-12">
                                        <div class="col-sm-9">
                                            
                                            <h4>Consultations</h4>
                                            <p>Click <u>"Details"</u> to <u><em>see</em></u> Student/s details and their assessments.</p>
                                            <p>Please print the <strong><a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Consultation Run Sheet.pdf' ?>" target="_blank">Consultation Run Sheet</a></strong> and take to your initial session along with the student's results reports.</p>
                                            <hr>
                                        </div>
                                        <div class="row center-content">
                                            <div class="col-md-12">
                                                
                                                <table class="responsive display table table-hover table-bordered consultationTable">
                                                    <thead>
                                                        <tr>
                                                            <th>Client</th>
                                                            <th>Consultation Date</th>
                                                            <th>Status</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($consultations as $key => $consultation): ?>
                                                        <tr>
                                                            <td><?= $consultation->leads[0]->client->first_name . ' ' . $consultation->leads[0]->client->last_name ?></td>
                                                            <td><?php echo date('Y/m/d h:i A', strtotime($consultation->consultation_session_date)); ?></td>
                                                            <td><?= $consultation->status ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <?php

                                                                    echo $this->Form->button(
                                                                            'Details', ['type' => 'button', 'class' => 'btn btn-sm btn-info consultationDetail', 'value' => $consultation->id]);
                                                                    if($consultation->status == "Pending Consultation"){
                                                                    echo $this->Html->link(
                                                                            "Edit", array('controller' => 'dashboard', 'action' => 'editConsultation', $consultation->id), ['class' => 'btn btn-sm btn-success', 'target' => '_blank']);
                                                                    }
                                                                    echo $this->Form->button(
                                                                            'Submit Report', ['type' => 'button', 'class' => 'btn btn-sm btn-warning consultationView', 'value' => $consultation->id]);
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php foreach ($consultation->leads as $key => $lead):
                                                            $conDate = date("Y/m/d H:i:s", strtotime($consultation->consultation_session_date));
                                                            $currentDate = date("Y/m/d H:i:s");
                                                            $seconds1 = strtotime($currentDate) - strtotime($conDate);
                                                            $hours1 = $seconds1 / 60 /  60;
                                                            
                                                            if ($hours1 >= 24):
                                                            
                                                        ?>
                                                        <div id="myModal1" class="modal fade">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h4 class="modal-title pull-left">Reminder <span class="glyphicon glyphicon-info-sign"></span></h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <p>Reminder: Records indicate you had a consultation booked recently for <strong> <?php echo $lead->client->first_name . " " . $lead->client->last_name . ".";  ?> </strong> Please complete your consultation report via your dashboard as soon as possible.
                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                            <?php endif; ?>
                                                        <?php endforeach;  ?>
                                                    <?php endforeach; ?>   
                                                    </tbody>
                                                </table>
                                            </div>
                                           </div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        
                                       
                                        
                                            <?php foreach($programDetails as $progDet): ?>
                                            <?php if(!empty($progDet->lessons)): ?>
                                            <div class="col-md-12">
                                                <h4>Overdue Reports</h4>
                                                <hr>
                                            </div>
                                         
                                            <div class="row center-content">
                                                <div class="col-md-12">
                                                    <table class="responsive display table table-hover table-bordered consultationTable">
                                                        <thead>
                                                            <tr>
                                                                <th>Client</th>
                                                                <th>Date</th>
                                                                <th>Time</th>
                                                                <th>Length</th>
                                                                <th>Status</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                            <?php foreach ($programDetails as $key => $program): ?>
                                                                <?php foreach ($program->lessons as $lessons): if ($lessons->status == "Scheduled"):?>
                                                            
                                                            <tr>
                                                                <td><?= $program->client->first_name . ' ' . $program->client->last_name ?></td>
                                                                <td><?php echo date('Y/m/d', strtotime($lessons->date)); ?></td>
                                                                <td><?php echo date('h:i A', strtotime($lessons->time));?> </td>
                                                                <td><?= $lessons->length ?></td>
                                                                <td><?php
                                                                    if($lessons->status == "Tutor"):
                                                                        echo "Pending";
                                                                    elseif($lessons->status == "Done"):
                                                                        echo "Completed";
                                                                    else:
                                                                        echo $lessons->status;
                                                                    endif;
                                                                ?></td>
                                                                <td><button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModalReports<?= $lessons->id?>">Add session report</button>
                                                                 <?php echo $this->Html->link(
                                                                    "Reschedule ",
                                                                    array('controller' => 'bookings','action' => 'view', $program->id),
                                                                    ['class' => 'btn btn-success btn-xs', 'type' => 'button', 'target' => '_blank' ]
                                                                    ); ?>   
                                                                <div class="col-md-12 text-center top-buffer">
                                                                   <?php
                                                                    echo $this->Form->create(null, [
                                                                    'url' => ['action' => 'saveSeessionReport'],
                                                                    'method' => 'post',
                                                                    'name' => 'formDelete'
                                                                    ]);

                                                                    ?>
                                                                    <div id="myModalReports<?= $lessons->id?>" class="modal fade" role="dialog" aria-hidden="true" >
                                                                        <input type="hidden" name="lessonId" value="<?= $lessons->id ?>" />
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content text-center">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                    <h4 class="modal-title">Add New Report</h4>
                                                                                    Please add your lesson notes in the field below, please ensure you cover major topics such as any specific issues e.g. Homework status from last week, homework for next week. These notes will be visible to both parents, tutors and admin.<br><br>
                                                                                    <strong><i class="glyphicon glyphicon-info-sign"></i>   NOTE: DO NOT SUBMIT SESSION REPORT IF LESSON WAS CANCELLED </strong>
                                                                                    <div class="modal-body">
                                                                                        <?php //echo $lessons->id . " " . date('h:i A', strtotime($lessons->time)). " " . date('Y/m/d', strtotime($lessons->date)); ?>
                                                                                        <div class="row">   
                                                                                            <div class="col-md-4">
                                                                                                <h5><strong>Date</strong></h5>
                                                                                                 <input type="hidden" name="lesson_id[]" value="<?= $lessons->id ?>" />
                                                                                                    <div class="input-group"><input class="form-control lessonDate1"type="text" value="<?php echo date('d/m/Y', strtotime($lessons->date))  ?>" name="lessonDate" required="">
                                                                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                                                    </div>
                                                                                            </div>
                                                                                            <div class="col-md-4">
                                                                                            <h5><strong>Time</strong></h5>
                                                                                                <div class="input-group"><input class="form-control lessonTime" id="lessonTime<?= $key ?>"
                                                                                                type="text" name="lessonTime" value="<?php echo date('h:i A', strtotime($lessons->time))  ?>" required=""><span
                                                                                                class="input-group-addon"><i
                                                                                                class="glyphicon glyphicon-time"></i></span></div>
                                                                                            </div>
                                                                                            <div class="col-md-4">
                                                                                                <h5><strong>Length</strong></h5>
                                                                                                <h5><?= $lessons->length ?></h5>
                                                                                            </div> 
                                                                                        </div>
                                                                                        <hr>
                                                                                        <!-- -->
                                                                                        <div class="modal-body">
                                                                                            <textarea rows="5" cols="50" class="form-control" name="note" placeholder="Report here" required="" ></textarea>

                                                                                        </div>
                                                                                        <div class='modal-body'>
                                                                                            <input type="checkbox" id="bookingVariation" class="enable_textarea pull-right" name="box" style='position:relative; top:-15px; '> <span class='pull-right' style='position:relative; top:-15px; left:-10px;' >Request Booking Variation </span>
                                                                                                <textarea rows="5" cols="50"  class="form-control show_textarea" name="reason" placeholder="Place your reason here."  ></textarea>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                                            <input type="submit" class="btn btn-primary" name="submitted"/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                      <?php echo $this->Form->end(); ?>
                                                                </div>
                                                                </td>
                                                                
                                                            
                                                            </tr>
                                                                <?php endif; endforeach; ?>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                    
                                                    
                                                </div>
                                            </div>
                                        
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
        
        <div id="client-view-dialogues"></div>
	<div id="consulation-edit-details"></div>
        <div id="assessment-details-dialogue"></div>
        <div id="consultation-dialogue"></div>
       
        
        <script>
                        
        $(document).ready(function(){
            
            
    $( "#dialog-message" ).dialog({
        modal: true,
        resizable: false,
        minWidth: 600,
        title: 'Announcements',
        hide: 'scale',
        buttons: {
          Ok: function() {
            $( this ).dialog( "close" );
          }
        }
    });
    
            $( "#dialog-notification" ).dialog({
                modal: true,
                resizable: false,
                minWidth: 600,
                title: 'Availability',
                hide: 'scale',
                buttons: {
                    "Save": {  
                        text: 'Save', 
                        class: 'btn btn-primary', 
                        click: function () {
                            
                            var isGood = confirm('Are you sure you wish to confirm the following availability?');
                            if (isGood) {
                                
                            
                                    var availability = $('.update_availability').val();
                                    var hours = $('.update_hours').val();

                                    $.ajax({
                                    type: "POST",
                                    url: "/tutor/dashboard/update-availability/"+<?php echo $tutorInfo->id; ?>,
                                    data: {
                                        availability : availability,
                                        hours: hours
                                    },
                                    cache: false,
                                    success:  function(data){
                                        console.log("Successfull" + data);
                                    }
                                  });
                          
                                $( this ).dialog( "close" );
                                
                                
                            }
//                            console.log(availability + " --> " + hours);
                        }
                    }
                }
            });
            
            $('#calendar').fullCalendar({
                    events: [
                        
                        <?Php 
                            foreach($calendarInfo as $calendar): 
                                $minute = $calendar->length * 60;
                                $hours = "+" . $minute . " minutes";
                                $fname = $calendar->program->client->first_name;
                                $lname = $calendar->program->client->last_name;
                            ?>
        
                        {
                            title  : "<?= $calendar->program->client->first_name . " " . $calendar->program->client->last_name ?>",
                            start  : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($calendar->time)) ?>',
                            end    : '<?php echo date("Y-m-d", strtotime($calendar->date)) . " " . date("H:i:s", strtotime($hours, strtotime($calendar->time))); ?>',
                            <?php if ($calendar->program->status == "Pending"): ?>
                            color: '#FF4500',
                            <?php endif; ?>
                            allDay : false ,
                            url: '/tutor/bookings/view/' + <?php echo $calendar->program->id; ?>
                        },
                                
                        <?php endforeach; ?>
                        

                        <?Php 
                            foreach($calendarConsultation as $key => $cons):
                                if($cons->consultation->payroll_date == ""):
                                      
                            ?>
                                
                                    {
                                        title  : "<?= $cons->client->first_name . " " . $cons->client->last_name ?>\n Consultation",
                                        start  : '<?php echo date("Y-m-d H:i:s", strtotime($cons->consultation->consultation_session_date)) ?>',
                                        end    : '<?php echo date("Y-m-d H:i:s", strtotime('60 minutes', strtotime($cons->consultation->consultation_session_date))) ?>',
                                        allDay : false,
                                        backgroundColor: '#a92727',
                                        borderColor: '#fff'
                                    },            
                            
                        <?php endif; endforeach; ?>
                        
                    ],
                    minTime: "08:00:00",
                    maxTime: "21:00:00",
                    theme: true,
                    editable: false,
                    defaultView: 'agendaWeek',
                    header: {
                      left: 'prev,next today',
                      center: 'title'
                    },
                    eventClick: function(event) {
                        if (event.url) {
                            window.open(event.url, "_blank");
                            return false;
                        }
                    }
            });

        
            var hash = window.location.hash;

            switch (hash) {
                case '#follow':
                    $('#response-dialog').modal('show');
                    $('#response-dialog .modal-title').text('Info');
                    $('#response-dialog .modal-body p').text('Records indicate you had a consultation booked recently for <?php //echo $newLeads->client->first_name; ?>. Please complete your consultation report via your dashboard as soon as possible.');
                    break;
            }
        });
        
        $(document).ready(function(){
            $('#myModal').modal('show');
             $('#myModal1').modal('show');
             
        });
        
        $(document).ready(function() {
            $('.lessonDate1').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy'
            
            
        });
        
        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        });
        
        });
        
        
        $(document).ready(function () {
                
                $('#formDelete').submit(function(e){
                        e.preventDefault();
                        $('#dialog').dialog('open');
                    });
                    $('#dialog').dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                            "Confirm": function() {
                                $('#formDelete').submit();  
                            },
                            "Cancel": function() {
                                $(this).dialog("close");
                            }
                    }
                }); 

//                $('#bookingTable').dataTable({"autoWidth": false});
//                $('#lessonTable').dataTable({"autoWidth": false});
//                $('#newLeadsTable').dataTable({"autoWidth": false});
//                $('[data-toggle="popover"]').popover(); 
                
                $('input.enable_textarea').change(function(){
                    if ($(this).is(':checked')){ 
                        $(this).parent().children('textarea.show_textarea').attr('required', "true").show();    
                        alert("Reminder: Variation requests are for when the length of sessions are not in accordance with the booking, do you wish to proceed?");
                    }
                    else{ 
                        $(this).parent().children('textarea.show_textarea').removeAttr('required').hide(); 
                    }
                }).change();                

            });
            
        
            
            $("#textUpdate").keydown(function(){
                var value = $(this).val();
                if (value && value.length > 0) {
                    // Exist text in your input
                    $("#updateButton").show();
                } else {
                    $("#updateButton").hide();
                }
            });
            
            function toggler(divId) {
                $("#" + divId).toggle();
            }
            
            function variation(){
                confirm("Either OK or Cancel.");
            }
        </script>
        
<?php if(!empty($announcement->toArray())): ?>
<div id="dialog-message" title="Basic dialog">
    <?php 
        foreach($announcement as $shout): 
            
            echo "Created By: <label>".$shout->message->user->first_name. " " .$shout->message->user->last_name."</label>";
            echo "<h3>".$shout->message->subject."</h3>";
            echo "<p>".$shout->message->message."</p><hr/><br/>";
            
        endforeach;
    ?>
</div>
<?php endif; ?>
<?= $this->Html->script('tutor/dashboard3.js'); ?>
<?= $this->Html->script('jquery.cookie.js'); ?>

        
<?php 
    if($tutorInfo->available_notification != ""):
        $cDate = date('Ymd', strtotime($tutorInfo->available_notification));  
    else:
        $cDate = date('Ymd');  
    endif;
    if(date('Ymd') >= $cDate): 
?>       
        <div style="overflow-x: hidden;" id="dialog-notification" title="Basic dialog">
                <div class="row">
                        <div class="col-md-12">
                            It's been a little while since we checked in. Could you please review your availability below and update accordingly if your availability has changed recently.
                            <br/>&nbsp;
                        </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    Availability
                </div>
                <div class="col-md-6">
                    Hours
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <select class="form-control update_availability">
                        <option value="Yes" <?php if($tutorInfo->availability == "Yes"): echo "selected"; endif; ?>>Yes</option>
                        <option value="No"  <?php if($tutorInfo->availability == "No"): echo "selected"; endif; ?>>No</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <select class="form-control update_hours">
                        <option value="5+" <?php if($tutorInfo->hours == "5+"): echo "selected"; endif; ?>>5+</option>
                        <option value="4" <?php if($tutorInfo->hours == "4"): echo "selected"; endif; ?>>4</option>
                        <option value="3" <?php if($tutorInfo->hours == "3"): echo "selected"; endif; ?>>3</option>
                        <option value="2" <?php if($tutorInfo->hours == "2"): echo "selected"; endif; ?>>2</option>
                        <option value="1" <?php if($tutorInfo->hours == "1"): echo "selected"; endif; ?>>1</option>
                    </select>
                </div>
            </div>
        </div>
<?php endif; ?>
<?php // debug($tutorInfo); ?>