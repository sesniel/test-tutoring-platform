<?= $this->Html->css('lity.css') ?>
<?= $this->Html->script('lity.js'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Study Skills Program</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            Tutor2You strives to provide not only a top quality service to our clients but
                            also
                            to our tutors. In some cases tutors have never tutored before As such we have created a
                            series
                            of videos to assist with learning our procedures and the fundamentals of tutoring. As a
                            new
                            tutor with Tutor2You you must review these videos as part of your training.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                                <p>We believe that one of the single biggest reasons that students are being left
                                    behind
                                    at school is that they don’t have the necessary skills to learn efficiently and
                                    effectively. Our goal is just to ensure students have sound foundational
                                    knowledge
                                    but that they have the skills and strategies to take control of their study now
                                    and
                                    for the rest of their academic lives.</p>
                                <p>This Study Skills Program is intended to be used in conjunction with our
                                    proprietary
                                    Learning IQ Assessment. The program covers a series of topics and includes a
                                    training video and supplementary resources.</p>
                                <div class="row"><h5 class="hideme">Weekly Student Checklist</h5></div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-4">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#wstdcheck"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Weekly Student Checklist</a>
                                                </h5>
                                            </div>
                                            <div id="wstdcheck" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/The Successful Students Weekly Checklist Guide.pdf' ?>"
                                                                       target='_blank'>
                                                                        The Successful Student's Weekly Checklist Guide
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/The Successful Students Weekly Checklist.pdf' ?>"
                                                                       target='_blank'>
                                                                        The Successful Student's Weekly Checklist
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#goal-setting"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Goal Setting</a>
                                                </h5>
                                            </div>
                                            <div id="goal-setting" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Goal Setting cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Goal Setting - Cheat Sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Goal Setting Worksheet and Guide.pdf' ?>"
                                                                       target='_blank'>
                                                                        Goal Setting Worksheet &amp; Guide</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Goal Setting Worksheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Goal Setting Worksheet</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/K2PEtQ_VgBU"
                                                                       data-lity>
                                                                        Goal Setting
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#sustudyarea"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Setting Up a Study Area</a>
                                                </h5>
                                            </div>
                                            <div id="sustudyarea" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Study Area cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Study Area Cheat Sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Study Area Checklist.pdf' ?>"
                                                                       target='_blank'>
                                                                        Study Area Checklist
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/pznGNRZCw3M"
                                                                       data-lity>
                                                                        Study Area
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"><h5 class="hideme">Note Taking</h5></div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-4">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#note-taking"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Note Taking</a>
                                                </h5>
                                            </div>
                                            <div id="note-taking" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Note Taking Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Note Taking Cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Note Taking Checklist.pdf' ?>"
                                                                       target='_blank'>
                                                                        Note Taking Checklist
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Note Taking Templates.pdf' ?>"
                                                                       target='_blank'>
                                                                        Note Taking Templates
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/8Y9s3w0tvQU"
                                                                       data-lity>
                                                                        Note Taking
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#memafoc"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Memory &amp; Focus Techniques</a>
                                                </h5>
                                            </div>
                                            <div id="memafoc" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Memory and Focus Techniques cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Memory and Focus Techniques cheat sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Memory Palace Worksheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Memory Palace Worksheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Mnemonic Devices.pdf' ?>"
                                                                       target='_blank'>
                                                                        Mnemonic Devices</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/Ldx-mBhR0L4"
                                                                       data-lity>
                                                                        Memory and Focus Techniques
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#bshabs"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Building Study Habits</a>
                                                </h5>
                                            </div>
                                            <div id="bshabs" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Study Habits Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Study Habits Cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Study Habits Rating Scale.pdf' ?>"
                                                                       target='_blank'>
                                                                        Study Habits Rating Scale
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/tJ2H50-aaqw"
                                                                       data-lity>
                                                                        Study Habit
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"><h5 class="hideme">Time Management</h5></div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-4">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#time-magnmnt"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Time Management</a>
                                                </h5>
                                            </div>
                                            <div id="time-magnmnt" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Monthly Calendar Template.pdf' ?>"
                                                                       target='_blank'>
                                                                        Monthly Calendar Template
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Task Description and Priority Form.pdf' ?>"
                                                                       target='_blank'>
                                                                        Task Description and Priority Form
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Time Management Calculator.pdf' ?>"
                                                                       target='_blank'>
                                                                        Time Management Calculator
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Time Managment Plan cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Time Managment Plan cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/weekly-schedule.pdf' ?>"
                                                                       target='_blank'>
                                                                        weekly-schedule
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/6CS0xRZCsqw"
                                                                       data-lity>
                                                                        Time Management
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-danger">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#assmntprep"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Assignment Preparation</a>
                                                </h5>
                                            </div>
                                            <div id="assmntprep" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Computer marked- Cheat Sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Computer Marked Assignment - Cheat Sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/End of course- Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        End of Course Assignment - Cheat Sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Essay Preparation - Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Essay Preparation - Cheat Sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Oral assignments Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Oral Assignment - Cheat Sheet</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Oral Presentation Rubric.pdf' ?>"
                                                                       target='_blank'>
                                                                        Oral Presentation Rubric</a>
                                                                </li>
                                                                <li class="text-left">
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Report Tips -Cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Report Tips -Cheat sheet</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/IXXC0D30Sqc"
                                                                       data-lity>
                                                                        Assignment Preparation
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h5 class="text-center">
                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                       href="#exam-prep"><i
                                                            class="glyphicon glyphicon-chevron-down"></i>
                                                        Exam Preparation</a>
                                                </h5>
                                            </div>
                                            <div id="exam-prep" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <ul class="list-unstyled">
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Resources
                                                            <ul>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Essay Exam Tips.pdf' ?>"
                                                                       target='_blank'>
                                                                        Essay Exam Tips
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Exam Preparation cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Exam Preparation cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Multiple Choice Tips cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Multiple Choice Tips cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Open Book Tips cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Open Book Tips cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Oral exam tips cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Oral exam tips cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Problem Exam Tips cheat sheet.pdf' ?>"
                                                                       target='_blank'>
                                                                        Problem Exam Tips cheat sheet
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Studying for Exams Checklist.pdf' ?>"
                                                                       target='_blank'>
                                                                        Studying for Exams Checklist
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Test Preparation Checklist.pdf' ?>"
                                                                       target='_blank'>
                                                                        Test Preparation Checklist
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="text-left">
                                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                                            Training Video
                                                            <ul>
                                                                <li>
                                                                    <a href="https://youtu.be/7VIS4RaElIk"
                                                                       data-lity>
                                                                        Exam Preparation
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>