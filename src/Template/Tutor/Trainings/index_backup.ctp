
    <?= $this->Html->css('jquery-toastmessage/style.css'); ?>
    <?= $this->Html->script('jquery-toastmessage/script.js'); ?>
<?php
    if(!empty($moduleDetails)):
    $moduleStat = ($moduleDetails->status == "Pending") ? 4 : $moduleDetails->status ; 
    else:
        $moduleStat = "No Info";
    endif;
    if(!empty($moduleDetails)):
    if($moduleDetails->status == "Done"):
?>
<script>
    // $(".infoToast").toastmessage('showToast', {
    // text     : 'You have successfully finish your training',
    // sticky   : false,
    // stayTime : 10000, 
    // type     : 'success'
});
</script>

<?php
    endif;
    endif;
    ?>
<?php // debug($moduleDetails->toArray()); ?>

<div class="widget-box">
<div class="widget-title">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            
            <?php 
                if(!empty($moduleDetails) && $moduleDetails->status == "Pending"): 
                    echo "<div class='col-sm-12 col-md-12  bg-success' style='padding-bottom: 15px;'><h2>Your answers to modules 1 - 4 will now be reviewed and your regional manager will be in touch shortly, in order to progress your application.</h2></div> <br/>&nbsp;<br/>&nbsp;";
                elseif (!empty($moduleDetails) && $moduleDetails->status == "Done"):
                    echo "<div class='col-sm-12 col-md-12  bg-success' style='padding-bottom: 15px;'><h2>Congratulations you have completed the training, your regional manager will be in touch shortly in order to finalise your documentation.</h2></div> <br/>&nbsp;<br/>&nbsp;";
                else:
                    echo '<h2>Training</h2>';
                endif; ?>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <ul class="nav nav-tabs">
                    
                    <?php for($z = 1; $z <= 8; $z++): ?>

                        <?php if($moduleStat == "Done" || (!empty($moduleDetails))): ?>
                        
                            <li <?php $stat = (!empty($moduleDetails) && $moduleStat == $z) ? "active" : $info = ( $moduleStat == "Done" && $z == 8 ) ? "active" : "" ; ?> class="<?= $stat ?>">
                                <a href="#module<?= $z ?>" <?php if($moduleStat >= $z || $moduleStat == "Done"): ?> data-toggle="tab" <?php else: ?> style="background-color:grey; color:white;" <?php endif; ?>>
                                    <h5>Module <?= $z ?></h5>
                                </a>
                            </li>
                            
                        <?php endif; ?>
                    
                    <?php endfor; ?>
                    
                </ul>
            
                <div class="tab-content">
                    <?php for($y = 1; $y <= 8; $y++): ?>
                    <?php if($moduleStat == "Done" || (!empty($moduleDetails) && $moduleStat >= $y)):  ?>
                    <div <?php $stat = (!empty($moduleDetails) && $moduleStat == $y) ? "active" : $info = ( $moduleStat == "Done" && $y == 8 ) ? "active" : "" ; ?> class="tab-pane fade <?= $stat ?> in" id="module<?= $y ?>"> 
                    
                    <?php 

                    $checkPoints = array("Pending","Done");

                    if ( ($moduleStat <= 4) && ($y <= 3)  && ($moduleStat != $y) && !(in_array($moduleDetails->status, $checkPoints)) ) {  
                    
                           echo $this->Form->create(null, ['url' => ['action' => 'editModule',$moduleDetails->id], 'method' => 'post', 'class' => 'editForm']);

                            ?>
                            <div class="row top-buffer ">
                                <div class="col-md-12">
                                    <?php 
                                        echo $this->Form->button('Edit Answers',['type' => 'button', 'class' => 'btn btn-info top-buffer editAnswer', 'value' => '#module'.$y, 'style' => 'width:100% ;']);
                                     ?>
                                </div>
                            </div>

                            <div class="row top-buffer editControls hidesometin">
                                <div class="col-md-12">
                                    <div class="btn-group" style="width: 100%;">
                                        <?php 
                                        echo $this->Form->button('Update',['type' => 'submit', 'class' => 'btn btn-success top-buffer','style' => 'width:50% ;']);
                                        ?>
                                        <?php 
                                        echo $this->Form->button('Cancel',['type' => 'button', 'class' => 'btn btn-primary top-buffer cancelEdit', 'value' => '#module'.$y, 'style' => 'width:50% ;']);
                                        ?>
                                    </div>
                                </div>
                            </div>

                        <?php } elseif( ($moduleStat > 4) && ($y > 4)  && ($moduleStat != $y) && !(in_array($moduleDetails->status, $checkPoints))  ) {
                
                             echo $this->Form->create(null, ['url' => ['action' => 'editModule',$moduleDetails->id], 'method' => 'post', 'class' => 'editForm']); ?>

                             <div class="row top-buffer ">
                                <div class="col-md-12">
                                    <?php 
                                        echo $this->Form->button('Edit Answers',['type' => 'button', 'class' => 'btn btn-info top-buffer editAnswer', 'value' => '#module'.$y, 'style' => 'width:100% ;']);
                                     ?>
                                </div>
                            </div>

                            <div class="row top-buffer editControls hidesometin">
                                <div class="col-md-12">
                                    <div class="btn-group" style="width: 100%;">
                                        <?php 
                                        echo $this->Form->button('Update',['type' => 'submit', 'class' => 'btn btn-success top-buffer','style' => 'width:50% ;']);
                                        ?>
                                        <?php 
                                        echo $this->Form->button('Cancel',['type' => 'button', 'class' => 'btn btn-primary top-buffer cancelEdit', 'value' => '#module'.$y, 'style' => 'width:50% ;']);
                                        ?>
                                    </div>
                                </div>
                            </div>

                        <?php }else {
                            
                            echo $this->Form->create(null, ['url' => ['action' => 'savemodule',$moduleDetails->id], 'method' => 'post']);

                            } ?>


                             <?php $access = ($moduleStat != $y || $moduleDetails->status == "Pending" || $moduleStat == "Done") ? "readonly" : "" ;
                                
                            $x = 0;
                            foreach($moduleDetails->module_user_questions as $userQuestion): 
                                if($userQuestion->module_id == $y):
                                    if($x == 0): $x++;
                        ?>
                                        <div class="row">

                                            <div class="col-md-12">
                                                <h3>Overview: <?= $userQuestion->module->overview ?></h3>     
                                                <h4>Download PDF: <a href='//<?= $_SERVER['SERVER_NAME'] . '/webroot/web/viewer.html?file=training/' . $userQuestion->module->pdf ?>'
                               target='_blank' class='btn btn-xs btn-success'>View Module</a></h4>                           
                                            </div>
                                            <div class="col-md-12">
                                            </div>

                                        </div>
                                    <?php endif; ?>

                             
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                              <h3 class="panel-title"><?= $x ?>. <?= $userQuestion->questions ?></h3>
                                            </div>
                                            <div class="panel-body">
                                                <?php if($userQuestion->type == "statement"): ?>
                                                <textarea required="" name="answer[<?= $userQuestion->id ?>]" rows="5" class="form-control" <?= $access ?>><?= $userQuestion->user_answer ?></textarea>
                                                <?php 
                                                
                                                    else:
                                                        $choices = json_decode( $userQuestion->choices, true);
                                                ?>      
                                                <div class="btn-group" data-toggle="buttons">
                                                    <?php foreach($choices as $c): ?>
                                                        <label class="btn btn-primary <?php if($c == $userQuestion->user_answer): echo "active"; endif; ?> ">
                                                            <input required="" type="radio" name="answer[<?= $userQuestion->id ?>]" autocomplete="off" value="<?= $c ?>" > <?= $c ?>
                                                        </label>
                                                    <?php endforeach; ?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                        
                                    
                        <?php     $x++;       
                                endif; 
                            endforeach; 
                            
                            if(($moduleStat == $y && $moduleDetails->status != "Pending") &&  $moduleDetails->status != "Done"):
                        ?>
                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php if ($y == 4){  ?>
                                            <input type="submit" class="btn btn-primary pull-right" value="Save and Submit Modules 1-4" />
                                        <?php } elseif ($y == 8){ ?>
                                            <input type="submit" class="btn btn-primary pull-right" value="Save and Submit Modules 5-8" />
                                        <?php } else { ?>
                                            <input type="submit" class="btn btn-primary pull-right" value="Save and Proceed" />
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-12">
                                        &nbsp;
                                    </div>
                                </div>
                        
                        <?php endif; echo $this->Form->end(); ?>
                    </div>
                    <?php endif; ?>
                    <?php endfor; ?>
                </div>

            </div>
        </div>

    </div>
</div>
</div>
<script>
    $(document).ready(function(){
        $('.editAnswer').click(function () {
            var module = $(this).val();
           
            $(''+ module + ' textarea').attr("readonly", false);
            $(''+ module + ' .editControls').removeClass("hidesometin");
            $(this).addClass("hidesometin");
        });
        $('.cancelEdit').click(function () {
            var module = $(this).val();
            $(''+ module + ' textarea').attr("readonly", true);
            $(''+ module + ' .editAnswer').removeClass("hidesometin");
            $(''+ module + ' .editControls').addClass("hidesometin");
        });

        var hash = window.location.hash;

        switch (hash) {
            case '#updated':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').text('Updated!');
            $('#response-dialog .modal-body p').text('Training Answers updated!');
            break;

        }
    }); 
</script>
<?php 
//        debug($moduleDetails);
//        debug($moduleUserQuestionDetails);
?>