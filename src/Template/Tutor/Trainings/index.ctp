<?= $this->Html->css('jquery-toastmessage/style.css'); ?>
<?= $this->Html->script('jquery-toastmessage/script.js'); ?>
<?php

    
    if($moduleDetails->status == "New" || $moduleDetails->status == "Completed Orientation"):
        $disable = array('', 'active', 'active', '', '', '', '', '', '', '', '');
        $check = 2;
    elseif($moduleDetails->status == "Pending Contract Review & Expectations" || $moduleDetails->status == "Completed Contract Review & Expectations"):
        $disable = array('', 'active', 'active', 'active', 'active', '', '', '', '', '', '');
        $check = 4;
    elseif($moduleDetails->status == "Completed Platform Training" || $moduleDetails->status == "Pending Platform Training"):
        $disable = array('', 'active', 'active', 'active', 'active', 'active', 'active', '', '', '', '');
        $check = 6;
    elseif($moduleDetails->status == "Pending Tutor Training" || $moduleDetails->status == "Completed Training"):
        $disable = array('', 'active', 'active', 'active', 'active', 'active', 'active', 'active', 'active', 'active', 'active');
        $check = 10;
    endif;


?>
<style>
    .title {
        margin-top: 10px;
        font-size: 23px;
        font-style: italic;
    }
    .instruction {
        text-align: justify;
        text-align: center;
        font-size: 20px;
    }
    .btn-module {
        padding-left: 100px;
        padding-right: 100px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .submit {
        margin-bottom: 20px;
        margin-right: 50px;
        
    }
    .submit-btn {
        padding-left: 100px;
        padding-right: 100px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .inactive {
        font-size: 35px;
        font-style: italic;
        padding-top: 150px;
        padding-left: 40px;
    }
</style>

<div class="widget-box">
<div class="widget-title">
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <ul class="nav nav-tabs">                    
                    <?php for($z = 1; $z <= 10; $z++): ?>
                        
                            <li class="<?php if($z == $moduleDetails->position): echo "active"; endif; ?>">
                                <a href="#module<?= $z ?>"  data-toggle="tab"
                                    <?php if($disable[$z] != "active"):?> style="background-color:grey; color:white;" <?php endif; ?> >
                                    <h5>Module <?= $z ?></h5>
                                    
                                </a>
                               
                            </li>                            
                    
                    <?php endfor;?>
                            
                </ul> 
                
                 
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="tab-content">
                    
                    <?php for($y = 1; $y <= 10; $y++): ?>
                    <div class="tab-pane <?php if($y == $moduleDetails->position): echo "active"; endif; ?> fade in" id="module<?= $y ?>"> 
                    <?php echo $this->Form->create(null, ['url' => ['action' => 'moduleUpdate', $moduleDetails->id]]);  ?>
                        <?php 
                            
                            $displayDetails = '';
                            $displayHeader = ''; 
                            $displayInstruction = '';
                            foreach($moduleDetails->module_user_questions as $userQuestion):
//                                debug($userQuestion->toArray());exit;
                                
                                
                                if($userQuestion->module_id == $y):
                                    if($y <= $check):
                                        
                                        if($y == 1):
                                            $displayInstruction = '<div class="row"><div class="col-md-12 top-buffer text-center"><p class="title"><b>Instruction</b></p>
                                                <p class="text-center instruction">Read each module by clicking on the green <b>\'View Module\'</b> button below then answer the questions.<br>Once completed, submit your answers using the <b>\'submit\'</b> button at the bottom of the page.</p>
                                                </div></div>';
                                        endif;
                                        
                                        $displayHeader = '<div class="row"><div class="col-md-12 top-buffer"><h4>Overview: '.
                                        $userQuestion->module->overview.'</h4><h4 class="text-center module"><a href="/webroot/web/viewer.html?file=training/'.
                                        $userQuestion->module->pdf.'" target="_blank" class="btn btn-success btn-module">View Module</a><br/>&nbsp;<br/></h4><h4>Questions:</h4></div></div>';
                                        
                                        $displayDetails .= '<div class="col-md-12">';
                                        $displayDetails .= '<div class="panel-default"><div class="panel-heading"><h3 class="panel-title">'.$userQuestion->questions.'</h3></div></div><div class="panel panel-body">';
                                        if($userQuestion->type == "statement"):
                                            if($userQuestion->user_answer == ""):
                                                $tag = "";
                                            else:
                                                $tag = "disabled";
                                            endif;
                                           $displayDetails .= '<textarea required="" '.$tag.' name="answer['.$userQuestion->id.']" rows="5" class="form-control" >'.$userQuestion->user_answer.'</textarea>';
                                        elseif($userQuestion->type == "choices"):
                                            $choices = json_decode( $userQuestion->choices, true);
                                            $displayDetails .= '<div class="btn-group" data-toggle="buttons">&nbsp;<br/>';
                                            foreach($choices as $c):
                                                if($c == $userQuestion->user_answer): $slct = "active"; else: $slct = ""; endif;
                                                $displayDetails .= '<label class="btn btn-primary '.$slct.' ">';
                                                $displayDetails .= '<input required="" type="radio" name="answer['.$userQuestion->id.']" autocomplete="off" value="'.$c.'" > '.$c.'';
                                                $displayDetails .= '</label>';
                                            endforeach;
                                            $displayDetails .= '</div>';
                                        endif;
                                        $displayDetails .= '</div>&nbsp;<br/>&nbsp;</div>';

                                    endif;                                    
                                endif;
                            
                            endforeach;  
                            
                            echo $displayInstruction;
                            echo $displayHeader; 
                            echo $displayDetails; 
                            
                            
                        if($moduleDetails->status == "New" 
                                || $moduleDetails->status == "Pending Contract Review & Expectations"
                                || $moduleDetails->status == "Pending Platform Training"
                                || $moduleDetails->status == "Pending Tutor Training"):    
                            if($y == $moduleDetails->position): 
                            if($tag == "" && $disable[$y] == "active"):
//                                echo '<div class="col-md-2"></div>';
                                echo '<div class="col-md-12 submit text-center"><input type="submit" class="btn btn-info submit-btn" value="Submit Module" /></div>';
                            else:
                                echo '<div class="col-md-12 submit text-center"><input type="submit" class="btn btn-info submit-btn" value="Submit Module" /></div>';
                            endif;
                            endif;
                        endif; 
                        
                        if($disable[$y] != "active"):
                                echo '<div class="row center-content"><div class="col-md-12">
                                    <p class="inactive">These modules are currently inactive and will be activated shortly.</p>
                                    </div></div>';
                        endif;
                        echo $this->Form->end(); 
                        ?>
                        </div>
                    
                    <?php endfor; ?>
                    
                    
                </div>
                   
            </div>
            
        </div>

    </div>
</div>
</div>

<script>
    $(document).ready(function () {
 
    var hash = window.location.hash;

    switch (hash) {
        case '#completed':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').html('<b>Done</b> <span class="glyphicon glyphicon-ok"></span>');
            $('#response-dialog .modal-body p').text('Thank you for completing these modules, your HR manager will review and get back to you as soon as possible with the next steps.');
        break;
        case '#orientation-complete':
            $('#response-dialog').modal('show');
            $('#response-dialog .modal-title').html('<b>Done</b> <span class="glyphicon glyphicon-ok"></span>');
            $('#response-dialog .modal-body p').text('Thank you for completing these modules, your orientation is now complete.');
        break;
    }

});
</script>
<?php 
//echo $check;
//debug($moduleDetails); 
?>