<?= $this->Html->css('lity.css') ?>
<?= $this->Html->script('lity.js'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Admin / Training</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive add-new-client-content">
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <p>
                            Tutor2You strives to provide not only a top quality service to our clients but
                            also
                            to our tutors. In some cases tutors have never tutored before As such we have created a
                            series
                            of videos to assist with learning our procedures and the fundamentals of tutoring. As a
                            new
                            tutor with Tutor2You you must review these videos as part of your training.
                        </p>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <ul id="attabs" class="nav nav-tabs">
                            <li class="dropdown ">
                                <a href="#general-documents" data-toggle="tab">
                                    <h4>General Documents</h4>
                                </a>
                            </li>
                            <li class="active">
                                <a href="#our-system" data-toggle="tab">
                                    <h4>Our System</h4>
                                </a>
                            </li>
                        </ul>
                        <div id="attabsContent" class="tab-content">
                            <div class="tab-pane fade" id="general-documents">
                                <h4 class="hideme">General Documents</h4>
                                <div class="row container col-md-12 center-table">
                                    <div class="col-md-6">
                                        <h5>
                                            <i class="glyphicon glyphicon-check"></i>
                                            <strong>Blue card and Working with Children </strong>
                                        </h5>
                                        <ul>
                                            <li>
                                                <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/working-with-children-code-of-conduct.pdf' ?>"
                                                   target="_blank">
                                                    Working with children code of conduct
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.bluecard.qld.gov.au/" target="_blank">
                                                    Blue Card Commission Queensland Site
                                                </a>
                                            </li>
                                            <li>
                                                <a href="https://www.bluecard.qld.gov.au/info-sheets.html"
                                                   target="_blank">
                                                    Working with Children Information
                                                </a>
                                            </li>
                                            <li>
                                                <a href="http://www.workingwithchildren.vic.gov.au/home/"
                                                   target="_blank">
                                                    Working with Children Victoria
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><i class="glyphicon glyphicon-check"></i> <strong>Employment document
                                                references</strong></h5>
                                        <ul>
                                            <li>
                                                <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Tutors-Terms-and-Conditions.pdf' ?>"
                                                   target='_blank'>Tutor Terms &amp; Conditions</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active in" id="our-system">
                                <h4 class="hideme">General Documents</h4>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-12">
                                        <h4><i class="glyphicon glyphicon-list-alt"></i> Our Mission, Vision
                                            &amp;
                                            Culture</h4>
                                        <p>
                                            At Tutor2You we believe every student deserves to learn the necessary
                                            skills they need to succeed at school and beyond, with the knowledge, focus
                                            and motivation to put these skills into action.
                                        </p>
                                        <p>
                                            We approach education with a proactive philosophy to give each and every
                                            student the opportunity to discover how they learn best and the skills they
                                            need to succeed. Using a broad range of scientifically based learning
                                            strategies and techniques, which are simply not being taught at school,
                                            students are able to absorb information faster and more effectively.
                                        </p>
                                        <p>
                                            Using diagnostic assessments, including learning styles and skills as well
                                            as subject-specific assessments, we can target core issues and ensure our
                                            students have a solid foundation.
                                        </p>
                                        <hr>
                                    </div>
                                    <div class="container col-md-12">
                                        <ul>
                                            <li>
                                                <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Tutor2You - Vision Statement.pdf' ?>"
                                                   target='_blank'>Vision Statement</a>
                                            </li>
                                            <li>
                                                <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Tutor2You - Mission Statement.pdf' ?>"
                                                   target='_blank'>Mission Statement</a>
                                            </li>
                                            <li>
                                                <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Tutor2You - Culture Statement.pdf' ?>"
                                                   target='_blank'>Culture Statement</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-12 top-buffer">
                                        <h4><i class="glyphicon glyphicon-list"></i> Tutoring Processes</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12 top-buffer">
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Receiving Clients <a class="accordion-toggle"
                                                                         data-toggle="collapse"
                                                                         href="#receiving-clients">(Read
                                                        more)</a>
                                                    <ul id="receiving-clients" class="panel-collapse collapse">
                                                        <li>
                                                            Once your employment has been confirmed by HR (following
                                                            submission of forms to the relevant working with children
                                                            agency) your profile will be activated on our system.
                                                        </li>
                                                        <li>
                                                            Once active, <strong>it is important that tutors remain
                                                                contactable</strong>. Obviously we appreciate that you
                                                            have other commitments on a day to day basis, however, if
                                                            you will be uncontactable for a prolonged period please
                                                            advise administration.
                                                        </li>
                                                        <li>
                                                            Admin will contact you once we have a lead in your area.
                                                            This can be via phone, email or text with some preliminary
                                                            details.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Responding to Admin Regarding New Clients <a
                                                        class="accordion-toggle"
                                                        data-toggle="collapse"
                                                        href="#rtarncl">(Read
                                                        more)</a>
                                                    <ul id="rtarncl" class="panel-collapse collapse">
                                                        <li>
                                                            It is important that once you receive a new client, you
                                                            respond swiftly to administration, confirming your
                                                            acceptance or otherwise.
                                                        </li>
                                                        <li>
                                                            Note: Please be aware that if the admin does not receive a
                                                            response within a few hours they may contact another tutor.
                                                        </li>
                                                        <li>
                                                            Once you have confirmed you will take the client then
                                                            admin will add the client to the Tutoring Platform and
                                                            allocate them to you (See Tutoring System)
                                                        </li>
                                                        <li>
                                                            Note: Please be aware that as a condition of employment we
                                                            expect tutors to be able to complete a minimum of 4 hours of
                                                            tutoring per week. Any less and it is really not financially
                                                            viable. So if you’re not meeting the minimum targets and
                                                            turning down work you may be contacted by HR to discuss your
                                                            situation.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Initial Consultation Call (Contacting clients for the first time &
                                                    scheduling alignment lesson) <a class="accordion-toggle"
                                                                                    data-toggle="collapse"
                                                                                    href="#iccccftftsal">(Read
                                                        more)</a>
                                                    <ul id="iccccftftsal" class="panel-collapse collapse">
                                                        <li>
                                                            Once confirmed admin will send you any remaining details
                                                            if any regarding what the client is looking for and you will
                                                            be asked to contact your client to discuss their child’s
                                                            needs and schedule an alignment lesson. (See Other Tutor
                                                            Training)
                                                        </li>
                                                        <li>
                                                            It is important that you attempt to <strong>contact
                                                                clients as soon as possible and definitely within 24
                                                                hours</strong>. If this is not possible please advise
                                                            administration.
                                                        </li>
                                                        <li>
                                                            If you can’t get through to a client make sure you leave a
                                                            message and try again at a later time. There is nothing
                                                            worse than a client calling us saying they never heard from
                                                            a tutor.
                                                        </li>
                                                        <li>
                                                            During your initial call, you should try to determine
                                                            which initial diagnostic assessments would be most suitable
                                                            for the student (see Tutoring Systems)
                                                        </li>
                                                        <li>
                                                            The initial lesson is designed to act as an opportunity to
                                                            meet both the parent and the student and review a student’s
                                                            results with the parents. It is also a good opportunity for
                                                            you to learn more about what the parent is looking for from
                                                            you and make sure their expectations are aligned with what
                                                            you can provide. For example, you should ensure parents are
                                                            not expecting their child to improve from a D to an A with
                                                            only 1hr a week. The chances are that there are a number of
                                                            underlying issues which will take time to resolve
                                                        </li>
                                                        <li>
                                                            The alignment lesson is paid for tutors but is free for
                                                            clients, as it is only intended to be a meet and greet it is
                                                            <strong>capped at 30min.</strong>
                                                        </li>
                                                        <li>
                                                            Note: In some cases, clients don’t want the alignment
                                                            session and want to get straight into paid lessons (maybe
                                                            there is an exam coming up). This is not a problem you just
                                                            need to confirm this during your initial consultation call
                                                            and advise administration.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Confirming the Alignment Lesson <a class="accordion-toggle"
                                                                                       data-toggle="collapse"
                                                                                       href="#conf-tal">(Read
                                                        more)</a>
                                                    <ul id="conf-tal" class="panel-collapse collapse">
                                                        <li>
                                                            Please contact administration once you have contacted the
                                                            client as soon as possible and advise when you have
                                                            scheduled the alignment lesson.
                                                        </li>
                                                        <li>
                                                            It is important that we enter the bookings in the system
                                                            to ensure the accuracy of payroll.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Allocating Initial Assessments <a class="accordion-toggle"
                                                                                      data-toggle="collapse"
                                                                                      href="#all-ia">(Read
                                                        more)</a>
                                                    <ul id="all-ia" class="panel-collapse collapse">
                                                        <li>
                                                            Once you have confirmed the initial alignment lesson it is
                                                            important to log in to your tutor portal and allocate the
                                                            initial assessments to the client as soon as possible.
                                                        </li>
                                                        <li>
                                                            This will help ensure that clients have enough time to
                                                            have their child complete the assessments before your
                                                            alignment lesson.
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Preparing for the Alignment Lesson & Reviewing Results Reports <a
                                                        class="accordion-toggle"
                                                        data-toggle="collapse"
                                                        href="#prepftalrrr">(Read
                                                        more)</a>
                                                    <ul id="prepftalrrr" class="panel-collapse collapse">
                                                        <li>
                                                            Whilst there isn’t a lot of preparation that is required
                                                            for an alignment lesson you should ensure that the students
                                                            have completed the assessments you set. This can be
                                                            monitored from the tutoring platform (see Tutoring Systems)
                                                        </li>
                                                        <li>
                                                            Whilst the client can view the results from their profile
                                                            in the Tutoring Platform, if possible, print out the reports
                                                            and take them with you to discuss with the client.
                                                        </li>
                                                        <li>
                                                            It is professional to review the results reports prior to
                                                            getting to the lesson.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Alignment Lesson <a class="accordion-toggle"
                                                                        data-toggle="collapse"
                                                                        href="#tpal">(Read
                                                        more)</a>
                                                    <ul id="tpal" class="panel-collapse collapse">
                                                        <li>
                                                            The alignment lesson is intended to act as a meet and
                                                            greet.
                                                        </li>
                                                        <li>
                                                            Print out a copy of the <a
                                                                href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Consultation Run Sheet.pdf' ?>"
                                                                target='_blank'>Consultation Run Sheet</a> to help
                                                            guide you if you’re unsure during the first
                                                            lesson.
                                                        </li>
                                                        <li>
                                                            <strong>Make sure you’re on time and well dressed</strong>
                                                            (There’s no need
                                                            to go overboard, but boardies and thongs don't cut it)
                                                        </li>
                                                        <li>
                                                            During your lesson make sure you have reviewed the
                                                            student's results and the client's thoughts with respect to
                                                            booking. Some clients call us thinking ‘surely an hour a
                                                            week would be enough
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Confirming a booking <a class="accordion-toggle"
                                                                            data-toggle="collapse"
                                                                            href="#confab">(Read
                                                        more)</a>
                                                    <ul id="confab" class="panel-collapse collapse">
                                                        <li>
                                                            It is important to agree on a regular time with your
                                                            client.
                                                        </li>
                                                        <li>
                                                            Once you have finished a lesson make sure you let admin
                                                            know how it went and what plan you agreed with the client.
                                                        </li>
                                                        <li>
                                                            Admin will contact the client to get feedback and confirm
                                                            the booking and arrange invoicing
                                                        </li>
                                                        <li>
                                                            We record all lesson bookings in our system down to the
                                                            day and time and reconcile these with lessons reports so it
                                                            is important that you update admin if there is a change in
                                                            schedule for any reason.
                                                        </li>
                                                        <li>
                                                            <strong>Note:</strong> If you don’t advise of changes to
                                                            lessons it may
                                                            result in a delay in your pay if you’re lesson reports don’t
                                                            match.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Ongoing Expectations <a class="accordion-toggle"
                                                                            data-toggle="collapse"
                                                                            href="#ongexp">(Read
                                                        more)</a>
                                                    <ul id="ongexp" class="panel-collapse collapse">
                                                        <li>
                                                            You’re expected to be able to provide a consistent lesson
                                                            time to your client, turn up on time, and presentable.
                                                        </li>
                                                        <li>
                                                            We record all lesson bookings in our system down to the
                                                            day and time and reconcile these with lessons reports so it
                                                            is important that you update admin if there is a change in
                                                            schedule for any reason.
                                                        </li>
                                                        <li>
                                                            At times we find clients aren’t willing to give negative
                                                            feedback to tutors directly, as such, from time to time we
                                                            will reach out to clients directly to ensure they are happy
                                                            with the service.
                                                        </li>
                                                        <li>
                                                            <strong>Note:</strong> If you don’t advise of changes to
                                                            lessons it may
                                                            result in a delay in your pay if you’re lesson reports don’t
                                                            match.
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-12 top-buffer">
                                        <h4><i class="glyphicon glyphicon-th-list"></i> Other Processes</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12 top-buffer">
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Lesson Report <a
                                                        class="accordion-toggle"
                                                        data-toggle="collapse"
                                                        href="#lesson-report">(Read
                                                        more)</a>
                                                    <ul id="lesson-report" class="panel-collapse collapse">
                                                        <li>
                                                            Following the conclusion of every lesson you’re required to
                                                            submit a lesson report. This serves a number of purposes
                                                            including:
                                                            <ul>
                                                                <li>These are reconciled against the client booking
                                                                </li>
                                                                <li>The reconciled lessons are then used for payroll
                                                                </li>
                                                                <li>We maintain a database of student records in the
                                                                    event a tutor needs to change they can pick up where
                                                                    the last one left off
                                                                </li>
                                                                <li>For insurance purposes to ensure you’re covered,
                                                                    we must have a record of the dates and times when
                                                                    you’re working and where
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Lesson reports should be submitted within 24 hours of a
                                                            lesson
                                                        </li>
                                                        <li>
                                                            Lesson reports are submitted via the tutoring portal
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Contact List <a class="accordion-toggle"
                                                                    data-toggle="collapse"
                                                                    href="#contact-list">(Read
                                                        more)</a>
                                                    <ul id="contact-list" class="panel-collapse collapse">
                                                        <li>
                                                            Tutor Facebook Group
                                                            <ul>
                                                                <li>For receiving regular update, interacting with
                                                                    management, asking questions & making suggestions
                                                                </li>
                                                                <li>
                                                                    <a href="https://www.facebook.com/groups/940688206000788/"
                                                                       target="_blank">Facebook Tutors Page</a></li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Customer Service & Sales 
                                                            <ul>
                                                                <li>For booking confirmations and customer & sales
                                                                    enquiries
                                                                </li>
                                                                <li><a href="">alex@tutor2you.com.au</a></li>
                                                                <li>1300 4200 79 ext 1</li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            People & Culture 
                                                            <ul>
                                                                <li>For all applicants, tutor employment enquiries</li>
                                                                <li><a href="">kate@tutor2you.com.au</a></li>
                                                                <li>1300 4200 79 ext 1</li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Tutoring Operations Manager
                                                            <ul>
                                                                <li>For all tutoring service related queries</li>
                                                                <li><a href="">dean@tutor2you.com.au</a></li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            General Administration
                                                            <ul>
                                                                <li>For payroll & general admin</li>
                                                                <li><a href="">charlene@tutor2you.com.au</a></li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Information Technology
                                                            <ul>
                                                                <li>For all platform enquiries</li>
                                                                <li><a href="">rosette@tutor2you.com.au</a></li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Management
                                                            <ul>
                                                                <li>For all other enquiries</li>
                                                                <li><a href="">scott@tutor2you.com.au</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                        </div>
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Payroll
                                                    <ul>
                                                        <li>
                                                            Payroll is conducted fortnightly ending on a Sunday.
                                                            Reconciliation is then conducted and the pay run is
                                                            processed on Wednesday thereafter.
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12 center-table">
                                    <div class="col-md-12 top-buffer">
                                        <h4><i class="glyphicon glyphicon-briefcase"></i> Tutoring
                                            System</h4>
                                        <p>
                                            At T2Y we believe our role is not only to provide highly targeted and
                                            efficient tutoring but also act as an education mentor to our students. We
                                            find that younger tutors often relate and connect better with our students
                                            than teachers and as such adopt more of a mentor style role
                                        </p>
                                        <p>
                                            We see our role as more than just homework help and it is here where our
                                            assessments and reporting come in.
                                        </p>
                                        <p>

                                        </p>
                                        <hr>
                                    </div>
                                    <div class="col-md-12 top-buffer">
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Understanding the Tutoring Platform
                                                    <ul>
                                                        <li>
                                                            The T2Y Tutoring Platform is fast becoming the hub for all
                                                            things
                                                            tutoring.
                                                        </li>
                                                        <li>
                                                            Providing client assessments facilities, program creation,
                                                            resources, training, lesson reports and more.
                                                        </li>
                                                        <li>
                                                            Note: as a tutor, you can’t actually see a client’s
                                                            assessments
                                                            because they are all generated when they begin the exam but
                                                            the
                                                            reports show the overall results and particularly the
                                                            outcomes and
                                                            topics they got wrong.
                                                        </li>
                                                        <li>
                                                            As you use the program and read these notes, you will notice
                                                            the
                                                            reference to learning Categories, major and minor
                                                            outcomes.<a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#tptsy4th"> (Continue Reading)</a>
                                                            <p id="tptsy4th" class="panel-collapse collapse">
                                                                These
                                                                are the Australian Curriculum outcomes which you can
                                                                find under
                                                                the
                                                                ‘Resources’ Tab in the platform. There are typically 3-4
                                                                Learning
                                                                Categories per year level, and a total of 7-10 Major
                                                                Outcomes,
                                                                with
                                                                each containing 1-5 Minor Outcomes.
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Understanding the Diagnostic Assessments
                                                    <ul>
                                                        <li>
                                                            At the time of writing we have 4 different types of
                                                            assessments in the system including: <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#utda"> (Continue Reading)</a>
                                                            <ul id="utda" class="panel-collapse collapse">
                                                                <li>Math Diagnostic</li>
                                                                <li>Math Extension</li>
                                                                <li>Learning Style</li>
                                                                <li>Learning IQ (Study Skills)</li>
                                                            </ul>
                                                        </li>
                                                        <li>Note: each assessment is designed to take approximately
                                                            15-30 minutes.
                                                        </li>
                                                        <li>
                                                            Maths & English Diagnostic <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#maed"> (Read More)</a>
                                                            <ul id="maed" class="panel-collapse collapse">
                                                                <li>These are assessments are, as named, diagnostic in
                                                                    nature. They are designed to identify underlying
                                                                    knowledge gaps with students in order to allow a
                                                                    tutor to rectify them.
                                                                </li>
                                                                <li>They do this by providing questions based on the
                                                                    year level 1 below what the student is currently in,
                                                                    so a year 5 student will be assessed against year 4
                                                                    outcomes. The reason for this is because if
                                                                    assessing a year 5 student against year 5 outcomes,
                                                                    there’s a chance he may not have learnt most of the
                                                                    year 5 curriculum yet.
                                                                </li>
                                                                <li>The questions are spread across each major outcome
                                                                    in the year level, variation in a number of
                                                                    questions will depend on how many minor outcomes
                                                                    exist in the curriculum, and how many questions are
                                                                    in the database.
                                                                </li>
                                                                <li>The reports group results by learning category,
                                                                    major outcome and also specify which specific
                                                                    questions the student got wrong and the minor
                                                                    outcome description.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Learning Style Assessment <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#lsa"> (Read More)</a>
                                                            <ul id="lsa" class="panel-collapse collapse">
                                                                <li>The purpose of this assessment is to identify the
                                                                    child’s learning style (Other Tutor Training for
                                                                    Learning Style Descriptions)
                                                                </li>
                                                                <li>The assessment is broken up into primary and
                                                                    secondary assessments.
                                                                </li>
                                                                <li>Once the assessment is completed the report includes
                                                                    the Students Learning Style as well as the relevant
                                                                    Learning Style Descriptions for the student.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Learning IQ Assessment <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#lia"> (Read More)</a>
                                                            <ul id="lia" class="panel-collapse collapse">
                                                                <li>This assessment is a proprietary assessment
                                                                    developed by Tutor2You to assess a student’s
                                                                    overall study skills. In the vast majority of cases
                                                                    we find that students are never really taught how to
                                                                    study as a result they are often not studying
                                                                    efficiently or effectively.
                                                                </li>
                                                                <li>The report outlines the student’s strengths and
                                                                    weaknesses.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul>
                                                <li>
                                                    Navigating the Platform
                                                    <ul>
                                                        <li>
                                                            Dashboard <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ntpdashboard"> (Read More)</a>
                                                            <ul id="ntpdashboard" class="panel-collapse collapse">
                                                                <li>The dashboard is where your client management
                                                                    occurs
                                                                </li>
                                                                <li>Your leads table is a list of the students allocated
                                                                    to you by admin following your confirmation you will
                                                                    take on the student.
                                                                </li>
                                                                <li>
                                                                    Lead Table
                                                                    <ul>
                                                                        <li>Once a lead has been allocated you may
                                                                            view the lead details using the button on
                                                                            the right of the record, and from here you
                                                                            may allocate the assessments.
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    Student Assessment Table
                                                                    <ul>
                                                                        <li>Once you have allocated assessments to a
                                                                            student, they will move from the lead table
                                                                            to the assessment table where you can
                                                                            monitor whether a student has completed
                                                                            their assessments or not.
                                                                        </li>
                                                                        <li>Using the view button on the right you can
                                                                            view results, allocate new assessments, or
                                                                            cancel existing assessments.
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>Resources - Resources</li>
                                                        <li>
                                                            Resources - Year Level Overview & Curriculum <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ryloac"> (Read More)</a>
                                                            <ul id="ryloac" class="panel-collapse collapse">
                                                                <li>
                                                                    This tab is where our tutoring resources are
                                                                    stored, including site membership details as well
                                                                    as the resources database table
                                                                    <ul>
                                                                        <li>The table at the bottom of the screen is
                                                                            our database of worksheets.
                                                                        </li>
                                                                        <li>You can filter the worksheets by year
                                                                            level or search by name, learning category,
                                                                            subject, minor outcome or major outcome
                                                                        </li>
                                                                        <li>Use the view button on the left to view
                                                                            the pdf and print it out for use during
                                                                            lessons
                                                                        </li>
                                                                        <li>Note: The assessment results reference the
                                                                            major and minor outcomes that students are
                                                                            struggling with or got wrong, but remember
                                                                            you have to select resources from the year 1
                                                                            below what the student is in currently.
                                                                            (It’s about reinforcing their knowledge and
                                                                            filling the gaps before extending it)
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>Here you will find all the year level portfolio’s
                                                                    curriculum and NAPLAN resources
                                                                </li>
                                                                <li>Note the Study Skills Module is also provided to
                                                                    some clients through their platform if they have
                                                                    booked for 1.5 hours per week or more.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Admin / Training <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ntpat"> (Read More)</a>
                                                            <ul id="ntpat" class="panel-collapse collapse">
                                                                <li>Here you will find all of your tutor training and
                                                                    procedures.
                                                                </li>
                                                                <li>Use the tabs to navigate different Training Topics
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Session Report <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ntpsr"> (Read More)</a>
                                                            <ul id="ntpsr" class="panel-collapse collapse">
                                                                <li>Here you will lodge your session reports for your
                                                                    clients.
                                                                </li>
                                                                <li>Note: if you have a smartphone, try navigating to
                                                                    the tutoring platform and adding the page to your
                                                                    phone home screen. This will allow you to quickly
                                                                    log in at the end of your session and submit the
                                                                    report then and there.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Report Bug / Comment <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ntprbc"> (Read More)</a>
                                                            <ul id="ntprbc" class="panel-collapse collapse">
                                                                <li>Here you can submit a bug or suggestion directly to
                                                                    our IT department simply fill in your comments.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            Profile <a
                                                                class="accordion-toggle"
                                                                data-toggle="collapse"
                                                                href="#ntpprofile"> (Read More)</a>
                                                            <ul id="ntpprofile" class="panel-collapse collapse">
                                                                <li>This is nested under your name in the top right
                                                                    corner of the platform.
                                                                </li>
                                                                <li>Here you can change edit your account details.</li>
                                                                <li>Whilst we’re not using these details for allocations
                                                                    or availability yet, they will be used shortly.
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12 top-buffer">
                                    <div class="col-md-12">
                                        <h4><i class="glyphicon glyphicon-th-list"></i> Other Tutor Training</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 class="hideme">Initial Client Consultation Call</h5>
                                        <div class="col-md-4">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h5 class="text-center">
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           href="#icccall"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Initial Client Consultation Call</a>
                                                    </h5>
                                                </div>
                                                <div id="icccall" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-unstyled">
                                                            <li class="text-left">
                                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                                                Training Video
                                                                <ul>
                                                                    <li>
                                                                        <a href="https://youtu.be/nGzCXlqcv1o"
                                                                           data-lity>
                                                                            Consultation Call Discussion
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <h5 class="text-center">
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           href="#strsestem"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            Structuring Sessions Template</a>
                                                    </h5>
                                                </div>
                                                <div id="strsestem" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-unstyled">
                                                            <li class="text-left">
                                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                                                Training Video
                                                                <ul>
                                                                    <li>
                                                                        <a href="https://youtu.be/28fna5A6crE"
                                                                           data-lity>
                                                                            Suggested Session Template
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel panel-danger">
                                                <div class="panel-heading">
                                                    <h5 class="text-center">
                                                        <a class="accordion-toggle" data-toggle="collapse"
                                                           href="#alsapp"><i
                                                                class="glyphicon glyphicon-chevron-down"></i>
                                                            A Learning Style Approach</a>
                                                    </h5>
                                                </div>
                                                <div id="alsapp" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <ul class="list-unstyled">
                                                            <li class="text-left">
                                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                                                Resources
                                                                <ul>
                                                                    <li>
                                                                        <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Learning Style Descriptions.pdf' ?>"
                                                                           target='_blank'>
                                                                            Learning Style
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li class="text-left">
                                                                <i class="glyphicon glyphicon-chevron-right"></i>
                                                                Training Video
                                                                <ul>
                                                                    <li>
                                                                        <a href="https://youtu.be/G8FhE4uUK4M"
                                                                           data-lity>
                                                                            A Learning Style Approach
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>