<?php
    $average = array();
    $assessmentDetails = array();
    
    foreach($typeOfAssessments as $key => $assessment): 
        
        $assessmentDetails[$key]['assessment_type_id']  = $assessment->assessment_type_id;
        $assessmentDetails[$key]['done_date']           = $assessment->done_date;
        $assessmentDetails[$key]['assessment_type_name']= $assessment->assessment_type->type;
    
        if($assessment->assessment_type_id <= 2):
            
            
            $assessmentDetails[$key]['average'] = assessmentLearning($assessment->user_questions);
            
        elseif($assessment->assessment_type_id > 2):
            
            $assessmentDetails[$key]['average'] = assessmentProcess($assessment->user_questions);
            
        endif;
        
    endforeach;
    
    
    function assessmentProcess($userQuestions = null){
        
        $correct = 0;
        foreach($userQuestions as $question):
            
            if($question->correct_answer == $question->user_answer):
                $correct += 1;
            endif;
            
        endforeach;
        
        if($correct != 0):
            $value = round((($correct / count($userQuestions)) * 100), 2);
        else:            
            $value = 0;
        endif;
        
        return $value;
        
    }
    
    function assessmentLearning($userQuestions = null){
        
        $correct = 0;
        $totalValue = 0;
        foreach($userQuestions as $question):
            
            $correct += getMarkValue($question->user_answer, $question->choices, $question->mark);
            $totalValue += getHighestMarkValue($question->mark);
      
        endforeach;
                
        if($correct != 0):
            $value = round((($correct / $totalValue) * 100), 2);
        else:            
            $value = 0;
        endif;
        
        return $value;
        
    }

    function getMarkValue($user_answer, $choices, $mark){
        
        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue  = json_decode($mark, TRUE);
        $choiceKey  = array_search($userAnswer,$choicesVal);
        return $markValue[$choiceKey];
        
    }
    
    function getHighestMarkValue($mark){
        $markValue  = json_decode($mark, TRUE);
        return max($markValue);
    }
    
    function sortByAverage($a, $b) { // or playcount or whatever
        if($a["average"] == $b["average"]) {
            return 0;
        }
        return ($a["average"] < $b["average"]) ? -1 : 1;
    }
    
    usort($assessmentDetails, "sortByAverage");
    
?>
<select id='ref-assmnts' name='leadAssessmentTypeId[]' multiple='multiple' required>

    <?php foreach($assessmentDetails as $at): ?>        
        <option value="<?= $at['assessment_type_id'] ?>">
            <?= $at['assessment_type_name'] ?>, <?php echo $at['average'] ?> % , <?php echo date("m-d-Y", strtotime($at['done_date'])); ?>
        </option>
    <?php endforeach; ?>
        
</select>
