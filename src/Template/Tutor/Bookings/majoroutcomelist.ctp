<?php

    $majorOutcome = array();
    
    foreach($StudentAssessmentTypeDetails as $key => $assessmentType):
        
        foreach($assessmentType['user_questions'] as $majorO):
        
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['majoroutcome_id'] = $majorO->minor_outcome->major_outcome->id;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['assessment_type_id'] = $assessmentType->assessment_type_id;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['majoroutcome_name'] = $majorO->minor_outcome->major_outcome->name;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['correct'] = 0;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['wrong'] = 0;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['total-mark-value'] = 0;
            $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['average'] = 0;
        
        endforeach;
        
    endforeach;
    
    
    foreach($StudentAssessmentTypeDetails as $key => $assessmentType):
        
        
        if($assessmentType->assessment_type_id <= 2):
            
            foreach($assessmentType['user_questions'] as $majorO):
            
                $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['correct'] += getMarkValue($majorO->user_answer, $majorO->choices, $majorO->mark);
                $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['total-mark-value'] += getHighestMarkValue($majorO->mark,$majorO->user_answer);
        
            endforeach;
            
        else:
            
            foreach($assessmentType['user_questions'] as $majorO):


                if($majorO->correct_answer == $majorO->user_answer):
                    $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['correct'] += 1;
                else:
                    $majorOutcome[$key][$majorO->minor_outcome->major_outcome_id]['wrong'] += 1;
                endif;


            endforeach;
            
        endif;
        
        
    endforeach;
    
    
    foreach($majorOutcome as $key => $compAverage): 
        
        foreach($compAverage as $key2 => $compDetail):
        
            if($compDetail['assessment_type_id'] <= 2):

                $majorOutcome[$key][$key2]['average'] = round(($compDetail['correct'] / ($compDetail['total-mark-value'])) * 100,2);
                
            else:

                $majorOutcome[$key][$key2]['average'] = round(($compDetail['correct'] / ($compDetail['correct'] + $compDetail['wrong'])) * 100,2);

            endif;

        endforeach;
        
    endforeach;
        
    foreach($majorOutcome as $k1 => $m):
        
        usort($majorOutcome[$k1], "sortByAverage");
        
    endforeach;
    
    
    function getMarkValue($user_answer, $choices, $mark)
    {
        //#skipped
        if($user_answer != "#skipped"):

            $userAnswer = $user_answer;
            $choicesVal = json_decode($choices, TRUE);
            $markValue = json_decode($mark, TRUE);
            $choiceKey = array_search($userAnswer, $choicesVal);

            return $markValue[$choiceKey];
        else:

            return 0;

        endif;

    }

    function getHighestMarkValue($mark, $user_answer)
    {
        if($user_answer != "#skipped"):

            $markValue = json_decode($mark, TRUE);
            return max($markValue);
        else:

            return 0;

        endif;
    }
        
    
    function sortByAverage($a, $b) { // or playcount or whatever
        if($a["average"] == $b["average"]) {
            return 0;
        }
        return ($a["average"] < $b["average"]) ? -1 : 1;
    }
    
    
//    debug($position);

?>

<input type='hidden' id="mdetails<?= $position ?>" value='<?= $position ?>' />

<select name="info[<?= $position ?>][majoroutcome]" id="majoroutcome<?= $position ?>" class="form-control majoroutcome" >
    
    <option disabled="" selected="" placeholder="Select Type">Choose Outcome</option>
    <?php foreach($majorOutcome as $mDetails): ?>
    
        <?php foreach($mDetails as $mDet): ?>
            <option value="<?= $mDet['majoroutcome_id'] ?>" ><?= $mDet['majoroutcome_name'] ?>, <?= $mDet['average'] ?> % </option>
        <?php endforeach; ?>
    
    <?php endforeach; ?> 
    
</select>


<?php if($compDetail['assessment_type_id'] <= 2): ?>

    <script>

        $('#majoroutcome<?= $position ?>').on('click', function () {
            var majorId = $(this).val(),
                passLessonId= $("#mdetails<?= $position ?>").val(),
                listing = $('#assmntTopic<?= $position ?>');
                
            if (majorId!==null){
                $.post('/tutor/bookings/topiclistlearning/'+passLessonId
                        
                
                , function (data) {
                    listing.html(data);
                });
            }

        });

    </script>

<?Php else: ?>

    <script>

        $('#majoroutcome<?= $position ?>').on('click', function () {
            var majorId = $(this).val(),
                passLessonId= $("#mdetails<?= $position ?>").val(),
                listing = $('#assmntTopic<?= $position ?>');
                
            if (majorId!==null){
                $.post('/tutor/bookings/topiclist/'+majorId+'/'+listing+'/'+passLessonId, function (data) {
                    listing.html(data);
                }); 
            }

        });

    </script>

<?php endif; ?>

<?php // debug($majorOutcome); ?>