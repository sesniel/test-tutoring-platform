<?php //debug($programDetails->toArray());exit;
    // if($programDetails->status == "In Progress"):
    //     header('Location: '.$this->request->params['prefix'].'/'.$this->request->params['controller'].'/table');
    // endif;
//    debug($assessmentSelect);
    $average = array();
    $assessmentDetails = array();
    $assessmentinfo = array();
    
    foreach($assessmentSelect as $studId => $student):
        
        if(!empty($student)):     
            foreach($student as $studentInfo):
//            debug($studentInfo);
            
            
    
                if($studentInfo->assessment_type_id <= 2):            

                    $assessmentDetails[$studId]['average'][] = assessmentLearning($studentInfo->user_questions);

                elseif($studentInfo->assessment_type_id > 2):

                    $assessmentDetails[$studId]['average'][] = assessmentProcess($studentInfo->user_questions);

                endif;
                        
                $assessmentDetails[$studId]['assessment_type_id'][]  = $studentInfo->assessment_type_id;
                $assessmentDetails[$studId]['done_date'][]           = $studentInfo->done_date;
                $assessmentDetails[$studId]['assessment_type_name'][]= $studentInfo->assessment_type->type;
                $assessmentDetails[$studId]['student_information'][] = $studentInfo->student;
            endforeach;
        else:    
            $assessmentDetails[$studId]['assessment_type_name'][]  = "General";
        endif;
        
        
    endforeach;
    
//    debug($assessmentDetails);
    
    
    function assessmentProcess($userQuestions = null){
        
        $correct = 0;
        foreach($userQuestions as $question):
            
            if($question->correct_answer == $question->user_answer):
                $correct += 1;
            endif;
            
        endforeach;
        
        if($correct != 0):
            $value = round((($correct / count($userQuestions)) * 100), 2);
        else:            
            $value = 0;
        endif;
        
        return $value;
        
    }
    
    function assessmentLearning($userQuestions = null){
        
        $correct = 0;
        $totalValue = 0;
        foreach($userQuestions as $question):
            
            $correct += getMarkValue($question->user_answer, $question->choices, $question->mark);
            $totalValue += getHighestMarkValue($question->mark);
      
        endforeach;
                
        if($correct != 0):
            $value = round((($correct / $totalValue) * 100), 2);
        else:            
            $value = 0;
        endif;
        
        return $value;
        
    }

    function getMarkValue($user_answer, $choices, $mark){
        
        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue  = json_decode($mark, TRUE);
        $choiceKey  = array_search($userAnswer,$choicesVal);
        return $markValue[$choiceKey];
        
    }
    
    function getHighestMarkValue($mark){
        $markValue  = json_decode($mark, TRUE);
        return max($markValue);
    }
    
    function sortByAverage($a, $b) { // or playcount or whatever
        if($a["average"] == $b["average"]) {
            return 0;
        }
        return ($a["average"] < $b["average"]) ? -1 : 1;
    }
    
//    usort($assessmentDetails, "sortByAverage");
    
?>
<style>
    .standard{
        width: 90px !important;
    }
    
    .form-control{
        height:30px !important;
    }
    
</style>
<div class="row widget-box">
    <div class="col-md-12">
        <h2 class="text-center"><strong>Lesson Information</strong></h2>
        <?php //if($client->toArray()) { echo "test";} ?>
        <input type="hidden" id="client_id" value="<?= $programDetails->tutor->id ?>" />
        <hr/>
    </div>
        
    <div class="col-md-12">

        <?php
            echo $this->Form->create(null, [
                'url' => ['action' => 'savelessoninfo'],
                'method' => 'post'
            ]);
        ?>
        <input type="hidden" value="<?= $programDetails->id ?>" name="program_id" />
        <?php //if(!($client->toArray())): ?>
        <table class="table table-striped responsive display table-hover table-bordered pending">
            <thead>
            <tr>
                <th class="standard">Date</th>
                <th class="standard">Time</th>
                <th class="standard">Length</th>
                <th class="standard">Status</th>
                <?php if(!($client->toArray())): ?>
                <th class="studentInfo" colspan="3">Student Information</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
                
            <?php $x = 0; foreach ($lessonDetails as $key => $lesson): ?>
                <?Php if($view == "none"): $x++; if($x == 1): $disp = ""; else: $disp = $x;  endif;  ?>
                    <tr>
                        <td>
                            <input type="hidden" name="lesson_id[]" value="<?= $lesson->id ?>" />
                            <div class="input-group"><input style="width:100px;" class="form-control lessonDate" id="lessonDate"
                                                            type="text" name="lessonDate[]" value="<?php echo date('d/m/Y', strtotime($lesson->date))  ?>" required><span
                                    class="input-group-addon"><i
                                        class="glyphicon glyphicon-calendar"></i></span></div>
                        </td>
                        <td>
                            <div class="input-group"><input style="width:100px;" class="form-control lessonTime" id="lessonTime"
                                                            type="text" name="lessonTime[]" value="<?php echo date('h:i A', strtotime($lesson->time))  ?>" required><span
                                    class="input-group-addon"><i
                                        class="glyphicon glyphicon-time"></i></span></div>
                        </td>
                        <td><?= $lesson->length ?></td>
                        <td><?php 
                            if($lesson->status == "Tutor"):
                                echo "Pending";
                            else:
                                echo $lesson->status;
                            endif;
                        ?></td>
                        <?php if(!($client->toArray())): ?>
                        <td>
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Subject</th>
                                    <th>Type</th>
                                    <th>Topic</th>                                    
                                </tr>
                            <?php foreach($lesson->lesson_students as $lessonInfo): ?>
                                <tr>
                                    <th>
                                        <?Php echo $lessonInfo->program_student->student->name; ?>
                                    </th>
                                    <td>
                                        <?php if($assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'][0] == "General"): ?>
                                            
                                                <input type="hidden" id='passLessonId' name='info[<?= $lessonInfo->id ?>][leadAssessmentTypeId]' value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name='info[<?= $lessonInfo->id ?>][lessonId]' value="<?= $lesson->id ?>" />
                                                <input type="hidden" value="<?= $lessonInfo->id ?>" />
                                                <select name='info[<?= $lessonInfo->id ?>][leadAssessmentTypeId]' id="type<?= $lessonInfo->id ?>" class="form-control" required>
                                                    <option selected disabled=""> - - - </option>
                                                    <option value="0"> General </option>
                                                </select>  

                                                <script>

                                                    $('#type<?= $lessonInfo->id ?>').on('click', function () {
                                                        var type = $(this).val(),
                                                            clientID = $("#client_id").val(),
                                                            passLessonId= $("#passLessonId").val(),
                                                            studentID= $("#student_id").val(),
                                                            listing = $('#assmntRefference<?= $lessonInfo->id ?>'),
                                                            position = $('#position<?= $lessonInfo->id ?>').val();

                //                                            console.log(position);

                                                        if (type!==null){
                                                            $.post('../majoroutcomelist/' + type + '/' + studentID + '/' + clientID + '/' + position + '/' + passLessonId, function (data) {
                                                                listing.html(data);
                                                            });
                                                        }

                                                    });

                                                </script>
                                        <?php else: ?>
                                                
                                                
                                                <input type="hidden" id="position<?= $lessonInfo->id ?>" value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name1='info[<?= $lessonInfo->id ?>][lessonStudentId]'  value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name='info[<?= $lessonInfo->id ?>][lessonId]' value="<?= $lesson->id ?>" />
                                                <input type="hidden" id="student_id<?= $lessonInfo->id ?>" value="<?= $lessonInfo->program_student->student->id ?>" />
                                                <select name='info[<?= $lessonInfo->id ?>][leadAssessmentTypeId]' id="type<?= $lessonInfo->id ?>" class="form-control" required>
                                                    <option selected disabled=""> - - - </option>
                                                    <option value="0"> General </option>
                                                    <?php foreach($assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'] as $assTypeKey => $assType): ?>
                                                        
                                                            <option value="<?= $assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_id'][$assTypeKey] ?>">
                                                                <?= $assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'][$assTypeKey] ?>,
                                                                <?php echo $assessmentDetails[$lessonInfo->program_student->student->id]['average'][$assTypeKey] ?> % , 
                                                                <?php echo date("m-d-Y", strtotime($assessmentDetails[$lessonInfo->program_student->student->id]['done_date'][$assTypeKey])); ?>
                                                            </option>
                                        
                                                    <?php endforeach; ?>
                                                </select>  

                                                <script>

                                                    $('#type<?= $lessonInfo->id ?>').on('click', function () {
                                                        var type = $(this).val(),
                                                            clientID = $("#client_id").val(),
                                                            passLessonId= $('#position<?= $lessonInfo->id ?>').val();
                                                            studentID= $("#student_id<?= $lessonInfo->id ?>").val(),
                                                            listing = $('#assmntRefference<?= $lessonInfo->id ?>'),
                                                            position = $('#position<?= $lessonInfo->id ?>').val();

                //                                            console.log(position);

                                                        if (type!==null){
                                                            $.post('../majoroutcomelist/' + type + '/' + studentID + '/' + clientID + '/' + position + '/' + passLessonId, function (data) {
                                                                listing.html(data);
                                                            });
                                                        }

                                                    });

                                                </script>
                                                
                                                
                                                
                                        <?php endif; ?>

                                    </td>
                                    <td>
                                        <div id="assmntRefference<?= $lessonInfo->id ?>" class="col-md-12">

                                        </div>
                                    </td>
                                    <td>
                                        <div id="assmntTopic<?= $lessonInfo->id ?>" class="col-md-12">

                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </table>
                            
                        </td>
                        <?php endif; ?>
                    </tr>
                <?php else: ?>
                    <?php if($view == $lesson->id): ?>
                    <tr>
                        <td>
                            <input type="hidden" name="lesson_id[]" value="<?= $lesson->id ?>" />
                            <div class="input-group"><input style="width:100px;" class="form-control lessonDate" id="lessonDate"
                                                            type="text" name="lessonDate[]" value="<?php echo date('d/m/Y', strtotime($lesson->date))  ?>" required><span
                                    class="input-group-addon"><i
                                        class="glyphicon glyphicon-calendar"></i></span></div>
                        </td>
                        <td>
                            <div class="input-group"><input style="width:100px;" class="form-control lessonTime" id="lessonTime"
                                                            type="text" name="lessonTime[]" value="<?php echo date('h:i A', strtotime($lesson->time))  ?>" required><span
                                    class="input-group-addon"><i
                                        class="glyphicon glyphicon-time"></i></span></div>
                        </td>
                        <td><?= $lesson->length ?></td>
                        <td><?php 
                            if($lesson->status == "Tutor"):
                                echo "Pending";
                            else:
                                echo $lesson->status;
                            endif;
                        ?></td>
                        <td>
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Subject</th>
                                    <th>Type</th>
                                    <th>Topic</th>                                    
                                </tr>
                            <?php foreach($lesson->lesson_students as $lessonInfo): ?>
                                <tr>
                                    <th>
                                        <?Php echo $lessonInfo->program_student->student->name; ?>
                                    </th>
                                    <td>
                                        <?php if($assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'][0] == "General"): ?>
                                            
                                                <input type="hidden" id="position<?= $lessonInfo->id ?>" value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name1='info[<?= $lessonInfo->id ?>][lessonStudentId]'  value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name='info[<?= $lessonInfo->id ?>][lessonId]' value="<?= $lesson->id ?>" />
                                                <input type="hidden" id="student_id<?= $lessonInfo->id ?>" value="<?= $lessonInfo->program_student->student->id ?>" />
                                                <select name='info[<?= $lessonInfo->id ?>][leadAssessmentTypeId]' id="type<?= $lessonInfo->id ?>" class="form-control" required>
                                                    <option selected disabled=""> - - - </option>
                                                    <option value="0"> General </option>
                                                </select>  

                                                <script>

                                                    $('#type<?= $lessonInfo->id ?>').on('click', function () {
                                                        var type = $(this).val(),
                                                            clientID = $("#client_id").val(),
                                                            passLessonId= $('#position<?= $lessonInfo->id ?>').val();
                                                            studentID= $("#student_id<?= $lessonInfo->id ?>").val(),
                                                            listing = $('#assmntRefference<?= $lessonInfo->id ?>'),
                                                            position = $('#position<?= $lessonInfo->id ?>').val();

                //                                            console.log(position);

                                                        if (type!==null){
                                                            $.post('/tutor/bookings/majoroutcomelist/' + type + '/' + studentID + '/' + clientID + '/' + position + '/' + passLessonId, function (data) {
                                                                listing.html(data);
                                                            });
                                                        }

                                                    });

                                                </script>
                                        <?php else: ?>
                                                 
                                                
                                                <input type="hidden" id="position<?= $lessonInfo->id ?>" value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name1='info[<?= $lessonInfo->id ?>][lessonStudentId]'  value="<?= $lessonInfo->id ?>" />
                                                <input type="hidden" name='info[<?= $lessonInfo->id ?>][lessonId]' value="<?= $lesson->id ?>" />
                                                <input type="hidden" id="student_id<?= $lessonInfo->id ?>" value="<?= $lessonInfo->program_student->student->id ?>" />
                                                <select name='info[<?= $lessonInfo->id ?>][leadAssessmentTypeId]' id="type<?= $lessonInfo->id ?>" class="form-control" required>
                                                    <option selected disabled=""> - - - </option>
                                                    <option value="0"> General </option>
                                                    <?php foreach($assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'] as $assTypeKey => $assType): ?>
                                                        
                                                            <option value="<?= $assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_id'][$assTypeKey] ?>">
                                                                <?= $assessmentDetails[$lessonInfo->program_student->student->id]['assessment_type_name'][$assTypeKey] ?>,
                                                                <?php echo $assessmentDetails[$lessonInfo->program_student->student->id]['average'][$assTypeKey] ?> % , 
                                                                <?php echo date("m-d-Y", strtotime($assessmentDetails[$lessonInfo->program_student->student->id]['done_date'][$assTypeKey])); ?>
                                                            </option>
                                        
                                                    <?php endforeach; ?>
                                                </select>  

                                                <script>

                                                    $('#type<?= $lessonInfo->id ?>').on('click', function () {
                                                        var type = $(this).val(),
                                                            clientID = $("#client_id").val(),
                                                            passLessonId= $('#position<?= $lessonInfo->id ?>').val();
                                                            studentID= $("#student_id<?= $lessonInfo->id ?>").val(),
                                                            listing = $('#assmntRefference<?= $lessonInfo->id ?>'),
                                                            position = $('#position<?= $lessonInfo->id ?>').val();

                //                                            console.log(position);

                                                        if (type!==null){
                                                            $.post('/tutor/bookings/majoroutcomelist/' + type + '/' + studentID + '/' + clientID + '/' + position + '/' + passLessonId, function (data) {
                                                                listing.html(data);
                                                            });
                                                        }

                                                    });

                                                </script>
                                                
                                                
                                                
                                        <?php endif; ?>

                                    </td>
                                    <td>
                                        <div id="assmntRefference<?= $lessonInfo->id ?>" class="col-md-12">

                                        </div>
                                    </td>
                                    <td>
                                        <div id="assmntTopic<?= $lessonInfo->id ?>" class="col-md-12">

                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </table>
                            
                        </td>
                    </tr>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>

            </tbody>
        </table>
        <?php //endif; ?>
    </div>
    <div class="col-lg-12" style="padding-bottom: 25px;">
        <div class="col-lg-2">
            <i class="glyphicon glyphicon-info-sign"></i> Note: All <strong>dates</strong> should be in dd/mm/yyyy format.
        </div>
        <button type="submit" class="btn btn-primary pull-right top-buffer" >Update Lesson</button>
                
    </div>
    
    <?php echo $this->Form->end(); ?>
</div>
<div id="lead-view-modal" class="modal fade" role="dialog"></div>
<script>
    $(document).ready(function () {
        
        $('#unallocatedStudents').dataTable({"autoWidth": false});
        $('#newLeadsTable').dataTable({"autoWidth": false});
        $('.pending').dataTable({
            "autoWidth": false,
            "searching": false,
            "bInfo": false,
            "paging": false
        });

        $('.lessonDate').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy',
            maxDate: "7m"
        });

        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        });
    });
</script>
<?php // debug($lessonDetails ->toArray()); ?>