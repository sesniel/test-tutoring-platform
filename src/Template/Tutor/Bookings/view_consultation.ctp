<div id="edit-consultation" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Consultation Info:</h4>
            </div>
            <div class="modal-body row">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">Please print the <strong><a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/others/Consultation Run Sheet.pdf' ?>" target="_blank">Consultation Run Sheet</a></strong> and take to your initial session along with the student's results reports.</p>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 center-indent">
                        <h4>Client Details:</h4>
                    </div>
                </div>
                <?php $x = 1; foreach ($leads as $info): if($x == 1){ ?>
                    <div class="row center-view">
                        <div class="col-md-6">
                            <div class="col-md-5"><h5><strong>Name:</strong></h5></div>
                            <div class="col-md-7"><h5><?= $info->client->first_name . ' ' . $info->client->last_name ?></h5></div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-3"><h5><strong>Email:</strong></h5></div>
                            <div class="col-md-9"><h5><?= $info->client->email ?></h5></div>
                        </div>
                    </div>
                    <div class="row center-view">
                        <div class="col-md-6">
                            <div class="col-md-5"><h5><strong>Mobile:</strong></h5></div>
                            <div class="col-md-7"><h5><?= ($info->client->mobile) ? $info->client->mobile : '- - -'; ?></h5></div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-3"><h5><strong>Phone:</strong></h5></div>
                            <div class="col-md-9"><h5><?= ($info->client->phone) ? $info->client->phone : '- - -'; ?></h5></div>
                        </div>
                    </div>
                    <div class="row center-view">
                        <div class="col-md-12">
                            <div class="col-md-2"><h5><strong>Address:</strong></h5></div>
                            <div class="col-md-10"><h5><?= $info->client->street_number . ' ' . $info->client->street_name . ', ' . $info->client->suburb ?></h5></div>
                        </div>
                    </div>
                <?php $x++; } endforeach; ?>
                <div class="row">
                    <div class="col-md-12 center-indent">
                        <p>Click Assessment button to Add/Delete an Assessment.</p>
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="center-content">
                        <table id="studentTbl" class="responsive display table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Student</th>
                                    <th>Year</th>
                                    <th>Assessments</th>
                                    <th>Subject/s</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($leads as $lead): ?>
                                    <tr>
                                        <td><?= $lead->student->name ?></td>
                                        <td><?= $lead->student->year_level->name ?></td>
                                        <td>
                                            <?php $ctr = 0; $total = 0; foreach ($assessments as $assessment){ if($assessment->student_id == $lead->student_id){
                                                $total += 1;
                                                if($assessment->status == 'Done'){
                                                    $ctr += 1;
                                                }
                                            }} echo $ctr.'/'.$total; ?>
                                        </td>
                                        <td> 
                                            <?php
                                                $subj = array();
                                                foreach ($lead->lead_subjects as $subjects) {
                                                    $subj[] = $subjects->subject->name;
                                                }
                                            echo implode(", ", $subj); ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <?= $this->Form->button(
                                                        'Assessments', ['type' => 'button', 'class' => 'btn btn-sm btn-success assessmentDetails', 'value' => $lead->student->id]);
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.assessmentDetails').click( function () {
		var id = $(this).val(),
		assessmentDialogue = $("#assessment-details-dialogue");

		$.post('/tutor/dashboard/assessments-view', {"id": id}, function (data) {
			assessmentDialogue.html(data);
			$('#assessment-details').modal('show');
		});

	});
    });
</script>