<style>
.hidden_studentInfo {
     display:none;
     padding-top:10px;
}
#updateButton{
    display:none;
}
.lessonDate1{z-index:1151 !important;}
</style>
<?php $date = date('Ymd'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
            <h2><strong>Lesson Details for <?= $programDetails->client->first_name . " " . $programDetails->client->last_name ?></strong></h2>
            </div>
            
           
            <hr class="customHr">
            <div class="widget-container">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" value="<?= $programDetails->id ?>" name="program_id" />
                        <table id="lessonTable" class="table table-striped responsive display table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Length</th>
                                    <th>Status</th>
                                     <th>Student Information</th>
                                </tr>
                            </thead>
                            <tbody>       
                                
                                    <tr>
                                        
                                        <?php 
                                            foreach ($programDetails->lessons as $key => $lesson): 

                                            $testThis = date('Ymd', strtotime($lesson->date));
                                        ?>  
                                        <td><?php echo date('Y/m/d', strtotime($lesson->date))  ?></td>
                                        <td><?php echo date('h:i A', strtotime($lesson->time))  ?></td>
                                        <td><?= $lesson->length ?></td>
                                        <td><?php  echo $lesson->status ?></td>
                                        <td colspan="5">
                                        <button class="button btn-xs btn-primary" onclick="$('#myContent<?= $key ?>').slideToggle();">
                                            View Lesson Details
                                        </button>
                                                
                                        <?php
                                        if($lesson->status != "Completed" && $this->request->session()->read("type") ):
                                            echo $this->Html->link(
                                                "edit",
                                                array('action' => 'editDetails', $programDetails->id, $lesson->id),
                                                ['class' => 'btn btn-success btn-xs', 'type' => 'button']
                                            );
                                        endif;
                                        ?>
                                            
                                        <?php 
                                            if($lesson->status != "Completed" && $this->request->session()->read("type")){ 
                                                if($testThis <= $date):
                                        ?>
                                            <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal<?= $key ?>">Add session report</button>
                                            <?php endif; ?>
                                              
                                        <div>

                                    <?php
                                    echo $this->Form->create(null, [
                                        'url' => ['action' => 'saveSeessionReport'],
                                        'method' => 'post',
                                        'name' => 'formDelete'
                                        ]);
                                    
                                        ?>
                                        <input type="hidden" name="lessonId" value="<?= $lesson->id ?>" />
                                        
                                        <div id="myModal<?= $key ?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Add New Report</h4>
                                                        Please add your lesson notes in the field below, please ensure you cover major topics such as any specific issues e.g. Homework status from last week, homework for next week. These notes will be visible to both parents, tutors and admin.<br><br>
                                                        <strong><i class="glyphicon glyphicon-info-sign"></i>   NOTE: DO NOT SUBMIT SESSION REPORT IF LESSON WAS CANCELLED </strong>
                                                    </div>
                                                    
                                                    <!-- -->
                                                    <div class="modal-body">  
                                                     <div class="row">   
                                                        <div class="col-md-4">
                                                            <h5><strong>Date</strong></h5>
                                                             <input type="hidden" name="lesson_id[]" value="<?= $lesson->id ?>" />
                                                                <div class="input-group"><input class="form-control lessonDate1" id="lessonDate1<?= $key ?>" type="text" value="<?php echo date('d/m/Y', strtotime($lesson->date))  ?>" name="lessonDate" required="">
                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Time</strong></h5>
                                                                <div class="input-group"><input class="form-control lessonTime" id="lessonTime<?= $key ?>"
                                                                type="text" name="lessonTime" value="<?php echo date('h:i A', strtotime($lesson->time))  ?>" required=""><span
                                                                class="input-group-addon"><i
                                                                class="glyphicon glyphicon-time"></i></span></div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <h5><strong>Length</strong></h5>
                                                            <h5><?= $lesson->length ?></h5>
                                                        </div> 
                                                     </div>
                                                    </div>

                                                    <!-- -->
                                                    <div class="modal-body">
                                                        <textarea rows="5" class="form-control" name="note" placeholder="Report here" required="" ></textarea>

                                                    </div>
                                                    <div class='modal-body'>
                                                        <input type="checkbox" id="bookingVariation" class="enable_textarea pull-right" name="box" style='position:relative; top:-15px; '> <span class='pull-right' style='position:relative; top:-15px; left:-10px;' >Request Booking Variation </span>

                                                            <textarea rows="5" class="form-control show_textarea" name="reason" placeholder="Place your reason here."  ></textarea>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <input type="submit" class="btn btn-primary" name="submitted"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } else {
                                            //echo htmlspecialchars($lesson->note);
                                        }  ?>
                                        <?php echo $this->Form->end(); ?>
                                                    <div id="myModal<?= $key ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Add New Report</h4>
                                                                    Please add your lesson notes in the field below, please ensure you cover major topics such as any specific issues e.g. Homework status from last week, homework for next week. These notes will be visible to both parents, tutors and admin
                                                                </div>
                                                                
                                                                <div class="modal-body">
                                                                    <textarea rows="5" class="form-control" name="note" placeholder="Report here" required="" ></textarea>
                                                               
                                                                </div>
                                                                <div class='modal-body'>
                                                                    <input type="checkbox" id="bookingVariation" class="enable_textarea pull-right" name="box" style='position:relative; top:-15px; ' /> <span class='pull-right' style='position:relative; top:-15px; left:-10px;' >Request Booking Variation </span>
                                                                        <textarea rows="5" class="form-control show_textarea" name="reason" placeholder="Place your reason here."  ></textarea>
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <input type="submit" class="btn btn-primary" name="Save changes" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                    <?php echo $this->Form->end(); ?>
                                        </div>      
                                                    
                                                                                      
                                            
                                        <div id="myContent<?= $key ?>" class='hidden_studentInfo'>
                                            <?php if(htmlspecialchars($lesson->note) != ""): ?>
                                            <?php
                                            echo $this->Form->create(null, [
                                                'url' => ['action' => 'updateSeessionReport',$lesson->id],
                                                'method' => 'post',
                                                'name' => 'formDelete'
                                                ]);
                                                ?>
                                            <table class="table">
                                                <tr><td>Session Report:</td></tr>
                                                <tr>
                                                    <td>
                                                        <textarea rows="5" class="form-control" name="note" placeholder="Place your report here." id="textUpdate" ><?php echo htmlspecialchars($lesson->note); ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><input type="submit" value="Update Comment" class="btn btn-xs btn-primary pull-right" id="updateButton"></td>
                                                </tr>
                                            </table>
                                            <?php echo $this->Form->end(); ?>
                                            <?php endif; ?>
                                            
                                            <table class="table">
                                                <tr>
                                                    <th>Student</th>
                                                    <th>Type</th>
                                                    <th>Subject</th>
                                                    <th>Type</th>
                                                    <th>Resources</th>
                                                </tr>
                                            <?php foreach($lesson->lesson_students as $studentsInfo): ?>
                                                
                                                <tr>
                                                    <td><?= $studentsInfo->program_student->student->name ?></td>
                                                    <td>
                                                        <?php 
                                                            if(!empty($studentsInfo->assessment_type)):
                                                                echo $studentsInfo->assessment_type->type;
                                                            else:    
                                                                echo "General";
                                                            endif;                                                                        
                                                        ?>
                                                    </td>
                                                    <td>                                                    
                                                        <?php if(isset($studentsInfo->major_outcome)): ?>
                                                            <?= $studentsInfo->major_outcome->name ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>                                                    
                                                        <?php if(isset($studentsInfo->minor_outcome) && !empty($studentsInfo->assessment_type)): ?>
                                                            <?= $studentsInfo->minor_outcome->code ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="pull-right">                                        
                                                        <?php
                                                        
                                                        if(isset($studentsInfo->lesson_resources) && isset($studentsInfo->lesson_resources)):

                                                            foreach($studentsInfo->lesson_resources as $resource):

                                                                ?>

                                                            <a href="<?= $this->request->webroot . 'web/viewer.html?file=resources/'. $studentsInfo->major_outcome->year_level->name . '/' . $studentsInfo->major_outcome->subject->name . '/' . $resource->resource->pdf ?>"
                                                               target='_blank' class='btn btn-xs btn-default'><?php echo $resource->resource->pdf; ?></a>
                                                               <br/>
                                                        <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </td>

                                                </tr>
                                                
                                            <?php endforeach; ?>
                                            </table>
                                        </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                <script>
                                                                            
                                $('.lessonDate1').datepicker({
                            //        minDate: 0,
                                    dateFormat: 'dd/mm/yy'

                                });

                                </script>
                                    </tbody>
                                </table>
                            </div>
                        </div>
               
                    </div>
                </div>
            </div>
        </div>
        <div id="variationInfo" style="display: none;">Variation requests are for when the length of sessions are not in accordance with the booking, do you wish to proceed?</div>
        <div id="lead-view-modal" class="modal fade" role="dialog"></div>
        
        <script>
            $(document).ready(function () {
                
                $('#formDelete').submit(function(e){
                        e.preventDefault();
                        $('#dialog').dialog('open');
                    });
                    $('#dialog').dialog({
                    autoOpen: false,
                    modal: true,
                    buttons: {
                            "Confirm": function() {
                                $('#formDelete').submit();  
                            },
                            "Cancel": function() {
                                $(this).dialog("close");
                            }
                    }
                }); 

//                $('#bookingTable').dataTable({"autoWidth": false});
//                $('#lessonTable').dataTable({"autoWidth": false});
//                $('#newLeadsTable').dataTable({"autoWidth": false});
//                $('[data-toggle="popover"]').popover(); 
                
                $('input.enable_textarea').change(function(){
                    if ($(this).is(':checked')){ 
                        $(this).parent().children('textarea.show_textarea').attr('required', "true").show();    
                        alert("Reminder: Variation requests are for when the length of sessions are not in accordance with the booking, do you wish to proceed?");
                    }
                    else{ 
                        $(this).parent().children('textarea.show_textarea').removeAttr('required').hide(); 
                    }
                }).change();                

            });
            
            $("#textUpdate").keydown(function(){
                var value = $(this).val();
                if (value && value.length > 0) {
                    // Exist text in your input
                    $("#updateButton").show();
                } else {
                    $("#updateButton").hide();
                }
            });
            
            function toggler(divId) {
                $("#" + divId).toggle();
            }
            
            function variation(){
                confirm("Either OK or Cancel.");
            }
            
            
           $(document).ready(function () {
        
        $('#unallocatedStudents').dataTable({"autoWidth": false});
        $('#newLeadsTable').dataTable({"autoWidth": false});

        $('.lessonDate1').datepicker({
    //        minDate: 0,
            dateFormat: 'dd/mm/yy'
            
        });

        $('.lessonTime').timepicker({

            'minTime': '12:00pm',
            'maxTime': '11:30am'

        });
    });
        </script>
        <?php 
//debug($lesson->toArray()); 

//                        debug($this->request);
        ?>
