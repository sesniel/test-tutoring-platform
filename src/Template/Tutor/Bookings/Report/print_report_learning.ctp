<?php
    $majorDetailsTemp = array();
    foreach($userQuestionInfoDetails as $major):
        $majorDetails[$major->major_outcome->id] = $major->major_outcome->name;
    endforeach;
    $userQuestionInfo = array();
    
    
    foreach($userQuestionInfoDetails as $userQuestionDetail):
               
        
        foreach($majorDetails as $keyM => $majorInfo):
            if($keyM === $userQuestionDetail->major_outcome->id):
                
                if(isset($userQuestionInfo[$keyM]['correct'])):
                    $userQuestionInfo[$keyM]['correct'] += getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
                else:
                    $userQuestionInfo[$keyM]['correct'] = getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
                endif;
                
                
                if(isset($userQuestionInfo[$keyM]['total-value'])):
                    $userQuestionInfo[$keyM]['total-value'] += getHighestMarkValue($userQuestionDetail->mark);
                else:
                    $userQuestionInfo[$keyM]['total-value'] = getHighestMarkValue($userQuestionDetail->mark);
                endif;
                
                
                
                if(!isset($userQuestionInfo[$keyM]['majoroutcome_name'])):
                    $userQuestionInfo[$keyM]['majoroutcome_name'] = $majorInfo;
                endif;
                
                if(!isset($userQuestionInfo[$keyM]['majoroutcome_name_description'])):
                    $userQuestionInfo[$keyM]['majoroutcome_name_description'] = $userQuestionDetail->major_outcome->description;
                endif;
            endif;
        endforeach;
        
        
    endforeach;
    
//    debug($userQuestionInfo);
    
    
    function getMarkValue($user_answer, $choices, $mark){
        
        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue  = json_decode($mark, TRUE);
        $choiceKey  = array_search($userAnswer,$choicesVal);
        
        return $markValue[$choiceKey];
        
    }
    
    function getHighestMarkValue($mark){
        $markValue  = json_decode($mark, TRUE);
        return max($markValue);
    }
    
        
    
?>
<style>
    .container{width:800px !important;}
</style>
<script type="text/javascript">
$(function () {
    
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo $studentAssessmentDetails->assessment_type->type ?>'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent of the assessment'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            
                <?Php 
                    $valueInPercentage = array();
                    $totalValue = 0;
                    foreach($userQuestionInfo as $key => $qInfo): 
                        $pval = round(($qInfo['correct'] / $qInfo['total-value']) * 100 , 0);
                        $valueInPercentage[$key] = $pval;
                        $totalValue += $pval;
                ?>
                    {
                        name: "<?= $qInfo['majoroutcome_name'] ?>",
                        y: <?php echo $pval ?>,
                        drilldown: null
                    },
                <?php 
                    endforeach; 
                    $percentageValue = $totalValue / count($valueInPercentage);
                ?>
            
            ]
        }]
    });
});

</script> 

<?php
    $x = 1; 
    $percValue = array();
    foreach($valueInPercentage as $keyP => $percent):
        
        if($x <= 2):
            
            $percValue[$keyP] = $percent;
            $x++;
        else:
            
            foreach($percValue as $keyV => $percVal):
            
                if($percent <= $percVal):
                    
                    unset($percValue[$keyV]);
                    $percValue[$keyP] = $percent;
                    break;
                    
                endif;
            
            endforeach;
        
        
        endif;
        
    endforeach;
    
?>

<div class="row"> <!-- Assessments Tables -->
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div id="printableArea">
            <div class="col-md-12">
                <h1><?= $studentAssessmentDetails->student->name ?></h1>
                <h4>Year Level: <?= $studentAssessmentDetails->year_level->name ?></h4>
                <h4>Completed Date: <?php echo date("d/m/Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
            </div>
            
            <div class="widget-container table-responsive">
                
                <div id="container" style="min-width: 110px; height: 400px; margin: 0 auto"></div>
                <?Php if($studentAssessmentDetails->assessment_type->type == "Learning Style"): ?>
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        <h2>Overview Learning Style</h2>

                        <p>While students are offered a wealth of opportunities at school, unfortunately, they are rarely taught how to learn. As a result, students are often unable to absorb new information efficiently and effectively, develop an understanding of new concepts, recall facts and formulas and much more.</p>

                        <p>This assessment was created to improve the self-awareness of a student’s learning styles and develop long-term skills and strategies to achieve efficient learning habits and improve results. Strategies that work effectively for one student could be completely ineffective for the next depending on the students learning style.</p>

                        <p>There are eight different learning styles Logical, Linguistic, Spatial, Kinaesthetic, Musical, Interpersonal, and Intrapersonal. It is important to identify a student’s learning style to adopt the most effective learning methodologies.</p>

                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        

                    <p>Results indicated that <strong><?= $studentAssessmentDetails->student->name ?></strong> had a tendency towards
                        <strong> <u>
                        <?php 
                            $x = 1; 
                            foreach($percValue as $keyPVal => $pVal): 
                                if($x == 1):
                                    $x++;
                                    echo $userQuestionInfo[$keyPVal]['majoroutcome_name'];
                                else:
                                    echo " and " . $userQuestionInfo[$keyPVal]['majoroutcome_name'];
                                endif;
                            endforeach; 
                        ?>
                        </u></strong> 
                        
                        learning styles.</p>

                    </div>
                </div>
                
                <div class="row">
                    <?php 
                        $x = 1; 
                        foreach($percValue as $keyPVal => $pVal): 
                            if($x == 1):
                                $x++;
                                echo '<div class="col-md-12"><u><h4>' . $userQuestionInfo[$keyPVal]['majoroutcome_name'] . '</h4></u></div>';
                                echo '<div class="col-md-12">' . $userQuestionInfo[$keyPVal]['majoroutcome_name_description'] . '</div>';
                            else:
                                echo '<div class="col-md-12"><u><h4>' . $userQuestionInfo[$keyPVal]['majoroutcome_name'] . '</h4></u></div>';
                                echo '<div class="col-md-12">' . $userQuestionInfo[$keyPVal]['majoroutcome_name_description'] . '</div>';
                            endif;
                        endforeach; 
                    ?>
                </div>
                <?php else: ?>
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        <h2>Overview Learning IQ</h2>

                        <p>While students are offered a wealth of opportunities at school, unfortunately, students are rarely taught how to learn. This means students are often unable to absorb new information efficiently and effectively, develop an understanding of new concepts, recall facts and formulas and much more.</p>

                        <p>This assessment has been created to develop long-term skills and strategies to achieve efficient learning habits and improve results. Strategies that work effectively for one student could be completely ineffective for the next depending on the students learning style.</p>

                        <p>The Learning IQ is a proprietary test created by Tutor2You to assess students understanding of a broad range of study skills and strategies.</p>

                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        <u><b><?= $studentAssessmentDetails->student->name ?></b></u> achieved a learning IQ score of <?php echo round($percentageValue, 2); ?> %.
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        
                        <?php 
                        
                        $iqStrength = array();
                        $iqWeekness = array();
                        
                        foreach($valueInPercentage as $key => $qInfo):  
                            
                        
                            if ($qInfo >= 80):

                                $iqStrength[] = $userQuestionInfo[$key]['majoroutcome_name'];

                            elseif ($qInfo <= 50):

                                $iqWeekness[] = $userQuestionInfo[$key]['majoroutcome_name'];

                            endif;
                        
                        endforeach; 
                        
                        if (!empty($iqStrength)):
                            echo "<h3><u>Strengths</u></h3>";
                            foreach ($iqStrength as $strength):

                                echo "<strong>" . $strength . "</strong><br/>";

                            endforeach;

                        endif;

                        if (!empty($iqWeekness)):
                            echo "<h3><u>Weaknesses</u></h3>";
                            foreach ($iqWeekness as $weekness):

                                echo "<strong>" . $weekness . "</strong><br/>";

                            endforeach;

                        endif;
                        
                        ?>
                        
                    </div>
                </div>                
                
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        <br/>
                        <h1>Result</h1>
                        <p>The Learning IQ test assesses students against a broad range of study techniques and strategies
                            across 54 questions. Question have been weighed according to the skills correlation with
                            improving results. A student who makes use of 75% of the most common learning techniques may
                            still score low if they do not utilise the specific strategies most closely linked with
                            improving results. </p>
                    </div>
                </div>
                        
                <?php endif; ?>
                
                
                
                </div>
                
            </div>

            <button class="btn btn-large btn-primary pull-right top-buffer" onclick="printDiv('printableArea')">
                Print Report
            </button>

                <script>
                    function printDiv(divName) {
                        var printContents = document.getElementById(divName).innerHTML;
                        var originalContents = document.body.innerHTML;

                        document.body.innerHTML = printContents;

                        window.print();

                        document.body.innerHTML = originalContents;
                    }
                </script>
            
        </div>
        

    </div>
</div>
<?php 
//    debug($iqStrength);
//    debug($iqWeekness); 
?>