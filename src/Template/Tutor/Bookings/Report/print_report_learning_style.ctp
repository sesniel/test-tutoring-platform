<?php
    $majorDetailsTemp = array();
    foreach($userQuestionInfoDetails as $major):
        $majorDetails[$major->major_outcome->id] = $major->major_outcome->name;
    endforeach;
    $userQuestionInfo = array();
    
    
    foreach($userQuestionInfoDetails as $userQuestionDetail):
               
        
        foreach($majorDetails as $keyM => $majorInfo):
            if($keyM === $userQuestionDetail->major_outcome->id):
                
//                if(isset($userQuestionInfo[$keyM]['correct'])):
//                    $userQuestionInfo[$keyM]['correct'] += getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
//                else:
//                    $userQuestionInfo[$keyM]['correct'] = getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
//                endif;
//                
//                
//                if(isset($userQuestionInfo[$keyM]['total-value'])):
//                    $userQuestionInfo[$keyM]['total-value'] += getHighestMarkValue($userQuestionDetail->mark);
//                else:
//                    $userQuestionInfo[$keyM]['total-value'] = getHighestMarkValue($userQuestionDetail->mark);
//                endif;
                
                
                
                if(!isset($userQuestionInfo[$keyM]['majoroutcome_name'])):
                    $userQuestionInfo[$keyM]['majoroutcome_name'] = $majorInfo;
                endif;
                
                if(!isset($userQuestionInfo[$keyM]['majoroutcome_name_description'])):
                    $userQuestionInfo[$keyM]['majoroutcome_name_description'] = $userQuestionDetail->major_outcome->description;
                endif;
            endif;
        endforeach;
        
        
    endforeach;
    

$totalMarkValue = 0;

foreach ($userQuestionInfoDetails as $userQuestionDetail):
    
    if (isset($majorDetailsInfo[$userQuestionDetail->major_outcome->id]['correct'])):
        $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['correct'] += getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
    else:
        $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['correct'] = getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
    endif;

    if (isset($majorDetailsInfo[$userQuestionDetail->major_outcome->id]['total-value'])):
        $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['total-value'] += getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    else:
        $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['total-value'] = getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    endif;

    if (!isset($majorDetailsInfo[$userQuestionDetail->major_outcome->id]['majoroutcome_name'])):
        $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['majoroutcome_name'] = $userQuestionDetail->major_outcome->name;
    endif;
    
    $totalMarkValue += getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    
    $majorDetailsInfo[$userQuestionDetail->major_outcome->id]['Details'][] = $userQuestionDetail;


endforeach;

//debug($totalMarkValue);
//debug($majorDetailsInfo);
//exit;

function getMarkValue($user_answer, $choices, $mark)
{
    //#skipped
    if($user_answer != "#skipped"):

        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue = json_decode($mark, TRUE);
        $choiceKey = array_search($userAnswer, $choicesVal);

        return $markValue[$choiceKey];
    else:
        
        return 0;
        
    endif;

}

function getHighestMarkValue($mark, $user_answer)
{
    if($user_answer != "#skipped"):
        
        $markValue = json_decode($mark, TRUE);
        return max($markValue);
    else:
        
        return 0;

    endif;
}
        
    
?>
<style>
    .container{width:800px !important;}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Learning Style'
        },
//        subtitle: {
//            text: 'Click the slices to view versions. Source: netmarketshare.com.'
//        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [{
                
            name: 'Details',
            colorByPoint: true,
            
            
            
            data: [
                <?php 
                    $valueInPercentage = array();
                    $totalValue = 0;
                    foreach($majorDetailsInfo as $key => $md):
                        
                        if($md['correct'] > 0):
                            $pval = ($md['correct'] / $totalMarkValue) * 100;
                        else:
                            $pval = 0;
                        endif;
                        $valueInPercentage[$key] = $pval;
                        $totalValue += $pval;
                ?>
                    {
                        name: '<?= $md['majoroutcome_name'] ?>',
                        y: <?= $pval ?>,
                        drilldown: null
                    }, 
                <?php 
                    endforeach; 
                
                    $percentageValue = $totalValue / count($valueInPercentage);    
                ?>
            
        
            ]
        }]
    });
});
</script>

<?php
    arsort($valueInPercentage);
    $x = 1; 
    $percValue = array();
    foreach($valueInPercentage as $keyP => $percent):
        
        if($x <= 2):
            
            $percValue[$keyP] = $percent;
            $x++;
        
        endif;
        
    endforeach;
    
?>

<div class="row"> <!-- Assessments Tables -->
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div id="printableArea" style='    width: 750px; margin:auto;'>
            <div class="col-md-12">
                <h1><?= $studentAssessmentDetails->student->name ?></h1>
                <h4>Year Level: <?= $studentAssessmentDetails->year_level->name ?></h4>
                <h4>Completed Date: <?php echo date("d/m/Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
            </div>
            
            <div class="widget-container table-responsive">
                
                <div id="container" style="min-width: 110px; height: 400px; width:500px; margin: 0 auto"></div>
                <?Php if($studentAssessmentDetails->assessment_type->type == "Learning Style"): ?>
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        <h2>Overview Learning Style</h2>

                        <p>While students are offered a wealth of opportunities at school, unfortunately, they are rarely taught how to learn. As a result, students are often unable to absorb new information efficiently and effectively, develop an understanding of new concepts, recall facts and formulas and much more.</p>

                        <p>This assessment was created to improve the self-awareness of a student’s learning styles and develop long-term skills and strategies to achieve efficient learning habits and improve results. Strategies that work effectively for one student could be completely ineffective for the next depending on the students learning style.</p>

                        <p>There are eight different learning styles Logical, Linguistic, Spatial, Kinaesthetic, Musical, Interpersonal, and Intrapersonal. It is important to identify a student’s learning style to adopt the most effective learning methodologies.</p>

                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12" id="generatePdfOverview">
                        

                    <p>Results indicated that <strong><?= $studentAssessmentDetails->student->name ?></strong> had a tendency towards
                        <strong> <u>
                        <?php 
                            $x = 1; 
                            foreach($percValue as $keyPVal => $pVal): 
                                if($x == 1):
                                    $x++;
                                    echo $userQuestionInfo[$keyPVal]['majoroutcome_name'];
                                else:
                                    echo " and " . $userQuestionInfo[$keyPVal]['majoroutcome_name'];
                                endif;
                            endforeach; 
                        ?>
                        </u></strong> 
                        
                        learning styles.</p>

                    </div>
                </div>
                
                <div class="row">
                    <?php 
                        $x = 1; 
                        foreach($percValue as $keyPVal => $pVal): 
                            if($x == 1):
                                $x++;
                                echo '<div class="col-md-12"><u><h4>' . $userQuestionInfo[$keyPVal]['majoroutcome_name'] . '</h4></u></div>';
                                echo '<div class="col-md-12">' . $userQuestionInfo[$keyPVal]['majoroutcome_name_description'] . '</div>';
                            else:
                                echo '<div class="col-md-12"><u><h4>' . $userQuestionInfo[$keyPVal]['majoroutcome_name'] . '</h4></u></div>';
                                echo '<div class="col-md-12">' . $userQuestionInfo[$keyPVal]['majoroutcome_name_description'] . '</div>';
                            endif;
                        endforeach; 
                    ?>
                </div>
                        
                <?php endif; ?>
                
                
                
                </div>
                
            </div>

            <button class="btn btn-large btn-primary pull-right top-buffer" onclick="printDiv('printableArea')">
                Print Report
            </button>

                <script>
                    function printDiv(divName) {
                        var printContents = document.getElementById(divName).innerHTML;
                        var originalContents = document.body.innerHTML;

                        document.body.innerHTML = printContents;

                        window.print();

                        document.body.innerHTML = originalContents;
                    }
                </script>
            
        </div>
        

    </div>
</div>
<?php 
//    debug($iqStrength);
//    debug($iqWeekness); 
?>