<?php
    function getYearLevelInfo($yearLevelId){
        if($yearLevelId == 1):
            return "F";
        else:
            return $yearLevelId - 1;
        endif;
    }
//    debug($programDetails->toArray());exit;
    $programStatus = array('Active', 'Completed');
    
?>
<div class="widget-box container">
    

    <div class="row">
        <div class="col-sm-6 col-md-6">
        <div class="widget-title">
            <h2><strong>Booking Information</strong></h2>
        </div>
        </div>
        <div class="col-sm-3 col-md-3"></div>
        <div class="col-sm-3 col-md-3 statusPos">
            <!-- Status start -->
            <?php
                echo $this->Form->create(null, [
                    'url' => ['action' => 'table'],
                    'method' => 'post'
                ]);
            ?>
            Status: 
            <select name="status" class="form-control" onchange="this.form.submit()" >
                <?php foreach($programStatus as $stat): ?>
                    <option <?php $sel = ($status === $stat ? 'selected' : ''); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo $this->Form->end(); ?>
            
             <!-- Status end -->
            
        </div>
    </div>
    
    <hr class="customHr"><br/>
    
    <div class="row">
    <div class="col-sm-12 col-md-12">
        
        <table id="bookingStudents"
               class="table table-striped responsive display table-hover table-bordered">
            <thead>
            <tr>
                <th>Client</th>
                <th>Students</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($programDetails as $prog): ?>
                    <tr>
                        <td><?= $prog->client->first_name . " " . $prog->client->last_name  ?></td>
                        <td><?php
                            if($prog->program_students):
                                $countStudents = count($prog->program_students) - 1;
                                foreach($prog->program_students as $key => $stdnt):
                                    if($countStudents == $key):
                                        echo $stdnt->student->name;
                                    else:    
                                        echo $stdnt->student->name . ", ";
                                    endif;
                                endforeach;
                            endif;
                        ?></td>
                        <td>
                            <?php 
                                if($prog->status == "Tutor"):
                                    echo "Pending";   
                                else:
                                    echo $prog->status;                                  
                                endif;
                            ?>
                        </td>
                        <td>
                            <?php 
                                
                                if($prog->status == "Tutor" || $prog->status == "Pending"):
                                    echo $this->Html->link(
                                                "Complete Lesson Plan",
                                                ['action' => 'details', $prog->id, 'prefix' => 'tutor'], ['escape' => false, 'class' => "btn btn-xs btn-warning pickLesson"]); 
                                elseif($prog->status == "Pending"):
                                    
                                else:
                                    echo $this->Html->link(
                                                "View Booking",
                                                ['action' => 'view', $prog->id, 'prefix' => 'tutor'], ['escape' => false, 'class' => "btn btn-xs btn-primary"]); 
                                endif;
                            
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    </div>
    
    <!-- Completed Consultations Table -->
    <div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-title">
            <h2><strong>Recent Consultations</strong></h2>
        </div>
        <hr>
        <table id="bookingStudents"
               class="table table-striped responsive display table-hover table-bordered consultationTable">
            <thead>
            <tr>
                <th>Client</th>
                <th>Consultation Date</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($consultationDetails as $consultation): ?>
                    <tr>
                        <td><?= $consultation->leads[0]->client->first_name . ' ' . $consultation->leads[0]->client->last_name ?></td>
                        <td><?php echo date('Y/m/d h:i A', strtotime($consultation->consultation_session_date)); ?></td>
                        <td><?= $consultation->status ?></td>
                        <td>
                            <div class="btn-group">
                                <?php

//                                echo $this->Form->button(
//                                        'Details', ['type' => 'button', 'class' => 'btn btn-sm btn-info consultationDetail', 'value' => $consultation->id]);
                                if($consultation->status == "Pending Consultation"){
                                echo $this->Html->link(
                                        "Edit", array('controller' => 'dashboard', 'action' => 'editConsultation', $consultation->id), ['class' => 'btn btn-sm btn-success', 'target' => '_blank']);
                                }
                                echo $this->Form->button(
                                        'View details', ['type' => 'button', 'class' => 'btn btn-sm btn-warning consultationView', 'value' => $consultation->id]);
                                ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    </div>
    <?php // debug($programDetails->toArray()) ?>
</div>
<div id="lead-view-modal" class="modal fade" role="dialog"></div>
<div id="client-view-dialogues"></div>
<div id="consulation-edit-details"></div>
<div id="assessment-details-dialogue"></div>
<div id="consultation-dialogue"></div>
<script>
    $(document).ready(function () {

        $('#bookingStudents').dataTable({"autoWidth": false});
        
        var hash = window.location.hash;

        switch (hash) {
            case '#existing-booking':
                $('#response-dialog').modal('show');
                $('#response-dialog .modal-title').text('Info');
                $('#response-dialog .modal-body p').text('Please complete existing booking before activating a new one.');
                break;
        }

    });
    
   
</script>
<?php // debug($programDetails->toArray()); ?>
<?= $this->Html->script('tutor/dashboard3.js'); ?>
<?= $this->Html->script('jquery.cookie.js'); ?>