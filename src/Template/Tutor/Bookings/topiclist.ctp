<?php

    $minorOutcome = array();
    foreach($majorOutcomeDetails['minor_outcomes'] as $key => $minor):
        
        $correct = 0;
        $wrong   = 0;
        $minorOutcome[$minor->id]['code'] = $minor->code;
        $minorOutcome[$minor->id]['id'] = $minor->id;
        
        foreach($minor['user_questions'] as $question):
        
            if($question->correct_answer === $question->user_answer):
                $correct += 1;
            else:
                $wrong += 1;
            endif;
        
        endforeach;
        
        $minorOutcome[$minor->id]['correct'] = $correct;
        $minorOutcome[$minor->id]['wrong'] = $wrong;
        if(($correct + $wrong) == 0):
        $minorOutcome[$minor->id]['average'] = 0;
        else:
        $minorOutcome[$minor->id]['average'] = round(($correct / ($correct + $wrong)) * 100,2);
        endif;
        
    endforeach;
    
    usort($minorOutcome, "sortByAverage");

    function sortByAverage($a, $b) { // or playcount or whatever
        if($a["average"] == $b["average"]) {
            return 0;
        }
        return ($a["average"] < $b["average"]) ? -1 : 1;
    }
?>
<select name="info[<?= $passLessonId ?>][minoroutcome]" class="form-control" required>
    
    <option disabled="" selected="" placeholder="Select Type">Choose Outcome</option>
    <option value="0" >General</option>
    <?php foreach($minorOutcome as $keyMinor => $minor): ?>
        <?Php if(($minor['correct'] + $minor['wrong']) != 0): ?>
        <option value="<?= $minor['id'] ?>" ><?= $minor['code'] ?>, <?= $minor['average'] ?> %</option>
        <?php endif; ?>
    <?Php endforeach; ?>
    
</select>

<?php
//    debug($minorOutcome);
?>