<div class="container">
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            
            <div class="widget-title">
                <h2><strong>Tutor Application</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
             
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Contact Details:</h4>
                        <p>Please complete the application form.</p>
                        <hr>
                    </div>
                </div>
              <form enctype="multipart/form-data" action="applications/submitApplication" method="POST">
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-4">
                        <div class="row top-buffer">
                           <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">First Name:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="fname"
                                       title="First Name" 
                                       value="" required><br>
                           </div>
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Last Name:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="lname"
                                       title="Last Name" 
                                       value="" required><br>
                            </div>
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Mobile:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="mobile"
                                       title="Mobile" 
                                       value="" required><br>
                           </div>
                            <div class="col-md-12">
                                <label for="street-name">Alternative Mobile:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="a_mobile"
                                       title="Alternative Mobile" 
                                       value=""><br>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row top-buffer">
                           <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Email:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="email"
                                       title="Email" 
                                       value="" required><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                                <label for="street-name">Unit/Street Number:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="street_number"
                                       title="Street Number" 
                                       value=""><br>
                           </div>
                        <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Street:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="street"
                                       title="Street" 
                                       value="" required><br>
                           </div>
                        <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Suburb:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="suburb"
                                       title="Suburb" 
                                       value="" required><br>
                           </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row top-buffer">
                           <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">City:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="city"
                                       title="City" 
                                       value="" required><br>
                            </div>
                            <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">Postcode:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="postcode"
                                       title="Postcode" 
                                       value="" required><br>
                           </div>
                            <div class="col-md-12">
                                 <span class="require-mark">&#42; </span><label for="state">State/Province:</label>
                               <select id="state" name='state' class="form-control"
                                        value="<?= $state ?>" required>
                                    <option selected='selected' disabled>Please select state</option>
                                    <?php foreach ($states as $state) : ?>
                                        <option
                                            value='<?= $state->id ?>'><?= $state->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                           </div>
                           
                        </div>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Working With Children Checks (NOTE: A Blue Card is NOT required to apply)</h4>
                        <hr>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-4">
                        <div class="col-md-12">
                                <label for="street-name">Card Number:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="card_number"
                                       title="Card Number" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12">
                                <label for="street-name">Expiry Date:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="expiry_date"
                                       title="Expiry Date" 
                                       value=""><br><i class="glyphicon glyphicon-info-sign"></i> Note: Please enter date format <strong>yyyy-mm-dd.</strong><br>
                        </div>
                    </div>
                   
                </div>
                  
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Academic Background</h4>
                        <hr>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-4">
                        <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">High School Result (OP/ATAR):</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_1"
                                       title="Answer_1" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12">
                                <span class="require-mark">&#42; </span><label for="street-name">GPA:</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_2"
                                       title="Answer_2" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12">
                            <label for="subjects">What Subjects do you feel you are able to tutor and to what level? (Hold CTRL to select multiple)</label>
                           
                        <select id="" name="subjects[]" class="form-control" multiple>
                                    <?php foreach ($subjs as $subj) : ?>
                                        <option id="opt"
                                        value='<?= $subj->id ?>'><?= $subj->name ?>
                                        </option> 
                                    <?php endforeach; ?>
                                                    
                        </select>
                           
                        </div>
                        
                        
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                                <label for="street-name">Are you currently studying, if so what?</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_4"
                                       title="Answer_4" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12">
                                <label for="street-name">Do you have a drivers license and access to your own car?</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_5"
                                       title="Answer_5" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12 top-buffer">
                            
                                <label for="street-name">Explain why you think you would make an excellent tutor for these subjects.</label>
                                <textarea name="ans_3"  title="Answer_3"class="form-control" id="exampleTextarea" rows="3"></textarea><br>
                                
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                                <label for="street-name">Where are you studying?</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_6"
                                       title="Answer_" 
                                       value=""><br>
                        </div>
                        <div class="col-md-12">
                                <label for="street-name">How many hours a week would you like to tutor?</label>
                                <input type="text" class="form-control" id="street-name"
                                       name="ans_7"
                                       title="Street Name" 
                                       value=""><br>
                        </div>
                        
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-12">
                        <h4>Personal Background & Ability</h4>
                        <hr>
                    </div>
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <label for="street-name">Describe how you would use your personal experience to assist your students, and why you believe it would help them.</label><br><br><br>
                                
                        </div>
                        <div class="col-md-12">
                            <label for="street-name">Given sessions are often 1-2 hours long, how long would you be comfortable travelling in order to get to a session? (Note: hourly rates vary from $27-35/hr, travel time not included)</label><br><br><br>
                                
                        </div>
                        <div class="col-md-12">
                            <label for="street-name">Lastly please describe why you would like to become an Tutor2You Tutor and why do you think you would represent our company well with clients.</label><br><br><br>
                                
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">
                            <p>Upload your resume here (PDF Format only)</p>
                            <input type="file" name="resume" accept="application/jpeg"></input><br />
                 
                        </div> 
                                
                        </div>
                          
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <textarea name="ans_8" class="form-control" id="exampleTextarea" rows="3"></textarea><br><br>
                        </div>
                        <div class="col-md-12">
                            <textarea name="ans_9" class="form-control" id="exampleTextarea" rows="3"></textarea><br><br>
                        </div>
                        <div class="col-md-12">
                            <textarea name="ans_10" class="form-control" id="exampleTextarea" rows="3"></textarea><br>
                        </div>
                    </div>
                    
                </div>
                <div class="row col-md-12 top-buffer">
                    <div class="col-md-4">
                        
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                    
                </div>
               </form>
                
            </div>
        </div>
    </div>
</div>
</div><!-- End .row-->
       
<script>
    $(document).ready(function () {
 
        var hash = window.location.hash;
        
        if (hash === '#submitted'){
            alert('Application submitted!');
           
        }

//        switch (hash) {
//            case '#submitted':
//                $('#response-dialog').modal('show');
//                $('#response-dialog .modal-title').text('Info');
//                $('#response-dialog .modal-body p').text('Please complete existing booking before activating a new one.');
//            break;
//        
//        }
    $('#subjects').multiSelect();
    });
 
</script>

<?= $this->Html->script('admin/add-new-clients.js'); ?>
