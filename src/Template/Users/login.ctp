<br/>
<br/>
<div class="row">
    <div class="col-sm-12 col-md-4 col-md-offset-4 sign-in animated flipInY">
        <div class="login-box center-block well">
            <?php
            echo $this->Form->create(null, [
                'url' => ['action' => 'login'],
                'type' => 'post', 'class' => 'form-horizontal']);
            ?>
            <div class="title icon-space text-center"><?= $this->Html->image('logo.png', array('style' => 'height: 44px; margin-top: 4px')) ?>
            </div><br>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <p id="loginHelper" class="text-center hidesometin" style="color: #ff1b1b;">Invalid email or password doesn't match any account.</p>
                </div>
            </div>            
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="email" placeholder="Email" name="email" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>
            </div>
            <label for="password" class="control-label sr-only">Password</label>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="password" placeholder="Password" name="password" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
            </div>
            <div class="form-group pull-right">
                <a data-toggle="modal" data-target="#forgot-password">Forgot Password?</a> &nbsp;&nbsp;&nbsp;&nbsp;
            </div><br>
<!--            <a href="http://aimhightuition.com.au/v1.1">Click here to login to the old platform.</a> <br><br>-->
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <button class="btn btn-success btn-block btn-auth"><i class="fa fa-arrow-circle-o-right"></i> Login</button>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div><!-- login container End here -->
</div>