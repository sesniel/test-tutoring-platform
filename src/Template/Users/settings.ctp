<div class="container">
    <div class="row centered-form">
    <div class="col-xs-12">
        <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Profile</h1>
                </div>
                <div class="panel-body">
                    <?php
                    echo $this->Form->create(null, [
                            'url' => ['action' => 'settings']
                        ]);
                    ?>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <strong class="pull-right"><?= $details['email'] ?></strong><br/><br/>
                        </div>
                    </div>
                    
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" value="<?= $details['client_fname'] ?>" class="form-control input-sm" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" value="<?= $details['client_lname'] ?>" class="form-control input-sm" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="col-xs6 col-sm-6 col-md-6">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-3 col-md-3">
                                    <label>Street Address</label>
                                        <input type="text" name="unit_num" value="<?= $details['unit_num'] ?>" class="form-control input-sm" placeholder="Unit Number">
                                    </div>
                                    <div class="col-xs-9 col-sm-9 col-md-9">
                                    <label>&nbsp;</label>
                                        <input type="text" name="street_name" value="<?= $details['street_name'] ?>" class="form-control input-sm" placeholder="Street">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Suburb</label>
                                    <input type="text" name="suburb" value="<?= $details['suburb'] ?>" class="form-control input-sm" placeholder="Suburb">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>State</label>
                                    <input type="text" name="state" value="<?= $details['state'] ?>" class="form-control input-sm" placeholder="State">
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>City</label>
                                    <select id="tutor-state" name="state" class="form-control admin-addnewleads-input" title="State" required="">
                                        <option <?php if( strtoupper($details['state']) == "NSW"): echo "selected"; endif; ?> value="NSW">NSW</option>
                                        <option <?php if( strtoupper($details['state']) == "ACT"): echo "selected"; endif; ?> value="ACT">ACT</option>
                                        <option <?php if( strtoupper($details['state']) == "NT"): echo "selected"; endif; ?> value="NT">NT</option>
                                        <option <?php if( strtoupper($details['state']) == "QLD"): echo "selected"; endif; ?> value="QLD">QLD</option>
                                        <option <?php if( strtoupper($details['state']) == "SA"): echo "selected"; endif; ?> value="SA">SA</option>
                                        <option <?php if( strtoupper($details['state']) == "VIC"): echo "selected"; endif; ?> value="VIC">VIC</option>
                                        <option <?php if( strtoupper($details['state']) == "WA"): echo "selected"; endif; ?> value="WA">WA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="phone" value="<?= $details['phone'] ?>" class="form-control input-sm" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" name="mobile" value="<?= $details['mobile'] ?>" class="form-control input-sm" placeholder="Mobile">
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Postcode</label>
                                    <input type="text" name="postcode" value="<?= $details['postcode'] ?>" class="form-control input-sm" placeholder="Postcode">
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label>Availability</label>
                                    <input type="text" name="availability" value="<?= $details['availability'] ?>" class="form-control input-sm" placeholder="Availability">
                                </div>
                            </div>
                                
                            
                        </div>
                        
                        <hr/>


                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <label>Update Password</label>
                            </div>
                                
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <input type="password" name="current_password" id="password" class="form-control input-sm" placeholder="Current Password">
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <input type="password" name="new_password" id="password" class="form-control input-sm" placeholder="New Password">
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="Update" class="btn btn-info pull-right">

                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
    <?php // debug($details) ?>
    
    <div class="col-md-12 well" >
        <?php        
            echo $this->Form->create(null, [
    'url' => ['controller' => 'client', 'action' => 'students']
]);
        ?>
        <h1>Student(s) Details:</h1>
        <table class="table table-striped">
            <tr>
                <th>Student</th>
                <th>Year</th>
                <th>Subjects</th>
            </tr>
            <?php foreach($students as $key => $student): ?>
            <tr>
                <td>
                    <input type="hidden" name="id[<?= $key ?>]" value="<?= $student['student_id'] ?>" class="form-control admin-addnewleads-input" placeholder="Student" required="">
                    <input type="text" name="name[<?= $key ?>]" value="<?= $student['name'] ?>" class="form-control admin-addnewleads-input" placeholder="Student" required="">
                </td>
                <td>
                    <select name="year-level[<?= $key ?>]" class="student-yrlvl form-control" required="">
                            <option <?php if($student['year_level'] == "F"): echo "selected"; endif; ?> value="F">F</option>
                            <option <?php if($student['year_level'] == "1"): echo "selected"; endif; ?> value="1">1</option>
                            <option <?php if($student['year_level'] == "2"): echo "selected"; endif; ?> value="2">2</option>
                            <option <?php if($student['year_level'] == "3"): echo "selected"; endif; ?> value="3">3</option>
                            <option <?php if($student['year_level'] == "4"): echo "selected"; endif; ?> value="4">4</option>
                            <option <?php if($student['year_level'] == "5"): echo "selected"; endif; ?> value="5">5</option>
                            <option <?php if($student['year_level'] == "6"): echo "selected"; endif; ?> value="6">6</option>
                            <option <?php if($student['year_level'] == "7"): echo "selected"; endif; ?> value="7">7</option>
                            <option <?php if($student['year_level'] == "8"): echo "selected"; endif; ?> value="8">8</option>
                            <option <?php if($student['year_level'] == "9"): echo "selected"; endif; ?> value="9">9</option>
                            <option <?php if($student['year_level'] == "10"): echo "selected"; endif; ?>  value="10">10</option>
                            <option <?php if($student['year_level'] == "10A"): echo "selected"; endif; ?> value="10A">10A</option>
                    </select>
                </td>
                <td>
                    <?php $splitSubjects = array_map('trim', explode(',',$student['subjects'])); ?>
                    <select name="subjects<?= $key ?>[]" data-placeholder="Subjects" class="form-control student-subjects-ms" multiple>
                        <option <?Php if(in_array("English",        $splitSubjects)): echo "selected"; endif; ?> value="English"        >English</option>
                        <option <?Php if(in_array("Learning IQ",    $splitSubjects)): echo "selected"; endif; ?> value="Learning IQ"    >Learning IQ</option>
                        <option <?Php if(in_array("Learning Style", $splitSubjects)): echo "selected"; endif; ?> value="Learning Style" >Learning Style</option>
                        <option <?Php if(in_array("Math",           $splitSubjects)): echo "selected"; endif; ?> value="Math"           >Math</option>
                        <option <?Php if(in_array("Science",        $splitSubjects)): echo "selected"; endif; ?> value="Science"        >Science</option>
                    </select>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <input type="submit" value="Update Student Details" class="btn btn-primary pull-right" />
    </div>
    <br/>
    <?php echo $this->Form->end(); ?>
</div>