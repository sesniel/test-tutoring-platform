<style>
    .my-width {
        width: 300px;
    }

    .my-width-choice {
        width: 250px;
        padding: 20px;
        font-size: x-large;
    }

    .borderless table {
        border-top-style: none;
        border-left-style: none;
        border-right-style: none;
        border-bottom-style: none;
    }

    .borderless td, .borderless th {
        border: none;
    }

    .pager {
        display: none;
    }

    .preload {
        width: 50px;
        height: 50px;
        position: fixed;
        top: 53%;
        left: 48%;
    }

    .btn-sm {
        padding: 10px 5px;
        font-size: 16px;
        line-height: 1.5;
        border-radius: 3px;
    }
    
    .next-question{
        display: none;
    }
    
</style>
<script>

    $(function () {
        $(".preload").fadeOut(1000, function () {
            $(".pager").fadeIn(1000);
        });
    });

    $(document).ready(function () {
        
        $('.next-question').hide();

        if ($('#answer1').is(':checked')) {
            $('.next-question').show();
            $('.skip-question').hide();
        }
        else if ($('#answer2').is(':checked')) {
            $('.next-question').show();
            $('.skip-question').hide();
        }
        else if ($('#answer3').is(':checked')) {
            $('.next-question').show();
            $('.skip-question').hide();
        }
        else if ($('#answer4').is(':checked')) {
            $('.next-question').show();
            $('.skip-question').hide();
        }
        else if ($('#answer5').is(':checked')) {
            $('.next-question').show();
            $('.skip-question').hide();
        }
        else {
            $('.next-question').hide();
            $('.skip-question').show();
        }

        $("input[name=answer]").on("change", function () {
            $('.next-question').show();
            $('.skip-question').hide();
        });
//        
    });


    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event) {
        if (event.button == 2) {
            return false;
        }
    }

</script>
<?php    
    $logPosition = $studentAssessmentDetails->user_question->position;
?>

<div id="test">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="widget-box">
                <div class="widget-title">
                    <h2><strong><?= ucwords($studentAssessmentDetails->student->name) ?></strong>
                        Year <?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id) ?>
                        Level</h2>
                </div>
                <hr class="customHr">
                <div class="widget-container table-responsive">
                    <div class="row center-table">
                        <div class="row col-md-12 top-buffer">
                            <div class="col-md-12">
                                <h2 class="text-center"><?php echo $studentAssessmentDetails->user_question->question; ?></h2>
                            </div>
                            <div class="col-md-12">
                                <?php
                                
                                if ($studentAssessmentDetails->user_question->image != "" && $studentAssessmentDetails->user_question->type == "General"):
                                    $newstring = substr($studentAssessmentDetails->user_question->image, -3);
                                    $subjectname = getSubjectName($studentAssessmentDetails->assessment_type->subject_id);
                                    echo $this->Html->image($subjectname . "/" . substr($studentAssessmentDetails->user_question->image, 0, -3) . strtolower($newstring),
                                        ['border' => '0', 'class' => 'img-responsive center-block']);
                                elseif($studentAssessmentDetails->user_question->type == "Stimulus"):
                                    
                                    if($totalUserQuestionLeft == 1):
//                                        echo $studentAssessmentDetails->user_question->image;
                                        echo '<iframe width="1325" height="663" src="https://www.youtube.com/embed/xMnx_3BC7EM" frameborder="0" allowfullscreen></iframe>';
                                    else: 
                                        $newstring = substr($studentAssessmentDetails->user_question->image, -3);
                                        echo $this->Html->image("Stimulus/" . substr($studentAssessmentDetails->user_question->image, 0, -3) . strtolower($newstring),
                                            ['border' => '0', 'class' => 'img-responsive center-block']);
                                    endif;

                                endif;

                                ?>

                                <br/>

                                <?php
                                if($studentAssessmentDetails->user_question->type != "Stimulus"):
                                    
                                    if(1 == $totalUserQuestionLeft):

                                        echo $this->Form->create(null, [
                                            'url' => ['action' => 'logfinalanswer/' . $this->request->params['pass'][0] . '/' . $logPosition]
                                        ]);
                                        
                                    else:
                                        
                                        echo $this->Form->create(null, [
                                            'url' => ['action' => 'loganswer/' . $this->request->params['pass'][0] . '/' . $logPosition]
                                        ]);   
                                    
                                    endif;
                                    
                                else:
                                    
                                    if(1 == $totalUserQuestionLeft):

                                        echo $this->Form->create(null, [
                                            'url' => ['action' => 'stimulusfinalanswer/' . $this->request->params['pass'][0] . '/' . $logPosition]
                                        ]);
                                        
                                    else:
                                        
                                        echo $this->Form->create(null, [
                                            'url' => ['action' => 'stimulusanswer/' . $this->request->params['pass'][0] . '/' . $logPosition]
                                        ]);   
                                    
                                    endif;
                                    
                                endif;
                                ?>
                                <table class="table borderless">
                                    <tr>
                                        <td>
                                            <br/>
                                            <?php if($studentAssessmentDetails->user_question->type == "Stimulus"): ?>
                                            
                                            <h5 class="text-center"> Reaction </h5>
                                            
                                            <div class="row text-center">
                                                <div class="col-md-12 text-center">
                                                        <input type="hidden"
                                                               value='<?= $studentAssessmentDetails->user_question->id ?>'
                                                               name='UserQuestionId'/>
                                                        
                                                        <textarea class="form-control" rows="8" name="reaction" required="" ></textarea>
                                                        
                                                        
                                                </div>
                                            </div>
                                            
                                            
                                            <?Php else: ?>
                                            
                                            <h5 class="text-center"> Choices </h5>
                                             
                                            <div class="row text-center">
                                                <div class="col-md-12 text-center">
                                                    <div class="btn-group" data-toggle="buttons">
                                                        <input type="hidden"
                                                               value='<?= $studentAssessmentDetails->user_question->id ?>'
                                                               name='UserQuestionId'/>
                                                        <?php
                                                        $questions = json_decode($studentAssessmentDetails->user_question->choices, true);
                                                        $x = 1;
                                                        foreach ($questions as $question):
                                                            ?>

                                                            <label class="btn btn-md btn-primary
                                    <?php
                                                            if ($question == $studentAssessmentDetails->user_question->user_answer):
                                                                echo "active";
                                                            endif;
                                                            ?> ">
                                                                <input type="radio" id="answer<?= $x ?>" name="answer"
                                                                       autocomplete="off"
                                                                       value="<?= $question ?>"
                                                                    <?php
                                                                    if ($question == $studentAssessmentDetails->user_question->user_answer):
                                                                        echo "checked";
                                                                    endif;
                                                                    ?> >
                                                                <?= $question ?>
                                                            </label>

                                                            <?php $x++; endforeach; ?>

                                                    </div>
                                                </div>
                                            </div>

                                            <?Php endif; ?>
                                            <br/>

                                            <div class="row text-center">
                                                <div class="col-md-12 text-center">
                                                    
                                                    <?php
                                                        if($studentAssessmentDetails->user_question->type != "Stimulus"):
                                                    
                                                        if(1 == $totalUserQuestionLeft):

                                                            echo $this->Html->link(
                                                                    "Skip Question",
                                                                    array('controller' => 'assessment', 'action' => 'skipfinal/' . $this->request->params['pass'][0] . "/" . $studentAssessmentDetails->user_question->id),
                                                                    ['class' => 'btn btn-default skip-question']
                                                                ); 

                                                        else:

                                                            echo $this->Html->link(
                                                                    "Skip Question",
                                                                    array('controller' => 'assessment', 'action' => 'skip/' . $this->request->params['pass'][0] . "/" . $studentAssessmentDetails->user_question->id),
                                                                    ['class' => 'btn btn-default skip-question']
                                                                );   

                                                        endif;
                                                    ?>
                                                                                                        
                                                    <input type="submit" value="Submit"
                                                           class="my-width-choice btn btn-sm btn-success next-question"/>
                                                    
                                                    <?php else: ?>
                                                    
                                                        <input type="submit" value="Submit"
                                                               class="my-width-choice btn btn-sm btn-success"/>
                                                    
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            
                                        </td>
                                    </tr>
                                </table>
                                </form>
                                <hr>
                            </div>
                            <div class="col-md-12 text-center">

                                <br/><br/><br/>
                                <?Php 
                                    if ($logPosition > 1): 
                                        $progress = round((($logPosition - 1) / $totalUserQuestion) * 100); 
                                ?>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped"
                                             role="progressbar"
                                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                             style="width:<?= $progress ?>%">
                                            <?= $progress ?>% Complete  (success) <?php //echo $totalUserQuestionLeft ?>
                                        </div>
                                    </div>
                                <?php echo  $logPosition . " / " .  $totalUserQuestion; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 

    function getYearLevelInfo($yearLevelId = null){
        if($yearLevelId == 1):
            return "F";
        else:
            return $yearLevelId - 1;
        endif;
    }
    
    function getSubjectName($subjectId = null){
        if($subjectId == 1):
            return "Math";
        elseif($subjectId == 2):
            return "English";
        else:
            return "";
        endif;
    }
    
?>
<?Php 
//echo $studentAssessmentDetails->user_question->position;
//pr($studentAssessmentDetails); 
?>