<?php
$majorDetailsTemp = array();
foreach ($userQuestionInfoDetails as $major):
    $majorDetails[$major->major_outcome->id] = $major->major_outcome->name;
endforeach;

$userQuestionInfo = array();
foreach ($userQuestionInfoDetails as $userQuestionDetail):

    foreach ($majorDetails as $keyM => $majorInfo):
        if ($keyM === $userQuestionDetail->major_outcome->id):
            if ($userQuestionDetail->correct_answer === $userQuestionDetail->user_answer):
                if (isset($userQuestionInfo[$keyM]['correct'])):

                    $userQuestionInfo[$keyM]['correct'] += 1;

                else:

                    $userQuestionInfo[$keyM]['correct'] = 1;

                endif;
            else:
                if (isset($userQuestionInfo[$keyM]['wrong'])):

                    $userQuestionInfo[$keyM]['wrong'] += 1;

                else:

                    $userQuestionInfo[$keyM]['wrong'] = 1;

                endif;
            endif;

            if (!isset($userQuestionInfo[$keyM]['majoroutcome_name'])):
                $userQuestionInfo[$keyM]['majoroutcome_name'] = $majorInfo;
            endif;
        endif;
    endforeach;

endforeach;


?>
<script type="text/javascript">
    $(function () {

        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '<?php echo $studentAssessmentDetails->assessment_type->type ?>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total percent of the assessment'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [

                    <?Php foreach($userQuestionInfo as $qInfo): ?>
                    {
                        name: "<?= $qInfo['majoroutcome_name'] ?>",
                        y: <?php // echo $qInfo['correct'] . " / " . $qInfo['wrong'];
                        if (isset($qInfo['correct']) && isset($qInfo['wrong'])):

                            echo round(($qInfo['correct'] / ($qInfo['correct'] + $qInfo['wrong'])) * 100, 0);

                        elseif (!isset($qInfo['correct']) && isset($qInfo['wrong'])):

                            echo "0.0";

                        elseif (isset($qInfo['correct']) && !isset($qInfo['wrong'])):

                            echo "100.0";

                        endif;

                        ?>,
                        drilldown: null
                    },
                    <?php endforeach; ?>

                ]
            }]
        });
    });

</script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Report Details</strong></h2>
            </div>
            <hr class="customHr">
            <div class="row col-md-12 center-content">
                <div class="col-md-8">
                    <h1><?= $studentAssessmentDetails->student->name ?></h1>
                    <h4>Start Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->created)); ?></h4>
                    <h4>Done Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
                </div>
                <div class="col-md-4 top-buffer">
                    <?Php
                    echo $this->Html->link(
                        'View / Print Detailed Report',
                        array('action' => 'printReport', $studentAssessmentDetails->id, 'prefix' => 'client'),
                        array('target' => '_blank', 'class' => 'btn btn-primary pull-right')  // important
                    );
                    ?>
                </div>
            </div>
            <div class="widget-container table-responsive">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>

        </div>
    </div>
</div>
<?php // debug($studentAssessmentDetails); ?>
