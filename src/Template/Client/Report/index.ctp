<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Results</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Here you can see your recent results</p>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <?Php foreach ($updatedClientInfo['students'] as $key => $info): ?>
                                        <li role="presentation" <?php if ($key == 0): echo 'class="active"'; endif; ?> >
                                            <a href="#_<?= $info->id ?>" aria-controls="home" role="tab"
                                               data-toggle="tab"><h5><?= $info->name ?></h5></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="tab-content">
                                    <?Php foreach ($updatedClientInfo['students'] as $key => $info): ?>
                                        <div role="tabpanel"
                                             class="well tab-pane <?php if ($key == 0): echo 'active'; endif; ?>"
                                             id="_<?= $info->id ?>">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h2><?= $info->name ?></h2>
                                                </div>
                                            </div>
                                            <?Php if(!empty($info['StudentAssessmentTypes'])): ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table id="resultsTbl" class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Assessment Type</th>
                                                            <th>Date Created</th>
                                                            <th>Done Date</th>
                                                            <th>Tutor Name</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($info['StudentAssessmentTypes'] as $studentAssessmentTypes): ?>

                                                            <tr>
                                                                <td><?= $studentAssessmentTypes->assessment_type->type ?></td>
                                                                <td><?php echo date('d-m-Y', strtotime($studentAssessmentTypes->created)); ?></td>
                                                                <td><?php echo date('d-m-Y', strtotime($studentAssessmentTypes->done_date)); ?></td>
                                                                <td><?= $studentAssessmentTypes->tutor->first_name ?> <?= $studentAssessmentTypes->tutor->last_name ?></td>
                                                                <td>
                                                                    <?Php
                                                                    if ($studentAssessmentTypes->status == "Done"):
                                                                        if ($studentAssessmentTypes->assessment_type_id == 1 || $studentAssessmentTypes->assessment_type_id == 2):

                                                                            echo $this->Html->link(
                                                                                'View',
                                                                                array('controller' => 'report', 'action' => 'view-learning/' . $studentAssessmentTypes->id, 'prefix' => 'client', '_full' => true),
                                                                                array('escape' => false, 'class' => 'btn btn-success btn-sm', 'target' => '_blank')
                                                                            );

                                                                        else:

                                                                            echo $this->Html->link(
                                                                                'View',
                                                                                array('controller' => 'report', 'action' => 'view/' . $studentAssessmentTypes->id, 'prefix' => 'client', '_full' => true),
                                                                                array('escape' => false, 'class' => 'btn btn-success btn-sm', 'target' => '_blank')
                                                                            );

                                                                        endif;
                                                                    endif;

                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <?php else: ?>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    There Are No Results Yet
                                                </div>
                                            </div>
                                            
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('clients/results.js'); ?>
<?php // pr($updatedClientInfo); //exit; ?>
<?php
function getYearLevelInfo($yearLevelId)
{
    if ($yearLevelId == 1):
        return "F";
    else:
        return $yearLevelId - 1;
    endif;
}

?>
