<?php
    $majorDetailsTemp = array();
    foreach($userQuestionInfoDetails as $major):
        $majorDetails[$major->major_outcome->id] = $major->major_outcome->name;
    endforeach;
    
    $userQuestionInfo = array();
    foreach($userQuestionInfoDetails as $userQuestionDetail):
        
        foreach($majorDetails as $keyM => $majorInfo):
            if($keyM === $userQuestionDetail->major_outcome->id):
                if($userQuestionDetail->correct_answer === $userQuestionDetail->user_answer):
                    if(isset($userQuestionInfo[$keyM]['correct'])):
                        
                        $userQuestionInfo[$keyM]['correct'] += 1;
                    
                    else:
                        
                        $userQuestionInfo[$keyM]['correct'] = 1;
                    
                    endif;
                else:
                    if(isset($userQuestionInfo[$keyM]['wrong'])):
                        
                        $userQuestionInfo[$keyM]['wrong'] += 1;
                    
                    else:
                        
                        $userQuestionInfo[$keyM]['wrong'] = 1;
                    
                    endif;
                endif;
                
                if(!isset($userQuestionInfo[$keyM]['majoroutcome_name'])):
                    $userQuestionInfo[$keyM]['majoroutcome_name'] = $majorInfo;
                endif;
                
                $userQuestionInfo[$keyM]['majoroutcome_details'][] = $userQuestionDetail;
                
            endif;
        endforeach;
        
    endforeach;
        
    
?>
<script type="text/javascript">
$(function () {
    
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo $studentAssessmentDetails->assessment_type->type ?>'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent of the assessment'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            
                <?Php foreach($userQuestionInfo as $qInfo): ?>
                    {
                        name: "<?= $qInfo['majoroutcome_name'] ?>",
                        y: <?php // echo $qInfo['correct'] . " / " . $qInfo['wrong'];
                                if(isset($qInfo['correct']) && isset($qInfo['wrong'])):
                                    
                                    echo round(($qInfo['correct'] / ($qInfo['correct'] + $qInfo['wrong'])) * 100 , 0);
                                
                                elseif(!isset($qInfo['correct']) && isset($qInfo['wrong'])):
                                    
                                    echo "0.0";
                                
                                elseif(isset($qInfo['correct']) && !isset($qInfo['wrong'])):
                                    
                                    echo "100.0";
                                
                                endif;
                                    
                                ?>,
                        drilldown: null
                    },
                <?php endforeach; ?>
            
            ]
        }]
    });
});

</script> 
    
<div class="row"> <!-- Assessments Tables -->
    <div class="col-sm-12 col-md-12">
        <div class='well-sm ' id='printableArea' style='    width: 800px; margin:auto;'>
        <div class="well">
            <div class="col-md-12">
                <h1><?= $studentAssessmentDetails->student->name ?></h1>
                <h4>Year Level: <?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id); ?></h4>
                <h4>Start Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->created)); ?></h4>
                <h4>Done Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
            </div>
            
            <div class="widget-container table-responsive">
            
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                <?Php                                               
//                    echo $this->Html->link(
//                        'Print Detailed Report',
//                          array('action'=>'printReport', $studentAssessmentDetails->id, 'prefix' => 'client'),
//                          array('target'=>'_blank', 'class' => 'btn btn-primary btn-lg pull-right')  // important  
//                        );
                ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <h2>Assessment Outcomes</h2>
                        <p>These are the relevant curriculum the student is assessed against and the year they are actually
                            in.</p>
                        <ul class="list-inline">
                            <li>
                                <a href="http://www.australiancurriculum.edu.au/<?= ($studentAssessmentDetails->assessment_type->subject_id === 1)? "mathematics" : "english"; ?>/curriculum/f-10?layout=1#level<?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id); ?>"
                                   class="btn btn-primary" data-lity>Target Outcomes for
                                    Year <?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id); ?></a></li>
                            <li>
                                <a href="http://www.australiancurriculum.edu.au/<?= ($studentAssessmentDetails->assessment_type->subject_id === 1)? "mathematics" : "english"; ?>/curriculum/f-10?layout=1#level<?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id) - 1; ?>"
                                   class="btn btn-primary" data-lity>Assessment Outcomes for
                                    Year <?php echo getYearLevelInfo($studentAssessmentDetails->student->year_level_id) - 1; ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <hr/>                
                
                <div class="row">
                    <div class="col-md-12">
                        <h2>Major Outcome Summary</h2>
                        <table class="table table-bordered">
                            <tr class="success">
                                <td>Major Outcome Name</td>
                                <td>Correct</td>
                                <td>Number of Question</td>
                                <td>Percentage</td>
                            </tr>
                            <?Php foreach($userQuestionInfo as $qInfo): ?>
                            
                                <tr>
                                    <td><?= $qInfo['majoroutcome_name']; ?></td>
                                    <td>
                                        <?php
                                            if(isset($qInfo['correct'])):
                                                echo $qInfo['correct'];
                                            else:
                                                echo 0;
                                            endif;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(isset($qInfo['correct']) && isset($qInfo['wrong'])):
                                                $totalQuestion = $qInfo['correct'] + $qInfo['wrong'];
                                            elseif(!isset($qInfo['correct']) && isset($qInfo['wrong'])):
                                                $totalQuestion = $qInfo['wrong'];
                                            elseif(isset($qInfo['correct']) && !isset($qInfo['wrong'])):
                                                $totalQuestion = $qInfo['correct'];
                                            else:
                                                $totalQuestion = 0;
                                            endif;
                                             
                                            echo $totalQuestion;
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            if(isset($qInfo['correct'])):
                                                echo round(($qInfo['correct'] / $totalQuestion) * 100, 2) . " %";
                                            else:
                                                echo "0 %";
                                            endif;
                                        ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        </table>
                        
                    </div>
                </div>
                
                <hr/>                
                
                <div class="row">
                    <div class="col-md-12">
                        <h2>Major Outcome Summary</h2>
                        <table class="table table-bordered">
                            <?Php foreach($userQuestionInfo as $key => $qInfo): ?>
                            
                                <tr class="info">
                                    <th colspan="7" style="text-align: center;">
                                         <h4><?= $qInfo['majoroutcome_name'] ?></h4>
                                    </th>
                                </tr>
                                <tr class="warning">
                                    <td>Description</td>
                                    <td>Question</td>
                                    <td>Answered</td>
                                    <td>Correct Answer</td>
                                    <td>Question Image</td>
                                    <td>Mark</td>
                                </tr>
                            
                                <?php foreach($qInfo['majoroutcome_details'] as $majoroutcomeDetails): ?>
                                    <tr>
                                        <td><?= $majoroutcomeDetails->minor_outcome->description ?></td>
                                        <td><?= $majoroutcomeDetails->question ?></td>
                                        <td><?= $majoroutcomeDetails->user_answer ?></td>
                                        <td>
                                            <?Php 
                                                if($majoroutcomeDetails->type != "Stimulus"):
                                                    echo $majoroutcomeDetails->correct_answer;
                                                endif;
                                            ?>
                                        </td>
                                        <td>
                                            <?php  //echo "sesniel->" . count($userQuestionInfo) . " ==> " . $key;
                                                if($majoroutcomeDetails->type != "Stimulus"):
                                                    
                                                    $subject = getSubjectName($majoroutcomeDetails->major_outcome->subject_id);
                                                    if ($majoroutcomeDetails->image != ""):
                                                        $newstring = substr($majoroutcomeDetails->image, -3);
                                                        echo $this->Html->image($subject . "/" . substr($majoroutcomeDetails->image, 0, -3) . strtolower($newstring),
                                                            ['border' => '0', 'class' => 'img-responsive center-block', 'data-lity']);
                                                    else:
                                                        echo '- - -';
                                                    endif; 
                                                else:
                                                    $newstring = substr($majoroutcomeDetails->image, -3);
                                                    if($newstring == "png"):
                                                        echo $this->Html->image("Stimulus/" . $majoroutcomeDetails->image,
                                                            ['border' => '0', 'class' => 'img-responsive center-block', 'data-lity']); 
                                                    else:
                                                        echo "<a href='" . $majoroutcomeDetails->image . "' target='_blank'> Link </a>";
                                                    endif;
                                                endif;
                                            ?>
                                        </td>
                                        <td>
                                            <?Php
                                                if($majoroutcomeDetails->user_answer === $majoroutcomeDetails->correct_answer):
                                                    echo '<i class="glyphicon glyphicon-ok"></i>';
                                                else:
                                                    echo '<i class="glyphicon glyphicon-remove"></i>';
                                                endif;
                                            ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>
                            
                                <tr>
                                    <th colspan="7">
                                        &nbsp;
                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
                            
                
            </div>
            
        </div>
        <button class="btn btn-large btn-primary pull-right" onclick="printDiv('printableArea')">Print Report</button>
        </div>
        <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;
            }
        </script>
    </div>
</div>
<?php 
    function getYearLevelInfo($yearLevelId){
        if($yearLevelId == 1):
            return "F";
        else:
            return $yearLevelId - 1;
        endif;
    }
    
    function getSubjectName($subjectId = null){
        if($subjectId == 1):
            return "Math";
        elseif($subjectId == 2):
            return "English";
        else:
            return "";
        endif;
    }
?>
<?php // debug($studentAssessmentDetails); ?>
