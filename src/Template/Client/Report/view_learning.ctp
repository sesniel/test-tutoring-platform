<?php
$majorDetailsTemp = array();
foreach ($userQuestionInfoDetails as $major):
    $majorDetails[$major->major_outcome->id] = $major->major_outcome->name;
endforeach;
$userQuestionInfo = array();


foreach ($userQuestionInfoDetails as $userQuestionDetail):


    foreach ($majorDetails as $keyM => $majorInfo):
        if ($keyM === $userQuestionDetail->major_outcome->id):

            if (isset($userQuestionInfo[$keyM]['correct'])):
                $userQuestionInfo[$keyM]['correct'] += getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
            else:
                $userQuestionInfo[$keyM]['correct'] = getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
            endif;


            if (isset($userQuestionInfo[$keyM]['total-value'])):
                $userQuestionInfo[$keyM]['total-value'] += getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
            else:
                $userQuestionInfo[$keyM]['total-value'] = getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
            endif;


            if (!isset($userQuestionInfo[$keyM]['majoroutcome_name'])):
                $userQuestionInfo[$keyM]['majoroutcome_name'] = $majorInfo;
            endif;
        endif;
    endforeach;


endforeach;

//    debug($userQuestionInfo);


function getMarkValue($user_answer, $choices, $mark)
{
    //#skipped
    if($user_answer != "#skipped"):

        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue = json_decode($mark, TRUE);
        $choiceKey = array_search($userAnswer, $choicesVal);

        return $markValue[$choiceKey];
    else:
        
        return 0;
        
    endif;

}

function getHighestMarkValue($mark, $user_answer)
{
    if($user_answer != "#skipped"):
        
        $markValue = json_decode($mark, TRUE);
        return max($markValue);
    else:
        
        return 0;

    endif;
}


?>
<script type="text/javascript">
    $(function () {

        $('#container').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: '<?php echo $studentAssessmentDetails->assessment_type->type ?>'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total percent of the assessment'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [

                    <?Php foreach($userQuestionInfo as $qInfo): ?>
                    {
                        name: "<?= $qInfo['majoroutcome_name'] ?>",
                        y: <?php // echo $qInfo['correct'] . " / " . $qInfo['wrong'];
                        echo round(($qInfo['correct'] / $qInfo['total-value']) * 100, 0);

                        ?>,
                        drilldown: null
                    },
                    <?php endforeach; ?>

                ]
            }]
        });
    });

</script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Report Details</strong></h2>
            </div>
            <hr class="customHr">
            <div class="row col-md-12 center-content">
                <div class="col-md-8">
                    <h1><?= $studentAssessmentDetails->student->name ?></h1>
                    <h4>Start Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->created)); ?></h4>
                    <h4>Done Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
                </div>
                <div class="col-md-4 top-buffer">
                    <?Php
                    echo $this->Html->link(
                        'View / Print Detailed Report',
                        array('action' => 'print_report_learning', $studentAssessmentDetails->id, 'prefix' => 'client'),
                        array('target' => '_blank', 'class' => 'btn btn-primary pull-right')  // important
                    );
                    ?>
                </div>
            </div>
            <div class="widget-container table-responsive">
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>
