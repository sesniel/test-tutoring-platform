<?php

$totalMarkValue = 0;

foreach ($userQuestionInfoDetails as $userQuestionDetail):
    
    if (isset($majorDetails[$userQuestionDetail->major_outcome->id]['correct'])):
        $majorDetails[$userQuestionDetail->major_outcome->id]['correct'] += getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
    else:
        $majorDetails[$userQuestionDetail->major_outcome->id]['correct'] = getMarkValue($userQuestionDetail->user_answer, $userQuestionDetail->choices, $userQuestionDetail->mark);
    endif;

    if (isset($majorDetails[$userQuestionDetail->major_outcome->id]['total-value'])):
        $majorDetails[$userQuestionDetail->major_outcome->id]['total-value'] += getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    else:
        $majorDetails[$userQuestionDetail->major_outcome->id]['total-value'] = getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    endif;

    if (!isset($majorDetails[$userQuestionDetail->major_outcome->id]['majoroutcome_name'])):
        $majorDetails[$userQuestionDetail->major_outcome->id]['majoroutcome_name'] = $userQuestionDetail->major_outcome->name;
    endif;
    
    $totalMarkValue += getHighestMarkValue($userQuestionDetail->mark, $userQuestionDetail->user_answer);
    
    $majorDetails[$userQuestionDetail->major_outcome->id]['Details'][] = $userQuestionDetail;


endforeach;

//debug($totalMarkValue);
//debug($majorDetails);
//exit;

function getMarkValue($user_answer, $choices, $mark)
{
    //#skipped
    if($user_answer != "#skipped"):

        $userAnswer = $user_answer;
        $choicesVal = json_decode($choices, TRUE);
        $markValue = json_decode($mark, TRUE);
        $choiceKey = array_search($userAnswer, $choicesVal);

        return $markValue[$choiceKey];
    else:
        
        return 0;
        
    endif;

}

function getHighestMarkValue($mark, $user_answer)
{
    if($user_answer != "#skipped"):
        
        $markValue = json_decode($mark, TRUE);
        return max($markValue);
    else:
        
        return 0;

    endif;
}


?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Learning Style'
        },
//        subtitle: {
//            text: 'Click the slices to view versions. Source: netmarketshare.com.'
//        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [{
                
            name: 'Details',
            colorByPoint: true,
            
            
            
            data: [
                <?php foreach($majorDetails as $md): ?>
                    {
                        name: '<?= $md['majoroutcome_name'] ?>',
                        y: 
                            <?php 
                                if($md['correct'] > 0):
                                    echo ($md['correct'] / $totalMarkValue) * 100;
                                else:
                                    echo 0;
                                endif;
                        ?>,
                        drilldown: '<?= $md['majoroutcome_name'] ?>'
                    }, 
                <?php endforeach; ?>
            
        
            ]
        }],
        drilldown: {
            series: [
            
                <?php foreach($majorDetails as $md): ?>
                
                    {
                        name: '<?= $md['majoroutcome_name'] ?>',
                        id: '<?= $md['majoroutcome_name'] ?>',
                        data: [
                            <?php foreach($md['Details'] as $detail): ?>
                                ['<?php echo str_replace("'","",$detail->question); ?>: <?= $detail->user_answer ?> ' , <?php echo round((1 / count($md['Details'])) * 100, 2); ?>],
                            <?php endforeach; ?>
                        ]
                    }, 
                
                <?php endforeach; ?>
            ]
        }
    });
});
</script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Report Details</strong></h2>
            </div>
            <hr class="customHr">
            <div class="row col-md-12 center-content">
                <div class="col-md-8">
                    <h1><?= $studentAssessmentDetails->student->name ?></h1>
                    <h4>Start Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->created)); ?></h4>
                    <h4>Done Date: <?php echo date("d-m-Y", strtotime($studentAssessmentDetails->done_date)); ?></h4>
                </div>
                <div class="col-md-4 top-buffer">
                    <?Php
                    echo $this->Html->link(
                        'View / Print Detailed Report',
                        array('action' => 'print_report_learning_style', $studentAssessmentDetails->id, 'prefix' => 'client'),
                        array('target' => '_blank', 'class' => 'btn btn-primary pull-right')  // important
                    );
                    ?>
                </div>
            </div>
            <div class="widget-container table-responsive">
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/data.js"></script>
                <script src="https://code.highcharts.com/modules/drilldown.js"></script>
                <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>
