<?php
    function getYearLevelInfo($yearLevelId){
        if($yearLevelId == 1):
            return "F";
        else:
            return $yearLevelId - 1;
        endif;
    }
    
    $programStatus = array('Active', 'Completed');
    
?>
<div class="widget-box container">
    

    <div class="row">
        <div class="col-sm-6 col-md-6">
        <h2><strong>Booking Information</strong></h2>
        </div>
        <div class="col-sm-3 col-md-3"></div>
        <div class="col-sm-3 col-md-3 statusPos">
           <!-- Status start -->
            <?php
                echo $this->Form->create(null, [
                    'url' => ['action' => 'table'],
                    'method' => 'post'
                ]);
            ?>
            Status: 
            <select name="status" class="form-control" onchange="this.form.submit()" >
                <?php foreach($programStatus as $stat): ?>
                    <option <?php $sel = ($status == $stat ? "selected" : ""); ?> <?= $sel ?> value="<?= $stat ?>"><?= $stat ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo $this->Form->end(); ?>
          <!-- Status end -->
        </div>
    </div>
    
    <hr class="customHr"><br/>
    
    <div class="row">
    <div class="col-sm-12 col-md-12">

        <table id="bookingStudents"
               class="table table-striped responsive display table-hover table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Client</th>
                <th>Students</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach($programDetails as $prog): ?>
                    <tr>
                        <td><?= $prog->id  ?></td>
                        <td><?= $prog->client->first_name . " " . $prog->client->last_name  ?></td>
                        <td><?php
                            if($prog->client->students):
                                $countStudents = count($prog->client->students) - 1;
                                foreach($prog->client->students as $key => $stdnt):
                                    if($countStudents == $key):
                                        echo $stdnt->name;
                                    else:    
                                        echo $stdnt->name . ", ";
                                    endif;
                                endforeach;
                            endif;
                        ?></td>
                        <td>
                            <?php 
                                if($prog->status == "Tutor"):
                                    echo "Pending";   
                                else:
                                    echo $prog->status;                                  
                                endif;
                            ?>
                        </td>
                        <td>
                            <?php 
                                
                                echo $this->Html->link(
                                            "View Booking",
                                            ['action' => 'view', $prog->id, 'prefix' => 'client'], ['escape' => false, 'class' => "btn btn-xs btn-primary"]); 

                            
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    </div>
    <?php // debug($programDetails->toArray()) ?>
</div>
<div id="lead-view-modal" class="modal fade" role="dialog"></div>
<script>
    $(document).ready(function () {

        $('#bookingStudents').dataTable({"autoWidth": false});

    });
</script>
<?php // debug($programDetails->toArray()); ?>