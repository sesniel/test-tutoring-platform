<style>
.hidden_studentInfo {
     display:none;
     padding-top:10px;
}
</style>
<?php // debug($programDetails->toArray()); ?>
<?php $date = date('Ymd'); ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
            <h2><strong>Lesson Details for <?= $programDetails->client->first_name . " " . $programDetails->client->last_name ?>
                <br/>
                <?php $countStudents = count($programDetails->client->students) - 1; ?>
                Student<?php if($countStudents > 0): echo "s"; endif;?>: 
            <?php
                            if($programDetails->client->students):
                                
                                foreach($programDetails->client->students as $key => $stdnt):
                                    if($countStudents == $key):
                                        echo $stdnt->name;
                                    else:    
                                        echo $stdnt->name . ", ";
                                    endif;
                                endforeach;
                            endif;
                        ?>
                </strong>
            </h2>
                
                <div class="col-md-5">
                    <h5><strong>Invoice Number:</strong> <?= $programDetails->invoice_number ?></h5>

                    <hr>
                </div>    
                
                <div class="col-md-6">
                    <h5><strong>Invoice Amount:</strong> <?php echo " $" .  money_format('%i', $programDetails->invoice_amount); ?> <?php //echo $programDetails->invoice_amount ?></h5>
                    <h5><strong>Invoice Amount:</strong> <?php echo " $ " .  money_format('%i', $programDetails->invoice_amount); ?> <?php //echo $programDetails->invoice_amount ?></h5>

                    <hr>
                </div> 
                
            </div>
            <hr class="customHr">
            <div class="widget-container">
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" value="<?= $programDetails->id ?>" name="program_id" />
                        <table id="lessonTable" class="table table-striped responsive display table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Length</th>
                                    <th>Status</th>
                                    <th colspan="5">Student Information</th>
                                </tr>
                            </thead>
                            <tbody>       
                                <?php 
                                    foreach ($programDetails->lessons as $key => $lesson): 
                                        $testThis = date('Ymd', strtotime($lesson->date));
                                ?>
                                    <tr>
                                        <td><?php echo date('Y/m/d', strtotime($lesson->date))  ?></td>
                                        <td><?php echo date('h:i A', strtotime($lesson->time))  ?></td>
                                        <td><?= $lesson->length ?></td>
                                        <td><?php
                                            if($lesson->status == "Tutor"):
                                                echo "Pending";
                                            else:
                                                echo $lesson->status;
                                            endif;
                                        ?></td>
                                        <td colspan="5">
                                        <button class="button btn-xs btn-primary" onclick="$('#myContent<?= $key ?>').slideToggle();">
                                            View Lesson Details
                                        </button>                                       
                                            
                                        <div id="myContent<?= $key ?>" class='hidden_studentInfo'>
                                            
                                            <table class="table">
                                                
                                            <?php if(htmlspecialchars($lesson->note) != ""): ?>
                                                <tr><td colspan="4">Session Report:</td></tr>
                                                <tr><td  style="max-width: 400px;" colspan="4"><p><?php echo htmlspecialchars($lesson->note); ?></p></td></tr>
                                            <?php endif; ?>
                                                
                                                <tr>
                                                    <th>Student</th>
                                                    <th>Type</th>
                                                    <th>Subject</th>
                                                    <th>Type</th>
                                                </tr>
                                            <?php foreach($lesson->lesson_students as $studentsInfo): ?>
                                                
                                                <tr>
                                                    <td><?= $studentsInfo->program_student->student->name ?></td>
                                                    <td>
                                                        <?php 
                                                            if(!empty($studentsInfo->assessment_type)):
                                                                echo $studentsInfo->assessment_type->type;
                                                            else:    
                                                                echo "General";
                                                            endif;                                                                        
                                                        ?>
                                                    </td>
                                                    <td>                                                    
                                                        <?php if(isset($studentsInfo->major_outcome)): ?>
                                                            <?= $studentsInfo->major_outcome->name ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>                                                    
                                                        <?php if(isset($studentsInfo->minor_outcome) && !empty($studentsInfo->assessment_type)): ?>
                                                            <?= $studentsInfo->minor_outcome->code ?>
                                                        <?php endif; ?>
                                                    </td>

                                                </tr>
                                                
                                            <?php endforeach; ?>
                                            </table>
                                        </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="lead-view-modal" class="modal fade" role="dialog"></div>
        <script>
            $(document).ready(function () {
                
               // $('#clientsTbl').dataTable({"autoWidth": false});
                $('#bookingTable').dataTable({"autoWidth": false});
                $('#lessonTable').dataTable({"autoWidth": false});
                $('#newLeadsTable').dataTable({"autoWidth": false});
                $('[data-toggle="popover"]').popover(); 
                
                $('input.enable_textarea').change(function(){
                    if ($(this).is(':checked')){ 
                        $(this).parent().children('textarea.show_textarea').attr('required', "true").show();
                    }
                    else{ 
                        $(this).parent().children('textarea.show_textarea').removeAttr('required').hide(); 
                    }
                }).change();

            });

            function toggler(divId) {
                $("#" + divId).toggle();
            }
            
        </script>
        <?php 
//debug($lesson->toArray()); 

//                        debug($this->request);
        ?>
