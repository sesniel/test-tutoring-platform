<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <div class="widget-title">
                <h2><strong>Assessments</strong></h2>
            </div>
            <hr class="customHr">
            <div class="widget-container table-responsive">
                <div class="row diffMarg">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Here you can manage your children’s pending assessments simply click ‘Take The Exam’
                                    button to begin. Note: if your child has to abandon the exam part way through they
                                    can always come back
                                    to it later using the “Continue The Exam Button”</p>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <?Php foreach ($updatedClientInfo['students'] as $key => $info): ?>
                                        <li role="presentation" <?php if ($key == 0): echo 'class="active"'; endif; ?> >
                                            <a href="#_<?= $info->id ?>" aria-controls="home" role="tab"
                                               data-toggle="tab"><h5><?= $info->name ?></h5></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="tab-content">     
                                    <?Php foreach ($updatedClientInfo['students'] as $key => $info): ?>
                                    <div role="tabpanel" class="well tab-pane <?php if ($key == 0): echo 'active'; endif; ?>" id="_<?= $info->id ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h2><?= $info->name ?></h2>
                                            </div>
                                        </div>
                                        <?php if(!empty($info['StudentAssessmentTypes'] )): ?>
                                        <div class="row">
                                            <div class="col-md-12">                                                
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Assessment Type</th>
                                                        <th>Date Created</th>
                                                        <th>Year Level</th>
                                                        <th>Status</th>
                                                        <th>Tutor Name</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($info['StudentAssessmentTypes'] as $studentAssessmentTypes): ?>
                                                    
                                                        <tr>
                                                            <td><?= $studentAssessmentTypes->assessment_type->type ?></td>
                                                            <td><?php echo date('d-m-Y', strtotime($studentAssessmentTypes->created)); ?></td>
                                                            <td><?php echo getYearLevelInfo($studentAssessmentTypes->year_level_id) ?></td>
                                                            <td><?= $studentAssessmentTypes->status ?></td>
                                                            <td><?= $studentAssessmentTypes->tutor->first_name ?> <?= $studentAssessmentTypes->tutor->last_name ?></td>
                                                            <td>
                                                                <?Php
                                                                    if($studentAssessmentTypes->status == "Pending"):
                                                                        echo $this->Html->link(
                                                                            'Take the Exam',
                                                                            array('controller' => 'assessment', 'action' => 'exam/' . $studentAssessmentTypes->id, 'prefix' => 'client', '_full' => true),
                                                                            array('escape' => false, 'class' => 'btn btn-success btn-sm')  // important
                                                                        );
                                                                    elseif($studentAssessmentTypes->status == "In Progress"):
                                                                        echo $this->Html->link(
                                                                            'Continue Exam',
                                                                            array('controller' => 'assessment', 'action' => 'exam/' . $studentAssessmentTypes->id, 'prefix' => 'client', '_full' => true),
                                                                            array('escape' => false, 'class' => 'btn btn-warning btn-sm')  // important
                                                                        );                                                                     
                                                                    else:
                                                                        echo $this->Html->link(
                                                                            "Access Exam -> " . $studentAssessmentTypes->status,
                                                                            array('controller' => 'assessment', 'action' => 'exam/' . $studentAssessmentTypes->id, 'prefix' => 'client', '_full' => true),
                                                                            array('escape' => false, 'class' => 'btn btn-warning btn-sm')  // important
                                                                        );    
                                                                    endif;
                                                                
                                                                ?>
                                                            </td>
                                                        </tr>

                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <?php else: ?>
                                            
                                        <div class="row">
                                            <div class="col-md-12">
                                                No Pending Assessments
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php // pr($updatedClientInfo); //exit; ?>
<?php 
    function getYearLevelInfo($yearLevelId){
        if($yearLevelId == 1):
            return "F";
        else:
            return $yearLevelId - 1;
        endif;
    }
?>
