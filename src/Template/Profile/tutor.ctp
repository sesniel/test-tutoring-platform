<?php 

$availabilityStatus = ['Yes', 'No'];
$hours = ['1', '2', '3', '4', '5+'];

?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <?php foreach ($userDetails as $user) : ?>
                <div class="widget-title">
                    <h2><strong>Profile</strong></h2>
                </div>
                <hr class="customHr">
                <div class="widget-container table-responsive">
                    <?= $this->Form->create(null, ['url' => ['action' => 'tutorUpdate']]); ?>
                    <div id="tutor-info" class="row">
                        <div class="row col-md-12 center-content">
                            <div class="col-md-11">
                                <h4>Contact Information:</h4>
                                <hr>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="edit-btn"
                                        class="btn btn-primary btn-xs pull-right top-buffer">Edit Details
                                </button>
                                <button type="submit" id="update-btn"
                                        class="btn btn-success btn-xs hidden pull-right top-buffer">
                                    Update
                                </button>
                            </div>
                        </div>
                        <?= $this->Form->hidden('token', ['value' => $user->token]) ?>
                        <div class="row center-indent">
                            <hr>
                            <div class="col-md-4">
                                <label for="fname">First Name:</label>
                                <input type="text" id="fname" name="first_name" value="<?= $user->first_name ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="lname">Last Name:</label>
                                <input type="text" id="lname" name="last_name" value="<?= $user->last_name ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email"
                                       value="<?= $user->email ?>" required>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <label for="mobile">Mobile:</label>
                                <input type="text" id="fname" name="mobile" value="<?= $user->mobile ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="phone">Phone:</label>
                                <input type="text" id="phone" name="phone" value="<?= $user->phone ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="street_number">Street Number:</label>
                                <input type="street_number" class="form-control" id="street_number" name="street_number"
                                       value="<?= $user->street_number ?>" required>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <label for="street_name">Street Name:</label>
                                <input type="text" id="street_name" name="street_name" value="<?= $user->street_name ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="suburb">Suburb:</label>
                                <input type="text" id="suburb" name="suburb" value="<?= $user->suburb ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="postcode">Postcode:</label>
                                <input type="postcode" class="form-control" id="postcode" name="postcode"
                                       value="<?= $user->postcode ?>" required>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-12">
                                <p>To Update your password first, confirm your current password and then enter your new password and confirm password by re-typing it:</p>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <input type="password" name="current_password" class="form-control"
                                       placeholder="Current Password">
                            </div>
                            <div class="col-md-4">
                                <input type="password" id="new_password" name="new_password" class="form-control"
                                       placeholder="New Password">
                            </div>
                            <div class="col-md-4">
                                <input type="password" id="confirm_password" name="confirm_password"
                                       class="form-control"
                                       placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="row col-md-12 center-content">
                            <div class="col-md-11">
                                <h4>General:</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <label>Working With Children Number:</label>
                                <input type="text" name="blue_card_number" class="form-control"
                                       value="<?= $user->blue_card_number ?>">
                            </div>
                            <div class="col-md-4">
                                <label>Working With Children Card Expiry:</label>
                                <input type="text" name="blue_card_expiry" class="form-control"
                                       value="<?php $date = $user->blue_card_expiry;
                                                    $expiryDate = date('Y-m-d', strtotime($date));
                                                    echo $expiryDate;?>">
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-8">
                                <label for="biography">Biography:</label>
                                <textarea name="biography"  title="comments"class="form-control" id="exampleTextarea" rows="3"><?= $user->biography ?></textarea><br> 
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-12">
                                <hr>
                                <h4>Availability</h4>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <label for="rate_name">Available for new clients:</label>
                                <select name="availability" id="training" class="form-control">
                                <?php foreach($availabilityStatus as $stats): ?>
                                   <option <?php $sel = ($user->availability === $stats ? "selected" : ""); ?> <?= $sel ?> value="<?= $stats ?>"><?= $stats ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label for="rate_name">How many more hours would you like?</label>
                                <select name="hours" id="training" class="form-control">
                                <?php foreach($hours as $stats): ?>
                                   <option <?php $sel = ($user->hours === $stats ? "selected" : ""); ?> <?= $sel ?> value="<?= $stats ?>"><?= $stats ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-12">
                                <label for="subjects">List of Subjects:</label>
                                <div class="subjects">
                                    <select multiple id="available-subjects" name="subjects[]"
                                            class="form-control">
                                       <?php foreach($subjectDetails as $subject): ?>
                                        <option value="<?= $subject->subject->id ?>" selected=""> <?= $subject->subject->name ?></option>
                                        <?php endforeach;
                                        foreach($subjectsList as $subjList): ?>
                                        <option value="<?= $subjList->id ?>"><?= $subjList->name ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->Html->script('tutor/profile.js'); ?>