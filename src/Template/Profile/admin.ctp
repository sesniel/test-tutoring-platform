<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="widget-box">
            <?php foreach ($userDetails as $user) : ?>
                <div class="widget-title">
                    <h2><strong>Profile</strong><span class="pull-right"><?= $user->email ?></span></h2>
                </div>
                <hr class="customHr">
                <div class="widget-container table-responsive">
                    <?= $this->Form->create(null, ['url' => ['action' => 'adminUpdate']]); ?>
                    <div id="info" class="row">
                        <div class="row col-md-12 center-content">
                            <div class="col-md-11">
                                <h4>Details:</h4>
                                <hr>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="edit-btn"
                                        class="btn btn-primary btn-xs pull-right top-buffer">Edit Details
                                </button>
                                <button type="submit" id="update-btn"
                                        class="btn btn-success btn-xs hidden pull-right top-buffer">
                                    Update
                                </button>
                            </div>
                        </div>
                        <?= $this->Form->hidden('token', ['value' => $user->token]) ?>
                        <div class="row center-indent">
                            <hr>
                            <div class="col-md-4">
                                <label for="fname">First Name:</label>
                                <input type="text" id="fname" name="first_name" value="<?= $user->first_name ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="lname">Last Name:</label>
                                <input type="text" id="lname" name="last_name" value="<?= $user->last_name ?>"
                                       class="form-control" required="required">
                            </div>
                            <div class="col-md-4">
                                <label for="email">Email:</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       value="<?= $user->email ?>" required>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-12">
                                <p>To Update your password first, confirm your current password and then enter your new password and confirm password by re-typing it:</p>
                            </div>
                        </div>
                        <div class="row center-indent">
                            <div class="col-md-4">
                                <input type="password" name="current_password" class="form-control"
                                       placeholder="Current Password">
                            </div>
                            <div class="col-md-4">
                                <input type="password" id="new_password" name="new_password" class="form-control"
                                       placeholder="New Password">
                            </div>
                            <div class="col-md-4">
                                <input type="password" id="confirm_password" name="confirm_password"
                                       class="form-control"
                                       placeholder="Confirm Password">
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?= $this->Html->script('admin/profile.js'); ?>