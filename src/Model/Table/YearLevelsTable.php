<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * YearLevels Model
 *
 * @property \Cake\ORM\Association\HasMany $MajorOutcomes
 * @property \Cake\ORM\Association\HasMany $StudentAssessmentTypes
 * @property \Cake\ORM\Association\HasMany $Students
 *
 * @method \App\Model\Entity\YearLevel get($primaryKey, $options = [])
 * @method \App\Model\Entity\YearLevel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\YearLevel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\YearLevel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\YearLevel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\YearLevel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\YearLevel findOrCreate($search, callable $callback = null)
 */
class YearLevelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('year_levels');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('MajorOutcomes', [
            'foreignKey' => 'year_level_id'
        ]);
        $this->hasMany('StudentAssessmentTypes', [
            'foreignKey' => 'year_level_id'
        ]);
        $this->hasMany('Students', [
            'foreignKey' => 'year_level_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        return $validator;
    }
}
