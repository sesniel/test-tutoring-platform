<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TrainingPages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Tutors
 * @property \Cake\ORM\Association\BelongsTo $Trainings
 *
 * @method \App\Model\Entity\TrainingPage get($primaryKey, $options = [])
 * @method \App\Model\Entity\TrainingPage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TrainingPage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TrainingPage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TrainingPage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TrainingPage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TrainingPage findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TrainingPagesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('training_pages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Tutors', [
            'foreignKey' => 'tutor_id'
        ]);
        $this->belongsTo('Trainings', [
            'foreignKey' => 'training_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tutor_id'], 'Tutors'));
        $rules->add($rules->existsIn(['training_id'], 'Trainings'));

        return $rules;
    }
}
