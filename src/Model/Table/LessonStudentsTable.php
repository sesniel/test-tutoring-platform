<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Lessons Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Programs
 * @property \Cake\ORM\Association\BelongsTo $MajorOutcomes
 * @property \Cake\ORM\Association\BelongsTo $AssessmentTypes
 * @property \Cake\ORM\Association\BelongsTo $Statuses
 * @property \Cake\ORM\Association\HasMany $LessonResources
 *
 * @method \App\Model\Entity\Lesson get($primaryKey, $options = [])
 * @method \App\Model\Entity\Lesson newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Lesson[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Lesson|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Lesson patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Lesson[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Lesson findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LessonStudentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lesson_students');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ProgramStudents', [
            'foreignKey' => 'program_student_id'
        ]);
        $this->belongsTo('MajorOutcomes', [
            'foreignKey' => 'major_outcome_id'
        ]);
        $this->belongsTo('MinorOutcomes', [
            'foreignKey' => 'minor_outcome_id'
        ]);
        $this->belongsTo('AssessmentTypes', [
            'foreignKey' => 'assessment_type_id'
        ]);
        $this->hasMany('LessonResources', [
            "className" => "LessonResources",
            'foreignKey' => 'lesson_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->integer('id')
//            ->allowEmpty('id', 'create');
//
//        $validator
//            ->allowEmpty('length');
//
//        $validator
//            ->date('date')
//            ->requirePresence('date', 'create')
//            ->notEmpty('date');
//
//        $validator
//            ->time('time')
//            ->requirePresence('time', 'create')
//            ->notEmpty('time');
//
//        $validator
//            ->allowEmpty('topic');
//
//        $validator
//            ->email('email')
//            ->allowEmpty('email');
//
//        $validator
//            ->allowEmpty('note');
//
//        return $validator;
//    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        
//        $rules->add($rules->existsIn(['program_student_id'], 'ProgramStudents'));
//
//        return $rules;
//    }
}
