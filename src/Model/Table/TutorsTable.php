<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Statuses
 * @property \Cake\ORM\Association\HasMany $Availabilities
 * @property \Cake\ORM\Association\HasMany $Feedbacks
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TutorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Rates', [
            'foreignKey' => 'rate_id'
        ]);
        $this->belongsTo('Owner', [
            "className" => "Users",
            "fields" => ['Owner.first_name', 'Owner.last_name']
        ]);
        $this->hasMany('Availabilities', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TutorSubjects', [
            'foreignKey' => 'tutor_id'
        ]);
        $this->hasMany('Feedbacks', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasOne('ModuleUsers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasOne('ApplicationAnswers', [
            'foreignKey' => 'application_id'
        ]);
        $this->hasMany('Leads', [
            'foreignKey' => 'tutor_id'
        ]);
        $this->hasMany('LeadPayroll', [
            'foreignKey' => 'tutor_id',
            "className" => "Leads",
            'conditions' => ['LeadPayroll.status' => 'Booked']
        ]);
        $this->hasMany('LeadPayrollAll', [
            'foreignKey' => 'tutor_id',
            "className" => "Leads"
        ]);
        $this->hasMany('Programs', [
            'foreignKey' => 'tutor_id',
            'conditions' => ['Programs.status != ' => 'Pending']
        ]);
        $this->hasMany('ProgramsLessons', [
            'foreignKey' => 'tutor_id',
            'conditions' => ['ProgramsLessons.status != ' => 'Cancelled'],
            'className' => 'Programs'
        ]);
        $this->hasMany('BookingPrograms', [
            "className" => "Programs",
            'foreignKey'=> 'tutor_id',
            'conditions' => ['BookingPrograms.status not in ' => ['Cancelled', 'Completed']]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->allowEmpty('phone');

        $validator
            ->allowEmpty('mobile');

        $validator
            ->allowEmpty('unit_number');

        $validator
            ->allowEmpty('street_number');

        $validator
            ->allowEmpty('street_name');

        $validator
            ->allowEmpty('suburb');

        $validator
            ->allowEmpty('postcode');

        $validator
            ->allowEmpty('city');

        $validator
            ->allowEmpty('state');

        $validator
            ->boolean('study_skill')
            ->requirePresence('study_skill', 'create')
            ->notEmpty('study_skill');

        $validator
            ->integer('login_status')
            ->requirePresence('login_status', 'create')
            ->notEmpty('login_status');

        $validator
            ->allowEmpty('blue_card_number');

        $validator
            ->allowEmpty('blue_card_expiry');

        $validator
            ->allowEmpty('availability');

        $validator
            ->allowEmpty('biography');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('token', 'create')
            ->notEmpty('token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
