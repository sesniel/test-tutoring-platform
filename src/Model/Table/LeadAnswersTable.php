<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class LeadAnswersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lead_answers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('lead_id', 'create')
            ->notEmpty('lead_id');

        $validator
            ->requirePresence('lead_question_id', 'create')
            ->notEmpty('lead_question_id');

        $validator
            ->requirePresence('question', 'create')
            ->notEmpty('question');

        $validator
            ->allowEmpty('answer');

        return $validator;
    }

}
