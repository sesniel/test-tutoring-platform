<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Consultations Model
 *
 * @property \Cake\ORM\Association\HasMany $LeadAnswers
 * @property \Cake\ORM\Association\HasMany $Leads
 *
 * @method \App\Model\Entity\Consultation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Consultation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Consultation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Consultation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consultation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Consultation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Consultation findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConsultationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

//        $this->table('consultations');
//        $this->displayField('id');
//        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('LeadAnswers', [
            'foreignKey' => 'consultation_id'
        ]);
        $this->hasMany('Leads', [
            'foreignKey' => 'consultation_id'
        ]);
        $this->hasMany('AbandonedLeads', [
            'foreignKey' => 'consultation_id',
            'conditions' => ['AbandonedLeads.status !=' => 'Abandoned'],
            "className" => "Leads"
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('consultation_session_date')
            ->requirePresence('consultation_session_date', 'create')
            ->notEmpty('consultation_session_date');

        $validator
            ->requirePresence('token', 'create')
            ->notEmpty('token');

        $validator
            ->requirePresence('payroll_date', 'create')
            ->notEmpty('payroll_date');

        $validator
            ->requirePresence('length', 'create')
            ->notEmpty('length');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
