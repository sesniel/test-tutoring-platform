<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LeadSubjects Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Leads
 * @property \Cake\ORM\Association\BelongsTo $Subjects
 *
 * @method \App\Model\Entity\LeadSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\LeadSubject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LeadSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LeadSubject|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LeadSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LeadSubject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LeadSubject findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LeadSubjectsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lead_subjects');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Leads', [
            'foreignKey' => 'lead_id'
        ]);
        $this->belongsTo('Subjects', [
            'foreignKey' => 'subject_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->integer('id')
//            ->allowEmpty('id', 'create');
//
//        return $validator;
//    }
//
//    /**
//     * Returns a rules checker object that will be used for validating
//     * application integrity.
//     *
//     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
//     * @return \Cake\ORM\RulesChecker
//     */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->existsIn(['lead_id'], 'Leads'));
//        $rules->add($rules->existsIn(['subject_id'], 'Subjects'));
//
//        return $rules;
//    }
}
