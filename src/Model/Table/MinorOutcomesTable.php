<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MinorOutcomes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $MajorOutcomes
 * @property \Cake\ORM\Association\HasMany $Questions
 * @property \Cake\ORM\Association\HasMany $Resources
 *
 * @method \App\Model\Entity\MinorOutcome get($primaryKey, $options = [])
 * @method \App\Model\Entity\MinorOutcome newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MinorOutcome[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MinorOutcome|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MinorOutcome patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MinorOutcome[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MinorOutcome findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MinorOutcomesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('minor_outcomes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('MajorOutcomes', [
            'foreignKey' => 'major_outcome_id'
        ]);
        $this->hasMany('Questions', [
            'foreignKey' => 'minor_outcome_id'
        ]);
        $this->hasMany('Resources', [
            'foreignKey' => 'minor_outcome_id'
        ]);
        $this->hasMany('LessonResources', [
            'foreignKey' => 'resources_id'
        ]);
        $this->hasMany('UserQuestions', [
            'foreignKey' => 'minor_outcome_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('code');

        $validator
            ->allowEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['major_outcome_id'], 'MajorOutcomes'));

        return $rules;
    }
}
