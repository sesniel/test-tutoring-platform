<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class LeadQuestionsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lead_questions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->hasMany('lead_answers', [
            'foreignKey' => 'lead_question_id'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('question');

        return $validator;
    }
}
