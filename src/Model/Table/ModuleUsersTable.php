<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ModuleUsers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $ModuleUserQuestions
 *
 * @method \App\Model\Entity\ModuleUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\ModuleUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ModuleUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ModuleUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ModuleUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ModuleUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ModuleUser findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ModuleUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('module_users');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Tutors', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ModuleUserQuestions', [
            'foreignKey' => 'module_user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
//    public function validationDefault(Validator $validator)
//    {
//        $validator
//            ->integer('id')
//            ->requirePresence('id', 'create')
//            ->notEmpty('id');
//
//        $validator
//            ->requirePresence('status', 'create')
//            ->notEmpty('status');
//
//        return $validator;
//    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->existsIn(['user_id'], 'Users'));
//
//        return $rules;
//    }
}
