<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StudentAssessmentTypes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Students
 * @property \Cake\ORM\Association\BelongsTo $AssessmentTypes
 * @property \Cake\ORM\Association\BelongsTo $YearLevels
 * @property \Cake\ORM\Association\BelongsTo $Statuses
 * @property \Cake\ORM\Association\HasMany $UserQuestions
 *
 * @method \App\Model\Entity\StudentAssessmentType get($primaryKey, $options = [])
 * @method \App\Model\Entity\StudentAssessmentType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StudentAssessmentType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StudentAssessmentType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StudentAssessmentType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StudentAssessmentType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StudentAssessmentType findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StudentAssessmentTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('student_assessment_types');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Students', [
            'foreignKey' => 'student_id'
        ]);
        $this->belongsTo('AssessmentTypes', [
            'foreignKey' => 'assessment_type_id'
        ]);
        $this->belongsTo('Tutors', [
            'foreignKey' => 'tutor_id'
        ]);
        $this->belongsTo('YearLevels', [
            'foreignKey' => 'year_level_id'
        ]);
        $this->hasMany('UserQuestions', [
            'foreignKey' => 'student_assessment_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Students'));
        $rules->add($rules->existsIn(['assessment_type_id'], 'AssessmentTypes'));
        $rules->add($rules->existsIn(['year_level_id'], 'YearLevels'));

        return $rules;
    }
}
