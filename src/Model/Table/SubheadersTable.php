<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Subheaders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Headers
 * @property \Cake\ORM\Association\HasMany $Pdf
 *
 * @method \App\Model\Entity\Subheader get($primaryKey, $options = [])
 * @method \App\Model\Entity\Subheader newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Subheader[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Subheader|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Subheader patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Subheader[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Subheader findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SubheadersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('subheaders');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Headers', [
            'foreignKey' => 'header_id'
        ]);
        $this->hasMany('Pdf', [
            'foreignKey' => 'subheader_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->requirePresence('link', 'create')
            ->notEmpty('link');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['header_id'], 'Headers'));

        return $rules;
    }
}
