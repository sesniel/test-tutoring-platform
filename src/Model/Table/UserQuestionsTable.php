<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $StudentAssessmentTypes
 *
 * @method \App\Model\Entity\UserQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\UserQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\UserQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\UserQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\UserQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\UserQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\UserQuestion findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UserQuestionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('user_questions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('StudentAssessmentTypes', [
            'foreignKey' => 'student_assessment_type_id'
        ]);
        
        $this->belongsTo('MinorOutcomes', [
            'foreignKey' => 'minor_outcome_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('position')
            ->allowEmpty('position');

        $validator
            ->allowEmpty('question');

        $validator
            ->allowEmpty('correct_answer');

        $validator
            ->allowEmpty('user_answer');

        $validator
            ->allowEmpty('choices');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_assessment_type_id'], 'StudentAssessmentTypes'));

        return $rules;
    }
}
