<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Programs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Students
 * @property \Cake\ORM\Association\BelongsTo $Tutors
 * @property \Cake\ORM\Association\BelongsTo $Statuses
 * @property \Cake\ORM\Association\HasMany $Lessons
 * @property \Cake\ORM\Association\HasMany $ReassessmentQuestions
 *
 * @method \App\Model\Entity\Program get($primaryKey, $options = [])
 * @method \App\Model\Entity\Program newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Program[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Program|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Program patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Program[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Program findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProgramsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('programs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Students', [
            'foreignKey' => 'student_id'
        ]);
        $this->belongsTo('Tutors', [
            'foreignKey' => 'tutor_id'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id'
        ]);
        $this->belongsTo('Rates', [
            'foreignKey' => 'rate_id'
        ]);
        $this->hasMany('Lessons', [
            'foreignKey' => 'program_id',
            'dependent' => true,
//            'cascadeCallbacks' => true,
            'sort' => ['date' => 'asc']
        ]);
        $this->hasMany('LessonsPending', [
            'foreignKey' => 'program_id',
            "className" => "Lessons",
            'conditions' => ['LessonsPending.status' => 'Pending']
        ]);
        $this->hasMany('LessonClient', [
            'foreignKey' => 'program_id',
            'conditions' => ['LessonClient.status in' => array('Completed', 'Pending')],
            'className' => 'Lessons',
//            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('ProgramStudents', [
            'foreignKey' => 'program_id'
        ]);
        $this->hasMany('RebookLessons', [
            'foreignKey' => 'program_id',
            'conditions' => ['RebookLessons.status in' => array('Pending', 'Scheduled')],
            "className" => "Lessons",
            'sort' => ['date' => 'desc']
        ]);
        $this->hasMany('LessonTutor', [
            'foreignKey' => 'program_id',
            'conditions' => ['LessonTutor.status' => "Scheduled", 'LessonTutor.date <= ' => date("Y-m-d H:i:s", strtotime('-3 days') )],
            "className" => "Lessons",
            'sort' => ['date' => 'desc']
        ]);
        $this->hasMany('PayrollLessons', [
            'foreignKey' => 'program_id',
            'conditions' => ['PayrollLessons.payroll_date' => '', 'PayrollLessons.status' => 'Completed'],
            "className" => "Lessons"
        ]);
        $this->hasMany('Bookings', [
            'foreignKey' => 'program_id',
//            'conditions' => ['Bookings.status != ' => 'Done'],
            "className" => "Lessons",
            'sort' => ['date' => 'asc']
        ]);
        $this->hasMany('PayrollLessonsInfo', [
            'foreignKey' => 'program_id',
            'conditions' => ['PayrollLessonsInfo.payroll_date != ' => ''],
            "className" => "Lessons"
        ]);
        $this->hasMany('ReassessmentQuestions', [
            'foreignKey' => 'program_id',
            'dependent' => true,
//            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('ProgramStudents', [
            'foreignKey' => 'program_id',
//            'dependent' => true,
//            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('ProgramBookings', [
            "className" => "Lessons",
            'foreignKey' => 'program_id',
            'conditions' => ['ProgramBookings.date <= ' => date("Y-m-d 00:00:01", strtotime('-1 day', strtotime(date("Y-m-d")))), 'ProgramBookings.status not in ' => ['Cancelled', 'Completed', 'Pending']]
        ]);
        $this->hasMany('ProgramBookingsLessons', [
            "className" => "Lessons",
            'foreignKey' => 'program_id',
            'conditions' => ['ProgramBookingsLessons.date <= ' => date("Y-m-d 00:00:01", strtotime('-2 day', strtotime(date("Y-m-d")))), 'ProgramBookingsLessons.status' => "Scheduled"]
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Students'));
        $rules->add($rules->existsIn(['tutor_id'], 'Tutors'));

        return $rules;
    }
}
