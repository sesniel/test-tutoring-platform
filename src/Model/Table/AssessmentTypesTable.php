<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AssessmentTypes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Subjects
 * @property \Cake\ORM\Association\HasMany $Lessons
 * @property \Cake\ORM\Association\HasMany $StudentAssessmentTypes
 *
 * @method \App\Model\Entity\AssessmentType get($primaryKey, $options = [])
 * @method \App\Model\Entity\AssessmentType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AssessmentType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AssessmentType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AssessmentType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AssessmentType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AssessmentType findOrCreate($search, callable $callback = null)
 */
class AssessmentTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('assessment_types');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Subjects', [
            'foreignKey' => 'subject_id'
        ]);
        $this->hasMany('Lessons', [
            'foreignKey' => 'assessment_type_id'
        ]);
        $this->hasMany('StudentAssessmentTypes', [
            'foreignKey' => 'assessment_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['subject_id'], 'Subjects'));

        return $rules;
    }
}
