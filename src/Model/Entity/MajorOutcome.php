<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MajorOutcome Entity
 *
 * @property int $id
 * @property int $category_id
 * @property int $subject_id
 * @property int $year_level_id
 * @property string $name
 * @property string $description
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\YearLevel $year_level
 * @property \App\Model\Entity\Lesson[] $lessons
 * @property \App\Model\Entity\MinorOutcome[] $minor_outcomes
 */
class MajorOutcome extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
