<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lead Entity
 *
 * @property int $id
 * @property int $consultation_id
 * @property int $client_id
 * @property string $status
 * @property int $tutor_id
 * @property int $student_id
 * @property string $note
 * @property \Cake\I18n\Time $consultation_session_date
 * @property string $payroll_date
 * @property string $length
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Consultation $consultation
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\Tutor $tutor
 * @property \App\Model\Entity\Student $student
 * @property \App\Model\Entity\LeadAnswer[] $lead_answers
 * @property \App\Model\Entity\LeadSubject[] $lead_subjects
 */
class Lead extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
