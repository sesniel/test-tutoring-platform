<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $type
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $mobile
 * @property string $unit_number
 * @property string $street_number
 * @property string $street_name
 * @property string $suburb
 * @property string $postcode
 * @property string $city
 * @property string $state
 * @property bool $study_skill
 * @property int $login_status
 * @property string $blue_card_number
 * @property string $blue_card_expiry
 * @property string $availability
 * @property int $status_id
 * @property string $biography
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $password
 * @property string $token
 *
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Availability[] $availabilities
 * @property \App\Model\Entity\Feedback[] $feedbacks
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];
}
