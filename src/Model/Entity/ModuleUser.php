<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ModuleUser Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ModuleUserQuestion[] $module_user_questions
 */
class ModuleUser extends Entity
{

}
