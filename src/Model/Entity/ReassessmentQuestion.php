<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReassessmentQuestion Entity
 *
 * @property int $id
 * @property int $program_id
 * @property string $description
 * @property string $image
 * @property string $answer
 * @property string $choices
 * @property string $mark
 * @property string $type
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Program $program
 */
class ReassessmentQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
