<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Program Entity
 *
 * @property int $id
 * @property int $student_id
 * @property int $tutor_id
 * @property int $status_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Student $student
 * @property \App\Model\Entity\Tutor $tutor
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\Lesson[] $lessons
 * @property \App\Model\Entity\ReassessmentQuestion[] $reassessment_questions
 */
class Program extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
