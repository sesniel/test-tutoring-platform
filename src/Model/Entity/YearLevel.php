<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * YearLevel Entity
 *
 * @property int $id
 * @property string $name
 *
 * @property \App\Model\Entity\MajorOutcome[] $major_outcomes
 * @property \App\Model\Entity\StudentAssessmentType[] $student_assessment_types
 * @property \App\Model\Entity\Student[] $students
 */
class YearLevel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
