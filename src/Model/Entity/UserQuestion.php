<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserQuestion Entity
 *
 * @property int $id
 * @property int $student_assessment_type_id
 * @property int $position
 * @property string $question
 * @property string $correct_answer
 * @property string $user_answer
 * @property string $choices
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\StudentAssessmentType $student_assessment_type
 */
class UserQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
