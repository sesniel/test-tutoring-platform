<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lesson Entity
 *
 * @property int $id
 * @property int $program_id
 * @property int $major_outcome_id
 * @property int $assessment_type_id
 * @property int $status_id
 * @property string $length
 * @property \Cake\I18n\Time $date
 * @property \Cake\I18n\Time $time
 * @property string $topic
 * @property string $email
 * @property string $note
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Program $program
 * @property \App\Model\Entity\MajorOutcome $major_outcome
 * @property \App\Model\Entity\AssessmentType $assessment_type
 * @property \App\Model\Entity\Status $status
 * @property \App\Model\Entity\LessonResource[] $lesson_resources
 */
class Lesson extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
