<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LessonResource Entity
 *
 * @property int $id
 * @property int $resources_id
 * @property int $lesson_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Resource $resource
 * @property \App\Model\Entity\Lesson $lesson
 */
class LessonResource extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
