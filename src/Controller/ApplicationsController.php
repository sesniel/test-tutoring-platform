<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use DateTime;
use Cake\Utility\Text;
use Cake\Mailer\Email;

class ApplicationsController extends AppController {
    
    public function index() {

        $statesTbl = TableRegistry::get('States');
        $subjsTbl = TableRegistry::get('Subjects');
        $states = $statesTbl->find('all');
        $subjs = $subjsTbl->find('all', ['conditions' => ['Subjects.id >' => '4']]);
       
        
//        debug($states->toArray()); exit;
        $this->set(compact('states', 'subjs'));
        $this->viewBuilder()->layout('application');
       
    }
    
    public function getAToken()
    {

        $usersTbl = TableRegistry::get('Users');
        do {
            $token = md5(rand(0, 9999));

            $tokenQuery = $usersTbl->find("all", [
                "conditions" => [
                    "Users.token" => $token
                ]
            ])->first();

        } while ($tokenQuery);
        return $token;

    }
   
    public function upload () {
    //         $path = WWW_ROOT."web/resume";
    // echo $path;exit;
        $this->viewBuilder()->layout('application');
    if(!empty($_FILES['resume'])){
        if ($_FILES['resume']['type'] == 'application/pdf') {
        $path = WWW_ROOT."web/resume/";
        $filename = $_FILES['resume']['name'];
        // echo $path;exit;
        $path = $path . basename( $_FILES['resume']['name']);
            if(move_uploaded_file($_FILES['resume']['tmp_name'], $path)) {
              echo "The file ".  basename( $filename) . 
              " has been uploaded<br>";
             // $filename =  basename( $_FILES['resume']['name']);
             echo $filename;

            }else{
            echo "There was an error uploading the file, please try again!";
            }
            } else {
            echo 'not pdf file';
            }
    }
    }
    
   
    private function generatePasswordString($length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
    

    public function submitApplication() {
        
        
       $usersTbl = TableRegistry::get('Users');
       $checkEmail = $usersTbl->find('all', ['conditions' => [
           'Users.email' => $this->request->data('email')
       ]])->first();
       
       if ($checkEmail) {
           return $this->redirect($this->referer() . "#email-exists"); 
       } else {
           
       $applicantData = $usersTbl->newEntity();
       $token = $this->getAToken();
//       $passwordGen = $this->generatePasswordString(7);
           
       $applicantData->type = "Applicant";
       $applicantData->first_name = $this->request->data('fname');
       $applicantData->last_name = $this->request->data('lname');
       $applicantData->mobile = $this->request->data('mobile');
       $applicantData->phone = $this->request->data('a_mobile');
       $applicantData->email = $this->request->data('email');
       $applicantData->street_number = $this->request->data('street_number');
       $applicantData->street_name = $this->request->data('street');
       $applicantData->suburb = $this->request->data('suburb');
       $applicantData->city = $this->request->data('city');
       $applicantData->postcode = $this->request->data('postcode');
       $applicantData->state_id = $this->request->data('state');
       
       
       
        if ($this->request->data('state') === 6) {
            $applicantData->owner_id = 735;
            $contactOwnerId = 735;
        } else {
            $applicantData->owner_id = 417;
            $contactOwnerId = 417;
        }
       
       $applicantData->blue_card_number = $this->request->data('blue_card_number');
       
       if(!empty($this->request->data('blue_card_expiry'))):
         $applicantData->blue_card_expiry = date('Y-m-d', strtotime($this->request->data('blue_card_expiry')));  
       else:
           $applicantData->blue_card_expiry = null;
       endif;
       
       $applicantData->token = $token;
       $applicantData->access = 0;
//       $applicantData->password = crypt($passwordGen, 99);
       //$applicant->subject_id = $this->request->data('subjects[]');
       $applicantDetails = $usersTbl->save($applicantData);
       $this->changeUserAddress($applicantDetails->id);

//     debug($applicantData->toArray()); exit;
        if ($applicantDetails) {
          
            $userID = $applicantDetails->id; //id on Users tbl
            
            $applicationsTbl = TableRegistry::get('Applications');
            $applicantSubjsTbl = TableRegistry::get('TutorSubjects');
            $subjects = $this->request->data('subjects');
            
            foreach ($subjects as $subject):
                $applicantSubjects = $applicantSubjsTbl->newEntity();
                $applicantSubjects->tutor_id = $userID;
                $applicantSubjects->subject_id = $subject;
                $applicantSubjsTbl->save($applicantSubjects);
            endforeach;
            
            $applicant = $applicationsTbl->newEntity();
            $applicant->user_id = $userID;
            $applicantID = $applicant->user_id;
            $applicant->status = 'New';
               
        if(!empty($_FILES['resume'])){
       
            if ($_FILES['resume']['type'] == 'application/pdf') {
                //$path = "/public_html/staging/webroot/web/resume";
                $path = WWW_ROOT."web/resume/";
                $path = $path . $applicantID . basename( $_FILES['resume']['name']);
                    if(move_uploaded_file($_FILES['resume']['tmp_name'], $path)) {
//                    $fileName = $applicantID . basename( $_FILES['resume']['name']);
//                    $applicant->resume = $fileName;
                    $applicant->resume = $applicantID . basename( $_FILES['resume']['name']);
               
                    }
            } else {
                $this->redirect($this->referer() . '#pdf');
            }
        } 

            $applicationDetails = $applicationsTbl->save($applicant);
//            debug($applicationDetails->toArray()); exit;
            $applicationAnswersTbl = TableRegistry::get('ApplicationAnswers');
            $applicationAnswer = $applicationAnswersTbl->newEntity();
            $applicationAnswer->application_id = $applicantID;
  
//          $applicationAnswer->details = $this->request->data('ans_1');
           
            $answers = array (
                    'HighSchoolResultOP_ATAR' => $this->request->data('ans_1'),
                    'GPA' => $this->request->data('ans_2'),
                    'Explain_why_you_thinks_you_would_make_an_excellent_tutor_for_these_subjects' => $this->request->data('ans_3'),
                    'Are_you_currently_studying_if_so_what' => $this->request->data('ans_4'),
                    'Do_you_have_a_drivers_license_and_access_to_your_own_car' => $this->request->data('ans_5'),
                    'Where_are_you_studying' => $this->request->data('ans_6'),
                    'How_many_hours_a_week_would_you_like_to_tutor' => $this->request->data('ans_7'),
                    'Describe_how_you_would_use_your_personal_experience_to_assist_your_students_and_why_you_believe_it_would_help_them' => $this->request->data('ans_8'),
                    'Given_sessions_are_often_1_to_2_hours_long_how_long_would_you_be_comfortable_travelling_in_order_to_get_to_a_session' => $this->request->data('ans_9'),
                    'Lastly_please_describe_why_you_would_like_to_become_an_Tutor2You_Tutor_and_why_do_you_think_you_would_represent_our_company_well_with_clients' => $this->request->data('ans_10')
           
                    );
          
            if (array_search('High School Result (OP/ATAR):', $answers)) {
                $answers->type = 'Personal Background';
            }
            
          $json = json_encode($answers);
          $applicationAnswer->details = $json;
          
         
         $applicationAnswersTbl->save($applicationAnswer);
         $this->applicationNotification($contactOwnerId);
         
        } 
//        return $this->redirect($this->referer() . "#submitted"); 
         return $this->redirect("http://www.tutor2you.com.au/thankyou-tutor-application/");
       }
       
    }
    
    public function changeUserAddress($id){
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->get($id);
        
        $userUpdate = $usersTable->get($userDetails->id);
        $address = $userUpdate->street_name . " " . $userUpdate->suburb . " " .  $userUpdate->city;

        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";
        $xml = simplexml_load_file($request_url) or die("url not loading ID: " . $userUpdate->id . " ==> " . $address);
        $status = $xml->status;
        if ($status=="OK") {
            $Lat = $xml->result->geometry->location->lat;
            $Lon = $xml->result->geometry->location->lng;
        }
        $userUpdate->lng = $Lon;
        $userUpdate->lat = $Lat;
        $userUpdate->address = $address;
        $usersTable->save($userUpdate);

       
    }
    
    
    
}
