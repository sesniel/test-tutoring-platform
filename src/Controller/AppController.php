<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;

class AppController extends Controller
{
    
    public $url = 'https://tutor2you.api-us1.com';
    public $apiId = '90c5519bb5a8b5ca412362468188cf26155bcabc430972cfa949bf485488ece36ecec7bc';
    
    public function crudContactList($post, $type="add"){

        $params = array(
            
            'api_key'      => $this->apiId,
            'api_action'   => 'contact_'.$type,
            'api_output'   => 'serialize',
            
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $url = rtrim($this->url, '/ ');

        if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            die('JSON not supported. (introduced in PHP 5.2.0)');
        }

        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object

        if ( !$response ) {
            die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }

        $result = unserialize($response);
        return $result;

    }
    
    public function getContactDetails($apiAction = 'contact_list'){        
        //user_list, 
        //original -> contact_list
        $params = array(

            'api_key'       => $this->apiId,
            'api_action'    => $apiAction,
            'api_output'    => 'serialize',
//            'ids'           => 'all',
            'filters[listid]' => 11,
            'filters[email]' => 'joshua@tutor2you.com.au',
//            'sort_direction'=> 'DESC',
//            'page' => $page,
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // clean up the url
        $url = rtrim($this->url, '/ ');
 
       // define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object
        
        $result = unserialize($response);
        
        return $result;

    }

    public function getContactEmail($email = 'admin@tutor2you.com.au'){
        

        $params = array(

            'api_key'      => $this->apiId,
            'api_action'   => 'contact_view_email',
            'api_output'   => 'serialize',
            'email'        => $email,
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // clean up the url
        $url = rtrim($this->url, '/ ');
 
       // define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object

        $result = unserialize($response);
        
        return $result;

    }
    
    public function checkName($name){
        
        if (substr($name, 0, 1) === '['):
            return "";
        else:    
            return $name;
        endif;
        
    }
    
    public function checkUserInfo($email){
        
        $UsersTable = TableRegistry::get('Users');
        $userDetails = $UsersTable->findByEmail($email)->first();
        if(!empty($userDetails)):
            return $userDetails->id;
        else:    
            return "";
        endif;
        
    }
    
    public function calculateSubject($user){
        
        $subject = "";
        $x = 1;
        foreach($user->students as $student):
            
            if(count($user->students) == $x):
                $subject .= $student->year_level->name;
            else:    
                $subject .= $student->year_level->name.",";
            endif;
            $x++;
            
        endforeach;
//        debug($user);
//        echo $subject;exit;
        return $subject;
        
    }
    
    public function initialize()
    {
        parent::initialize();
        //check
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    public function checkLoginStatus()
    {
        $type = ($this->request->session()->check("type") ? $this->request->session()->read("type") : 0);
        if (!$type) {
            $this->redirect("/");
        } else {

            switch ($type) {
                case 'Admin':
                    $this->viewBuilder()->layout('admin');
                    break;
                case 'Tutor':
                    $this->viewBuilder()->layout('tutor');
                    break;
                case 'Client':
                    $this->viewBuilder()->layout('client');
                    break;
            }

            return strtolower($type);
        }

    }

    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                 Tutor2You | Primary and Secondary | One-on-One Tutoring
                 Email: support@tutor2you.com.au | Phone: 1300 4200 79',
            'rosette@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }

    private function getUserDetails($token)
    {
        $UserTable = TableRegistry::get('Clients');
        $userDetails = $UserTable->findByToken($token)->first();
        return $userDetails;
    }

    public function emailPasswordNotif($token, $password)
    {
        $emailInfo = $this->emailInfo();
        $userDetails = $this->getUserDetails($token);
        $subject = 'Password Reset';
        $message = '

                Hi ' . $userDetails->first_name . ',

                You recently requested to reset your password for your account. 
                
                This is your login details and new password:
                Email: ' . $userDetails->email . '
                Password: ' . $password . '
                   
                You can login here ' . $emailInfo[0] . '

                ' . $emailInfo[2] . '

                ' . $emailInfo[3] . '

                ';

        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to($userDetails->email)
            ->bcc($emailInfo[4])
            ->addBcc($emailInfo[5])
            ->subject($subject)
            ->send($message);


    }

    private function getFeedBackDetails($id)
    {
        $feedBacksTable = TableRegistry::get('Feedbacks');
        $feedbackDetails = $feedBacksTable->get($id, [
            'contain' => ['Users']
        ]);
        return $feedbackDetails;
    }

    public function emailSupport($feedBackId)
    {
        $emailInfo = $this->emailInfo();
        $feedBackDetails = $this->getFeedBackDetails($feedBackId);
        $subject = 'Tutor2You Support:'.$feedBackDetails->subject;
        $message = '

                '.$feedBackDetails->description.'


                Details: 
                Page: '.$feedBackDetails->page.'
                User Name: '.$feedBackDetails->user->first_name.' '.$feedBackDetails->user->last_name.'
                Type: '.$feedBackDetails->user->email.'
                User Token: '.$feedBackDetails->user->token.'

                ';

        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to($emailInfo[4])
            ->subject($subject)
            ->send($message);


    }
    
    public function applicationNotification($contactOwnerId) {

        $usersTbl = TableRegistry::get('Users');
        $hrManager = $usersTbl->get($contactOwnerId);
        $subject = "New Application Received";
        $message = '
            
            Hi,
            
            Please be aware that a new application has been received in your region.
            
            Tutor2you';
        
        $email = new Email();
        $email->from(['admin@tutor2you.com.au' => 'Tutor2you'])
            ->to($hrManager->email)
            ->bcc('tammy@tutor2you.com.au')
            ->subject($subject)
            ->send($message);

    }


}
