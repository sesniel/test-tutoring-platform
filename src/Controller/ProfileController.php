<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use DateTime;
use Cake\Utility\Text;

class ProfileController extends AppController
{

	public function admin()
	{
		$type = $this->checkLoginStatus();
		if ($this->request->param('action') != $type) {
			$this->redirect(array("controller" => "../users", "action" => "login"));
		}
		$userDetails = $this->getUserDetails();
		$this->set('userDetails', $userDetails);
//                debug($userDetails->toArray()); exit;
	}

	public function tutor()
	{
		$type = $this->checkLoginStatus();
                if ($this->request->param('action') != $type) {
			$this->redirect(array("controller" => "../users", "action" => "login"));
		}
		$userDetails = $this->getUserDetails();
                $tutor = $userDetails->toArray();
//                debug($tutor->toArray); exit;
                $subjectTbl = TableRegistry::get('TutorSubjects');
                $subjectListTbl = TableRegistry::get('Subjects');
                $subjectsList = $subjectListTbl->find('all');
                $subjectDetails = $subjectTbl->find('all', ['conditions' => [
                    'TutorSubjects.tutor_id ' => $tutor[0]['id']
                ]])->contain(['Subjects']);
//              debug($subjectDetails->toArray());exit;
                $this->set('userDetails', $userDetails);
		$states = $this->getStates();
		$this->set('states', $states);
                $this->set(compact('subjectDetails', 'subjectsList'));
                
	}

	public function client()
	{
		$type = $this->checkLoginStatus();
		if ($this->request->param('action') != $type) {
			$this->redirect(array("controller" => "../users", "action" => "login"));
		}
		$userDetails = $this->getClientDetails();
//                debug($userDetails->toArray());exit;
		$this->set('userDetails', $userDetails);
		$states = $this->getStates();
		$this->set('states', $states);
		$yearLevelTable = TableRegistry::get('YearLevels');
		$yrlvls = $yearLevelTable->find('all');
		$this->set('yrlvls', $yrlvls);
		$yearLevelTable = TableRegistry::get('YearLevels');
		$yrlvls = $yearLevelTable->find('all');
		$this->set('yrlvls', $yrlvls);
		$subjsTbl = TableRegistry::get('Subjects');
		$subjs = $subjsTbl->find('all');
		$this->set('subjs', $subjs);
	}
        
        private function getTutorDetails() {
            
            $tutorsTbl = TableRegistry::get('Tutors');
            $tutorDetails = $tutorsTbl->findByToken($this->request->session()->read("token"));
            return $tutorDetails;
        }

	private function getUserDetails()
	{
		$UserTable = TableRegistry::get('Clients');
		$userDetails = $UserTable->findByToken($this->request->session()->read("token"))
		->contain(['States']);
		return $userDetails;
	}
	private function getClientDetails()
	{
		$UserTable = TableRegistry::get('Clients');
		$userDetails = $UserTable->findByToken($this->request->session()->read("token"))
		->contain(['Students.YearLevels', 'Students.StudentSubjects.Subjects', 'States']);
		return $userDetails;
	}

	private function getStates()
	{
		$statesTable = TableRegistry::get('States');
		$states = $statesTable->find('all');
		return $states;
	}

	public function clientUpdate()
	{
            if ($this->request->is('post') || $this->request->is('put')) {
                $email = $this->emailCheck($this->request->data('email'));
                if ($email) {
                        return $this->redirect($this->referer() . "#email-exist");
                }else{
                    $usersTbl = TableRegistry::get('Users');
                    $userID = $this->getUserID($this->request->data('token'));
                    $user = $usersTbl->get($userID);
                    $user->first_name = $this->request->data('first_name');
                    $user->last_name = $this->request->data('last_name');
                    $user->email = $this->request->data('email');
                    $user->mobile = $this->request->data('mobile');
                    $user->phone = $this->request->data('phone');
                    $user->unit_number = $this->request->data('unit_num');
                    $user->street_number = $this->request->data('street_num');
                    $user->street_name = $this->request->data('street_name');
                    $user->suburb = $this->request->data('suburb');
                    $user->state_id = $this->request->data('state');
                    $user->city = $this->request->data('city');
                    $user->email_notif = $this->request->data('email_notif');
                    if ($this->request->data('current_password')){
                            $this->passwordCheck($this->request->data('current_password'), $user->password);
                            $user->password = crypt($this->request->data('confirm_password'), 99);
                    }
                    $usersTbl->save($user);
                    $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                        'Users.email' => $this->request->data('email'),
                    ]])->contain(['Students.YearLevels', 'States'])->first();
                    $contactDetails = $this->getContactEmail($userDetailsUp->email);
                    $list = 14;//($user->type == "Tutor") ? 11 : 10;
                    $list1 = 11;
                    $grades = $this->calculateSubject($userDetailsUp);
                    $type = "edit";
                    $post = array(
                        'id'                       => $contactDetails['id'], 
                        'email'                    => $userDetailsUp->email,
                        'first_name'               => $userDetailsUp->first_name,
                        'last_name'                => $userDetailsUp->last_name,
                        'orgname'                  => 'Tutor2You',
                        'field[14,0]'              => $userDetailsUp->postcode,
                        'field[9,0]'               => $grades,
                        'field[18,0]'              => $userDetailsUp->type,
                        'field[19,0]'              => $userDetailsUp->status,
                        'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                        'tags'                     => 'api,'.$userDetailsUp->type,
                        'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'p['.$list1.']'            => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                    );
                    $this->crudContactList($post, $type);
//                                debug($this->getContactEmail($userDetailsUp->email));exit;
                    return $this->redirect($this->referer() . "#updated");
                }
            }
	}

	public function tutorUpdate()
	{
            debug($this->request->data());exit;
            if ($this->request->is('post') || $this->request->is('put')) {
//                    debug($this->request->data);exit;
			$email = $this->emailCheck($this->request->data('email'));
			if ($email) {
				return $this->redirect($this->referer() . "#email-exist");
			}else{
				$usersTbl = TableRegistry::get('Users');
                                $subjectsTbl = TableRegistry::get('TutorSubjects');
				$userID = $this->getUserID($this->request->data('token'));
                                $user = $usersTbl->get($userID);
                                
                                if($user->availability != $this->request->data('availability') || $user->hours != $this->request->data('hours')):
                                    $user->available_notification = date("Y-m-d", strtotime("30 days"));                                 
                                endif;
                                
				$user->first_name = $this->request->data('first_name');
				$user->last_name = $this->request->data('last_name');
				$user->email = $this->request->data('email');
				$user->mobile = $this->request->data('mobile');
				$user->phone = $this->request->data('phone');
				$user->street_num = $this->request->data('street_number');
				$user->street_name = $this->request->data('street_name');
				$user->suburb = $this->request->data('suburb');
				$user->state_id = $this->request->data('state');
                                $user->postcode = $this->request->data('postcode');
				$user->city = $this->request->data('city');
                                $user->availability = $this->request->data('availability');
                                $user->hours = $this->request->data('hours');
                                
                                $date = $this->request->data('blue_card_expiry');
                                $expiryDate = date('Y-m-d', strtotime($date));
				$user->blue_card_number = $this->request->data('blue_card_number');
				$user->blue_card_expiry = $expiryDate;
				$user->biography = $this->request->data('biography');
                                
                                $newSubjects = $this->request->data('subjects');
//                                debug($newSubjects);exit;
                                $subjects = $subjectsTbl->find('all', ['conditions' => [
                                    'TutorSubjects.tutor_id' => $userID 
                                ]]);
                                
                                foreach($subjects as $subject):
                                    $deleteSubj = $subjectsTbl->get($subject->id);
                                    $subjectsTbl->delete($deleteSubj);
                                    //debug($deleteSubj->toArray());
                                endforeach; 
                                
                                
                                foreach($newSubjects as $subject):
                                    
                                    $newSubj = $subjectsTbl->newEntity();
                                    $newSubj->tutor_id = $userID;
                                    $newSubj->subject_id = $subject;
//                                    debug($newSubj->toArray());
                                    $subjectsTbl->save($newSubj);
                                    
                                endforeach;
                                
				if ($this->request->data('current_password')){
					$this->passwordCheck($this->request->data('current_password'), $user->password);
					$user->password = crypt($this->request->data('confirm_password'), 99);
				}
				$usersTbl->save($user);
//                                debug($user->toArray()); exit;
                    $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                        'Users.email' => $this->request->data['email'],
                    ]])->contain(['Students.YearLevels', 'States'])->first();
                    $contactDetails = $this->getContactEmail($userDetailsUp->email);
                    $list = 14;//($user->type == "Tutor") ? 11 : 10;
                    $list1 = 11;
                    $grades = $this->calculateSubject($userDetailsUp);
                    $type = "edit";
                    $post = array(
                        'id'                       => $contactDetails['id'], 
                        'email'                    => $userDetailsUp->email,
                        'first_name'               => $userDetailsUp->first_name,
                        'last_name'                => $userDetailsUp->last_name,
                        'orgname'                  => 'Tutor2You',
                        'field[14,0]'              => $userDetailsUp->postcode,
                        'field[9,0]'               => $grades,
                        'field[18,0]'              => $userDetailsUp->type,
                        'field[19,0]'              => $userDetailsUp->status,
                        'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                        'tags'                     => 'api,'.$userDetailsUp->type,
                        'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'p['.$list1.']'             => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                    );
                    $this->crudContactList($post, $type);
//                                debug($this->getContactEmail($userDetailsUp->email));exit;

                    return $this->redirect($this->referer() . "#updated");
                }
            }
	}

	public function adminUpdate()
	{
            if ($this->request->is('post') || $this->request->is('put')) {
                $usersTbl = TableRegistry::get('Users');
                $userID = $this->getUserID($this->request->data('token'));
                $user = $usersTbl->get($userID);
                $email = $this->emailCheck($this->request->data('email'));
                if ($email){
                        return $this->redirect($this->referer() . "#email-exist");
                }else{
                        $user->first_name = $this->request->data('first_name');
                        $user->last_name = $this->request->data('last_name');
                        $user->email = $this->request->data('email');
                        if ($this->request->data('current_password')){
                                $this->passwordCheck($this->request->data('current_password'), $user->password);
                                $user->password = crypt($this->request->data('confirm_password'), 99);
                        }
                        $usersTbl->save($user);
                        $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                            'Users.email' => $this->request->data('email'),
                        ]])->contain(['Students.YearLevels', 'States'])->first();
                        $contactDetails = $this->getContactEmail($userDetailsUp->email);
                        $list = 14;//($user->type == "Tutor") ? 11 : 10;
                        $list1 = 11;
                        $type = "edit";
                        $post = array(
                            'id'                       => $contactDetails['id'], 
                            'email'                    => $userDetailsUp->email,
                            'first_name'               => $userDetailsUp->first_name,
                            'last_name'                => $userDetailsUp->last_name,
                            'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                            'p['.$list1.']'            => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                            'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                        );
                        $this->crudContactList($post, $type);
                        return $this->redirect($this->referer() . "#updated");
                    }
            }
	}

	private function emailCheck($email)
	{
		$usersTbl = TableRegistry::get('Users');

		$findEmail = $usersTbl->find("all", [
			"conditions" => [
			"Users.email" => $email
			]
			]);
		if (count($findEmail->toArray()) >= 2) {
			return 1;
		} else {
			return 0;
		}

	}

	private function passwordCheck($userInput, $curPass)
	{
		$cryptUserInput = crypt($userInput, 99);
		if ($cryptUserInput !== $curPass) {
			return $this->redirect($this->referer() . "#wrong-password");
		}

	}

	public function studentUpdate()
	{
		if ($this->request->is('post') || $this->request->is('put')) {
			$studentsTbl = TableRegistry::get('Students');
			$stdID = $this->request->data('student_id');
			$stdName = $this->request->data('student_name');
			$stdYrlvl = $this->request->data('year_level');

			foreach ($stdID as $key => $std) {
				$student = $studentsTbl->get($std);
				$student->name = $stdName[$key];
				$student->year_level_id = $stdYrlvl[$key];
				$studentDetails = $studentsTbl->save($student);

				if ($studentDetails) {
					$this->studentSubjUpdate($std, $key);
				}

			}
			return $this->redirect($this->referer() . "#updated");
		}
	}

	private function studentSubjUpdate($stdID, $key)
	{
		$stdSubjTbl = TableRegistry::get('StudentSubjects');
		$newSubjs = $this->request->data('subjects' . ($key + 1));
		$subjects = $stdSubjTbl->find('all', [
			'conditions' => ['StudentSubjects.student_id' => $stdID]
			]);

		foreach ($subjects as $subject) {
			$delStdSubj = $stdSubjTbl->get($subject->id);
			$stdSubjTbl->delete($delStdSubj);
		}
		foreach ($newSubjs as $newSubsID) {
			$newSubject = $stdSubjTbl->newEntity();
			$newSubject->student_id = $stdID;
			$newSubject->subject_id = $newSubsID;
			$stdSubjTbl->save($newSubject);
		}
	}

	private function getUserID($token)
	{
		$usersTbl = TableRegistry::get('Users');
		$user = $usersTbl->find("all", [
			"conditions" => [
			"Users.token" => $token
			]
			])->first();

		return $user->id;
	}

}
