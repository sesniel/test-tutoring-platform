<?php

namespace App\Controller\Tutor;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;

class AppController extends Controller {

    public function checkLoginStatus(){
        
        $type = ($this->request->session()->check("type") ? $this->request->session()->read("type") : 0);
        if ($type == "Tutor" || $type == "Applicant") { //echo $type;exit;
            $UserTable = TableRegistry::get('Users');
            $userDetails = $UserTable->find('all',['conditions' => ['Users.token' => $this->request->session()->read("token")]])
                    ->select(['id', 'first_name','last_name','email','phone', 'mobile', 'availability', 'available_notification', 'hours','unit_number','street_number','street_name','suburb','postcode','city','state_id','study_skill','login_status','status','biography'])->first();
            
            if($this->request->session()->read("access") == 1):
                $this->viewBuilder()->layout('tutor');
            else:     
                $this->viewBuilder()->layout('tutor-noaccess');
            endif;
            
            return $userDetails;
        }else{
            $this->redirect("/");
        }
        
    }
    
    public function eventActiveTrigger($clientId, $event){
        
        $ClientsTable = TableRegistry::get('Clients');
        $tutorDetails = $ClientsTable->get($clientId);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://trackcmp.net/event");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
          "actid" => "223206443",
          "key" => "509e4058240273ba2f7ce71fb03cac93a1dfd382",
          "event" => $event,
          "eventdata" => $event,
          "visit" => json_encode(array(
            // If you have an email address, assign it here.
            "email" => $tutorDetails->email,
          )),
        ));

        $result = curl_exec($curl);
        if ($result !== false) {
          $result = json_decode($result);
          if ($result->success) {
//            echo 'Successfull! ';
          } else {
//            echo 'Error! ';
          }

//          echo $result->message;
        } else {
//          echo 'cURL failed to run: ', curl_error($curl);
        }
        
    }
    
    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions or issues regarding the platform or accessing your assessments, 
                please respond to this email or call us and one of our staff will be happy to assist.',
            'Kind regards,
                 Tutor2You | Primary and Secondary | One-on-One Tutoring
                 Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'rosette@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }

    private function getAssmentDetails($id)
    {
        $stdAssmntTypeTbl = TableRegistry::get('StudentAssessmentTypes');
        $assmntDetails = $stdAssmntTypeTbl->get($id, [
            'contain' => ['Students.Clients', 'Tutors', 'AssessmentTypes']
        ]);
        return $assmntDetails;
    }
    
    public function emailNotifTraining($moduleId) {
        
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleUsers = $moduleUsersTable->get($moduleId, ['contain' => [
            'Tutors.Owner'
        ]]);
        

        if (($moduleUsers->tutor->owner_id) === 417) {
            $adminName = 'Kate';
        } elseif (($moduleUsers->tutor->owner_id) === 419) {
            $adminName = 'Alex';
        } elseif (($moduleUsers->tutor->owner_id) === 735) {
            $adminName = 'Amy';
        } elseif (($moduleUsers->tutor->owner_id) === 415) {
            $adminName = 'Tammy';
    }  
        
        
//        Modules
        if ($moduleUsers->position === 2):
            $modules = '1-2';
        elseif ($moduleUsers->position === 4):
            $modules = '3-4';
        elseif ($moduleUsers->position === 6):
            $modules = '5-6';
        elseif ($moduleUsers->position === 10):
            $modules = '7 to 10';
        endif;
        
        $message = '
            
                Hi ' . $adminName . ',
                
                Please be aware that ' . $moduleUsers->tutor->first_name . " " . $moduleUsers->tutor->last_name . ' has completed modules '.   $modules  . 
                ' and is ready to proceed.
                    
                 Regards,
                 T2Y'
                
                ;
        
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
//        ->to('tammy@tutor2you.com.au')
        ->to($moduleUsers->tutor->owner->email)
//        ->replyTo('tammy@tutor2you.com.au')
        ->replyTo($moduleUsers->tutor->owner->email)
        ->send($message);
        
    }
    
    public function sessionReportEmail ($lessonId) {
        
        $emailInfo = $this->emailInfo();
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->get($lessonId, ['contain' => [
            'Programs.Clients.Students'
        ]]);
        
        foreach ($lessonDetails->program->client->students as $student) {
            $student =  $student->name;
        }
        
        $subject = 'Session Report for ' . $student . " - " . $lessonDetails->date;
        $message = '
            
        Hi ' . $lessonDetails->program->client->first_name . ',
            
        Please see below your tutor\'s notes for the lesson dated ' . $lessonDetails->date . ':
            
        ' . $lessonDetails->note . '
            
        Please note that all of your booking details and historical lesson notes can be viewed on your client portal at ' . $emailInfo[0] . '
        
        ' . $emailInfo[3] . '
            
            ';
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
//        ->to('tammy@tutor2you.com.au')
        ->to($lessonDetails->program->client->email)
//        ->replyTo('tammy@tutor2you.com.au')
        ->replyTo($emailInfo[5])
        ->subject($subject)
        ->send($message);
    }
    
    public function emailTutorSessionReport($lessonId = null){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->get($lessonId, ['contain' => ['Programs', 'Programs.Students', 'Programs.Tutors', 'Programs.Clients']]);
        
//        debug($lessonDetails->toArray()); exit;
        $subject = 'Booking Variation Request';
        
        $message = '

Hi Alex,
                
Session reported with booking variation request by ' . $lessonDetails->program->tutor->first_name . " " . $lessonDetails->program->tutor->last_name . '.             
    
Details are as follows:

Date: ' . date('d/m/Y', strtotime($lessonDetails->date)) . ' 
    
Client: ' . $lessonDetails->program->client->first_name . $lessonDetails->program->client->last_name . '
    
Student: ' . $lessonDetails->program->student->name . '

Length: ' . $lessonDetails->length . ' 

Report: ' . $lessonDetails->note . ' 

Reason: ' . $lessonDetails->reason . ' 

                ';
       
        
       $email = new Email();
       $email->from($lessonDetails->program->tutor->email)
           ->to('alex@tutor2you.com.au')
//           ->to('joshua@tutor2you.com.au')
          ->bcc('joshua@tutor2you.com.au')
           ->subject($subject)
           ->send($message);
        
    }

    public function emailNewAssmntCreated($id, $assessmentDetails)
    {   
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $assmntDetails = $this->getAssmentDetails($id);
        $assessmentInfo = $this->generateAssessmentDetails($assessmentDetails);
        $subject = 'Tutor2you Assessment for ' . $assmntDetails->student->name;
        $message = '

                Hi ' . $assmntDetails->student->client->first_name . ',
                    
                ' . $assmntDetails->tutor->first_name ." ". $assmntDetails->tutor->last_name . ' has requested to complete the following ' . $assessmentInfo . ' 
                Assessment via Tutor2you Online Platform.
                
                The results of this assessment will assist your tutor to personally tailor tutoring to suit your childs needs.
               
                Please login to the Online Platform here to complete the assessments: ' . $emailInfo[0] . '
               
                Note: Your username and password have previously been sent via email when you were added to the system. 
                If you are unable to locate this email, please visit the URL above and use the forgot password function, 
                using the email you provided to admin at the time of booking.
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';
        
       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($assmntDetails->student->client->email)
           ->bcc('joshua@tutor2you.com.au')
           ->subject($subject)
           ->send($message);
    }
    
    private function generateAssessmentDetails($assessmentDetails){
        $count = count($assessmentDetails) - 1;
        $value = "";
        $assessmentTypesTbl = TableRegistry::get('AssessmentTypes');
        foreach($assessmentDetails as $key => $assessment):
            
            if($count == $key && $count > 0):
                $assessmentInfo = $assessmentTypesTbl->get($assessment);
                $value .= "and " . $assessmentInfo->type;
            elseif($count == $key && $count == 0):
                $assessmentInfo = $assessmentTypesTbl->get($assessment);
                $value .= $assessmentInfo->type;
            else:
                $assessmentInfo = $assessmentTypesTbl->get($assessment);
                $value .= $assessmentInfo->type . ", ";
            endif;
            
        endforeach;
        return $value;
        
    }
    public function followUpEmail ($leadID) {
        
        $this->checkLoginStatus(); 
        $emailInfo = $this->emailInfo();
        $leadsTbl = TableRegistry::get('Leads');
        $leadDetails = $leadsTbl->get($leadID, ['contain' => ['Clients', 'Tutors']]);
        $message = '

                Hi ' . $leadDetails->tutor->first_name .',

                It looks like you were allocated a new client ' . $leadDetails->client->first_name . " " . $leadDetails->client->last_name . ' and a consultation date has not been booked in. Please book your consultation with the client and
                allocate any relevant assessments as soon as possible. 

                Alternatively, please phone the office on 1300 4200 79 if there are any difficulties.

                 ' . $emailInfo[3] . '

                  ';
             
        
//        $email = new Email();
//        $email->from([$emailInfo[5] => 'Tutor2You'])
////           ->to($leadDetails->tutor->email)
//           ->to('tammy@tutor2you.com.au')
//           //->bcc('joshua@tutor2you.com.au')
//           ->send($message);
        
    }

    public function emailNewAssmntCreatedByJoshua($leadId, $assessmentDetails)
    {   
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        
        $leadsTbl = TableRegistry::get('Leads');
        $leadDetails = $leadsTbl->get($leadId, ['contain' => ['Clients', 'Tutors', 'Students']]);
        $assessmentInfo = $this->generateAssessmentDetails($assessmentDetails);

        $subject = 'Tutor2you Assessment for ' . $leadDetails->student->name;
        $message = '

                Hi ' . $leadDetails->client->first_name  . " " .$leadDetails->client->last_name . ',

                ' . $leadDetails->tutor->first_name ." ". $leadDetails->tutor->last_name . ' has requested to complete the following ' . $assessmentInfo . ' 
                Assessment via Tutor2you Online Platform.
                
                The results of this assessment will assist your tutor to personally tailor tutoring to suit your childs needs.
               
                Please login to the Online Platform here to complete the assessments: ' . $emailInfo[0] . '
               
                Note: Your username and password have previously been sent via email when you were added to the system. 
                If you are unable to locate this email, please visit the URL above and use the forgot password function, 
                using the email you provided to admin at the time of booking.
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';
        
       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($leadDetails->client->email)
           ->bcc('joshua@tutor2you.com.au')
           ->subject($subject)
           ->send($message);
    }

    
    
    
    public function emailSessionReport($reportDetails)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();

        $subject = 'Session Report';
        $message = '

                Hi,

                The following time sheet details have been sent successfully,

                Tutor Name: ' . $reportDetails['full_name'] . ',
                 Client Name: ' . $reportDetails['client'] . ',
                Session Length: ' . $reportDetails['session-length'] . ',
                Session Date:  ' . $reportDetails['session-date'] . ',
                Session Notes: ' . $reportDetails['session-note'] . '
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';

       $email = new Email();

       $email->from([$emailInfo[5] => 'Tutor2You'])
       ->to($emailInfo[5])
      ->bcc('joshua@tutor2you.com.au')
     ->cc($reportDetails['tutor-email'])
      ->subject($subject)
       ->send($message);

       if($reportDetails['cmail'] != ''){
         $email->from([$emailInfo[5] => 'Tutor2You'])
          ->to($reportDetails['cmail'])
           ->bcc('joshua@tutor2you.com.au')
          ->subject($subject)
          ->send($message);         
    }
   
}
    
}
