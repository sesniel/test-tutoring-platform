<?php
namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class MessagesController extends AppController
{

    public function index($count = 10){
        
        $tutorDetails = $this->checkLoginStatus();
        $MessagesTable = TableRegistry::get('MessageTags');
        $messageTagDetails = $MessagesTable->find('all', ['conditions' => [
            'MessageTags.user_id' => $tutorDetails->id
        ]])->contain(['Messages.Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'token']);
            }])->order(['MessageTags.created' => 'desc']);
//        debug($messageTagDetails->toArray());exit;

        $this->set(compact('messageTagDetails'));
        $this->set('_serialize', ['messageTagDetails']);
        
    }
        
    public function view($id = null){
        
        $this->checkLoginStatus();
        $message = $this->Messages->get($id, [
            'contain' => ['Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }, 'MessageTags.Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'token']);
            }]
        ]);
//            debug($message);exit;
        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }
        
}
