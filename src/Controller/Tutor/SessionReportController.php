<?php
namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class SessionReportController extends AppController
{

    public function index(){
        $this->redirect(['controller' => 'bookings', 'action' => 'index']);
        $details = $this->checkLoginStatus();

        $LeadsTbl = TableRegistry::get('Leads');
        $leads = $LeadsTbl->find('all', ['conditions' => [
                'Leads.tutor_id' => $details->id,
            ] ])
        ->distinct(['Leads.client_id'])
        ->contain(['Clients']);

        $this->set(compact('details','leads'));
    }

    public function sessionReport(){
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->emailSessionReport($this->request->data());
            return $this->redirect($this->referer() . "#report-sent");
        }        
    }    

}
