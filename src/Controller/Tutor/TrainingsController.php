<?php
namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

class TrainingsController extends AppController
{

    public function index(){ 
        
        $tutorInfo = $this->checkLoginStatus(); //debug($tutorInfo);exit;
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $statusTable = TableRegistry::get('Status');
        $statusDetails = $statusTable->find('all', ['conditions' => ['belong_to' => "Training"]]);
        $moduleDetails = $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.user_id' => $tutorInfo->id
        ]])
        ->contain(['ModuleUserQuestions','ModuleUserQuestions.Modules', 'Tutors'])
        ->first();  
//         debug($moduleDetails->toArray()); exit;
        $this->set(compact('tutorInfo', 'moduleDetails', 'statusDetails'));
        
    }
    
    
    public function moduleUpdate($moduleId = null){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        foreach($this->request->data['answer'] as $key => $answer):
            $moduleUserDetails = $moduleUserQuestionsTable->get($key);
            $moduleUserDetails->user_answer = $answer;
            $moduleUserQuestionsTable->save($moduleUserDetails);
        endforeach; 
        
        if($moduleId != null):
            $moduleDetails = $moduleUsersTable->get($moduleId, ['contain' => ['Tutors']]);
            if($moduleDetails->position == 2):
//                $ownerId = $moduleDetails->tutor->owner_id;
                $moduleDetails->status = "Completed Orientation";
                $this->emailNotifTraining($moduleId);
                $this->redirect($this->referer() . "#completed");  
            elseif($moduleDetails->position == 4):
                $moduleDetails->status = "Completed Contract Review & Expectations";
                $this->emailNotifTraining($moduleId);
                $this->redirect($this->referer() . "#completed"); 
            elseif($moduleDetails->position == 6):
                $moduleDetails->status = "Completed Platform Training";
                $this->emailNotifTraining($moduleId);
                $this->redirect($this->referer() . "#completed"); 
            elseif($moduleDetails->position == 10):
                $moduleDetails->status = "Completed Training";
                $this->emailNotifTraining($moduleId);
                $this->redirect($this->referer() . "#orientation-complete"); 
            else:
                $moduleDetails->position = $moduleDetails->position + 1;
            endif;
                $moduleUsersTable->save($moduleDetails);
        endif;
                $this->redirect($this->referer());  
//                 debug($moduleDetails);exit; 
        
    }
    
    
    public function savemodule($moduleId = null) {
       
        $ModuleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        foreach($this->request->data['answer'] as $key => $answer):
            $moduleAnswerDetails = $ModuleUserQuestionsTable->get($key);
            $moduleAnswerDetails->user_answer = $answer;
            $ModuleUserQuestionsTable->save($moduleAnswerDetails);
        endforeach;
        $this->updateStatusModuleUsers($moduleId);
        $this->redirect($this->referer());
        
    }
    
    private function updateStatusModuleUsers($moduleId){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleUserDetails = $moduleUsersTable->get($moduleId);
        if($moduleUserDetails->status == 4):
            $moduleUserDetails->status = "Pending";
        elseif($moduleUserDetails->status == 8):
            $moduleUserDetails->status = "Done";
        else:
            $moduleUserDetails->status = $moduleUserDetails->status + 1;
        endif;        
            $moduleUsersTable->save($moduleUserDetails);
        
    }

    public function general(){ 
        
        $this->checkLoginStatus();
        
    }

    public function editmodule($moduleId = null) {
        $ModuleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        foreach($this->request->data['answer'] as $key => $answer):
            $moduleAnswerDetails = $ModuleUserQuestionsTable->get($key);
            $moduleAnswerDetails->user_answer = $answer;
            $ModuleUserQuestionsTable->save($moduleAnswerDetails);
        endforeach;
        $this->redirect($this->referer().'#updated');        
    }
    
    public function studyskills(){ 
        
        $this->checkLoginStatus();
        
    }
    
   

}
