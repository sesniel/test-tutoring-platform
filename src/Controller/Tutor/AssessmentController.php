<?php

namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class AssessmentController extends AppController {

    public function index() {
        echo "joshua";
        exit;
    }

    public function test() {

        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentATypeDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.status' => 'In Progress',
                        'StudentAssessmentTypes.year_level_id >= ' => 10,
                        'StudentAssessmentTypes.assessment_type_id in ' => [4, 6]
            ]])->contain(['Tutors', 'Students']);
        debug($studentATypeDetails->toArray());
        exit;
    }

    public function student($id = null, $leadId = null) {

        $this->viewBuilder()->layout('blank');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentDetails = $studentTable->find('all', [
                    'conditions' => ['Students.id' => $id]
                ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $assessmentTypeDetails = $assessmentTypesTable->find('all')
                ->contain(['Subjects'])
                ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'leadId'));
    }

    public function cancelAssessment($id) {

        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentsAT = $studentAssessmentTypesTable->get($id);
        if ($studentAssessmentTypesTable->delete($studentsAT)) {
            $this->delUserQuestions($id);
            $this->redirect($this->referer());
        }
    }

    private function delUserQuestions($stdAssTypId) {
        $userQuestTbl = TableRegistry::get('UserQuestions');
        $userQuestions = $userQuestTbl->find('all', [
            'conditions' => ['UserQuestions.student_assessment_type_id' => $stdAssTypId]
        ]);
        foreach ($userQuestions as $key => $value) {
            $question = $userQuestTbl->get($value->id);
            $userQuestTbl->delete($question);
        }
    }

    public function editStudent($id = null) {

        $this->viewBuilder()->layout('blank');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentDetails = $studentTable->find('all', [
                    'conditions' => ['Students.id' => $id]
                ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $studentAssessmentDetails = $studentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.student_id' => $id,
                        'StudentAssessmentTypes.status in ' => array('Pending', 'In Progress')
            ]])->contain(['AssessmentTypes', 'Students']);

        $assessmentTypeDetails = $assessmentTypesTable->find('all')
                ->contain(['Subjects'])
                ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'studentAssessmentDetails'));
    }

    public function allocate() {
        
        $studentTable = TableRegistry::get('Students');
        $studentDetails = $studentTable->find('all', [
                    'conditions' => ['Students.id' => $this->request->data['student_id']]
                ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();

        $questions = array();
        foreach ($this->request->data['assessments'] as $assessmentId):

            $categoryId = $this->getCategoryInfoId($assessmentId, $studentDetails->year_level_id);

            if ($categoryId != 0):
                $majorDetails = $this->getQuestionDetailsByCategory($categoryId);
                $questions = $this->arrangeCategoryQuestion($majorDetails);
            else:
                $studentYearId = $this->identifyDiagnosticExtension($studentDetails->year_level_id, $assessmentId);
                $majorDetails = $this->getQuestionDetailsBySubjectYear($assessmentId, $studentYearId);
                $questions = $this->arrangeQuestion($majorDetails, $studentDetails->year_level_id);
            endif;

//            debug($loginInfo);
            $studentAssessmentLastId = $this->saveStudentAssessmentTypes($assessmentId, $studentDetails, $this->request->data);

            $this->saveUserQuestions($questions, $studentAssessmentLastId);

        endforeach;
        $this->emailNewAssmntCreated($studentAssessmentLastId,$this->request->data['assessments']);

//        if (isset($this->request->data['lead_id'])):
//            $consultation = $this->request->data('conDate') . " " . $this->request->data('conTime');
//            $this->updateLeadStatus($this->request->data['lead_id'], $consultation);
//        endif;

        return $this->redirect(array('controller' => 'clients', 'prefix' => "tutor", '_full' => true));
    }

    public function bookConsultation() {
        $tutorInfo = $this->checkLoginStatus();
        $students = $this->request->data('student_id');
        $leads = $this->request->data('lead_id');
        $studentTable = TableRegistry::get('Students');
//       debug($leads); exit;
      
    
             
         foreach ($students as $key => $student) { 
            $studentDetails = $studentTable->find('all', [
                        'conditions' => ['Students.id' => $student]
                    ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
              
            $questions = array();
        
            $assessmentIds = $this->request->data('assessments' . $key); //debug($assessmentIds); exit; 
            
            if (!isset($assessmentIds)) {
              // echo "not set"; .  
                $this->updateConsultation();
                $this->generateConsultationQuestions();
               return $this->redirect(array('controller' => 'dashboard', 'prefix' => "tutor", '_full' => true));
               
            } else {
                
                foreach ($assessmentIds as $assessmentId):
                
                    $categoryId = $this->getCategoryInfoId($assessmentId, $studentDetails->year_level_id); 
                    if ($categoryId != 0):
                        $majorDetails = $this->getQuestionDetailsByCategory($categoryId);
                        $questions = $this->arrangeCategoryQuestion($majorDetails);
                    else:
                        $studentYearId = $this->identifyDiagnosticExtension($studentDetails->year_level_id, $assessmentId);
                        $majorDetails = $this->getQuestionDetailsBySubjectYear($assessmentId, $studentYearId);
                        $questions = $this->arrangeQuestion($majorDetails, $studentDetails->year_level_id);  
                    endif;
                    $studentAssessmentLastId = $this->saveStudentAssessmentTypesBC($assessmentId, $studentDetails, $student, $tutorInfo->id);
                    $this->saveUserQuestions($questions, $studentAssessmentLastId);
                    
                endforeach;
            }
            
            $this->emailNewAssmntCreatedByJoshua($leads[$key], $this->request->data('assessments' . $key));
        }
          
        $this->updateConsultation();
        $this->generateConsultationQuestions();
        return $this->redirect(array('controller' => 'dashboard', 'prefix' => "tutor", '_full' => true));
    }

    public function allocateInfo() {

        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentTypeDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.year_level_id >= ' => 10,
                        'StudentAssessmentTypes.status != ' => "Done",
                        'StudentAssessmentTypes.assessment_type_id in' => [4, 5, 6]
            ]])->contain('UserQuestions');
//        debug($studentAssessmentTypeDetails->toArray());exit;

        $x = 0;
        foreach ($studentAssessmentTypeDetails as $studentAssessment):
            if (empty($studentAssessment->user_questions)):
                debug($studentAssessment);
                $x++;
            endif;
        endforeach;
        echo $x;
        exit;



        $studentTable = TableRegistry::get('Students');
        $studentDetails = $studentTable->find('all', [
                    'conditions' => ['Students.id' => $this->request->data['student_id']]
                ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();

        $questions = array();
        foreach ($this->request->data['assessments'] as $assessmentId):

            $categoryId = $this->getCategoryInfoId($assessmentId, $studentDetails->year_level_id);

            if ($categoryId != 0):
                $majorDetails = $this->getQuestionDetailsByCategory($categoryId);
                $questions = $this->arrangeCategoryQuestion($majorDetails);
            else:
                $studentYearId = $this->identifyDiagnosticExtension($studentDetails->year_level_id, $assessmentId);
                $majorDetails = $this->getQuestionDetailsBySubjectYear($assessmentId, $studentYearId);
                $questions = $this->arrangeQuestion($majorDetails, $studentDetails->year_level_id);
            endif;

//            debug($loginInfo);
            $studentAssessmentLastId = $this->saveStudentAssessmentTypes($assessmentId, $studentDetails, $this->request->data);

            $this->saveUserQuestions($questions, $studentAssessmentLastId);

        endforeach;




        exit;
        $studentAssessmentTypeDetails11 = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.year_level_id >= ' => 10,
                        'StudentAssessmentTypes.status != ' => "Done",
                        'StudentAssessmentTypes.assessment_type_id in' => [4, 5, 6]
            ]])->contain('UserQuestions');
        $x = 0;
        foreach ($studentAssessmentTypeDetails11 as $studentAssessment):
            if (empty($studentAssessment->user_questions)):
                debug($studentAssessment);
                $x++;
            endif;
        endforeach;
        echo $x;
        exit;

//        if(isset($this->request->data['lead_id'])):
//            $consultation = $this->request->data('conDate') . " " . $this->request->data('conTime');
//            $this->updateLeadStatus($this->request->data['lead_id'], $consultation);
//        endif;
//        
//        return $this->redirect(array('controller' => 'clients', 'prefix' => "tutor", '_full' => true));
    }

    private function arrangeCategoryQuestion($majorDetails = null) {

        foreach ($majorDetails as $major):
            if ($major['minor_outcomes']):
                foreach ($major['minor_outcomes'] as $minor):

                    foreach ($minor['Questions'] as $ques):
                        $saveQ[] = $ques;
                    endforeach;

                endforeach;
            endif;
        endforeach;
        return $saveQ;
    }

    private function updateConsultation() {
        $dateTime = $this->request->data('conDate').' '.$this->request->data('conTime');
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultation = $consultationsTbl->get($this->request->data('consultation_id'));
        $consultation->consultation_session_date = date('Y-m-d H:i:s', strtotime($dateTime));
        $consultation->status = 'Pending Consultation';
        $consultationsTbl->save($consultation);
    }
    
    private function generateConsultationQuestions(){
        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadQuestionsTbl = TableRegistry::get('LeadQuestions');
        $leadQuestions = $leadQuestionsTbl->find('all');
        foreach ($leadQuestions as $key => $question) {
            $leadAnswers = $leadAnswersTbl->newEntity();
            $leadAnswers->consultation_id = $this->request->data('consultation_id');
            $leadAnswers->lead_question_id = $question->id;
            $leadAnswers->question = $question->question;
            $leadAnswersTbl->save($leadAnswers);
        }
    }

    private function generateLeadQuestions($lead_id) {

        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadQuestionsTbl = TableRegistry::get('LeadQuestions');
        $leadQuestions = $leadQuestionsTbl->find('all');
        foreach ($leadQuestions as $key => $question) {
            $leadAnswers = $leadAnswersTbl->newEntity();
            $leadAnswers->lead_id = $lead_id;
            $leadAnswers->lead_question_id = $question->id;
            $leadAnswers->question = $question->question;
            $leadAnswersTbl->save($leadAnswers);
        }
    }

    private function saveStudentAssessmentTypesBC($assessmentId = null, $studentDetails = null, $studentID = null, $tutorID = null ) {
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentType = $StudentAssessmentTypesTable->newEntity();
        $studentAssessmentType->student_id = $studentID;
        $studentAssessmentType->assessment_type_id = $assessmentId;
        $studentAssessmentType->tutor_id = $tutorID;
        $studentAssessmentType->year_level_id = $studentDetails->year_level_id;
        $studentAssessmentType->status = 'Pending';
        $studentAssessmentTypeId = $StudentAssessmentTypesTable->save($studentAssessmentType);
        $studentAssessmentLastId = $studentAssessmentTypeId->id;
        //$this->emailNewAssmntCreated($studentAssessmentLastId);
        return $studentAssessmentLastId;
    }
    
    private function saveStudentAssessmentTypes($assessmentId = null, $studentDetails = null, $studentPostDetails = null) {
        $clientInfo = $this->checkLoginStatus();
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentType = $StudentAssessmentTypesTable->newEntity();
        $studentAssessmentType->student_id = $studentPostDetails['student_id'];
        $studentAssessmentType->assessment_type_id = $assessmentId;
        $studentAssessmentType->tutor_id = $clientInfo->id;
        $studentAssessmentType->year_level_id = $studentDetails->year_level_id;
        $studentAssessmentType->status = 'Pending';
        $studentAssessmentTypeId = $StudentAssessmentTypesTable->save($studentAssessmentType);
        $studentAssessmentLastId = $studentAssessmentTypeId->id;
//        $this->emailNewAssmntCreated($studentAssessmentLastId);
        return $studentAssessmentLastId;
    }

    private function generateStimulusQuestions($questions = null) {

        $stimulusInfo = array('StimulusA.png', 'StimulusB.png', 'https://www.youtube.com/embed/xMnx_3BC7EM');

        foreach ($stimulusInfo as $stimuli):
            $countPos = count($questions);
            $questions[$countPos] = (object) 'ciao';
            $questions[$countPos]->minor_outcome_id = 625;
            
            if($stimuli == "StimulusB.png"):
                $questions[$countPos]->description = "Do you agree or disagree with the question on the stimulus? Explain your answer. What could be some possible arguments for both sides of this issue? (List at least 3 for each side)";	
            else:                
                $questions[$countPos]->description = "What is your reaction in this?";
            endif;
            
            $questions[$countPos]->mark = "";
            $questions[$countPos]->answer = "";
            $questions[$countPos]->image = $stimuli;
            $questions[$countPos]->choices = "";
            $questions[$countPos]->type = "Stimulus";

        endforeach;

        return $questions;
    }

    private function saveUserQuestions($questions = null, $studentAssessmentId = null) {

        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentDetails = $StudentAssessmentTypesTable->get($studentAssessmentId);

        if ($studentAssessmentDetails->assessment_type_id == 4 || $studentAssessmentDetails->assessment_type_id == 6):
            if ($studentAssessmentDetails->year_level_id >= 10):
                $questions = $this->generateStimulusQuestions($questions);
            endif;
        endif;


//        debug($studentAssessmentDetails);
//        debug($questions);
//        exit;

        $x = 0;
        foreach ($questions as $ques): $x++;
            $userQuestion = $UserQuestionsTable->newEntity();
            $userQuestion->student_assessment_type_id = $studentAssessmentId;
            $userQuestion->position = $x;
            $userQuestion->minor_outcome_id = $ques->minor_outcome_id;
            $userQuestion->question = $ques->description;
            $userQuestion->mark = $ques->mark;
            $userQuestion->correct_answer = $ques->answer;
            $userQuestion->image = $ques->image;
            $userQuestion->choices = $ques->choices;
            if ($ques->type == "Stimulus")
                $userQuestion->type = $ques->type;
            $UserQuestionsTable->save($userQuestion);
        endforeach;
    }

    private function arrangeQuestion($majorDetails = null, $year_level_id = null) {

        foreach ($majorDetails as $major):

            if ($major['minor_outcomes']):
                foreach ($major['minor_outcomes'] as $minor):
                    $qVal = count($minor['Questions']);
                    if ($year_level_id < 7):

                        if ($qVal > 0):

                            $questionsIds = array_rand($minor['Questions'], 2);
                            $setId = rand(1, $qVal);
                            $setId -= 1;
                            if ($minor['Questions']):
                                $saveQ[] = $minor['Questions'][$setId];
                            endif;

                        endif;
                    else:

                        $qVal = count($minor['Questions']);
                        if ($qVal >= 1): $questionsIds = array_rand($minor['Questions'], 1);
                        endif;

                        if (isset($questionsIds)):
                            if ($minor['Questions']): $saveQ[] = $minor['Questions'][$questionsIds];
                            endif;
                        endif;

                    endif;
                endforeach;
            endif;
        endforeach;
        shuffle($saveQ);
        return $saveQ;
    }

    private function getQuestionDetailsByCategory($categoryId = null) {

        $majorOutcomesTable = TableRegistry::get('MajorOutcomes');
        $questionsTable = TableRegistry::get('Questions');
        $majorOutcomeDetails = $majorOutcomesTable->find('all', ['conditions' => [
                        'MajorOutcomes.category_id' => $categoryId
            ]])->contain(['MinorOutcomes'])->toArray();
        foreach ($majorOutcomeDetails as $key => $major):
            foreach ($major['minor_outcomes'] as $key2 => $minor):
                $questionDetails = $questionsTable->find('all', ['conditions' => [
                                'Questions.minor_outcome_id' => $minor->id
                    ]])->toArray();
                $majorOutcomeDetails[$key]['minor_outcomes'][$key2]['Questions'] = $questionDetails;
            endforeach;
        endforeach;
        return $majorOutcomeDetails;
    }

    private function getQuestionDetailsBySubjectYear($assessmentTypeId = null, $yearLevelId = null) {

        if ($yearLevelId >= 10):

            if ($assessmentTypeId == 4 || $assessmentTypeId == 6 || $assessmentTypeId == 5):
                $yearLevelId = 10;
            endif;

        endif;

        $majorOutcomesTable = TableRegistry::get('MajorOutcomes');
        $questionsTable = TableRegistry::get('Questions');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $assessmentDetails = $assessmentTypesTable->find('all', ['conditions' => ['AssessmentTypes.id' => $assessmentTypeId]])->first();
        $majorOutcomeDetails = $majorOutcomesTable->find('all', ['conditions' => [
                        'MajorOutcomes.subject_id' => $assessmentDetails->subject_id,
                        'MajorOutcomes.year_level_id' => $yearLevelId
            ]])->contain(['MinorOutcomes'])->toArray();
        foreach ($majorOutcomeDetails as $key => $major):
            foreach ($major['minor_outcomes'] as $key2 => $minor):
                $questionDetails = $questionsTable->find('all', ['conditions' => [
                                'Questions.minor_outcome_id' => $minor->id
                    ]])->toArray();
                $majorOutcomeDetails[$key]['minor_outcomes'][$key2]['Questions'] = $questionDetails;
            endforeach;
        endforeach;
        return $majorOutcomeDetails;
    }

    private function identifyDiagnosticExtension($year_level_id, $assessmentId) {


        if ($year_level_id == 1 || $assessmentId >= 5): //Extension
            $studentYearId = $year_level_id;
        elseif ($assessmentId == 3 || $assessmentId == 4): //Diagnostic
            $studentYearId = $year_level_id - 1;
        else: //Others
            $studentYearId = $year_level_id;
        endif;
        return $studentYearId;
    }

    private function getStudentSubject($studentSubjects) {

        $subjectsTable = TableRegistry::get('Subjects');
        $subjectArray = array();
        foreach ($studentSubjects as $subject):

            $studentDetails = $subjectsTable->find('all', [
                        'conditions' => ['Subjects.id' => $subject->subject_id]
                    ])->first();
            $subjectArray[] = $studentDetails->name;

        endforeach;
        sort($subjectArray);
        return $subjectArray;
    }

    private function getCategoryInfoId($assessmentId, $year_level_id) {


        if ($assessmentId == 1):

            if ($year_level_id <= 7):
                $categoryId = 9;
            else:
                $categoryId = 10;
            endif;

        elseif ($assessmentId == 2):

            if ($year_level_id <= 7):
                $categoryId = 7;
            else:
                $categoryId = 8;
            endif;

        else:

            $categoryId = 0;

        endif;

        return $categoryId;
    }

//    public function populateanswer(){
//        
//        $UserQuestionsTable = TableRegistry::get('UserQuestions');
//        
//        $userQuestionDetails= $UserQuestionsTable->find('all');
//        foreach($userQuestionDetails as $questionDetails):
//            
//           $studentsAT = $UserQuestionsTable->get($questionDetails->id);
//            $studentsAT->user_answer = $questionDetails->correct_answer;
//            $UserQuestionsTable->save($studentsAT);
//            
//        endforeach;        
//        
//        
//    }
}
