<?php

namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

class DashboardController extends AppController {
    
    private function announcement($tutorInfo){
        
        $MessageTagsTable = TableRegistry::get('MessageTags');
        $messageTagsDetails = $MessageTagsTable->find('all', ['conditions' => [
            'MessageTags.user_id' => $tutorInfo->id,
            'MessageTags.type' => 'Active'
        ]])->contain(['Messages.Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }]);
        foreach($messageTagsDetails as $messageTag):
            $messageTagUpdate = $MessageTagsTable->get($messageTag->id);
            $messageTagUpdate->type = "Read";
            $MessageTagsTable->save($messageTagUpdate);
        endforeach;
//        debug($messageTagsDetails->toArray()); exit;
        return $messageTagsDetails;
    }
    
    public function updateAvailability($tutorId){
        
        $UsersTable = TableRegistry::get('Users');
        $availability = $this->request->data("availability");
        $hours = $this->request->data("hours");        
        $availabilityNotification = date("Y-m-d", strtotime("+30 days"));        
        $updateUser = $UsersTable->get($tutorId);
        $updateUser->availability = $availability;
        $updateUser->available_notification = $availabilityNotification;
        $updateUser->hours = $hours;
        $UsersTable->save($updateUser);
        $this->set(compact('availabilityNotification'));
        $this->viewBuilder()->layout('ajax');
        return $availability;
    }
   
    public function index ($status = "Pending Consultation", $lessonID = null) {
        
        $tutorInfo = $this->checkLoginStatus();
        $announcement = $this->announcement($tutorInfo);
        $MessagesTable = TableRegistry::get('MessageTags');
        $LeadsTable = TableRegistry::get('Leads');
        $ProgramsTable  = TableRegistry::get('Programs');
        $leads = $LeadsTable->find('all', ['conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.tutor_id' => $tutorInfo->id,
                        
                
            ]])->contain(['Clients', 'Tutors', 'Consultations'])
                ->group(['Leads.consultation_id']);
        
//     debug($leads->toArray()); exit;
        $lAlignDetails = $LeadsTable->find('all', [
                    'conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.tutor_id' => $tutorInfo->id,
                    ]
                ])->contain(['Students.YearLevels', 'Tutors', 'Clients', 'LeadSubjects']);

        if (!empty($leads)) {
                foreach($leads as $lead):
                 $leadID = $lead->id;
                 $dateCreated = date("Y-m-d H:i:s", strtotime($lead->created));
                 $currentDate = date("Y-m-d H:i:s");
                
                 $seconds = strtotime($currentDate) - strtotime($dateCreated);
                 $hours = $seconds / 60 /  60;
                
             endforeach;
        }
        
        $lAlign = $this->getStudentAssessmentTypes($lAlignDetails);
        $assmntResults = $this->getAssessmentResults($tutorInfo->id);
        
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;
        
        $statusSelect = ($status == "All") ? "%%" : $status;

        $conDetails = $this->getConsultationDetails($tutorInfo->id);
        $consultations = $this->getConsultations($tutorInfo->id);
//        debug($consultations->toArray());exit;
        if(!empty($consultations)):
            foreach ($consultations as $consultation):
            //$consultation_date = $consultation->consultation_session_date;
            $conDate = date("Y/m/d H:i:s", strtotime($consultation->consultation_session_date));
            $currentDate = date("Y/m/d H:i:s");
            
            $seconds1 = strtotime($currentDate) - strtotime($conDate);
            $hours1 = $seconds1 / 60 /  60;
           // echo $conDate;
            
            endforeach;
        endif;
        
        //$choices = ['In Progress', 'Pending'];
        $programDetails = $ProgramsTable->find('all', ['conditions' => [
            'Programs.tutor_id' => $tutorInfo->id,
            'Programs.status in'   => "In Progress"
        ]])->contain(['Clients', 'Tutors', 'Clients', 'ProgramStudents.Students', 'Lessons' => ['conditions' => [
            'Lessons.date <= ' => date("Y/m/d H:i:s", strtotime('-24 hours')),
            'Lessons.status' => 'Scheduled'
        ]]
                ]);
  
        $calendarConsultation = $this->getConsultInfo($tutorInfo->id);        
        $calendarInfo = $this->getCalendarDetails($tutorInfo);
       
        $messageTagDetails = $MessagesTable->find('all', ['conditions' => [
            'MessageTags.user_id' => $tutorInfo->id
        ]])->contain(['Messages.Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'token']);
            }])->order(['MessageTags.created' => 'desc'])->limit(5);
//        debug($messageTagDetails->toArray());exit;

        $this->set(compact('messageTagDetails','tutorInfo', 'announcement', 'calendarConsultation', 'calendarInfo', 'leads', 'lAlign', 'assmntCount', 'assmntResults', 'consultations', 'conDetails','status', 'hours', 'conDate', 'programDetails'));
        
        $this->viewBuilder()->layout('tutor-calendar');
        
    }
    
    private function getConsultInfo($tutorId){ 
        $leadsTable = TableRegistry::get('Leads');
        $leadsQuery = $leadsTable->find('all', ['conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.tutor_id' => $tutorId,
            ]])->contain(['Clients','Consultations']);
        return $leadsQuery;
    }
    
    public function saveSeessionReport(){ 
        
       // debug($this->request->data);exit;
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonId = $this->request->data['lessonId'];
        $lessonDetails = $LessonsTable->get($lessonId, ['contain' => [
            'Programs.Clients'
        ]]);
        
        
//        debug($lessonDetails->toArray());exit;
        $ltime = date("H:i:s", strtotime($this->request->data['lessonTime']));        
        $ld = explode('/',$this->request->data['lessonDate']);
        $ldate = $ld[2].'-'.$ld[1].'-'.$ld[0];       
            
        $lessonDetails->note = $this->request->data['note'];
        $lessonDetails->reason = $this->request->data['reason'];
        
        //$lessonDetails->submitted = $this->request->data['submitted'];
        $lessonDetails->submitted = date("Y-m-d H:i:s");
        $lessonDetails->date = $ldate;
        $lessonDetails->time = $ltime; 
        
       
        $lessonDetails->status = "Completed";
        $LessonsTable->save($lessonDetails); 
//        debug($lessonDetails->toArray()); exit;
        
        
       if (($lessonDetails->program->client->email_notif) === 'Yes'):
                $this->sessionReportEmail($lessonId);
       endif;
        
        if(isset($this->request->data['box'])):
            //place email here
//            $this->emailTutorSessionReport($this->request->data['lessonId']);
        endif;
        
        $this->changeBookings($lessonDetails->program_id);
        $this->redirect(['action' => 'index']);
        
    }
    public function changeBookings(){
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails = $ProgramsTable->find("all",['conditions' => [
            'Programs.status' => 'In Progress',
            'Programs.tutor_id' => $this->request->session()->read("id")
        ]])->contain(['Lessons']);
        $x = 0;
        foreach($programDetails as $prog):
            
            $up = "Yes";
            foreach($prog->lessons as $lesson):
                
                if($lesson->status == "Pending" || $lesson->status == "In Progress" || $lesson->status == "Scheduled"):
                    $up = "No";
                endif;
                
            endforeach;
            
            if($up == "Yes"):
                
                $progDetails = $ProgramsTable->get($prog->id);
                $progDetails->status = "Completed";
                $ProgramsTable->save($progDetails);
                $x++;
                echo $prog->id . "<br/>";
            endif;
            
        endforeach;
//        echo "$x -> Successful";
//        debug($programDetails->toArray());
//        exit;
        
    }
    
    private function getCalendarDetails(){
                
        $tutorInfo = $this->checkLoginStatus();        
        $LessonsTable = TableRegistry::get('Lessons');
        $lessonDetails= $LessonsTable->find('all', ['conditions' => [
            'Lessons.date >=' => date('Y-m-d', strtotime("-21 days")),
            'Lessons.date <=' => date('Y-m-d', strtotime("+14 days")),
            'Programs.status not in' => ['Completed','Cancelled'],
            'Programs.tutor_id' => $tutorInfo->id
        ]])->contain(['Programs.Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            },'Programs.Clients'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            },'Programs.ProgramStudents.Students'=>function ($q) {
                return $q->select(['id','name']);
            }])->order(['Lessons.date']);        
        return $lessonDetails;
        
    }
   
    
    private function getConsultations($tutor_id) {
        $consultationTbl = TableRegistry::get('Consultations');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status' => 'Pending Consultation'
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students'])
                ->distinct('Consultations.id')
                ->matching('Leads', function($q) use($tutor_id) {
        return $q->where(['Leads.tutor_id' => $tutor_id]); });
        return $consultations;
    }

    private function getConsultationDetails($tutorID) {
        $leadsTable = TableRegistry::get('Leads');
        $consultationsTable = TableRegistry::get('Consultations');
        $leadsQuery = $leadsTable->find('all', ['conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.tutor_id' => $tutorID,
            ]])->distinct('Leads.consultation_id')
                ->select('Leads.consultation_id');
        $consultationDetails = $consultationsTable->find()
                ->where(['Consultations.id in' => $leadsQuery]);
       return $consultationDetails;
    }

    private function getStudentAssessmentTypes($leads = null) {

        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $eachLeads = $leads->toArray();
        foreach ($eachLeads as $key => $lead):
            $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                    'StudentAssessmentTypes.student_id' => $lead->student_id,
                    'StudentAssessmentTypes.status != ' => 'In Active',
            ]]);
            if (!empty($studentAssessmentDetails)):
                $eachLeads[$key]['studentAssessmentTypeDetails'] = $studentAssessmentDetails->toArray();
            else:
                $eachLeads[$key]['studentAssessmentTypeDetails'] = array();
            endif;
        endforeach;
        return $eachLeads;
    }

    private function getAssessmentResults($tutorId = null) {
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $students = $studentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.tutor_id' => $tutorId,
                        'StudentAssessmentTypes.status' => 'Done'
            ]])->contain(['AssessmentTypes', 'Students']);
        return $students;
    }

    public function clientView() {
        $this->viewBuilder()->layout('blank');
        $consultation_id = $this->request->data('consultation_id');
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.consultation_id' => $consultation_id
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects', 'LeadSubjects', 'LeadSubjects.Subjects']);
//        debug($leads->toArray());exit;
        $this->set('leads', $leads);
    }

    public function bookConsultation($consultation_id) {
        $tutorInfo = $this->checkLoginStatus();
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.consultation_id' => $consultation_id,
                        'Leads.tutor_id' => $tutorInfo->id
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects']);
        $assessmentsTbl = TableRegistry::get('AssessmentTypes');
        $assessments = $assessmentsTbl->find('all');
//        debug($assessments->toArray()); exit;
        $this->set(compact('leads', 'assessments', 'consultation_id'));
    }

    public function assessmentsView() {
        $this->viewBuilder()->layout('blank');
        $id = $this->request->data('id');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentDetails = $studentTable->find('all', [
                    'conditions' => ['Students.id' => $id]
                ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $studentAssessmentDetails = $studentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.student_id' => $id,
                        'StudentAssessmentTypes.status !=' => 'In Active'
            ]])->contain(['AssessmentTypes', 'Students']);

        $assessmentTypeDetails = $assessmentTypesTable->find('all')
                ->contain(['Subjects'])
                ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'studentAssessmentDetails', 'token'));
    }

    private function getStudentSubject($studentSubjects) {

        $subjectsTable = TableRegistry::get('Subjects');
        $subjectArray = array();
        foreach ($studentSubjects as $subject):

            $studentDetails = $subjectsTable->find('all', [
                        'conditions' => ['Subjects.id' => $subject->subject_id]
                    ])->first();
            $subjectArray[] = $studentDetails->name;

        endforeach;
        sort($subjectArray);
        return $subjectArray;
    }

    public function consultationView() {
        $this->viewBuilder()->layout('blank');
        $consultation_id = $this->request->data('consultation_id');
        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadQuestions = $leadAnswersTbl->find('all', [
            'conditions' => ['LeadAnswers.consultation_id' => $consultation_id]
        ]);
        foreach($leadQuestions as $questions):
            $answer = $questions->answer;
        endforeach;
        $this->set('leadQuestions', $leadQuestions);
        $this->set(compact('answer'));
    }
    
    

    public function leadAnswers() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $leadAnsTbl = TableRegistry::get('LeadAnswers');
            $consultation_id = 0;
            foreach ($this->request->data() as $key => $value) {
                $answers = $leadAnsTbl->get($key);
                $consultation_id = $answers->consultation_id;
                $answers->answer = $value;
                $leadAnsTbl->save($answers);
            }
            $this->updateConsultationStatus($consultation_id);
            return $this->redirect($this->referer() . "#consultation-updated");
        }
    }
    
    
    private function updateConsultationStatus($consultation_id = null) {
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultation = $consultationsTbl->get($consultation_id);
        
        if($consultation->status == "Pending Consultation"):
            $consultation->status = 'Completed Consultation';
        endif;
        
        $consultationsTbl->save($consultation);
          
    }

//    private function updateLeadStatus($consultation_id = null) {
//        $leadsTbl = TableRegistry::get('Leads');
//        $leads = $leadsTbl->find('all', [
//            'conditions' => ['Leads.consultation_id' => $consultation_id]
//        ]);
//        foreach ($leads as $lead) {
//            $leadDetail = $leadsTbl->get($lead->id);
//            $leadDetail->status = 'Completed Consultation';
//            $leadsTbl->save($leadDetail);
//        }
//    }

    public function viewConsultation() {
        $this->viewBuilder()->layout('blank');
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.consultation_id' => $this->request->data('id')
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects','Clients']);
        $assessments = $this->getLeadsStudentAssessmentTypes();
        $this->set(compact('leads', 'assessments'));
    }

    private function getLeadsStudentAssessmentTypes() {
        $tutor_id = $this->getTutorID($this->request->session()->read("token"));
        $leadsTable = TableRegistry::get('Leads');
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $leadsQuery = $leadsTable->find('all', ['conditions' => [
                        'Leads.consultation_id' => $this->request->data('id')
            ]])->select('Leads.student_id');
        $assessments = $studentAssessmentTypesTable->find('all', ['conditions' => [
                        'StudentAssessmentTypes.tutor_id' => $tutor_id
            ]])->where(['StudentAssessmentTypes.student_id in' => $leadsQuery]);
        return $assessments;
    }

    private function getTutorID($token) {
        $usersTbl = TableRegistry::get('Users');
        $tutor = $usersTbl->find("all", [
                    "conditions" => [
                        "Users.token" => $token
                    ]
                ])->first();
        return $tutor->id;
    }

    public function editConsultation($consultation_id) {
        $tutorInfo = $this->checkLoginStatus();
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.consultation_id' => $consultation_id,
                        'Leads.tutor_id' => $tutorInfo->id
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects']);
        $consultation = $this->getConsultation($consultation_id);
        $this->set(compact('leads', 'consultation'));
    }

    private function getConsultation($consultation_id) {
        $consultationTbl = TableRegistry::get('Consultations');
        $consultation = $consultationTbl->get($consultation_id);
        return $consultation;
    }

    public function updateConsultation() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $consultation = $this->request->data('conDate') . " " . $this->request->data('conTime');
            $consultationTable = TableRegistry::get('Consultations');
            $consultationDetails = $consultationTable->get($this->request->data('consultation_id'));
            $consultationDetails->consultation_session_date = date('Y-m-d H:i:s', strtotime($consultation));
            $consultationTable->save($consultationDetails);
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index#consultation-updated'));
        }
    }

}
