<?php
namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class ClientsController extends AppController
{

    public function index(){
        $tutorInfo = $this->checkLoginStatus();
        $leadsTbl = TableRegistry::get('Leads');
        $clients = $leadsTbl->find('all', [
            'conditions' => [
                'Leads.tutor_id' => $tutorInfo->id,
                "Leads.status" => "Booked"
            ]
        ])->contain(['Clients.Availabilities.Days','Students.YearLevels','LeadSubjects.Subjects'])
            ->distinct(['Leads.client_id'])
            ->order('Clients.first_name');

        $leads = $leadsTbl->find('all', [
            'conditions' => [
                'Leads.tutor_id' => $tutorInfo->id,
                "Leads.status" => "Booked"
            ]
        ])->contain(['Clients','Students.YearLevels','LeadSubjects.Subjects']);

        $this->set(compact('clients','leads'));
    }

    public function clientView()
    {
        $this->viewBuilder()->layout('blank');
        $token = $this->request->data('token');
        $clientsTbl = TableRegistry::get('Clients');
        $client = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.token' => $token,
                'Clients.type' => 'Client',
            ]
        ]);
        $students = $this->getStudents($token);
        $yrLvlTbl = TableRegistry::get('YearLevels');
        $yrlvls = $yrLvlTbl->find('all');
        $subjsTbl = TableRegistry::get('Subjects');
        $subjs = $subjsTbl->find('all');
        $this->set(compact('client', 'students', 'yrlvls', 'subjs'));
    }

    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $user = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $user->id;
    }

    private function getStudents($client_token){
        $client_id = $this->getUserID($client_token);
        $tutor_id = $this->getUserID($this->request->session()->read("token"));
        $leadsTbl = TableRegistry::get('Leads');
        $students = $leadsTbl->find('all', [
            'conditions' => [
                'Leads.tutor_id' => $tutor_id,
                'Leads.client_id' => $client_id,
                "Leads.status" => "Booked"
            ]
        ])
            ->contain(['Students.YearLevels', 'Students.StudentSubjects.Subjects']);
        return $students;
    }

    public function studentUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $studentsTbl = TableRegistry::get('Students');
            $stdID = $this->request->data('student_id');
            $stdName = $this->request->data('student_name');
            $stdYrlvl = $this->request->data('year_level');

            foreach ($stdID as $key => $std) {
                $student = $studentsTbl->get($std);
                $student->name = $stdName[$key];
                $student->year_level_id = $stdYrlvl[$key];
                $studentDetails = $studentsTbl->save($student);

                if ($studentDetails) {
                    $this->studentSubjUpdate($std, $key);
                }

            }
            return $this->redirect($this->referer() . "#student-updated");
        }
    }

    private function studentSubjUpdate($stdID, $key)
    {
        $stdSubjTbl = TableRegistry::get('StudentSubjects');
        $newSubjs = $this->request->data('subjects' . ($key + 1));
        $subjects = $stdSubjTbl->find('all', [
            'conditions' => ['StudentSubjects.student_id' => $stdID]
        ]);

        foreach ($subjects as $subject) {
            $delStdSubj = $stdSubjTbl->get($subject->id);
            $stdSubjTbl->delete($delStdSubj);
        }
        foreach ($newSubjs as $newSubsID) {
            $newSubject = $stdSubjTbl->newEntity();
            $newSubject->student_id = $stdID;
            $newSubject->subject_id = $newSubsID;
            $stdSubjTbl->save($newSubject);
        }
    }

    public function assessmentView()
    {
        $this->viewBuilder()->layout('blank');
        $id = $this->request->data('id');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentDetails = $studentTable->find('all', [
            'conditions' => ['Students.id' => $id]
        ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $studentAssessmentDetails = $studentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.student_id' => $id,
            'StudentAssessmentTypes.status !=' => 'In Active'
        ]])->contain(['AssessmentTypes', 'Students']);

        $assessmentTypeDetails = $assessmentTypesTable->find('all')
            ->contain(['Subjects'])
            ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'studentAssessmentDetails', 'token'));
    }

    private function getStudentSubject($studentSubjects)
    {

        $subjectsTable = TableRegistry::get('Subjects');
        $subjectArray = array();
        foreach ($studentSubjects as $subject):

            $studentDetails = $subjectsTable->find('all', [
                'conditions' => ['Subjects.id' => $subject->subject_id]
                ])->first();
        $subjectArray[] = $studentDetails->name;

        endforeach;
        sort($subjectArray);
        return $subjectArray;

    }


}
