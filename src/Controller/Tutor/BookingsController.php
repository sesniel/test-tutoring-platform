<?php
namespace App\Controller\Tutor;

use App\Controller\Tutor\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
/**
 * Lessons Controller
 *
 * @property \App\Model\Table\LessonsTable $Lessons
 */
class BookingsController extends AppController {
    
    
    public function index(){
        
        return $this->redirect(array('action' => 'table', 'prefix' => "tutor", '_full' => true));
        
    }
    
    public function table($status = "Active"){
        
        $tutorDetails   = $this->checkLoginStatus();
        $ProgramsTable = TableRegistry::get('Programs');
        if (($this->request->data('status')) == 'Completed'):
           $choices = array ('Completed'); 
           $status = $this->request->data('status');
        else:
            $choices = array ('In Progress', 'Pending');
        endif;
       
       $programDetails = $ProgramsTable->find('all', ['conditions' => [
            'Programs.tutor_id' => $tutorDetails->id,
            'Programs.status in'   => $choices
        ]])->contain(['Clients', 'Tutors', 'Clients.Students']);
//        debug($programDetails->toArray()); exit;
        
        $consultationDetails = $this->getConsultations($tutorDetails->id);
//        debug($consultationDetails->toArray());exit;
        
        
        $this->set(compact('programDetails', 'status', 'consultationDetails'));
        
    }
    
    private function getConsultations($tutor_id) {
        $consultationTbl = TableRegistry::get('Consultations');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status in' => ['Completed Consultation', 'Booked', 'Discontinued Consultation']
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students'])
                ->distinct('Consultations.id')
                ->matching('Leads', function($q) use($tutor_id) {
        return $q->where(['Leads.tutor_id' => $tutor_id]); });
        return $consultations;
    }
    
    public function editDetails($programId = null, $view = "none"){
        
        $tutorDetails   = $this->checkLoginStatus();
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        $this->checkLoginStatus();
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'asc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources'
                    ]);
        $programDetails1  = $ProgramsTable->get($programId,['contain' => 
            ['Lessons', 'Lessons.LessonStudents', 'Lessons.LessonStudents.MajorOutcomes',
             'Lessons.LessonStudents.MinorOutcomes','Lessons.LessonStudents.AssessmentTypes',
             'Lessons.LessonStudents.MinorOutcomes.Resources', 'Lessons.LessonStudents.ProgramStudents',
             'Lessons.LessonStudents.LessonResources',
             'Lessons.LessonStudents.LessonResources.Resources',
             'Lessons.LessonStudents.ProgramStudents.Students', 'Lessons.LessonStudents.MajorOutcomes.YearLevels',
             'Lessons.LessonStudents.MajorOutcomes.Subjects', 'Clients', 
             
            ]]);
       
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['ProgramStudents','ProgramStudents.Students', 'Tutors']]);
        
        $assessmentSelect = array();
        foreach($programDetails->program_students as $students):
            $assessmentSelect[$students->student_id] = $this->assessmentSelect($programDetails->tutor_id, $students->student_id);
        endforeach;
        
         $clientID = $programDetails1->client_id;
         
         $client = $ProgramsTable->find('all', [
                'conditions' => [
                    'Clients.id' => $clientID,
                    'Programs.status in' => 'In Progress',
                    'Programs.tutor_id' => $tutorDetails->id
                 
                ]
            ])->contain(['Clients', 'Tutors', 'Clients.Students']);
          
        $this->set(compact('programDetails', 'lessonDetails', 'assessmentSelect', 'view', 'client'));
        
    }

    public function details($programId = null, $view = "none"){
        
        $tutorDetails   = $this->checkLoginStatus();
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        $this->checkLoginStatus();
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'asc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources'
                    ]);
        $programDetails1  = $ProgramsTable->get($programId,['contain' => 
            ['Lessons', 'Lessons.LessonStudents', 'Lessons.LessonStudents.MajorOutcomes',
             'Lessons.LessonStudents.MinorOutcomes','Lessons.LessonStudents.AssessmentTypes',
             'Lessons.LessonStudents.MinorOutcomes.Resources', 'Lessons.LessonStudents.ProgramStudents',
             'Lessons.LessonStudents.LessonResources',
             'Lessons.LessonStudents.LessonResources.Resources',
             'Lessons.LessonStudents.ProgramStudents.Students', 'Lessons.LessonStudents.MajorOutcomes.YearLevels',
             'Lessons.LessonStudents.MajorOutcomes.Subjects', 'Clients', 
             
            ]]);
       
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['ProgramStudents','ProgramStudents.Students', 'Tutors']]);
        
        $assessmentSelect = array();
        foreach($programDetails->program_students as $students):
            $assessmentSelect[$students->student_id] = $this->assessmentSelect($programDetails->tutor_id, $students->student_id);
        endforeach;
        
         $clientID = $programDetails1->client_id;
         
         //$progStat = array('In Progress', 'Tutor');
         $client = $ProgramsTable->find('all', [
                'conditions' => [
                    'Clients.id' => $clientID,
                    'Programs.status in' => 'In Progress',
                    'Programs.tutor_id' => $tutorDetails->id
                 
                ]
            ])->contain(['Clients', 'Tutors', 'Clients.Students']);
       
     if (($client->toArray())) {
         
      
        //return $this->redirect($this->referer() . "#existing-booking");
    }
        $this->set(compact('programDetails', 'lessonDetails', 'assessmentSelect', 'view', 'client'));
        
    }
    
    public function majoroutcomelist($leadAssessmentTypeId = null, $studentId = null, $tutorId = null, $position = null, $passLessonId = null){
                
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes'); 
                
        $StudentAssessmentTypeDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                'StudentAssessmentTypes.assessment_type_id'  => $leadAssessmentTypeId,
                'StudentAssessmentTypes.student_id'  => $studentId,
                'StudentAssessmentTypes.tutor_id'  => $tutorId,
                'StudentAssessmentTypes.status'  => 'Done'
            ]])->contain(['AssessmentTypes', 'UserQuestions.MinorOutcomes.MajorOutcomes']);
            
        $this->set(compact('StudentAssessmentTypeDetails', 'position','passLessonId'));
        $this->viewBuilder()->layout('blank');
        
        if($leadAssessmentTypeId == 0):
            $this->render('general_details');
        endif;
        
    }
    
    public function topiclist($majorOutcomeId = null, $position = null, $passLessonId = null){
        
        $MajorOutcomesTable = TableRegistry::get('MajorOutcomes'); 
        $majorOutcomeDetails  = $MajorOutcomesTable->get($majorOutcomeId, ['contain' => ['MinorOutcomes', 'MinorOutcomes.UserQuestions']]);
        
        $this->set(compact('majorOutcomeDetails','position', 'passLessonId'));
        $this->viewBuilder()->layout('blank');
    }
    
    public function topiclistlearning($passLessonId = null){
          
        $this->set(compact('passLessonId'));
        $this->viewBuilder()->layout('blank');
    }
    
    public function savelessoninfo(){
        
        $LessonsTable  = TableRegistry::get('Lessons');
//        debug($this->request->data);exit;
        foreach($this->request->data("lesson_id") as $keyLes => $lessonId):
            $lessonDetails = $LessonsTable->get($lessonId);
            $lDate = $this->request->data("lessonDate");
            $lTime = $this->request->data("lessonTime");
            $ld = explode('/',$lDate[$keyLes]);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];

            $lessonDetails->date = $new_date;
            $lessonDetails->time = date('H:i:s', strtotime($lTime[$keyLes]));
//            debug($lessonDetails->toArray()); exit;
            $LessonsTable->save($lessonDetails);
        endforeach;
        
        $LessonStudentsTable = TableRegistry::get('LessonStudents'); 
        foreach($this->request->data['info'] as $key => $lessonId):
            
            if(isset($lessonId['leadAssessmentTypeId']) && $lessonId['leadAssessmentTypeId'] != 0):
                               
                $saveLessonInfo = $LessonStudentsTable->get($key);
                
                $saveLessonInfo['major_outcome_id']   = (isset($lessonId['majoroutcome']) ? $lessonId['majoroutcome'] : 0);
                $saveLessonInfo['assessment_type_id'] = (isset($lessonId['leadAssessmentTypeId']) ? $lessonId['leadAssessmentTypeId'] : 0); 
                                
                $saveLessonInfo['status'] = "In Progress";
                if(isset($lessonId['minoroutcome']) && $lessonId['minoroutcome'] != 0):
                    $this->processLearningResources($key, $lessonId['minoroutcome']);
                $saveLessonInfo['minor_outcome_id']   = $lessonId['minoroutcome'];
                endif;
                $LessonStudentsTable->save($saveLessonInfo);
            endif;
                        
        endforeach;
        $this->updateProgramStatus($this->request->data['program_id'],"In Progress");
        $this->redirect(['action' => 'view',$this->request->data['program_id']]);
        
    }
    
    public function updateSeessionReport($lessonId){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->get($lessonId);
        $lessonDetails->note = $this->request->data['note'];
        $LessonsTable->save($lessonDetails); 
        return $this->redirect($this->referer());
        
    }
    
    public function view($programId = null, $view = 'none'){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        $this->checkLoginStatus();
        $programDetails  = $ProgramsTable->get($programId,['contain' => 
            ['Lessons', 'Lessons.LessonStudents', 'Lessons.LessonStudents.MajorOutcomes',
             'Lessons.LessonStudents.MinorOutcomes','Lessons.LessonStudents.AssessmentTypes',
             'Lessons.LessonStudents.MinorOutcomes.Resources', 'Lessons.LessonStudents.ProgramStudents',
             'Lessons.LessonStudents.LessonResources',
             'Lessons.LessonStudents.LessonResources.Resources',
             'Lessons.LessonStudents.ProgramStudents.Students', 'Lessons.LessonStudents.MajorOutcomes.YearLevels',
             'Lessons.LessonStudents.MajorOutcomes.Subjects', 'Clients.Students',
             
            ]]);
        
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'asc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources'
                    ]);

        $this->set(compact('programDetails', 'lessonDetails', 'view', 'clients'));
        
    }    
    
    public function saveSeessionReport(){ 
        
       // debug($this->request->data);exit;
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonId = $this->request->data['lessonId'];
        $lessonDetails = $LessonsTable->get($lessonId, ['contain' => [
            'Programs.Clients'
        ]]);
        
        
//        debug($lessonDetails->toArray());exit;
        $ltime = date("H:i:s", strtotime($this->request->data['lessonTime']));        
        $ld = explode('/',$this->request->data['lessonDate']);
        $ldate = $ld[2].'-'.$ld[1].'-'.$ld[0];       
            
        $lessonDetails->note = $this->request->data['note'];
        $lessonDetails->reason = $this->request->data['reason'];
        
        //$lessonDetails->submitted = $this->request->data['submitted'];
        $lessonDetails->submitted = date("Y-m-d H:i:s");
        $lessonDetails->date = $ldate;
        $lessonDetails->time = $ltime; 
        
       
        $lessonDetails->status = "Completed";
        $LessonsTable->save($lessonDetails); 
//        debug($lessonDetails->toArray()); exit;
        
        
       if (($lessonDetails->program->client->email_notif) === 'Yes'):
                $this->sessionReportEmail($lessonId);
       endif;
        
        if(isset($this->request->data['box'])):
            //place email here
            $this->emailTutorSessionReport($this->request->data['lessonId']);
        endif;
        
        $this->changeBookings($lessonDetails->program_id);
        $this->redirect(['action' => 'view',$lessonDetails->program_id]);
        
    }
    
    private function bookingLesson($programId){
        
        $programsTable = TableRegistry::get('Programs');
        $programDetails= $programsTable->get($programId, ['contain' => ['Bookings']]);
        
        if(empty($programDetails['bookings']) && $programDetails->status != "Completed"):
            $this->eventActiveTrigger($programDetails->client_id, "Consultation Completed");
            $programDetails->status = "Completed";
            $programsTable->save($programDetails);
            $this->redirect(['action' => 'table']);
        endif;
        
    }
    
    private function processLearningResources($lessonId, $minorOutcomeId){
        
        if($minorOutcomeId != 0):
            
            $MinorOutcomesTable = TableRegistry::get('MinorOutcomes'); 
            $minorOutcomeDetails= $MinorOutcomesTable->get($minorOutcomeId, ['contain' => ['Resources']]);
            if(!empty($minorOutcomeDetails->resources)):
                $this->saveLearningResources($lessonId, $minorOutcomeDetails->resources);
            endif;
            
        endif;
        
    }
       
    private function updateProgramStatus($programId,$status){
        
        $ProgramsTable  = TableRegistry::get('Programs'); 
        $programDetails = $ProgramsTable->get($programId);
        $programDetails->status = $status;
        $ProgramsTable->save($programDetails);
        
    }
    
    private function saveLearningResources($lessonId, $resourcesDetails){
        
        $LessonResourcesTable = TableRegistry::get('LessonResources'); 
        if(!empty($resourcesDetails)):
            
            $val = (rand(1, count($resourcesDetails)) - 1);
            $lessonResourcesDetails = $LessonResourcesTable->newEntity();
            $lessonResourcesDetails->resources_id = $resourcesDetails[$val]['id'];
            $lessonResourcesDetails->lesson_id = $lessonId;
            $LessonResourcesTable->save($lessonResourcesDetails);
        
        endif;
        
//        debug($resourcesDetails);exit;
//        if(count($resourcesDetails) <= 3):
//            
//            
//            foreach($resourcesDetails as $resource):
//            
//                $lessonResourcesDetails = $LessonResourcesTable->newEntity();
//                $lessonResourcesDetails->resources_id = $resource->id;
//                $lessonResourcesDetails->lesson_id = $lessonId;
//                $LessonResourcesTable->save($lessonResourcesDetails);
//        
//            endforeach;
//            
//        else:
//            
//            $rand_keys = array_rand($resourcesDetails, 3);
//            foreach($rand_keys as $key):                
//                
//                $lessonResourcesDetails = $LessonResourcesTable->newEntity();
//                $lessonResourcesDetails->resources_id = $resourcesDetails[$key]['id'];
//                $lessonResourcesDetails->lesson_id = $lessonId;
//                $LessonResourcesTable->save($lessonResourcesDetails);
//                
//            endforeach;
//                
//        endif;
        
    }
    
    private function assessmentSelect($tutorId = null, $studentId = null){
        
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');        
        $selectLeadAssessment = $StudentAssessmentTypesTable->find('all',['conditions' => [
            'StudentAssessmentTypes.tutor_id' => $tutorId,
            'StudentAssessmentTypes.student_id' => $studentId,
            'StudentAssessmentTypes.status' => "Done"
        ]])->contain(['AssessmentTypes', 'UserQuestions', 'Students']); 
        return $selectLeadAssessment->toArray();
        
    }
        
    public function testProcess(){
       
        
//        $ProgramsTable           = TableRegistry::get('Programs');
//        $progDetails = $ProgramsTable->find('all')->contain(['Students', 'Students.Clients']);
//        foreach($progDetails as $prog):
//            $this->processLessonClient($prog->id);exit;
//        endforeach;
//        debug($progDetails->toArray());
//        exit;
        
    }
    
    public function processLessonClient(){
        
        $LessonsTable           = TableRegistry::get('Programs');
        $lessonDetails         = $LessonsTable->find('all')->contain(['Students', 'Students.Clients']);
//        debug($lessonDetails->toArray());exit;
        foreach($lessonDetails as $det):
            
            $lessInfo = $LessonsTable->get($det->id);
            if(isset($det->student->client->id)):
                $lessInfo->client_id = $det->student->client->id;
                $LessonsTable->save($lessInfo);  
            endif;
                
        endforeach;
        
        echo "done";
        exit;
                
    }
                    
    public function changeBookings(){
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails = $ProgramsTable->find("all",['conditions' => [
            'Programs.status' => 'In Progress',
            'Programs.tutor_id' => $this->request->session()->read("id")
        ]])->contain(['Lessons']);
        $x = 0;
        foreach($programDetails as $prog):
            
            $up = "Yes";
            foreach($prog->lessons as $lesson):
                
                if($lesson->status == "Pending" || $lesson->status == "In Progress" || $lesson->status == "Scheduled"):
                    $up = "No";
                endif;
                
            endforeach;
            
            if($up == "Yes"):
                
                $progDetails = $ProgramsTable->get($prog->id);
                $progDetails->status = "Completed";
                $ProgramsTable->save($progDetails);
                $x++;
                echo $prog->id . "<br/>";
            endif;
            
        endforeach;
        
    }
    
    public function processProgram($progId = null){
        
        $LessonStudentsTable    = TableRegistry::get('LessonStudents');
        $ProgramStudentsTable   = TableRegistry::get('ProgramStudents');
        
        $LessonsTable           = TableRegistry::get('Lessons');
        $ProgramsTable          = TableRegistry::get('Programs');
        $programDetails         = $ProgramsTable->get($progId, ['contain' => ['Students', 'Students.Clients']]);
        debug($programDetails);exit;
        $lessonStudentDetails   = $LessonsTable->findByProgramId($progId)
                    ->contain(['LessonStudents']);
        foreach($lessonStudentDetails as $lesson):
            
                $programStudentsDetails = $ProgramStudentsTable->newEntity();
                $programStudentsDetails->program_id = $progId;
                $programStudentsDetails->student_id = $programDetails->student_id;
                $progInfo = $ProgramStudentsTable->save($programStudentsDetails);
                
                $lessonStudentsDetails = $LessonStudentsTable->newEntity();
                $lessonStudentsDetails->lesson_id = $lesson->id;
                if($lesson->major_outcome_id != null):
                    $lessonStudentsDetails->major_outcome_id   = $lesson->major_outcome_id;
                endif;
                
                if($lesson->minor_outcome_id != null):
                    $lessonStudentsDetails->minor_outcome_id   = $lesson->minor_outcome_id;
                endif;
                
                if($lesson->assessment_type_id != null):
                    $lessonStudentsDetails->assessment_type_id = $lesson->assessment_type_id;
                endif;
                
                $lessonStudentsDetails->program_student_id = $progInfo->id;
                $check = $LessonStudentsTable->save($lessonStudentsDetails);  
            
        endforeach;
        
        
        
    }
    
}
