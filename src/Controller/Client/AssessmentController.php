<?php
namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class AssessmentController extends AppController
{
    
    public function exam($studentAssessmentTypeId = null, $position = 1){
        
        $this->checkLoginStatus();
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
//        debug($studentAssessmentDetails->toArray());exit;
        $this->changeStudentAssessmentTypeStatus($studentAssessmentDetails);
        $userQuestion = $this->getUserQuestion($studentAssessmentTypeId, $position);
        $studentAssessmentDetails['user_question'] = $userQuestion;
        $totalUserQuestionLeft = $this->getTotalUserQuestionCountLeft($studentAssessmentTypeId);
        $totalUserQuestion = $this->getTotalUserQuestionCount($studentAssessmentTypeId);
        $this->set(compact('studentAssessmentDetails', 'totalUserQuestionLeft', 'totalUserQuestion'));
        
    }
    
    public function loganswer($studentAssessmentTypeId = null){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $studentsAT = $UserQuestionsTable->get($this->request->data['UserQuestionId']);
        $studentsAT->user_answer = $this->request->data['answer'];
        $UserQuestionsTable->save($studentsAT);
        $this->redirect(array('action' => 'exam', $studentAssessmentTypeId));
        
    }
    
    public function stimulusanswer($studentAssessmentTypeId = null){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $studentsAT = $UserQuestionsTable->get($this->request->data['UserQuestionId']);
        $studentsAT->user_answer = $this->request->data['reaction'];
        $studentsAT->correct_answer = $this->request->data['reaction'];
        $UserQuestionsTable->save($studentsAT);
        $this->redirect(array('action' => 'exam', $studentAssessmentTypeId));
        
    }
    
    public function stimulusfinalanswer($studentAssessmentTypeId = null){
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentsAT = $UserQuestionsTable->get($this->request->data['UserQuestionId']);
        $studentsAT->user_answer = $this->request->data['reaction'];
        $studentsAT->correct_answer = $this->request->data['reaction'];
        $UserQuestionsTable->save($studentsAT);
        
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->first();
        $this->changeStudentAssessmentTypeStatus($studentAssessmentDetails, "Done");
        $this->emailClientAssessmentResult($studentAssessmentTypeId);
        
        $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
        
    }
    
    public function logfinalanswer($studentAssessmentTypeId = null){
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentsAT = $UserQuestionsTable->get($this->request->data['UserQuestionId']);
        $studentsAT->user_answer = $this->request->data['answer'];
        $UserQuestionsTable->save($studentsAT);
        
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->first();
        $this->changeStudentAssessmentTypeStatus($studentAssessmentDetails, "Done");
        $this->emailClientAssessmentResult($studentAssessmentTypeId);
        
        $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
        
    }
    
    
    public function skipfinal($studentAssessmentTypeId = null, $userQuestionId = null, $position = 1){
        
//        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
//        $studentsAT = $UserQuestionsTable->get($userQuestionId);
//        $studentsAT->user_answer = "#skipped";
//        $UserQuestionsTable->save($studentsAT);
                
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->first();
        $this->changeStudentAssessmentTypeStatus($studentAssessmentDetails, "Done");
        $this->emailClientAssessmentResult($studentAssessmentTypeId);
        
        $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
        
    }
    
    public function skip($studentAssessmentTypeId = null, $userQuestionId = null, $position = 1){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $studentsAT = $UserQuestionsTable->get($userQuestionId);
        $studentsAT->user_answer = "#skipped";
        $UserQuestionsTable->save($studentsAT);
        $this->redirect(array('action' => 'exam', $studentAssessmentTypeId, ($position - 1)));
        
    }
    
    private function changeStudentAssessmentTypeStatus($studentAssessmentDetails = null, $status = "Pending"){
        
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
            
        if($status == $studentAssessmentDetails->status):
            
            $studentAssessmentDetailsUpdate = $StudentAssessmentTypesTable->get($studentAssessmentDetails->id);
            $studentAssessmentDetailsUpdate->status = "In Progress";
            $StudentAssessmentTypesTable->save($studentAssessmentDetailsUpdate);
            
        elseif($status == "Done"):
        
            $studentAssessmentDetailsUpdate = $StudentAssessmentTypesTable->get($studentAssessmentDetails->id);
            $studentAssessmentDetailsUpdate->status = $status;            
            $studentAssessmentDetailsUpdate->done_date = date("Y-m-d H:i:s");
            $StudentAssessmentTypesTable->save($studentAssessmentDetailsUpdate);
            
        endif;
        
    }
    
    private function getTotalUserQuestionCountLeft($studentAssessmentTypeId = null){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' => $studentAssessmentTypeId,
            'UserQuestions.user_answer' => ''
        ]])->count(); 
        
        return $userQuestionDetails;
        
    }
    
    
    private function getTotalUserQuestionCount($studentAssessmentTypeId = null){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' => $studentAssessmentTypeId,        ]])->count(); 
        
        return $userQuestionDetails;
        
    }
    
    private function getUserQuestion($studentAssessmentTypeId = null){
        
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
//        $testPosition = 1;
//        do{
//            
//            $userQuestionDetails= $UserQuestionsTable->find('all', ['conditions' => [
//                'UserQuestions.student_assessment_type_id' => $studentAssessmentTypeId,
//                'UserQuestions.position' => $testPosition,
//                'UserQuestions.user_answer' => ''
//            ]])->first();       
//            $testPosition++;
//            
//        }while(empty($userQuestionDetails));
//        
//        if(($position + 1) == $testPosition):
//            return $userQuestionDetails;            
//        else:
//            $this->redirect(array('action' => 'exam', $studentAssessmentTypeId, ($testPosition - 1)));
//        endif;
        
            $userQuestionDetails= $UserQuestionsTable->find('all', ['conditions' => [
                'UserQuestions.student_assessment_type_id' => $studentAssessmentTypeId,
                'UserQuestions.user_answer' => ''
            ]])->first(); 
            return $userQuestionDetails;   
    }


}
