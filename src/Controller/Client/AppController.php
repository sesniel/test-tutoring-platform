<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Client;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public function checkLoginStatus()
    {

        $this->viewBuilder()->layout('client');
        $type = ($this->request->session()->check("type") ? $this->request->session()->read("type") : 0);
        if ($type !== "Client") {
            $this->redirect('/');
        } else {
            $clientsTable = TableRegistry::get('Clients');
            $userDetails = $clientsTable->find('all', ['conditions' => ['Clients.token' => $this->request->session()->read("token")]])
                ->select(['id', 'email', 'phone', 'mobile', 'unit_number', 'street_number', 'street_name', 'suburb', 'postcode', 'city', 'study_skill', 'login_status', 'status', 'biography'])
                ->contain(['Students'])
                ->first();
            return $userDetails;
        }

    }

    public function getStudentAssessmentTypes($clientInfo = null, $statuses = null)
    {

        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        foreach ($clientInfo['students'] as $key => $students):
            $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                'StudentAssessmentTypes.student_id' => $students->id,
                'StudentAssessmentTypes.status in ' => $statuses
            ]])->contain(['Tutors', 'AssessmentTypes']);
            $clientInfo['students'][$key]['StudentAssessmentTypes'] = $studentAssessmentDetails->toArray();
        endforeach;
        return $clientInfo;

    }

    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                 Tutor2You | Primary and Secondary | One-on-One Tutoring
                 Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'rosette@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }

    private function getAssmntDetails($id)
    {
        $studentAssmntTypeTable = TableRegistry::get('StudentAssessmentTypes');
        $assessmentDetails = $studentAssmntTypeTable->get($id, [
            'contain' => ['Students.Clients', 'Tutors', 'AssessmentTypes']
        ]);
        return $assessmentDetails;
    }

    public function emailClientAssessmentResult($stdAssmntTypeID)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $assmntDetails = $this->getAssmntDetails($stdAssmntTypeID);
        $this->emailTutorAssessmentResult($stdAssmntTypeID);
        $reportLayout = ($assmntDetails->assessment_type_id > 2) ? 'print-report' : 'print-report-learning';

        $subject = $assmntDetails->student->name . ' Assessment Result';
        $message = '

                Hi ' . $assmntDetails->student->client->first_name . ',

                ' . $assmntDetails->student->name . ' has completed ' . $assmntDetails->assessment_type->type . ' Assessment.

                You can see the report here: ' . $emailInfo[0] . '/client/report/' . $reportLayout . '/' . $stdAssmntTypeID . '

                You can also login to the platform here ' . $emailInfo[0] . ' and access all past results reports.

                ' . $emailInfo[2] . '

                ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($assmntDetails->student->client->email)
           ->subject($subject)
           ->send($message);


    }

    private function emailTutorAssessmentResult($stdAssmntTypeID)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $assmntDetails = $this->getAssmntDetails($stdAssmntTypeID);
        $reportLayout = ($assmntDetails->assessment_type_id > 2) ? 'print-report' : 'print-report-learning';

        $subject = $assmntDetails->student->name . ' Assessment Result';
        $message = '

                Hi ' . $assmntDetails->tutor->first_name . ',

                ' . $assmntDetails->student->name . ' has completed ' . $assmntDetails->assessment_type->type . ' Assessment.

                You can see the report here: ' . $emailInfo[0] . '/tutor/report/' . $reportLayout . '/' . $stdAssmntTypeID . '

                You can also login to the platform here ' . $emailInfo[0] . ' and access all past results reports.

                ' . $emailInfo[2] . '

                ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($assmntDetails->tutor->email)
           ->subject($subject)
           ->send($message);

    }

    public function beforeRender(Event $event)
    {
//        if (!array_key_exists('_serialize', $this->viewVars) &&
//            in_array($this->response->type(), ['application/json', 'application/xml'])
//        ) {
//            $this->set('_serialize', true);
//        }
    }


}
