<?php
namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class ReportController extends AppController
{    
        
    public function index(){
            
        $clientInfo = $this->checkLoginStatus();
        if($clientInfo):
            $updatedClientInfo = $this->getStudentAssessmentTypes($clientInfo, ["Done"]);        
            $this->set(compact('updatedClientInfo'));
        endif;

    }
    
    public function printReport($studentAssessmentTypeId = null){
                
        $this->viewBuilder()->layout('client-highchart-print');
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' =>  $studentAssessmentTypeId
        ]])->contain(['MinorOutcomes'])->toArray();
        $userQuestionInfoDetails = $this->getMajorOutcomeFromMinorOutcome($userQuestionDetails);
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
        $this->set(compact('studentAssessmentDetails', 'userQuestionInfoDetails'));
        
    }
    
    public function printReportLearning($studentAssessmentTypeId = null){
                
        $this->viewBuilder()->layout('client-highchart-print');
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' =>  $studentAssessmentTypeId
        ]])->contain(['MinorOutcomes'])->toArray();
        $userQuestionInfoDetails = $this->getMajorOutcomeFromMinorOutcome($userQuestionDetails);
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
        $this->set(compact('studentAssessmentDetails', 'userQuestionInfoDetails'));
        
    }
    
    public function viewLearning($studentAssessmentTypeId = null){
                
        $this->checkLoginStatus();
        $this->viewBuilder()->layout('client-highchart');
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' =>  $studentAssessmentTypeId
        ]])->contain(['MinorOutcomes'])->toArray();
        $userQuestionInfoDetails = $this->getMajorOutcomeFromMinorOutcome($userQuestionDetails);
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
        $this->set(compact('studentAssessmentDetails', 'userQuestionInfoDetails'));
        if($studentAssessmentDetails->assessment_type->id == 1):
            $this->render('view_learning_style');
        endif;
        
    }
    
    public function printReportLearningStyle($studentAssessmentTypeId = null){
                
        $this->viewBuilder()->layout('client-highchart-print');
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' =>  $studentAssessmentTypeId
        ]])->contain(['MinorOutcomes'])->toArray();
        $userQuestionInfoDetails = $this->getMajorOutcomeFromMinorOutcome($userQuestionDetails);
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
        $this->set(compact('studentAssessmentDetails', 'userQuestionInfoDetails'));
        
    }
    
    public function view($studentAssessmentTypeId = null){
        
        $this->checkLoginStatus();
        $this->viewBuilder()->layout('client-highchart');
        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        
        $userQuestionDetails = $UserQuestionsTable->find('all', ['conditions' => [
            'UserQuestions.student_assessment_type_id' =>  $studentAssessmentTypeId
        ]])->contain(['MinorOutcomes'])->toArray();
        $userQuestionInfoDetails = $this->getMajorOutcomeFromMinorOutcome($userQuestionDetails);
        $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.id' => $studentAssessmentTypeId
        ]])->contain(['Tutors', 'AssessmentTypes', 'YearLevels', 'Students'])->first();
        
//        debug($userInfoDetails);exit;
        $this->set(compact('studentAssessmentDetails', 'userQuestionInfoDetails'));
    }
    
    private function getMajorOutcomeFromMinorOutcome($userQuestionDetails = null){
        
        $MajorOutcomesTable = TableRegistry::get('MajorOutcomes');
        foreach($userQuestionDetails as $key => $minorOutcome){
            
            $MajorDetetails = $MajorOutcomesTable->find('all', ['conditions' => [
                'MajorOutcomes.id' => $minorOutcome->minor_outcome->major_outcome_id
            ]])->first();
            $userQuestionDetails[$key]['major_outcome'] = $MajorDetetails;
            
        }
        
        return $userQuestionDetails;
        
    }


}
