<?php
namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class BookingsController extends AppController {
    
    
    public function index(){
        
        return $this->redirect(array('action' => 'table', 'prefix' => "tutor", '_full' => true));
        
    }
        
    public function table($status = "Active"){
        
        $clientDetails = $this->checkLoginStatus();
        $ProgramsTable = TableRegistry::get('Programs');
       
       if (($this->request->data('status')) == 'Completed'):
           $status = $this->request->data('status'); //debug($status); exit;
           $choices = array ('Completed'); 
       else:
           $choices = array ('In Progress', 'Pending');
       endif;
        
        
        $programDetails = $ProgramsTable->find('all', ['conditions' => [
            'Programs.client_id' => $clientDetails->id,
            'Programs.status in'   => $choices
        ]])->contain(['Clients', 'Tutors', 'Clients.Students']);
        
        $this->set(compact('programDetails', 'status'));
        
    }

    public function details($programId = null, $view = "none"){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        $this->checkLoginStatus();
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'asc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources'
                    ]);
        
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['ProgramStudents','ProgramStudents.Students', 'Tutors']]);
        
        $assessmentSelect = array();
        foreach($programDetails->program_students as $students):
            $assessmentSelect[$students->student_id] = $this->assessmentSelect($programDetails->tutor_id, $students->student_id);
        endforeach;
        
        $this->set(compact('programDetails', 'lessonDetails', 'assessmentSelect', 'view'));
        
    }
    
    public function majoroutcomelist($leadAssessmentTypeId = null, $studentId = null, $tutorId = null, $position = null){
        
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes'); 
                
        $StudentAssessmentTypeDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                'StudentAssessmentTypes.assessment_type_id'  => $leadAssessmentTypeId,
                'StudentAssessmentTypes.student_id'  => $studentId,
                'StudentAssessmentTypes.tutor_id'  => $tutorId,
                'StudentAssessmentTypes.status'  => 'Done'
            ]])->contain(['AssessmentTypes', 'UserQuestions.MinorOutcomes.MajorOutcomes']);
            
        $this->set(compact('StudentAssessmentTypeDetails', 'position'));
        $this->viewBuilder()->layout('blank');
        
    }
    
    public function topiclist($majorOutcomeId = null){
        
        $MajorOutcomesTable = TableRegistry::get('MajorOutcomes'); 
        $majorOutcomeDetails  = $MajorOutcomesTable->get($majorOutcomeId, ['contain' => ['MinorOutcomes', 'MinorOutcomes.UserQuestions']]);
        
        $this->set(compact('majorOutcomeDetails'));
        $this->viewBuilder()->layout('blank');
    }
    
    public function topiclistlearning(){
        
        $this->viewBuilder()->layout('blank');
    }
    
    public function savelessoninfo(){

        $LessonsTable = TableRegistry::get('Lessons'); 
        foreach($this->request->data['lesson_id'] as $key => $lessonId):
            
            $saveLessonInfo = $LessonsTable->get($lessonId);
            $saveLessonInfo['major_outcome_id']   = $this->request->data['majoroutcome'][$key];
            $saveLessonInfo['minor_outcome_id']   = $this->request->data['minoroutcome'][$key];
            $saveLessonInfo['assessment_type_id'] = $this->request->data['leadAssessmentTypeId'][$key];
            $saveLessonInfo['status'] = "In Progress";
            $LessonsTable->save($saveLessonInfo);
            $this->processLearningResources($lessonId, $this->request->data['minoroutcome'][$key]);
                        
        endforeach;
        $this->updateProgramStatus($this->request->data['program_id'],"In Progress");
        $this->redirect(['action' => 'table']);
    }
    
    public function view($programId = null){
        
        $ProgramsTable = TableRegistry::get('Programs');
        $this->checkLoginStatus();
        $programDetails  = $ProgramsTable->get($programId,['contain'=> 
            ['Lessons', 'Lessons.LessonStudents', 'Lessons.LessonStudents.MajorOutcomes',
             'Lessons.LessonStudents.MinorOutcomes','Lessons.LessonStudents.AssessmentTypes',
             'Lessons.LessonStudents.MinorOutcomes.Resources', 'Lessons.LessonStudents.ProgramStudents',
             'Lessons.LessonStudents.LessonResources',
             'Lessons.LessonStudents.LessonResources.Resources',
             'Lessons.LessonStudents.ProgramStudents.Students', 'Lessons.LessonStudents.MajorOutcomes.YearLevels',
             'Lessons.LessonStudents.MajorOutcomes.Subjects', 'Clients.Students'
             
            ]]);
//        debug($programDetails);exit;
        $this->set(compact('programDetails'));
        
    }     
    
    public function saveSeessionReport(){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->get($this->request->data['lessonId']);
        $lessonDetails->note = $this->request->data['note'];
        $lessonDetails->status = "Completed";
        $LessonsTable->save($lessonDetails);
        $this->redirect(['action' => 'view',$lessonDetails->program_id]);
        
    }
    
    private function processLearningResources($lessonId, $minorOutcomeId){
        
        if($minorOutcomeId != 0):
            
            $MinorOutcomesTable = TableRegistry::get('MinorOutcomes'); 
            $minorOutcomeDetails= $MinorOutcomesTable->get($minorOutcomeId, ['contain' => ['Resources']]);
            if(!empty($minorOutcomeDetails->resources)):
                $this->saveLearningResources($lessonId, $minorOutcomeDetails->resources);
            endif;
            
        endif;
        
    }
       
    private function updateProgramStatus($programId,$status){
        
        $ProgramsTable  = TableRegistry::get('Programs'); 
        $programDetails = $ProgramsTable->get($programId);
        $programDetails->status = $status;
        $ProgramsTable->save($programDetails);
        
    }
    
    private function saveLearningResources($lessonId, $resourcesDetails){
        
        $LessonResourcesTable = TableRegistry::get('LessonResources'); 
        if(count($resourcesDetails) <= 3):
            
            
            foreach($resourcesDetails as $resource):
            
                $lessonResourcesDetails = $LessonResourcesTable->newEntity();
                $lessonResourcesDetails->resources_id = $resource->id;
                $lessonResourcesDetails->lesson_id = $lessonId;
                $LessonResourcesTable->save($lessonResourcesDetails);
        
            endforeach;
            
        else:
            
            $rand_keys = array_rand($resourcesDetails, 3);
            foreach($rand_keys as $key):                
                
                $lessonResourcesDetails = $LessonResourcesTable->newEntity();
                $lessonResourcesDetails->resources_id = $resourcesDetails[$key]['id'];
                $lessonResourcesDetails->lesson_id = $lessonId;
                $LessonResourcesTable->save($lessonResourcesDetails);
                
            endforeach;
                
        endif;
        
    }
    
    private function assessmentSelect($tutorId = null, $studentId = null){
        
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');        
        $selectLeadAssessment = $StudentAssessmentTypesTable->find('all',['conditions' => [
            'StudentAssessmentTypes.tutor_id' => $tutorId,
            'StudentAssessmentTypes.student_id' => $studentId,
            'StudentAssessmentTypes.status' => "Done"
        ]])->contain(['AssessmentTypes', 'UserQuestions']);    
        return $selectLeadAssessment;
        
    }
        
}
