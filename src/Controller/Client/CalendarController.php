<?php
namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class CalendarController extends AppController{

    public function lessons(){
        $this->viewBuilder()->layout('client-calendar');
        $getUserID = $this->getUserID($this->request->session()->read("token"));
        $lessonsTbl = TableRegistry::get('Lessons');
        $lessons = $lessonsTbl->find('all', ['conditions' => [
            'Programs.client_id' => $getUserID
            ]])->order('Lessons.date')->contain(['Programs', 'LessonStudents.ProgramStudents.Students', 'LessonStudents.AssessmentTypes']);
        $this->set('lessons',$lessons);
    }

    private function getUserID($token){
        $UserTable = TableRegistry::get('Tutors');
        $userDetails = $UserTable->findByToken($token)->first();
        return $userDetails->id;
    }

}
