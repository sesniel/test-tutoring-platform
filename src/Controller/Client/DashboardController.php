<?php
namespace App\Controller\Client;

use App\Controller\Client\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class DashboardController extends AppController
{    
        
    public function index(){
            
        $clientInfo = $this->checkLoginStatus();
        if($clientInfo):
            $updatedClientInfo = $this->getStudentAssessmentTypes($clientInfo, ["Pending", "Processed", "In Progress"]);        
            $this->set(compact('updatedClientInfo'));
        endif;
//            debug($updatedClientInfo->toArray());exit;
    }


}
