<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class ClientsController extends AppController
{

    public function index($status = "Active"){
        $statusSelect = ($status == "All") ? "%%" : $status; 
        
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;
        $this->checkLoginStatus();
        $clientsTbl = TableRegistry::get('Clients');
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.type' => 'Client',
                'Clients.status like ' => $statusSelect
            ]
        ])->contain(['Students.StudentSubjects.Subjects', 'Availabilities.Days'])
            ->order('Clients.first_name');
        $this->set(compact('clients', 'status'));
    }
    
     public function withoutBookings($status = "Active"){
        
        
        $statusSelect = ($status == "All") ? "%%" : $status; 
        
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;
        $this->checkLoginStatus();
        $clientsTbl = TableRegistry::get('Clients');
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.type' => 'Client',
                'Clients.status like ' => $statusSelect,
               
            ]
        ])->contain(['Students.StudentSubjects.Subjects', 'Availabilities.Days', 'ProgramsWithoutActiveBookings'])
            ->order('Clients.first_name');
       
//        debug($clients->toArray()); exit;
        
        $this->set(compact('clients', 'status'));
    }


    public function passwordReset()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $userID = $this->getUserID($this->request->data('token'));
            $passwordGen = $this->generatePasswordString(7);
            $usersTbl = TableRegistry::get('Users');
            $user = $usersTbl->get($userID);
            $user->password = crypt($passwordGen, 99);
            $userDetails = $usersTbl->save($user);
            if ($userDetails) {
                $this->emailPasswordNotif($this->request->data('token'),$passwordGen);
                return $this->redirect($this->referer() . "#reset");
            }
        }
    }

    private function generatePasswordString($length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }

    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $client = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $client->id;
    }

    private function emailCheck($email)
    {
        $usersTbl = TableRegistry::get('Users');

        $findEmail = $usersTbl->find("all", [
            "conditions" => [
                "Users.email" => $email
            ]
        ]);

        if (count($findEmail->toArray()) >= 2) {
            return 1; 
        } else {
            return 0;
        }

    }

    public function clientUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->emailCheck($this->request->data('email'));
            if ($email) {
                return $this->redirect($this->referer() . "#email-exist");
            } else {
                
                $usersTbl = TableRegistry::get('Users');
                $clientID = $this->getUserID($this->request->data('token'));
                $client = $usersTbl->get($clientID);
                
                if($this->request->data('status') == "Discontinue"):
                    $leadsTbl = TableRegistry::get('Leads'); 
                    $leadsDetails = $leadsTbl->find('all', ['conditions' => [
                        'Leads.client_id' => $clientID
                    ]]);
                    foreach($leadsDetails as $lead):
                        $leadUp = $leadsTbl->get($lead->id);
                        $leadUp->status = "Abandoned";
                        $leadsTbl->save($leadUp);
                    endforeach;
                endif;
                
                $client->first_name = $this->request->data('first_name');
                $client->last_name = $this->request->data('last_name');
                $client->email = $this->request->data('email');
                $client->mobile = $this->request->data('mobile');
                $client->phone = $this->request->data('phone');
                $client->suburb = $this->request->data('suburb');
                $client->status = $this->request->data('status');
                $client->street_number = $this->request->data('street_number');
                $client->street_name = $this->request->data('street_name');
                $client->comments = $this->request->data('comments');
                $clientDetailAddress = $usersTbl->save($client);
                $this->changeUserAddress($clientDetailAddress->id);
                
                $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                    'Users.email' => $this->request->data('email'),
                ]])->contain(['Students.YearLevels', 'States'])->first();
                $contactDetails = $this->getContactEmail($userDetailsUp->email);
                $list = 14;//($user->type == "Tutor") ? 11 : 10;
                $list1 = 11;
                $grades = $this->calculateSubject($userDetailsUp);
                $type = "edit";
                $post = array(
                    'id'                       => $contactDetails['id'], 
                    'email'                    => $userDetailsUp->email,
                    'first_name'               => $userDetailsUp->first_name,
                    'last_name'                => $userDetailsUp->last_name,
                    'orgname'                  => 'Tutor2You',
                    'field[14,0]'              => $userDetailsUp->postcode,
                    'field[9,0]'               => $grades,
                    'field[18,0]'              => $userDetailsUp->type,
                    'field[19,0]'              => $userDetailsUp->status,
                    'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                    'tags'                     => 'api,'.$userDetailsUp->type,
                    'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'p['.$list1.']'             => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                );
                $this->crudContactList($post, $type);

                return $this->redirect($this->referer() . "#contact-updated");
            }
        }
    }

    public function studentUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) {

            $studentsTbl = TableRegistry::get('Students');            
            $student = $studentsTbl->get($this->request->data('student_id'));
            $student->name = $this->request->data('student_name');
            $student->year_level_id = $this->request->data('year_level');
            $studentDetails = $studentsTbl->save($student);

            if ($studentDetails) {
                $this->studentSubjUpdate($this->request->data('student_id'));
            }

            return $this->redirect($this->referer() . "#student-updated");
        }
    }

    private function studentSubjUpdate($stdID)
    {
        $stdSubjTbl = TableRegistry::get('StudentSubjects');
        $newSubjs = $this->request->data('subjects');
        $subjects = $stdSubjTbl->find('all', [
            'conditions' => ['StudentSubjects.student_id' => $stdID]
        ]);

        foreach ($subjects as $subject) {
            $delStdSubj = $stdSubjTbl->get($subject->id);
            $stdSubjTbl->delete($delStdSubj);
        }
        foreach ($newSubjs as $newSubsID) {
            $newSubject = $stdSubjTbl->newEntity();
            $newSubject->student_id = $stdID;
            $newSubject->subject_id = $newSubsID;
            $stdSubjTbl->save($newSubject);
        }
    }

    public function addStudents()
    {
        if ($this->request->is('post') || $this->request->is('put')) {

            $token = $this->request->data('client_token');
            $clientID = $this->getUserID($token);
            if ($clientID) {
                $studentsTbl = TableRegistry::get('Students');
                $students = $this->request->data('addStdName');
                $yearLevel = $this->request->data('addYrLvlId');

                foreach ($students as $key => $student) {
                    $std = $studentsTbl->newEntity();
                    $std->client_id = $clientID;
                    $std->year_level_id = $yearLevel[$key];
                    $std->name = $student;
                    $studentDetail = $studentsTbl->save($std);

                    if ($studentDetail) {
                        $this->newStdSubjs($studentDetail->id, $key);
                    }

                }
                return $this->redirect(array('controller' => 'allocateLead', 'action' => 'created', $token, 'prefix' => "admin", '_full' => true));
            }
        }
    }

    private function newStdSubjs($stdID, $key)
    {
        $stdSubjTbl = TableRegistry::get('StudentSubjects');
        $subjects = $this->request->data('addSubjId' . ($key + 1));

        foreach ($subjects as $subject) {
            $stdSubj = $stdSubjTbl->newEntity();
            $stdSubj->student_id = $stdID;
            $stdSubj->subject_id = $subject;
            $stdSubjTbl->save($stdSubj);
        }
    }

    public function changeStatus()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $userID = $this->getUserID($this->request->data('token'));
            $usersTbl = TableRegistry::get('Users');
            $leadsTbl = TableRegistry::get('Leads');
            $programsTbl = TableRegistry::get('Programs');
            $user = $usersTbl->get($userID, ['contain' => ['Leads', 'Programs']]);
            $user->status = $this->request->data('status');
//            debug($user);exit;
    
            foreach($user->leads as $lead):
                
                $lead = $leadsTbl->get($lead->id);
                $lead->status = "Abandoned";
                $leadsTbl->save($lead);
                
            endforeach;
            
            foreach($user->programs as $program):
                
                $prog = $programsTbl->get($program->id);
                $prog->status = "Cancelled";
                $programsTbl->save($prog);
                
            endforeach;
            
            
            
            $usersTbl->save($user);
            return $this->redirect($this->referer() . "#status-change");
        }
    }
    

    public function viewClient($token = null, $status = 'Active')
    {
        //$statusSelect = ($status == "All") ? "%%" : $status;
        $choices = array('All', 'Cancel', 'Done', 'In Progress', 'Pending', 'Tutor', 'Active');
        $consultationTbl = TableRegistry::get('Consultations');
        $leadTbl = TableRegistry::get('Leads');
        
        $this->checkLoginStatus();
        
        $clientID = $this->getUserID($token);
        
        if ($this->request->is(['patch', 'post', 'put'])):
            
            if(($this->request->data('status')) == 'All'):
                $status = $this->request->data('status'); 
                $choices = array('Cancelled', 'Completed', 'In Progress', 'Pending'); 
            elseif(($this->request->data('status')) == 'Active'):
                $status = $this->request->data('status'); 
                $choices = array('In Progress', 'Pending'); 
            else:
                $status = $this->request->data('status'); 
                $choices = array($this->request->data('status'));    
            endif;
            
        else:
            $choices = array('In Progress', 'Pending'); 
        endif;
        
        $clientsTbl = TableRegistry::get('Clients');
        $client = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.token' => $token,
                'Clients.type' => 'Client',
            ]
        ])
            ->contain(['Students.YearLevels', 'Students.StudentSubjects.Subjects']);
       
        $yrLvlTbl = TableRegistry::get('YearLevels');
        $yrlvls = $yrLvlTbl->find('all');

        $subjsTbl = TableRegistry::get('Subjects');
        $subjs = $subjsTbl->find('all');
        $cD = $client->toArray();
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails= $ProgramsTable->find('all', ['conditions' => [
            'Programs.client_id' => $cD[0]['id'],
              'Programs.status in ' => $choices
        ]])->contain(['Tutors', 'Clients']);
        
        
        $statusActiveConsultations = $leadTbl->find('all', [
            'conditions' => [
            'Leads.client_id' => $clientID,
            'Leads.status !=' => 'Abandoned'
            ]
        ])->contain(['Consultations'=>function ($q) {
            return $q->where(['Consultations.status in ' => array('New', 'Pending Consultation')]);        
          }]);
        
        $statusActiveBookings = $ProgramsTable->find('all', ['conditions' => [
            'Programs.client_id' => $cD[0]['id'],
              'Programs.status in ' => array('In Progress', 'Pending')
        ]]);

        
        $leads = $leadTbl->find('all', [
            'conditions' => [
            'Leads.client_id' => $clientID,
            'Leads.status !=' => 'Abandoned'
            ]
        ])
            ->contain(['Tutors', 'Clients', 'Students.YearLevels', 'LeadSubjects.Subjects']);

        $stdAssmTypsTbl = TableRegistry::get('StudentAssessmentTypes');
        $stdAssTypes = $stdAssmTypsTbl->find('all', [
            'conditions' => ['Students.client_id' => $clientID]
        ])
            ->contain(['Students', 'Tutors', 'AssessmentTypes']);


        if(isset($this->request->data['editStudent'])):

            $stdDetails = $this->request->data['editStudent'];
            $this->set('stdDetails',$stdDetails);

        endif;

        if(isset($this->request->data['addStudent'])):

            $addStudent = $this->request->data['addStudent'];
            $this->set('addStudent',$addStudent);

        endif;

        if(isset($this->request->data['editLeadSubjs'])):
            $leadInfo = $this->getLeadDetails($this->request->data['editLeadSubjs']);
            $subjetcsTbl = TableRegistry::get('Subjects');
            $regSubjects = $subjetcsTbl->find('all')->order('name');
            $this->set(compact('leadInfo', 'regSubjects'));
        endif;
        
        
        
        $this->set(compact('client', 'yrlvls', 'subjs', 'leads', 'stdAssTypes', 'programDetails', 'status' , 'statusActiveConsultations', 'statusActiveBookings'));

    }
    
//    public function updateView($token){
//        
//        
//        $status = $this->request->data['status'];
//        $this->redirect(['action' => 'viewClient', $token, $status]);
//    }
  
    private function getLeadDetails($lead_id){
        $leadTbl = TableRegistry::get('Leads');
        $leadInfo = $leadTbl->get($lead_id, [
            'contain' => ['Clients', 'Students.YearLevels','Students.StudentSubjects.Subjects', 'LeadSubjects.Subjects','Tutors']
        ]);
        return $leadInfo;
    }

    public function leadSubjUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) { 
            $leadID = $this->request->data('lead_id');
            $newSubjs = $this->request->data('subjects');
            $leadSubjTbl = TableRegistry::get('LeadSubjects');
            $subjects = $leadSubjTbl->find('all', [
                'conditions' => ['LeadSubjects.lead_id' => $leadID]
                ]);
            foreach ($subjects as $subject) {
                $delStdSubj = $leadSubjTbl->get($subject->id);
                $leadSubjTbl->delete($delStdSubj);
            }
            foreach ($newSubjs as $newSubsID) {
                $newSubject = $leadSubjTbl->newEntity();
                $newSubject->lead_id = $leadID;
                $newSubject->subject_id = $newSubsID;
                $leadSubjTbl->save($newSubject);
                $this->redirect($this->referer().'#subjects-updated');
            }
        }
    }
    
    public function details($programId = null){
        $this->checkLoginStatus();
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['Tutors.Rates', 'Clients']]);
       // debug($programDetails->toArray()); exit;
        
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'desc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources',
                            'Rates'
                    ])
                    ->order(['Lessons.Date' => 'asc']) ;
        //  debug($lessonDetails->toArray()); exit;       
////        $programDetails = $ProgramsTable->get($programId, ['contain' => ['Tutors']]);
//                
//        debug($lessonDetails->toArray()); 
        $this->set(compact('programDetails', 'lessonDetails'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    public function edit($lessonId = null){
        
        $this->checkLoginStatus();
        $LessonsTable = TableRegistry::get('Lessons');
        $RatesTable = TableRegistry::get('Rates');
        $lessonDetails= $LessonsTable->get($lessonId, ['contain' => 
            ['Programs'=>function ($q) {
                return $q->select(['id','tutor_id']);
            }, 'Programs.Tutors'=>function ($q) {
                return $q->select(['id','rate_id']);
            }, 'Programs.Tutors.Rates','MajorOutcomes','MajorOutcomes.Subjects','MajorOutcomes.YearLevels', 
             'AssessmentTypes', 'MinorOutcomes', 'LessonResources',
             'LessonResources.Resources']]);
        $rateDetails = $RatesTable->find('all');
        $this->set(compact('lessonDetails','rateDetails'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function delete($lessonId = null){

        $LessonsTable  = TableRegistry::get('Lessons');        
        $entity = $LessonsTable->get($lessonId);
        //2017-09-27
        //$LessonsTable->delete($entity);
        return $this->redirect($this->referer());
        
    }

}
