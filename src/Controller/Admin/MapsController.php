<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Xml;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;


class MapsController extends AppController
{
    
    public function index(){

        $this->checkLoginStatus();
        $this->viewBuilder()->layout('admin-map');
        
    }
    
    public function indexMap($userArray = ""){

        $getClient = 0;
        $getHour = 0;
        $getTutor = 0;
        $getApplicant = 0;
//        $session->delete('query_client');  
//        $session->delete('query_hour'); 
//        $session->delete('query_tutor');  
//        $session->delete('query_applicant');   
        $originLng = 0;
        $originLat = 0;
                
        if ($this->request->is('post') || $this->request->is('put')) {
                        
            if(isset($this->request->data['address']['checkbox'])):
                $this->changeUserAddress("1", $this->request->data['address']['details']);
                $this->request->data['client'] = 1;
            endif;
            
            if(!empty($this->request->data('applicant'))):
                $getApplicant = $this->processGet($this->request->data('applicant'));
            endif;
            
            if(!empty($this->request->data('tutor'))):
                $getTutor = $this->processGet($this->request->data('tutor'));     
            endif;
            
            if(!empty($this->request->data('hour'))): 
                $getHour = $this->processGet($this->request->data('hour'));     
            endif;
            
            if(array_key_exists('Client', $this->request->data['users'])):
                if(!empty($this->request->data('client'))): 
                    $getClient = $this->request->data('client');      
                    $clientsTable = TableRegistry::get('Clients');
                    $clientDetails= $clientsTable->get($this->request->data('client'));
                    $originLng = $clientDetails->lng;
                    $originLat = $clientDetails->lat;
                endif;
                unset($this->request->data['users']['Client']);
            endif;
            
            $x = 1; $userArray = "";
            foreach($this->request->data['users'] as $key => $user):
                if(count($this->request->data['users']) != $x):
                    $userArray .= $key."-";
                else:
                    $userArray .= $key;
                endif;
                $x++;
            endforeach;
            
//        debug($getClient);
//        debug($getHour);
//        debug($getTutor);
//        debug($getApplicant);
//        exit;
            
        } 
        
        $this->set(compact('userArray', 'originLng', 'originLat', 'getApplicant', 'getTutor', 'getHour', 'getClient'));
        $this->viewBuilder()->layout('blank');
        
    }
    private function processGet($arrayType){
        $x = 1;
        $details = "";
        foreach($arrayType as $key => $hour):
            
            if((count($arrayType)) == $x):
                $details .= $key;
            else:
                $details .= $key . "-";
            endif;
            $x++;
        endforeach;
        return $details;        
    }
    
    public function test(){
        
    }
    
    public function links(){

        $this->viewBuilder()->layout('links');
        $clientsTable = TableRegistry::get('Clients');
        $clientDetails = $clientsTable->find('all', ['conditions' => [
            'Clients.type' => 'Client',
            'Clients.status' => 'Active',
            'Clients.lat !=' => ''
        ]])->select(['id', 'first_name', 'last_name'])->order(['Clients.first_name']);
        $this->set(compact('clientDetails'));
    }
    
    public function generate(){
                
        $this->viewBuilder()->layout('blank');
        $usersTable = TableRegistry::get('Users');
        $userDetails = $usersTable->find('all', ['conditions' => [
//            'Users.status' => "Active",
            'Users.address != ' => "",
            'Users.type in ' => ['Tutor', 'Client', 'Applicant']
        ]])
        ->select(['id', 'type', 'first_name', 'last_name', 'email', 'phone', 'mobile', 'address', 'lng', 'lat' ]);
        $this->set(compact('userDetails'));
//        $file = new File(WWW_ROOT . 'map'.DS.'personnel.xml', true, 0777);
               
    }
    
    private function explodeUser($data){
        $submit = explode("-", $data);
        return $submit;
    }
    
    private function checkApplicant($applicantDetails, $applicant){
        
        $status = "No";    
        $qApp = explodeUser($applicant);
        if($qApp[0] !== 0):    //$this->request->session()->read("query_applicant");
            if(in_array('New', $qApp) == 1):
                $status = "Applicant_New";
            elseif($applicantDetails[0]->status == "Reviewed" && in_array('Reviewed', $qApp) == 1):
                $status = "Applicant_Review";
            elseif($applicantDetails[0]->status == "Accepted" && in_array('Accepted', $qApp) == 1):
                $status = "Applicant_Accepted";
            elseif($applicantDetails[0]->status == "Abandoned" && in_array('Abandoned', $qApp) == 1):
                $status = "Applicant_Abandoned";
            endif;  
        endif;          
        return $status;
        
    }
    
    private function checkTutor($availability, $hours, $tutor, $hour){
        
        $status = "No";  
//        $tutor = 0;
        $qApp = $this->explodeUser($tutor);
//        debug($qApp);
//        echo in_array('Available', $qApp) . " --> ";
//        echo $availability . " --> ";
       //  echo in_array('Available', $qApp);
       // exit;
        if($qApp[0] !== 0):  
            // echo "sesniel -> " . $availability . " ==> ";
            // echo "Joshua";
            if($availability == 'Yes' && in_array('Available', $qApp) == 1): 
                
                if($hour !== 0):
                    // echo " -> sesniel";exit;
                    $qApp2 = $this->explodeUser($hour);                
                
                    if($hours == 1  && in_array('one', $qApp2) == 1):
                        $status = "Tutor_One";
                    elseif($hours == 2  && in_array('two', $qApp2) == 1):
                        $status = "Tutor_Two";
                    elseif($hours == 3  && in_array('three', $qApp2) == 1):
                        $status = "Tutor_Three";
                    elseif($hours == 4  && in_array('four', $qApp2) == 1):
                        $status = "Tutor_Four";
                    elseif($hours == "5+" && in_array('five', $qApp2) == 1):
                        $status = "Tutor_Five";
                    endif;
                
                else:
                    $status = "Tutor_Available";
                endif;  
            
            
            elseif(in_array('NotAvailable', $qApp) == 1):    
                $status = "Tutor_NotAvailable";
            endif;
        endif;
        // exit;
        return $status;
        
    }
    
      public function siteXml($format = "xml", $users = 0, $client = 0, $hour = 0,$tutor = 0, $applicant = 0){
        
        $usersTable = TableRegistry::get('Users');
        $this->loadComponent('RequestHandler');     
//        debug($this->explodeUser($users));exit;
        $userDetails = $usersTable->find('all', ['conditions' => [
            'Users.status' => "Active",
            'Users.lat is not ' => "",
            'Users.type in ' => $this->explodeUser($users)
        ]])->contain(['Applications.ApplicationAnswers', 'TutorSubjects.Subjects'])
        ->select(['id', 'type', 'token', 'first_name', 'last_name', 'email', 'phone','availability','hours', 'mobile', 'address', 'lng', 'lat' ])
        ->order(['Users.id' => 'asc']);
        
//        $format = strtolower($format);

        // Format to view mapping
        $formats = [
          'xml' => 'Xml',
          'json' => 'Json',
        ];

        // Error on unknown type
        if (!isset($formats[$format])) {
            throw new NotFoundException(__('Unknown format.'));
        }
        
        $xmlMarker = array();
        $currentType = "";
        
        //Client
        if($client != 0 || is_numeric($users)):

        if(is_numeric($users)):
            $userDetails2 = $usersTable->find('all', ['conditions' => [
                'Users.id' => $users
            ]])->contain(['Applications', 'Leads.Students.StudentSubjects.Subjects'])
            ->select(['id', 'type', 'first_name', 'token', 'last_name', 'email', 'phone','availability','hours', 'mobile', 'address', 'lng', 'lat' ])
            ->first();
        else:
            $userDetails2 = $usersTable->find('all', ['conditions' => [
                'Users.id' => $client
            ]])->contain(['Applications', 'Leads.Students.StudentSubjects.Subjects'])
            ->select(['id', 'type', 'first_name', 'token', 'last_name', 'email', 'phone','availability','hours', 'mobile', 'address', 'lng', 'lat' ])
            ->first();
        endif;
            
            $leadInfo = array();
            $count = 0;
            if(!empty($userDetails2->leads)):
                foreach($userDetails2->leads as $leads): 
                    $count++;
                    if(!empty($leads->student->student_subjects)):
                        $x = 1; $studentSubject = "";
                        foreach($leads->student->student_subjects as $subject):
                            if(count($leads->student->student_subjects) == $x):
                                $studentSubject .= $subject->subject->name;
                            else:
                                $studentSubject .= $subject->subject->name . ", ";
                            endif;
                            $x++;
                        endforeach;
                    endif;
                        $leadInfo[] = array(
                            '@student_name' => $leads->student->name,
                            '@student_subj' => $studentSubject
                        );
                endforeach;
            endif;
        
            $xmlMarker['marker'][] = array(                    
                            '@id' => $userDetails2->id,
                            '@name' => $userDetails2->first_name .' '. $userDetails2->last_name,
                            '@address' => $userDetails2->address,
                            '@lat' => $userDetails2->lat,
                            '@lng' => $userDetails2->lng,
                            '@type' => "Client",
                            '@subjects' => "",
                            '@gpa' => "",
                            '@opatar' => "",
                            '@link' => "/admin/clients/view-client/" . $userDetails2->token,
                            '@student_count' => $count,
                            'leads' => $leadInfo
                    );
        endif; 
        foreach($userDetails as $user): 
            
            if($user->type == "Applicant" && !empty($user->applications)):
                $currentType = $this->checkApplicant($user->applications,$applicant);
            elseif($user->type == "Tutor"):
                $currentType = $this->checkTutor($user->availability, $user->hours, $tutor, $hour);
            else:    
                $currentType = $user->type;
            endif;
            // debug($user);
            // echo $currentType . "<br/>";
            // exit;
            $subjects = "";
            if(!empty($user->tutor_subjects)):
                $x = 1;
                foreach($user->tutor_subjects as $subject):
                    if(count($user->tutor_subjects) == $x):
                        $subjects .= $subject->subject->name;
                    else:
                        $subjects .= $subject->subject->name . ", ";
                    endif;
                    $x++;
                endforeach;             
            endif;
            
            $gpa = "";
            $opatar = "";
            if(!empty($user->applications)):
                foreach($user->applications as $applicant):
                    if(!empty($applicant->application_answer)):
                        $appDetails = json_decode($applicant->application_answer->details);
                        $gpa = $appDetails->GPA;
                        $opatar = $appDetails->HighSchoolResultOP_ATAR;
                    endif;
                endforeach;
            endif;
                
            
            if($user->type == "Applicant"):
                $link = "/admin/trainings/applicant-view/" . $user->token;
            else:
                $link = "/admin/tutors/tutor-view/" . $user->token;
            endif;
            
            if($currentType != "No" && $user->type != "Client"):
                
                $xmlMarker['marker'][] = array(                        
                        '@id' => $user->id,
                        '@name' => $user->first_name .' '. $user->last_name,
                        '@address' => $user->address,
                        '@lat' => $user->lat,
                        '@lng' => $user->lng,
                        '@type' => $currentType,
                        '@subjects' => $subjects,
                        '@gpa' => $gpa,
                        '@opatar' => $opatar,
                        '@link' => $link
                    );
            endif;
        
        endforeach; 
        // exit;
        $markers = $xmlMarker;
        
        // Set Out Format View
        $this->viewBuilder()->className($formats[$format]);
        
//        $this->viewBuilder()->layout('blank');
        $this->set(compact('markers'));
        $this->set('_serialize', ['markers']);
    }
    
    public function destination($format = "xml", $olat, $olng, $lat, $dlng){
        //http://tutor2you.local/admin/maps/destination/json/-27.3728931/153.0310787/-27.486958/152.993034
        
        $locInfo = "origins=$olat,$olng&destinations=$lat,$dlng";        
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?'.$locInfo.'&mode=driving&language=en-EN&sensor=false&key=AIzaSyDudMe2fMw8WDRKUH_wWJkfolMKgKg7gVI';
        
        $result = file_get_contents($url);
        $details = json_decode($result);
        $formats = [
          'xml' => 'Xml',
          'json' => 'Json',
        ];
        
        $this->viewBuilder()->className($formats[$format]);
        
        $this->set(compact('details'));
        $this->set('_serialize', ['details']);
        
    }

}
