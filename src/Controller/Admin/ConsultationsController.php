<?php
namespace App\Controller;

namespace App\Controller\Admin;
use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Controller\Controller;
use Cake\Mailer\Email;

class ConsultationsController extends AppController {
   
    public $paginate = [
        'limit' => 100,
        'order' => [
            'Clients.first_name' => 'asc'
        ]
    ];
    
    public function index($status = "Completed Consultation"){
        
        $this->checkLoginStatus();
        if($status == "All"):
            $status = "%%";
        endif;
        $ConsultationTable = TableRegistry::get('Consultations');
        $consDetails = $this->paginate($ConsultationTable->find('all', ['conditions' => [
            'Consultations.status like ' => $status
        ]])
                ->contain(['Leads'=>function ($q) {
                               return $q->select(['id','consultation_id', 'client_id','status','tutor_id', 'student_id', 'consultation_session_date']);
                           },'Leads.Tutors'=>function ($q) {
                               return $q->select(['id','first_name', 'last_name','email']);
                           },'Leads.Clients'=>function ($q) {
                               return $q->select(['id','first_name', 'last_name','email']);
                           },'Leads.Students'=>function ($q) {
                               return $q->select(['id','name']);
                           }])
                ->select(['id', 'consultation_session_date', 'payroll_date', 'status', 'created']));
        $this->set(compact('consDetails','status'));
        
    }
    
    public function selectstatus(){
        
        return $this->redirect(['action' => 'index', $this->request->data['bookingStatus']]);
        
    }
    
    public function updateConsultationInfo(){
        
        $ConsultationTable = TableRegistry::get('Consultations');
        foreach($this->request->data['lessonDates'] as $key => $lessonDates):
            
            if($this->request->data['lessonDates'][$key] != ""):
                $ld = explode('/',$lessonDates);
                $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0] . " " . date("H:i:s", strtotime($this->request->data['lessonTime'][$key]));
            else:
                $new_date = "";
            endif;
            
            if($this->request->data['payrollDate'][$key] != ""):
                $pd = explode('/',$this->request->data['payrollDate'][$key]);
                $pay_date = $pd[2].'-'.$pd[1].'-'.$pd[0];
            else:
                $pay_date = "";
            endif;
            $consDate = $new_date;
            $consultationDetails = $ConsultationTable->get($key);
            $consultationDetails->consultation_session_date = $consDate;
            $consultationDetails->payroll_date = $pay_date;
            $consultationDetails->status = $this->request->data['status'][$key];
            $ConsultationTable->save($consultationDetails);
            
        endforeach;
        return $this->redirect(['action' => 'index', $this->request->data['bookingStatus']]);
        
    }

}
