<?php
namespace App\Controller;

namespace App\Controller\Admin;
use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Controller\Controller;
use Cake\Mailer\Email;

class BookingsController extends AppController {
    
    public $paginate = [
        'limit' => 100,
        'order' => [
            'Clients.first_name' => 'asc'
        ]
    ];
    
    public function rebook($status = "Cancelled"){
        
        $this->checkLoginStatus();
        $programsTable = TableRegistry::get('Programs');
        
        if($status == "Cancelled"):
            $cond = " != ";
        else:            
            $cond = "";
        endif; 
        
       
        
        $programDetails = $programsTable->find('all', ['conditions' => [
            'Programs.status' . $cond => $status,
            //'Programs.rebook_lessons in ' => 3,
            "Programs.rebook_status is null"
        ]])->contain(['RebookLessons', 'ProgramStudents.Students', 'Clients'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            },'Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }]);  
//            debug($programDetails->toArray());exit;
        
        $this->set(compact('programDetails'));        
        
        
    }
    
    
    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,
                Tutor2You | Primary and Secondary | One-on-One Tutoring
                Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'joshua@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }
    
   public function reviewedNotification ($id) {
        
//        $this->checkLoginStatus();
//        $emailInfo = $this->emailInfo();
//        $applicantDetails = $this->get($id);
       
        $emailInfo = $this->emailInfo();
        $applicantTbl = TableRegistry::get('Users');
        $applicantDetails = $applicantTbl->get($id);
        
        $message = '
            
           Hi ' . $applicantDetails->first_name . ',
               
           Thank you for your application with us at Tutor2You. I am following up as we have an opening in your area and would like to progress you to the next stage of the application process if you\'re still interested.
           
           The application processes is made up of various stages including; 
           - Initial questionnaire (Next phase) 
           - Phone interview 
           - Contract review & orientation
           - Face to face interview 
           - Official offer (please note the current starting tutor pay rate is $27.50/hr plus super where applicable) 
           - Systems Training
           
           Please be advised you will need to be willing to obtain a Working With Children Check. 
            
           In addition to this email, you will also receive login details to our Tutoring Platform and further details regarding the next step.
           
           If you have any questions about the process please let me know. 
           
           ' . $emailInfo[3] . '
           
            ';
        
        echo $message;exit;
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
        ->to('tammy@tutor2you.com.au')
//        ->to($moduleUsers->tutor->owner->email)
//        ->replyTo('tammy@tutor2you.com.au')
        
        ->send($message);
//        debug($applicantDetails);exit;
    }
    
    public function bookingLessons(){
        
        $this->checkLoginStatus();
        $this->loadComponent('Paginator');
        $ClientTable = TableRegistry::get('Clients');
        $clientDetails = $this->paginate($ClientTable->find('all', ['conditions' => ['Clients.type' => 'Client', 'Clients.status' => 'active']])
                ->contain(['Programs.Bookings', 
                    'Programs.Tutors'=>function ($q) {
                                            return $q->select(['id','first_name','last_name']);
                                        }])
                ->select(['id', 'first_name', 'last_name', 'email']));
        $this->set(compact('clientDetails'));
//        echo count($clientDetails->toArray());
     //  debug($clientDetails->toArray());exit;
        
    }
    
    public function lessons($selectedClient = null){
        
        $this->checkLoginStatus();
        $ClientTable = TableRegistry::get('Clients');
        $client = $ClientTable->find('all', ['conditions' => ['Clients.type' => 'Client','Clients.status' => 'active']])
                              ->order(['first_name' => 'ASC'])
                              ->select(['id', 'first_name', 'last_name']);
        
        if ($this->request->is('post') || $this->request->is('put') || $selectedClient != null):
            
            if($selectedClient == null):
                $selectedClient = $this->request->data("id");
            endif;
            $clientDetails = $this->paginate($ClientTable->find('all', ['conditions' => [   
                    'Clients.id' => $selectedClient, 
                    'Clients.status' => 'active']])
                    ->contain(['Programs.Bookings','Students', 
                        'Programs.Tutors'=>function ($q) {
                                                return $q->select(['id','first_name','last_name']);
                                            }])                       
                    ->select(['id', 'first_name', 'last_name', 'email']))->first();
            
        endif;
        
        $this->set(compact('clientDetails', 'selectedClient', 'client'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function updateLessonDetails($selectedClient = null){
        
        $LessonTable = TableRegistry::get('Lessons');
//        debug($this->request->data);
        foreach($this->request->data("lessonDates") as $key => $lessonDetails):
            $ltime = date("H:i:s", strtotime($this->request->data['lessonTime'][$key]));        
            $ld = explode('/',$lessonDetails);
            $ldate = $ld[2].'-'.$ld[1].'-'.$ld[0];       
            $stats = $this->request->data['programStatus'][$key];
            if($this->request->data['payrollDates'][$key] != ""):
                $pd = explode('/',$this->request->data['payrollDates'][$key]);
                $pdate = $pd[2].'-'.$pd[1].'-'.$pd[0];
                $stats = "Completed";
            else:
                $pdate = "";
            endif;
            
            $length = $this->request->data['length'][$key];
//            echo $stats;
//            debug($this->request->data);exit;
            $lessonUpdate = $LessonTable->get($key);            
            $lessonUpdate->status = $stats;
            $lessonUpdate->date = $ldate;
            $lessonUpdate->time = $ltime;
            $lessonUpdate->payroll_date = $pdate;
            $lessonUpdate->length = $length;
//            debug($lessonUpdate);
            $LessonTable->save($lessonUpdate);
//            exit;
//            debug($lessonDetails);
        endforeach;
        return $this->redirect(['action' => 'lessons', $selectedClient]);
        exit;
        
    }
    
    public function tutor(){
        
        $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.type' => "Tutor",
            'Tutors.status' => 'Active'
        ]])->contain(['BookingPrograms.ProgramBookings', 'BookingPrograms.Clients'=>function ($q) {
                return $q->select(['id', 'email_notified', 'first_name', 'last_name']);
            }])
            ->select(['id', 'email_notified', 'first_name', 'last_name']);
        
        $date = date("Y-m-d");
        if ($this->request->is('post') || $this->request->is('put')) {
            $ld = explode('/',$this->request->data['date']);
            $date = $ld[2].'-'.$ld[1].'-'.$ld[0];
        }
//        debug($tutorDetails->toArray());exit;
        $this->set(compact('tutorDetails', 'date'));
        
    }
    
    public function sendEmail($tutorId, $date){

        $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.id' => $tutorId
        ]])->contain(['BookingPrograms.ProgramBookings', 'BookingPrograms.Clients']);
        
        //
        $display = "";
        foreach($tutorDetails as $tutor):         
        
        
        
        $header = '<div class="row">
                        <div class="col-sm-12 col-md-12">
                        <h3 style="text-align: left"><strong>'.$tutor->first_name. " " . $tutor->last_name.'</strong></h3>
                        </div>
                    </div><hr class="customHr"><br/>';
        
            foreach($tutor->booking_programs as $booking):
            
                if(!empty($booking->program_bookings)):
                    
                    if($booking->status == "Tutor"):
                        $stat = "Pending";
                    else:
                        $stat = $booking->status;
                    endif;
                    $header .= '<table id="bookingStudents"
                       class="table table-striped responsive display table-hover table-bordered">
                    <thead>
                    <tr>
                        <th><div style="text-align: left">Client: '.$booking->client->first_name . ' ' . $booking->client->last_name.', ' . $stat .'</div></th>
                    </tr>
                    </thead>';
                    $header .= "<tbody>";
                    $header .= '<tr><th>';
                    $header .= '<table class="table table-striped responsive display table-hover table-bordered">';
                    
                    $header .= '<tr>';
                    $header .= '<th style="width: 150px;">Status</th>';
                    $header .= '<th style="width: 150px;">Date</th>';
                    $header .= '<th style="width: 150px;">Time</th>';
                    $header .= '<th style="width: 150px;">Length</th>';
                    $header .= '</tr>';
                    foreach($booking->program_bookings as $pbook):
                    
                        if($pbook->status == "Tutor"):
                            $stats = "Pending";
                        else:
                            $stats = $pbook->status;
                        endif;
                        if(date('Ymd', strtotime($date)) >= date('Ymd', strtotime($pbook->date))):
                            $header .= '<tr>';
                            $header .= '<td>'.$stats.'</td>';
                            $header .= '<td>'.date('d/m/Y', strtotime($pbook->date)).'</td>';
                            $header .= '<td>'.date('h:i A', strtotime($pbook->time)).'</td>';
                            $header .= '<td>'.$pbook->length.'</td>';
                            $header .= '</tr>';
                        endif;
                       
                    endforeach;
                    $header .= '</table>';
                    $header .= '</th></tr>';
                    $header .= '</tbody>
                                </table>';
                endif;
            endforeach;
                
            $display .= $header;        
          
        endforeach; 
        
        
        $this->emailSessionNotification($tutorId,$display);
        $tutorUpdate = $TutorsTable->get($tutorId);
        $tutorUpdate->email_notified = date("Y-m-d H:i:s");
        $TutorsTable->save($tutorUpdate);
        return $this->redirect(['controller' => 'bookings', 'action' => 'tutor']);
        
    }

    public function create(){
        
        $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        $ratesTable = TableRegistry::get('Rates');
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.type' => 'Tutor',
            'Tutors.status' => 'Active'
        ]])->order('Tutors.first_name');
        if ($this->request->is('post')) :
            
            $selectedTutor = $this->getTutorDetails($this->request->data('tutor_id'));
            $leadsDetails = $this->getLeadDetailsId($this->request->data('tutor_id'));
            $selectedClient = ( $this->request->data('client_id') ? $this->getClientDetais($this->request->data('client_id')) : array() );
            $selectLeadAssessment = ( $this->request->data('student_id') ? $this->getLeadDetailsId($this->request->data('tutor_id'), $this->request->data('client_id'), $this->request->data('student_id')) : array() );
            $selectedStudent = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );
            $typeOfAssessments = ( $this->request->data('student_id') ? $this->getStudentAssessmentType($this->request->data('student_id'), $this->request->data('tutor_id')) : array() );
            $studentId = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );
           
        endif;
        $rateDetails = $ratesTable->find('all');
        $this->set(compact('rateDetails', 'studentId', 'typeOfAssessments', 'selectedStudent', 'tutorDetails', 'selectedTutor', 'leadsDetails', 'selectedClient', 'selectLeadAssessment'));

    }

    public function createRebook($programId = 0){
        
        $this->checkLoginStatus();
        $ProjectsTable = TableRegistry::get('Programs');
        $TutorsTable = TableRegistry::get('Tutors');
        $ratesTable = TableRegistry::get('Rates');
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.type' => 'Tutor',
            'Tutors.status' => 'Active'
        ]])->order('Tutors.first_name');
        $rateDetails = $ratesTable->find('all');
        
        if($programId >= 1):
            
            $projectDetails = $ProjectsTable->get($programId, ['contain' => ['ProgramStudents']]); 
            $selectedTutor = $this->getTutorDetails($projectDetails->tutor_id);
            $leadsDetails = $this->getLeadDetailsId($projectDetails->tutor_id);
            $selectedClient = $this->getClientDetais($projectDetails->client_id);
            
            $selectLeadAssessment = ( $this->request->data('student_id') ? $this->getLeadDetailsId($this->request->data('tutor_id'), $this->request->data('client_id'), $this->request->data('student_id')) : array() );
            $selectedStudent = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );
            $typeOfAssessments = ( $this->request->data('student_id') ? $this->getStudentAssessmentType($this->request->data('student_id'), $this->request->data('tutor_id')) : array() );
            $studentId = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );

        else:
        
            if ($this->request->is('post')) :

                $projectDetails = $ProjectsTable->get($programId, ['contain' => ['ProgramStudents']]); 
                $selectedTutor = $this->getTutorDetails($this->request->data('tutor_id'));
                $leadsDetails = $this->getLeadDetailsId($this->request->data('tutor_id'));
                $selectedClient = ( $this->request->data('client_id') ? $this->getClientDetais($this->request->data('client_id')) : array() );
                $selectLeadAssessment = ( $this->request->data('student_id') ? $this->getLeadDetailsId($this->request->data('tutor_id'), $this->request->data('client_id'), $this->request->data('student_id')) : array() );
                $selectedStudent = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );
                $typeOfAssessments = ( $this->request->data('student_id') ? $this->getStudentAssessmentType($this->request->data('student_id'), $this->request->data('tutor_id')) : array() );
                $studentId = ( $this->request->data('student_id') ? $this->request->data('student_id') : 0 );
                $this->viewBuilder()->template('create_rebook2');

            endif;

        endif;
        
        
        $this->set(compact('projectDetails', 'rateDetails', 'studentId', 'typeOfAssessments', 'selectedStudent', 'tutorDetails', 'selectedTutor', 'leadsDetails', 'selectedClient', 'selectLeadAssessment'));

    }
    
    public function autoGenProgram($lead_id){

        $leadTable = TableRegistry::get('Leads');
        $lead = $leadTable->get($lead_id);   

        $programsTable = TableRegistry::get('Programs');
        $program = $programsTable->newEntity();
        $program->status = "Pending";
        $program->tutor_id = $lead->tutor_id;
        $program->student_id = $lead->student_id;
        $program->client_id = $lead->client_id;
        $programInfo = $programsTable->save($program);
        $this->saveProgramStudents($programInfo,$this->getStudentForProgramStudents($lead->client_id));
//        $this->updateLeadStatusToPendingBooking($lead_id);

        return $this->redirect(['action' => 'createLessons', $programInfo->id, $lead_id]);
    }
    
    public function getStudentForProgramStudents($client_id){
        
        $studentTable = TableRegistry::get('Students');
        $studentDetails = $studentTable->find('list', ['conditions' => ['Students.client_id' => $client_id]]);
        foreach($studentDetails as $key => $name):
            $access[] = $key;
        endforeach;
        return $access;
        
    }
    
    public function editbookings($id = null){
        
        $this->checkLoginStatus();
        $ProjectsTable = TableRegistry::get('Programs');
        $RatesTable = TableRegistry::get('Rates');
        $programDetails= $ProjectsTable->find('all', ['conditions' => [
            'Programs.id' => $id
        ]])->contain(['Clients', 'Tutors', 'Rates'])->first();
        $rateDetails = $RatesTable->find('all');
        $this->set(compact('programDetails', 'rateDetails'));
       
    }
    
    public function updateProgram($id = null){
                
        $ProjectsTable = TableRegistry::get('Programs');
        $lessonsTbl = TableRegistry::get('Lessons');
        $programDetails = $ProjectsTable->get($id, ['contain' => ['Lessons']]);
        $programDetails->invoice_number = $this->request->data['number'];
        $programDetails->invoice_amount = $this->request->data['amount'];
        $programDetails->note = $this->request->data['note'];
        if(isset($this->request->data['rebook_status'])):
            $programDetails->rebook_status = $this->request->data['rebook_status'];
        endif;
        if($this->request->data['status'] == "Cancelled"):
            $programDetails->rebook_status = "Discontinued";
            $this->cancelLessons($id);
        elseif($this->request->data['status'] == "In Progress"):
            foreach($programDetails->lessons as $lessons):
                if($lessons->status == "Pending"):
                    $lessons->status = "Scheduled";
                    $lessonsTbl->save($lessons);
                endif;
            endforeach;
        endif;
//        debug($programDetails);exit;
        $programDetails->status = $this->request->data['status'];
        $programDetails->rate_id = $this->request->data['rate_id'];
        $ProjectsTable->save($programDetails);
        return $this->redirect(['action' => 'details', $id]);
        
    }
    
    private function cancelLessons($programId){
        
        $LessonsTable = TableRegistry::get('Lessons');
        $lessonDetails= $LessonsTable->find('all', ['conditions' => [
            'Lessons.program_id' => $programId,
            'Lessons.status in' => ['Pending', 'Tutor', 'In Progress', 'Ready']
        ]])->select(['id', 'status']);
        foreach($lessonDetails as $lesson):
            
            $lessonUp = $LessonsTable->get($lesson->id);
            $lessonUp->status = "Cancelled";
            $LessonsTable->save($lessonUp);
//            debug($lessonUp);
        endforeach;
//        exit;
        
    }
    
    public function changeRebookStatus($programId, $status = 'Rebooked'){
                
        $ProjectsTable = TableRegistry::get('Programs');
        $projectDetails = $ProjectsTable->get($programId);
        
        if($status == "Discontinued"):
            $projectDetails->rebook_status = $status;
            $ProjectsTable->save($projectDetails);
            return $this->redirect($this->referer());
        elseif($status == "Rebooked"):
            $projectDetails->rebook_status = $status;
            $ProjectsTable->save($projectDetails);
           // debug($projectDetails->toArray()); exit;
            return $this->redirect(['action' => 'createRebook', $programId]);
        endif;
        
    }
    
    public function changeStatus($consultationId = null, $status = null){
        
        $consultationsTable = TableRegistry::get('Consultations');
        $consultationDetails= $consultationsTable->get($consultationId, ['contain' => [
            'Leads.Clients.Programs'
        ]]);
        $consultationDetails->status = $status;
        $consultationsTable->save($consultationDetails);
        
        if(!empty($consultationDetails->leads)):
            
            foreach($consultationDetails->leads as $lead):
                
                if(!empty($lead->client->programs)):
                    foreach($lead->client->programs as $program):
                    
                        if ($status == 'Abandoned'):
                          return $this->redirect($this->referer());
                        else:
                           return $this->redirect(['action' => 'changeRebookStatus', $program->id, 'Rebooked']);
                        endif;
                        
                    endforeach;
                else:
                    return $this->redirect(['action' => 'create']);
                endif;
            
            endforeach;
        else:
            
            return $this->redirect(['action' => 'create']);
        
        endif;
        
        debug($consultationDetails->toArray());exit;
//        return $this->redirect($this->referer());
        
    }
    
    public function createProgram(){
        
        $this->eventActiveTrigger($this->request->data['client_id'], "Booked Consultation");
        $ProjectsTable = TableRegistry::get('Programs');
        $progResult = $ProjectsTable->newEntity();
        $progResult->status = "Pending";
        $progResult->tutor_id = $this->request->data['tutor_id'];
        $progResult->client_id = $this->request->data['client_id'];
        $progResult->rate_id = $this->request->data['rate_id'];
        $progResult->invoice_number = $this->request->data['number'];
        $progResult->invoice_amount = $this->request->data['amount'];
        $programInfo = $ProjectsTable->save($progResult);
        $this->saveProgramStudents($programInfo,$this->request->data['studentInfo']);
        return $this->redirect(['action' => 'createLessons', $programInfo->id]); 
      
        
    }    
    public function programRebook(){
        
        $ProjectsTable = TableRegistry::get('Programs');
        $progResult = $ProjectsTable->newEntity();
        $progResult->status = "Pending";
        $progResult->tutor_id = $this->request->data['tutor_id'];
        $progResult->client_id = $this->request->data['client_id'];
        $progResult->rate_id = $this->request->data['rate_id'];
        $progResult->invoice_number = $this->request->data['number'];
        $progResult->invoice_amount = $this->request->data['amount'];
        $programInfo = $ProjectsTable->save($progResult);
        $this->saveProgramStudents($programInfo,$this->request->data['studentInfo']);
//        debug($progResult->toArray()); exit;
        return $this->redirect(['action' => 'createLessonsRebook', $programInfo->id]); 
      
    }
    
    
    public function createLessons($programId = null, $lead_id = null){
        
        $this->checkLoginStatus();
        $ProjectsTable  = TableRegistry::get('Programs');
        $RatesTable  = TableRegistry::get('Rates');
        $programDetails = $ProjectsTable->get($programId, ['contain' => [
                    'Tutors','ProgramStudents','ProgramStudents.Students'
                ]]);
        $rateDetails = $RatesTable->find('all');
//        debug($programDetails->toArray());
        $this->set(compact('programDetails', 'rateDetails', 'lead_id', 'book'));
        $this->viewBuilder()->layout('admin-program');

    }
    
    public function createLessonsRebook($programId = null, $lead_id = null){
        
        $this->checkLoginStatus();
        $ProjectsTable  = TableRegistry::get('Programs');
        $RatesTable  = TableRegistry::get('Rates');
        $programDetails = $ProjectsTable->get($programId, ['contain' => [
                    'Tutors','ProgramStudents','ProgramStudents.Students'
                ]]);
        $rateDetails = $RatesTable->find('all');
//        debug($programDetails->toArray());
        $this->set(compact('programDetails', 'rateDetails', 'lead_id', 'book'));
        $this->viewBuilder()->layout('admin-program');

    }
    
    public function viewLessons($programId = null, $lead_id = null){
        
        $this->checkLoginStatus();
        $ProjectsTable  = TableRegistry::get('Programs');
        $programDetails = $ProjectsTable->get($programId, ['contain' => ['Students', 'Tutors']]);
        $this->set(compact('programDetails'));
        $this->viewBuilder()->layout('admin-program');
        $this->set('lead_id',$lead_id);

    }

    public function sslct(){
        $this->checkLoginStatus();
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');        
        $selectLeadAssessment = $StudentAssessmentTypesTable->find('all',['conditions' => [
            'StudentAssessmentTypes.tutor_id' => $this->request->data('tutor_id'),
            'StudentAssessmentTypes.student_id' => $this->request->data('student_id'),
            'StudentAssessmentTypes.status' => "Done"
        ]])->contain(['AssessmentTypes', 'UserQuestions']);   
        $this->set('typeOfAssessments', $selectLeadAssessment);        
        $this->viewBuilder()->layout('blank');
        
    }
    
 
    
    public function generateLesson($programId = null, $type="new"){ 
                
        $lessonsTable = TableRegistry::get('Lessons');
//        debug($this->request->data); 
        foreach($this->request->data['lessonDates'] as $key => $lesson):
            $ld = explode('/',$lesson);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            
            $lessonDetails = $lessonsTable->newEntity();
            $lessonDetails->program_id = $programId;
            $lessonDetails->date = $new_date;
            $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime'][$key]));
//            $lessonDetails->rate_id = $this->request->data['rate_id'][$key];
            $lessonDetails->length = $this->request->data['length'][$key];
            $lessonDetails->status = "Pending";
            $lessonInfo = $lessonsTable->save($lessonDetails);
            $this->saveLessonStudents($programId, $lessonInfo);
            
        endforeach;
        
        if($type == "new"):
            $this->emailCliengBooking($programId);
            $this->changeProgramStatus($programId, "Pending");
//            if(($this->request->data('lead_id')) !== null):
//                $this->updateLeadStatusToBooked($this->request->data('lead_id'));
//            endif;
            $this->emailLessonGenerated($programId);
//            $this->rebookEmail($programId);
        endif;
            $this->redirect(['action' => 'details',$programId]);
        
    }
    public function generateLessonRebook($programId = null){ 
                
        $lessonsTable = TableRegistry::get('Lessons');
//        debug($this->request->data); 
        foreach($this->request->data['lessonDates'] as $key => $lesson):
            $ld = explode('/',$lesson);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            
            $lessonDetails = $lessonsTable->newEntity();
            $lessonDetails->program_id = $programId;
            $lessonDetails->date = $new_date;
            $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime'][$key]));
//            $lessonDetails->rate_id = $this->request->data['rate_id'][$key];
            $lessonDetails->length = $this->request->data['length'][$key];
            $lessonDetails->status = "Pending";
            $lessonInfo = $lessonsTable->save($lessonDetails);
            $this->saveLessonStudents($programId, $lessonInfo);
            
        endforeach;
        $this->emailLessonGeneratedRebook($programId);
//        debug($lessonDetails->toArray()); exit;
//        $this->changeProgramStatus($programId, "Pending");
//        $this->rebookEmail($programId);
//        
        $this->redirect(['action' => 'details',$programId]);
        
    }
    
    public function generateLesson1($programId = null, $type="new"){ 
                
        $lessonsTable = TableRegistry::get('Lessons');
//        debug($this->request->data); 
        foreach($this->request->data['lessonDates'] as $key => $lesson):
            $ld = explode('/',$lesson);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            
            $lessonDetails = $lessonsTable->newEntity();
            $lessonDetails->program_id = $programId;
            $lessonDetails->date = $new_date;
            $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime'][$key]));
//            $lessonDetails->rate_id = $this->request->data['rate_id'][$key];
            $lessonDetails->length = $this->request->data['length'][$key];
            $lessonDetails->status = "Tutor";
            $lessonInfo = $lessonsTable->save($lessonDetails);
            $this->saveLessonStudents($programId, $lessonInfo);
            
        endforeach;
        
        if($type == "new"):
            $this->changeProgramStatus($programId, "Tutor");
            if($this->request->data('lead_id')):
                $this->updateLeadStatusToBooked($this->request->data('lead_id'));
            endif;
            $this->emailLessonGenerated($programId);
        endif;
            $this->redirect(['action' => 'details',$programId]);
        
    }
    
    private function getViewStatus($status, $booking){
        
            if($status == 'All'):
                $status = $status; 
                $choices = array('Cancelled', 'Completed', 'In Progress', 'Pending'); 
            elseif($status == 'Active'):
                $status = $status; 
                $choices = array('In Progress', 'Pending'); 
            else:
                $status = $status; 
                $choices = array($status);    
            endif;            
            
            if($booking == 'All'):
                $Bookingstats = $booking; 
                $Bookingstatus = array('New','Pending Consultation','Completed Consultation','Abandoned','Booked','Discontinued Consultation'); 
            elseif($booking == 'Active'):
                $Bookingstats = $booking; 
                $Bookingstatus = array('New', 'Pending Consultation'); 
            else:
                $Bookingstats = $booking; 
                $Bookingstatus = array($booking);    
            endif;
            
            return array($status,$choices,$Bookingstats,$Bookingstatus);
            
    }
    
    public function view($status = 'Active', $Bookingstatus = "Active"){
        
        $this->checkLoginStatus();
        if ($this->request->is(['patch', 'post', 'put'])):
            
            $statVal = $this->getViewStatus($this->request->data('status'), $this->request->data('booking'));
            $status = $statVal[0]; 
            $choices = $statVal[1];   
            $Bookingstats = $statVal[2]; 
            $Bookingstatus = $statVal[3];    
            
        else:
            $status = 'Active'; 
            $choices = array('In Progress', 'Pending'); 
            $Bookingstats = 'Active'; 
            $Bookingstatus = array('New', 'Pending Consultation'); 
        endif;
           
          
                
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails= $ProgramsTable->find('all', ['conditions' => [
            'Programs.status in ' => $choices
        ]])->contain(['Tutors', 'Clients']);
        
        $consultations = $this->getConsultations($Bookingstatus);
        
        $this->set(compact('programDetails', 'status', 'consultations', 'Bookingstatus', 'Bookingstats'));
        
        
        
    }
    
   
    public function consultations($Bookingstatus = 'Active') {
        $this->checkLoginStatus();
        
        $status = array('All','Active', 'New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation');
        $consultationTbl = TableRegistry::get('Consultations');
        
//        debug($consultations->toArray());exit;
    if ($this->request->is(['patch', 'post', 'put'])):
        if ($this->request->data('booking') == 'New'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['New'];
        elseif ($this->request->data('booking') == 'Pending Consultation'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['Pending Consultation'];
        elseif ($this->request->data('booking') == 'Abandoned'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['Abandoned'];  
        elseif ($this->request->data('booking') == 'Completed Consultation'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['Completed Consultation']; 
        elseif ($this->request->data('booking') == 'Booked'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['Booked']; 
        elseif ($this->request->data('booking') == 'Discontinued Consultation'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['Discontinued Consultation']; 
        elseif ($this->request->data('booking') == 'All'):
            $Bookingstatus = $this->request->data('booking');
            $status = ['New','Pending Consultation', 'Abandoned','Completed Consultation', 'Booked', 'Discontinued Consultation'];
        endif;
    else:
            $status = ['New', 'Pending Consultation'];
            
     endif;
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status in ' => $status
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students'])
                ->order(['Consultations.consultation_session_date' => 'asc']);
//        $consultations = $this->getConsultations($Bookingstatus);
//        debug($consultations->toArray());exit;
        $this->set(compact('consultations', 'Bookingstatus', 'status'));
    }
    
    private function getConsultations($Bookingstatus) {
        
        $consultationTbl = TableRegistry::get('Consultations');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status in ' => $Bookingstatus
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students'])
                ->order(['Consultations.consultation_session_date' => 'asc']);
        return $consultations;
    }
    
    public function edit($lessonId = null){
        
        $tutorDetails = $this->checkLoginStatus();
        $LessonsTable = TableRegistry::get('Lessons');
        $RatesTable = TableRegistry::get('Rates');
        $lessonDetails= $LessonsTable->get($lessonId, ['contain' => 
            ['Programs'=>function ($q) {
                return $q->select(['id','tutor_id', 'rate_id']);
            }, 'Programs.Tutors'=>function ($q) {
                return $q->select(['id','rate_id']);
            }, 'Programs.Tutors.Rates', 'Programs.Rates','MajorOutcomes','MajorOutcomes.Subjects','MajorOutcomes.YearLevels', 
             'AssessmentTypes','MinorOutcomes', 'LessonResources', 'Rates',
             'LessonResources.Resources']]);
        $rateDetails = $RatesTable->find('all');
        $this->set(compact('lessonDetails', 'tutorDetails', 'rateDetails'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function updateLesson($lessonId = null){

        $LessonsTable = TableRegistry::get('Lessons');
        $lessonDetails= $LessonsTable->get($lessonId);        
        $ld = explode('/',$this->request->data['lessonDates']);
        $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];       
        if($this->request->data['payrolldate'] == ""):
            $lessonDetails->payroll_date = '';
        else:
            $pd = explode('/',$this->request->data['payrolldate']);
            $pay_date = $pd[2].'-'.$pd[1].'-'.$pd[0];     
            $lessonDetails->payroll_date = $pay_date;
        endif;    
        $lessonDetails->date = $new_date;
        $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime']));
        $lessonDetails->length = $this->request->data['length'];
        $lessonDetails->status = $this->request->data['status'];
        $lessonDetails->rate_id = $this->request->data['rate_id'];
        $LessonsTable->save($lessonDetails);
        return $this->redirect(['action' => 'details', $lessonDetails->program_id]);
        
    }

    public function details($programId = null){
        $this->checkLoginStatus();
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['Tutors.Rates', 'Clients', 'Rates']]);
        
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'desc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources',
                            'Rates'
                    ])
                    ->order(['Lessons.Date' => 'asc']) ;
        
        $this->set(compact('programDetails', 'lessonDetails'));
        
        $this->viewBuilder()->layout('admin-program');
        
    }

    public function addNewLessons($programId = null){
        
        $this->checkLoginStatus();
        $ProjectsTable  = TableRegistry::get('Programs');
        $RatesTable  = TableRegistry::get('Rates');
        $programDetails = $ProjectsTable->get($programId, ['contain' => [
                    'Tutors','ProgramStudents','ProgramStudents.Students'
                ]]);
        $rateDetails = $RatesTable->find('all');
//        debug($programDetails->toArray());
        $this->set(compact('programDetails', 'rateDetails'));
        $this->viewBuilder()->layout('admin-program');

    }
    
    public function deleteProgram($programId = null) {

        $ProgramsTable  = TableRegistry::get('Programs');        
        $entity = $ProgramsTable->get($programId);
//        debug($entity);exit;
        //2017-09-27
        //$ProgramsTable->delete($entity);
       // return $this->redirect($this->referer());
        return $this->redirect(['controller' => 'bookings', 'action' => 'view']);
        
    }
    
    public function delete($lessonId = null){

        $LessonsTable  = TableRegistry::get('Lessons');        
        $entity = $LessonsTable->get($lessonId);
        //2017-09-27
        $LessonsTable->delete($entity);
        return $this->redirect($this->referer());
        
    }
        
    private function getLeadDetailsId($tutorId = null, $clientId = null, $studentId = null){

        $LeadsTable = TableRegistry::get('Leads');
        
        if($clientId == null && $studentId == null):

            $leadsDetails = $LeadsTable->find('all', ['conditions' => [
                'Leads.tutor_id' => $tutorId
            ]])->contain(['Clients'])->group(['Clients.first_name', 'Clients.last_name'])->order('Clients.first_name');
            
        else:
        
            $leadsDetails = $LeadsTable->find('all', ['conditions' => [
                'Leads.tutor_id' => $tutorId,
                'Leads.client_id' => $clientId,
                'Leads.student_id' => $studentId
            ]])->contain(['Clients'])->group(['Clients.first_name', 'Clients.last_name'])->order('Clients.first_name');

        endif;
//        debug($leadsDetails->toArray());exit;
        return $leadsDetails;
        
    }
    
    private function getStudentAssessmentType($studentId, $tutorId){
        
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $typeOfAssessments = $StudentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.student_id' => $studentId,
            'StudentAssessmentTypes.tutor_id' => $tutorId
        ]])->contain(['AssessmentTypes','YearLevels']);
        return $typeOfAssessments;
            
    }
    
    private function getClientDetais($id = null){
        
        $ClientsTable = TableRegistry::get('Clients');
        $selectedClient = $ClientsTable->get($id, ['contain' => ['Students']]);
        return $selectedClient;
            
    }
    
    private function getTutorDetails($id = null){
        
        $TutorsTable = TableRegistry::get('Tutors');
        $selectedTutor = $TutorsTable->find('all', ['conditions' => [
            'Tutors.status' => 'Active',
            'Tutors.id' => $id
        ]])->first();
        return $selectedTutor;
            
    }
    
    private function changeProgramStatus($id = null, $status = null){
        
        $ProjectsTable  = TableRegistry::get('Programs');
        $programDetails = $ProjectsTable->get($id);
        $programDetails->status = $status;
        $ProjectsTable->save($programDetails);
                
    }
    
    private function saveProgramStudents($programInfo, $studentInfo){
        
        $ProjectStudentsTable = TableRegistry::get('ProgramStudents');
        foreach($studentInfo as $student):
            
            $progStudentResult = $ProjectStudentsTable->newEntity();
            $progStudentResult->program_id = $programInfo->id;
            $progStudentResult->student_id = $student;
            $ProjectStudentsTable->save($progStudentResult);
            
        endforeach;
        
    }
    
    private function updateLeadStatusToBooked($lead_id){
        $leadTable = TableRegistry::get('Leads');
        $lead = $leadTable->get($lead_id);
        $lead->status = 'Booked';
        $leadTable->save($lead);
    }

//    private function updateLeadStatusToPendingBooking($lead_id){
//        $leadTable = TableRegistry::get('Leads');
//        $lead = $leadTable->get($lead_id);
//        $lead->status = 'Pending Booking';
//        $leadTable->save($lead);
//    }
    
    private function saveLessonStudents($programId, $lessonInfo){
        
        $ProgramStudentsTable = TableRegistry::get('ProgramStudents');
        $LessonStudentsTable = TableRegistry::get('LessonStudents');
        $programStudentsDetails = $ProgramStudentsTable->findByProgramId($programId);
        foreach($programStudentsDetails as $progStudent):
            
            $lessStudentResult = $LessonStudentsTable->newEntity();
            $lessStudentResult->lesson_id = $lessonInfo->id;
            $lessStudentResult->program_student_id = $progStudent->id;  
            $LessonStudentsTable->save($lessStudentResult);      
        
        endforeach;
        
    }
       

    public function leadAnswers() {
        if ($this->request->is('post') || $this->request->is('put')) {
            $leadAnsTbl = TableRegistry::get('LeadAnswers');
            $consultation_id = 0;
            foreach ($this->request->data() as $key => $value) {
                $answers = $leadAnsTbl->get($key);
                $consultation_id = $answers->consultation_id;
                $answers->answer = $value;
                $leadAnsTbl->save($answers);
            }
            $this->updateConsultationStatus($consultation_id);
            return $this->redirect($this->referer() . "#consultation-updated");
        }
    }

    private function updateConsultationStatus($consultation_id = null) {
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultation = $consultationsTbl->get($consultation_id);
        $consultation->status = 'Completed Consultation';
        $consultationsTbl->save($consultation);
    }
    
    public function consultationDetails(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');

        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadAnswers = $leadAnswersTbl->find('all',[
                'conditions' => ['LeadAnswers.lead_id' => $lead_id]
            ]);
        $this->set('leadQuestions',$leadAnswers);
    }
    
    public function consultationView() {
        $this->viewBuilder()->layout('blank');
        $consultation_id = $this->request->data('consultation_id');
        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadQuestions = $leadAnswersTbl->find('all', [
            'conditions' => ['LeadAnswers.consultation_id' => $consultation_id]
        ]);
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultations = $consultationsTbl->find('all', ['conditions' => [
            'Consultations.id' => $consultation_id
        ]])->contain(['Leads.Clients', 'Leads.Students']);
        
        foreach ($consultations as $key => $con):
            $conId = $con->id;
            foreach($con->leads as $lead):
                $leadId = $lead->id;
                $leadClientId = $lead->client->id;
                $studentId = $lead->student->id;
            endforeach;
        endforeach;
        $this->set('leadQuestions', $leadQuestions);
        $this->set(compact('consultation_id', 'consultations', 'con', 'conId', 'leadId', 'leadClientId', 'studentId'));
        
    }
    
    public function discontinue() {
        
        
        $consultation_id = $this->request->data('consultation_id');
//        echo $consultation_id;exit;
        $consultationsTbl = TableRegistry::get('Consultations');
//        $consultations = $consultationsTbl->find('all', ['conditions' => [
//            'Consultations.id' => $consultation_id
//            
//        ]]);
        $consultations = $consultationsTbl->get($consultation_id, ['contain' => [
            'Leads.Clients'
        ]]);
        $consultations->status = 'Discontinued Consultation';
        
        foreach ($consultations->leads as $lead):
            $lead->status = 'Abandoned';
        endforeach;

        $consultationsTbl->save($consultations);
//        debug($consultations->toArray());exit;
        
        $this->redirect($this->referer(). '#discontinued');
        
    }
    
    
    public function reAllocateTutor() {
        echo 'reallocate';exit;
    }
    
    
    public function viewStudentDetails() {
        $this->viewBuilder()->layout('blank');
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.consultation_id' => $this->request->data('consultation_id')
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects','Clients']);
        $this->set(compact('leads'));
    }
    
    public function reallocate(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');
        $student_id = $this->request->data('student_id');
        $this->set(compact('lead_id','student_id'));
    }
    
    public function abandon(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');
        $this->set(compact('lead_id'));
    }
    
    public function abandonLead() {
        $leadTable = TableRegistry::get('Leads');
        $lead = $leadTable->get($this->request->data['lead_id']);
        $lead->note = $this->request->data['reason'];
        $lead->status = "Abandoned";
        $leadTable->save($lead);
        if ($this->request->data('student_id')) {
            $this->redirect(array('controller' => 'allocateLead', 'action' => 'student', $this->request->data('student_id')));
        } else {
            $this->redirect($this->referer() . '#lead-abandoned');
        }
    }
    
    public function changeTutorRate(){
        $tutorTable = TableRegistry::get('Tutors');
        $tutorDetails = $tutorTable->find('all', ['conditions' => ['Type' => 'Tutor']]);
        foreach($tutorDetails as $tutor):
            $tutorUp = $tutorTable->get($tutor->id);
            $tutorUp->rate_id = 1;
//            echo "<pre>".print_r($tutorUp)."</pre>";
//            $tutorTable->save($tutorUp);
        endforeach;
        echo "Success";exit;
    }
    
    public function uninvoiceView($status = "In Progress"){
        $statusSelect = ($status == "All") ? "%%" : $status;
        $this->checkLoginStatus();
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'uninvoice_view', $this->request->data['status']]);
        endif;
                
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails= $ProgramsTable->find('all', ['conditions' => [
            'Programs.status like ' => $statusSelect,
            'Programs.invoice_number' => ''
            
        ]])->contain(['Tutors', 'Clients']);


//        debug($programDetails->toArray());exit;
        $this->set(compact('programDetails', 'status', 'consultations', 'Bookingstatus'));    
        
    }
    
    public function changeBookings(){
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails = $ProgramsTable->find("all",['conditions' => [
            'Programs.status' => 'In Progress'
        ]])->contain(['Lessons']);
        $x = 0;
        foreach($programDetails as $prog):
            
            $up = "Yes";
            foreach($prog->lessons as $lesson):
                
                if($lesson->status == "Pending" || $lesson->status == "Scheduled"):
                    $up = "No";
                endif;
                
            endforeach;
            
            if($up == "Yes"):
                
                $progDetails = $ProgramsTable->get($prog->id);
                $progDetails->status = "Completed";
                $ProgramsTable->save($progDetails);
                $x++;
                echo $prog->id . "<br/>";
            endif;
            
        endforeach;
        echo "$x -> Successful";
//        debug($programDetails->toArray());
        exit;
        
    }
}
