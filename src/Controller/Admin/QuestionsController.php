<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;


/**
 * Questions Controller
 *
 * @property \App\Model\Table\QuestionsTable $Questions
 */
class QuestionsController extends AppController
{
    
    public function changeAvailabilityNotification(){
        
       $tutorsTable = TableRegistry::get('Tutors');
       $tutorDetails = $tutorsTable->find('all', ['conditions' => [
           'Tutors.type' => 'Tutor',
           'Tutors.status' => 'Active'
       ]]);
       foreach($tutorDetails as $tutor):
           $tutorUpdate = $tutorsTable->get($tutor->id);
           $tutorUpdate->available_notification = "2017-10-01";
           $tutorsTable->save($tutorUpdate);
       endforeach;
       
    }
    
    public function changeLessonStatusToScheduled(){
        
       $programsTable = TableRegistry::get('Programs');
       $lessonsTable = TableRegistry::get('Lessons');
       $programDetails = $programsTable->find('all', ['conditions' => [
           'Programs.status' => 'In Progress'
       ]])->contain(['LessonsPending']);
       foreach($programDetails as $program):
           
           if(!empty($program->lessons_pending)):
                foreach($program->lessons_pending as $lesson):
                    $lessonUpdate = $lessonsTable->get($lesson->id);
                    $lessonUpdate->status = "Scheduled";
                    $lessonsTable->save($lessonUpdate);
                    echo $lesson->id . " -> " . $lesson->status . "<br/>";
                endforeach;
           endif;
           
       endforeach;
       exit;
       
    }
    
    public function changeLeadsStatus(){
        
       $leadsTable = TableRegistry::get('Leads');
       $leadsDetails = $leadsTable->find('all', ['conditions' => [
           'Leads.status !=' => 'Abandoned'
       ]]);
       foreach($leadsDetails as $lead):
           
           $leadUpdate = $leadsTable->get($lead->id);
           $leadUpdate->status = "Booked";
           $leadsTable->save($leadUpdate);
           
       endforeach;
       //For Checking
       //SELECT status, count(status) FROM `leads` group by `status`
       echo "Successful";exit;
       
    }
    
    public function changeProgramStatus(){
        
        $programsTable = TableRegistry::get('Programs');
        $programsDetails = $programsTable->find('all');
        foreach($programsDetails as $program):
            
            if($program->status == "Done"):
                $programUp = $programsTable->get($program->id);
                $programUp->status = "Completed";
                $programsTable->save($programUp);
            elseif($program->status == "Tutor"):
                $programUp = $programsTable->get($program->id);
                $programUp->status = "Pending";
                $programsTable->save($programUp);
            elseif($program->status == "Cancel"):
                $programUp = $programsTable->get($program->id);
                $programUp->status = "Cancelled";
                $programsTable->save($programUp);
            endif;
            
        endforeach;
        //For Checking
        //SELECT status, count(status) FROM `programs` group by `status`
        echo "Successful";exit;
        
    }
    
    public function changeLessonStatus(){
        
        $LessonsTable = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->find('all');
        foreach($lessonDetails as $lesson):
            
            $lessonUp = $LessonsTable->get($lesson->id);
            if($lessonUp->status == "Tutor"):
                
                $lessonUp->status = "Scheduled";
                $LessonsTable->save($lessonUp);
            
            elseif($lessonUp->status == "Done"):
                
                $lessonUp->status = "Completed";
                $LessonsTable->save($lessonUp);
            
            elseif($lessonUp->status == "Cancel"):
                
                $lessonUp->status = "Cancelled";
                $LessonsTable->save($lessonUp);
            
            elseif($lessonUp->status == "In Progress"):
                
                $lessonUp->status = "Scheduled";
                $LessonsTable->save($lessonUp);
            
            endif;
            
        endforeach;
        
        //SELECT status, count(status) FROM `lessons` group by `status`
        echo "Successful";exit;
        
        
    }
    
    public function changeBookings(){
        
        $ProgramsTable = TableRegistry::get('Programs');
        $programDetails = $ProgramsTable->find("all",['conditions' => [
            'Programs.status in ' => ['Pending', 'In Progress']
        ]])->contain(['Lessons']);
        $x = 0;
        foreach($programDetails as $prog):
            
            $up = "Yes";
            foreach($prog->lessons as $lesson):
                
                if($lesson->status == "Pending" || $lesson->status == "Scheduled"):
                    $up = "No";
                endif;
                
            endforeach;
            
            if($up == "Yes"):
                
                $progDetails = $ProgramsTable->get($prog->id);
                $progDetails->status = "Completed";
                $ProgramsTable->save($progDetails);
                $x++;
                echo $prog->id . "<br/>";
            endif;
            
        endforeach;
        echo "$x -> Successful";
//        debug($programDetails->toArray());
        exit;
        
    }
    
    private function updateUserUpAddress($userId, $val = 2){
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->get($userId);
        $userDetails->up_address = $val;
        $usersTable->save($userDetails);
        
    }
    
    public function upHourAvailability(){
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->find('all', ['conditions' => [
            'Users.lat is not ' => null,
            'Users.type' => 'Tutor'
        ]])->order(['Users.id' => 'asc']);
        foreach($userDetails as $user):
            
            $hour = rand(1, 5);
            if($hour == 5):
                $hour = "5+";
            endif;
            $availability = "True";
            $userUp = $usersTable->get($user->id);
            $userUp->availability = $availability;
            $userUp->hours = $hour;
            $usersTable->save($userUp);
            
        endforeach;
        
    }
    
    public function changeAvailability(){
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->find('all', ['conditions' => [
            'Users.availability is' => null
        ]]);  
        
        $x = 1;
        foreach($userDetails as $user):
            $userUpdate = $usersTable->get($user->id);
            
            if($x % 2):
                $userUpdate->availability = "True";
            else:
                $userUpdate->availability = "False";
            endif;
            $usersTable->save($userUpdate);
        
            $x++;
        endforeach;
        
    }


    public function updateAddressSesniel(){

        $usersTable  = TableRegistry::get('Users');
        $tutorsTable  = TableRegistry::get('Tutors');

        $tutorDetails = $tutorsTable->find('all', ['conditions' => [
            'Tutors.type' => "Tutor" 
        ]]);

        foreach($tutorDetails as $details):

            $userDetails = $usersTable->get($details->id);
            if($userDetails->address == "" || $userDetails->address == null){
                $userDetails->address = $details->address;
                $userDetails->lng = $details->lng;
                $userDetails->lat = $details->lat;
                $userDetails->up_address = "3";
                $usersTable->save($userDetails);
            }

        endforeach;

    }
    
    public function changeUserAddress(){

        //SELECT * FROM `users` WHERE `status` = "Active" and type = "Tutor" and lng is null
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->find('all', ['conditions' => [
            'Users.address is' => null,
            'Users.up_address is' => null, //UPDATE THIS
            'Users.type' => 'Client',
            'Users.status' => 'Active'
        ]])->order(['Users.id' => 'asc']);
        
      // debug($userDetails->toArray());exit;
        if(!empty($userDetails)):
            foreach($userDetails as $user):

            //reverse geolocation
//                 $userUpdate = $usersTable->get($user->id);
//                 $lng = $userUpdate->lng;
//                 $lat = $userUpdate->lat;
//
////                $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
//                $request_url = 'http://maps.googleapis.com/maps/api/geocode/xml?latlng='.$lat.','.$lng.'&sensor=false';
//                $xml = simplexml_load_file($request_url) or die("url not loading ID: " . $user->id . "<br/>$request_url");
//                $status = $xml->status;
//                if ($status=="OK") {
//                    $userUpdate->address = $xml->result->formatted_address;
//                    echo $xml->result->formatted_address . "<br/>";
//                    $usersTable->save($userUpdate);
//                    $this->updateUserUpAddress($user->id, 4);
//                
//                }

//                 else:
//
//                     $this->updateUserUpAddress($user->id);
//
//                 endif;
                     
                     

                 $userUpdate = $usersTable->get($user->id);
                 $address = $user->street_name . " " . $user->suburb . " " . $user->city;

                 // echo $address . "<br/> ";
                 if($user->street_name != "" && $user->suburb != "" && $user->city != ""):

                     $Address = urlencode($address);
                     $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
                     $xml = simplexml_load_file($request_url) or die("url not loading ID: " . $user->id . " ==> " . $address);
                     // echo $request_url;
                     // debug($xml);exit;
                     $status = $xml->status;
                     if ($status=="OK") {
                         $Lat = $xml->result->geometry->location->lat;
                         $Lon = $xml->result->geometry->location->lng;
                     }
                     $userUpdate->lng = $Lon;
                     $userUpdate->lat = $Lat;
                     $userUpdate->address = $address;
                     $usersTable->save($userUpdate);
                     $this->updateUserUpAddress($user->id, 3);

                 else:

                     $this->updateUserUpAddress($user->id);

                 endif;

            endforeach;
        endif;
        echo "successful";
       exit;
    }
    
    public function minoroutcome($id = null){
        
       $this->checkLoginStatus();
       $questionsTbl = TableRegistry::get('Questions');
       $minoroutcomesTbl = TableRegistry::get('MinorOutcomes');
       $minoroutcomes = $minoroutcomesTbl->find('all');
       
       $questions = $questionsTbl->find('all', [
                    'conditions' => [
                        'Questions.minor_outcome_id' => $id
                    ]
                ])->contain(['MinorOutcomes']);
       
       
     
      $this->set(compact('questions'));
      
//      debug($minoroutcomes->toArray());
      $this->set(compact('minoroutcomes'));
 
//      echo $id; exit;
         
    } 
   
    public function index()
    { 
        $this->checkLoginStatus();
        $this->paginate = [
            'contain' => ['MinorOutcomes']
        ];
        $questions = $this->paginate($this->Questions);

        $this->set(compact('questions'));
        $this->set('_serialize', ['questions']);
    }

    /**
     * View method
     *
     * @param string|null $id Question id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->checkLoginStatus();
        $Majoroutcome = TableRegistry::get('MajorOutcomes');
        $question = $this->Questions->get($id, [
            'contain' => ['MinorOutcomes', 'UserQuestions']
        ]);
        $majorOutcomes = $Majoroutcome->find('all')
            ->where([
                'Majoroutcome.id' => $question->minor_outcome->major_outcome_id
            ])
            ->first();

        $this->set(compact("question", "majorOutcomes"));
        $this->set('_serialize', ['question']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->checkLoginStatus();
        $question = $this->Questions->newEntity();
        if ($this->request->is('post')) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            if ($this->Questions->save($question)) {
                $this->Flash->success(__('The question has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The question could not be saved. Please, try again.'));
            }
        }
        $minorOutcomes = $this->Questions->MinorOutcomes->find('list', ['limit' => 200]);
        $this->set(compact('question', 'minorOutcomes'));
        $this->set('_serialize', ['question']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Question id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->checkLoginStatus();
        $question = $this->Questions->get($id, [
            'contain' => ['MinorOutcomes','MinorOutcomes.MajorOutcomes','MinorOutcomes.MajorOutcomes.Subjects']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            
            $QuestionsTable = TableRegistry::get('Questions');
            $questionDetails= $QuestionsTable->get($question->id);
            $questionDetails->type = $this->request->data['type'];
            $questionDetails->description = $this->request->data['description'];
            $questionDetails->answer = $this->request->data['answer'];
            $questionDetails->choices = $this->request->data['choices'];
            $questionDetails->mark = $this->request->data['mark'];
            $questionDetails->image = $this->request->data['image'];
             
            debug($this->request->data);
            debug($question->id);
//            $QuestionsTable->save($question);
//            exit;
            if ($QuestionsTable->save($question)) {
//                $this->Flash->success('The question has been saved.');
//                echo "joshua";exit;
                echo '<script type="text/javascript">
                window.history.go(-2);
                </script>';
//                return $this->redirect(['action' => 'index']);
               
            } else {
//                echo "sesniel";exit;
//                $this->Flash->error('The question could not be saved. Please, try again.');
            }
        }
        $minorOutcomes = $this->Questions->MinorOutcomes->find('list', ['limit' => 200]);
        $this->set(compact('question', 'minorOutcomes'));
        $this->set('_serialize', ['question']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Question id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $question = $this->Questions->get($id);
        if ($this->Questions->delete($question)) {
            $this->Flash->success(__('The question has been deleted.'));
        } else {
            $this->Flash->error(__('The question could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    
    public function userUpdate(){
        
       $usersTable = TableRegistry::get('Users');
       $userDetails = $usersTable->find("all");
       foreach($userDetails as $key => $user):
           
           $firstName = "First Name - " . $key;
           $lastName =  "Last Name - " . $key;
           $emailDetails = "user" . $key . "@sesniel.com";
           
           $userUpdate = $usersTable->get($user->id);
           $userUpdate->first_name = $firstName;
           $userUpdate->last_name = $lastName;
           $userUpdate->email = $emailDetails;
           $usersTable->save($userUpdate);
           
           
       endforeach;
       
    }
}

