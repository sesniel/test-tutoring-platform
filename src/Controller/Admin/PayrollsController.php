<?php
namespace App\Controller;

namespace App\Controller\Admin;
use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Controller\Controller;
use Cake\Mailer\Email;

class PayrollsController extends AppController {
    
    public function index(){
        
        $this->checkLoginStatus();
        $dateRestrict = "";
        if ($this->request->is('post') || $this->request->is('put')) {
            $ld = explode('/',$this->request->data['date']);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            $dateRestrict = $new_date;
            $tutorsInfo = $this->getAllTutorDetails();
        }    
        $this->set(compact('tutorsInfo','dateRestrict'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function report(){
        
        $this->checkLoginStatus();
        $dateRestrict = "";
        if ($this->request->is('post') || $this->request->is('put')) {
            $ld = explode('/',$this->request->data['date']);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            $dateRestrict = $new_date;
            $tutorsInfo = $this->getAllTutorDetailsInfo();
        }    
//        $tutorsInfo = $this->getAllTutorDetailsInfo();
//        debug($tutorsInfo->toArray());exit;
        $this->set(compact('tutorsInfo','dateRestrict'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function view(){
        
        $this->checkLoginStatus();
        $tutorsInfo = $this->getAllTutorDetailsView();
        $this->set(compact('tutorsInfo'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    
    public function edit($id){

        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->get($id, ['contain' => ['Programs', 'Programs.Students', 'Programs.Tutors']]);
        $this->viewBuilder()->layout('admin-program');
        $this->set('lessonDetails',$lessonDetails);
//        debug($lessonDetails);exit;
        
    }
    
    public function updateLesson($lessonId = null){ 
                
        $lessonsTable = TableRegistry::get('Lessons');
        $lessonDetails = $lessonsTable->get($lessonId);
        $ld = explode('/',$this->request->data['lessonDate']);
        $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
        $lessonDetails->date = $new_date;
        $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime']));
        $lessonDetails->length = $this->request->data['length'];
        $lessonDetails->status = "Pending";
//        debug($lessonDetails);exit;
        $lessonsTable->save($lessonDetails);
            
        $this->redirect(['action' => 'index']);
        
    }
    
    public function ajaxSavePayrollDateLesson(){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        if($this->request->data['payroll_date'] != ""):
            $ld = explode('/',$this->request->data['payroll_date']);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            $info = $new_date;
        else:
            $info = "";
        endif;
        $lessonDetails = $LessonsTable->get($this->request->data['id']);
        $lessonDetails->payroll_date = $info;
        $LessonsTable->save($lessonDetails);
        $this->set(compact('info'));        
        $this->viewBuilder()->layout('ajax');
        
    }
    
    public function ajaxSavePayrollDateLead(){
        
        $LeadsTable  = TableRegistry::get('Leads');
        if($this->request->data['payroll_date'] != ""):
            $ld = explode('/',$this->request->data['payroll_date']);
            $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];
            $info = $new_date;
        else:
            $info = ""; 
        endif;
        $leadDetails = $LeadsTable->get($this->request->data['id']);
        $leadDetails->payroll_date = $info;
        $LeadsTable->save($leadDetails);
        $this->set(compact('info'));        
        $this->viewBuilder()->layout('ajax');
        
    }
    
    public function updatePayrollDate(){   
        
        $ConsultationsTable  = TableRegistry::get('Consultations');
        $LessonsTable  = TableRegistry::get('Lessons');
        $ld = explode('/',$this->request->data['date']);
        $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];   
        $new_date = date("Y-m-d", strtotime($new_date));
//        debug($this->request->data);
//        echo $this->request->data['date'];
//        echo $new_date;exit;
        if($new_date == "1970-01-01"):
            $new_date = "";
        endif;
        
//        echo $this->request->data['rate_id'][3916];
//        debug($this->request->data);exit;
        if(isset($this->request->data['consultation_id'])):
            foreach($this->request->data['consultation_id'] as $lead):
                $conDetails = $ConsultationsTable->get($lead);
                $conDetails->payroll_date = $new_date;
                $ConsultationsTable->save($conDetails);
            endforeach;
        endif;
        
        if(isset($this->request->data['lesson_id'])):
            foreach($this->request->data['lesson_id'] as $key => $lesson):
                $lessonDetails = $LessonsTable->get($key);
                $lessonDetails->payroll_date = $new_date;
                $lessonDetails->rate_id = $this->request->data['rate_id'][$key];
                $LessonsTable->save($lessonDetails);
            endforeach;
        endif;
        $this->redirect(['action' => 'index']);
    }
    
    private function getAllTutorDetails(){
        
        $TutorsTable = TableRegistry::get('Tutors');
            $leadsDetails = $TutorsTable->find('all', ['conditions' => [
//                'Tutors.id' => 711,
                'Tutors.type' => 'Tutor',
                'Tutors.status' => 'Active'
            ]])->contain([
                'Rates',
//                'LeadPayroll',
//                'LeadPayroll.Clients', 
//                'Programs.Students', 
                'Leads.Clients.Students', 
                'Leads.Consultations', 
                'Programs', 
                'Programs.Clients', 
                'Programs.Rates', 
                'Programs.PayrollLessons.Rates', 
                'Programs.PayrollLessons.LessonStudents',
                'Programs.PayrollLessons.LessonStudents.ProgramStudents.Students'])
                ->order(['Tutors.first_name']);
        return $leadsDetails;
    }
    
    private function getAllTutorDetailsInfo(){
        
        $TutorsTable = TableRegistry::get('Tutors');
            $leadsDetails = $TutorsTable->find('all', ['conditions' => [
                'Tutors.type' => 'Tutor',
                'Tutors.status' => 'Active'
            ]])->contain([
                'Rates',
                'LeadPayroll',
                'LeadPayroll.Clients', 
                'Leads.Clients.Students', 
                'Leads.Consultations', 
                'Programs', 
                'Programs.Clients', 
//                'Programs.Students', 
                'Programs.PayrollLessonsInfo.Rates', 
                'Programs.PayrollLessonsInfo.LessonStudents',
                'Programs.PayrollLessonsInfo.LessonStudents.ProgramStudents.Students'])
                ->order(['Tutors.first_name']);
        return $leadsDetails;
    }
    
    private function getAllTutorDetailsView(){
        
        $TutorsTable = TableRegistry::get('Tutors');
        $leadsDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.type' => 'Tutor'
        ]])->contain(['LeadPayrollAll', 'Leads.Clients', 'Programs', 'Programs.Students', 'Programs.PayrollLessons']);
        return $leadsDetails;
    }
    
}
