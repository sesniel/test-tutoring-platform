<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class DataReviewController extends AppController
{

    public function minorOutcome(){
        $this->checkLoginStatus();
        $minorOutcomeTbl = TableRegistry::get('MinorOutcomes');
        $minorOutcomes = $minorOutcomeTbl->find('all')->contain(['MajorOutcomes']);
        $this->set('minorOutcomes', $minorOutcomes);
    }

    public function editMinor(){
        $this->viewBuilder()->layout('blank');
        $minorOutcomeTbl = TableRegistry::get('MinorOutcomes');
        $minorOutcomes = $minorOutcomeTbl->get($this->request->data('id'), [
            'contain' => ['MajorOutcomes']
        ]);
        $this->set('minorOutcomes', $minorOutcomes);
    }

    public function updateMinorOutcome(){
       if ($this->request->is('post') || $this->request->is('put')) {
            $minOutTbl = TableRegistry::get('MinorOutcomes');
            $minorOutcome = $minOutTbl->get($this->request->data('min-id'));
            $minorOutcome->major_outcome_id = $this->request->data('min-majid');
            $minorOutcome->code = $this->request->data('min-code');
            $minorOutcome->description = $this->request->data('min-desc');
            if ($minOutTbl->save($minorOutcome)) {
               $this->updateMajorOutcome($this->request->data('min-majid'));
               return $this->redirect($this->referer() . "#data-updated");
            }
       }
    }

    private function updateMajorOutcome($majid = null){
        $majOutTbl = TableRegistry::get('MajorOutcomes');
        $majorOutcome = $majOutTbl->get($majid);
        $majorOutcome->category_id = $this->request->data('maj-catid');
        $majorOutcome->subject_id = $this->request->data('maj-subid');
        $majorOutcome->year_level_id = $this->request->data('maj-yrid');
        $majorOutcome->name = $this->request->data('maj-name');
        $majorOutcome->description = $this->request->data('maj-desc');
        $majOutTbl->save($majorOutcome);
    }
    
    public function updateconsultations01(){
        
        $ConsultationsTable = TableRegistry::get('Consultations'); 
        $consultationDetails = $ConsultationsTable->find('all', ['conditions' => [
            'Consultations.created <= ' => '2017-04-09 00:00:01',
            'Consultations.consultation_session_date <= ' => '2017-04-09 00:00:01',
            'Consultations.payroll_date' => ""
        ]]);
        foreach($consultationDetails as $consultation):
            
            $updateConsultation = $ConsultationsTable->get($consultation->id);
            $updateConsultation->payroll_date = "2017-03-17";
            $updateConsultation->status = "Completed Consultation";
            $ConsultationsTable->save($updateConsultation);
            
        endforeach;
        $consultationDetails = $ConsultationsTable->find('all', ['conditions' => [
            'Consultations.created <= ' => '2017-04-09 00:00:01',
            'Consultations.consultation_session_date <= ' => '2017-04-09 00:00:01',
            'Consultations.payroll_date' => ""
        ]]);
        debug($consultationDetails->toArray());exit;
        
    }
    
    public function updateconsultations02(){
        
        $ConsultationsTable = TableRegistry::get('Consultations'); 
        $consultationDetails = $ConsultationsTable->find('all', ['conditions' => [
            'Consultations.created <= ' => '2017-04-20 00:00:01',
            'Consultations.status' => 'New',
            'Consultations.payroll_date' => ""
        ]]);
        debug($consultationDetails->toArray());exit;
        foreach($consultationDetails as $consultation):
            
            $updateConsultation = $ConsultationsTable->get($consultation->id);
            $updateConsultation->payroll_date = "2017-04-01";
            $updateConsultation->status = "Completed Consultation";
            $ConsultationsTable->save($updateConsultation);
            
        endforeach;
                
    }

}