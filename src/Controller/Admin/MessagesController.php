<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class MessagesController extends AppController
{

    public function board($count = 10){
        
        $adminDetails = $this->checkLoginStatus();
        if ($this->request->is('post')):
            $count = $this->request->data('count');
        endif;
        $this->paginate = [
//            'conditions' => ['Messages.user_id' => $adminDetails->id],
            'limit' => $count,
            'contain' => ['Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }, 'MessageTags'],
            'order' => ['id' => 'desc']
        ];
        $messages = $this->paginate($this->Messages);
//        debug($messages);

        $this->set(compact('messages', 'count'));
        $this->set('_serialize', ['messages']);
        
    }
    
    public function create(){
        
        $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        $tutorDetails= $TutorsTable->find('all', ['conditions' => [
            'Tutors.status' => 'Active',
            'Tutors.type' => 'Tutor'
        ]])->order(['first_name'])->select(['id', 'first_name', 'last_name']);
        $this->set(compact('tutorDetails'));
        
    }
    
    public function view($id = null){
        
        $this->checkLoginStatus();
        $message = $this->Messages->get($id, [
            'contain' => ['Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }, 'MessageTags.Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'token']);
            }]
        ]);
//            debug($message);exit;
        $this->set('message', $message);
        $this->set('_serialize', ['message']);
    }
    
    public function saveMessage(){
//        debug($this->request->data);exit;
        $adminDetails = $this->checkLoginStatus();
        $TutorsTable = TableRegistry::get('Tutors');
        $MessagesTable = TableRegistry::get('Messages');
        $MessagesTagsTable = TableRegistry::get('MessageTags');   
        
        $messageDetails = $MessagesTable->newEntity();
        $question = $MessagesTable->patchEntity($messageDetails, $this->request->data);
        $question->user_id = $adminDetails->id;
        $savedMessage = $MessagesTable->save($question);
        $tutorDetails = $TutorsTable->find('all', ['conditions' => [
            'Tutors.type' => 'Tutor',
            'Tutors.status' => 'Active'
        ]]);
        foreach($tutorDetails as $tutor):

            $messageTagsDetails = $MessagesTagsTable->newEntity();
            $messageTagsDetails->message_id = $savedMessage->id;
            $messageTagsDetails->user_id = $tutor->id;
            $MessagesTagsTable->save($messageTagsDetails);

        endforeach;
        
        if($this->request->data('tutorInfo') && $this->request->data('tutor_email_all')):
            foreach($this->request->data('tutorInfo') as $tutor):
                $this->sendAnnouncement($savedMessage->id, $tutor);
            endforeach;
        elseif(!$this->request->data('tutorInfo') && $this->request->data('tutor_email_all')):
            foreach($tutorDetails as $tutor):
                $this->sendAnnouncement($savedMessage->id, $tutor->id);
            endforeach;
        endif;
        
//        debug($this->request->data);exit;
        return $this->redirect(['action' => 'board']);
        
    }
    
}
