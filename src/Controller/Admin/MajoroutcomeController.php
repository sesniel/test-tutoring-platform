<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class MajoroutcomeController extends AppController
{

    public function index(){
        
        $this->checkLoginStatus();
        $majoroutcomeTbl = TableRegistry::get('MajorOutcomes');
        $majoroutcome = $majoroutcomeTbl->find('all')->contain(['Categories', 'Subjects', 'YearLevels', 'MinorOutcomes']);
           
//      debug($majoroutcome->toArray());exit;
        $this->set(compact('majoroutcome'));
    }

    

}

