<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class ActiveCampaignsController extends AppController
{
    public $url = 'https://tutor2you.api-us1.com';
    public $apiId = '90c5519bb5a8b5ca412362468188cf26155bcabc430972cfa949bf485488ece36ecec7bc';
    
    public function index(){
                
        $UsersTable = TableRegistry::get('Users');
        $userDetails = $UsersTable->find('all', ['conditions' => [
            'Users.id >' => 1301,
            'Users.id <=' => 1500
        ]])->contain(['Students.YearLevels', 'States']);
//        echo count($userDetails->toArray());
//        debug($userDetails->toArray());exit;
        
        $x = 0;
        foreach($userDetails as $user): $x++;
            $states = (!empty($user->state)) ? $user->state->name : "";
            $contactDetails = $this->getContactEmail($user->email);
            $list = 14;//($user->type == "Tutor") ? 11 : 10;
            $grades = $this->calculateSubject($user);
            
//            $contactDetails = $this->getContactEmail('cath.tkatchenko@gmail.com');
//            debug($contactDetails);exit;
            
            if($contactDetails['result_code'] === 1):
                $type = "edit";
                $post = array(
                    'id'                       => $contactDetails['id'], 
                    'email'                    => $user->email,
                    'first_name'               => $user->first_name,
                    'last_name'                => $user->last_name,
                    'orgname'                  => 'Tutor2You',
                    'field[14,0]'              => $user->postcode,
                    'field[5,0]'               => $grades,
                    'field[16,0]'              => $states,
                    'field[18,0]'              => $user->type,
                    'field[19,0]'              => $user->status,
                    'phone'                    => $user->phone . " , " . $user->mobile,
                    'tags'                     => 'api,'.$user->type,
                    'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                );
            else:
                $type = "add";
                $post = array(
                    'email'                    => $user->email,
                    'first_name'               => $user->first_name,
                    'last_name'                => $user->last_name,
                    'orgname'                  => 'Tutor2You',
                    'field[14,0]'              => $user->postcode,
                    'field[5,0]'               => $grades,
                    'field[16,0]'              => $states,
                    'field[18,0]'              => $user->type,
                    'field[19,0]'              => $user->status,
                    'phone'                    => $user->phone . " , " . $user->mobile,
                    'tags'                     => 'api,'.$user->type,
                    'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                );
            endif;
            
            $this->crudContactList($post, $type);
            echo $user->id . " ==> " . $contactDetails['result_code'] . " -- " . $type . " -> " . $user->email . " ==> " . $grades;
            echo "<br/>";
            
            
        endforeach;
            
        
        echo count($userDetails->toArray()) . " == " . $x;
//            $contactDetails = $this->getContactDetails();
//            $contactDetails = $this->getContactEmail();
//            $contactDetails = $this->addContactInList();
        
//            $list = 11;
//            $post = array(
//                'email'                    => "3312j33os221@tutor2you.com.au",
//                'first_name'               => $userDetails->first_name,
//                'last_name'                => $userDetails->last_name,
//                'orgname'                  => 'Tutor2You',
//                'field[14,0]'              => $userDetails->postcode,
//                'field[15,0]'              => $this->calculateSubject($userDetails),
//                'field[16,0]'              => "States",
//                'field[18,0]'              => $userDetails->type,
//                'field[19,0]'              => $userDetails->status,
//                'phone'                    => $userDetails->phone . " , " . $userDetails->mobile,
//                'tags'                     => 'api,'.$userDetails->type,
//                'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
//                'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
//            );   
//            $this->crudContactList($post, "add");
//            $contactDetails = $this->getContactEmail('3312j33os221@tutor2you.com.au');
//            debug($contactDetails);
            exit;
        
        
        
        
        
//        $contactDetails = $this->getContactEmail();
        
    }
    
}
