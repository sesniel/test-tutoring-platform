<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class LeadsController extends AppController
{

    public function index($status = "Booked")
    {
        $this->checkLoginStatus();

        $statusSelect = ($status == "All") ? "%%" : $status;
        
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;

        $leadTbl = TableRegistry::get('Leads');
        $leads = $leadTbl->find('all', [
            'conditions' => ['Leads.status like' => $statusSelect]
        ])
            ->contain(['Tutors', 'Clients', 'Students.YearLevels', 'LeadSubjects.Subjects']);
        
        $this->set(compact('leads', 'status'));
    }

    public function leadView()
    {
        $this->viewBuilder()->layout('blank');
        $leadTbl = TableRegistry::get('Leads');
        $leadInfo = $leadTbl->get($this->request->data('id'), [
            'contain' => ['Clients', 'Students.YearLevels','Students.StudentSubjects.Subjects', 'LeadSubjects.Subjects','Tutors']
        ]);
        $subjetcsTbl = TableRegistry::get('Subjects');
        $regSubjects = $subjetcsTbl->find('all')->order('name');
        $this->set(compact('leadInfo', 'regSubjects'));
    }

    public function abandonLead()
    {
        $leadTable = TableRegistry::get('Leads');
        $lead = $leadTable->get($this->request->data['lead_id']);
        $lead->note = $this->request->data['reason'];
        $lead->status = "Abandoned";
        $leadTable->save($lead);
        $this->redirect($this->referer());
    }

    public function leadSubjUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) { 
            $leadID = $this->request->data('lead_id');
            $newSubjs = $this->request->data('subjects');
            $leadSubjTbl = TableRegistry::get('LeadSubjects');
            $subjects = $leadSubjTbl->find('all', [
                'conditions' => ['LeadSubjects.lead_id' => $leadID]
                ]);
            foreach ($subjects as $subject) {
                $delStdSubj = $leadSubjTbl->get($subject->id);
                $leadSubjTbl->delete($delStdSubj);
            }
            foreach ($newSubjs as $newSubsID) {
                $newSubject = $leadSubjTbl->newEntity();
                $newSubject->lead_id = $leadID;
                $newSubject->subject_id = $newSubsID;
                $leadSubjTbl->save($newSubject);
                $this->redirect($this->referer().'#subjects-updated');
            }
        }
    }
    
    public function processdb(){
        
        $leadTable = TableRegistry::get('Leads');
        $consultationTable = TableRegistry::get('Consultations');
        $leadDetails = $leadTable->find('all', ['conditions' => [
            'Leads.id > ' => 100
        ]])->limit(100);
//        debug($leadDetails->toArray());
        exit;
        $leadInfo = array();
        foreach($leadDetails as $leads):
            
            $leadInfo[$leads->client_id][$leads->tutor_id][] = $leads;
            
        endforeach;
        
        
        foreach($leadInfo as $clientInfo):
            

            foreach($clientInfo as $tutorInfo):
            
                $consultInfo = array();
                foreach($tutorInfo as $set):
            
                    if(empty($consultInfo)):
                        
                        $consultationInfo = $consultationTable->newEntity();

                        if($set->payroll_date != "" || $set->payroll_date != null):
                            $consultationInfo->payroll_date = $set->payroll_date;
                        endif;

                        if($set->consultation_session_date != "" || $set->consultation_session_date != null):
                            $consultationInfo->consultation_session_date = $set->consultation_session_date;
                        endif;

                        if($set->status == "Booked"):  
                            $consultationInfo->status = "Completed Consultation";   
                        else: 
                            $consultationInfo->status = $set->status;  
                        endif;

                        $consultInfo = $consultationTable->save($consultationInfo);

                    endif;
//                    echo $set->status;
                    $leadsInfo = $leadTable->get($set->id);
                    $leadsInfo->consultation_id = $consultInfo->id;
                    $leadTable->save($leadsInfo);
//                    echo $set->id;exit;
                
                endforeach;
                        
            endforeach;
            
        endforeach;
        
//        debug($leadInfo);
        
//        echo "sesniel";exit;
        
    }
    
    public function editSubjects(){
        $this->viewBuilder()->layout('blank');
        $leadTbl = TableRegistry::get('Leads');
        $leadInfo = $leadTbl->get($this->request->data('id'), [
            'contain' => ['Clients', 'Students.YearLevels','Students.StudentSubjects.Subjects', 'LeadSubjects.Subjects','Tutors']
        ]);
        $subjetcsTbl = TableRegistry::get('Subjects');
        $regSubjects = $subjetcsTbl->find('all')->order('name');
        $this->set(compact('leadInfo', 'regSubjects'));
    }
    
    public function changeStatus() {
        
        if ($this->request->is('post') || $this->request->is('put'))  { //debug($this->request->data);exit;
        $leadID = $this->request->data('lead_id'); 
        $leadsTbl = TableRegistry::get('Leads'); 
        $user = $leadsTbl->get($leadID);
        $user->status = $this->request->data('status');
         $leadsTbl->save($user);
//         debug($user); exit;
         return $this->redirect($this->referer() . "#status-change");
        }
    }

    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $client = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $client->id;
    }
}
