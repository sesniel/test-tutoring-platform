<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class AddClientController extends AppController
{

    public function index() 
    {
        $this->checkLoginStatus();
        $statesTbl = TableRegistry::get('States');
        $states = $statesTbl->find('all');
        $yrLvlsTbl = TableRegistry::get('YearLevels');
        $yrLvls = $yrLvlsTbl->find('all');
        $subjsTbl = TableRegistry::get('Subjects');
        $subjs = $subjsTbl->find('all', ['conditions' => ['Subjects.id >' => '4']]);
        $dataInfo = array();
        $check = "";
        if ($this->request->is('post') || $this->request->is('put')) { 
//            debug($this->request->data);exit;
            $dataInfo = $this->request->data();
            $check = $this->createClient();
        }

        $this->set(compact("check", 'states', 'yrLvls', 'subjs', 'dataInfo'));

    }

    private function getAToken()
    {

        $usersTbl = TableRegistry::get('Users');
        do {
            $token = md5(rand(0, 9999));

            $tokenQuery = $usersTbl->find("all", [
                "conditions" => [
                    "Users.token" => $token
                ]
            ])->first();

        } while ($tokenQuery);
        return $token;

    }

    private function generatePasswordString($length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }

    public function createClient()
    {
//        if ($this->request->is('post') || $this->request->is('put')) { 
            
            $usersTbl = TableRegistry::get('Users');

            $email = $usersTbl->find("all", [
                "conditions" => [
                    "Users.email" => $this->request->data("email")
                ]
            ])->first();

            if ($email) {
                
              
               //echo $this->Flash->error();
//               return $this->redirect(array('controller' => 'addClient', 'action' => 'index', 'value' => ));
              
//             return "Email exists";
              return $this->redirect($this->referer() . "#email-exists"); 

            } else {

                $token = $this->getAToken();
                $passwordGen = $this->generatePasswordString(7);

                $userData = $usersTbl->newEntity();
                $userData->type = "Client";
                $userData->first_name = $this->request->data['fname'];
                $userData->last_name = $this->request->data['lname'];
                $userData->email = $this->request->data['email'];
                $userData->phone = $this->request->data['phone'];
                $userData->mobile = $this->request->data['mobile'];
                $userData->unit_number = $this->request->data['unit-number'];
                $userData->street_number = $this->request->data['street-number'];
                $userData->street_name = $this->request->data['street-name'];
                $userData->suburb = $this->request->data['suburb'];
                $userData->postcode = $this->request->data['postcode'];
                $userData->city = $this->request->data['city'];
                $userData->state_id = $this->request->data['state'];
                $userData->status = "Active";
                $userData->password = crypt($passwordGen, 99);
                $userData->token = $token;
                $userData->email_notif = "Yes";
                $userDetails = $usersTbl->save($userData);
 
                if ($userDetails) {

                    $clientID = $userDetails->id;

                    // $availTbl = TableRegistry::get('Availabilities');
                    // $userAvailability = $this->request->data('availability');
                    // foreach ($userAvailability as $val) {
                    //     $availability = $availTbl->newEntity();
                    //     $availability->user_id = $clientID;
                    //     $availability->day_id = $val;
                    //     $availTbl->save($availability);
                    // }

                    $studentsTbl = TableRegistry::get('Students');
                    $stdSubjTbl = TableRegistry::get('StudentSubjects');

                    $students = $this->request->data('student');
                    $yearLevel = $this->request->data('year-level');

                    foreach ($students as $key => $student) {
                        $std = $studentsTbl->newEntity();
                        $std->client_id = $clientID;
                        $std->year_level_id = $yearLevel[$key];
                        $std->name = $student;
                        $studentDetail = $studentsTbl->save($std);

                        $stdSubject = $this->request->data(["subjects" . ($key + 1)]);

                        if ($studentDetail) {

                            foreach ($stdSubject as $subjects) {
                                $stdSubj = $stdSubjTbl->newEntity();
                                $stdSubj->student_id = $studentDetail->id;
                                $stdSubj->subject_id = $subjects;
                                $stdSubjTbl->save($stdSubj);
                            }
                        }

                    }                       
                    $this->emailNewClient($token,$passwordGen,$userData);
                    
    //                debug($userDetails);
                    $statesTbl = TableRegistry::get('States');
                    $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                        'Users.email' => $this->request->data['email'],
                    ]])->contain(['Students.YearLevels', 'States'])->first();
                    $list = 14;//($user->type == "Tutor") ? 11 : 10;
                    $list1 = 11;
                    $grades = $this->calculateSubject($userDetailsUp);
                    $type = "add";
                    $statesDetails = $statesTbl->get($userDetails->state_id);
                    $post = array(
                        'email'                    => $userDetailsUp->email,
                        'first_name'               => $userDetailsUp->first_name,
                        'last_name'                => $userDetailsUp->last_name,
                        'orgname'                  => 'Tutor2You',
                        'field[14,0]'              => $userDetailsUp->postcode,
                        'field[9,0]'               => $grades,
                        'field[8,0]'               => $statesDetails->name,
                        'field[18,0]'              => $userDetailsUp->type,
                        'field[19,0]'              => $userDetailsUp->status,
                        'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                        'tags'                     => 'api,'.$userDetailsUp->type,
                        'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'p['.$list1.']'             => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                    );
                    $this->crudContactList($post, $type);
                    
                    return $this->redirect(array('controller' => 'allocateLead', 'action' => 'created', $token));
                }

            }

//        }

    }


}
