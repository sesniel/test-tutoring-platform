<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class TutorsController extends AppController{

    public function index($status = "Active"){
        $statusSelect = ($status == "All") ? "%%" : $status;
        
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;
        $this->checkLoginStatus();
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.type' => 'Tutor',
                'Tutors.status like ' => $statusSelect
            ]
        ])->contain(['ModuleUsers', 'Rates'])
            ->order('Tutors.first_name');
//        debug($tutors->toArray());exit;
        $this->set(compact('tutors', 'status'));

    }
    
    public function activateTraining($token = null){

        $tutorsTable = TableRegistry::get('Tutors');
        $tutorDetails= $tutorsTable->findByToken($token)->first();
        $moduleInfo = $this->saveModuleUsers($tutorDetails->id, "New");
        $this->generateModuleUserQuestions($moduleInfo);   
        return $this->redirect($this->referer());
        
    }
    
    public function fullAccess(){
        
//        debug($this->request->data);exit;
        $tutorsTable = TableRegistry::get('Tutors');
        $tutorDetail = $tutorsTable->get($this->request->data['id']);
        $tutorDetail->access = $this->request->data['access'];
        $tutorsTable->save($tutorDetail);
        return $this->redirect($this->referer());

    }
    
    private function saveModuleUsers($id, $status, $type = "New"){
                
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        
        if($type == "New"):
            $moduleDetails = $moduleUsersTable->newEntity();
        else:
            $moduleDetails = $moduleUsersTable->get($type);
        endif;            
        
        $moduleDetails->user_id = $id;
        $moduleDetails->status = $status;
        $moduleDetails->position = 1;
        $moduleInfo = $moduleUsersTable->save($moduleDetails);
        
        return $moduleInfo;
        
    }
    
    private function generateModuleUserQuestions($moduleInfo){
        
        $moduleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        $moduleQuestionsTable = TableRegistry::get('ModuleQuestions');
        $moduleQuestionsDetails = $moduleQuestionsTable->find('all')->order(['ModuleQuestions.module_id' => 'asc']);
                
        foreach($moduleQuestionsDetails as $ques):
            
            $muqDetails = $moduleUserQuestionsTable->newEntity();
            $muqDetails->module_id      = $ques->module_id;
            $muqDetails->module_user_id = $moduleInfo->id;
            $muqDetails->type           = $ques->type;
            $muqDetails->questions      = $ques->questions;
            $muqDetails->choices        = $ques->choices;
            $muqDetails->answer         = $ques->answer;
            $muqDetails->image          = $ques->image;
            $moduleUserQuestionsTable->save($muqDetails);
            
        endforeach;
        
        
    }

    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $tutor = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $tutor->id;
    }

    private function emailCheck($email)
    {
        $usersTbl = TableRegistry::get('Users');

        $findEmail = $usersTbl->find("all", [
            "conditions" => [
                "Users.email" => $email
            ]
        ]);

        if (count($findEmail->toArray()) >= 2) {
            return 1; //email can't be used
        } else {
            return 0;
        }

    }

    public function changeStatus()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $userID = $this->getUserID($this->request->data('token'));
            $usersTbl = TableRegistry::get('Users');
            $user = $usersTbl->get($userID);
            $user->status = $this->request->data('status');
            $usersTbl->save($user);
//            debug($user->toArray()); exit;
            return $this->redirect($this->referer() . "#status-change");
        }
    }
    
    public function wwc() {
        
        $this->checkLoginStatus();
        $tutorsTbl = TableRegistry::get('Users');
        $tutorDetails = $tutorsTbl->find('all', ['conditions' => [
            'Users.type' => 'Tutor',
            'Users.status' => 'Active',
        ]])->select(['id', 'type', 'first_name', 'last_name', 'status', 'blue_card_number', 'blue_card_expiry', 'created', 'token']);
//        debug($tutorDetails->toArray());exit;
        $this->set(compact('tutorDetails'));
    }
    
    public function tutorOnboarding() {
        $this->checkLoginStatus();
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutorDetails = $tutorsTbl->find('all', ['conditions' => [
            'Tutors.status' => 'Active',
            'Tutors.type' => 'Tutor'
        ]])->contain(['ModuleUsers']);
        
//        debug($tutorDetails->toArray());exit;
        $this->set(compact('tutorDetails'));
    }
   
    
    public function tutorView($token = null, $idTutor = null, $status = "Active"){
        
//        debug($tutorId);exit;
        $tutorStatus = array('Active', 'Discontinue');
        $ratesTbl = TableRegistry::get('Rates');
        $userTable = TableRegistry::get('Tutors');
        $documentsTbl = TableRegistry::get('Documents');
        $statesTbl = TableRegistry::get('States');
        $adminSelect = $userTable->find('all', ['conditions' => [
            'Tutors.id in ' => [417,419,735] 
        ]])->select(['id', 'first_name', 'last_name'])->order(['first_name']);
        
        $states = $statesTbl->find('all');
        
        $statusTbl = TableRegistry::get('Status');
        $statusDetails = $statusTbl->find('list', ['keyField' => 'id', 'valueField' => 'name',
            'conditions' => ['Status.belong_to' => 'Training']]);     
        
//        debug($statusDetails->toArray());exit;
        $rateDetails = $ratesTbl->find('all');
        //$statusSelect = ($status == "All") ? "%%" : $status;
        
        $choices = array('All', 'Cancel', 'Done', 'In Progress', 'Pending', 'Tutor', 'Active');
        $this->checkLoginStatus();
        $tutorID = $this->getUserID($token);
        $documentDetails = $documentsTbl->find('all', ['conditions' => [
            'Documents.tutor_id' => $tutorID
        ]]);
//        debug($documentDetails->toArray());exit;
        if ($this->request->is(['patch', 'post', 'put'])):
            
            if(($this->request->data('status')) == 'All'):
                $status = $this->request->data('status'); 
                $choices = array('Cancelled', 'Completed', 'In Progress', 'Pending'); 
            elseif(($this->request->data('status')) == 'Active'):
                $status = $this->request->data('status'); 
                $choices = array('In Progress', 'Pending'); 
            else:
                $status = $this->request->data('status'); 
                $choices = array($this->request->data('status'));    
            endif;
            
        else:
            $choices = array('In Progress', 'Pending'); 
        endif;
        
        
        $tutorsTbl = TableRegistry::get('Tutors');
        $leadTbl = TableRegistry::get('Leads');
        $ProgramsTable = TableRegistry::get('Programs');
        
        $tutor = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.token' => $token,
                'Tutors.type in' => ['Applicant', 'Tutor'],
                'Tutors.status in ' => $tutorStatus,
           
            ]
        ])->contain(['ModuleUsers', 'ApplicationAnswers', 'TutorSubjects.Subjects']);
//        debug($tutor->toArray());exit;
        $tD = $tutor->toArray();
        $programDetails = $ProgramsTable->find('all', ['conditions' => [
            //'Programs.status like ' => $statusSelect
           'Programs.tutor_id' => $tutorID,
            'Programs.status in ' => $choices
        ]])->contain(['Tutors', 'Clients']);
//        debug($programDetails->toArray());exit;
        $tutorSubjectsTbl = TableRegistry::get('TutorSubjects');
        $subjectsTbl = TableRegistry::get('Subjects');
        $subjectsList = $subjectsTbl->find('all');
        $tutorSubjects = $tutorSubjectsTbl->find('all', ['conditions' => [
            'TutorSubjects.tutor_id' => $tutorID
        ]])->contain(['Subjects']);
//        debug($tutorSubjects->toArray()); exit;
        $leads = $leadTbl->find('all', [
            'conditions' => [
            'Leads.tutor_id' => $tutorID,
            'Leads.status !=' => 'Abandoned'
            ]
        ])->contain(['Clients', 'Students', 'Students.YearLevels', 'LeadSubjects', 'LeadSubjects.Subjects']);

        if(isset($this->request->data['editLeadSubjs'])):
            $leadInfo = $this->getLeadDetails($this->request->data['editLeadSubjs']);
            $subjetcsTbl = TableRegistry::get('Subjects');
            $regSubjects = $subjetcsTbl->find('all')->order('name');
            $this->set(compact('leadInfo', 'regSubjects'));
        endif;
        
//        debug($programDetails->toArray()); exit;
        $this->set(compact('tutor','leads', 'status', 'programDetails', 'availabilityStatus', 'hours', 'Bookingstatus', 'tutorStatus', 'rateDetails', 'adminSelect', 'statusDetails', 'documentDetails', 'tutorSubjects','subjectsList', 'states'));
    }
    
    public function editbookings($id = null){
        
        $this->checkLoginStatus();
        $ProjectsTable = TableRegistry::get('Programs');
        $RatesTable = TableRegistry::get('Rates');
        $programDetails= $ProjectsTable->find('all', ['conditions' => [
            'Programs.id' => $id
        ]])->contain(['Clients', 'Tutors', 'Rates'])->first();
        $rateDetails = $RatesTable->find('all');
//        debug($programDetails->toArray());exit;
        $this->set(compact('programDetails', 'rateDetails'));
       
    }
    
    public function updateProgram($id = null){
                
        $ProjectsTable = TableRegistry::get('Programs');
        $programDetails= $ProjectsTable->get($id);
        $programDetails->invoice_number = $this->request->data['number'];
        $programDetails->invoice_amount = $this->request->data['amount'];
        $programDetails->note = $this->request->data['note'];
        if(isset($this->request->data['rebook_status'])):
            $programDetails->rebook_status = $this->request->data['rebook_status'];
        endif;
        if($this->request->data['status'] == "Cancelled"):
            $programDetails->rebook_status = "Discontinued";
            $this->cancelLessons($id);
        endif;
        $programDetails->status = $this->request->data['status'];
        $programDetails->rate_id = $this->request->data['rate_id'];
        debug($this->request->data['rate_id']);exit;
        $ProjectsTable->save($programDetails);
//        debug($programDetails->toArray());exit;
        return $this->redirect(['action' => 'details', $id]);
        
    }
    
    public function updateView($token){
        $status = $this->request->data['status'];
        
        $this->redirect(['action' => 'tutorView', $token, $status]);
    }
    
    
    
    public function passwordReset(){
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $userID = $this->getUserID($this->request->data('token'));
            $passwordGen = $this->generatePasswordString(7);
            $usersTbl = TableRegistry::get('Users');
            $user = $usersTbl->get($userID);
            $user->password = crypt($passwordGen, 99);
            $userDetails = $usersTbl->save($user);
            if ($userDetails){
                $this->emailPasswordNotif($this->request->data('token'),$passwordGen);
                return $this->redirect($this->referer() . "#reset");
            }
        }
        
    }

    private function generatePasswordString($length){

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }

    public function tutorUpdate(){
        
//        debug($this->request->data());exit;
        if ($this->request->is('post') || $this->request->is('put')) {
//debug($this->request->data('availability'));exit;
            $email = $this->emailCheck($this->request->data('email'));
            if ($email) {
                return $this->redirect($this->referer() . "#email-exist");
            } else {                
                $usersTbl = TableRegistry::get('Users');
                $tutorSubjectsTbl = TableRegistry::get('TutorSubjects');
                $tutorID = $this->getUserID($this->request->data('token'));
                $tutor = $usersTbl->get($tutorID);

                if($tutor->availability != $this->request->data('availability') || $tutor->hours != $this->request->data('hours')):
                    $tutor->available_notification = date("Y-m-d", strtotime("30 days"));                                 
                endif;
                
                $this->changeTrainingStatus($tutorID, $this->request->data('training'));
                $tutor->first_name = $this->request->data('first_name');
                $tutor->last_name = $this->request->data('last_name');
                $tutor->email = $this->request->data('email');
                $tutor->mobile = $this->request->data('mobile');
                $tutor->phone = $this->request->data('phone');
                $tutor->suburb = $this->request->data('suburb');
                $tutor->rate_id = $this->request->data('rate_id');
                $tutor->owner_id = $this->request->data('owner_id');
                $tutor->street_number = $this->request->data('street_number');
                $tutor->street_name = $this->request->data('street_name');
                $tutor->biography = $this->request->data('biography');
                $tutor->status = $this->request->data('tutorStatus');
                $tutor->access = $this->request->data('access');
                $tutor->hours = $this->request->data('hours');
                $tutor->bank_details = $this->request->data('bank_details');
                $tutor->availability = $this->request->data('availability');
                $tutor->comments = $this->request->data('comments');
                
                if(!empty($this->request->data('subjects'))):
                $newSubjects = $this->request->data('subjects');
                $subjects = $tutorSubjectsTbl->find('all', ['conditions' => [
                    'TutorSubjects.tutor_id' => $tutorID
                ]]);
                foreach($subjects as $subject):
                    $deleteSubj = $tutorSubjectsTbl->get($subject->id);
                    $tutorSubjectsTbl->delete($deleteSubj);
                endforeach;

                foreach($newSubjects as $subject):
                    $newSubj = $tutorSubjectsTbl->newEntity();
                    $newSubj->tutor_id = $tutorID;
                    $newSubj->subject_id = $subject;
                    $tutorSubjectsTbl->save($newSubj);
                endforeach;
                endif;

                if(!empty($newSubjects)):
                    foreach($newSubjects as $subject):
                        $newSubj = $tutorSubjectsTbl->newEntity();
                        $newSubj->tutor_id = $tutorID;
                        $newSubj->subject_id = $subject;
                        $tutorSubjectsTbl->save($newSubj);
                    endforeach;
                endif;
                
                $tutor->blue_card_number = $this->request->data('blue_card_number');
                if(!empty($this->request->data('blue_card_expiry'))):
                   $tutor->blue_card_expiry = date('d/m/Y', strtotime($this->request->data('blue_card_expiry'))); 
                endif;
                $tutorDetailAddress = $usersTbl->save($tutor);
                $this->changeUserAddress($tutorDetailAddress->id);
                $this->checkBookingTutorStatus($tutorID,$this->request->data('rate_id'));
//                debug($tutor->toArray()); exit;
    //                debug($userDetails);
                $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                    'Users.email' => $this->request->data['email'],
                ]])->contain(['Students.YearLevels', 'States'])->first();
                $contactDetails = $this->getContactEmail($userDetailsUp->email);
                $list = 14;//($user->type == "Tutor") ? 11 : 10;
                $list1 = 11;
                $grades = $this->calculateSubject($userDetailsUp);
                $type = "edit";
                $post = array(
                    'id'                       => $contactDetails['id'], 
                    'email'                    => $userDetailsUp->email,
                    'first_name'               => $userDetailsUp->first_name,
                    'last_name'                => $userDetailsUp->last_name,
                    'orgname'                  => 'Tutor2You',
                    'field[14,0]'              => $userDetailsUp->postcode,
                    'field[9,0]'               => $grades,
                    'field[18,0]'              => $userDetailsUp->type,
                    'field[19,0]'              => $userDetailsUp->status,
                    'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                    'tags'                     => 'api,'.$userDetailsUp->type,
                    'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'p['.$list1.']'             => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                    'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                );
                $this->crudContactList($post, $type);

                return $this->redirect($this->referer() . "#contact-updated");
                  
            }
        }
     
    }
    
    public function upload () {
    
//        debug($this->request->data);exit;
        $tutorId = $this->request->data('tutor_id');
        $documentsTbl = TableRegistry::get('Documents');
        if(!empty($_FILES['hrdocs'])){
            $path = WWW_ROOT."web/hrdocs/";
            $filename = basename( $_FILES['hrdocs']['name']);
            $path = $path . $tutorId . basename( $_FILES['hrdocs']['name']);
                if(move_uploaded_file($_FILES['hrdocs']['tmp_name'], $path)) {
                    $documentDetails = $documentsTbl->newEntity();
                    $documentDetails->tutor_id = $tutorId;
                    $documentDetails->reference = $this->request->data("reference");
                    $documentDetails->filename = $tutorId . $filename;

                    $documentsTbl->save($documentDetails);
                    $this->redirect($this->referer() . "#uploaded");
                } else {
                $this->redirect($this->referer() . "#error");
                }
        } else {
            $this->redirect($this->referer() . '#empty');
        }
    }
    
    public function deleteDocument($documentId = null) {
        
        $documentTbl = TableRegistry::get('Documents');
        $entity = $documentTbl->get($documentId);
        $documentTbl->delete($entity);
        return $this->redirect($this->referer());
    }
    
    private function changeTrainingStatus($tutorId, $status){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->findByUserId($tutorId)->first();
        $moduleDetails->status = $status;
            
        if($status == "New"):
            
            $moduleDetails->position = 1;
            
        elseif($status=="Completed Orientation"):  
            
            $moduleDetails->position = 2;
            
        elseif($status=="Pending Contract Review & Expectations"):    
            
            $moduleDetails->position = 3;
            
        elseif($status=="Completed Contract Review & Expectations"):   
            
            $moduleDetails->position = 4; 
            
        elseif($status=="Pending Platform Training"):    
            
            $moduleDetails->position = 5;
            
        elseif($status=="Completed Platform Training"): 
            
            $moduleDetails->position = 6;   
            
        elseif($status=="Pending Tutor Training"):   
            
            $moduleDetails->position = 7; 
            
        elseif($status=="Completed Training"):   
            
            $moduleDetails->position = 10; 
            
        endif;
        
        $moduleUsersTable->save($moduleDetails);
//        debug($moduleDetails);exit;
        
    }
    
    private function checkBookingTutorStatus($tutorId = null, $rateId = null){
        
        $programTable = TableRegistry::get('Programs');
        $programDetails = $programTable->find("all", ['conditions' => [
            'Programs.tutor_id' => $tutorId,
            'Programs.rate_id < ' => $rateId,
            'Programs.status in' => array("Pending", "In Progress")
        ]]);
        foreach($programDetails as $program):
            $progUpdate = $programTable->get($program->id);
            $progUpdate->rate_id = $rateId;
            $programTable->save($progUpdate);
        endforeach;
//        debug($programDetails->toArray());exit;
        
    }

    private function getLeadDetails($lead_id){
        $leadTbl = TableRegistry::get('Leads');
        $leadInfo = $leadTbl->get($lead_id, [
            'contain' => ['Clients', 'Students.YearLevels','Students.StudentSubjects.Subjects', 'LeadSubjects.Subjects','Tutors']
        ]);
        return $leadInfo;
    }

    public function leadSubjUpdate()
    {
        if ($this->request->is('post') || $this->request->is('put')) { 
            $leadID = $this->request->data('lead_id');
            $newSubjs = $this->request->data('subjects');
            $leadSubjTbl = TableRegistry::get('LeadSubjects');
            $subjects = $leadSubjTbl->find('all', [
                'conditions' => ['LeadSubjects.lead_id' => $leadID]
                ]);
            foreach ($subjects as $subject) {
                $delStdSubj = $leadSubjTbl->get($subject->id);
                $leadSubjTbl->delete($delStdSubj);
            }
            foreach ($newSubjs as $newSubsID) {
                $newSubject = $leadSubjTbl->newEntity();
                $newSubject->lead_id = $leadID;
                $newSubject->subject_id = $newSubsID;
                $leadSubjTbl->save($newSubject);
                $this->redirect($this->referer().'#subjects-updated');
            }
        }
    }
 
    public function details($programId = null){
        $this->checkLoginStatus();
        $LessonsTable  = TableRegistry::get('Lessons');
        $ProgramsTable = TableRegistry::get('Programs');
        
        $programDetails = $ProgramsTable->get($programId, ['contain' => ['Tutors.Rates', 'Clients']]);
//       debug($programDetails->toArray()); exit;
        
        $lessonDetails  = $LessonsTable->findAllByProgramId($programId, ['order' => ['Lessons.date' => 'desc']])
                ->contain(['LessonResources', 'LessonResources.Resources', 'LessonStudents',
                            'LessonStudents.ProgramStudents',
                            'LessonStudents.ProgramStudents.Students',
                            'LessonStudents.MajorOutcomes',
                            'LessonStudents.MinorOutcomes',
                            'LessonStudents.AssessmentTypes',
                            'LessonStudents.LessonResources',
                            'Rates'
                    ])
                    ->order(['Lessons.Date' => 'asc']) ;
//          debug($lessonDetails->toArray()); exit;       
////        $programDetails = $ProgramsTable->get($programId, ['contain' => ['Tutors']]);
//                
//        debug($lessonDetails->toArray()); 
        $this->set(compact('programDetails', 'lessonDetails'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    public function deleteProgram($programId = null) {

        $ProgramsTable  = TableRegistry::get('Programs');        
        $entity = $ProgramsTable->get($programId);
        //2017-09-27
        //$ProgramsTable->delete($entity);
        return $this->redirect($this->referer());
        
    }
    public function edit($lessonId = null){
        
        $this->checkLoginStatus();
        $LessonsTable = TableRegistry::get('Lessons');
        $RatesTable = TableRegistry::get('Rates');
        $lessonDetails= $LessonsTable->get($lessonId, ['contain' => 
            ['Programs'=>function ($q) {
                return $q->select(['id','tutor_id']);
            }, 'Programs.Tutors'=>function ($q) {
                return $q->select(['id','rate_id']);
            }, 'Programs.Tutors.Rates','MajorOutcomes','MajorOutcomes.Subjects','MajorOutcomes.YearLevels', 
             'AssessmentTypes', 'MinorOutcomes', 'LessonResources',
             'LessonResources.Resources']]);
        $rateDetails = $RatesTable->find('all');
        $this->set(compact('lessonDetails','rateDetails'));
        $this->viewBuilder()->layout('admin-program');
        
    }
    public function updateLesson($lessonId = null){

        $LessonsTable = TableRegistry::get('Lessons');
        $lessonDetails= $LessonsTable->get($lessonId);        
        $ld = explode('/',$this->request->data['lessonDates']);
        $new_date = $ld[2].'-'.$ld[1].'-'.$ld[0];       
        if($this->request->data['payrolldate'] == ""):
            $lessonDetails->payroll_date = '';
        else:
            $pd = explode('/',$this->request->data['payrolldate']);
            $pay_date = $pd[2].'-'.$pd[1].'-'.$pd[0];     
            $lessonDetails->payroll_date = $pay_date;
        endif;    
        $lessonDetails->date = $new_date;
        $lessonDetails->time = date('H:i:s', strtotime($this->request->data['lessonTime']));
        $lessonDetails->length = $this->request->data['length'];
        $lessonDetails->status = $this->request->data['status'];
        $lessonDetails->rate_id = $this->request->data['rate_id'];
        $LessonsTable->save($lessonDetails);
        return $this->redirect(['action' => 'details', $lessonDetails->program_id]);
        
    }
    
    public function delete($lessonId = null){

        $LessonsTable  = TableRegistry::get('Lessons');        
        $entity = $LessonsTable->get($lessonId);
        //2017-09-27
        //$LessonsTable->delete($entity);
        return $this->redirect($this->referer());
        
    }
}
