<?php

namespace App\Controller\Admin;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use Cake\Mailer\Email;

class AppController extends Controller
{
    
    public $url = 'https://tutor2you.api-us1.com';
    public $apiId = '90c5519bb5a8b5ca412362468188cf26155bcabc430972cfa949bf485488ece36ecec7bc';
    
    public function crudContactList($post, $type="add"){

        $params = array(
            
            'api_key'      => $this->apiId,
            'api_action'   => 'contact_'.$type,
            'api_output'   => 'serialize',
            
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $url = rtrim($this->url, '/ ');

        if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            die('JSON not supported. (introduced in PHP 5.2.0)');
        }

        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $data); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object

        if ( !$response ) {
            die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }

        $result = unserialize($response);
        return $result;

    }
    
    public function getContactDetails($apiAction = 'contact_list'){        
        //user_list, 
        //original -> contact_list
        $params = array(

            'api_key'       => $this->apiId,
            'api_action'    => $apiAction,
            'api_output'    => 'serialize',
//            'ids'           => 'all',
            'filters[listid]' => 11,
            'filters[email]' => 'joshua@tutor2you.com.au',
//            'sort_direction'=> 'DESC',
//            'page' => $page,
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // clean up the url
        $url = rtrim($this->url, '/ ');
 
       // define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object
        
        $result = unserialize($response);
        
        return $result;

    }

    public function getContactEmail($email = 'admin@tutor2you.com.au'){
        

        $params = array(

            'api_key'      => $this->apiId,
            'api_action'   => 'contact_view_email',
            'api_output'   => 'serialize',
            'email'        => $email,
        );

        // This section takes the input fields and converts them to the proper format
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        // clean up the url
        $url = rtrim($this->url, '/ ');
 
       // define a final API request - GET
        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request); // execute curl fetch and store results in $response

        curl_close($request); // close curl object

        $result = unserialize($response);
        
        return $result;

    }
    
    public function checkName($name){
        
        if (substr($name, 0, 1) === '['):
            return "";
        else:    
            return $name;
        endif;
        
    }
    
    public function checkUserInfo($email){
        
        $UsersTable = TableRegistry::get('Users');
        $userDetails = $UsersTable->findByEmail($email)->first();
        if(!empty($userDetails)):
            return $userDetails->id;
        else:    
            return "";
        endif;
        
    }
    
    public function calculateSubject($user){
        
        $subject = "";
        $x = 1;
        foreach($user->students as $student):
            
            if(count($user->students) == $x):
                $subject .= $student->year_level->name;
            else:    
                $subject .= $student->year_level->name.",";
            endif;
            $x++;
            
        endforeach;
//        debug($user);
//        echo $subject;exit;
        return $subject;
        
    }
    
    public function checkLoginStatus()
    {

        $this->viewBuilder()->layout('admin');
        $type = ($this->request->session()->check("type") ? $this->request->session()->read("type") : 11);
        if ($type !== "Admin") {
            $this->redirect('/');
        } else {
            $usersTable = TableRegistry::get('Users');
            $userDetails = $usersTable->find('all', ['conditions' => ['Users.token' => $this->request->session()->read("token")]])
                ->select(['id', 'login_status', 'first_name', 'last_name', 'access', 'email', 'token'])
                ->first();
            return $userDetails;
        }

    }
    
    public function eventActiveTrigger($clientId, $event){
        
        $ClientsTable = TableRegistry::get('Clients');
        $tutorDetails = $ClientsTable->get($clientId);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://trackcmp.net/event");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
          "actid" => "223206443",
          "key" => "509e4058240273ba2f7ce71fb03cac93a1dfd382",
          "event" => $event,
          "eventdata" => $event,
          "visit" => json_encode(array(
            // If you have an email address, assign it here.
            "email" => $tutorDetails->email,
          )),
        ));

        $result = curl_exec($curl);
        if ($result !== false) {
          $result = json_decode($result);
          if ($result->success) {
//            echo 'Successfull! ';
          } else {
//            echo 'Error! ';
          }

//          echo $result->message;
        } else {
//          echo 'cURL failed to run: ', curl_error($curl);
        }
        
    }
    
    public function sendAnnouncement($messageId, $tutorId){
        
        $MessagesTable = TableRegistry::get('Messages');
        $TutorsTable = TableRegistry::get('Tutors');
        $messageDetails = $MessagesTable->find('all',['conditions' => [
            'Messages.id' => $messageId
        ]])->contain(['Users'=>function ($q) {
                return $q->select(['id','first_name', 'last_name','email']);
            }])->first();
        $tutorDetails = $TutorsTable->get($tutorId); 
//        debug($messageDetails->toArray());exit;
            
            $bodyEmail = "Hi ". $tutorDetails->first_name . " " . $tutorDetails->last_name . ", <br/><br/><b>" . $messageDetails->subject . "</b><br/>";
            $bodyEmail .= $messageDetails->message;
            
            
            $email = new Email();
            $email->emailFormat('html')
                    ->from([$messageDetails->user->email => 'Tutor2You'])
                ->to('admin@tutor2you.com.au')
//                ->to($tutorDetails->email)
                ->subject("Announcement from Tutor2You: " . $messageDetails->subject)
                ->send($bodyEmail);
//            exit;
            
        
    }
    
    public function changeDateDisplay($date2Change){
        $date = explode('/',$date2Change);
        $ndate = $date[2].'-'.$date[1].'-'.$date[0];    
        return $ndate;
    }

    private function emailInfo()
    {
        $sendInfo = array(
            'http://www.platform.tutor2you.com.au/', 'support@tutor2you.com.au',
            'If you have any questions regarding the platform please contact admin at rosette@tutor2you.com.au
                 and one of our staff will be happy to assist.',
            'Kind regards,<br>
                Tutor2You | Primary and Secondary | One-on-One Tutoring<br>
                Email: admin@tutor2you.com.au | Phone: 1300 4200 79',
            'joshua@tutor2you.com.au',
            'admin@tutor2you.com.au'
        );
        return $sendInfo;
    }

    private function getUserDetails($token){
        $UserTable = TableRegistry::get('Clients');
        $userDetails = $UserTable->findByToken($token)->first();
        return $userDetails;
    }
    
    public function changeUserAddress($id, $address = null){
        
        $usersTable  = TableRegistry::get('Users');
        $userDetails = $usersTable->get($id);
        
        $userUpdate = $usersTable->get($userDetails->id);
        if($address == null):
            $address = $userUpdate->street_name . " " . $userUpdate->suburb . " " .  $userUpdate->city;
        endif;
        
        $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";
        $xml = simplexml_load_file($request_url) or die("url not loading ID: " . $userUpdate->id . " ==> " . $address);
        $status = $xml->status;
        if ($status=="OK") {
            $Lat = $xml->result->geometry->location->lat;
            $Lon = $xml->result->geometry->location->lng;
        }
        $userUpdate->lng = $Lon;
        $userUpdate->lat = $Lat;
        $userUpdate->address = $address;
        $usersTable->save($userUpdate);

       
    }
    
    public function moduleEmail($tutorId){
        
        $emailInfo = $this->emailInfo();
        $tutorTable = TableRegistry::get('Tutors');
        $tutorDetail= $tutorTable->get($tutorId, ['contain' => [
            'ModuleUsers'
        ]]);
        $subject = 'Tutor2you Training Activated';
        
        if ($tutorDetail->module_user->position === 2) {
            $modules = '3 and 4';
            $contact = 'HR Manager';
        } elseif ($tutorDetail->module_user->position === 4) {
            $modules = '5 and 6';
            $contact = 'Sales Manager';
        } elseif ($tutorDetail->module_user->position === 6) {
            $modules = '7, 8, 9, and 10';
            $contact = 'Sales Manager';
        }
        
        $message = '
            Hi ' . $tutorDetail->first_name . ',
                
            You are now able to login to the platform to complete modules ' . $modules . '.
                
            Please <a href="http://www.platform.tutor2you.com.au/" target="_blank">login here</a> to complete the next round of 
            module training.
            
            Our ' . $contact . ' will be in touch with you after the submission of these modules.
                
            ' . $emailInfo[3] . '
            ';
            


        $email = new Email();
        $email->emailFormat('html')
                ->from([$emailInfo[5] => 'Tutor2You'])
//            ->to('deloso.sesniel@yahoo.com')
            ->to($tutorDetail->email)
            ->bcc($emailInfo[5])
            ->subject($subject)
            ->send($message);
            
    }
    
    public function employmentInformation($tutorId) {
        
        $emailInfo = $this->emailInfo();
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutor = $tutorsTbl->get($tutorId);
        
        $subject = 'Employment Information Request';
        $message = '

            Hi ' . $tutor->first_name .', <br><br>

            Thank you for taking the time to complete the recruitment process, and congratulations on obtaining a position with us at Tutor2You. In order for us to enter you into our payroll system we just require a couple more details. Please simply reply to this email completing the information below:<br><br>

            <b>General</b>
            DOB:<br>
            Bank Account Name:<br>
            BSB:<br>
            Account Number:<br><br>

            <b>Tax Information</b><br>
            What is your tax file number: <br>
            Are you an Australian Resident for Tax Purposes? <br>
            Do you want to claim Tax Free Threshold? <br>
            Do you have a HECS or HELP Debt?<br> 
            Are you claiming the Senior or Pensioners Tax Offset? <br>
            Do you have a Financial Supplement Debt?<br>
            Are you claiming any other Tax Offsets? <br><br>

            In the event that these details change in the future please email ' . $emailInfo[5] .' to request an update.<br><br>

            Once again congratulations and welcome to the team. <br><br>

            ' . $emailInfo[3] . '
            ';
        
            $tutor->email_bankdetails = date("Y-m-d H:i:s");
            $tutorsTbl->save($tutor);

            $email = new Email();
            $email->emailFormat('html')
                ->from([$emailInfo[5] => 'Tutor2You'])
                ->to($tutor->email)
                ->subject($subject)
                ->send($message);
        
    }
    
    public function acceptedNotification ($applicationID) {
        
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $applicantTbl = TableRegistry::get('Users');
        $applicantDetails = $applicantTbl->get($applicationID);
        
        $message = '
            
           Hi ' . $applicantDetails->first_name . ',
               
           Thank you for your application with us at Tutor2You. I am following up as we have an opening in your area and would like to progress you to the next stage of the application process if you\'re still interested.
           
           The application processes is made up of various stages including; 
           - Initial questionnaire (Next phase) 
           - Phone interview 
           - Contract review & orientation
           - Face to face interview 
           - Official offer (please note the current starting tutor pay rate is $27.50/hr plus super where applicable) 
           - Systems Training
           
           Please be advised you will need to be willing to obtain a Working With Children Check. 
            
           In addition to this email, you will also receive login details to our Tutoring Platform and further details regarding the next step.
           
           If you have any questions about the process please let me know. 
           
           ' . $emailInfo[3] . '
           
            ';
        

        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
        ->to($applicantDetails->email)
        ->replyTo($emailInfo[5])
        ->send($message);

    }
    
    public function reviewedNotification($applicationID) {
        
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $applicantTbl = TableRegistry::get('Users');
        $applicantDetails = $applicantTbl->get($applicationID);
        
        $message = '
            
           Hi ' . $applicantDetails->first_name . ',
               
           You recently applied for one of our tutoring positions. I can confirm I have reviewed your application and unfortunately, the position in your local area has already been filled. 
           However, we believe you could make a fantastic addition to our team so we will hold your details on file and will be in touch when a position becomes available in your area.
           
           Whilst we can\'t control client demand in specific areas, we do place dozens of new clients with tutors each and every week, so hopefully, we will be in touch again soon. 
            
           
           
           ' . $emailInfo[3] . '
           
            ';
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
        ->to($applicantDetails->email)
        ->replyTo($emailInfo[5])
        ->send($message);
            
    }
    
    public function rejectedNotification($applicationID) {
       
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $applicantTbl = TableRegistry::get('Users');
        $applicantDetails = $applicantTbl->get($applicationID);
        
        $message = '
            
           Hi ' . $applicantDetails->first_name . ',
               
           Thank you so much for taking the time to apply for a tutoring position with Tutor2you. 
           
           Unfortunately, we have recently filled our tutor quota for this term. However, we are regularly increasing our tutor intake as we grow.
           I would like to keep your details on file to allow me to contact you when a position opens up in your area. 
           
           However, we would not expect you to simply wait for a position to open up with us as such there are a couple of other tuition companies that you may wish to try applying for,
           these include; 
           
           -  Scooter Tutor (http://www.scootertutor.com.au/) 
           - Grace Simpkins (http://www.personaltutors.com.au/) 
           - Tutoring for Excellence (http://www.tutoringforexcellence.com.au/) 
           
           Best of luck with your search.
            
           
           ' . $emailInfo[3] . '
           
            ';
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
        ->to($applicantDetails->email)
        ->replyTo($emailInfo[5])
        ->send($message);
    }

    public function emailNewClient($token, $password)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $userDetails = $this->getUserDetails($token);


            $subject = 'Tutor2you Login Information';
            $message = '

                Hi ' . $userDetails->first_name . ',

                Your Tutor2you platform account has now been created. The platform provides 
                you access to your assessments, booking management, session reports, study programs and much more.

                You can login via the following URL ' . $emailInfo[0] . '

                Please note, your login details are as follows:
                Email: ' . $userDetails->email . '
                Password: ' . $password . '

                Your tutor will be in touch shortly to confirm your initial consultation. 
                Following this, you shall receive a notification once they have allocated your childs assessments.

                If you have any questions regarding the platform, or issues accessing any of your assessments 
                please reply to this email and one of our staff will be happy to assist.

                ' . $emailInfo[3] . '

                ';

           $email = new Email();
           $email->from([$emailInfo[5] => 'Tutor2You'])
               ->to($userDetails->email)
               ->bcc($emailInfo[5])
               ->subject($subject)
               ->send($message);
    }

    public function emailNewTutor($token, $password)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $userDetails = $this->getUserDetails($token);

            $subject = 'Tutoring Account';
            $message = '

                Hi ' . $userDetails->first_name . ',

                Please visit ' . $emailInfo[0] . ' to complete the modules training. 

                Your login details are as follows:
                Email: ' . $userDetails->email . '
                Password: ' . $password . '
                    
                If you have any questions regarding the platform please email us at ' . $emailInfo[5] . ' and one of our staff
                will be happy to assist.
                
                On completion of the first two modules, our HR Manager will be in touch with you to arrange a phone interview.

                ' . $emailInfo[3] . '

                ';
            
            

           $email = new Email();
           $email->from([$emailInfo[5] => 'Tutor2You'])
               ->to($userDetails->email)
               ->bcc($emailInfo[5])
//               ->to('tammy@tutor2you.com.au')
               ->subject($subject)
               ->send($message);

    }
    

    private function getLeadDetails($id)
    {
        $leadsTable = TableRegistry::get('Leads');
        $leadsDetails = $leadsTable->get($id, [
            'contain' => ['Clients', 'Students.YearLevels', 'LeadSubjects.Subjects', 'Tutors']
        ]);
        return $leadsDetails;
    }

    private function chopStudentSubjects($subjects)
    {
        $subj = array();
        foreach ($subjects as $val) {
            $subj[] = $val->subject->name;
        }
        return $subj;
    }

    public function emailNewAllocation($leadId)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $leadDetails = $this->getLeadDetails($leadId);
        $subjects = $this->chopStudentSubjects($leadDetails->lead_subjects);

        $subject = 'New Client';
        $message = '

                Hi ' . $leadDetails->tutor->first_name . ',

                A new client has been allocated to you on the tutoring platform.

                 Client Details:
                    -   Contact Name: ' . $leadDetails->client->first_name . ' ' . $leadDetails->client->last_name . '
                    -   Mobile: ' . $leadDetails->client->mobile . '
                    -   Phone: ' . $leadDetails->client->phone . '
                    -   Address: ' . $leadDetails->client->street_number . ' ' . $leadDetails->client->street_name . ', ' . $leadDetails->client->suburb . '

                 Student Details:
                    -   Student Name: ' . $leadDetails->student->name . '
                    -   Year: ' . $leadDetails->student->year_level->name . '
                    -   Subject/s: ' . implode(", ", $subjects) . '

                 Login to the tutoring platform here ' . $emailInfo[0] . '
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($leadDetails->tutor->email)
           ->subject($subject)
           ->send($message);
    }

    public function emailBookedAllocation($leadId)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $leadDetails = $this->getLeadDetails($leadId);
        $subjects = $this->chopStudentSubjects($leadDetails->lead_subjects);

        $subject = 'New Client';
        $message = '

                Hi ' . $leadDetails->tutor->first_name . ',

                A new client has been allocated to you on the tutoring platform.
                Please note this maybe an existing client so please review the account and view existing results.

                 Client Details:
                    -	Contact Name: ' . $leadDetails->client->first_name . ' ' . $leadDetails->client->last_name . '
                    -	Mobile: ' . $leadDetails->client->mobile . '
                    -	Phone: ' . $leadDetails->client->phone . '
                    -	Address: ' . $leadDetails->client->street_number . ' ' . $leadDetails->client->street_name . ', ' . $leadDetails->client->suburb . '

                 Student Details:
                    -	Student Name: ' . $leadDetails->student->name . '
                    -	Year: ' . $leadDetails->student->year_level->name . '
                    -	Subject/s: ' . implode(", ", $subjects) . '

                 Login to the tutoring platform here ' . $emailInfo[0] . '
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($leadDetails->tutor->email)
           ->subject($subject)
           ->send($message);
    }

    public function allocationFollowUp($leadId)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $leadDetails = $this->getLeadDetails($leadId);
        $subjects = $this->chopStudentSubjects($leadDetails->lead_subjects);

        $subject = 'New Client';
        $message = '

                Hi ' . $leadDetails->tutor->first_name . ',

                Our records show you were recently allocated a new client and it appears that assessments haven’t been allocated yet.

                 Client Details:
                    -	Contact Name: ' . $leadDetails->client->first_name . ' ' . $leadDetails->client->last_name . '
                    -	Mobile: ' . $leadDetails->client->mobile . '
                    -	Phone: ' . $leadDetails->client->phone . '
                    -	Address: ' . $leadDetails->client->street_number . ' ' . $leadDetails->client->street_name . ', ' . $leadDetails->client->suburb . '

                 Student Details:
                    -	Student Name: ' . $leadDetails->student->name . '
                    -	Year: ' . $leadDetails->student->year_level->name . '
                    -	Subject/s: ' . implode(", ", $subjects) . '

                 Login to the tutoring platform here ' . $emailInfo[0] . ' to view all your current clients 
                 and allocate the assessments once you have discussed your client’s needs during the initial consultation call. 
                 Prompt allocation will give you student the best opportunity to complete the assessments before your initial alignment lesson.
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to($leadDetails->tutor->email)
           ->subject($subject)
           ->send($message);
    }

    private function getAssmentDetails($id)
    {
        $stdAssmntTypeTbl = TableRegistry::get('StudentAssessmentTypes');
        $assmntDetails = $stdAssmntTypeTbl->get($id, [
            'contain' => ['Clients', 'Tutors', 'AssessmentTypes']
        ]);
        return $assmntDetails;
    }

    public function emailNewAssmntCreated($id)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $assmntDetails = $this->getAssmentDetails($id);

        $subject = 'New Assessment Allocated to ' . $assmntDetails->student->name;
        $message = '

                Hi ' . $assmntDetails->client->first_name . ',

                ' . $assmntDetails->tutor->first_name . ' has requested ' . $assmntDetails->student->name . ' to complete ' . $assmntDetails->assessment_type->type . ' 
                please arrange for ' . $assmntDetails->student->name . ' to complete this assessment at the soonest convenience.
                
                The results of these assessment will assist your tutor to personally tailor tutoring to ' . $assmntDetails->student->name . ' 
                individual needs.
               
                Please login here to complete the assessments ' . $emailInfo[0] . '
               
                Note: Your username is simply your email address which this email has been sent to. Your password would 
                have been emailed when you were added to the platform. If you cannot locate your password please feel free 
                to use the forgotten password function to generate a new one, this can always be changed via your profile 
                once you have logged in.
                    
                 ' . $emailInfo[2] . '

                 ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($assmntDetails->client->email)
           ->subject($subject)
           ->send($message);
    }

    public function emailPasswordNotif($token, $password)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $userDetails = $this->getUserDetails($token);
        $subject = 'Password Reset';
        $message = '

                Hi ' . $userDetails->first_name . ',

                You recently requested to reset your password for your account. 
                
                This is your login details and new password:
                Email: ' . $userDetails->email . '
                Password: ' . $password . '
                   
                You can login here ' . $emailInfo[0] . '

                ' . $emailInfo[2] . '

                ' . $emailInfo[3] . '

                ';

       $email = new Email();
       $email->from([$emailInfo[5] => 'Tutor2You'])
           ->to($userDetails->email)
           ->bcc($emailInfo[4])
           ->subject($subject)
           ->send($message);

    }

    private function getProgramDetails($programID){
        $programTable = TableRegistry::get('Programs');
        $programDetail = $programTable->get($programID, [
                'contain' => ['Clients','Tutors', 'Lessons']
            ]);
        return $programDetail;
    }
    
    public function emailCliengBooking($programID){
        
        $emailInfo = $this->emailInfo();
        $programDetails = $this->getProgramDetails($programID); 
        $lessonDisplay = "";
        if($programDetails->invoice_number != ""):
            $invoice = $programDetails->invoice_number;
        else:
            $invoice = "TBC";
        endif;
        
        if($programDetails->lessons):
            $lessonDisplay .= "<table>";
            $lessonDisplay .= "<tr>"
                    . "<td>Lesson Date&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>"
                    . "<td>Time&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>"
                    . "<td>Length&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>"
                    . "</tr>";
            
            foreach($programDetails->lessons as $lesson):
                $lessonDisplay .= "<tr>"
                        . "<td>".date("d/m/Y", strtotime($lesson->date))."</td>"
                        . "<td>".date("g:i A", strtotime(date("Y-m-d") . " " .$lesson->time))."</td>"
                        . "<td>".$lesson->length."</td>"
                        . "</tr>";
            endforeach;
            $lessonDisplay .= "</table>";
        endif;
        
        $message = " 
Hi ".$programDetails->client->first_name.",<br/><br/>

Please find below details of your new booking:<br/><br/>

Booking ID: ".$programDetails->id."<br/>
Invoice #: ".$invoice."<br/>
Tutor: ".$programDetails->tutor->first_name . " " . $programDetails->tutor->last_name ."<br/><br/>

$lessonDisplay
    
<br/><br/>

As your booking progresses, session reports will be emailed to you. If you'd like to change these settings, view your booking details or history 
<a href='http://platform.tutor2you.com.au'>Login Here</a> Please note that this is not a tax invoice, and this will be sent separately.
<br/><br/>
Kind regards,
<br/><br/>
Tutor2you
<br/>
www.tutor2you.com.au

                ";
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
                ->emailFormat('html')
            ->to($programDetails->client->email)
            ->bcc('deloso.sesniel@yahoo.com')
//            ->to('tammy@tutor2you.com.au')
            ->subject("New Booking Confirmation")
            ->send($message);
//        echo "<pre>".$message."</pre>";
//        debug($programDetails->toArray());exit;
        
    }

    public function emailLessonGenerated($programID)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $programDetails = $this->getProgramDetails($programID); //debug($programDetails);exit;
        $subject = 'New Booking';
        $message = '

                Hi ' . $programDetails->tutor->first_name . ',

                New Booking created!
                
                See details below:
                Client Name: ' . $programDetails->client->first_name . ' ' . $programDetails->client->last_name . '
                You can login here and view your bookings: ' . $emailInfo[0] . '/tutor/bookings/view/' . $programID . '

                ' . $emailInfo[2] . '

                ' . $emailInfo[3] . '

                ';
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to($programDetails->tutor->email)
            ->bcc('joshua@tutor2you.com.au')
//            ->to('tammy@tutor2you.com.au')
            ->subject($subject)
            ->send($message);


    }
    
    public function emailLessonGeneratedRebook($programID)
    {
        $this->checkLoginStatus();
        $emailInfo = $this->emailInfo();
        $programDetails = $this->getProgramDetails($programID); //debug($programDetails);exit;
        $subject = 'New Booking - ' . $programDetails->client->first_name . " " . $programDetails->client->last_name;
        $message = '

                Hi ' . $programDetails->tutor->first_name . ',

                Please be advised that ' . $programDetails->client->first_name . " " . $programDetails->client->last_name .  ' has rebooked for ' . count($programDetails->lessons) . ' lessons.
                
                Once you have completed any current active bookings with this client, you can confirm the pending booking via your "Bookings" tab in your platform.

                ' . $emailInfo[3] . '

                ';
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to($programDetails->tutor->email)
//            ->bcc('joshua@tutor2you.com.au')
//            ->to('tammy@tutor2you.com.au')
            ->subject($subject)
            ->send($message);


    }

    public function emailSessionNotification($tutorID, $message)
    {
        
        $emailInfo = $this->emailInfo();
        $TutorsTable = TableRegistry::get('Tutors');
        $tutorDetails = $TutorsTable->get($tutorID);
        $subject = $tutorDetails->first_name . ' Session Report Follow Up';
        $message2 = "Hi " . $tutorDetails->first_name . ",
            <br/><br/>
Please be advised that platform records indicate that there are one or more of your session reports that appear to be overdue. 
 <br/><br/>
Please review the list below, and either submit your report or reschedule the lesson in order to avoid further follow ups and to avoid any payroll delays. <br/>" . $message;
        
//        echo $message2;exit;
        $email = new Email();
        $email->emailFormat('html')
                ->from([$emailInfo[5] => 'Tutor2You'])
                ->to($tutorDetails->email)

//                ->to('joshua@tutor2you.com.au')
                ->cc('alex@tutor2you.com.au')
                ->bcc('joshua@tutor2you.com.au')
                ->subject($subject)
                ->send($message2);


    }

}
