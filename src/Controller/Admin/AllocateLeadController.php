<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class AllocateLeadController extends AppController
{

    public function index()
    {
        $this->checkLoginStatus();  
        $clientsTbl = TableRegistry::get('Clients');
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.status' => 'Active',
                'Clients.type' => 'Client',
            ]
        ])->order('Clients.first_name');

        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);

        $this->set(compact('tutors', 'clients'));

    }
    
    
    public function reallocateTutor()
    {
//        debug($this->request->data);exit;
        $leadId = $this->request->data('lead_id');  
        $clientId = $this->request->data('client_id');
        $studentId = $this->request->data('student_id');
//        debug($this->request->data);exit;
        $this->checkLoginStatus();  
        $leadsTbl = TableRegistry::get('Leads');
        $clientsTbl = TableRegistry::get('Clients');
        
        $leadDetails = $leadsTbl->get($leadId, ['contain' => [
            'Consultations', 'Clients', 'Tutors'
        ]]);
//       debug($leadDetails);exit;
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.id' => $clientId,
                'Clients.status' => 'Active',
                'Clients.type' => 'Client',
            ]
        ])->order('Clients.first_name')
          ->contain(['Leads.Students' => ['conditions' => [
              'Leads.id' => $leadId,
//              'Leads.status' => 'Pending'
              
              ]],'Students.StudentSubjects.Subjects', 'Students.Leads.LeadSubjects.Subjects', 'Leads.Tutors', 'Leads.Consultations']);
        $subjectsTbl = TableRegistry::get('Students');
        $subjects = $subjectsTbl->find('all', [
            'conditions' => ['Students.id' => $studentId]
        ])->contain(['Leads.LeadSubjects.Subjects', 'StudentSubjects.Subjects']);
        
//        debug($clients->toArray());exit;
        foreach ($clients as $client):
            $clientToken = $client->token;
            foreach ($client->leads as $lead):
                $tutorId = $lead->tutor_id;
                $leadId = $lead->id;
                $consultationId = $lead->consultation->id;
                
            endforeach;
            foreach ($client->students as $student):
                foreach ($student->leads as $leads):
                    if($leads->status === 'Booked'):
                        
                        $studentId = $student->id;
                        $studentName = $student->name;
                         //echo $studentId . " ";
                    endif;
                endforeach;
            endforeach;
        endforeach;
        
//        debug($clients->toArray());exit;
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);

        $this->set(compact('tutors', 'clients', 'clientToken', 'tutorId', 'leadId', 'consultationId', 'studentId', 'studentName', 'studentId', 'subjects', 'leadDetails'));

    }

    public function genStudent()
    {
        $this->viewBuilder()->layout('blank');
        if ($this->request->data('token')) {
            $token = '1';
            $clientsTbl = TableRegistry::get('Clients');
            $client = $clientsTbl->find('all', [
                'conditions' => ['Clients.token' => $this->request->data('token')]
            ])->contain(['Students'])->first();
            $this->set(compact('client', 'token'));
        }

    }
    

    public function genSubjects()
    {
        $this->viewBuilder()->layout('blank');
        if ($this->request->data('studentID')) {
            $token = '1';
            $stdSubjTbl = TableRegistry::get('StudentSubjects');
            $stdSubjs = $stdSubjTbl->find('all', [
                'conditions' => ['StudentSubjects.student_id' => $this->request->data('studentID')]
            ])->contain(['Subjects']);
            $this->set(compact('stdSubjs', 'token'));
        }

    }

    public function created($clientToken = null)
    {
        $this->checkLoginStatus();
        $clientsTbl = TableRegistry::get('Clients');
        $client = $clientsTbl->find('all', [
            'conditions' => ['Clients.token' => $clientToken]
            ])->contain('Students.StudentSubjects.Subjects');

        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
            'Tutors.status' => 'Active',
            'Tutors.type' => 'Tutor'
            ]
            ])->order(['Tutors.first_name']);
        $this->set(compact('client', 'tutors'));
    }

    public function student($studentID)
    {
        $this->checkLoginStatus();
        $studentTbl = TableRegistry::get('Students');
        $student = $studentTbl->find('all', [
            'conditions' => ['Students.id' => $studentID]
        ])->contain(['Clients.Students', 'StudentSubjects.Subjects']);
//        debug($student->toArray());exit;
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);
        
        $this->set(compact('student', 'tutors'));
    }
    
    public function allocateTutor($clientID) {
        
//        debug($this->request->data);exit;
        $this->checkLoginStatus();
        $studentTbl = TableRegistry::get('Students');
        $student = $studentTbl->find('all', [
            'conditions' => ['Students.client_id' => $clientID]
        ])->contain(['Clients.Students', 'StudentSubjects.Subjects']);
//        debug($student->toArray());exit;
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);
        
        $clientTbl = TableRegistry::get('Clients');
        $client = $clientTbl->find('all', ['conditions' => [
            'Clients.id' => $clientID
        ]])->contain(['Students.StudentSubjects.Subjects']);
//        debug($client->toArray());exit;
        $this->set(compact('student', 'tutors', 'client'));
    }

    private function getClientID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $client = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $client->id;
    }

    private function getTutorsID($tokens)
    {

        $usersTbl = TableRegistry::get('Users');
        $tutorIDs = array();

        foreach ($tokens as $val) {
            $tutors = $usersTbl->find("all", [
                "conditions" => [
                    "Users.token" => $val
                ]
            ])->first();
            $tutorIDs[] = $tutors->id;

        }
        return $tutorIDs;

    }

    public function saveLeadSubjects($key, $leadID)
    {
        $leadSubjTbl = TableRegistry::get('LeadSubjects');  
        foreach ($key as $subjs) {
            $leadSubj = $leadSubjTbl->newEntity();
            $leadSubj->lead_id = $leadID;
            $leadSubj->subject_id = $subjs;
            $leadSubjTbl->save($leadSubj);
//            debug($leadSubj);exit;
        }
    }
    
    
    
    public function saveNewLeadsReallocate(){
        
        if ($this->request->is('post') || $this->request->is('put')) {
//            debug($this->request->data);exit;
//            echo count($this->request->data["subjectRow1"]);exit;
            
//            debug($this->request->data["subjectRow1"]);exit;
//            debug($this->request->data["subjectRow2"]);exit;
//            echo $this->request->data["subjectRow1"][0];exit;
            
            $cDate = $this->changeDateDisplay($this->request->data('conDate'));
            $cTime = date("H:i:s", strtotime( $this->request->data('conTime')));
            $date = $cDate . " " . $cTime;
            
            $leadsTbl = TableRegistry::get('Leads');
            $usersTable = TableRegistry::get('Users');
            $consultationsTable = TableRegistry::get('Consultations');
            $clientDetails = $usersTable->findByToken($this->request->data("client_token"))->first();
            
            //Create Consultation
            $this->eventActiveTrigger($clientDetails->id, "New Booking Created");
            $consultationDetails = $consultationsTable->newEntity();
            $consultationDetails->consultation_session_date = $date;
            $consultationDetails->status = "New";
            $consultationDetailsUpdated = $consultationsTable->save($consultationDetails);
//            debug($consultationDetails);exit;
            
            
//            debug($clientDetails);exit;
            //Create Lead/s
            $x=0;
            foreach($this->request->data("tutors") as $key => $tutor):
                $x++;
                $tutorDetails = $usersTable->findByToken($tutor)->first();
                
                $studentId = $this->request->data["students"][$key];
                if ($this->validateLeadCombination($studentId, $tutorDetails->id)) {
                    return $this->redirect($this->referer() . "#lead-exist");
                } else {
                
                $leadDetails = $leadsTbl->newEntity();
                
                $leadDetails->consultation_id = $consultationDetailsUpdated->id;
                $leadDetails->tutor_id = $tutorDetails->id;
                $leadDetails->student_id = $studentId;
                $leadDetails->client_id = $clientDetails->id;
                $leadDetails->status = "Booked";
//                debug($leadDetails);exit;
                $leadDetailsUpdated = $leadsTbl->save($leadDetails);
                
            }
            endforeach;
            $this->saveLeadSubjects($this->request->data("subjectRow" . $x), $leadDetails->id);
            
            //Abandon Lead
            $oldLead = $leadsTbl->get($this->request->data("lead_id"));
            $oldLead->status = "Abandoned";
            $leadUpdated = $leadsTbl->save($oldLead);
            
            $oldConsultation = $consultationsTable->get($this->request->data("consultation_id"));
            if($oldConsultation->status != "Completed Consultation"):
                $oldConsultation->status = "Discontinued Consultation"; 
                $consultationUpdated = $consultationsTable->save($oldConsultation);
            endif;
            
            //debug($leadsDetails->toArray());exit;
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index#allocated', 'prefix' => "admin", '_full' => true));
            
        }
    }

    public function saveLeads(){
        
//       debug($this->request->data);exit;
        if ($this->request->is('post') || $this->request->is('put')) { 
            $x=0;
            $leadsTbl = TableRegistry::get('Leads');
            $clientID = $this->getClientID($this->request->data("client_token"));
            $tutorsID = $this->getTutorsID($this->request->data("tutors"));
            $tutorInfo = $this->request->data("tutors");
            $studentIDs = $this->request->data('students');
            $consultation = $this->processConsultation($this->request->data());
            
            $this->eventActiveTrigger($clientID, "New Allocation");
//            foreach ($tutorsID as $key => $tutID) {
////                $x++;
//                echo $studentIDs[$key];
//            }exit;
            
            foreach ($studentIDs as $key => $stdID) {
                $x++;
                
                if ($this->validateLeadCombination($stdID, $tutorsID[$key])) {
                    return $this->redirect($this->referer() . "#lead-exist");
                } else {
                    
//                    echo count($tutorsID);exit;
                    $lead = $leadsTbl->newEntity();
                    $lead->client_id = $clientID;
                    $lead->status = 'Booked';
                    $lead->tutor_id = $tutorsID[$key];
                    $lead->student_id = $stdID;
                    $lead->consultation_id = $consultation[$tutorInfo[$key]]->id;
                    $leadDetails = $leadsTbl->save($lead);
                    
                    
                    if (!empty($leadDetails)) { 
                        $this->saveLeadSubjects($this->request->data("subjectRow" . $x), $leadDetails->id);
                        ($this->request->data('skip_allocation'))? $this->emailBookedAllocation($leadDetails->id): $this->emailNewAllocation($leadDetails->id);
                    }
                }

            }

            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index#allocated', 'prefix' => "admin", '_full' => true));
        }
    }
    
    private function processConsultation($information){
        
        
        $consultationInfo = array();
       
        foreach($information['tutors'] as $key => $tutor):
            $consultationInfo[$tutor]['student'][$key]['id'] = $information['students'][$key];
            $consultationInfo[$tutor]['student'][$key]['subjects'] = $information['subjectRow'.($key+1)];
            
        endforeach;
        
        $cPass = $this->saveConsultation($consultationInfo);
       
        return $cPass;
        
    }
    
    private function saveConsultation($consultationInfo){
        
        
        $consultationsTable = TableRegistry::get('Consultations');
        $cPass = array();
        
        foreach($consultationInfo as $key => $tutorInfo):
            $consult = $consultationsTable->newEntity();
            $consult->token = $key;
            $cPass[$key] = $consultationsTable->save($consult);
        endforeach;
        
        return $cPass;
        
    }

    private function validateLeadCombination($student_id, $tutor_id){
        $leadTable = TableRegistry::get('Leads');
        $findLead = $leadTable->find("all", [
            "conditions" => [
            "Leads.student_id" => $student_id,
            "Leads.tutor_id" => $tutor_id
            ]])->first();

        if ($findLead) {
            return true; 
        } else {
            return false; 
        }
    }

}
