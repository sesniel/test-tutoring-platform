<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class StudentsController extends AppController
{

    public function index(){
        
        $this->checkLoginStatus();
        $studentsTbl = TableRegistry::get('Students');
        $students = $studentsTbl->find('all')->contain(['StudentSubjects.Subjects', 'Clients'])
            ->order('Students.name');
        $this->set(compact('students'));
    }

    

}