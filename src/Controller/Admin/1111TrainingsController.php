<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class TrainingsController extends AppController
{

    public function index($status = "My Active Applicants"){

       
       if ($this->request->is(['patch', 'post', 'put'])):
           if ($this->request->data('status')) {
               $this->redirect(['action' => 'index', $this->request->data['status']]);
           }
       endif;
        $this->checkLoginStatus();
        $statusTbl = TableRegistry::get('Status');
        $applicantsTbl = TableRegistry::get('Users');
        $applicantDetails = $applicantsTbl->find("all", ['conditions' => [
                'Users.type' => 'Applicant'
        ]])->contain(['Applications', 'ApplicationAnswers']);
        $statusDetails = $statusTbl->find('list', ['keyField' => 'id', 'valueField' => 'name',
            'conditions' => ['Status.belong_to' => 'Training']]);   

       $appStatus = ['New', 'Reviewed', 'Accepted', 'Abandoned'];
       
//       debug($applicantDetails->toArray()); exit;
       $info = $this->getTrainingStatus($status);
       $tutors = $this->getTutorTrainingDetails($status, $info);    
       $this->set(compact('tutors', 'info', 'status', 'statusDetails', 'applicantDetails', 'applicationAnswersDetails', 'applicationDetails', 'application', 'appStatus'));
    
    }
   
    public function applicantView($token = null) {
        $this->viewBuilder()->layout('admin-program');
        $applicantsTbl = TableRegistry::get('Users');
        $users = ['Tutor', 'Applicant'];
        $applicantDetails = $applicantsTbl->find('all', ['conditions' => [
                'Users.type in ' => $users,
                'Users.token' => $token
        ]])->contain(['Applications', 'ApplicationAnswers', 'TutorSubjects.Subjects']);
        
        foreach ($applicantDetails as $applicant):
            foreach ($applicant->tutor_subjects as $subjects):
                $applicantSubjects = $subjects->subject->name;
            endforeach;
        endforeach;
        
//        debug($applicantDetails->toArray()); exit;
        
      
        $this->set(compact('applicantDetails', 'applicationStatus', 'applicantSubjects'));
    }
    public function changeOwnerId($limit = 100){
        
        $usersTbl = TableRegistry::get('Users');
        $userDetails = $usersTbl->find("all", ['conditions' => [
            'Users.owner_id' => 0,
            'Users.type in ' => ['Client', 'Tutor']
        ]])->limit($limit);
        $count = 0;
        foreach($userDetails as $user):
            
            $upDate = $usersTbl->get($user->id);        
            if($user->type == "Client"):
                $upDate->owner_id = 419;                
            elseif($user->type == "Tutor"):    
                $upDate->owner_id = 417;
            endif;
            $usersTbl->save($upDate);
            $count++;
            
        endforeach;
        echo $count . " Successfully UPdated";
//        debug($userDetails->toArray());
        exit;
        
    }
    
    public function emailNotif(){
        
        echo date("Y-m-d H:i:s");exit;
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $emailInfo = $this->emailInfo();
        $info = array('New','Pending Contract Review & Expectations','Pending Platform Training');
        
        $moduleUsersDetail_3= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status in ' => $info,
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-3 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
            
            
        $subject = 'Tutor2you Training Followup';
            
        foreach($moduleUsersDetail_3 as $day3):
            
            $updateInfoMD = $moduleUsersTable->get($day3->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day3->tutor->first_name . " " . $day3->tutor->last_name . ',


                    You have a "' . $day3->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
            
        endforeach;
            
            
//        $moduleUsersDetail_10= $moduleUsersTable->find('all', ['conditions' => [
//            'ModuleUsers.status' => 'Pending Tutor Training',
//            'OR' => [
//                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-10 days")),
//                'ModuleUsers.email is' => null
//            ]
//        ]])->contain(['Tutors'=>function 
//            ($q) {
//                return $q->select(['id','first_name', 'last_name', 'email']);
//            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to('deloso.sesniel@yahoo.com')
//            ->to($tutorId->email)
//            ->bcc($emailInfo[5])
            ->subject($subject)
            ->send($message);
        $this->out('Hello world.');
        
        
        debug($moduleUsersDetail_3->toArray());exit;
//        $tutorTable = TableRegistry::get('Tutors');
//        $tutorDetail= $tutorTable->find('all');
//        $subject = 'Tutor2you Training Activated';
//        $message = ''
//                . 'Hi ' . $tutorDetail->first_name . " " . $tutorDetail->last_name . ", 
//                
//                Your tutor training has been activated.";

    }
   
    
    private function getTutorTrainingDetails($status, $accessTraining){
        
        $tutorsTbl = TableRegistry::get('Tutors');
        if($status == "My Active Applicants"):
            $tutors = $tutorsTbl->find('all', ['conditions' => [
                    'Tutors.type' => 'Tutor', 'Tutors.status !=' => 'Discontinue',
                    'Tutors.owner_id' => $this->request->session()->read("id"),
                    'ModuleUsers.status in' => $accessTraining
            ] ])->contain(['ModuleUsers', 'Owner'])->order('Tutors.first_name');
        else:
            $tutors = $tutorsTbl->find('all', ['conditions' => [
                    'Tutors.type' => 'Tutor', 'Tutors.status !=' => 'Discontinue',
                    'ModuleUsers.status in' => $accessTraining
            ] ])->contain(['ModuleUsers', 'Owner'])->order('Tutors.first_name');
        endif;
        return $tutors;
        
    }
   
    private function getTrainingStatus($status){
        
        switch($status):
            case "All": 
                $info = array('New','Completed Orientation','Pending Contract Review & Expectations','Completed Contract Review & Expectations','Pending Platform Training','Completed Platform Training','Pending Tutor Training','Completed Training');break; 
            case "New Applicants":
                $info = array('New'); break;              
            case "Active Applicants": 
                $info = array('New','Completed Orientation','Pending Contract Review & Expectations','Completed Contract Review & Expectations','Pending Platform Training','Completed Platform Training'); break;
            case "Completed Training": 
                $info = array('Completed Training'); break;  
            case "My Active Applicants": 
                $info = array('Completed Orientation','Completed Contract Review & Expectations','Completed Platform Training');break; 
            case "Applicants to be Finalized": 
                $info = array('New','Pending Contract Review & Expectations','Pending Platform Training','Pending Tutor Training');  break;   
        endswitch; 
        return $info;
        
    }

    public function activate($moduleId = null, $status = null){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->get($moduleId);
        $moduleDetails->status = $status;
        
        if($moduleDetails->position == 6):
            $usersTable = TableRegistry::get('Users');
            $userDetails = $usersTable->get($moduleDetails->user_id);
            $userDetails->access = 1;
            $usersTable->save($userDetails);
//            debug($moduleDetails);exit;
        endif;
        $this->moduleEmail($moduleDetails->user_id);
        
        $moduleDetails->position    += 1;
        $moduleDetails->email       = date("Y-m-d H:i:s");
        $moduleUsersTable->save($moduleDetails);
        $this->redirect($this->referer());  

    }
    
    public function general(){ 
        
        $this->checkLoginStatus();
        
    }
    
    public function studyskills(){ 
        
        $this->checkLoginStatus();
        
    }
    
    public function view($moduleUserQuestionId = null){ 
        
        $this->checkLoginStatus();
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.user_id' => $moduleUserQuestionId
        ]])
        ->contain(['ModuleUserQuestions','ModuleUserQuestions.Modules', 'Tutors'])
        ->first();        
        $this->set(compact('tutorInfo', 'moduleDetails'));
        
    }
    
    public function moduleUserEmailUpdate(){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.email is ' => null
        ]]);
        $count = 0;
        foreach($moduleDetails as $mod):
            $count++;
            $modUpdate = $moduleUsersTable->get($mod->id);
            $modUpdate->email = $mod->created;
            $moduleUsersTable->save($modUpdate);
        endforeach;
//        debug($moduleDetails->toArray());
        echo "$count record successfully updated!";
        exit;
        
    }
    private function emailCheck($email) {
        $usersTbl = TableRegistry::get('Users');

        $findEmail = $usersTbl->find("all", [
            "conditions" => [
                "Users.email" => $email
            ]
        ]);

        if (count($findEmail->toArray()) >= 2) {
            return 1; //email can't be used
        } else {
            return 0;
        }

    }
    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $applicant = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $applicant->id;
    }
  
    
  
    public function applicantUpdate() {
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->emailCheck($this->request->data('email'));
            if ($email) {
                return $this->redirect($this->referer() . "#email-exist");
            } else { 
                
                $usersTbl = TableRegistry::get('Users');
                $applicantID = $this->getUserID($this->request->data('token'));
                $applicant = $usersTbl->get($applicantID);
                
                
                $applicant->first_name = $this->request->data('first_name');
                $applicant->last_name = $this->request->data('last_name');
                $applicant->email = $this->request->data('email');
                $applicant->mobile = $this->request->data('mobile');
                $applicant->phone = $this->request->data('phone');
                $applicant->suburb = $this->request->data('suburb');
                $applicant->street_number = $this->request->data('street_number');
                $applicant->street_name = $this->request->data('street_name');
                $applicant->city = $this->request->data('city');
                $applicant->postcode = $this->request->data('postcode');
                $applicant->blue_card_number = $this->request->data('blue_card_number');
                $applicant->blue_card_expiry = $this->request->data('blue_card_expiry');
                
//                debug($applicant->toArray());exit;
               
                $applicationID = $applicant->id;
                
                $applicationsTbl = TableRegistry::get('Applications');
               $applications = $applicationsTbl->find('all', ['conditions' => [
                   'Applications.user_id' => $applicationID
               ]]);
               
               foreach ($applications as $application) {
                   $id = $application->id;
               }
              $applicationDetails = $applicationsTbl->get($id);
              
                if ($this->request->data('status') == 'Accepted') {
                    $applicant->type = "Tutor";
                    $this->redirect($this->referer() . '#application-accepted');
                } elseif ($this->request->data('status') == 'Reviewed' || $this->request->data('status') == 'New' || $this->request->data('status') == 'Abandoned') {
                    $applicant->type = "Applicant";
                    
                }
              
              $applicationDetails->status = $this->request->data('status');
              $applicationsTbl->save($applicationDetails);
               $usersTbl->save($applicant);
//              debug($applicationDetails); exit;
                return $this->redirect($this->referer() . "#applicant-updated");
            }
        }
    }
        
//    public function activateModule($moduleId = null){
//                
//        $this->checkLoginStatus();
//        $moduleUsersTable = TableRegistry::get('ModuleUsers');
//        $moduleDetails = $moduleUsersTable->get($moduleId);
//        if($moduleDetails->status == "Pending"):
//            $moduleDetails->status = 5;
//            $moduleUsersTable->save($moduleDetails);
//        endif;
//        return $this->redirect(array('controller' => 'trainings', 'prefix' => "admin", '_full' => true));
//        
//    }
    
//    public function bookingLesson(){
//        
//        $programsTable = TableRegistry::get('Programs');
//        $programDetails= $programsTable->find('all', ['contain' => ['Bookings']]);
//        $check = "Joshua-> <br/>";
//        foreach($programDetails as $program):
//            
//            if(empty($program['bookings']) && $program->status != "Done"):
//                $programInfo = $programsTable->get($program->id);
//                $programInfo->status = "Done";
//                $programsTable->save($programInfo);
//                $check .= "Sesniel -> <br/>";
////                echo $program->status;
////                debug($program);
//            endif;
//            
//        endforeach;
//        echo "Done <br/> $check";exit;
//        
//    }
    
  
}
 