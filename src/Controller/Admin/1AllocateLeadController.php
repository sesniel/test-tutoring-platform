<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;


class AllocateLeadController extends AppController
{

    public function index()
    {
        $this->checkLoginStatus();  
        $clientsTbl = TableRegistry::get('Clients');
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.status' => 'Active',
                'Clients.type' => 'Client',
            ]
        ])->order('Clients.first_name');

        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);

        $this->set(compact('tutors', 'clients'));

    }
    
    
    public function reallocateTutor()
    {
        $leadId = $this->request->data('lead_id');
        $clientId = $this->request->data('client_id');
//        echo $clientId;exit;
        $this->checkLoginStatus();  
        $clientsTbl = TableRegistry::get('Clients');
       
        $clients = $clientsTbl->find('all', [
            'conditions' => [
                'Clients.id' => $clientId,
                'Clients.status' => 'Active',
                'Clients.type' => 'Client',
            ]
        ])->order('Clients.first_name')
          ->contain(['Leads.Students' => ['conditions' => [
              'Leads.id' => $leadId
              
              ]], 'Students', 'Leads.Tutors', 'Leads.Consultations']);
        
        foreach ($clients as $client):
            $clientToken = $client->token;
            foreach ($client->leads as $lead):
                $tutorId = $lead->tutor_id;
                $leadId = $lead->id;
                $consultationId = $lead->consultation->id;
            endforeach;
        
        endforeach;
        
//        debug($clients->toArray());exit;
        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);

        $this->set(compact('tutors', 'clients', 'clientToken', 'tutorId', 'consultationId', 'leadId'));

    }

    public function genStudent()
    {
        $this->viewBuilder()->layout('blank');
        if ($this->request->data('token')) {
            $token = '1';
            $clientsTbl = TableRegistry::get('Clients');
            $client = $clientsTbl->find('all', [
                'conditions' => ['Clients.token' => $this->request->data('token')]
            ])->contain(['Students'])->first();
            $this->set(compact('client', 'token'));
        }

    }
    

    public function genSubjects()
    {
        $this->viewBuilder()->layout('blank');
        if ($this->request->data('studentID')) {
            $token = '1';
            $stdSubjTbl = TableRegistry::get('StudentSubjects');
            $stdSubjs = $stdSubjTbl->find('all', [
                'conditions' => ['StudentSubjects.student_id' => $this->request->data('studentID')]
            ])->contain(['Subjects']);
            $this->set(compact('stdSubjs', 'token'));
        }

    }

    public function created($clientToken = null)
    {
        $this->checkLoginStatus();
        $clientsTbl = TableRegistry::get('Clients');
        $client = $clientsTbl->find('all', [
            'conditions' => ['Clients.token' => $clientToken]
            ])->contain('Students.StudentSubjects.Subjects');

        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
            'Tutors.status' => 'Active',
            'Tutors.type' => 'Tutor'
            ]
            ])->order(['Tutors.first_name']);
        $this->set(compact('client', 'tutors'));
    }

    public function student($studentID)
    {
        $this->checkLoginStatus();
        $studentTbl = TableRegistry::get('Students');
        $student = $studentTbl->find('all', [
            'conditions' => ['Students.id' => $studentID]
        ])->contain(['Clients', 'StudentSubjects.Subjects']);

        $tutorsTbl = TableRegistry::get('Tutors');
        $tutors = $tutorsTbl->find('all', [
            'conditions' => [
                'Tutors.status' => 'Active',
                'Tutors.type' => 'Tutor'
            ]
        ])->order(['Tutors.first_name']);

        $this->set(compact('student', 'tutors'));
    }

    private function getClientID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $client = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $client->id;
    }

    private function getTutorsID($tokens)
    {

        $usersTbl = TableRegistry::get('Users');
        $tutorIDs = array();

        foreach ($tokens as $val) {
            $tutors = $usersTbl->find("all", [
                "conditions" => [
                    "Users.token" => $val
                ]
            ])->first();
            $tutorIDs[] = $tutors->id;

        }
        return $tutorIDs;

    }

    public function saveLeadSubjects($key, $leadID)
    {
        $leadSubjTbl = TableRegistry::get('LeadSubjects');  
        foreach ($key as $subjs) {
            $leadSubj = $leadSubjTbl->newEntity();
            $leadSubj->lead_id = $leadID;
            $leadSubj->subject_id = $subjs;
            $leadSubjTbl->save($leadSubj);
        }
    }
    
    public function saveNewLeadsReallocate111() {
        $test = $this->request->data('client_token');
        echo $test;exit;
    }
    
    public function saveNewLeadsReallocate(){
        
        if ($this->request->is('post') || $this->request->is('put')) { 
            $x=0;
            $leadsTbl = TableRegistry::get('Leads');
            $clientID = $this->getClientID($this->request->data("client_token"));
//            echo $clientID;exit;
            $tutorId = $this->request->data('tutor_id');
            $leadId = $this->request->data('lead_id');
            $consultationId = $this->request->data('consultation_id');
//            echo $tutorId;exit;
            $tutorsID = $this->getTutorsID($this->request->data("tutors"));
            
            $tutorInfo= $this->request->data("tutors");
            
            $studentIDs = $this->request->data('students');
            $consultation = $this->processConsultation($this->request->data());
//            debug($tutorsID);exit;
            foreach ($studentIDs as $key => $stdID) {
                $x++;
                if ($this->validateLeadCombination($stdID, $tutorsID[$key])) {
                    return $this->redirect($this->referer() . "#lead-exist");
                } else {
//                    echo 'lead doesnt exist';
                    $clientsTbl = TableRegistry::get('Clients');
                    $consultationsTbl = TableRegistry::get('Consultations');
//                    $clientsDetails = $clientsTbl->get($clientID, ['contain' => [
//                        'Leads.Tutors', 'Leads.Consultations'
//                    ]]);
                    
                    $consultationDetails = $consultationsTbl->get($consultationId);
                    $consultationDetails->status = "Discontinued Consultation";
                    $consultationsTbl->save($consultationDetails);
                    
                    $leadsDetails = $leadsTbl->get($leadId);
                    $leadsDetails->status = "Abandoned";
                    $leadsTbl->save($leadsDetails);
//                    debug($leadsDetails);exit;
                    $clientsDetails = $leadsTbl->newEntity();
                    $lead->client_id = $clientID;
                    $lead->status = 'Booked';
                    $lead->tutor_id = $tutorsID[$key];
//                    debug($lead->toArray());exit; 
                    $lead->student_id = $stdID;
                    $lead->consultation_id = $consultation[$tutorInfo[$key]]->id;
                    
                    $leadDetails = $leadsTbl->save($clientsDetails);
                    
                    foreach ($clientsDetails->leads as $lead):
                        if (($lead->tutor_id) == $tutorId):
//                             echo 'tutor id matched';
                             $lead->status = 'Abandoned';
                             $lead->consultation->status = 'Discontinued Consultation';
                       endif;
                        $clientsTbl->save($lead);
                    endforeach;
                    
//                   debug($clientsDetails->toArray());exit; 

                    if (!empty($leadDetails)) { 
                        $this->saveLeadSubjects($this->request->data("subjectRow" . $x), $leadDetails->id);
                        ($this->request->data('skip_allocation'))? $this->emailBookedAllocation($leadDetails->id): $this->emailNewAllocation($leadDetails->id);
                    }
                }

            }

            return $this->redirect(array('controller' => 'bookings', 'action' => 'consultations#allocated', 'prefix' => "admin", '_full' => true));
        }
    }

    public function saveLeads(){
        if ($this->request->is('post') || $this->request->is('put')) { 
            $x=0;
            $leadsTbl = TableRegistry::get('Leads');
            $clientID = $this->getClientID($this->request->data("client_token"));
            $tutorsID = $this->getTutorsID($this->request->data("tutors"));
            $tutorInfo= $this->request->data("tutors");
            $studentIDs = $this->request->data('students');
            $consultation = $this->processConsultation($this->request->data());
            foreach ($studentIDs as $key => $stdID) {
                $x++;
                if ($this->validateLeadCombination($stdID, $tutorsID[$key])) {
                    return $this->redirect($this->referer() . "#lead-exist");
                } else {
                    $lead = $leadsTbl->newEntity();
                    $lead->client_id = $clientID;
                    $lead->status = 'Booked';
                    $lead->tutor_id = $tutorsID[$key];
                    $lead->student_id = $stdID;
                    $lead->consultation_id = $consultation[$tutorInfo[$key]]->id;
                    $leadDetails = $leadsTbl->save($lead);

                    if (!empty($leadDetails)) { 
                        $this->saveLeadSubjects($this->request->data("subjectRow" . $x), $leadDetails->id);
                        ($this->request->data('skip_allocation'))? $this->emailBookedAllocation($leadDetails->id): $this->emailNewAllocation($leadDetails->id);
                    }
                }

            }

            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index#allocated', 'prefix' => "admin", '_full' => true));
        }
    }
    
    private function processConsultation($information){
        
        $consultationInfo = array();
        
        foreach($information['tutors'] as $key => $tutor):
            $consultationInfo[$tutor]['student'][$key]['id'] = $information['students'][$key];
            $consultationInfo[$tutor]['student'][$key]['subjects'] = $information['subjectRow'.($key+1)];
        endforeach;
        
        $cPass = $this->saveConsultation($consultationInfo);
        
        return $cPass;
        
    }
    
    private function saveConsultation($consultationInfo){
        
        $consultationsTable = TableRegistry::get('Consultations');
        $cPass = array();
        foreach($consultationInfo as $key => $tutorInfo):
            $consult = $consultationsTable->newEntity();
            $consult->token = $key;
            $cPass[$key] = $consultationsTable->save($consult);
        endforeach;
        
        return $cPass;
        
    }

    private function validateLeadCombination($student_id, $tutor_id){
        $leadTable = TableRegistry::get('Leads');
        $findLead = $leadTable->find("all", [
            "conditions" => [
            "Leads.student_id" => $student_id,
            "Leads.tutor_id" => $tutor_id
            ]])->first();

        if ($findLead) {
            return true; 
        } else {
            return false; 
        }
    }

}
