<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class ResourcesController extends AppController
{

    public function index(){
        $this->checkLoginStatus();

        $resourcesTbl= TableRegistry::get('Resources');
        $resources = $resourcesTbl->find('all')
            ->contain(['MinorOutcomes.MajorOutcomes.YearLevels','MinorOutcomes.MajorOutcomes.Subjects','MinorOutcomes.MajorOutcomes.Categories']);

        $this->set('resources',$resources);
//      debug($resources->toArray()); exit;
    }

    public function yearCumRes(){
        $this->checkLoginStatus();        
    }


}
