<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class TrainingsController extends AppController
{

    public function index($status = "My Active Applicants", $applicationStatus = 'New'){
        
        
        $this->checkLoginStatus();
        $statusTbl = TableRegistry::get('Status');
        $applicantsTbl = TableRegistry::get('Users');
        $applicationsTbl = TableRegistry::get('Applications');
        $appStat = ['New', 'Reviewed', 'Accepted', 'Abandoned'];
        if ($this->request->is(['patch', 'post', 'put'])){
            
           if ($this->request->data('status')) {
               $this->redirect(['action' => 'index', $this->request->data['status']]);
           } elseif ($this->request->data('applicationStatus'))
               if ($this->request->data('applicationStatus') == 'Accepted') {
                   $applicationStatus = $this->request->data('applicationStatus');
                   $appStat = ['Accepted'];
                   
               } elseif ($this->request->data('applicationStatus') == 'Reviewed') {
                   $applicationStatus = $this->request->data('applicationStatus');
                   $appStat = ['Reviewed'];
               } elseif (($this->request->data('applicationStatus') == 'Abandoned')) {
                   $applicationStatus = $this->request->data('applicationStatus');
                   $appStat = 'Abandoned';
               } else {
                   $appStat = ['New'];
               }
               
        } else {
            $appStat = ['New'];
        }
        
        
        $users = ['Applicant', 'Tutor'];
        $applicantDetails = $applicantsTbl->find("all", ['conditions' => [
                'Users.type' => 'Applicant'
           
        ]])->contain(['Applications', 'ApplicationAnswers']);
             
       $applicationDetails = $applicationsTbl->find('all', ['conditions' => [
           'Applications.status in' => $appStat
       ]])->contain(['ApplicationAnswers', 'Users' => ['conditions' => [
                   'Users.type in' => $users
               ]]]);
       
        $appID = array();
        foreach ($applicantDetails as $applicants):
           
            $appID[] = $applicants->id;
            $appId = implode(', ', $appID);
        
        endforeach;
        
       $statusDetails = $statusTbl->find('list', ['keyField' => 'id', 'valueField' => 'name',
            'conditions' => ['Status.belong_to' => 'Training']]);   
       
       $info = $this->getTrainingStatus($status);
       $tutors = $this->getTutorTrainingDetails($status, $info);    
       $this->set(compact('tutors', 'info', 'status', 'statusDetails', 'applicantDetails', 'applicationAnswersDetails', 'applicationDetails', 'application', 'applicationStatus', 'details', 'appId'));
    
    }
    
    public function applicantView($token = null) {
        $this->viewBuilder()->layout('admin-program');
        $applicantsTbl = TableRegistry::get('Users');
        $userTable = TableRegistry::get('Tutors');
        $statusTbl = TableRegistry::get('Status');
        $users = ['Tutor', 'Applicant'];
        $applicantDetails = $applicantsTbl->find('all', ['conditions' => [
                'Users.type in' => $users,
                'Users.token' => $token
        ]])->contain(['ModuleUsers', 'Applications', 'ApplicationAnswers', 'TutorSubjects.Subjects']);
//       debug($applicantDetails->toArray());exit;
        $adminSelect = $userTable->find('all', ['conditions' => [
            'Tutors.id in ' => [417,419,735] 
        ]])->select(['id', 'first_name', 'last_name'])->order(['first_name']);
        $statusDetails = $statusTbl->find('list', ['keyField' => 'id', 'valueField' => 'name',
            'conditions' => ['Status.belong_to' => 'Training']]); 
//        debug($statusDetails->toArray());exit;
        $this->set(compact('applicantDetails', 'applicationStatus', 'adminSelect', 'statusDetails'));
    }
    public function changeOwnerId($limit = 100){
        
        $usersTbl = TableRegistry::get('Users');
        $userDetails = $usersTbl->find("all", ['conditions' => [
            'Users.owner_id' => 0,
            'Users.type in ' => ['Client', 'Tutor']
        ]])->limit($limit);
        $count = 0;
        foreach($userDetails as $user):
            
            $upDate = $usersTbl->get($user->id);        
            if($user->type == "Client"):
                $upDate->owner_id = 419;                
            elseif($user->type == "Tutor"):    
                $upDate->owner_id = 417;
            endif;
            $usersTbl->save($upDate);
            $count++;
            
        endforeach;
        echo $count . " Successfully UPdated";
//        debug($userDetails->toArray());
        exit;
        
    }
    
    public function emailNotif(){
        
        echo date("Y-m-d H:i:s");exit;
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $emailInfo = $this->emailInfo();
        $info = array('New','Pending Contract Review & Expectations','Pending Platform Training');
        
        $moduleUsersDetail_3= $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.status in ' => $info,
            'OR' => [
                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-3 days")),
                'ModuleUsers.email is' => null
            ]
        ]])->contain(['Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name', 'email']);
            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
            
            
        $subject = 'Tutor2you Training Followup';
            
        foreach($moduleUsersDetail_3 as $day3):
            
            $updateInfoMD = $moduleUsersTable->get($day3->id);
            $updateInfoMD->email = date('Y-m-d H:i:s');
            $moduleUsersTable->save($updateInfoMD);
            $message = 'Hi ' . $day3->tutor->first_name . " " . $day3->tutor->last_name . ',


                    You have a "' . $day3->status . '", please revisit tutor2you platform and answer the training questions.'
                    . '<br/>'
                    . 'Regards,'
                    . '<br/>-Admin';
            
        endforeach;
            
            
//        $moduleUsersDetail_10= $moduleUsersTable->find('all', ['conditions' => [
//            'ModuleUsers.status' => 'Pending Tutor Training',
//            'OR' => [
//                'ModuleUsers.email <= ' => date('Y-m-d H:i:s', strtotime("-10 days")),
//                'ModuleUsers.email is' => null
//            ]
//        ]])->contain(['Tutors'=>function 
//            ($q) {
//                return $q->select(['id','first_name', 'last_name', 'email']);
//            }])->select(['id', 'user_id', 'status', 'email', 'modified']);
        
        $email = new Email();
        $email->from([$emailInfo[5] => 'Tutor2You'])
            ->to('deloso.sesniel@yahoo.com')
//            ->to($tutorId->email)
//            ->bcc($emailInfo[5])
            ->subject($subject)
            ->send($message);
        $this->out('Hello world.');
        
        
        debug($moduleUsersDetail_3->toArray());exit;
//        $tutorTable = TableRegistry::get('Tutors');
//        $tutorDetail= $tutorTable->find('all');
//        $subject = 'Tutor2you Training Activated';
//        $message = ''
//                . 'Hi ' . $tutorDetail->first_name . " " . $tutorDetail->last_name . ", 
//                
//                Your tutor training has been activated.";

    }
   
    
    private function getTutorTrainingDetails($status, $accessTraining){
        
        $tutorsTbl = TableRegistry::get('Tutors');
        if($status == "My Active Applicants"):
            $tutors = $tutorsTbl->find('all', ['conditions' => [
                    'Tutors.type in' => ['Tutor','Applicant'], 
                    'Tutors.status !=' => 'Discontinue',
                    'Tutors.owner_id' => $this->request->session()->read("id"),
                    'ModuleUsers.status in' => $accessTraining
            ] ])->contain(['ModuleUsers', 'Owner'])->order('Tutors.first_name');
        else:
            $tutors = $tutorsTbl->find('all', ['conditions' => [
                    'Tutors.type in' => ['Tutor','Applicant'], 
                    'Tutors.status !=' => 'Discontinue',
                    'ModuleUsers.status in' => $accessTraining
            ] ])->contain(['ModuleUsers', 'Owner'])->order('Tutors.first_name');
        endif;
        return $tutors;
        
    }
   
    private function getTrainingStatus($status){
        
        switch($status):
            case "All": 
                $info = array('New','Completed Orientation','Pending Contract Review & Expectations','Completed Contract Review & Expectations','Pending Platform Training','Completed Platform Training','Pending Tutor Training','Completed Training');break; 
            case "New Applicants":
                $info = array('New'); break;              
            case "Active Applicants": 
                $info = array('New','Completed Orientation','Pending Contract Review & Expectations','Completed Contract Review & Expectations','Pending Platform Training','Completed Platform Training'); break;
            case "Completed Training": 
                $info = array('Completed Training'); break;  
            case "My Active Applicants": 
                $info = array('New','Pending Contract Review & Expectations','Pending Platform Training','Completed Orientation','Completed Contract Review & Expectations','Completed Platform Training');break; 
            case "Applicants to be Finalized": 
                $info = array('New','Pending Contract Review & Expectations','Pending Platform Training','Pending Tutor Training');  break;   
        endswitch; 
        return $info;
        
    }

    public function activate($moduleId = null, $status = null){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->get($moduleId);
        $moduleDetails->status = $status;
        
        if($moduleDetails->position == 6):
            $usersTable = TableRegistry::get('Users');
            $userDetails = $usersTable->get($moduleDetails->user_id);
            $userDetails->access = 1;
            $userDetails->type = "Tutor";
            $usersTable->save($userDetails);
//            debug($moduleDetails);exit;
        endif;
        $this->moduleEmail($moduleDetails->user_id);
        
        $moduleDetails->position    += 1;
        $moduleDetails->email       = date("Y-m-d H:i:s");
        $moduleUsersTable->save($moduleDetails);
        $this->redirect($this->referer());  

    }
    
    public function general(){ 
        
        $this->checkLoginStatus();
        
    }
    
    public function studyskills(){ 
        
        $this->checkLoginStatus();
        
    }
    
    public function generatePasswordString($length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
    
    public function view($moduleUserQuestionId = null){ 
        
        $this->checkLoginStatus();
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.user_id' => $moduleUserQuestionId
        ]])
        ->contain(['ModuleUserQuestions.Modules', 'Tutors'])
        ->first();       
        $this->set(compact('tutorInfo', 'moduleDetails'));
        
    }
    
    public function moduleUserEmailUpdate(){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->find('all', ['conditions' => [
            'ModuleUsers.email is ' => null
        ]]);
        $count = 0;
        foreach($moduleDetails as $mod):
            $count++;
            $modUpdate = $moduleUsersTable->get($mod->id);
            $modUpdate->email = $mod->created;
            $moduleUsersTable->save($modUpdate);
        endforeach;
//        debug($moduleDetails->toArray());
        echo "$count record successfully updated!";
        exit;
        
    }
    private function emailCheck($email) {
        $usersTbl = TableRegistry::get('Users');

        $findEmail = $usersTbl->find("all", [
            "conditions" => [
                "Users.email" => $email
            ]
        ]);

        if (count($findEmail->toArray()) >= 2) {
            return 1; //email can't be used
        } else {
            return 0;
        }

    }
    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $applicant = $usersTbl->find("all", [
            "conditions" => [
                "Users.token" => $token
            ]
        ])->first();

        return $applicant->id;
    }
  
    private function getApplicantID ($appID) {
        $usersTbl = TableRegistry::get('Users');
        $applicant = $usersTbl->find("all", [
            "conditions" => [
                "Users.id" => $appID
            ]
        ])->first();
        
        return $applicant->id;
    }
    
  
    public function applicantUpdate() {
        
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->emailCheck($this->request->data('email'));
            
            if ($email) {
                return $this->redirect($this->referer() . "#email-exist");
            } else { 
                
                $usersTbl = TableRegistry::get('Users');
                $applicantID = $this->getUserID($this->request->data('token'));
                $applicant = $usersTbl->get($applicantID);
                
//                $this->changeTrainingStatus($applicantID, $this->request->data('training'));
                $applicant->first_name = $this->request->data('first_name');
                $applicant->last_name = $this->request->data('last_name');
                $applicant->email = $this->request->data('email');
                $applicant->mobile = $this->request->data('mobile');
                $applicant->phone = $this->request->data('phone');
                $applicant->suburb = $this->request->data('suburb');
                $applicant->owner_id = $this->request->data('owner_id');
                $applicant->street_number = $this->request->data('street_number');
                $applicant->street_name = $this->request->data('street_name');
                $applicant->city = $this->request->data('city');
                $applicant->postcode = $this->request->data('postcode');
                $applicant->comments = $this->request->data('comments');
                $applicant->blue_card_number = $this->request->data('blue_card_number');
                $applicant->blue_card_expiry = date('Y-m-d', strtotime($this->request->data('blue_card_expiry')));
                
//                debug($applicant->toArray());exit;
                
                $applicationID = $applicant->id;
                
                $applicationsTbl = TableRegistry::get('Applications');
               $applications = $applicationsTbl->find('all', ['conditions' => [
                   'Applications.user_id' => $applicationID
               ]]);
               
               foreach ($applications as $application) {
                   $id = $application->id;
               }
              $applicationDetails = $applicationsTbl->get($id);
                
                if ($this->request->data('status') == "Accepted") {
                    
                    $tutorsTable = TableRegistry::get('Tutors');
                    $tutorDetails= $tutorsTable->findByToken($applicant->token)->first();
                    $passwordGen = $this->generatePasswordString(7);
                    $applicant->password = crypt($passwordGen, 99);
                    $applicant->status = "Active";
                    $applicationDetails->status = $this->request->data('status');
                    $token = $applicant->token;
                    
                    $this->acceptedNotification($applicationID);
                    $this->emailNewTutor($token, $passwordGen);
                                       
                    $moduleInfo = $this->saveModuleUsers($tutorDetails->id, "New");
                    $this->generateModuleUserQuestions($moduleInfo); 
                    
                    $usersTbl->save($applicant);
                    $applicationsTbl->save($applicationDetails);
                } elseif ($this->request->data('status') == "Abandoned") {
                    $applicationDetails->status = $this->request->data('status');
                    $applicant->status = "Discontinue";
                    $this->rejectedNotification($applicationID);
                    
                    $usersTbl->save($applicant);
                    $applicationsTbl->save($applicationDetails);
                } elseif ($this->request->data('status') == "Reviewed") {
                    $applicationDetails->status = $this->request->data('status');
                    $applicationsTbl->save($applicationDetails);
                    $this->reviewedNotification($applicationID);
                }
                
                
                if ($this->request->data('training')) {
                     $this->changeTrainingStatus($applicantID, $this->request->data('training'));

                $usersTbl->save($applicant);
//              debug($applicationDetails); exit;
                return $this->redirect($this->referer() . "#applicant-updated");
            }
        }
    }

    }

    private function changeTrainingStatus($applicantID, $status){
        
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        $moduleDetails = $moduleUsersTable->findByUserId($applicantID)->first();
        $moduleDetails->status = $status;
            
        if($status == "New"):
            
            $moduleDetails->position = 1;
            
        elseif($status=="Completed Orientation"):  
            
            $moduleDetails->position = 2;
            
            
        elseif($status=="Pending Contract Review & Expectations"):    
            
            $moduleDetails->position = 3;
            
        elseif($status=="Completed Contract Review & Expectations"):   
            
            $moduleDetails->position = 4; 
            
        elseif($status=="Pending Platform Training"):    
            
            $moduleDetails->position = 5;
            
        elseif($status=="Completed Platform Training"): 
            
            $moduleDetails->position = 6;   
            
        elseif($status=="Pending Tutor Training"):   
            
            $moduleDetails->position = 7; 
            
        elseif($status=="Completed Training"):   
            
            $moduleDetails->position = 10; 
            
        endif;
        
        $moduleUsersTable->save($moduleDetails);
//        debug($moduleDetails);exit;
        
    }
    
    private function saveModuleUsers($id, $status, $position = 1){
                
        $moduleUsersTable = TableRegistry::get('ModuleUsers'); 
        $moduleDetails = $moduleUsersTable->newEntity();      
        $moduleDetails->email = date("Y-m-d H:i:s");
        $moduleDetails->user_id = $id;
        $moduleDetails->status = $status;
        $moduleDetails->position = $position;
        $moduleInfo = $moduleUsersTable->save($moduleDetails);
        return $moduleInfo;
        
    }
    
    private function generateModuleUserQuestions($moduleInfo){
        
        $moduleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        $moduleQuestionsTable = TableRegistry::get('ModuleQuestions');
        $moduleQuestionsDetails = $moduleQuestionsTable->find('all')->order(['ModuleQuestions.module_id' => 'asc']);
                
        foreach($moduleQuestionsDetails as $ques):
            
            $muqDetails = $moduleUserQuestionsTable->newEntity();
            $muqDetails->module_id      = $ques->module_id;
            $muqDetails->module_user_id = $moduleInfo->id;
            $muqDetails->type           = $ques->type;
            $muqDetails->questions      = $ques->questions;
            $muqDetails->choices        = $ques->choices;
            $muqDetails->answer         = $ques->answer;
            $muqDetails->image          = $ques->image;
            $muqDetails->user_answer    = "";
            $moduleUserQuestionsTable->save($muqDetails);
            
        endforeach;
        
        
    }
        
//    public function activateModule($moduleId = null){
//                
//        $this->checkLoginStatus();
//        $moduleUsersTable = TableRegistry::get('ModuleUsers');
//        $moduleDetails = $moduleUsersTable->get($moduleId);
//        if($moduleDetails->status == "Pending"):
//            $moduleDetails->status = 5;
//            $moduleUsersTable->save($moduleDetails);
//        endif;
//        return $this->redirect(array('controller' => 'trainings', 'prefix' => "admin", '_full' => true));
//        
//    }
    
//    public function bookingLesson(){
//        
//        $programsTable = TableRegistry::get('Programs');
//        $programDetails= $programsTable->find('all', ['contain' => ['Bookings']]);
//        $check = "Joshua-> <br/>";
//        foreach($programDetails as $program):
//            
//            if(empty($program['bookings']) && $program->status != "Done"):
//                $programInfo = $programsTable->get($program->id);
//                $programInfo->status = "Done";
//                $programsTable->save($programInfo);
//                $check .= "Sesniel -> <br/>";
////                echo $program->status;
////                debug($program);
//            endif;
//            
//        endforeach;
//        echo "Done <br/> $check";exit;
//        
//    }
    
  
}
 