<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class KpiController extends AppController
{

    public function index(){
       
        $this->checkLoginStatus();
        $lessonDetails = array('start_date' => '', 'end_date' => '');
        $lessonNotCDDetails = array('date' => '');
        if ($this->request->is('post') || $this->request->is('put')):
            
            if($this->request->data('start_date') != ""):
                $startDate = $this->changeDateDisplay($this->request->data('start_date'));
                $endDate   = $this->changeDateDisplay($this->request->data('end_date'));     
                $lessonDetails = $this->getLessonDoneSum($startDate, $endDate);
                $lessonDetails->start_date = $this->request->data('start_date');
                $lessonDetails->end_date = $this->request->data('end_date');
            endif;
            
            if($this->request->data('date') != ""):
                $lessonNotCDDetails = $this->getLessonHours($this->request->data('date'));
                $lessonNotCDDetails->date = $this->request->data('date');
            endif;
            
        endif;
        
        $this->set(compact('lessonDetails', 'lessonNotCDDetails'));        
        
    }
    
    public function leadingIndicators() {
        
        $this->checkLoginStatus();
        $this->viewBuilder()->layout('kpi-reporting');
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultationDetails = $consultationsTbl->find('all', ['conditions' => [
            'Consultations.created >=' => date("Y/m/d H:i:s", strtotime('-6 months')),
//            'Consultations.status in ' => ['New', 'Booked', 'Discontinued Consultation']
        ]])->select(['id','created', 'status'])
           ->order(['Consultations.created' => 'desc']);
//        debug($consultationDetails->toArray());exit;
        
        $programsTbl = TableRegistry::get('Programs');
        $programDetails = $programsTbl->find('all', ['conditions' => [
            'Programs.created >=' => date("Y/m/d H:i:s", strtotime('-6 months'))
        ]])->select(['id', 'created', 'status'])
           ->order(['Programs.created' => 'desc']);
//        debug($programDetails->toArray());exit;

        $this->set(compact('consultationDetails', 'programDetails'));
       
    }
    
    public function keyPerformanceIndicators() {
        
        $this->checkLoginStatus();
        $this->viewBuilder()->layout('kpi-reporting');
        $lessonsTbl = TableRegistry::get('Lessons');
        $programsTbl = TableRegistry::get('Programs');
        $clientTbl = TableRegistry::get('Clients');
        $lessonDetails = $lessonsTbl->find('all' , ['conditions' => [
            'Lessons.status' => "Completed"
        ]])->select(['length', 'status']);
        
        $status = ['Pending','Tutor','Done','Cancel','In Progress','Completed','Cancelled'];
        $programDetails = $programsTbl->find('all', ['conditions' => [
            'Programs.status in ' => $status
        ]])->contain(['Lessons'=> function ($q) {
            return $q->select(['program_id', 'id']);
        }])->select(['status', 'id', 'created']);
        
        $clientDetails = $clientTbl->find('all')
        ->contain(['Programs.Lessons'])
        ->select(['id', 'status', 'first_name']);
//        debug($clientDetails->toArray());exit;
        
        
        
        
        $this->set(compact('lessonDetails', 'programDetails', 'clientDetails'));
    }
    
    public function growthMetrics() {
        
        $this->checkLoginStatus();
        $this->viewBuilder()->layout('kpi-reporting');
        $lessonsTbl = TableRegistry::get('Lessons');
        $lessonDetails = $lessonsTbl->find('all', ['conditions' => [
//            'Lessons.status' => "Completed"
        ]])->select(['length', 'created', 'status', 'date'])
           ->order(['Lessons.created' => 'desc']);
//        debug($lessonDetails->toArray());exit;

        $this->set(compact('lessonDetails'));
 
    }
    
    private function getLessonHours($date){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->find('all', ['conditions' => [
            'Lessons.status not in' =>  ['Completed', 'Cancelled'],
            'Lessons.created <= ' => $this->changeDateDisplay($date) . " 00:00:00"
        ]]);
        $lessonDetails = $lessonDetails->select(['sumlength' => $lessonDetails->func()->sum('length')])->first();     
//        echo $date;
//        debug($lessonDetails->toArray()); exit;
        return $lessonDetails;
    }
    
    private function getLessonDoneSum($start, $end){
        
        $LessonsTable  = TableRegistry::get('Lessons');
        $lessonDetails = $LessonsTable->find('all', ['conditions' => [
            'Lessons.status' =>  "Completed",
            'Lessons.date >=' => $start,
            'Lessons.date <=' => $end
        ]]);
        $lessonDetails = $lessonDetails->select(['sumlength' => $lessonDetails->func()->sum('length')])->first();     

        return $lessonDetails;
    }

}
