<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

class DashboardController extends AppController {
    
    
//    public function uuupdateEmail(){
//        
//        $usersTable = TableRegistry::get('Users');
//        $uDetails = $usersTable->find('all');
//        foreach($uDetails as $u):
//            $uUpdate = $usersTable->get($u->id);
//            $uUpdate->email = $uUpdate->email . "12";
//            $usersTable->save($uUpdate);
//        endforeach;
//        echo "test";exit;
//        
//    }
    
    public function editView(){
        $this->checkLoginStatus();
        $leadsTbl1 = TableRegistry::get('Leads');
        $notAlign = $leadsTbl1->find('all', [
                    'conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.created >' => date('Y-m-d', strtotime("-2 month"))
                    ]
                ])->contain(['Tutors', 'Clients', 'Students.YearLevels', 'LeadSubjects.Subjects']);
        
        $leads1 = $leadsTbl1->find('all');
        
//        debug($notAlign->toArray()); exit;
        $this->set(compact('leads1', 'notAlign'));
    }

    public function sendemailblast() {

        $usersTable = TableRegistry::get('Users');

        $userDetails = $usersTable->find('all', ['conditions' => [
                'Users.id' => 416
        ]]);


        // $userDetails = $usersTable->find('all', ['conditions' => [
        //     'Users.type != ' => 'Admin'
        // ]]);
        // debug($this->getGeneratedRandomString());
        // debug($userDetails->toArray());exit;
        //debug

        foreach ($userDetails as $user):

            $userInfo = $usersTable->get($user->id);
            $password = $this->getGeneratedRandomString();
            $encryptedPassword = crypt($password, 99);
            $userInfo->password = $encryptedPassword;
            $userInfo->blast = 1;
            // debug($userDetails->toArray());
            // debug($userInfo);exit;
            if ($usersTable->save($userInfo)):


                $subject = 'Credentials for Tutor2You';
                $message = 'Good evening ' . ucfirst($user->first_name) . ' ' . ucfirst($user->last_name) . ',

                    Please find below login details to the new platform.

                    http://platform.tutor2you.com.au

                    Your login details are as follows:
                    Email: ' . $user->email . '
                    Password: ' . $password . ' 

                    If you have any issues accessing the new platform, please contact 
                    admin@tutor2you.com.au. 

                    Regards,

                    Tutor2you
                    www.tutor2you.com.au
                    
                ';

                $email = new Email();
                $email->from('noreply@tutor2you.com.au')
                        ->to($user->email)
                        ->subject($subject)
                        ->send($message);
            else:

                echo $user->id . " -> " . $user->first_name . ' ' . $user->last_name;


            endif;


        endforeach;

        echo "DOne";
        exit;
    }

    function getGeneratedRandomString($length = 10) {
        $characters = '0v-wxyzABCD67#89abcdefgh_EFGY12345noNOPQRSTUVWXstu&pqrHIJKLMijklmZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    public function index ($stat = 'Cancelled') {
        $this->checkLoginStatus();
        $status = ['New', 'Pending Consultation'];
        $consultationTbl = TableRegistry::get('Consultations');
        $programsTable = TableRegistry::get('Programs');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status in ' => $status,
                        'Consultations.created <= ' => date("Y/m/d H:i:s", strtotime('-24 hours'))
                     ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students','AbandonedLeads']);
//        debug($consultations->toArray());exit;
        
        if($stat == "Cancelled"):
            $cond = " != ";
        else:            
            $cond = "";
        endif;
    
        $programDetails = $programsTable->find('all', ['conditions' => [
            'Programs.status' . $cond => $stat,
            //'Programs.rebook_lessons in ' => 3,
            "Programs.rebook_status is null"
        ]])->contain(['RebookLessons', 'ProgramStudents.Students', 'Clients'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            },'Tutors'=>function ($q) {
                return $q->select(['id','first_name', 'last_name']);
            }]); 
            
        $rebookCount = 0;
        foreach ($programDetails as $programs):
           $rebook = count($programs->rebook_lessons);
//            echo $rebook . "<br>";
           $rebookCount += $rebook;
        endforeach;
        
        $uninvoicedBookingsDetails = $programsTable->find('all', ['conditions' => [
            'Programs.status in ' => $programStatus,
            'Programs.invoice_number' => ''
            
        ]])->contain(['Tutors', 'Clients']);
        
        $uninvoicedCount = 0;
        foreach ($programDetails as $programs):
            $uninvoiced = count($programs);
            $uninvoicedCount += $uninvoiced;
        endforeach;
        
//        debug($programDetails->toArray());exit;
        $this->set(compact('consultations', 'programDetails'));
        
        
    }


    public function index1($status = "Pending Consultation")
    {
    
        $this->checkLoginStatus();
        $leadTbl = TableRegistry::get('Leads');
        $studentsTbl = TableRegistry::get('Students');
        $leadSubQuery = $leadTbl->find()->select('Leads.student_id');
        $noTutor = $studentsTbl->find()
                ->where(['Students.id not in' => $leadSubQuery, 'Clients.status' => 'Active'])
                ->contain(['YearLevels', 'StudentSubjects.Subjects', 'Clients']);
        
//        debug($noTutor->toArray()); exit;
        
        $statusSelect = ($status == "All") ? "%%" : $status;
        if ($this->request->is(['patch', 'post', 'put'])):
            $this->redirect(['action' => 'index', $this->request->data['status']]);
        endif;
        //$statusSelect = ($status == "All") ? "%%" : $status;
        
        if($status == 'All'):
            $statusSelect = array('New','Pending Consultation','Completed Consultation','Abandoned','Booked','Discontinued Consultation'); 
        elseif($status == 'Active'):
            $statusSelect = array('New', 'Pending Consultation'); 
        else:
            $statusSelect = array($status);    
        endif;
        
        
        $consultations = $this->getConsultations($statusSelect);
        
        $this->set(compact('noTutor', 'consultations','status','consultations'));
        $this->set('assmntNotCom', $this->stdAssmntNotCom());
        $this->set('notAligned', $this->leadWithNotAlign());

    }
    
    private function getConsultations($status) {
        $consultationTbl = TableRegistry::get('Consultations');
        $consultations = $consultationTbl->find('all', [
                    'conditions' => [
                        'Consultations.status in ' => $status
                    ]
                ])->contain(['Leads.Tutors','Leads.Clients','Leads.Students','AbandonedLeads']);
//        debug($consultations->toArray());exit;
        return $consultations;
    }

    public function updateconsultation($consultation_id = null){
//        $leads = $this->updateLeadsStatus($consultation_id);
        $consultationsTbl = TableRegistry::get('Consultations');
        $consultation = $consultationsTbl->get($consultation_id);
        $consultation->status = "Completed Consultation";
        $consultationsTbl->save($consultation);
        $this->redirect(['action' => 'index']);
    }
    
//    private function updateLeadsStatus($consultation_id){        
//        $leadsTbl = TableRegistry::get('Leads');
//        $leads = $leadsTbl->find('all', [
//            'conditions' => ['Leads.consultation_id' => $consultation_id]]
//                )->select('id');
//        foreach($leads as $lead){
//            $detail = $leadsTbl->get($lead->id);
//            $detail->status = "Completed Consultation";
//            $leadsTbl->save($detail);
//        }        
//    }

    private function bulkAddTokenAndPass(){
        $usersTbl = TableRegistry::get('Users');
        $users = $usersTbl->find('all');

        $password = crypt('temp01', 99);
        foreach ($users as $key => $user) {
            $detail = $usersTbl->get($user->id);
            $detail->password = $password;
            $detail->token = $this->userTokenGenerate();
            $usersTbl->save($detail);
        }

        debug("executed!");
        exit;
    }

    private function userTokenGenerate(){
        $usersTbl = TableRegistry::get('Users');
        do {
            $token = md5(rand(0, 9999));

            $tokenQuery = $usersTbl->find("all", [
                        "conditions" => [
                            "Users.token" => $token
                        ]
                    ])->first();
        } while ($tokenQuery);
        return $token;
    }

    private function stdAssmntNotCom() {
        $stdAssmntTypeTbl = TableRegistry::get('StudentAssessmentTypes');
        $notCompleted = $stdAssmntTypeTbl->find('all', [
                    'conditions' => [
                        'StudentAssessmentTypes.status' => 'Pending',
                        'StudentAssessmentTypes.created <' => new \DateTime('-2 days')
                    ]
                ])->contain(['Students', 'AssessmentTypes', 'Tutors']);
        return $notCompleted;
    }

    private function leadWithNotAlign() {
        $leadTbl = TableRegistry::get('Leads');
        $notAlign = $leadTbl->find('all', [
                    'conditions' => [
                        'Leads.status' => 'Booked',
                        'Leads.created >' => date('Y-m-d', strtotime("-2 month"))
                    ]
                ])->contain(['Tutors', 'Clients', 'Students.YearLevels', 'LeadSubjects.Subjects']);
        return $notAlign;
    }

    public function abandonLead() {
        $leadTable = TableRegistry::get('Leads');
        $lead = $leadTable->get($this->request->data['lead_id']);
        $lead->note = $this->request->data['reason'];
        $lead->status = "Abandoned";
        $leadTable->save($lead);
        if ($this->request->data('student_id')) {
            $this->redirect(array('controller' => 'allocateLead', 'action' => 'student', $this->request->data('student_id')));
        } else {
            $this->redirect($this->referer() . '#lead-abandoned');
        }
    }
    
    public function consultationDetails(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');

        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadAnswers = $leadAnswersTbl->find('all',[
                'conditions' => ['LeadAnswers.lead_id' => $lead_id]
            ]);
        $this->set('leadQuestions',$leadAnswers);
    }
    
    public function consultationView() {
        $this->viewBuilder()->layout('blank');
        $consultation_id = $this->request->data('consultation_id');
        $leadAnswersTbl = TableRegistry::get('LeadAnswers');
        $leadQuestions = $leadAnswersTbl->find('all', [
            'conditions' => ['LeadAnswers.consultation_id' => $consultation_id]
        ]);
        $this->set('leadQuestions', $leadQuestions);
    }

    public function viewStudentDetails() {
        $this->viewBuilder()->layout('blank');
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.consultation_id' => $this->request->data('consultation_id'),
                        'Leads.status' => "Booked"
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects','Clients']);
        $this->set(compact('leads'));
    }
    
    public function reallocate(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');
        $student_id = $this->request->data('student_id');
        $this->set(compact('lead_id','student_id'));
    }
    
    public function abandon(){
        $this->viewBuilder()->layout('blank');
        $lead_id = $this->request->data('lead_id');
        $this->set(compact('lead_id'));
    }
    
    public function editConsultation($consultation_id, $type = "Edit"){
        $this->checkLoginStatus();
        $leadsTbl = TableRegistry::get('Leads');
        $leads = $leadsTbl->find('all', [
                    'conditions' => [
                        'Leads.consultation_id' => $consultation_id
//                        ,'Leads.status !=' => 'Abandoned'
                        
                    ]
                ])->contain(['Students.YearLevels', 'LeadSubjects.Subjects', 'Clients']);
      
        
//        debug($leads->toArray());
        $consultation = $this->getConsultation2($consultation_id);
       
        $this->set(compact('leads', 'consultation', 'type'));
    }
    
    private function getConsultation2($consultation_id) {
        $consultationTbl = TableRegistry::get('Consultations');
        $consultation = $consultationTbl->get($consultation_id);
        return $consultation;
    }
    
    public function newConAndPayDate(){
        
        $leadsTable = TableRegistry::get('Leads');
        $leadDetails= $leadsTable->findByConsultationId($this->request->data('consultation_id'))
                ->order(['created' => 'desc'])->first();
        debug($leadDetails->toArray());exit;
        
    }
    
    public function updateConAndPayDate() {
        if ($this->request->is('post') || $this->request->is('put')) {
            
            $leadStatus = array("Booked", "Abandoned"); 
            $consultationTable = TableRegistry::get('Consultations');
                      
            $pDate = $this->changeDateDisplay($this->request->data('payDate')); 
            $cDate = $this->changeDateDisplay($this->request->data('conDate'));
            $cTime= date('H:i:s', strtotime($this->request->data('conTime')));
            $consultation = $cDate . " " . $cTime;            
            
            $consultationDetails = $consultationTable->get($this->request->data('consultation_id'));
            $consultationDetails->consultation_session_date = $consultation;
            $consultationDetails->payroll_date = $pDate;
            
            if (in_array($this->request->data('status'), $leadStatus)) {
                
                $consultationDetails->status = $this->request->data('status');  
                $this->updateLeadStatus($this->request->data('consultation_id'), $this->request->data('status'));
                
            }else{
                
                $consultationDetails->status = $this->request->data('status');
                
            }
          
            $consultationTable->save($consultationDetails);
            return $this->redirect(array('controller' => 'bookings', 'action' => 'view#consultation-updated'));
        }
     
    }
    
    public function updateLeadStatus($consultationId, $status){
        
        $leadsTbl = TableRegistry::get('Leads');
        $leadDetails = $leadsTbl->find('all', ['conditions' => ['Leads.consultation_id' => $consultationId]]);
        
        foreach($leadDetails as $lead):
            
            $updateLeadDetails = $leadsTbl->get($lead->id);
            $updateLeadDetails->status = $status;
            $leadsTbl->save($updateLeadDetails);
        
        endforeach;
        
            
    }
    
}
