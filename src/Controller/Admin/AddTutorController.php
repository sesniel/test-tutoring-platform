<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class AddTutorController extends AppController
{

     public function index()
    {
        $this->checkLoginStatus();
        $ratesTbl = TableRegistry::get('Rates');
        $rateDetails = $ratesTbl->find('all');
        $statesTbl = TableRegistry::get('States');
        $states = $statesTbl->find('all');
        $yrLvlsTbl = TableRegistry::get('YearLevels');
        $yrLvls = $yrLvlsTbl->find('all');
        $subjsTbl = TableRegistry::get('Subjects');
        $subjs = $subjsTbl->find('all', [
            'conditions' => ['Subjects.id >' => '4']
        ]);

        $this->set(compact('states', 'yrLvls', 'subjs', 'rateDetails'));

    }  
    
    private function generateModuleUserQuestions($moduleInfo){
        
        $moduleUserQuestionsTable = TableRegistry::get('ModuleUserQuestions');
        $moduleQuestionsTable = TableRegistry::get('ModuleQuestions');
        $moduleQuestionsDetails = $moduleQuestionsTable->find('all')->order(['ModuleQuestions.module_id' => 'asc']);
                
        foreach($moduleQuestionsDetails as $ques):
            
            $muqDetails = $moduleUserQuestionsTable->newEntity();
            $muqDetails->module_id      = $ques->module_id;
            $muqDetails->module_user_id = $moduleInfo->id;
            $muqDetails->type           = $ques->type;
            $muqDetails->questions      = $ques->questions;
            $muqDetails->choices        = $ques->choices;
            $muqDetails->answer         = $ques->answer;
            $muqDetails->image          = $ques->image;
            $moduleUserQuestionsTable->save($muqDetails);
            
        endforeach;
        
        
    }

    public function getAToken()
    {

        $usersTbl = TableRegistry::get('Users');
        do {
            $token = md5(rand(0, 9999));

            $tokenQuery = $usersTbl->find("all", [
                "conditions" => [
                    "Users.token" => $token
                ]
            ])->first();

        } while ($tokenQuery);
        return $token;

    }

    public function generatePasswordString($length)
    {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
    
    public function generateModuleUsers(){
        
        $tutorsTable = TableRegistry::get('Tutors');
        $tutorsInfo = $tutorsTable->find('all', ['conditions' => [
            'Tutors.type' => 'Tutor'
        ]])->contain(['ModuleUsers']);
        
        $count = 0;
        foreach($tutorsInfo as $tutor):
            if(empty($tutor->module_user)):
                
                if($tutor->id == 903 || $tutor->id == 913):

                    $moduleInfo = $this->saveModuleUsers($tutor->id, "Pending Platform Training", 5);
                    $this->generateModuleUserQuestions($moduleInfo); 
                    $count++;
                  
                else:

                    $moduleInfo = $this->saveModuleUsers($tutor->id, "Pending Tutor Training", 7);
                    $this->generateModuleUserQuestions($moduleInfo); 
                    $count++;
                    
                endif;
                
            endif;
        endforeach;
        echo $count;
        exit;
        debug($tutorsInfo->toArray());
        
        
    }

    public function createTutor()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
 
        
            $usersTbl = TableRegistry::get('Users');

            $email = $usersTbl->find("all", [
                "conditions" => [
                    "Users.email" => $this->request->data("email")
                ]
            ])->first();

            if ($email) {

                return $this->redirect($this->referer() . "#email-exist");

            } else {

                $token = $this->getAToken();
                $passwordGen = $this->generatePasswordString(7);

                $userData = $usersTbl->newEntity();
                $userData->owner_id = $this->request->session()->read("id");
                $userData->type = "Tutor";
                $userData->access = 0;
                $userData->first_name = $this->request->data['fname'];
                $userData->last_name = $this->request->data['lname'];
                $userData->email = $this->request->data['email'];
                $userData->phone = $this->request->data['phone'];
                $userData->mobile = $this->request->data['mobile'];
                $userData->rate_id = $this->request->data['rate_id'];
                $userData->status = "Active";
                $userData->password = crypt($passwordGen, 99);
                $userData->token = $token;
                $userDetails = $usersTbl->save($userData);

                if ($userDetails) {
                    
                    $tutorsTable = TableRegistry::get('Tutors');
                    $tutorDetails= $tutorsTable->findByToken($token)->first();
                    $moduleInfo = $this->saveModuleUsers($tutorDetails->id, "New");
                    $this->generateModuleUserQuestions($moduleInfo); 

                    $tutorID = $userDetails->id;

                    $tutorSubjTbl = TableRegistry::get('TutorSubjects');
                    $tutSubject = $this->request->data('subjects');

                    foreach ($tutSubject as $subjects) {
                        $tutSubj = $tutorSubjTbl->newEntity();
                        $tutSubj->tutor_id = $tutorID;
                        $tutSubj->subject_id = $subjects;
                        $tutorSubjTbl->save($tutSubj);
                    }

                }
                    
    //                debug($userDetails);
                    $userDetailsUp = $usersTbl->find('all', ['conditions' => [
                        'Users.email' => $this->request->data['email'],
                    ]])->contain(['Students.YearLevels', 'States'])->first();
                    $list = 14;//($user->type == "Tutor") ? 11 : 10;
                    $list1 = 11;
                    $grades = $this->calculateSubject($userDetailsUp);
                    $type = "add";
                    $post = array(
                        'email'                    => $userDetailsUp->email,
                        'first_name'               => $userDetailsUp->first_name,
                        'last_name'                => $userDetailsUp->last_name,
                        'orgname'                  => 'Tutor2You',
                        'field[14,0]'              => $userDetailsUp->postcode,
                        'field[9,0]'               => $grades,
                        'field[18,0]'              => $userDetailsUp->type,
                        'field[19,0]'              => $userDetailsUp->status,
                        'phone'                    => $userDetailsUp->phone . " , " . $userDetailsUp->mobile,
                        'tags'                     => 'api,'.$userDetailsUp->type,
                        'p['.$list.']'             => $list, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'p['.$list1.']'             => $list1, // example list ID (REPLACE '123' WITH ACTUAL LIST ID, IE: p[5] = 5)
                        'status[1]'                => 1, // 1: active, 2: unsubscribed (REPLACE '123' WITH ACTUAL LIST ID, IE: status[5] = 0)
                    );
                    $this->crudContactList($post, $type);
                    
                $this->emailNewTutor($token,$passwordGen);
                return $this->redirect(array('controller' => 'dashboard', 'action' => 'index#account-created'));
            }

        }

    }
    
    private function saveModuleUsers($id, $status, $position = 1){
                
        $moduleUsersTable = TableRegistry::get('ModuleUsers');
        
//        if($type == "New"):
            $moduleDetails = $moduleUsersTable->newEntity();
//        else:
//            $moduleDetails = $moduleUsersTable->get($type);
//        endif;            
        
        $moduleDetails->email = date("Y-m-d H:i:s");
        $moduleDetails->user_id = $id;
        $moduleDetails->status = $status;
        $moduleDetails->position = $position;
        $moduleInfo = $moduleUsersTable->save($moduleDetails);
        
        return $moduleInfo;
        
    }


}