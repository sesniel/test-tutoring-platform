<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class AssessmentsController extends AppController
{

    public function index()
    {
        $this->checkLoginStatus();
        $LeadsTable = TableRegistry::get('Leads');
//        $leadsDetails = $LeadsTable->find('all', ['conditions' => [
//            'Leads.status' => 'Booked'
//        ]])->contain(['Students', 'Tutors', 'Clients', 'LeadSubjects']);

        $lAlign = $LeadsTable->find('all', [
            'conditions' => [
                'Leads.status' => 'Booked'
            ]
        ])->contain(['Students.StudentAssessmentTypes'=>function ($q) {
                return $q->where(['status != ' => 'In Active']);
            }, 'Tutors', 'Clients', 'LeadSubjects']);
//        debug($lAlign->toArray());exit;
//        $leads = $this->getLeadSubjectDetails($leadsDetails);
//        $lAlign = $this->getStudentAssessmentTypes($lAlignDetails);
        $assmntResults = $this->getAssessmentResults();
        $this->set(compact('lAlign', 'assmntCount', 'assmntResults'));
    }

    private function getLeadSubjectDetails($leads = null)
    {

        $studentSubjectDetails = $leads->toArray();
        $SubjectsTable = TableRegistry::get('Subjects');
        foreach ($studentSubjectDetails as $key => $lead):
            foreach ($lead['lead_subjects'] as $sub => $subject):
                $subjectDetails = $SubjectsTable->find('all', ['conditions' => [
                    'Subjects.id' => $subject->subject_id
                ]])->first();
                $studentSubjectDetails[$key]['lead_subjects'][$sub]['subject_info'] = $subjectDetails;
            endforeach;
        endforeach;
        return $studentSubjectDetails;
    }

    private function getStudentAssessmentTypes($leads = null)
    {

        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $eachLeads = $leads->toArray();
        foreach ($eachLeads as $key => $lead):
            $studentAssessmentDetails = $StudentAssessmentTypesTable->find('all', ['conditions' => [
                'StudentAssessmentTypes.student_id' => $lead->student_id,
                'StudentAssessmentTypes.status != ' => 'In Active',
            ]]);
            if (!empty($studentAssessmentDetails)):
                $eachLeads[$key]['studentAssessmentTypeDetails'] = $studentAssessmentDetails->toArray();
            else:
                $eachLeads[$key]['studentAssessmentTypeDetails'] = array();
            endif;
        endforeach;
        return $eachLeads;

    }

    private function getAssessmentResults()
    {
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $students = $studentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.status' => 'Done'
        ]])->contain(['AssessmentTypes', 'Students']);
        return $students;

    }

    public function student($id = null, $leadId = null)
    {

        $this->viewBuilder()->layout('blank');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentDetails = $studentTable->find('all', [
            'conditions' => ['Students.id' => $id]
        ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $assessmentTypeDetails = $assessmentTypesTable->find('all')
            ->contain(['Subjects'])
            ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'leadId'));

    }

    public function cancelAssessment($id)
    {
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentsAT = $studentAssessmentTypesTable->get($id); 
        if ($studentAssessmentTypesTable->delete($studentsAT)) {
            $this->delUserQuestions($id);
            $this->redirect($this->referer());   
        }        

    }

    private function delUserQuestions($stdAssTypId){
        $userQuestTbl = TableRegistry::get('UserQuestions');
        $userQuestions = $userQuestTbl->find('all',[
                'conditions' => ['UserQuestions.student_assessment_type_id' => $stdAssTypId]
            ]);
        foreach ($userQuestions as $key => $value) {
            $question = $userQuestTbl->get($value->id); 
            $userQuestTbl->delete($question);
        }
    }

    public function editStudent($id = null, $token = null)
    {

        $this->viewBuilder()->layout('blank');
        $studentTable = TableRegistry::get('Students');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $studentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentDetails = $studentTable->find('all', [
            'conditions' => ['Students.id' => $id]
        ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();
        $studentAssessmentDetails = $studentAssessmentTypesTable->find('all', ['conditions' => [
            'StudentAssessmentTypes.student_id' => $id,
            'StudentAssessmentTypes.status in ' => array('Pending', 'In Progress')
        ]])->contain(['AssessmentTypes', 'Students']);

        $assessmentTypeDetails = $assessmentTypesTable->find('all')
            ->contain(['Subjects'])
            ->order(['AssessmentTypes.type']);
        $studentSubjects = $this->getStudentSubject($studentDetails->student_subjects);
        $this->set(compact('studentDetails', 'assessmentTypeDetails', 'studentSubjects', 'studentAssessmentDetails', 'token'));

    }

    private function getTutorID($token)
    {
        $tutorTable = TableRegistry::get('Tutors');
        $tutorID = $tutorTable->findByToken($token)
            ->select('id')
            ->first();
        return $tutorID;
    }

    public function allocate()
    {
        $studentTable = TableRegistry::get('Students');
        $studentDetails = $studentTable->find('all', [
            'conditions' => ['Students.id' => $this->request->data['student_id']]
        ])->contain(['Clients', 'YearLevels', 'StudentSubjects'])->first();

        $questions = array();
        foreach ($this->request->data['assessments'] as $assessmentId):

            $categoryId = $this->getCategoryInfoId($assessmentId, $studentDetails->year_level_id);

            if ($categoryId != 0):
                $majorDetails = $this->getQuestionDetailsByCategory($categoryId);
                $questions = $this->arrangeCategoryQuestion($majorDetails);
            else:
                $studentYearId = $this->identifyDiagnosticExtension($studentDetails->year_level_id, $assessmentId);
                $majorDetails = $this->getQuestionDetailsBySubjectYear($assessmentId, $studentYearId);
                $questions = $this->arrangeQuestion($majorDetails, $studentDetails->year_level_id);
            endif;

            $studentAssessmentLastId = $this->saveStudentAssessmentTypes($assessmentId, $studentDetails, $this->request->data);

            $this->saveUserQuestions($questions, $studentAssessmentLastId);

        endforeach;

        if (isset($this->request->data['lead_id'])):
            $this->updateLeadStatus($this->request->data['lead_id']);
        endif;

        return $this->redirect(array('controller' => 'assessments', 'prefix' => "admin", '_full' => true));

    }

    private function arrangeCategoryQuestion($majorDetails = null)
    {

        foreach ($majorDetails as $major):
            if ($major['minor_outcomes']):
                foreach ($major['minor_outcomes'] as $minor):

                    foreach ($minor['Questions'] as $ques):
                        $saveQ[] = $ques;
                    endforeach;

                endforeach;
            endif;
        endforeach;
        return $saveQ;

    }

    private function updateLeadStatus($id = null)
    {

        $LeadsTable = TableRegistry::get('Leads');
        $LeadsDetails = $LeadsTable->get($id);
        $LeadsDetails->status = "Booked";
        $LeadsTable->save($LeadsDetails);

    }

    private function saveStudentAssessmentTypes($assessmentId = null, $studentDetails = null, $studentPostDetails = null)
    {
        $tutorID = $this->getTutorID($studentPostDetails['tutor_token']);
        $StudentAssessmentTypesTable = TableRegistry::get('StudentAssessmentTypes');
        $studentAssessmentType = $StudentAssessmentTypesTable->newEntity();
        $studentAssessmentType->student_id = $studentPostDetails['student_id'];
        $studentAssessmentType->assessment_type_id = $assessmentId;
        $studentAssessmentType->tutor_id = $tutorID['id'];
        $studentAssessmentType->year_level_id = $studentDetails->year_level_id;
        $studentAssessmentType->status = 'Pending';
        $studentAssessmentTypeId = $StudentAssessmentTypesTable->save($studentAssessmentType);
        $studentAssessmentLastId = $studentAssessmentTypeId->id;
        $this->emailNewAssmntCreated($studentAssessmentLastId);
        return $studentAssessmentLastId;
    }

    private function saveUserQuestions($questions = null, $studentAssessmentId = null)
    {

        $UserQuestionsTable = TableRegistry::get('UserQuestions');
        $x = 0;
        foreach ($questions as $ques): $x++;
            $userQuestion = $UserQuestionsTable->newEntity();
            $userQuestion->student_assessment_type_id = $studentAssessmentId;
            $userQuestion->position = $x;
            $userQuestion->minor_outcome_id = $ques->minor_outcome_id;
            $userQuestion->question = $ques->description;
            $userQuestion->mark = $ques->mark;
            $userQuestion->correct_answer = $ques->answer;
            $userQuestion->image = $ques->image;
            $userQuestion->choices = $ques->choices;
            $UserQuestionsTable->save($userQuestion);
        endforeach;

    }

    private function arrangeQuestion($majorDetails = null, $year_level_id = null)
    {

        foreach ($majorDetails as $major):
            if ($major['minor_outcomes']):
                foreach ($major['minor_outcomes'] as $minor):
                    $qVal = count($minor['Questions']);
                    if ($year_level_id < 7):
                        if ($qVal > 1): $questionsIds = array_rand($minor['Questions'], 2);
                        else: $questionsIds = array(0); endif;

                        if (isset($questionsIds)):
                            foreach ($questionsIds as $id):
                                if ($minor['Questions']):
                                    $saveQ[] = $minor['Questions'][$id];
                                endif;
                            endforeach;
                        endif;
                    else:

                        $qVal = count($minor['Questions']);
                        if ($qVal >= 1): $questionsIds = array_rand($minor['Questions'], 1); endif;

                        if (isset($questionsIds)):
                            if ($minor['Questions']): $saveQ[] = $minor['Questions'][$questionsIds]; endif;
                        endif;

                    endif;
                endforeach;
            endif;
        endforeach;
        shuffle($saveQ);
        return $saveQ;

    }

    private function getQuestionDetailsByCategory($categoryId = null)
    {

        $majorOutcomesTable = TableRegistry::get('MajorOutcomes');
        $questionsTable = TableRegistry::get('Questions');
        $majorOutcomeDetails = $majorOutcomesTable->find('all', ['conditions' => [
            'MajorOutcomes.category_id' => $categoryId
        ]])->contain(['MinorOutcomes'])->toArray();
        foreach ($majorOutcomeDetails as $key => $major):
            foreach ($major['minor_outcomes'] as $key2 => $minor):
                $questionDetails = $questionsTable->find('all', ['conditions' => [
                    'Questions.minor_outcome_id' => $minor->id
                ]])->toArray();
                $majorOutcomeDetails[$key]['minor_outcomes'][$key2]['Questions'] = $questionDetails;
            endforeach;
        endforeach;
        return $majorOutcomeDetails;

    }

    private function getQuestionDetailsBySubjectYear($assessmentTypeId = null, $yearLevelId = null)
    {

        $majorOutcomesTable = TableRegistry::get('MajorOutcomes');
        $questionsTable = TableRegistry::get('Questions');
        $assessmentTypesTable = TableRegistry::get('AssessmentTypes');
        $assessmentDetails = $assessmentTypesTable->find('all', ['conditions' => ['AssessmentTypes.id' => $assessmentTypeId]])->first();
        $majorOutcomeDetails = $majorOutcomesTable->find('all', ['conditions' => [
            'MajorOutcomes.subject_id' => $assessmentDetails->subject_id,
            'MajorOutcomes.year_level_id' => $yearLevelId
        ]])->contain(['MinorOutcomes'])->toArray();
        foreach ($majorOutcomeDetails as $key => $major):
            foreach ($major['minor_outcomes'] as $key2 => $minor):
                $questionDetails = $questionsTable->find('all', ['conditions' => [
                    'Questions.minor_outcome_id' => $minor->id
                ]])->toArray();
                $majorOutcomeDetails[$key]['minor_outcomes'][$key2]['Questions'] = $questionDetails;
            endforeach;
        endforeach;
        return $majorOutcomeDetails;

    }

    private function identifyDiagnosticExtension($year_level_id, $assessmentId)
    {


        if ($year_level_id == 1 || $assessmentId >= 5): //Extension
            $studentYearId = $year_level_id;
        elseif ($assessmentId == 3 || $assessmentId == 4): //Diagnostic
            $studentYearId = $year_level_id - 1;
        else: //Others
            $studentYearId = $year_level_id;
        endif;
        return $studentYearId;

    }

    private function getStudentSubject($studentSubjects)
    {

        $subjectsTable = TableRegistry::get('Subjects');
        $subjectArray = array();
        foreach ($studentSubjects as $subject):

            $studentDetails = $subjectsTable->find('all', [
                'conditions' => ['Subjects.id' => $subject->subject_id]
            ])->first();
            $subjectArray[] = $studentDetails->name;

        endforeach;
        sort($subjectArray);
        return $subjectArray;

    }

    private function getCategoryInfoId($assessmentId, $year_level_id)
    {


        if ($assessmentId == 1):

            if ($year_level_id <= 7):
                $categoryId = 9;
            else:
                $categoryId = 10;
            endif;

        elseif ($assessmentId == 2):

            if ($year_level_id <= 7):
                $categoryId = 7;
            else:
                $categoryId = 8;
            endif;

        else:

            $categoryId = 0;

        endif;

        return $categoryId;

    }


}
