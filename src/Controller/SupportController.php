<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Model\Table\OrderTable;
use App\Model\Table\App\Model\Table;
use DateTime;
use Cake\Utility\Text;

class SupportController extends AppController
{

    public function support()
    {
        if ($this->request->is('post') || $this->request->is('put')) {
            $type = $this->checkLoginStatus();
            $userID = $this->getUserID($this->request->data('token'));
            $feedbackTbl = TableRegistry::get('Feedbacks');
            $feedback = $feedbackTbl->newEntity();
            $feedback->user_id = $userID;
            $feedback->page = $this->request->data('page');
            $feedback->subject = $this->request->data('subject');
            $feedback->description = $this->request->data('message');
            $feedbackDetails = $feedbackTbl->save($feedback);
            if ($feedbackDetails){
                $this->emailSupport($feedbackDetails->id);
                return $this->redirect($this->referer() . "#report-sent");
            }
        }
    }

    private function getUserID($token)
    {
        $usersTbl = TableRegistry::get('Users');
        $user = $usersTbl->findByToken($token)->first();
        return $user->id;
    }

}
