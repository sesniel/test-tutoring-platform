<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ModuleUserQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ModuleUserQuestionsTable Test Case
 */
class ModuleUserQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ModuleUserQuestionsTable
     */
    public $ModuleUserQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.module_user_questions',
        'app.modules',
        'app.module_questions',
        'app.module_users',
        'app.users',
        'app.students',
        'app.clients',
        'app.states',
        'app.availabilities',
        'app.days',
        'app.availability_time',
        'app.feedbacks',
        'app.year_levels',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.tutors',
        'app.reassessment_questions',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.user_questions',
        'app.student_assessment_types',
        'app.lesson_resources',
        'app.lead_subjects',
        'app.leads',
        'app.student_subjects',
        'app.tutor_subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ModuleUserQuestions') ? [] : ['className' => 'App\Model\Table\ModuleUserQuestionsTable'];
        $this->ModuleUserQuestions = TableRegistry::get('ModuleUserQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ModuleUserQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
