<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MajorOutcomesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MajorOutcomesTable Test Case
 */
class MajorOutcomesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MajorOutcomesTable
     */
    public $MajorOutcomes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.year_levels',
        'app.lessons',
        'app.programs',
        'app.assessment_types',
        'app.student_assessment_types',
        'app.statuses',
        'app.lesson_resources',
        'app.resources',
        'app.minor_outcomes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MajorOutcomes') ? [] : ['className' => 'App\Model\Table\MajorOutcomesTable'];
        $this->MajorOutcomes = TableRegistry::get('MajorOutcomes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MajorOutcomes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
