<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MessagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MessagesTable Test Case
 */
class MessagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MessagesTable
     */
    public $Messages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.messages',
        'app.users',
        'app.rates',
        'app.tutors',
        'app.owner',
        'app.students',
        'app.clients',
        'app.states',
        'app.leads',
        'app.consultations',
        'app.lead_answers',
        'app.abandoned_leads',
        'app.lead_subjects',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.program_students',
        'app.lesson_students',
        'app.major_outcomes',
        'app.categories',
        'app.year_levels',
        'app.student_assessment_types',
        'app.user_questions',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources',
        'app.rebook_lessons',
        'app.payroll_lessons',
        'app.bookings',
        'app.payroll_lessons_info',
        'app.reassessment_questions',
        'app.program_bookings',
        'app.student_subjects',
        'app.tutor_subjects',
        'app.availabilities',
        'app.days',
        'app.availability_time',
        'app.feedbacks',
        'app.rebooked',
        'app.applications',
        'app.application_answers',
        'app.module_users',
        'app.module_user_questions',
        'app.modules',
        'app.module_questions',
        'app.lead_payroll',
        'app.lead_payroll_all',
        'app.booking_programs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Messages') ? [] : ['className' => 'App\Model\Table\MessagesTable'];
        $this->Messages = TableRegistry::get('Messages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Messages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
