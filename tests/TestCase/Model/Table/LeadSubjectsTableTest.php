<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeadSubjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeadSubjectsTable Test Case
 */
class LeadSubjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeadSubjectsTable
     */
    public $LeadSubjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lead_subjects',
        'app.leads',
        'app.subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LeadSubjects') ? [] : ['className' => 'App\Model\Table\LeadSubjectsTable'];
        $this->LeadSubjects = TableRegistry::get('LeadSubjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LeadSubjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
