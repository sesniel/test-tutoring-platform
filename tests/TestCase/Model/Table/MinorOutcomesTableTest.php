<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MinorOutcomesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MinorOutcomesTable Test Case
 */
class MinorOutcomesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MinorOutcomesTable
     */
    public $MinorOutcomes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.minor_outcomes',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.year_levels',
        'app.lessons',
        'app.programs',
        'app.assessment_types',
        'app.student_assessment_types',
        'app.statuses',
        'app.lesson_resources',
        'app.resources',
        'app.questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MinorOutcomes') ? [] : ['className' => 'App\Model\Table\MinorOutcomesTable'];
        $this->MinorOutcomes = TableRegistry::get('MinorOutcomes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MinorOutcomes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
