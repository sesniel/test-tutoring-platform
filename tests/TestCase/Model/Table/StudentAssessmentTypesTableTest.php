<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StudentAssessmentTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StudentAssessmentTypesTable Test Case
 */
class StudentAssessmentTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StudentAssessmentTypesTable
     */
    public $StudentAssessmentTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.student_assessment_types',
        'app.students',
        'app.assessment_types',
        'app.subjects',
        'app.lessons',
        'app.programs',
        'app.tutors',
        'app.statuses',
        'app.reassessment_questions',
        'app.major_outcomes',
        'app.categories',
        'app.year_levels',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources',
        'app.user_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StudentAssessmentTypes') ? [] : ['className' => 'App\Model\Table\StudentAssessmentTypesTable'];
        $this->StudentAssessmentTypes = TableRegistry::get('StudentAssessmentTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StudentAssessmentTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
