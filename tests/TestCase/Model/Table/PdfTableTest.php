<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PdfTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PdfTable Test Case
 */
class PdfTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PdfTable
     */
    public $Pdf;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pdf',
        'app.subheaders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pdf') ? [] : ['className' => 'App\Model\Table\PdfTable'];
        $this->Pdf = TableRegistry::get('Pdf', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pdf);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
