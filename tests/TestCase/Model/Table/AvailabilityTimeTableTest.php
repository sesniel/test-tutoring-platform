<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AvailabilityTimeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AvailabilityTimeTable Test Case
 */
class AvailabilityTimeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AvailabilityTimeTable
     */
    public $AvailabilityTime;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.availability_time',
        'app.availabilities',
        'app.users',
        'app.days'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AvailabilityTime') ? [] : ['className' => 'App\Model\Table\AvailabilityTimeTable'];
        $this->AvailabilityTime = TableRegistry::get('AvailabilityTime', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AvailabilityTime);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
