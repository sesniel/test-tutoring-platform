<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReassessmentQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReassessmentQuestionsTable Test Case
 */
class ReassessmentQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReassessmentQuestionsTable
     */
    public $ReassessmentQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.reassessment_questions',
        'app.programs',
        'app.students',
        'app.tutors',
        'app.statuses',
        'app.lessons',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.year_levels',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.assessment_types',
        'app.student_assessment_types',
        'app.lesson_resources'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReassessmentQuestions') ? [] : ['className' => 'App\Model\Table\ReassessmentQuestionsTable'];
        $this->ReassessmentQuestions = TableRegistry::get('ReassessmentQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReassessmentQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
