<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SubheadersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SubheadersTable Test Case
 */
class SubheadersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SubheadersTable
     */
    public $Subheaders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.subheaders',
        'app.headers',
        'app.trainings',
        'app.pdf'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Subheaders') ? [] : ['className' => 'App\Model\Table\SubheadersTable'];
        $this->Subheaders = TableRegistry::get('Subheaders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subheaders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
