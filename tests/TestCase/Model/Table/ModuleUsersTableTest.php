<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ModuleUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ModuleUsersTable Test Case
 */
class ModuleUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ModuleUsersTable
     */
    public $ModuleUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.module_users',
        'app.users',
        'app.students',
        'app.clients',
        'app.states',
        'app.availabilities',
        'app.days',
        'app.availability_time',
        'app.feedbacks',
        'app.year_levels',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.tutors',
        'app.reassessment_questions',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.user_questions',
        'app.student_assessment_types',
        'app.lesson_resources',
        'app.lead_subjects',
        'app.leads',
        'app.student_subjects',
        'app.tutor_subjects',
        'app.module_user_questions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ModuleUsers') ? [] : ['className' => 'App\Model\Table\ModuleUsersTable'];
        $this->ModuleUsers = TableRegistry::get('ModuleUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ModuleUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
