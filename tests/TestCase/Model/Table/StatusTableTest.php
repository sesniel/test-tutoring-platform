<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatusTable Test Case
 */
class StatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatusTable
     */
    public $Status;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.status',
        'app.leads',
        'app.clients',
        'app.statuses',
        'app.tutors',
        'app.students',
        'app.lead_subjects',
        'app.subjects',
        'app.lessons',
        'app.programs',
        'app.reassessment_questions',
        'app.major_outcomes',
        'app.categories',
        'app.year_levels',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.assessment_types',
        'app.student_assessment_types',
        'app.lesson_resources',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Status') ? [] : ['className' => 'App\Model\Table\StatusTable'];
        $this->Status = TableRegistry::get('Status', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Status);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
