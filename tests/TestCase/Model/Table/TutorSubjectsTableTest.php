<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TutorSubjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TutorSubjectsTable Test Case
 */
class TutorSubjectsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TutorSubjectsTable
     */
    public $TutorSubjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tutor_subjects',
        'app.tutors',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.students',
        'app.clients',
        'app.year_levels',
        'app.leads',
        'app.statuses',
        'app.lead_subjects',
        'app.student_assessment_types',
        'app.user_questions',
        'app.student_subjects',
        'app.reassessment_questions',
        'app.major_outcomes',
        'app.categories',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TutorSubjects') ? [] : ['className' => 'App\Model\Table\TutorSubjectsTable'];
        $this->TutorSubjects = TableRegistry::get('TutorSubjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TutorSubjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
