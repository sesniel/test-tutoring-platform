<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserQuestionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserQuestionsTable Test Case
 */
class UserQuestionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserQuestionsTable
     */
    public $UserQuestions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_questions',
        'app.student_assessment_types',
        'app.students',
        'app.clients',
        'app.year_levels',
        'app.leads',
        'app.statuses',
        'app.tutors',
        'app.lead_subjects',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.reassessment_questions',
        'app.major_outcomes',
        'app.categories',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources',
        'app.student_subjects',
        'app.tutor_subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserQuestions') ? [] : ['className' => 'App\Model\Table\UserQuestionsTable'];
        $this->UserQuestions = TableRegistry::get('UserQuestions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserQuestions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
