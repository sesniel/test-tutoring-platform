<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LeadsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LeadsTable Test Case
 */
class LeadsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LeadsTable
     */
    public $Leads;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leads',
        'app.consultations',
        'app.lead_answers',
        'app.clients',
        'app.states',
        'app.students',
        'app.year_levels',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.tutors',
        'app.availabilities',
        'app.users',
        'app.feedbacks',
        'app.days',
        'app.availability_time',
        'app.module_users',
        'app.module_user_questions',
        'app.modules',
        'app.module_questions',
        'app.lead_payroll',
        'app.lead_subjects',
        'app.lead_payroll_all',
        'app.payroll_lessons',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources',
        'app.user_questions',
        'app.student_assessment_types',
        'app.lesson_students',
        'app.program_students',
        'app.reassessment_questions',
        'app.student_subjects',
        'app.tutor_subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Leads') ? [] : ['className' => 'App\Model\Table\LeadsTable'];
        $this->Leads = TableRegistry::get('Leads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Leads);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
