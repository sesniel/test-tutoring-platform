<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\YearLevelsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\YearLevelsTable Test Case
 */
class YearLevelsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\YearLevelsTable
     */
    public $YearLevels;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.year_levels',
        'app.major_outcomes',
        'app.categories',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.students',
        'app.clients',
        'app.leads',
        'app.statuses',
        'app.tutors',
        'app.lead_subjects',
        'app.student_assessment_types',
        'app.user_questions',
        'app.student_subjects',
        'app.reassessment_questions',
        'app.lesson_resources',
        'app.resources',
        'app.minor_outcomes',
        'app.questions',
        'app.tutor_subjects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('YearLevels') ? [] : ['className' => 'App\Model\Table\YearLevelsTable'];
        $this->YearLevels = TableRegistry::get('YearLevels', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->YearLevels);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
