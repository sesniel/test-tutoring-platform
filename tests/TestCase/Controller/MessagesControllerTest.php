<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MessagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MessagesController Test Case
 */
class MessagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.messages',
        'app.users',
        'app.rates',
        'app.tutors',
        'app.owner',
        'app.students',
        'app.clients',
        'app.states',
        'app.leads',
        'app.consultations',
        'app.lead_answers',
        'app.abandoned_leads',
        'app.lead_subjects',
        'app.subjects',
        'app.assessment_types',
        'app.lessons',
        'app.programs',
        'app.program_students',
        'app.lesson_students',
        'app.major_outcomes',
        'app.categories',
        'app.year_levels',
        'app.student_assessment_types',
        'app.user_questions',
        'app.minor_outcomes',
        'app.questions',
        'app.resources',
        'app.lesson_resources',
        'app.rebook_lessons',
        'app.payroll_lessons',
        'app.bookings',
        'app.payroll_lessons_info',
        'app.reassessment_questions',
        'app.program_bookings',
        'app.student_subjects',
        'app.tutor_subjects',
        'app.availabilities',
        'app.days',
        'app.availability_time',
        'app.feedbacks',
        'app.rebooked',
        'app.applications',
        'app.application_answers',
        'app.module_users',
        'app.module_user_questions',
        'app.modules',
        'app.module_questions',
        'app.lead_payroll',
        'app.lead_payroll_all',
        'app.booking_programs'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
